/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-28 10:40:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-27 15:02:51
 */
import React, {Component} from 'react';
import { Platform, Alert } from 'react-native';
import AppNavigator from './src/AppNavigator';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import firebase from 'react-native-firebase';
import type { Messaging } from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './src/store/configureStore';
import { Actions } from 'react-native-router-flux';
import { Sentry } from 'react-native-sentry';
import { getReservationDetail, getVehicleAttribute } from './src/actions';
import { PUBLIC_DSN } from './src/constant';

Sentry.config(PUBLIC_DSN).install();

const store = configureStore();

export default class App extends Component {
  
  componentWillMount(){
    if(Platform.OS === 'android'){
      SplashScreen.show();
      setTimeout(() => {
        SplashScreen.hide();
      }, 500) ;
    }
  }

  async componentDidMount() {
    firebase.notifications().removeAllDeliveredNotifications();
    this.checkPermission();
    this.createNotificationListeners();
  }  

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await firebase.messaging().getToken();
    console.log(fcmToken);
  }

  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        this.getToken();
    } catch (error) {
        console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    // Triger pada saat foreground
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body, data, notificationId } = notification;
        console.log('notifikasi tiba ==>', notification);
        const channelId = new firebase.notifications.Android.Channel("Default", "Default", firebase.notifications.Android.Importance.High);
        firebase.notifications().android.createChannel(channelId);
        let notif = new  firebase.notifications.Notification({
                                                    data: data,
                                                    sound: 'default',
                                                    show_in_foreground: true,
                                                    title: title,
                                                    body: body,
                                                });

        if(Platform.OS == "android") {
            notif
              .android.setPriority(firebase.notifications.Android.Priority.High)
              .android.setChannelId("Default")
              .android.setVibrate(1000)
              .setNotificationId(notificationId);
        } else {
          console.log('notifikasi di ios');
        }

        firebase.notifications().displayNotification(notif);
    });
  
    // Triger pada saat klik tray notifikasi
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { notificationId, data } = notificationOpen.notification;
        let dt = JSON.parse(data.body);
        firebase.notifications().removeDeliveredNotification(notificationId);
        this.showPage(dt);
    });

    this.iOSnotificationOpenedListener = firebase.notifications().onNotificationDisplayed(notification => {
      console.log('ios ==>', notification)
    })
  
    // handle jika ada notifikasi pada saat aplikasi tertutup
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { action, notification: { notificationId, data } } = notificationOpen;
        let dt = JSON.parse(data.body);
        this.showPage(dt);
    }
    // this.messageListener = firebase.messaging().onMessage((message) => {
    //   console.log(JSON.stringify(message));
    // });
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  showPage(dt) {
    switch (dt.category) {
      case "detail":
        let state = store.store.getState();
        store.store.dispatch(getVehicleAttribute({ token: state.authentication.login.token }))
        Actions.push("CarDetail", {
          currentPage: "My Booking",
          item: {
            Id:null,
            CompanyId:null,
            ReservationId:null,
            BusinessUnitId:null,
            CustomerId:null,
            CustomerName:null,
            CustomerType:null,
            ChannelId:null,
            ServiceTypeId:null,
            ServiceTypeName:null,
            CreatedBy:null,
            TotalPrice:null,
            CostCenterId:null,
            CancellationReason:null,
            CancellationFee:null,
            CancellationTime:null,
            Status:null,
            created_at:null,
            deleted_at:null,
            PIC:null,
            PaymentStatus:null,
            PONumber:null,
            PODate:null,
            CancelBy:null,
            PONumber2:null,
            VANumber:null,
            WaitingForPaymentTime:null,
            PICPhone:null,
            NotesReservation:null,
            PriceRefund:null,
            PICName:null,
            NoRekening: null,
            BankName: null,
            BankAccountName: null,
            additional: [],
            details: []
          },
          ReservationId: dt.ReservationId
        });
        break;
      default:
        Actions.push("MyBooking");
        break;
    }
  }

  render() {
    return (
      <Provider store={store.store}>
        <PersistGate loading={null} persistor={store.persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    );
  }
}
