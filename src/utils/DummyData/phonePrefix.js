const phonePrefix = [
    {
        id: '62',
        value: '+62',
        image: require('../../assets/images/png/flags/62.png'),
    },
    {
        id: '1',
        value: '+01',
        image: require('../../assets/images/png/flags/1.png'),
    },
    {
        id: '3',
        value: '+03',
        image: require('../../assets/images/png/flags/3.png'),
    },
    {
        id: '65',
        value: '+65',
        image: require('../../assets/images/png/flags/65.png'),
    },
]

export default phonePrefix;