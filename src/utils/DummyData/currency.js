const currencyList = [
    {
        id: 'IDR',
        label: 'Indonesia Rupiah',
        img: require('../../assets/images/png/country/ID.png'),
        selected: true
    },
    {
        id: 'USA',
        label: 'US Dollars',
        img: require('../../assets/images/png/country/USA.png'),
        selected: false
    },
    {
        id: 'EUR',
        label: 'EURO',
        img: require('../../assets/images/png/country/EUR.png'),
        selected: false
    },
    {
        id: 'SGD',
        label: 'Singapore Dollars',
        img: require('../../assets/images/png/country/SGD.png'),
        selected: false
    },
    {
        id: 'JPY',
        label: 'Japan',
        img: require('../../assets/images/png/country/SGD.png'),
        selected: false
    },
]

export default currencyList