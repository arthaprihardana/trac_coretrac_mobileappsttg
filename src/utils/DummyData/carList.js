const carList = [
    {
        id: 1,
        name: 'Toyota Avanza',
        price: 325000,
        img: require('../../assets/images/png/cars/Avanza.png'),
        pessenger: 6,
        baggage: 3,
        transmition: 'MT',
        airbag: true,
        ac: true,
        status: 1,
        statusNote: '',
        numBook: 0,
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: Toyota Avanza",
                    "Year: 2017",
                    "Vehicle Age: 1 Years Old",
                    "Capacity: 4 Passangers",
                    "Gear Box: Manual",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
    {
        id: 2,
        name: 'Alphard',
        price: 625000,
        img: require('../../assets/images/png/cars/toyota-alphard.png'),
        pessenger: 6,
        baggage: 2,
        transmition: 'MT',
        airbag: true,
        ac: true,
        status: 1,
        statusNote: '',
        numBook: 0,
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: Alphard",
                    "Year: 2016",
                    "Vehicle Age: 2 Years Old",
                    "Capacity: 6 Passangers",
                    "Gear Box: Manual",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
    {
        id: 3,
        name: 'BMW',
        price: 725000,
        img: require('../../assets/images/png/cars/bmw.png'),
        pessenger: 3,
        baggage: 2,
        transmition: 'MT',
        airbag: true,
        ac: true,
        status: 2,
        statusNote: 'Hurry 1 Car Left',
        numBook: 0,
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: BMW",
                    "Year: 2017",
                    "Vehicle Age: 1 Years Old",
                    "Capacity: 3 Passangers",
                    "Gear Box: Auto",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
    {
        id: 4,
        name: 'Innova',
        price: 425000,
        img: require('../../assets/images/png/cars/innova.png'),
        pessenger: 5,
        baggage: 2,
        transmition: 'M',
        airbag: true,
        ac: true,
        status: 3,
        statusNote: 'Car on Booking',
        numBook: 0,
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: Innova",
                    "Year: 2017",
                    "Vehicle Age: 1 Years Old",
                    "Capacity: 5 Passangers",
                    "Gear Box: Manual",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
    {
        id: 5,
        name: 'Yaris',
        price: 225000,
        img: require('../../assets/images/png/cars/yaris.png'),
        pessenger: 4,
        baggage: 2,
        transmition: 'A',
        status: 1,
        statusNote: '',
        numBook: 0,
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: Yaris",
                    "Year: 2017",
                    "Vehicle Age: 1 Years Old",
                    "Capacity: 4 Passangers",
                    "Gear Box: Auto",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Parking",
                    "Tax"
                ]
            }
        ]
    },
]

export default carList;