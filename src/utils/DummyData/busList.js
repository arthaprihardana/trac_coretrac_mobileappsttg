const busList = [
  {
    id: 1,
    name: 'HD Big Bus',
    price: 325000,
    img: require('../../assets/images/png/bus/bus.png'),
    pessenger: 45,
    karaoke: true,
    charger: true,
    wifi: true,
    status: 1,
    statusNote: '',
    numBook: 0,
    recommended: true,
    interior: [
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
    ],
    moreDetail: [
      {
        labelDetail: 'Basic Services',
        contents: [
          "14 Hours services maximum",
          "Max 4 hours non stop driving",
        ],
      },
      {
        labelDetail: 'Basic Vehicle Information',
        contents: [
          "Reclining seat",
          "A/C",
          "Cabin Baggage",
        ]
      },
      {
        labelDetail: 'Safety Equipment',
        contents: [
          "Emergency Exit",
          "APAR",
          "Palu pemecah kaca",
          "2 Point safety belt",
          "Real Time Monitoring OBD"
        ]
      }
    ]
  },
  {
    id: 2,
    name: 'HD Big Bus',
    price: 325000,
    img: require('../../assets/images/png/bus/bus.png'),
    pessenger: 45,
    karaoke: true,
    charger: true,
    wifi: true,
    status: 1,
    statusNote: '',
    numBook: 0,
    recommended: true,
    interior: [
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
    ],
    moreDetail: [
      {
        labelDetail: 'Basic Services',
        contents: [
          "14 Hours services maximum",
          "Max 4 hours non stop driving",
        ],
      },
      {
        labelDetail: 'Basic Vehicle Information',
        contents: [
          "Reclining seat",
          "A/C",
          "Cabin Baggage",
        ]
      },
      {
        labelDetail: 'Safety Equipment',
        contents: [
          "Emergency Exit",
          "APAR",
          "Palu pemecah kaca",
          "2 Point safety belt",
          "Real Time Monitoring OBD"
        ]
      }
    ]
  },
  {
    id: 3,
    name: 'HD Big Bus',
    price: 325000,
    img: require('../../assets/images/png/bus/bus.png'),
    pessenger: 45,
    karaoke: true,
    charger: true,
    wifi: true,
    status: 1,
    statusNote: '',
    numBook: 0,
    interior: [
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
    ],
    moreDetail: [
      {
        labelDetail: 'Basic Services',
        contents: [
          "14 Hours services maximum",
          "Max 4 hours non stop driving",
        ],
      },
      {
        labelDetail: 'Basic Vehicle Information',
        contents: [
          "Reclining seat",
          "A/C",
          "Cabin Baggage",
        ]
      },
      {
        labelDetail: 'Safety Equipment',
        contents: [
          "Emergency Exit",
          "APAR",
          "Palu pemecah kaca",
          "2 Point safety belt",
          "Real Time Monitoring OBD"
        ]
      }
    ]
  },
  {
    id: 4,
    name: 'HD Big Bus',
    price: 325000,
    img: require('../../assets/images/png/bus/bus.png'),
    pessenger: 45,
    karaoke: true,
    charger: true,
    wifi: true,
    status: 1,
    statusNote: '',
    numBook: 0,
    interior: [
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
    ],
    moreDetail: [
      {
        labelDetail: 'Basic Services',
        contents: [
          "14 Hours services maximum",
          "Max 4 hours non stop driving",
        ],
      },
      {
        labelDetail: 'Basic Vehicle Information',
        contents: [
          "Reclining seat",
          "A/C",
          "Cabin Baggage",
        ]
      },
      {
        labelDetail: 'Safety Equipment',
        contents: [
          "Emergency Exit",
          "APAR",
          "Palu pemecah kaca",
          "2 Point safety belt",
          "Real Time Monitoring OBD"
        ]
      }
    ]
  },
  {
    id: 5,
    name: 'HD Big Bus',
    price: 325000,
    img: require('../../assets/images/png/bus/bus.png'),
    pessenger: 45,
    karaoke: true,
    charger: true,
    wifi: true,
    status: 1,
    statusNote: '',
    numBook: 0,
    interior: [
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
      {image: require('../../assets/images/png/interior.png')},
    ],
    moreDetail: [
      {
        labelDetail: 'Basic Services',
        contents: [
          "14 Hours services maximum",
          "Max 4 hours non stop driving",
        ],
      },
      {
        labelDetail: 'Basic Vehicle Information',
        contents: [
          "Reclining seat",
          "A/C",
          "Cabin Baggage",
        ]
      },
      {
        labelDetail: 'Safety Equipment',
        contents: [
          "Emergency Exit",
          "APAR",
          "Palu pemecah kaca",
          "2 Point safety belt",
          "Real Time Monitoring OBD"
        ]
      }
    ]
  },
]

export default busList;