const faqList = [
  {
    category: "PERTANYAAN UMUM",
    question: "Siapa TRAC?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque.",
  },
  {
    category: "PERTANYAAN UMUM",
    question: "Apa saja layanan yang disediakan oleh TRAC?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque.",
  },
  {
    category: "PERTANYAAN UMUM",
    question: "Apa keuntungan bagi saya jika menggunakan layanan TRAC?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque.",
  },
  {
    category: "PEMBAYARAN, PEMBATALAN, DAN REFUND",
    question: "Di mana saja saya bisa menggunakan layanan TRAC?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque.",
  },
  {
    category: "PEMBAYARAN, PEMBATALAN, DAN REFUND",
    question: "Jenis mobil apa saja yang disewakan TRAC?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque.",
  },
  {
    category: "PERTANYAAN UMUM",
    question: "Ke mana saya bisa menghubungi TRAC?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque.",
  },
  {
    category: "PERTANYAAN UMUM",
    question: "Apakah saya bisa menitipkan kendaraan saya kepada TRAC untuk disewakan?",
    answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
      "\n\n" +
      "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adi"
  }
]

export default faqList