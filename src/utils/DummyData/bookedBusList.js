const bookedBusList = [
    {
        id: 1,
        name: 'HD Big Bus',
        price: 325000,
        originalPrice: 400000,
        img: require('../../assets/images/png/bus/big-bus.png'),
        passanger: 4,
        karaoke: true,
        wifi: true,
        charger: true,
        status: 1,
        isActive: true,
        statusNote: '',
        numBook: 0,
        extras: [
            {
                id: 'ex1',
                label: 'Add Hour',
                value: 0,
                desc: 'Rp. 150,000/hour',
                priceFitur: 150000
            },
            {
                id: 'ex2',
                label: 'Out of Town',
                value: 0,
                desc: 'Rp. 50,000/trip',
                priceFitur: 50000
            },
            {
                id: 'ex3',
                label: 'Overnight',
                value: 0,
                desc: 'Rp. 150,000/hour',
                priceFitur: 150000
            },
            {
                id: 'ex4',
                label: 'Fuel Plan',
                value: false,
                desc: 'Rp. 50,000/trip',
                priceFitur: 50000
            },
        ],
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: Toyota Avanza",
                    "Year: 2017",
                    "Vehicle Age: 1 Years Old",
                    "Capacity: 4 Passangers",
                    "Gear Box: Manual",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
    {
        id: 2,
        name: 'Coaster',
        price: 325000,
        originalPrice: 400000,
        img: require('../../assets/images/png/bus/coaster.png'),
        passanger: 6,
        suitcase: 2,
        isActive: false,
        transmition: 'M',
        status: 1,
        statusNote: '',
        numBook: 0,
        extras: [
            {
                id: 'ex1',
                label: 'Add Hour',
                value: 0,
                desc: 'Rp. 150,000/hour',
                priceFitur: 150000
            },
            {
                id: 'ex2',
                label: 'Out of Town',
                value: 0,
                desc: 'Rp. 50,000/trip',
                priceFitur: 50000
            },
            {
                id: 'ex3',
                label: 'Overnight',
                value: 0,
                desc: 'Rp. 150,000/hour',
                priceFitur: 150000
            },
            {
                id: 'ex4',
                label: 'Fuel Plan',
                value: false,
                desc: 'Rp. 50,000/trip',
                priceFitur: 50000
            },
        ],
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: Alphard",
                    "Year: 2016",
                    "Vehicle Age: 2 Years Old",
                    "Capacity: 6 Passangers",
                    "Gear Box: Manual",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
    {
        id: 3,
        name: 'Mini Bus',
        price: 325000,
        originalPrice: 400000,
        img: require('../../assets/images/png/bus/mini-bus.png'),
        passanger: 3,
        suitcase: 2,
        transmition: 'A',
        status: 2,
        isActive: false,
        statusNote: 'Hurry 1 Car Left',
        numBook: 0,
        extras: [
            {
                id: 'ex1',
                label: 'Add Hour',
                value: 0,
                desc: 'Rp. 150,000/hour',
                priceFitur: 150000
            },
            {
                id: 'ex2',
                label: 'Out of Town',
                value: 0,
                desc: 'Rp. 50,000/trip',
                priceFitur: 50000
            },
            {
                id: 'ex3',
                label: 'Overnight',
                value: 0,
                desc: 'Rp. 150,000/hour',
                priceFitur: 150000
            },
            {
                id: 'ex4',
                label: 'Fuel Plan',
                value: false,
                desc: 'Rp. 50,000/trip',
                priceFitur: 50000
            },
        ],
        moreDetail: [
            {
                labelDetail: 'Vehicle Specifications',
                contents: [
                    "Model: BMW",
                    "Year: 2017",
                    "Vehicle Age: 1 Years Old",
                    "Capacity: 3 Passangers",
                    "Gear Box: Auto",
                    "Fuel Type: Premium",
                    "Fuel Tank: 45 Litres"
                ],
            },
            {
                labelDetail: 'Features',
                contents: [
                    "Air Conditioning",
                    "CD Player / MP3 / Radio",
                    "Air Bags",
                ]
            },
            {
                labelDetail: 'What’s Included',
                contents: [
                    "Driver",
                    "Toll",
                    "Unlimited Mileage",
                    "Tax"
                ]
            }
        ]
    },
]

export default bookedBusList;