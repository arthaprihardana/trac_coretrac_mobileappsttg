export const GetShortMonth = (month) => {
  let strMonth = '';
  switch(month) {
    case 1: strMonth = 'Jan'; break;
    case 2: strMonth = 'Feb'; break;
    case 3: strMonth = 'Mar'; break;
    case 4: strMonth = 'Apr'; break;
    case 5: strMonth = 'May'; break;
    case 6: strMonth = 'Jun'; break;
    case 7: strMonth = 'Jul'; break;
    case 8: strMonth = 'Aug'; break;
    case 9: strMonth = 'Sep'; break;
    case 10: strMonth = 'Oct'; break;
    case 11: strMonth = 'Nov'; break;
    case 12: strMonth = 'Des'; break;
  }

  return strMonth;
}

export const FormatTwoDigit = (digit) => {
  if (digit < 10) {
    return "0" + digit;
  } else {
    return String(digit);
  }
}