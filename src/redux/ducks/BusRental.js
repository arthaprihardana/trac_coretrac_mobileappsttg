// Initial State
const STATE = {
  selectedCity: null,
  selectedAddress: null,
  selectedDestination: null,
  selectedDestinationDetail: null,
  passengerNumber: null,
  selectedTime: null,
  selectedStartDate: null,
  selectedEndDate: null,
};




// Action Types
export const UPDATE_SELECTED_CITY = 'trac/bus-rental/UPDATE_SELECTED_CITY';
export const UPDATE_SELECTED_ADDRESS = 'trac/bus-rental/UPDATE_SELECTED_ADDRESS';
export const UPDATE_SELECTED_DESTINATION = 'trac/bus-rental/UPDATE_SELECTED_DESTINATION';
export const UPDATE_SELECTED_DESTINATION_DETAIL = 'trac/bus-rental/UPDATE_SELECTED_DESTINATION_DETAIL';
export const UPDATE_PASSENGER = 'trac/bus-rental/UPDATE_PASSENGER';
export const UPDATE_PICKUP_DATE_TIME = 'trac/bus-rental/UPDATE_PICKUP_DATE_TIME';
export const RESET_BUS_RENTAL_DATA = 'trac/bus-rental/RESET_BUS_RENTAL_DATA';




// Reducers
export default function BusRental(state = STATE, action = {}) {
  switch(action.type) {
    case UPDATE_SELECTED_CITY:
      return {
        ...state,
        selectedCity: action.payload.selectedCity
      }
    case UPDATE_SELECTED_ADDRESS:
      return {
        ...state,
        selectedAddress: action.payload.selectedAddress
      }
    case UPDATE_SELECTED_DESTINATION:
      return {
        ...state,
        selectedDestination: action.payload.selectedDestination
      }
    case UPDATE_SELECTED_DESTINATION_DETAIL:
      return {
        ...state,
        selectedDestinationDetail: action.payload.selectedDestinationDetail
      }
    case UPDATE_PASSENGER:
      return {
        ...state,
        passengerNumber: action.payload.passengerNumber
      }
    case UPDATE_PICKUP_DATE_TIME:
      return {
        ...state,
        selectedTime: action.payload.selectedTime,
        selectedStartDate: action.payload.selectedStartDate,
        selectedEndDate: action.payload.selectedEndDate
      }
    case RESET_BUS_RENTAL_DATA: 
      return {}
    default: 
      return state;
  }
}





// Action Creators

export function updateSelectedCity(payload) {
  return { 
    type: UPDATE_SELECTED_CITY,
    payload: payload
  }
}

export function updateSelectedAddress(payload) {
  return { 
    type: UPDATE_SELECTED_ADDRESS,
    payload: payload
  }
}

export function updateSelectedDestination(payload) {
  return { 
    type: UPDATE_SELECTED_DESTINATION,
    payload: payload
  }
}

export function updateSelectedDestinationDetail(payload) {
  return { 
    type: UPDATE_SELECTED_DESTINATION_DETAIL,
    payload: payload
  }
}

export function updatePassenger(payload) {
  return { 
    type: UPDATE_PASSENGER,
    payload: payload
  }
}

export function updateSelectedPickupDateTime(payload) {
  return { 
    type: UPDATE_PICKUP_DATE_TIME,
    payload: payload
  }
}

export function resetBusRentalData() {
  return { 
    type: RESET_BUS_RENTAL_DATA
  }
}