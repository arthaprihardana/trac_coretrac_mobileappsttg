// Initial State
const STATE = {
  selectedServiceType: null,
  selectedCity: null,
  selectedAddress: null,
  selectedPackage: null,
  selectedTime: null,
  selectedStartDate: null,
  selectedEndDate: null,
};




// Action Types
export const UPDATE_SELECTED_SERVICE_TYPE = 'trac/car-rental/UPDATE_SELECTED_SERVICE_TYPE';
export const UPDATE_SELECTED_CITY = 'trac/car-rental/UPDATE_SELECTED_CITY';
export const UPDATE_SELECTED_ADDRESS = 'trac/car-rental/UPDATE_SELECTED_ADDRESS';
export const UPDATE_SELECTED_PACKAGE = 'trac/car-rental/UPDATE_SELECTED_PACKAGE';
export const UPDATE_PICKUP_DATE_TIME = 'trac/car-rental/UPDATE_PICKUP_DATE_TIME';
export const RESET_CAR_RENTAL_DATA = 'trac/car-rental/RESET_CAR_RENTAL_DATA';




// Reducers
export default function CarRental(state = STATE, action = {}) {
  switch(action.type) {
    case UPDATE_SELECTED_SERVICE_TYPE: 
      return {
        ...state,
        selectedServiceType: action.payload.selectedServiceType
      }
    case UPDATE_SELECTED_CITY:
      return {
        ...state,
        selectedCity: action.payload.selectedCity
      }
    case UPDATE_SELECTED_ADDRESS:
      return {
        ...state,
        selectedAddress: action.payload.selectedAddress
      }
    case UPDATE_SELECTED_PACKAGE:
      return {
        ...state,
        selectedPackage: action.payload.selectedPackage
      }
    case UPDATE_PICKUP_DATE_TIME:
      return {
        ...state,
        selectedTime: action.payload.selectedTime,
        selectedStartDate: action.payload.selectedStartDate,
        selectedEndDate: action.payload.selectedEndDate
      }
    case RESET_CAR_RENTAL_DATA: 
      return {}
    default: 
      return state;
  }
}





// Action Creators
export function updateSelectedServiceType(payload) {
  return { 
    type: UPDATE_SELECTED_SERVICE_TYPE,
    payload: payload
  }
}

export function updateSelectedCity(payload) {
  return { 
    type: UPDATE_SELECTED_CITY,
    payload: payload
  }
}

export function updateSelectedAddress(payload) {
  return { 
    type: UPDATE_SELECTED_ADDRESS,
    payload: payload
  }
}

export function updateSelectedPackage(payload) {
  return { 
    type: UPDATE_SELECTED_PACKAGE,
    payload: payload
  }
}

export function updateSelectedPickupDateTime(payload) {
  return { 
    type: UPDATE_PICKUP_DATE_TIME,
    payload: payload
  }
}

export function resetCarRentalData() {
  return { 
    type: RESET_CAR_RENTAL_DATA
  }
}