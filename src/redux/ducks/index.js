import { combineReducers } from 'redux';

import CarRental from './CarRental';
import BusRental from './BusRental';

const allReducer = {
  CarRental,
  BusRental
}

const RootReducer = combineReducers(allReducer);

export default RootReducer;