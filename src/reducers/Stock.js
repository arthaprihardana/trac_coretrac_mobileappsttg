/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 15:21:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-07 14:28:32
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_STOCK_LIST, GET_STOCK_LIST_SUCCESS, GET_STOCK_LIST_FAILURE, SET_REQUEST_STOCK_LIST, TEMP_STOCK, TEMP_STOCK_COMPLETE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    stock: null,
    requestStock: null,
    tempStock: null,
    tempStockComplete: null
}

const persistConfig = {
    key: 'stockList',
    storage: storage,
    whitelist: ['requestStock', 'tempStock', 'tempStockComplete']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_STOCK_LIST:
            return { ...state, loading: true }
        case GET_STOCK_LIST_SUCCESS:
            return { ...state, loading: false, stock: action.payload }
        case GET_STOCK_LIST_FAILURE:
            return { ...state, loading: false, stock: null }
        case SET_REQUEST_STOCK_LIST:
            return { ...state, requestStock: action.payload }
        case TEMP_STOCK:
            return { ...state, tempStock: action.payload }
        case TEMP_STOCK_COMPLETE:
            return { ...state, tempStockComplete: action.payload }
        default:
            return { ...state }
    }
})