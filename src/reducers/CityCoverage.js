/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 14:43:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-05 14:44:25
 */
import { GET_CITY_COVERAGE, GET_CITY_COVERAGE_SUCCESS, GET_CITY_COVERAGE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    cityCoverage: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CITY_COVERAGE:
            return { ...state, loading: true }
        case GET_CITY_COVERAGE_SUCCESS:
            return { ...state, loading: false, cityCoverage: action.payload }
        case GET_CITY_COVERAGE_FAILURE:
            return { ...state, loading: false, cityCoverage: null }
        default:
            return { ...state }
    }
}