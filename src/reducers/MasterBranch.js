/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 11:37:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-21 14:14:53
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_MASTER_BRANCH_FOR_BUS, GET_MASTER_BRANCH_FOR_BUS_SUCCESS, GET_MASTER_BRANCH_FOR_BUS_FAILURE, SET_ACTUAL_BRANCH_FOR_BUS } from '../actions/Types';

const INIT_STATE = {
    loading: false,
    branch: null,
    actualBranch: null
}

const persistConfig = {
    key: 'masterBranch',
    storage: storage,
    whitelist: ['branch', 'actualBranch']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_BRANCH_FOR_BUS:
            return { ...state, loading: true }
        case GET_MASTER_BRANCH_FOR_BUS_SUCCESS:
            return { ...state, loading: false, branch: action.payload }
        case GET_MASTER_BRANCH_FOR_BUS_FAILURE:
            return { ...state, loading: false, branch: null }
        case SET_ACTUAL_BRANCH_FOR_BUS:
            return { ...state, actualBranch: action.payload }
        default:
            return { ...state }
    }
});