/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 17:17:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-03 11:13:23
 */
import _ from "lodash";
import { GET_ADDRESS_GM, GET_ADDRESS_GM_FAILURE, GET_ADDRESS_GM_SUCCESS, SEARCH_KEY_GM, GET_DISTANCE_MATRIX_GM, GET_DISTANCE_MATRIX_GM_SUCCESS, GET_DISTANCE_MATRIX_GM_FAILURE, GET_PLACE_DETAIL_GM, GET_PLACE_DETAIL_GM_SUCCESS, GET_PLACE_DETAIL_GM_FAILURE, GET_FIND_GEOMETRY_GM, GET_FIND_GEOMETRY_GM_SUCCESS, GET_FIND_GEOMETRY_GM_FAILURE, GET_PLACE_DETAIL_GM_FOR_TO_LOCATION_BUS, GET_DISTANCE_MATRIX_GM_FOR_BUS, GET_DISTANCE_MATRIX_GM_FOR_BUS_SUCCESS, GET_DISTANCE_MATRIX_GM_FOR_BUS_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    searchKey: null,
    address: null,
    distanceMatrix: null,
    placeDetail: null,
    distanceMatrixForBus: null,
    geometry: null,
    isToLocationBus: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SEARCH_KEY_GM:
            return { ...state, searchKey: action.payload }
        case GET_ADDRESS_GM:
            return { ...state, loading: true }
        case GET_ADDRESS_GM_SUCCESS:
            let address = _.map(action.payload, (v, k) => {
                return {
                  label: v.description,
                  value: v.place_id
                }
            });
            return { ...state, loading: false, address: address }
        case GET_ADDRESS_GM_FAILURE:
            return { ...state, loading: false, address: null }
        case GET_DISTANCE_MATRIX_GM:
            return { ...state, loading: true }
        case GET_DISTANCE_MATRIX_GM_SUCCESS:
            // let distanceBetween = null;
            let toKm = null;
            // if(action.payload.length > 1) {
            //     distanceBetween = _.map(action.payload, v => v.elements[0].distance.value / 1000);
            // } else {
                let calculate = action.payload[0].elements[0].distance.value / 1000;
                toKm = _.round(calculate)
            // }
            return { ...state, loading: false, distanceMatrix: toKm }
        case GET_DISTANCE_MATRIX_GM_FAILURE:
            return { ...state, loading: false, distanceMatrix: null }
        case GET_DISTANCE_MATRIX_GM_FOR_BUS:
            return { ...state, loading: true }
        case GET_DISTANCE_MATRIX_GM_FOR_BUS_SUCCESS:
            let distanceBetween = null;
            distanceBetween = _.map(action.payload, v => v.elements[0].distance.value / 1000);
            return { ...state, loading: false, distanceMatrixForBus: distanceBetween }
        case GET_DISTANCE_MATRIX_GM_FOR_BUS_FAILURE:
            return { ...state, loading: false, distanceMatrixForBus: null }
            
        case GET_PLACE_DETAIL_GM:
            return { ...state, loading: true }
        case GET_PLACE_DETAIL_GM_SUCCESS:
            return { ...state, loading: false, placeDetail: action.payload }
        case GET_PLACE_DETAIL_GM_FAILURE:
            return { ...state, loading: false, placeDetail: null }
        case GET_PLACE_DETAIL_GM_FOR_TO_LOCATION_BUS:
            return { ...state, isToLocationBus: true }

        case GET_FIND_GEOMETRY_GM:
            return { ...state, loading: true }
        case GET_FIND_GEOMETRY_GM_SUCCESS:
            return { ...state, loading: false, geometry: action.payload }
        case GET_FIND_GEOMETRY_GM_FAILURE:
            return { ...state, loading: false, geometry: null }
        default:
            return { ...state }
    }
}