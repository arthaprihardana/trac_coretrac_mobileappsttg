/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-08 11:49:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-15 15:03:28
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { TEMP_FORM, TEMP_PERSONAL_DETAIL, TEMP_DRIVER_DETAIL, TEMP_COORDINATOR_DETAIL } from '../actions/Types';

const INIT_STATE = {
    tempForm: null,
    tempPersonalDetail: null,
    tempDriverDetail: null,
    tempCoordinatorDetail: null
}

const persistConfig = {
    key: 'temporaryForm',
    storage: storage,
    whitelist: ['tempForm', 'tempPersonalDetail', 'tempDriverDetail', 'tempCoordinatorDetail']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case TEMP_FORM:
            return { ...state, tempForm: action.payload }
        case TEMP_PERSONAL_DETAIL: 
            return { ...state, tempPersonalDetail: action.payload }
        case TEMP_DRIVER_DETAIL:
            return { ...state, tempDriverDetail: action.payload }
        case TEMP_COORDINATOR_DETAIL:
            return { ...state, tempCoordinatorDetail: action.payload }
        default:
            return { ...state }
    }
});