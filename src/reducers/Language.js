/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-28 02:09:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 02:31:48
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { SET_LANGUAGE } from '../actions/Types';

const INIT_STATE = {
    lang: 'id'
}

const persistConfig = {
    key: 'language',
    storage: storage,
    whitelist: ['lang']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_LANGUAGE:
            return { ...state, lang: action.payload }
        default:
            return { ...state }
    }
})