/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-08 07:11:27 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-04-08 07:11:27 
 */
import { GET_BUFFER_IMAGE_KTP, GET_BUFFER_IMAGE_KTP_SUCCESS, GET_BUFFER_IMAGE_KTP_FAILURE, GET_BUFFER_IMAGE_SIM, GET_BUFFER_IMAGE_SIM_SUCCESS, GET_BUFFER_IMAGE_SIM_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    bufferSim: null,
    bufferKtp: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BUFFER_IMAGE_KTP:
            return { ...state, loading: true }
        case GET_BUFFER_IMAGE_KTP_SUCCESS:
            return { ...state, loading: false, bufferKtp: action.payload }
        case GET_BUFFER_IMAGE_KTP_FAILURE:
            return { ...state, loading: false, bufferKtp: null }
        case GET_BUFFER_IMAGE_SIM:
            return { ...state, loading: true }
        case GET_BUFFER_IMAGE_SIM_SUCCESS:
            return { ...state, loading: false, bufferSim: action.payload }
        case GET_BUFFER_IMAGE_SIM_FAILURE:
            return { ...state, loading: false, bufferSim: null }
        default:
            return { ...state }
    }
}