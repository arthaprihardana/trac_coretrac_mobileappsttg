/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 23:32:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-05 08:47:17
 */
import { GET_CREDIT_CARD, GET_CREDIT_CARD_SUCCESS, GET_CREDIT_CARD_FAILURE, SAVE_CREDIT_CARD, SAVE_CREDIT_CARD_SUCCESS, SAVE_CREDIT_CARD_FAILURE, UPDATE_CREDIT_CARD, UPDATE_CREDIT_CARD_SUCCESS, UPDATE_CREDIT_CARD_FAILURE, DELETE_CREDIT_CARD, DELETE_CREDIT_CARD_SUCCESS, DELETE_CREDIT_CARD_FAILURE, SET_PRIMARY_CREDIT_CARD, SET_PRIMARY_CREDIT_CARD_SUCCESS, SET_PRIMARY_CREDIT_CARD_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    cc: null,
    saveCC: null,
    updateCC: null,
    deleteCC: null,
    primaryCC: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CREDIT_CARD:
            return { ...state, loading: true }
        case GET_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, cc: action.payload }
        case GET_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, cc: null }
        case SAVE_CREDIT_CARD:
            return { ...state, loading: true }
        case SAVE_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, saveCC: action.payload }
        case SAVE_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, saveCC: null }
        case UPDATE_CREDIT_CARD:
            return { ...state, loading: true }
        case UPDATE_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, updateCC: action.payload }
        case UPDATE_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, updateCC: null }
        case DELETE_CREDIT_CARD:
            return { ...state, loading: true }
        case DELETE_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, deleteCC: action.payload }
        case DELETE_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, deleteCC: null }
        case SET_PRIMARY_CREDIT_CARD:
            return { ...state, loading: true }
        case SET_PRIMARY_CREDIT_CARD_SUCCESS:
            return { ...state, loading: false, primaryCC: action.payload }
        case SET_PRIMARY_CREDIT_CARD_FAILURE:
            return { ...state, loading: false, primaryCC: null }
        default:
            return { ...state }
    }
}