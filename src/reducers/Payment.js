/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-27 14:18:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-27 14:19:00
 */
import { GET_VIRTUAL_ACCOUNT, GET_VIRTUAL_ACCOUNT_SUCCESS, GET_VIRTUAL_ACCOUNT_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    va: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VIRTUAL_ACCOUNT:
            return { ...state, loading: true }
        case GET_VIRTUAL_ACCOUNT_SUCCESS:
            return { ...state, loading: false, va: action.payload }
        case GET_VIRTUAL_ACCOUNT_FAILURE:
            return { ...state, loading: false, va: null }
        default:
            return { ...state }
    }
}