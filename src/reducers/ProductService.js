/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 07:47:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-20 14:47:47
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import _ from 'lodash';
import { GET_PRODUCT_SERVICES, GET_PRODUCT_SERVICES_SUCCESS, GET_PRODUCT_SERVICES_FAILURE, SET_SELECTED_PRODUCT } from '../actions/Types';

const INIT_STATE = {
    loading: false,
    products: null,
    selectedProduct: null
}

const persistConfig = {
    key: 'productService',
    storage: storage,
    whitelist: ['products', 'selectedProduct']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PRODUCT_SERVICES:
            return { ...state, loading: true }
        case GET_PRODUCT_SERVICES_SUCCESS:
            let filter = _.filter(action.payload, value => {
                let reg = /^[a-z|A-Z|0-9]+[^I]\s?(6|7|8){1}$/g;
                if(reg.test(value.MsProductId)) {
                    value.icon = '';
                    switch (parseInt(value.MsProductId.substr(value.MsProductId.length - 1))) {
                        case 6:
                            // value.icon = 'icoCarRental';
                            // value.pushTo = 'CarRentalForm';
                            value.icon = 'car_rental';
                            value.pushTo = 'CarRental';
                            break;
                        case 7:
                            // value.icon = 'icoAirportTransfer';
                            // value.pushTo = 'AirportTransferForm';
                            value.icon = 'airport_transfer';
                            value.pushTo = 'AirportTransfer';
                            break;
                        case 8:
                            // value.icon = 'icoBusRental';
                            // value.pushTo = 'BusRentalForm';
                            value.icon = 'bus_rental';
                            value.pushTo = 'BusRental';
                            break;
                        default:
                            value.icon = '';
                            break;
                    }
                    return value;
                }
            });
            return { ...state, loading: false, products: filter }
        case GET_PRODUCT_SERVICES_FAILURE:
            return { ...state, loading: false, products: null }
        case SET_SELECTED_PRODUCT:
            return { ...state, selectedProduct: action.payload }
        default:
            return { ...state }
    }
})