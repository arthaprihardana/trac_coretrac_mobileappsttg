/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 14:25:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-20 14:26:48
 */
import { GET_AIRPORT_ZONE, GET_AIRPORT_ZONE_SUCCESS, GET_AIRPORT_ZONE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    zone: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_AIRPORT_ZONE:
            return { ...state, loading: true }
        case GET_AIRPORT_ZONE_SUCCESS:
            return { ...state, loading: false, zone: action.payload }
        case GET_AIRPORT_ZONE_FAILURE:
            return { ...state, loading: false, zone: null }
        default:
            return { ...state }
    }
}