/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 21:56:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-01 14:02:59
 */
import { POST_CANCEL_RESERVATION, POST_CANCEL_RESERVATION_SUCCESS, POST_CANCEL_RESERVATION_FAILURE, POST_CANCEL_RESERVATION_REFUND, POST_CANCEL_RESERVATION_REFUND_SUCCESS, POST_CANCEL_RESERVATION_REFUND_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    cancel: null,
    refund: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_CANCEL_RESERVATION:
            return { ...state, loading: true }
        case POST_CANCEL_RESERVATION_SUCCESS:
            return { ...state, loading: false, cancel: action.payload }
        case POST_CANCEL_RESERVATION_FAILURE:
            return { ...state, loading: false, cancel: null }
        case POST_CANCEL_RESERVATION_REFUND:
            return { ...state, loading: true }
        case POST_CANCEL_RESERVATION_REFUND_SUCCESS:
            return { ...state, loading: false, refund: action.payload }
        case POST_CANCEL_RESERVATION_REFUND_FAILURE:
            return { ...state, loading: false, refund: null }
        default:
            return { ...state }
    }
}