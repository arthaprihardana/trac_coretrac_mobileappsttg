/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 14:43:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-07 01:48:01
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_EXTRAS, GET_EXTRAS_SUCCESS, GET_EXTRAS_FAILURE, TEMP_EXTRAS } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    extras: null,
    tempExtras: null
};

const persistConfig = {
    key: 'masterExtras',
    storage: storage,
    whitelist: ['tempExtras']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_EXTRAS:
            return { ...state, loading: true }
        case GET_EXTRAS_SUCCESS:
            return { ...state, loading: false, extras: action.payload }
        case GET_EXTRAS_FAILURE:
            return { ...state, loading: false, extras: null }
        case TEMP_EXTRAS:
            return { ...state, tempExtras: action.payload }
        default:
            return { ...state }
    }
})