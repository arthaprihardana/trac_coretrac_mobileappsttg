/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 11:39:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-29 14:21:24
 */
import { GET_MY_BOOKING, GET_MY_BOOKING_SUCCESS, GET_MY_BOOKING_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    myBooking: null,
    totalData: null,
    currentPage: null,
    totalPage: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MY_BOOKING:
            return { ...state, loading: true }
        case GET_MY_BOOKING_SUCCESS:
            return { ...state, loading: false, myBooking: action.payload.data, totalData: action.payload.total, currentPage: action.payload.current_page, totalPage: action.payload.last_page }
        case GET_MY_BOOKING_FAILURE:
            return { ...state, loading: false, myBooking: null, totalData: null, currentPage: null, totalPage: null }
        default:
            return { ...state }
    }
}