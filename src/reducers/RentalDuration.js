/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 10:33:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-06 10:34:41
 */
import { GET_RENTAL_PACKAGE, GET_RENTAL_PACKAGE_SUCCESS, GET_RENTAL_PACKAGE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    rentalDuration: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_RENTAL_PACKAGE:
            return { ...state, loading: true }
        case GET_RENTAL_PACKAGE_SUCCESS:
            return { ...state, loading: false, rentalDuration: action.payload }
        case GET_RENTAL_PACKAGE_FAILURE:
            return { ...state, loading: false, rentalDuration: null }
        default:
            return { ...state }
    }
}