/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-04 17:28:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-06 14:03:03
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { POST_LOGIN, POST_LOGIN_SUCCESS, POST_LOGIN_FAILURE, POST_REGISTER, POST_REGISTER_SUCCESS, POST_REGISTER_FAILURE, POST_FORGOT_PASSWORD, POST_FORGOT_PASSWORD_SUCCESS, POST_FORGOT_PASSWORD_FAILURE, POST_LOGOUT, GET_REFRESH_TOKEN, GET_REFRESH_TOKEN_SUCCESS, GET_REFRESH_TOKEN_FAILURE, SET_NEW_TOKEN, SET_REMEMBER_ME, SET_NEW_USER, POST_LOGIN_SOSMED, POST_LOGIN_SOSMED_SUCCESS, POST_LOGIN_SOSMED_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    isLogin: false,
    login: null,
    register: null,
    forgotPassword: null,
    refreshToken: null,
    rememberMe: false
}

const persistConfig = {
    key: 'authentication',
    storage: storage,
    whitelist: ['login', 'isLogin', 'rememberMe']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {

        case POST_LOGIN:
            return { ...state, loading: true }
        case POST_LOGIN_SUCCESS:
            if(action.payload !== null) {
                let login_data = action.payload.login_data;
                return { ...state, loading: false, login: login_data, isLogin: true }
            } else {
                return { ...state, loading: false, login: null, isLogin: false }
            }
        case POST_LOGIN_FAILURE:
            return { ...state, loading: false, login: action.payload, isLogin: false }
        case POST_LOGIN_SOSMED:
            return { ...state, loading: true }
        case POST_LOGIN_SOSMED_SUCCESS:
            if(action.payload !== null) {
                let login_data = action.payload.login_data;
                return { ...state, loading: false, login: login_data, isLogin: true }
            } else {
                return { ...state, loading: false, login: null, isLogin: false }
            }
        case POST_LOGIN_SOSMED_FAILURE:
            return { ...state, loading: false, login: action.payload, isLogin: false }
        case POST_REGISTER:
            return { ...state, loading: true }
        case POST_REGISTER_SUCCESS:
            return { ...state, loading: false, register: action.payload } 
        case POST_REGISTER_FAILURE:
            return { ...state, loading: false, register: "Email has been register" }

        case POST_FORGOT_PASSWORD:
            return { ...state, loading: true }
        case POST_FORGOT_PASSWORD_SUCCESS:
            return { ...state, loading: false, forgotPassword: action.payload }
        case POST_FORGOT_PASSWORD_FAILURE:
            return { ...state, loading: false, forgotPassword: null }

        case POST_LOGOUT:
            return { ...state, isLogin: false, login: null }

        case GET_REFRESH_TOKEN:
            return { ...state, loading: true }
        case GET_REFRESH_TOKEN_SUCCESS:
            return { ...state, loading: false, refreshToken: action.payload }
        case GET_REFRESH_TOKEN_FAILURE:
            return { ...state, loading: false, refreshToken: { token : null } }

        case SET_NEW_TOKEN:
            return { ...state, login: action.payload }

        case SET_REMEMBER_ME:
            return { ...state, rememberMe: action.payload }
        
        case SET_NEW_USER:
            return { ...state, login: action.payload }
            
        default:
            return { ...state }
    }
})