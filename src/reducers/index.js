/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-28 10:20:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 02:10:01
 */
import { combineReducers } from 'redux';
import IsSkipWelcomeScreen from './IsSkipWelcomeScreen';
import AuthenticationReducer from './Authentication';
import ProductServiceReducer from './ProductService';
import CityCoverageReducer from './CityCoverage';
import AddressFromGoogleReducer from './AddressFromGoogle';
import RentalPackageReducer from './RentalDuration';
import MasterCarTypeReducer from './MasterCarType';
import MasterExtrasReducer from './Extras';
import StockReducer from './Stock';
import PriceReducer from './Price';
import TempFormReducer from './TempForm';
import NotesReducer from './Notes';
import BankReducer from './Bank';
import PromoReducer from './Promo';
import ReservationReducer from './Reservation';
import AirportCoverageReducer from './AirportCoverage';
import AirportZoneReducer from './AirportZone';
import MasterBranchReducer from './MasterBranch';
import BusZoneReducer from './BusZone';
import AdjustmentTimeReducer from './AdjustmentTime';
import CaptureCameraReducer from './Camera';
import PaymentReducer from './Payment';
import MyBookingReducer from './MyBooking';
import VehicleAttributeReducer from './VehicleAttribute';
import MyHistoryReducer from './MyHistory';
import CancelReservationReducer from './CancelReservation';
import UserReducer from './User';
import CancelReasonReducer from './CancelReason';
import CreditCardReducer from './CreditCard';
import ImageBufferReducer from './ImageBuffer';
import ContactUsReducer from './ContactUs';
import ContentArticleReducer from './ContentArticle';
import LanguageReducer from './Language';

const reducers = combineReducers({
    isSkipWelcomeScreen: IsSkipWelcomeScreen,
    authentication: AuthenticationReducer,
    productService: ProductServiceReducer,
    cities: CityCoverageReducer,
    addressFromGoogle: AddressFromGoogleReducer,
    rentalPackage: RentalPackageReducer,
    masterCarType: MasterCarTypeReducer,
    masterExtras: MasterExtrasReducer,
    stockList: StockReducer,
    priceList: PriceReducer,
    temporaryForm: TempFormReducer,
    tempNotes: NotesReducer,
    masterBank: BankReducer,
    discountPromo: PromoReducer,
    reservationSave: ReservationReducer,
    airportCoverage: AirportCoverageReducer,
    airportZone: AirportZoneReducer,
    masterBranch: MasterBranchReducer,
    busZone: BusZoneReducer,
    adjustmentTime: AdjustmentTimeReducer,
    camera: CaptureCameraReducer,
    payment: PaymentReducer,
    booking: MyBookingReducer,
    vehicleAttribute: VehicleAttributeReducer,
    history: MyHistoryReducer,
    cancelReservation: CancelReservationReducer,
    userProfile: UserReducer,
    cancelReason: CancelReasonReducer,
    creditCard: CreditCardReducer,
    imageBuffer: ImageBufferReducer,
    contactUs: ContactUsReducer,
    contentArticle: ContentArticleReducer,
    language: LanguageReducer
});

export default reducers;
