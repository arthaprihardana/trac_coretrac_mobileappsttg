/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-27 10:57:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-04 09:03:00
 */
import { SET_CAPTURE_CAMERA, OPEN_CAMERA_FOR_KTP, OPEN_CAMERA_FOR_SIM, OPEN_CAMERA_FOR_PROFILE, CAPTURE_FOR_KTP, CAPTURE_FOR_SIM, CAPTURE_FOR_PROFILE, CLEAR_CAPTURE_FOR_ALL } from "../actions/Types";

const INIT_STATE = {
    capture: null,
    captureKtp: null,
    captureSim: null,
    captureProfile: null,
    openCameraKtp: false,
    openCameraSim: false,
    openCameraProfile: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_CAPTURE_CAMERA:
            return { ...state, capture: action.payload }
        case OPEN_CAMERA_FOR_KTP:
            return { ...state, openCameraKtp: action.payload }
        case OPEN_CAMERA_FOR_SIM:
            return { ...state, openCameraSim: action.payload }
        case OPEN_CAMERA_FOR_PROFILE:
            return { ...state, openCameraProfile: action.payload }
        case CAPTURE_FOR_KTP:
            return { ...state, captureKtp: action.payload }
        case CAPTURE_FOR_SIM:
            return { ...state, captureSim: action.payload }
        case CAPTURE_FOR_PROFILE:
            return { ...state, captureProfile: action.payload }
        case CLEAR_CAPTURE_FOR_ALL:
            return { ...state, captureKtp: null, captureSim: null, captureProfile: null }
        default:
            return { ...state }
    }
}