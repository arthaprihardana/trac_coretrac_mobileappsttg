/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-01 09:29:23 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-04-01 09:29:23 
 */
import { GET_CANCEL_REASON, GET_CANCEL_REASON_SUCCESS, GET_CANCEL_REASON_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    reason: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CANCEL_REASON:
            return { ...state, loading: true }
        case GET_CANCEL_REASON_SUCCESS:
            return { ...state, loading: false, reason: action.payload }
        case GET_CANCEL_REASON_FAILURE:
            return { ...state, loading: false, reason: null }
        default:
            return { ...state }
    }
}