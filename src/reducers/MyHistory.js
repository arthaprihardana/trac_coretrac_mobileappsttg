/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 16:20:17 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-31 16:20:17 
 */
import { GET_MY_HISTORY, GET_MY_HISTORY_SUCCESS, GET_MY_HISTORY_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    myHistory: null,
    totalData: null,
    currentPage: null,
    totalPage: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MY_HISTORY:
            return { ...state, loading: true }
        case GET_MY_HISTORY_SUCCESS:
            return { ...state, loading: false, myHistory: action.payload.data, totalData: action.payload.total, currentPage: action.payload.current_page, totalPage: action.payload.last_page }
        case GET_MY_HISTORY_FAILURE:
            return { ...state, loading: false, myHistory: null, totalData: null, currentPage: null, totalPage: null }
        default:
            return { ...state }
    }
}