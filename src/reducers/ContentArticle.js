/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 14:33:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-22 14:47:46
 */
import { GET_CONTENT_ARTICLE, GET_CONTENT_ARTICLE_SUCCESS, GET_CONTENT_ARTICLE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    articles: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CONTENT_ARTICLE:
            return { ...state, loading: true }
        case GET_CONTENT_ARTICLE_SUCCESS:
            return { ...state, loading: false, articles: action.payload }
        case GET_CONTENT_ARTICLE_FAILURE:
            return { ...state, loading: false, articles: null }
        default:
            return { ...state }
    }
}