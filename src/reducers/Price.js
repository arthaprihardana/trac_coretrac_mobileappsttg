/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 02:14:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-26 21:32:16
 */
import { GET_PRICE, GET_PRICE_SUCCESS, GET_PRICE_FAILURE, GET_PRICE_FOR_BUS, GET_PRICE_FOR_BUS_SUCCESS, GET_PRICE_FOR_BUS_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false, 
    price: null,
    priceForBus: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PRICE:
            return { ...state, loading: true }
        case GET_PRICE_SUCCESS:
            return { ...state, loading: false, price: action.payload }
        case GET_PRICE_FAILURE:
            return { ...state, loading: false, price: null }
        case GET_PRICE_FOR_BUS:
            return { ...state, loading: true }
        case GET_PRICE_FOR_BUS_SUCCESS:
            return { ...state, loading: false, priceForBus: action.payload }
        case GET_PRICE_FOR_BUS_FAILURE:
            return { ...state, loading: false, priceForBus: null }
        default:
            return { ...state }
    }
}