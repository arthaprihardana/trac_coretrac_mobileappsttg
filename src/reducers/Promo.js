/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 11:28:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 13:04:06
 */
import { GET_VALIDATE_PROMO, GET_VALIDATE_PROMO_SUCCESS, GET_VALIDATE_PROMO_FAILURE, IS_PROMO_VALID, SET_TOTAL_PRICE_AFTER_DISCOUNT, GET_PROMO, GET_PROMO_SUCCESS, GET_PROMO_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    discount: null,
    isPromoValid: false,
    totalPriceAfterDiscount: 0,
    promo: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VALIDATE_PROMO:
            return { ...state, loading: true }
        case GET_VALIDATE_PROMO_SUCCESS:
            return { ...state, loading: false, discount: action.payload }
        case GET_VALIDATE_PROMO_FAILURE:
            return { ...state, loading: false, discount: null }
        case IS_PROMO_VALID:
            return { ...state, isPromoValid: action.payload }
        case SET_TOTAL_PRICE_AFTER_DISCOUNT:
            return { ...state, totalPriceAfterDiscount: action.payload }
        case GET_PROMO:
            return { ...state, loading: true }
        case GET_PROMO_SUCCESS:
            return { ...state, loading: false, promo: action.payload }
        case GET_PROMO_FAILURE:
            return { ...state, loading: false, promo: null }
        default:
            return { ...state }
    }
}