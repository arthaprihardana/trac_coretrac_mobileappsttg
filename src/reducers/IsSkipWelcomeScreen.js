/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 13:18:01 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-19 17:40:41
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { IS_SKIP } from '../actions/Types';

const INIT_STATE = {
    isSkip: false,
    // goTo: null
}

export default persistReducer({
    key: 'isSkipWelcomeScreen',
    storage: storage,
    whitelist: ['isSkip']
}, ( state = INIT_STATE, action) => {
    switch (action.type) {
        case IS_SKIP:
            return { ...state, isSkip: action.payload }
            // return { ...state, isSkip: action.payload.skip, goTo: action.payload.goTo }
        default:
            return { ...state }
    }
});