/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 13:38:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-07 16:34:04
 */
import _ from "lodash";
import { GET_MASTER_CAR_TYPE, GET_MASTER_CAR_TYPE_SUCCESS, GET_MASTER_CAR_TYPE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    carType: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_CAR_TYPE:
            return { ...state, loading: true }
        case GET_MASTER_CAR_TYPE_SUCCESS:
            let carType = _.map(action.payload, v => {
              return {
                label: v.Name,
                value: v.CarTypeId,
              }
            })
            return { ...state, loading: false, carType: carType }
        case GET_MASTER_CAR_TYPE_FAILURE:
            return { ...state, loading: false, carType: null }
        default:
            return { ...state }
    }
}