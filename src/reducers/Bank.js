/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 15:10:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-31 22:34:37
 */
import { GET_MASTER_BANK, GET_MASTER_BANK_SUCCESS, GET_MASTER_BANK_FAILURE, GET_ALL_BANK, GET_ALL_BANK_SUCCESS, GET_ALL_BANK_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    bank: null,
    allBank: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_BANK:
            return { ...state, loading: true }
        case GET_MASTER_BANK_SUCCESS:
            return { ...state, loading: false, bank: action.payload }
        case GET_MASTER_BANK_FAILURE:
            return { ...state, loading: false, bank: null }
        case GET_ALL_BANK:
            return { ...state, loading: true }
        case GET_ALL_BANK_SUCCESS:
            return { ...state, loading: false, allBank: action.payload }
        case GET_ALL_BANK_FAILURE:
            return { ...state, loading: false, allBank: null }
        default:
            return { ...state }
    }
}