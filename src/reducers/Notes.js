/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 10:13:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-14 10:14:21
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { SET_NOTES } from '../actions/Types';
const INIT_STATE = {
    notes: null
}

const persistConfig = {
    key: 'tempNotes',
    storage: storage,
    whitelist: ['notes']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_NOTES:
            return { ...state, notes: action.payload }
        default:
            return { ...state }
    }
})