/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-19 10:18:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-20 13:39:46
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import _ from "lodash";
import { GET_AIRPORT_COVERAGE, GET_AIRPORT_COVERAGE_SUCCESS, GET_AIRPORT_COVERAGE_FAILURE, GET_MASTER_AIRPORT, GET_MASTER_AIRPORT_SUCCESS, GET_MASTER_AIRPORT_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    airport: null,
    airportCityCoverage: null
}

const persistConfig = {
    key: 'airportCoverage',
    storage: storage,
    whitelist: ['airport']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_AIRPORT:
            return { ...state, loading: true }
        case GET_MASTER_AIRPORT_SUCCESS:
            let airport = _.map(_.filter(action.payload, v => v.Airport !== "-"), v => {
                return {
                label: `${v.CityName} (${v.Airport})`,
                value: v.Code
                }
            });
            return { ...state, loading: false, airport: airport }
        case GET_MASTER_AIRPORT_FAILURE:
            return { ...state, loading: false, airport: null }
        case GET_AIRPORT_COVERAGE:
            return { ...state, loading: true }
        case GET_AIRPORT_COVERAGE_SUCCESS:
            return { ...state, loading: false, airportCityCoverage: action.payload }
        case GET_AIRPORT_COVERAGE_FAILURE:
            return { ...state, loading: false, airportCityCoverage: null }
        default:
            return { ...state }
    }
});