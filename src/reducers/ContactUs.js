/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 22:34:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-20 22:36:34
 */
import { POST_CONTACT_US, POST_CONTACT_US_SUCCESS, POST_CONTACT_US_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    sendContactUs: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_CONTACT_US:
            return { ...state, loading: true }
        case POST_CONTACT_US_SUCCESS:
            return { ...state, loading: false, sendContactUs: action.payload }
        case POST_CONTACT_US_FAILURE:
            return { ...state, loading: false, sendContactUs: null }
        default:
            return { ...state }
    }
}