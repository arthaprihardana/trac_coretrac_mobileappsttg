/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 15:25:09 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-29 15:25:09 
 */
import { GET_VEHICLE_ATTRIBUTE, GET_VEHICLE_ATTRIBUTE_SUCCESS, GET_VEHICLE_ATTRIBUTE_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    vehicle: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VEHICLE_ATTRIBUTE:
            return { ...state, loading: false }
        case GET_VEHICLE_ATTRIBUTE_SUCCESS:
            return { ...state, loading: false, vehicle: action.payload }
        case GET_VEHICLE_ATTRIBUTE_FAILURE:
            return { ...state, loading: false, vehicle: null }
        default:
            return { ...state }
    }
}