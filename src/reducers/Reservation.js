/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 14:38:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 14:31:11
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { POST_RESERVATION_SAVE, POST_RESERVATION_SAVE_SUCCESS, POST_RESERVATION_SAVE_FAILURE, SET_SUMMARY, GET_RESERVATION_DETAIL, GET_RESERVATION_DETAIL_SUCCESS, GET_RESERVATION_DETAIL_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    summary: null,
    reservation: null,
    detailReservation: null
};

const persistConfig = {
    key: 'reservationSave',
    storage: storage,
    whitelist: ['summary']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_SUMMARY:
            return { ...state, summary: action.payload }
        case POST_RESERVATION_SAVE:
            return { ...state, loading: true }
        case POST_RESERVATION_SAVE_SUCCESS:
            return { ...state, loading: false, reservation: action.payload }
        case POST_RESERVATION_SAVE_FAILURE:
            return { ...state, loading: false, reservation: null }
        case GET_RESERVATION_DETAIL:
            return { ...state, loading: true }
        case GET_RESERVATION_DETAIL_SUCCESS:
            return { ...state, loading: false, detailReservation: action.payload }
        case GET_RESERVATION_DETAIL_FAILURE:
            return { ...state, loading: false, detailReservation: null }
        default:
            return { ...state }
    }
})