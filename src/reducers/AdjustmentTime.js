/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-25 14:00:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-25 14:02:45
 */
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { GET_ADJUSTMENT_TIME, GET_ADJUSTMENT_TIME_SUCCESS, GET_ADJUSTMENT_TIME_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    time: null
}

const persistConfig = {
    key: 'adjustmentTime',
    storage: storage,
    whitelist: ['time']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ADJUSTMENT_TIME:
            return { ...state, loading: true }
        case GET_ADJUSTMENT_TIME_SUCCESS:
            return { ...state, loading: false, time: action.payload }
        case GET_ADJUSTMENT_TIME_FAILURE:
            return { ...state, loading: false, time: null}
        default:
            return { ...state }
    }
})