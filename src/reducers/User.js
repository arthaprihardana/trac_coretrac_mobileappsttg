/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-01 07:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-04 23:46:14
 */
import { PUT_USER_PROFILE, PUT_USER_PROFILE_SUCCESS, PUT_USER_PROFILE_FAILURE, GET_USER_PROFILE, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE, PUT_CHANGE_PASSWORD, PUT_CHANGE_PASSWORD_SUCCESS, PUT_CHANGE_PASSWORD_FAILURE, GET_CREDIT_CARD, GET_CREDIT_CARD_SUCCESS, GET_CREDIT_CARD_FAILURE } from "../actions/Types";

const INIT_STATE = {
    loading: false,
    updateUser: null,
    user: null,
    changePassword: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PUT_USER_PROFILE:
            return { ...state, loading: true }
        case PUT_USER_PROFILE_SUCCESS:
            return { ...state, loading: false, updateUser: action.payload }
        case PUT_USER_PROFILE_FAILURE:
            return { ...state, loading: false, updateUser: null }
        case GET_USER_PROFILE:
            return { ...state, loading: true }
        case GET_USER_PROFILE_SUCCESS:
            return { ...state, loading: false, user: action.payload }
        case GET_USER_PROFILE_FAILURE:
            return { ...state, loading: false, user: null }
        case PUT_CHANGE_PASSWORD:
            return { ...state, loading: true }
        case PUT_CHANGE_PASSWORD_SUCCESS:
            return { ...state, loading: false, changePassword: action.payload }
        case PUT_CHANGE_PASSWORD_FAILURE:
            return { ...state, loading: false, changePassword: null }
        default:
            return { ...state }
    }
}