import React from 'react';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { appVariables } from '../assets/styles';
import SvgIcon from '../utils/SvgIcon';

// ONBOARDING
import Onboarding from '../containers/Onboarding';

// AUTH
import ForgotPassword from '../containers/Explore/AirportTransfer/screens/ForgotPassword';
import ResetPassword from '../containers/Explore/AirportTransfer/screens/ResetPassword';
// import ForgotPassword from '../containers/Auth/ForgotPassword';
import Login from '../containers/Explore/AirportTransfer/screens/Login';
// import Login from '../containers/Auth/Login';

import Register from '../containers/Explore/AirportTransfer/screens/Register';
// import Register from '../containers/Auth/Register';
import Otp from '../containers/Explore/AirportTransfer/screens/Otp';
// import Otp from '../containers/Auth/Otp';

// EXPLORE
import Explore from '../containers/Explore';

// BUS RENTAL
import BusRental from '../containers/Explore/AirportTransfer/screens/BusRentalForm';
// import BusRental from '../containers/Explore/BusRental/BusRentalSelectOptions';

import FindBus from '../containers/Explore/BusRental/FindBus';
import OrderDetailBusRental from '../containers/Explore/BusRental/OrderDetailBusRental';
import OrderLoginBusRental from '../containers/Explore/BusRental/OrderLoginBusRental';
import PickupDetail from '../containers/Explore/BusRental/PickupDetail';
import BusRentalPaymentInfo from '../containers/Explore/BusRental/PaymentInfo';
import BusRentalPaymentPage from '../containers/Explore/BusRental/PaymentPage';
import BusRentalPaymentFinish from '../containers/Explore/BusRental/PaymentFinish';

//CAR RENTAL
import CarRental from '../containers/Explore/AirportTransfer/screens/CarRentalForm';
// import CarRental from '../containers/Explore/CarRental/CarrentalSelectOptions';

import FindCar from '../containers/Explore/CarRental/Findcar';
import OrderDetail from '../containers/Explore/CarRental/OrderDetail';
import OrderLogin from '../containers/Explore/CarRental/OrderLogin';
import OrderReviewPayment from '../containers/Explore/CarRental/OrderReviewPayment';
import PaymentPage from '../containers/Explore/CarRental/PaymentPage';
import PaymentFinish from '../containers/Explore/CarRental/PaymentFinish';
import PaymentInfo from '../containers/Explore/CarRental/PaymentInfo';
import PaymentInstruction from '../containers/Explore/CarRental/PaymentInstruction';
import PaymentVirtualAccount from '../containers/Explore/CarRental/PaymentVirtualAccount';
import PersonalDriverInfo from '../containers/Explore/CarRental/PersonalDriverInfo';
import DriverInfo from '../containers/Explore/CarRental/DriverInfo';
import DriverInformation from '../containers/Explore/AirportTransfer/screens/DriverInformation';
import CreditCardInformation from '../containers/Explore/AirportTransfer/screens/CreditCardInformation';
import PickupDetails from '../containers/Explore/AirportTransfer/screens/PickupDetails';
import RegistrationSuccess from '../containers/Explore/AirportTransfer/screens/RegistrationSuccess';
import SeatAvailability from '../containers/Explore/AirportTransfer/screens/SeatAvailability';
import CarDetail from '../containers/Explore/AirportTransfer/screens/CarDetail';

// AIRPORT TRANSFER
import AirportTransfer from '../containers/Explore/AirportTransfer/screens/AirportTransferForm';
import CarList from '../containers/Explore/AirportTransfer/screens/CarList';
import BusList from '../containers/Explore/AirportTransfer/screens/BusList';
import BookedCarDetail from '../containers/Explore/AirportTransfer/screens/BookedCarDetail';
import BookedBusDetail from '../containers/Explore/AirportTransfer/screens/BookedBusDetail';
import PassangerDetail from '../containers/Explore/AirportTransfer/screens/PassangerDetail';
import Payment from '../containers/Explore/AirportTransfer/screens/Payment';
import PaymentSuccess from '../containers/Explore/AirportTransfer/screens/PaymentSuccess';
import FlightDetail from '../containers/Explore/AirportTransfer/screens/FlightDetail';

// MY BOOKING
import MyBooking from '../containers/MyBooking';

// NOTIFICATION
import Notification from '../containers/Notification';

// MORE
import More from '../containers/More';

//Profile
import Profile from '../containers/Profile';
import Account from '../containers/Profile/Account';
import IdentificationInformation from '../containers/Profile/IdentificationInformation';
import CreditCard from '../containers/Profile/CreditCard';
import ChangePassword from '../containers/Profile/ChangePassword';
import HistoryBooking from '../containers/Profile/HistoryBooking';
import Message from '../containers/Profile/Message';

const OnboardingRoutes = createStackNavigator(
  {
    Onboarding: {
      screen: Onboarding,
    },
  },
  {
    initialRouteName: 'Onboarding',
    headerMode: 'none',
  }
)

// const AuthRoutes = createStackNavigator(
//   {
//     // Login: {
//     //   screen: Login,
//     // },
//     // ForgotPassword: {
//     //   screen: ForgotPassword
//     // },
//     // Otp: {
//     //   screen: Otp
//     // },
//     // Register: {
//     //   screen: Register
//     // }
//   },
//   // {
//   //   initialRouteName: 'Login',
//   //   headerMode: 'none',
//   // }
// )

const CarRentalRoutes = createStackNavigator(
  {
    // CarRental: {
    //   screen: CarRental
    // },
    // OrderDetail: {
    //   screen: OrderDetail
    // },
    // FindCar: {
    //   screen: FindCar
    // },
    // OrderReviewPayment: {
    //   screen: OrderReviewPayment
    // },
    // OrderLogin: {
    //   screen: OrderLogin
    // },
    PaymentInfo: {
      screen: PaymentInfo
    },
    PaymentPage: {
      screen: PaymentPage
    },
    PaymentVirtualAccount: {
      screen: PaymentVirtualAccount
    },
    PaymentInstruction: {
      screen: PaymentInstruction
    },
    PersonalDriverInfo: {
      screen: PersonalDriverInfo
    },
    PaymentFinish: {
      screen: PaymentFinish
    }
  },
  {
    headerMode: 'none',
  }
);

const BusRentalRoutes = createStackNavigator(
  {
    // BusRental: {
    //   screen: BusRental
    // },
    FindBus: {
      screen: FindBus
    },
    OrderDetailBusRental: {
      screen: OrderDetailBusRental
    },
    OrderLoginBusRental: {
      screen: OrderLoginBusRental
    },
    PickupDetail: {
      screen: PickupDetail
    },
    BusRentalPaymentInfo: {
      screen: BusRentalPaymentInfo
    },
    BusRentalPaymentPage: {
      screen: BusRentalPaymentPage
    },
    BusRentalPaymentFinish: {
      screen: BusRentalPaymentFinish
    }
  },
  {
    headerMode: 'none',
  }
);

const AirportTransferRoutes = createStackNavigator(
  {
    AirportTransfer: {
      screen: AirportTransfer
    },
    CarRental: {
      screen: CarRental
    },
  },
  {
    headerMode: 'none',
  }
);

/* Semua routing yang ada di tab EXPLORE */
const ExploreRoutes = createStackNavigator(
  {
    ExploreLanding: {
      screen: Explore,
    },
    AirportTransferForm: {
      screen: AirportTransfer,
    },
    CarRentalForm: {
      screen: CarRental,
    },
    BusRentalForm: {
      screen: BusRental,
    },
    CarList: {
      screen: CarList,
    },
    BusList: {
      screen: BusList,
    },
    BookedCarDetail: {
      screen: BookedCarDetail,
    },
    BookedBusDetail: {
      screen: BookedBusDetail,
    },
    Login: {
      screen: Login,
    },
    PassangerDetail: {
      screen: PassangerDetail,
    },
    Payment: {
      screen: Payment,
    },
    Register: {
      screen: Register,
    },
    PaymentSuccess: {
      screen: PaymentSuccess,
      // navigationOptions: {
      //   gesturesEnabled: false,
      // }
    },
    ForgotPassword: {
      screen: ForgotPassword,
    },
    Otp: {
      screen: Otp,
    },
    ResetPassword: {
      screen: ResetPassword,
    },
    FlightDetail: {
      screen: FlightDetail,
    },
    DriverInformation:{
      screen:DriverInformation
    },
    CreditCardInformation:{
      screen:CreditCardInformation,
    },
    PickupDetails:{
      screen:PickupDetails,
    },
    RegistrationSuccess:{
      screen:RegistrationSuccess,
    },
    SeatAvailability:{
      screen:SeatAvailability,
    },
    CarDetail:{
      screen:CarDetail,
    }
    // CarRentalLanding: {
    //   screen: CarRentalRoutes,
    // },
    // BusRentalLanding: {
    //   screen: BusRentalRoutes,
    // },
    // AirportTransferLanding: {
    //   screen: AirportTransferRoutes,
    // }
  },
  {
    headerMode: 'none',
    initialRouteName: 'ExploreLanding'
  }
)

const ProfileRoutes = createStackNavigator(
  {
    ProfileLanding:{
      screen:Profile
    },
    AccountLanding: {
      screen: Account
    },
    IdentificationInformationLanding: {
      screen: IdentificationInformation
    },
    CreditCardLanding: {
      screen: CreditCard
    },
    ChangePasswordLanding: {
      screen: ChangePassword,
    },
    HistoryBookingLanding: {
      screen: HistoryBooking
    },
    MessageLanding: {
      screen: Message
    }
  },
  {
    headerMode: 'none',
    initialRouteName: 'ProfileLanding'
  }
)

/* Hide tab bar only on specific page */
ExploreRoutes.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


/* Main routing yang ada di bottom tab */
const TabNavigator = createMaterialBottomTabNavigator(
  {
    Explore: {
      screen: ExploreRoutes,
      navigationOptions: {
        title: 'EXPLORE',
        gesturesEnabled: false,
      }
    },
    MyBooking: {
      screen: MyBooking,
      navigationOptions: {
        title: 'MY BOOKING',
      }
    },
    // Notification: {
    //   screen: Notification,
    //   navigationOptions: {
    //     title: 'NOTIFICATION',
    //   }
    // },
    Profile: {
      screen: ProfileRoutes,
      navigationOptions: {
        title: 'PROFILE',
      }
    },
    More: {
      screen: More,
      navigationOptions: {
        title: 'MORE',
      }
    }
  },
  {
    labeled: true,
    shifting: false,
    initialRouteName: 'Explore',
    activeColor: appVariables.colorWhite,
    inactiveColor: appVariables.colorTabInactive,
    navigationOptions: ({navigation}) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Explore') {
          iconName = `icoExplore${focused ? 'Active' : ''}`;
        } else if (routeName === 'MyBooking') {
          iconName = `icoMyBooking${focused ? 'Active' : ''}`;
        } else if (routeName === 'Profile') {
          iconName = `icoNotification${focused ? 'Active' : ''}`;
        } else if (routeName === 'More') {
          iconName = `icoMore${focused ? 'Active' : ''}`;
        }

        return <SvgIcon name={iconName} />
      },
    }),
    barStyle: { 
      backgroundColor: appVariables.colorDark 
    },
  }
);

const AppNavigator = createSwitchNavigator({
  // BusRental,
  // PassangerDetail,
  // PaymentSuccess,
  // PickupDetail,
  // ProfileRoutes,
  // RegistrationSuccess,
  // SeatAvailability,
  // CreditCardInformation,
  // DriverInformation,
  // CarDetail,
  OnboardingRoutes,
  // AirportTransferRoutes,
  OnboardingRoutes,
  // CarRentalRoutes,
  // AuthRoutes,
  TabNavigator,
})

export default AppNavigator;
