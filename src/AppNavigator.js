/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-12 11:58:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 14:43:43
 */
import React, { Component } from "react";
import {
  Actions,
  Router,
  Scene,
  Tabs,
	Stack,
	Reducer
} from "react-native-router-flux";
import { YellowBox, Platform, BackHandler, ToastAndroid } from 'react-native';
import { connect } from "react-redux";
// import firebase from 'react-native-firebase';
// import type { Messaging } from 'react-native-firebase';
// import type { Notification, NotificationOpen } from 'react-native-firebase';
import { isSkipWelcomeScreen, getReservationDetail } from "./actions";

// MAIN
import Home from "@airport-transfer-screens/Home";
// import Onboarding from "@airport-transfer-screens/Onboarding";
import Onboarding from "./containers/Onboarding";

// AUTH
import Login from "@airport-transfer-screens/Login";
import Register from "@airport-transfer-screens/Register";
import CreditCardInformation from "@airport-transfer-screens/CreditCardInformation";
import ForgotPassword from "@airport-transfer-screens/ForgotPassword";
import Otp from "@airport-transfer-screens/Otp";
import ResetPassword from "@airport-transfer-screens/ResetPassword";

// VEHICLE 
import CarRental from "@airport-transfer-screens/CarRentalForm";
import AirportTransfer from "@airport-transfer-screens/AirportTransferForm";
import BusRental from "@airport-transfer-screens/BusRentalForm";
import CarList from "@airport-transfer-screens/CarList";
import BusList from "@airport-transfer-screens/BusList";
import BookedCarDetail from "@airport-transfer-screens/BookedCarDetail";
import BookedBusDetail from "@airport-transfer-screens/BookedBusDetail";

// BOOKING DATA
import PassangerDetail from "@airport-transfer-screens/PassangerDetail";
import DriverInformation from "@airport-transfer-screens/DriverInformation";
import CoordinatorInformation from "@airport-transfer-screens/CoordinatorInformation";
import FlightDetail from "@airport-transfer-screens/FlightDetail";
import PickupDetail from "@airport-transfer-screens/PickupDetail";

// PAYMENT
import Payment from "@airport-transfer-screens/Payment";
import PaymentSuccess from "@airport-transfer-screens/PaymentSuccess";
import CreditCard from "@airport-transfer-screens/Payment/CreditCard";
import VirtualAccountSelected from "@airport-transfer-screens/Payment/VirtualAccountSelected";

//PROFILE
import Account from "@airport-transfer-screens/Account";
import ChangePassword from "@airport-transfer-screens/ChangePassword";
import ProfileCreditCard from "@airport-transfer-screens/ProfileCreditCard";
import ViewCreditCard from "@airport-transfer-screens/ViewCreditCard";
import HistoryBooking from "@airport-transfer-screens/HistoryBooking";
import IdentificationInformation from "@airport-transfer-screens/IdentificationInformation";
import Message from "@airport-transfer-screens/Message";
import Profile from "@airport-transfer-screens/Profile";
import DeleteCreditCardConfirmation from "@airport-transfer-screens/DeleteCreditCardConfirmation";
import CarDetail from "@airport-transfer-screens/CarDetail";
import DriverReview from "@airport-transfer-screens/DriverReview";
import AddCreditCard from "@airport-transfer-screens/AddCreditCard";
import CancelBookingList from "@airport-transfer-screens/CancelBookingList";
import CancelBookingReason from "@airport-transfer-screens/CancelBookingReason";
import CancelBookingRefund from "@airport-transfer-screens/CancelBookingRefund";
import CancelBookingConfirmation from "@airport-transfer-screens/CancelBookingConfirmation";
import MyVoucher from "@airport-transfer-screens/MyVoucher";
import MessageDetail from "@airport-transfer-screens/MessageDetail";
import FAQ from "@airport-transfer-screens/FAQ";

//MY BOOKING
import MyBooking from "@airport-transfer-screens/MyBooking";

//MORE
import More from "@airport-transfer-screens/More";
import ContactUs from "@airport-transfer-screens/ContactUs";
import OutletLocation from "@airport-transfer-screens/OutletLocation";
import AboutUs from "@airport-transfer-screens/AboutUs";
import PageNotification from "@airport-transfer-screens/Notification";
import PrivacyPolicy from "@airport-transfer-screens/PrivacyPolicy";
import TNC from "@airport-transfer-screens/TNC";
import NewsEvent from "@airport-transfer-screens/NewsEvent";
import NewsDetail from "@airport-transfer-screens/NewsDetail";
import Promo from "@airport-transfer-screens/Promo";
import PromoDetail from "@airport-transfer-screens/PromoDetail";
import ProductService from "@airport-transfer-screens/ProductService";
import ProductServiceDetail from "@airport-transfer-screens/ProductServiceDetail";
import CallCenter from "@airport-transfer-screens/CallCenter";

import MGaleri from '@airport-transfer-screens/MGaleri';

YellowBox.ignoreWarnings(['Warning: ...']);

const reducerCreate = params => {
	const defaultReducer = new Reducer(params);
	return (state, action) => {
			return defaultReducer(state, action);
	};
};
let exitCount = 0;
const onBackPress = () => {
	let route = Actions.state.routes;
	let topSection = route[route.length - 1]
	console.log('topSection ==>', topSection);
	switch (topSection.routeName) {
		case "Home":
			exitCount++;
			ToastAndroid.showWithGravityAndOffset(
					'Tekan back 1x lagi untuk keluar',
					ToastAndroid.SHORT,
					ToastAndroid.BOTTOM,
					25,
					100
			);
			if(exitCount == 2) {
					exitCount = 0
					BackHandler.exitApp()
			}
			break;
		case "More":
			Actions.reset("Home");
			break;
		case "MyBooking":
			Actions.reset("Home");
			break;
		case "Profile":
			Actions.reset("Home");
			break;
		case "PassangerDetail":
			Actions.popTo("CarList");
			break;
		default:
			Actions.pop();
			break;
	}
	return true;
}

class AppNavigator extends Component {
	
  	constructor() {
		super();
	}

	componentDidMount = () => {
		if(this.props.isSkip) {
			Actions.replace("Home");
		}
	}

	componentDidUpdate = (prevProps, prevState) => {
		if(this.props.isSkip !== prevProps.isSkip) {
			if(this.props.isSkip) {
				Actions.replace("Home");
			}
		}	
	}
	
  	render() {
		return (
		<Router createReducer={reducerCreate} backAndroidHandler={onBackPress} >
			<Scene key="root" hideNavBar={true}>

				{/* MAIN */}
				<Scene key="Onboarding" component={Onboarding} initial/>
				<Scene key="Home" component={Home} />

				{/* AUTH */}
				<Scene key="Login" component={Login}/>
				<Scene key="Register" component={Register}/>
				<Scene key="CreditCardInformation" component={CreditCardInformation}/>
				<Scene key="ForgotPassword" component={ForgotPassword}/>
				<Scene key="Otp" component={Otp}/>
				<Scene key="ResetPassword" component={ResetPassword}/>

				{/* VEHICLE LIST */}
				<Scene key="CarRental" component={CarRental}/>
				<Scene key="AirportTransfer" component={AirportTransfer}/>
				<Scene key="BusRental" component={BusRental}/>
				<Scene key="CarList" component={CarList}/>
				<Scene key="BusList" component={BusList}/>
				<Scene key="BookedCarDetail" component={BookedCarDetail}/>
				<Scene key="BookedBusDetail" component={BookedBusDetail}/>
				
				{/* BOOKING DATA */}
				<Scene key="PassangerDetail" component={PassangerDetail}/>
				<Scene key="DriverInformation" component={DriverInformation}/>
				<Scene key="CoordinatorInformation" component={CoordinatorInformation} />
				<Scene key="FlightDetail" component={FlightDetail}/>
				<Scene key="PickupDetail" component={PickupDetail}/>
				
				{/* PAYMENT */}
				<Scene key="Payment" component={Payment}/>
				<Scene key="PaymentSuccess" component={PaymentSuccess}/>
				<Scene key="VirtualAccountSelected" component={VirtualAccountSelected}/>

				{/* MY BOOKING */}
				<Scene key="MyBooking" component={MyBooking}/>
				
				{/*PROFILE */}
				<Scene key="Profile" component={Profile}/>
				<Scene key="Account" component={Account}/>
				<Scene key="ChangePassword" component={ChangePassword}/>
				<Scene key="ProfileCreditCard" component={ProfileCreditCard}/>
				<Scene key="ViewCreditCard" component={ViewCreditCard} />
				<Scene key="HistoryBooking" component={HistoryBooking}/>
				<Scene key="IdentificationInformation" component={IdentificationInformation}/>
				<Scene key="Message" component={Message}/>
				<Scene key="AddCreditCard" component={AddCreditCard}/>
				<Scene key="DeleteCreditCardConfirmation" component={DeleteCreditCardConfirmation}/>
				<Scene key="CarDetail" component={CarDetail}/>
				<Scene key="DriverReview" component={DriverReview}/>
				<Scene key="CancelBookingList" component={CancelBookingList}/>
				<Scene key="CancelBookingReason" component={CancelBookingReason}/>
				<Scene key="CancelBookingRefund" component={CancelBookingRefund}/>
				<Scene key="CancelBookingConfirmation" component={CancelBookingConfirmation}/>
				<Scene key="CreditCard" component={CreditCard}/>
				<Scene key="MyVoucher" component={MyVoucher}/>
				<Scene key="MessageDetail" component={MessageDetail}/>
				<Scene key="FAQ" component={FAQ}/>
				{/* MORE */}
				<Scene key="More" component={More}/>
				<Scene key="PrivacyPolicy" component={PrivacyPolicy}/>
				<Scene key="Notification" component={PageNotification}/>
				<Scene key="AboutUs" component={AboutUs} />
				<Scene key="OutletLocation" component={OutletLocation} />
				<Scene key="ContactUs" component={ContactUs} />
				<Scene key="TNC" component={TNC} />
				<Scene key="NewsEvent" component={NewsEvent} />
				<Scene key="NewsDetail" component={NewsDetail} />
				<Scene key="Promo" component={Promo} />
				<Scene key="PromoDetail" component={PromoDetail} />
				<Scene key="ProductService" component={ProductService} />
				<Scene key="ProductServiceDetail" component={ProductServiceDetail} />
				<Scene key="CallCenter" component={CallCenter} />

				<Scene key="Galeri" component={MGaleri} />
			</Scene>
		</Router>
		);
	}

}

const mapStateToProps = ({ isSkipWelcomeScreen }) => {
	const { isSkip } = isSkipWelcomeScreen;
  	return { isSkip };
}

export default connect(mapStateToProps, {
	isSkipWelcomeScreen,
	getReservationDetail
})(AppNavigator);
