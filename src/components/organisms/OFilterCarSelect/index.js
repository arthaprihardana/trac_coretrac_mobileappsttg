import React, {Component} from 'react';
import {View, Text} from 'react-native';
import MBlueRadio from '../../molecules/MBlueRadio';
import PropTypes from 'prop-types';
import { appVariables } from '../../../assets/styles';

class OFilterCarSelect extends Component {
    state = {
        brands: []
    }

    handleSelect = (id, brandSelect) => {
        let newBrandSelect = [...this.state.brands];
        let indexBrandSelect = newBrandSelect.findIndex(brand => brand.labelBrand === brandSelect);

        let newCarSelect = [...newBrandSelect[indexBrandSelect].cars];
        let indexCarSelect = newCarSelect.findIndex(car => car.id === id);
        
        newCarSelect[indexCarSelect].selected = !newCarSelect[indexCarSelect].selected;
        this.setState({
            brands: newBrandSelect
        })
    }

    componentDidMount(){
        this.setState({
            brands: this.props.listBrandCars
        })
    }

    render(){
        let listBrandShow = this.state.brands.map((brand,i) => {
            let carsShow = brand.cars.map((car,j) => {
                return (
                    <View key={j} style={{marginBottom: 20}}>
                        <MBlueRadio touchText label={car.name} active={car.selected} onPress={() => this.handleSelect(car.id, brand.labelBrand)}  />
                    </View>
                )
            })
            return (
                <View key={i}>
                    <Text style={{fontFamily: appVariables.museo900, marginBottom: 20}}>{brand.labelBrand.toUpperCase()}</Text>
                    {carsShow}
                </View>
            )
        }) 
        return (
            <View>
                {listBrandShow}
            </View>
        )
    }
}

OFilterCarSelect.propTypes = {
    listBrandCars: PropTypes.array.isRequired,

}

export default OFilterCarSelect;