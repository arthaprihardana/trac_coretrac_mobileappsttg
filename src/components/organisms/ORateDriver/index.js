import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import MainButton from "@airport-transfer-fragments/MainButton";
import { appVariables } from '@variables';

class ORateDriver extends Component {
  render() {
    let rateDriverImage = require("../../../assets/images/png/rate-driver-popup.png");
    let starsRate = require("../../../assets/images/png/stars-rate.png");
    let { action } = this.props;
    let { modalWrapper, rateDriverImageStyle, textWrapper, askRateStyle, pleaseRateStyle, starsRateStyle, rateButtonStyle } = styles;

    return (
      <View style={{ backgroundColor: 'rgba(0,0,0,0.7)',flex:1}}>
        <View style={modalWrapper}>
          <Image source={rateDriverImage} style={rateDriverImageStyle}></Image>
          <View style={textWrapper}>
            <Text style={askRateStyle}>How was your ride today, Bryan?</Text>
            <Text style={pleaseRateStyle}>Please rate your experience today to make us better. Thank you.</Text>
          </View>
          <Image source={starsRate} style={starsRateStyle}></Image>
          <MainButton
            label={"RATE"}
            onPress={action}
            style={{ paddingVertical:10,height: 50, width: 200, alignItems: "center", justifyContent: "center"}}
            rounded>
          </MainButton>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalWrapper: {
    height: '60%',
    marginLeft: 20,
    marginRight: 20,
    marginTop:120,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    padding : 20,
    borderRadius: 10
  },
  rateDriverImageStyle: {
    width: 150,
    height: 150
  },
  textWrapper: {
    alignItems: 'center'
  },
  askRateStyle: {
    marginTop: 20,
    fontSize: 18,
    color: '#2A2E36',
    maxWidth: 285,
    textAlign: 'center'
  },
  pleaseRateStyle: {
    paddingVertical: 17,
    fontSize: 12,
    color: '#8A959E',
    maxWidth: 213,
    textAlign: 'center'
  },
  starsRateStyle: {
    paddingVertical:10,
    height: 40,
    width: 220,
    alignItems: 'center',
    flex:-1,
  }
})

export default ORateDriver;