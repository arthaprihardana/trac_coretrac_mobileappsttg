import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { AButton } from '../../../components/atoms';
import { appVariables } from '../../../assets/styles';

class OModalAction extends Component {
    render() {
        let notifyImg = require("../../../assets/images/png/avatar/notify.png");
        let {action} = this.props;
        let {modalWrapper, contentWrapper, avatarSectionWrapper, avatarWrapper, avatarStyle, titleStyle, descWrapper, descStyle} = styles;
        return (
            <View style={modalWrapper}>
                <View style={contentWrapper}>
                    <View style={avatarSectionWrapper}>
                        <View style={avatarWrapper}>
                            <Image source={notifyImg} style={avatarStyle} />
                        </View>
                    </View>
                    <Text style={titleStyle}>Notify Me</Text>
                    <View style={descWrapper}>
                        <Text style={descStyle}>Enter your e-mail address so we can report as soon as possible if this service is available</Text>
                    </View>
                    {this.props.children}
                    <AButton primary block onPress={action}>
                        SUBMIT
                    </AButton>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    modalWrapper: { height: '100%' },
    contentWrapper: { backgroundColor: 'white', position: 'absolute', bottom: 0, width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8, padding: 20 },
    avatarSectionWrapper: { alignItems: 'center', marginTop: 20 },
    avatarWrapper: { width: 121, height: 121 },
    avatarStyle: { width: undefined, height: undefined, flex: 1, resizeMode: 'contain' },
    titleStyle: { marginTop: 25, marginBottom: 14, fontSize: 24, fontFamily: appVariables.museo700, color: '#2A2E36', textAlign: 'center' },
    descWrapper: { alignItems: 'center' },
    descStyle: { fontSize: 12, fontFamily: appVariables.museo500, color: '#2A2E36', maxWidth: 276, textAlign: "center", marginBottom: 51 },
})

export default OModalAction;