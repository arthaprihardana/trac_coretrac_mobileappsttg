import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { appVariables } from '../../../assets/styles';
import PropTypes from 'prop-types';

class ORadioCircle extends Component {
    state = {
        activeId: 1
    }
    handleSelect = (id) => {
        this.setState({
            activeId: id
        }, ()=> {
            this.props.onChange(id)
        })
    }
    componentDidMount(){
        this.setState({
            activeId: this.props.defaultId
        })
    }
    render() {
        let { maxNumb } = this.props
        let showButton = []
        for (let i = 1; i <= maxNumb; i++) {
            showButton.push(
                <View key={i} style={{ width: '20%', marginBottom: 20 }}>
                    <TouchableOpacity onPress={() => this.handleSelect(i)}>
                        <View style={[styles.circleStyle, this.state.activeId === i ? styles.activeStyle : null]}>
                            <Text style={[styles.textStyle, this.state.activeId === i ? styles.activeText : null]}>{i}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }

        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {showButton}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    circleStyle: { width: 35, height: 35, backgroundColor: 'white', borderRadius: 100, justifyContent: 'center', alignItems: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 6 },
    activeStyle: {backgroundColor: '#1F419B'},
    textStyle: { fontSize: 12, fontFamily: appVariables.museo700, color: '#2A2E36' },
    activeText: {color: 'white'}
})

ORadioCircle.propTypes = {
    maxNumb: PropTypes.number,
    defaultId: PropTypes.number,
    onChange: PropTypes.func
}

export default ORadioCircle;