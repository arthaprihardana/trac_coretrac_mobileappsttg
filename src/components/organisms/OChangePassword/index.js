/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 22:56:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-04 22:57:49
 */
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import MainButton from "@airport-transfer-fragments/MainButton";
import { appMetrics } from '../../../assets/styles'
import { connect } from "react-redux";

class OChangePassword extends Component {
  render() {
    let changePasswordImage = require("../../../assets/images/png/change-password-popup.png")
    let { action } = this.props;
    let { modalWrapper, contentWrapper, avatarSectionWrapper, avatarWrapper, avatarStyle, descWrapper, descStyle } = styles;
    return (
      <View style={{ backgroundColor: 'rgba(0,0,0,0.7)', height: '100%' }}>
        <View style={modalWrapper}>
          <View style={contentWrapper}>
            <View style={avatarSectionWrapper}>
              <View style={avatarWrapper}>
                <Image source={changePasswordImage} style={avatarStyle} />
                <View style={descWrapper}>
                  <Text style={descStyle}>{this.props.login.Data.FirstName} {this.props.login.Data.LastName}, your password have successfully changed</Text>
                </View>
                <MainButton
                  label={"DONE"}
                  onPress={action}
                  style={{borderRadius:10}}>
                </MainButton>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalWrapper: { height: (appMetrics.screenHeight-200), justifyContent: 'center', alignItems: 'center' },
  contentWrapper: { backgroundColor: 'white', position: 'absolute', bottom: 0, width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8, padding: 20, borderRadius: 10 },
  avatarSectionWrapper: { alignItems: 'center', marginTop: 20 },
  avatarWrapper: { width: 300, height: 300 },
  avatarStyle: { width: undefined, height: undefined, flex: 1, resizeMode: 'contain' },
  descWrapper: { alignItems: 'center' },
  descStyle: { marginTop: 20, fontSize: 16, color: '#2A2E36', maxWidth: 276, textAlign: "center", marginBottom: 20 },
});

const mapStateToProps = ({ authentication }) => {
  const { login } = authentication;
  return { login }
}

export default connect(mapStateToProps, {})(OChangePassword);
