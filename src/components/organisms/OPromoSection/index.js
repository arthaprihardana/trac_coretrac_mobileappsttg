import React, { Component } from 'react';
import { View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import MSection from '../../molecules/MSection';
import { Button, Text } from 'native-base';
import Carousel from 'react-native-snap-carousel'; 
import { appMetrics } from '../../../assets/styles';
import Mask from 'react-native-mask';

const promoImage = require('../../../assets/images/png/promo/promo1.png');
const promoData = [
  { promoImage },
  { promoImage },
  { promoImage }
]

class OPromoSection extends Component {
  _renderItem({item, index}) {
    return (
      <Mask shape={'rounded'}>
        <TouchableHighlight key={index} style={styles.contentContainer}>
          <Image source={item.promoImage} style={styles.promoImage} resizeMode='cover' />
        </TouchableHighlight>
      </Mask>
    );
  }

  render() {
    return(
      <MSection title="Promo" padder={false}>
        <Carousel
          data={promoData}
          renderItem={this._renderItem}
          sliderWidth={appMetrics.windowWidth}
          itemWidth={appMetrics.windowWidth - 50}
          activeSlideAlignment='start'
          containerCustomStyle={{paddingLeft: 20, marginBottom: 20}}
          slideStyle={{marginRight: 10}}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
        />
        <View style={styles.buttonWrapper}>
          <Button bordered dark block><Text>SEE ALL DEALS</Text></Button>
        </View>
      </MSection>
    )
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    height: 190,
    width: appMetrics.windowWidth,
  },
  promoImage: {
    width: appMetrics.windowWidth - 40, 
    height: 190
  },
  buttonWrapper: {
    paddingLeft: 20, 
    paddingRight: 20, 
    paddingBottom: 10,
  }
})

export default OPromoSection;