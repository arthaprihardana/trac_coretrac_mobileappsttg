import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Button, Container, Content, Col, Row, Grid, Text, H1 } from 'native-base';
import { appStyles, appMetrics, appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';

import ASquareButton from '../../atoms/ASquareButton';
import ASmallButtonWithIcon from '../../atoms/ASmallButtonWithIcon';
import MHeader from '../../molecules/MHeader';
import AFullScreenImage from '../../atoms/AFullScreenImage';
import { withNavigation } from 'react-navigation';

const landingBanner = require("../../../assets/images/png/landing-banner.png");

class OHomeBanner extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.mainBanner}>
        <AFullScreenImage image={landingBanner} />

        {/* HOME HEADER */}
        <View style={styles.homeBanner}>
          <MHeader />
        </View>
        
        {/* HOME BANNER CONTENT */}
        <View style={styles.homeContent}>
          <H1 style={[styles.h1, appStyles.bold, appStyles.applyFont]}>Explore & Discover with TRAC To Go</H1>
          <Text style={styles.p}>Customize your booking car based on your needs, we got them covered.</Text>

          <View style={styles.homeButtons}>
            <Grid>
              <ASquareButton label="Car Rental" icon="icoCarRental" bgColor="#2246A8" onPress={() => this.props.carRentalClicked()} />
              <ASquareButton label="Airport Transfer" icon="icoAirportTransfer" bgColor="#1F419B" onPress={() => {}} />
              <ASquareButton label="Bus Rental" icon="icoBusRental" bgColor="#1A3681" onPress={() => {}} />
            </Grid>
          </View>

          {/* <View style={appStyles.mt20}>
            <ASmallButtonWithIcon label="MORE" iconName="icoDownArrow" />
          </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainBanner: {
    width: appMetrics.windowWidth,
    // height: appMetrics.windowHeight - 60,
    height: 610,
    flex: 1,
    overflow: 'hidden'
  },
  homeBanner: {
    flex: 2
  },
  homeContent: {
    flex: 2,
    padding: 20
  },
  homeButtons: {
    height: 110,
    marginTop: 20,
  },
  h1: {
    color: appVariables.colorWhite,
    marginBottom: 10
  },
  p: {
    color: appVariables.colorWhite
  }
});

export default withNavigation(OHomeBanner);