import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';

const ThisListOrderCar = ({imgUri, active, onPress}) => {
    let {carWrapper, carImgWrapper, carImg, activeCarStyle} = styles;
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={[carWrapper, active ? activeCarStyle : null]}>
                <View style={carImgWrapper}>
                    <Image source={imgUri} style={carImg} />
                </View>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    carWrapper: { width: 87, height: 50, backgroundColor: 'white', padding: 6, marginHorizontal: 5, borderRadius: 4 },
    activeCarStyle: {backgroundColor: '#E8ECF0'},
    carImgWrapper: { flex: 1 },
    carImg: { flex: 1, width: undefined, height: undefined, resizeMode: 'contain' },
})

ThisListOrderCar.propTypes = {
    imgUri: PropTypes.any,
    active: PropTypes.bool,
    onPress: PropTypes.func
}

export default ThisListOrderCar;
