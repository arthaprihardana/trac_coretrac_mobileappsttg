import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { ThisListOrderCar } from './thisComponent';

class OCarShowDetail extends Component {
    state = {
        cars: []
    }

    componentDidMount(){
        this.setState({
            cars: this.props.listCar
        })
    }

    selectCar = (selectedCar) => {
        let listCar = [...this.state.cars];
        listCar.map(car => {
            car.isActive = false
            if(car.id === selectedCar.id){
                car.isActive = true
            }
        })
        this.setState({
            cars: listCar
        })
        this.props.onChange(listCar);
    }

    render() {
        let {orderCarWrapper} = styles;
        let showCars = null;
        if(this.state.cars.length > 0){
            showCars = this.state.cars.map((car, i) => {
                return <ThisListOrderCar key={i} active={car.isActive} onPress={() => this.selectCar(car)} imgUri={car.img} />
            })
        }
        return (
            <View style={orderCarWrapper}>
                {showCars}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    orderCarWrapper: { padding: 10, flexDirection: 'row', justifyContent: 'center', backgroundColor: 'white' }
})

export default OCarShowDetail;
