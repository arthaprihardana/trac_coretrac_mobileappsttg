import React, { Component } from 'react';
import MSection from '../../molecules/MSection';
import MBlogCard from '../../molecules/MBlogCard';

const blogData = [
  {
    image: require('../../../assets/images/png/blog/example1.png'),
    title: 'Lorem Ipsum dolor sit amet sid ut ima amet lorem',
    postDate: 'Jul 6, 2017'
  },
  {
    image: require('../../../assets/images/png/blog/example2.png'),
    title: 'Lorem Ipsum dolor sit amet sid ut ima amet lorem',
    postDate: 'Jul 6, 2017'
  },
  {
    image: require('../../../assets/images/png/blog/example3.png'),
    title: 'Lorem Ipsum dolor sit amet sid ut ima amet lorem',
    postDate: 'Jul 6, 2017'
  }
]

class OBlogSection extends Component {
  render() {
    let blogs = [];

    const blogList = blogData.map((item, index) => {
      return (
        <MBlogCard 
          key={index}
          image={item.image}
          title={item.title}
          postDate={item.postDate}
        />
      )
    });

    return (
      <MSection title="Blog">
        {blogList}
      </MSection>
    );
  }
}

export default OBlogSection;