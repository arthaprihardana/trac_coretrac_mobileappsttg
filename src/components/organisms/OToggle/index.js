import React, { Component } from 'react';
import { AToggle } from '../../atoms';
import PropTypes from 'prop-types';

class OToggle extends Component {
    state = {
        status: false
    }
    toggleChange = () => {
        this.setState({
            status: !this.state.status
        }, () => {
            if (this.props.onChange) this.props.onChange(this.state.status)
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.defaultValue !== undefined) {
            this.setState({ status: nextProps.defaultValue })
        }
    }

    componentDidMount() {
        if (this.props.defaultValue) {
            this.setState({
                status: this.props.defaultValue
            })
        }
    }
    render() {
        let { status } = this.state;
        return (
            <AToggle isOn={status} onPress={this.toggleChange} />
        )
    }
}

OToggle.propTypes = {
    onChange: PropTypes.func,
    defaultValue: PropTypes.bool
}

export default OToggle;