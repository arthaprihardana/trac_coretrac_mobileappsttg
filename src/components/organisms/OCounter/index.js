import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { MCounter } from '../../molecules';

class OCounter extends Component {
    state = {
        value: 0
    }

    changeValue = (newValue) => {
        this.setState({
            value: newValue
        }, () => {
            if (this.props.onChange) return this.props.onChange(newValue)
        })
    }

    handleMinus = () => {
        if (this.state.value > 0) this.changeValue(this.state.value - 1)
    }

    handlePlus = () => this.changeValue(this.state.value + 1)

    componentWillReceiveProps(nextProps) {
        if(nextProps.defaultValue !== undefined){
            this.setState({ value: nextProps.defaultValue });
        }
    }

    componentDidMount() {
        if (this.props.defaultValue) {
            this.setState({
                value: this.props.defaultValue
            })
        }
    }

    render() {
        let { value } = this.state;
        return (
            <MCounter value={value} minusAction={this.handleMinus} plusAction={this.handlePlus} positionFooter={this.props.positionFooter} />
        )
    }
}

OCounter.propTypes = {
    onChange: PropTypes.func,
    positionFooter: PropTypes.bool,
    defaultValue: PropTypes.number
}

export default OCounter;