import React, { Component } from 'react';
import MMessageCard from '../../molecules/MMessageCard';
import MMessageSection from '../../molecules/MMessageSection';

const messageData =[
  {
    title:'Promo 17 Agustus diskon 17% untuk Semua Service',
    description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum',
    postDate:'12 Agustus 2018'
  },
  {
    title:'Promo 17 Agustus diskon 17% untuk Semua Service',
    description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum',
    postDate:'12 Agustus 2018'
  },
  {
    title:'Promo 17 Agustus diskon 17% untuk Semua Service',
    description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum',
    postDate:'12 Agustus 2018'
  }
]
class OMessageSection extends Component{
  render(){
    let messages =[];
    
    const messageList = messageData.map((item,index) => {
      return(
        <MMessageCard
          key={index}
          title={item.title}
          description={item.description}
          postDate={item.postDate}
        />
      )
    });

      return(
        <MMessageSection title="Message">
          {messageList}
        </MMessageSection>

      );    
  }
}


export default OMessageSection;

