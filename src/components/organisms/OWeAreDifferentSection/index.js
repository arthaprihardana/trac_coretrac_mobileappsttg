import React, { Component } from 'react';
import { View, StyleSheet} from 'react-native';
import MSection from '../../molecules/MSection';
import { Button, Text } from 'native-base';
import SvgIcon from '../../../utils/SvgIcon';
import Carousel from 'react-native-snap-carousel'; 
import appMetrics from '../../../assets/styles/appMetrics';
import appVariables from '../../../assets/styles/appVariables';

const articleData = [
  {
    iconName: 'icoLaunch',
    title: 'Fast booking process',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.'
  },
  {
    iconName: 'icoLaunch',
    title: 'Car Subtitution',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.'
  },
  {
    iconName: 'icoLaunch',
    title: 'Car Rental Anytime',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.'
  }
];

class OWeAreDifferentSection extends Component {
  _renderItem({item, index}) {
    return (
      <View>
        <SvgIcon name={item.iconName} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.p}>{item.description}</Text>
      </View>
    )
  }

  render() {
    return (
      <MSection title="We Are Different" padder={false}>
        <Carousel
          data={articleData}
          renderItem={this._renderItem}
          sliderWidth={appMetrics.windowWidth}
          itemWidth={appMetrics.windowWidth - 120}
          activeSlideAlignment='start'
          containerCustomStyle={{paddingLeft: 20}}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
        />
      </MSection>
    )
  }
}

const styles = StyleSheet.create({
  p: {
    color: '#8A959E',
    lineHeight: 24
  },
  title: {
    color: '#2A2E36',
    fontFamily: appVariables.museo700,
    marginBottom: 10,
    marginTop: 10
  }
})

export default OWeAreDifferentSection;