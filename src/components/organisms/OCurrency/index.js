import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { ACurrencyItem } from '../../atoms';

class OCurrency extends Component {
    state = {
        currency: []
    }

    changeIdCurrency = (id) => {
        let newCurrency = [...this.state.currency];
        newCurrency.map(cty => {
            cty.selected = false
            if (cty.id === id) cty.selected = true
        })
        return newCurrency
    }

    selected = (country) => {
        this.setState({
            currency: this.changeIdCurrency(country.id)
        }, () => {
            this.props.onChange(this.changeIdCurrency(country.id))
        })
    }

    componentDidMount() {
        this.setState({
            currency: this.props.currencyList
        })
    }

    render() {
        let { currencyStyle } = styles;
        let showListCurrency = this.state.currency.map((country, i) => {
            return <ACurrencyItem key={i} flagUri={country.img} label={country.label} active={country.selected} onPress={() => this.selected(country)} />
        })
        return (
            <View style={currencyStyle}>
                {showListCurrency}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    currencyStyle: { position: 'absolute', top: 155, right: 20, zIndex: 100, borderWidth: 1, borderColor: '#C3C8D3', borderRadius: 3, paddingHorizontal: 12, backgroundColor: 'white' }
})

export default OCurrency;