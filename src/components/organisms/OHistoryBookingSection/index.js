/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 17:06:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-31 17:09:35
 */
import React, { Component } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import MHistoryBookingCard from '../../molecules/MHistoryBookingCard';
import MHistoryBookingSection from '../../molecules/MHistoryBookingSection';
import { connect } from "react-redux";

const historyBookingData =[
  {
   bookingStatus:'Completed',
   image:require('../../../assets/images/png/cars/Avanza.png'),
   totalCars:'2 Cars',
   bookingNumber:'A09E82093U',
   startDate:'12 Sep 2018',
   endDate:'15 Sep 2018'
  },
  {
   bookingStatus:'Cancelled',
   image:require('../../../assets/images/png/cars/Avanza.png'),
   totalCars:'2 Cars',
   bookingNumber:'A09E82093U',
   startDate:'12 Sep 2018',
   endDate:'15 Sep 2018'
  },
  {
   bookingStatus:'Completed',
   image:require('../../../assets/images/png/cars/Avanza.png'),
   totalCars:'2 Cars',
   bookingNumber:'A09E82093U',
   startDate:'12 Sep 2018',
   endDate:'15 Sep 2018'
  }
]
class OHistoryBookingSection extends Component{
  render(){
    const historyBookingCardList = historyBookingData.map((item,index) => {
      return(
        <MHistoryBookingCard
          key={index}
          bookingStatus={item.bookingStatus}
          image={item.image}
          totalCars={item.totalCars}
          bookingNumber={item.bookingNumber}
          startDate={item.startDate}
          endDate={item.endDate}   
        />
      )
    });

    return(
      <MHistoryBookingSection title="HistoryBooking">
        {historyBookingCardList}
      </MHistoryBookingSection>

    );    
  }
}

const mapStateToProps = ({ history }) => {
  const { loading, myHistory } = history;
  return { loading, myHistory };
}

export default connect(mapStateToProps, {})(OHistoryBookingSection);

