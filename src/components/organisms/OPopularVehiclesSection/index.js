import React, { Component } from 'react';
import { View, StyleSheet, Image} from 'react-native';
import MSection from '../../molecules/MSection';
import { Button, Text, Card, Body, CardItem, Grid, Col } from 'native-base';
import MCardPopularCar from '../../molecules/MCardPopularCar';
import Carousel from 'react-native-snap-carousel'; 
import appMetrics from '../../../assets/styles/appMetrics';

const vehiclesData = [
  {
    carImage: require('../../../assets/images/png/cars/toyota-alphard.png'),
    carName: 'Toyota Alphard',
    rentPrice: 'Rp. 680,000'
  },
  {
    carImage: require('../../../assets/images/png/cars/toyota-alphard.png'),
    carName: 'Toyota Kijang Innova',
    rentPrice: 'Rp. 400,000'
  },
  {
    carImage: require('../../../assets/images/png/cars/toyota-alphard.png'),
    carName: 'Toyota Avanza',
    rentPrice: 'Rp. 372,000'
  }
];

class OPopularVehiclesSection extends Component {
  _renderItem({item, index}) {
    return(
      <MCardPopularCar 
        carImage={item.carImage}
        carName={item.carName}
        rentPrice={item.rentPrice}
      />
    )
  }

  render() {
    return (
      <MSection title="Popular Vehicles" padder={false}>
        <Carousel
          data={vehiclesData}
          renderItem={this._renderItem}
          sliderWidth={appMetrics.windowWidth}
          itemWidth={appMetrics.windowWidth - 40}
          activeSlideAlignment='start'
          containerCustomStyle={{paddingLeft: 20}}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
        />
      </MSection>
    )
  }
}

export default OPopularVehiclesSection;