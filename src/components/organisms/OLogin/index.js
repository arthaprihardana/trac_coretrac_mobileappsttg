import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { Body, Card, CardItem, Form, Input, Item, Label, Left, Radio, Right, Text, Icon } from 'native-base';
import { withNavigation } from 'react-navigation';
import appVariables from '../../../assets/styles/appVariables';
import AButton from '../../atoms/AButton';
import ACircleIconButton from '../../atoms/ACircleIconButton';
import ASeparator from '../../atoms/ASeparator';
import SvgIcon from '../../../utils/SvgIcon';

class OLogin extends React.Component {
  render() {
    let {onLogin} = this.props;
    return (
      <Form>
        <Card style={styles.card}>
          <CardItem style={styles.wrapper}>
            <Body style={styles.cardBody}>
              <Item floatingLabel>
                <Label>Email Address</Label>
                <Input />
              </Item>
              
              <ASeparator />
              <ASeparator />
              
              <Item floatingLabel>
                <Label>Password</Label>
                <Input secureTextEntry={true}/>
                {/* <Icon name="eye" /> */}
              </Item>

              <ASeparator />
              
              <View style={styles.inlineWrapper}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')} style={styles.buttonForgotPassword}><Text style={styles.smallText}>Forgot Password?</Text></TouchableOpacity>
              </View>

              <ASeparator />
              <ASeparator />
              
              <AButton primary block onPress={onLogin}>LOGIN</AButton>
              
              <ASeparator />
              
              <View style={styles.orContainer}><Text style={styles.orText}>or with</Text></View>
              
              <ASeparator />
              
              <View style={styles.socmedButtonWrapper}>
                <ACircleIconButton iconName={'icoFacebook'} bgColor={'#2C4F7E'} />
                {/* <ACircleIconButton iconName={'icoTwitter'} bgColor={'#46ABEC'} /> */}
                <ACircleIconButton iconName={'icoGplus'} bgColor={'#AE2920'} />
              </View>
            </Body>
          </CardItem>
        </Card>

        <View style={styles.bottomText}>
          <Text style={styles.teks}>Don't have an account yet?</Text> 
          <TouchableOpacity style={styles.link} onPress={() => {this.props.navigation.navigate('Register')}}>
            <Text style={styles.linkText}>Register here</Text>
          </TouchableOpacity>
        </View>
      </Form>
    )
  }
}

const styles = StyleSheet.create({
  link: {
    alignSelf: 'center',
    marginLeft: 5,
  },
  teks: {
    alignSelf:'center',
    fontSize: 14,
  },
  linkText: {
    color: appVariables.colorLink,
    fontFamily: appVariables.museo700,
    fontSize: 14,
  },
  bottomText: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 30,
  },
  orText: {
    color: appVariables.colorGray
  },
  orContainer: {
    alignSelf: 'center', 
    flex: 1
  },
  buttonForgotPassword: {
    marginTop: 0, 
  },
  smallText: {
    fontSize: 14,
    fontFamily: appVariables.museo700,
    color: appVariables.colorLink
  },
  inlineWrapper: {
    flex: 1, 
    flexDirection: 'row',
  },
  buttonRememberMe: {
    marginLeft: 8, 
    marginTop: 10,
  },
  socmedButtonWrapper: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  wrapper: {
    padding: appVariables.padding
  },
  loginHeader: {
    alignSelf: 'center',
    fontFamily: appVariables.museo700,
    marginBottom: 20,
  },
  card: {
    paddingBottom: appVariables.padding
  },
  cardBody: {
    paddingTop: 40,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
})

export default withNavigation(OLogin);