/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-05 20:56:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 14:08:49
 */
import React, { Component } from 'react';
import { ActivityIndicator } from "react-native";
import firebase from 'react-native-firebase';
import { connect } from "react-redux";
import _ from "lodash";
import moment from 'moment';
import 'moment/locale/id';
import MNotificationCard from '../../molecules/MNotificationCard';
import MMessageSection from '../../molecules/MMessageSection';
import { Actions } from 'react-native-router-flux';

class ONotificationSection extends Component{

  state = {
    data: [],
    isLoading: false
  }

  componentDidMount() {
    if(this.props.isLogin) {
      var retrieveFirebaseData = firebase.database().ref(this.props.login.Data.Id);
      console.log('retrieveFirebaseData==>', retrieveFirebaseData);
      this.setState({ isLoading : true })
      retrieveFirebaseData.on('value', snapshot => {
        let data = snapshot.val();
        console.log('data ==>', data);
        if(data !== null) {
          let arr = [];
          let cnt = 0;
          _.mapValues(data, (val, key) => {
              cnt++;
              arr.push({
                  id: key,
                  data: val
              });
              if(cnt === Object.keys(data).length) {
                this.setState({
                    data: arr,
                    isLoading: false
                });
              }
          });
        } else {
            this.setState({
                data: [],
                isLoading: false
            })
        }
      })
    }
  }
  

  render(){
    const messageList = this.state.data.map((item,index) => {
      return(
        <MNotificationCard
          key={index}
          item={item}
          onPress={() => Actions.push("MessageDetail", { item: item })}
        />
      )
    });

    return(
      <MMessageSection title="Message">
        { this.state.isLoading && <ActivityIndicator size="small" color="#2A2E36" style={{ marginTop: 20 }} /> }
        {messageList}
      </MMessageSection>
    );
  }
}

const mapStateToProps = ({ authentication }) => {
  const { isLogin, login } = authentication;
  return { isLogin, login }
}

export default connect(mapStateToProps, {})(ONotificationSection);

