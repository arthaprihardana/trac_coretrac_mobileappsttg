import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { H1 } from 'native-base';
import appVariables from '../../../assets/styles/appVariables';

class MTitle extends Component {
  _renderDescription() {
    if (this.props.description) {
      return <View style={[styles.desc, this.getAlign]}>{this.props.description}</View>
    }
  }

  getAlign = () => {
    let { left, center } = this.props;
    let alignIt = styles.centerIt;

    if (left) {alignIt = styles.centerIt;}
    if (center) {alignIt = styles.centerIt;}

    return alignIt;
  }

  render() {
    return (
      <View style={styles.headerWrapper}>
        <H1 style={[styles.header, this.getAlign]}>{this.props.title}</H1>
        {this._renderDescription()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  centerIt: {alignSelf: 'center'},
  leftIt: {alignSelf: 'flex-start'},
  headerWrapper: {
    marginBottom: 25
  },
  header: {
    fontFamily: appVariables.museo700,
    marginBottom: 10
  },
  desc: {
    fontSize: 12,
    flex: 1,
    justifyContent: 'center',
    color: appVariables.colorDarkBlue,
  }
})

export default MTitle;