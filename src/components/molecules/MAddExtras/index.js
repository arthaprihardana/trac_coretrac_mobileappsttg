import { Button, Input, Text } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';

class MAddExtras extends Component {
  render = () => {
    let bottomContent = <SvgIcon name="icoAddExtras" />;
    if (this.props.selected === true) {
      bottomContent = (
        <View style={styles.activeBottom}>
          <SvgIcon name="icoAddExtrasActive" />
        </View>
      );
    }

    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
        <View style={styles.topContainer}>
          <SvgIcon name={this.props.icoName} />
          <Text style={styles.label}>{this.props.label}</Text>
        </View>
        <View style={styles.bottomContainer}>
          { bottomContent }
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: appVariables.colorWhite,
    height: 195,
    flex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    flexDirection: 'column',
    marginHorizontal: 4,
  },
  activeBottom: {
    backgroundColor: appVariables.colorBlueHeader,
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  label: {
    fontFamily: appVariables.museo900,
    fontSize: 11,
    textAlign: 'center',
    marginTop: 30
  },
  topContainer: {
    flex: 2.5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  bottomContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: appVariables.colorBorderColor,
    borderTopWidth: 1
  }
});

export default MAddExtras;