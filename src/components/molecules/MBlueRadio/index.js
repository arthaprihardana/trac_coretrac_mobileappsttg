import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';

const BlueRadioText = ({ active, onPress, label }) => {
  let comp = <View style={styles.blueRadio}></View>;
  if (active === true) {
    comp = (
      <View style={[styles.blueRadio, { backgroundColor: appVariables.colorBlueHeader }]}>
        <SvgIcon name="icoOk" />
      </View>
    );
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.radioContainer}>
        {comp}
        <Text style={{ fontFamily: appVariables.museo500 }}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}

const BlueRadio = ({ active, onPress, label }) => {
  let comp = <TouchableOpacity onPress={onPress}><View style={styles.blueRadio}></View></TouchableOpacity>;

  if (active === true) {
    comp = (
      <TouchableOpacity onPress={onPress} style={[styles.blueRadio, { backgroundColor: appVariables.colorBlueHeader }]}>
        <SvgIcon name="icoOk" />
      </TouchableOpacity>
    );
  }
  return (
    <View style={styles.radioContainer}>
      {comp}
      {label}
    </View>
  );
}

class MBlueRadio extends React.Component {
  render() {
    if (this.props.touchText) return <BlueRadioText {...this.props} />

    return <BlueRadio {...this.props} />
  }
}

const styles = StyleSheet.create({
  radioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  blueRadio: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: appVariables.colorBorder,
    backgroundColor: appVariables.colorWhite,
    borderRadius: 20,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 2,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 3,
    },
    top: 1,
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6
  }
})

MBlueRadio.propTypes = {
  active: PropTypes.bool,
  onPress: PropTypes.func,
  label: PropTypes.any,
  touchText: PropTypes.bool
}

export default MBlueRadio;