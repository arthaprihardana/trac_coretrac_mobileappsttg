/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-02 11:40:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 13:15:21
 */
import React, { Component } from 'react';
import { Container, H1, Text } from 'native-base';
import { View, TouchableOpacity, Modal, SafeAreaView, ActivityIndicator } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from "react-redux";
import { appVariables, appMetrics } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';
import Icon from 'react-native-vector-icons/Ionicons';
import { setCaptureForProfile } from '../../../actions';
import _ from "lodash";

class MCameraProfile extends Component {

  state = {
    loading: false,
    flashMode: [
        { name: "auto", e: RNCamera.Constants.FlashMode.auto },
        { name: "on", e: RNCamera.Constants.FlashMode.on },
        { name: "off", e: RNCamera.Constants.FlashMode.off }
    ],
    selectedFlash: "auto",
    reverseCam: [
        { name: "back", e: RNCamera.Constants.Type.back },
        { name: "front", e: RNCamera.Constants.Type.front }
    ],
    selectedReverseCam: "back"
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.captureProfile !== prevProps.captureProfile) {
      return { captureProfile: this.props.captureProfile }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.captureProfile !== undefined) {
        this.setState({ loading: false }, () => this.props.onClose());
      }
    }
  }

  takePicture = async function() {
    this.setState({ loading: true });
    if (this.camera) {
      const options = { quality: 0.3, base64: true, fixOrientation: true, forceUpOrientation: true, orientation: "portrait" };
      const data = await this.camera.takePictureAsync(options);
      this.props.setCaptureForProfile(data);
    }
  };

  render = () => {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={() => {}}>
        <SafeAreaView style={{flex: 1, backgroundColor: '#000'}}>
          <View style={{flex: 1}}>
            <Container style={{backgroundColor: appVariables.colorWhite}}>
          <RNCamera
            ref={ref => this.camera = ref }
            style={{ width: appMetrics.windowWidth, height: appMetrics.windowHeight }}
            type={_.filter(this.state.reverseCam, { name: this.state.selectedReverseCam })[0].e}
            flashMode={_.filter(this.state.flashMode, { name: this.state.selectedFlash })[0].e}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          >
            {({ camera, status }) => {
              return(
                <View style={{ flexDirection: 'column', width: appMetrics.windowWidth, height: appMetrics.windowHeight, padding: 20 }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                      <TouchableOpacity style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.onClose()}>
                        <SvgIcon name="icoClose" />
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View style={{ flex: 10, alignItems: 'center', justifyContent: 'center' }}>
                      {this.state.loading && <ActivityIndicator size="large" color="#FFFFFF" />}
                  </View>

                  <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                      <TouchableOpacity onPress={() => {
                          var x = _.findIndex(this.state.flashMode, v => v.name === this.state.selectedFlash);
                          this.setState({
                              selectedFlash: this.state.flashMode[x+1] !== undefined ? this.state.flashMode[x+1].name : "auto"
                          });
                      }} style={{ width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}>
                          <Icon name={this.state.selectedFlash === "off" ? "md-flash-off" : "md-flash"} color="#FFFFFF" size={32} />
                          {this.state.selectedFlash === "auto" && <Text style={{ fontSize: 10, color: '#FFFFFF' }}>Auto</Text> }
                      </TouchableOpacity>
                      <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', width: 200 }} onPress={() => !this.state.loading ? this.takePicture(camera) : {}}>
                          <View style={{ width: 80, height: 80, borderRadius: 40, backgroundColor: '#FFF', borderWidth: 3, borderColor: '#CCC' }} />
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => {
                          var x = _.findIndex(this.state.reverseCam, v => v.name === this.state.selectedReverseCam);
                          this.setState({
                              selectedReverseCam: this.state.reverseCam[x+1] !== undefined ? this.state.reverseCam[x+1].name : "back"
                          });
                      }} style={{ width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}>
                          <Icon name="md-reverse-camera" color="#FFFFFF" size={32} />
                      </TouchableOpacity>
                  </View>
                </View>
              );
            }}
          </RNCamera>
        </Container>
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}

const mapStateToProps = ({ camera }) => {
  const { captureProfile } = camera;
  return { captureProfile };
}

export default connect(mapStateToProps, {
    setCaptureForProfile
})(MCameraProfile);