import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  carTitle:{
    container:{
      flex: 1,
      flexDirection: 'row',
      paddingVertical: 10,
      borderBottomWidth: 1,
      borderBottomColor: "#D8D8D8",
    },
  },
  mainContainer:{
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomColor: "#D8D8D8",
    borderBottomWidth: 10,
    backgroundColor:appVariables.colorWhite,
  },
}