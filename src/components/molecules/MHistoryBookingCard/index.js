import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import styles from "./style";
import { Actions } from "react-native-router-flux";

export default class MHistoryBookingCard extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props){
    super(props);
    this.state = {
      currentPage: Actions.currentScene
    }
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  goToCarDetail() {
    Actions.CarDetail({
      navData:{
        currentPage:"History Booking",
      }
    });
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderCarRentalTitle() {
    return (
      <View style={styles.carTitle.container}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-start'
        }}>
          <Text
            size={20}>
            Car Rental
          </Text>
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-end'
        }}>
          <Text
            size={14}
            color={appVariables.colorOrange}>
            Completed
          </Text>
        </View>
      </View>
    );
  }



  // ---------------------------------------------------

  _renderCarSwiper() {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
      }}>
        <Image
          source={require('@images/png/cars/Avanza.png')}
          style={{ height: 80, width: 150, resizeMode: "stretch" }} />
      </View>
    );
  }


  // ---------------------------------------------------

  _renderCarTotal() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
      }}>

        <View style={{
          flex: 1,
          flexDirection: 'column',
          paddingLeft: 20,
        }}>
          <Text
            size={11}
            color={appVariables.colorGray}>
            Car Total
          </Text>

          <Text
            size={14}
            color={appVariables.colorBlack}
            style={{ paddingTop: 10 }}>
            2 Cars
        </Text>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderBookingNumber() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>

          <View style={{
            flex: 1,
            flexDirection: 'column',
          }}>
            <Text
              size={11}
              color={appVariables.colorGray}>
              Booking Number
            </Text>

            <Text
              size={14}
              color={appVariables.colorBlack}
              style={{ paddingTop: 10 }}>
              A09E82093U
            </Text>
          </View>
        </View>
      </View>
    );
  }


  // ---------------------------------------------------

  _renderDates() {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        paddingTop: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <Text
            size={11}
            color={appVariables.colorGray}>
            Dates
          </Text>
        </View>
      </View>
    );
  }


  // ---------------------------------------------------

  _renderDetails() {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'column',
        paddingVertical: 10,
      }}>
        <View style={{
          flex: 2,
          flexDirection: 'row',
        }}>
          <Text
            size={14}
            color={appVariables.colorBlack}>
            12 Sep 2018 - 15 Sep 2018
            </Text>
        </View>

        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingVertical:20,
          }}
          onPress={() => this.goToCarDetail()}>

          <View style={{
            flex: 2,
            flexDirection: 'row',
          }}>
            <Text
              size={12}
              color={appVariables.colorBlack}>
              DETAILS
            </Text>
          </View>
          <View style={{
            flex: 8,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems:'center',
          }}>
            <Icon
              name="arrow-right"
              size={12}
              color={appVariables.colorOrange} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderCarRentals() {
    var carRentals = [];
    var carAmount = 4;
    for (let i = 0; i < carAmount; i++) {
      carRentals.push(
        <View key={i}>
          <View style={styles.mainContainer}>
            {this._renderCarRentalTitle()}
            <View style={{
              flex: -1,
              flexDirection: 'row',
              paddingVertical: 20,
            }}>
              {this._renderCarSwiper()}
              {this._renderCarTotal()}
              {this._renderBookingNumber()}
            </View>
            {this._renderDates()}
            {this._renderDetails()}
          </View>
        </View>
      )
    }
    return (
      <View>
        {carRentals}
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (

      <ScrollView style={{
        flex: 1,
      }}>
        {this._renderCarRentals()}
      </ScrollView>

    );
  }
}