import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { appMetrics, appVariables } from '../../../assets/styles';

class MStepper extends Component {
  render() {
    let leftLine = null;
    let middleLineLeft = [];
    let middleLineRight = [];
    let rightLine = null;
    let ndolLeft = null;
    let ndolMiddle = [];
    let ndolRight = null;

    const { step, labels } = this.props;
    const sLine = {width: (appMetrics.windowWidth / labels.length) / 2};
    const sLineLeft = {left: (appMetrics.windowWidth / labels.length) / 2};
    const sLineMiddleRight = {left: (appMetrics.windowWidth / labels.length) / 2};
    
    // switch(this.props.step) {
    //   case 1:
    //     ndolLeft = styles.ndolActive;
    //     break;
    //   case 2: 
    //     ndolLeft = styles.ndolActive;
    //     leftLine = styles.lineActive;
    //     middleLineLeft = styles.lineActive;
    //     ndolMiddle = styles.ndolActive;
    //     break;
    //   case 3:
    //     ndolLeft = styles.ndolActive;
    //     leftLine = styles.lineActive;
    //     middleLineLeft = styles.lineActive;
    //     ndolMiddle = styles.ndolActive;
    //     middleLineRight = styles.lineActive;
    //     rightLine = styles.lineActive;
    //     ndolRight = styles.ndolActive;
    //     break;
    // }

    let content = labels.map((item, index) => {
      let sIsi;
      
      if (index <= 0) {
        if (step <= 1) {
          ndolLeft = styles.ndolActive; 
        } else {
          ndolLeft = styles.ndolActive; 
          leftLine = styles.lineActive;
        }

        sIsi = (
          <View style={styles.nc} key={index} key={index}>
            <View style={[styles.line, sLine, sLineLeft, leftLine]}></View>
            <View style={[styles.ndol, ndolLeft]}></View>
            <Text style={styles.textNc}>{labels[index]}</Text>
          </View>
        );
      } else if (index > 0 && index < (labels.length - 1)) {
        if (index < step) {
          ndolMiddle[index] = styles.ndolActive;
          middleLineLeft[index] = styles.lineActive;
          middleLineRight[index] = styles.lineActive;

          if (index == (step - 1)) {
            middleLineRight[index] = null;
          }
        }

        sIsi = (
          <View style={[styles.nc]} key={index}>
            <View style={[styles.line, sLine, styles.lineMiddleLeft, middleLineLeft[index]]}></View>
            <View style={[styles.line, sLine, sLineMiddleRight, middleLineRight[index]]}></View>
            <View style={[styles.ndol, ndolMiddle[index]]}></View>
            <Text style={styles.textNc}>{labels[index]}</Text>
          </View>
        )
      } else {
        if (step >= labels.length) {
          rightLine = styles.lineActive;
          ndolRight = styles.ndolActive;
        }

        sIsi = (
          <View style={styles.nc} key={index}>
            <View style={[styles.line, sLine, styles.lineRight, rightLine]}></View>
            <View style={[styles.ndol, ndolRight]}></View>
            <Text style={styles.textNc}>{labels[index]}</Text>
          </View>
        )
      }

      return sIsi;
    });


    return (
      <View style={styles.container}>
        <View style={styles.ndolContainer}>
          {content}
          {/* <View style={styles.nc}>
            <View style={[styles.line, styles.lineLeft, leftLine]}></View>
            <View style={[styles.ndol, ndolLeft]}></View>
            <Text style={styles.textNc}>{this.props.labels[0]}</Text>
          </View>
          <View style={[styles.nc, styles.centerNc]}>
            <View style={[styles.line, styles.lineMiddleLeft, middleLineLeft]}></View>
            <View style={[styles.line, styles.lineMiddleRight, middleLineRight]}></View>
            <View style={[styles.ndol, ndolMiddle]}></View>
            <Text style={styles.textNc}>{this.props.labels[1]}</Text>
          </View>
          <View style={styles.nc}>
            <View style={[styles.line, styles.lineRight, rightLine]}></View>
            <View style={[styles.ndol, ndolRight]}></View>
            <Text style={styles.textNc}>{this.props.labels[2]}</Text>
          </View> */}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#12265B',
    height: 62,
    width: '100%',
  },
  ndolContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  nc: {
    flex: 1,
    height: 62,
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ndol: {
    width: 14,
    height: 14,
    backgroundColor: '#14348B',
    borderRadius: 50,
    position: 'absolute',
    top: 14
  },
  ndolActive: {
    backgroundColor: appVariables.colorOrange,
  },
  textNc: {
    color: '#FFFFFF',
    fontSize: 12,
    marginTop: 8,
    position: 'absolute',
    top: 25
  },
  line: {
    height: 3,
    backgroundColor: '#14348B',
    position: 'absolute',
    top: 20,
  },
  lineActive: {
    backgroundColor: appVariables.colorOrange,
  },
  lineLeft: {
    left: (appMetrics.windowWidth / 3) / 2,
  },
  lineMiddleLeft: {
    left: 0
  },
  lineMiddleRight: {
    left: (appMetrics.windowWidth / 3) / 2,
  },
  lineRight: {
    left: 0
  },
})

export default MStepper;