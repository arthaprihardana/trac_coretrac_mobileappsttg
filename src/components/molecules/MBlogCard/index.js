import PropTypes from 'prop-types';
import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Mask from 'react-native-mask';
import { appMetrics, appVariables } from '../../../assets/styles';

const MBlogCard = ({ image, onPress, title, postDate }) => (
  <View style={styles.blogContainer}>
    <Mask shape={'rounded'}>
      <Image source={image} style={styles.blogImage} resizeMode='cover' />
    </Mask>
    <TouchableOpacity onPress={() => onPress}>
      <Text style={styles.blogTitle}>{title}</Text>
    </TouchableOpacity>
    <Text style={styles.blogPostDate}>{postDate}</Text>
  </View>
);

const styles = StyleSheet.create({
  blogImage: {
    flex: 1,
    width: appMetrics.windowWidth - 40,
    height: 190
  },
  blogContainer: {
    marginBottom: 20,
    flex: 1,
  },
  blogTitle: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 18,
    fontFamily: appVariables.museo700,
    color: appVariables.colorDark
  },
  blogPostDate: {
    flex: 1,
    color: appVariables.colorGray,
    fontSize: 16
  }
})

MBlogCard.propTypes = {
  image: PropTypes.any,
  onPress: PropTypes.func,
  title: PropTypes.string,
  postDate: PropTypes.string
}

export default MBlogCard;