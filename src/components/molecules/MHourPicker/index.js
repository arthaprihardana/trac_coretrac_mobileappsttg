import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import { Grid, Col } from 'native-base';
import { appStyles } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';
import appVariables from '../../../assets/styles/appVariables';
import DatePicker from 'react-native-datepicker'

class MHourPicker extends Component {
  render() {
    return (
      <View style={styles.hourPickerContainer}>
        <TouchableOpacity style={styles.hourCont}>
          <Text style={styles.hour}>{this.props.selectedTime}</Text>
        </TouchableOpacity>
        <DatePicker
          style={{ borderWidth: 0, position: 'absolute', left: 0, top: 0, width: '100%' }}
          date={new Date()}
          mode="time"
          showIcon={false}
          hideText={true}
          format="HH:mm"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={(hour) => this.props.onTimeChange(hour)}
          />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  hourCont: {
    alignSelf: 'flex-end',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
  },
  hour: {
    fontSize: 18,
    fontFamily: appVariables.museo900,
    color: appVariables.colorWhite,
    height: 24,
  },
})

export default MHourPicker;