/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 11:46:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 13:14:59
 */
import React, { Component } from 'react';
import { Container, H1, Text } from 'native-base';
import { View, TouchableOpacity, Modal, SafeAreaView, ActivityIndicator } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from "react-redux";
import { appVariables, appMetrics } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';
import { setCaptureCamera } from '../../../actions';

class MCamera extends Component {

  state = {
    loading: false
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.capture !== prevProps.capture) {
      return { capture: this.props.capture }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.capture !== undefined) {
        this.setState({ loading: false }, () => this.props.onClose());
      }
    }
  }

  takePicture = async function() {
    this.setState({ loading: true });
    if (this.camera) {
      const options = { quality: 0.3, base64: true };
      const data = await this.camera.takePictureAsync(options);
      this.props.setCaptureCamera(data);
    }
  };

  render = () => {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={() => {}}>   
        <SafeAreaView style={{flex: 1, backgroundColor: '#000'}}>
          <View style={{ flex: 1 }}>
            <Container style={{backgroundColor: appVariables.colorWhite}}>
              <RNCamera
                ref={ref => this.camera = ref }
                style={{ width: appMetrics.windowWidth, height: appMetrics.windowHeight }}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.auto}
                androidCameraPermissionOptions={{
                  title: 'Permission to use camera',
                  message: 'We need your permission to use your camera',
                  buttonPositive: 'Ok',
                  buttonNegative: 'Cancel',
                }}
                androidRecordAudioPermissionOptions={{
                  title: 'Permission to use audio recording',
                  message: 'We need your permission to use your audio',
                  buttonPositive: 'Ok',
                  buttonNegative: 'Cancel',
                }}
              >
                {({ camera, status }) => {
                  return(
                    <View style={{ flexDirection: 'column', width: appMetrics.windowWidth, height: appMetrics.windowHeight, padding: 20 }}>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 3 }}>
                          <Text style={{ color: appVariables.colorWhite, fontFamily: appVariables.museo700, fontSize: 24 }}>Hold the front of your</Text>
                          <Text style={{ color: appVariables.colorWhite, fontFamily: appVariables.museo700, fontSize: 24 }}>ID Card here</Text>
                          <Text style={{ color: appVariables.colorWhite, fontSize: 14, width: '100%', marginTop: 20}}>And Tap the button "Capture" at the bottom</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                          <TouchableOpacity style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.onClose()}>
                            <SvgIcon name="icoClose" />
                          </TouchableOpacity>
                        </View>
                      </View>

                      <View style={{ flex: 10, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: 310, height: 195, borderWidth: 4, borderColor: 'white', borderStyle: 'dotted', borderRadius: 14, justifyContent: 'center', alignItems: 'center'}}>
                          {this.state.loading && <ActivityIndicator size="large" color="#FFFFFF" />}
                        </View>
                      </View>

                      <View style={{ flex: 1 }}>
                        <TouchableOpacity style={{ flex: 1, position: 'absolute', bottom: 20, left: 0 }} onPress={() => !this.state.loading ? this.takePicture(camera) : {}}>
                          <View style={{ flexDirection: 'row',}}>
                            <SvgIcon name="icoUploadPhoto" />
                            <Text style={{ fontSize: 12, marginLeft: 10, marginTop: 4, color: appVariables.colorWhite}}>CAPTURE ID CARD</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  );
                }}
              </RNCamera>
            </Container>
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}

const mapStateToProps = ({ camera }) => {
  const { capture } = camera;
  return { capture };
}

export default connect(mapStateToProps, {
  setCaptureCamera
})(MCamera);