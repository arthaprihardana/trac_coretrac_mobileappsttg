import PropTypes from 'prop-types';
import React from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import SvgIcon from '../../../utils/SvgIcon';
import { appVariables } from '@variables';


const MMessageCard = ({ onPress, title, description, postDate }) => (
  <View style={styles.cardMessageWrapper}>

    {/* Message title */}
    <View style={styles.messageTitle}>
      <Text style={{
        fontSize: 14,
        color: appVariables.colorBlack}}>
        {title}
      </Text>
    </View>

    {/* Message Content */}
    <View style={styles.messageContent}>
      <Text style={{
        fontSize: 12,
        color: appVariables.colorBlack}}>
        {description}
      </Text>
    </View>

    {/* Date and See Blog */}
    <View style={{
      flex: -1,
      flexDirection: "row"}}>

      <View style={styles.postDate}>
        <Text style={{
          fontSize: 12,
          color: appVariables.colorGray}}>
          {postDate}
        </Text>
      </View>

      <View style={{
        flex: 1,
        flexDirection: "column",
        paddingVertical: 10,
      }}>
      </View>

      <TouchableOpacity style={styles.seeBlog}>
        <Text style={{
          fontSize: 12,
          color: appVariables.colorBlack}}>
          SEE BLOG
        </Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.iconNext}>
        <SvgIcon name="icoNext" />
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
  cardMessageWrapper: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 20,
    flexDirection: "column",
    borderBottomColor: "#F3F4F5",
    borderBottomWidth: 10,
  },
  messageTitle: {
    flex: -1,
    flexDirection: "row",
    borderBottomColor: appVariables.colorGray,
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
  messageContent: {
    flex: -1,
    flexDirection: "row",
    paddingVertical: 20,
  },
  postDate: {
    flex: 3,
    flexDirection: "column",
    paddingVertical: 10,
  },
  seeBlog: {
    flex: 1,
    flexDirection: "column",
    paddingVertical: 10,
    alignItems:"center",
  },
  iconNext: {
    flex: 0.5,
    flexDirection: "column",
    justifyContent:"center",
    alignItems:"center",
  }
})

MMessageCard.propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  description: PropTypes.string,
  postDate: PropTypes.string
}

export default MMessageCard;