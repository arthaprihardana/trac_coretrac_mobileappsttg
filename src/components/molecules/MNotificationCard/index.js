/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-05 21:07:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-05 21:20:11
 */
import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import SvgIcon from '../../../utils/SvgIcon';
import { appVariables } from '@variables';
import moment from "moment";
import "moment/locale/id";
import { Actions } from "react-native-router-flux";
 
const MNotificationCard = ({ onPress, item }) => (
  <View style={styles.cardMessageWrapper}>
    
    {/* Message title */}
    <View style={styles.messageTitle}>
      <Text style={{
        fontSize: 16,
        color: appVariables.colorBlack}}>
        {item.data.title}
      </Text>
    </View>

    {/* Message Content */}
    {/* <View style={styles.messageContent}>
      <Text style={{
        fontSize: 12,
        color: appVariables.colorBlack}}>
        {description}
      </Text>
    </View> */}

    {/* See Blog */}
    <View style={{
      flex: -1,
      flexDirection: "row"}}>

      {/* <View style={{
        flex: 7,
        flexDirection: "column",
        paddingVertical: 10,
      }}>
      </View> */}

      {/* Date  */}
      <View style={styles.postDate}>
        <Text style={{
          fontSize: 12,
          color: appVariables.colorGray}}>
          {moment().format('LLL')}
        </Text>
      </View>

      <TouchableOpacity style={styles.seeBlog} onPress={onPress}>
        <Text style={{
          fontSize: 12,
          color: appVariables.colorBlack}}>
          SEE DETAIL
        </Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.iconNext}>
        <SvgIcon name="icoNext" />
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
  cardMessageWrapper: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 20,
    flexDirection: "column",
    borderBottomColor: "#F3F4F5",
    borderBottomWidth: 10,
  },
  messageTitle: {
    flex: -1,
    flexDirection: "row",
    borderBottomColor: "#D8D8D8",
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
  messageContent: {
    flex: -1,
    flexDirection: "row",
    paddingVertical: 20,
  },
  postDate: {
    flex: 3,
    flexDirection: "column",
    paddingVertical: 10,
  },
  seeBlog: {
    flex: 1,
    flexDirection: "column",
    paddingVertical: 10,
    alignItems:"center",
    justifyContent:"flex-end"
  },
  iconNext: {
    flex: 0.5,
    flexDirection: "column",
    justifyContent:"center",
    alignItems:"center",
  }
})

MNotificationCard.propTypes = {
  onPress: PropTypes.func,
  item: PropTypes.any
}

export default MNotificationCard;