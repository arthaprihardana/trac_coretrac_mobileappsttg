import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Content } from 'native-base';
import ASectionTitle from '../../atoms/ASectionTitle';
import { PropTypes } from 'prop-types';

class MSection extends React.Component {
  render() {
    let padder = true;
    let padding = 0;
    if (this.props.padder === false) {padder = false; padding = 20;}

    return (
      <Content style={[{ paddingTop: padding, paddingBottom: padding }, styles.section]} padder={padder}>
        <View style={{ paddingLeft: padding, paddingRight: padding }}>
          <ASectionTitle title={this.props.title} />
        </View>
        <View>
          { this.props.children }
        </View>
      </Content>
    );
  }
}

MSection.propTypes = {
  title: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  section: {
    marginBottom: 0
  },
});

export default MSection;