import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { Content } from 'native-base';
import SvgIcon from '../../../utils/SvgIcon';
import Mask from 'react-native-mask';
import appVariables from '../../../assets/styles/appVariables';

class MSortDropdown extends Component {
    state = {
      dropdownShowed: false,
    }

    renderItem(data, index) {
      return (
        <View key={index} style={styles.dropdownItemWrapper}>
          <Mask shape={'circle'}>
            <Image source={data.image} style={styles.dropdownImage}  />
          </Mask>
          <Text style={styles.dropdownText}>{data.value}</Text>
        </View>
      )
    }

    pressHandler = () => {
      this.setState({
        dropdownShowed: !this.state.dropdownShowed
      })
    }

    render = () => {
        let { label, value, noborder, onPress, data} = this.props;

        let dropdownContent = data.map((item, index) => this.renderItem(item, index));

        return (
          <View style={{alignSelf: 'center', position: 'relative'}}>
            <View>
              <TouchableOpacity onPress={this.pressHandler}>
                <View style={styles.sortWrapper}>
                  <Mask style={styles.itemImage} shape={'circle'}><Image style={styles.image} source={data[0].image} /></Mask>
                  <Text style={styles.labelStyle}>{value}</Text>
                  <SvgIcon name="icoSort" style={styles.iconWrapper} />
                </View>
              </TouchableOpacity>
            </View>
            {this.state.dropdownShowed ?
              <View style={styles.dropdown}>
                <Content>
                { dropdownContent }
                </Content>
              </View>
            : null}
          </View>
        )
    }
}

const styles = StyleSheet.create({
    sortWrapper: { flexDirection: 'row', height: 30, alignItems: 'center' },
    itemImage: { marginRight: 5},
    image: { width: 20, height: 20 },
    labelStyle: { fontSize: 16, fontFamily: appVariables.museo500, marginLeft: 10, marginRight: 10, marginTop: 0, },
    iconWrapper: { marginTop: 0 },
    dropdown: { position: 'absolute', height: 160, top: 30, left: 0, elevation: 10, borderWidth: 1, borderColor: '#C3C8D3', borderRadius: 3, padding: 10, backgroundColor: 'white', width: 200, },
    dropdownItemWrapper: { flexDirection: 'row', alignItems: 'center', borderBottomColor: '#C3C8D3', borderBottomWidth: 1, padding: 10, },
    dropdownImage: { width: 20, height: 20},
    dropdownText: { fontSize: 14, fontFamily: appVariables.museo500, color: '#242842', marginLeft: 10 }
})

MSortDropdown.propTypes = {
    label: PropTypes.string, 
    value: PropTypes.any, 
    noborder: PropTypes.bool, 
    onPress: PropTypes.func,
    dropdownShowed: PropTypes.bool,
    data: PropTypes.any
}

export default MSortDropdown;