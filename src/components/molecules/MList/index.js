import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { appVariables } from '../../../assets/styles';

const MList = ({ label, listItem }) => {
    let { listWrapper, labelStyle, itemWrapper, iconList } = styles;
    return (
        <View style={listWrapper}>
            <Text style={labelStyle}>{label}</Text>
            {
                listItem.map((item, i) => {
                    return (
                        <View key={i} style={itemWrapper}>
                            <View style={iconList}></View>
                            <Text style={{fontFamily: appVariables.museo500}}>{item}</Text>
                        </View>
                    )
                })
            }
        </View>
    )
}

const styles = StyleSheet.create({
    listWrapper: { marginTop: 22, marginBottom: 8, marginHorizontal: 20 },
    labelStyle: { fontSize: 14, fontFamily: appVariables.museo700, color: '#2A2E36', marginBottom: 10 },
    itemWrapper: { flexDirection: 'row', alignItems: 'center', marginBottom: 6 },
    iconList: { width: 6, height: 6, backgroundColor: '#2A2E36', borderRadius: 100, marginRight: 12 },
})

MList.propTypes = {
    label: PropTypes.string,
    listItem: PropTypes.array
}

export default MList;