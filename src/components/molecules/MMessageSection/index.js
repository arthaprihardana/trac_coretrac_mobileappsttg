import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Content } from 'native-base';
import { PropTypes } from 'prop-types';

class MMessageSection extends React.Component {
  render() {
    return (
        <View>
          { this.props.children }
        </View>
    );
  }
}

MMessageSection.propTypes = {
  title: PropTypes.string.isRequired,
};

export default MMessageSection;