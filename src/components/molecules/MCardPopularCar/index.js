import { Card, Col, Grid, Text } from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const MCardPopularCar = ({ onPress, carImage, carName, rentPrice }) => (
  <Card style={{ marginRight: 10 }}>
    <View style={styles.cardContent}>
      <View onPress={() => onPress}>
        <View style={styles.carImageWrapper}>
          <Image style={styles.carImage} source={carImage} />
        </View>
      </View>
      <View style={styles.separator}></View>
      <View style={styles.textWrapper}>
        <Grid>
          <Col><Text style={styles.carName}>{carName}</Text></Col>
          <Col><Text style={styles.rentPrice}>{rentPrice}</Text><Text style={styles.rentTime}>per day</Text></Col>
        </Grid>
      </View>
    </View>
  </Card>
)

const styles = StyleSheet.create({
  separator: {
    flex: 1,
    borderTopColor: '#2A2E36',
    borderTopWidth: .3,
    marginTop: 20,
    marginBottom: 20
  },
  cardContent: {
    paddingTop: 30,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    alignSelf: 'stretch',
  },
  textWrapper: {
    height: 50,
  },
  carImageWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 180,
  },
  carImage: {
    alignSelf: 'center',
  },
  carName: {
    fontSize: 18,
    color: '#2A2E36',
    fontFamily: appVariables.museo700,
  },
  rentPrice: {
    alignSelf: 'flex-end',
  },
  rentTime: {
    alignSelf: 'flex-end',
    color: '#8A959E',
  }
});

MCardPopularCar.propTypes = {
  onPress: PropTypes.func,
  carImage: PropTypes.any,
  carName: PropTypes.string,
  rentPrice: PropTypes.any
}

export default MCardPopularCar;