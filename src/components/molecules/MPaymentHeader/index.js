import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const MPaymentHeader = (props) => (
  <View style={styles.container}>
    <Text style={styles.title}>{props.title}</Text>
    <View>
      {props.children}
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: { flex: 1, flexDirection: 'column', backgroundColor: appVariables.colorBlueHeader, padding: 20, },
  title: { fontSize: 24, color: appVariables.colorWhite, marginBottom: 10, fontFamily: appVariables.museo700 },

});

export default MPaymentHeader;