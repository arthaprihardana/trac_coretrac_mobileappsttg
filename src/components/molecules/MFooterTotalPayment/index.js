import { Footer, Text } from 'native-base';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';

const MFooterTotalPayment = (props) => (
  <Footer style={styles.footerContainer}>
    <View style={styles.footerWrapper}>
      <View style={{ flex: 1 }}>
        <Text style={styles.footerLabelTop}>Total payment</Text>
        <Text style={styles.footerLabelPrice}>Rp {props.totalPrice}<Text style={styles.footerLabelDay}>/ {props.totalDay} days</Text></Text>
      </View>
      <View style={styles.footerLabelDetail}>
        <TouchableOpacity onPress={() => props.onPress()} style={{flexDirection: 'row',}}>
          <Text>DETAIL</Text>
          <SvgIcon name="icoSort" style={styles.footerIcoSort} />
        </TouchableOpacity>
      </View>
    </View>
  </Footer>
);

const styles = StyleSheet.create({
  footerContainer: { 
    height: 'auto',
  },
  footerWrapper: { 
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 20, 
    paddingVertical: 12,
  },
  footerLabelTop: { 
    fontSize: 14, 
    fontFamily: appVariables.museo500, 
    color: '#8A959E', 
    marginBottom: 4 
  },
  footerLabelPrice: { fontSize: 18, fontFamily: appVariables.museo500, color: '#2A2E36' },
  footerLabelDay: { fontSize: 12, fontWeight: '500', color: '#8A959E' },
  footerLabelDetail: { flex: 1, justifyContent: 'center', alignItems: 'flex-end'},
  footerIcoSort: {marginTop: 6, marginLeft: 6}
});

export default MFooterTotalPayment;