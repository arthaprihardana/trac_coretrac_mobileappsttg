import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Grid, Col } from 'native-base';
import { appStyles } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';
import { withNavigation } from 'react-navigation';

const MHeader = (props) => (
  <View style={styles.homeHeader}>
    <Grid>
      <Col>
        <SvgIcon name="logoTrac" />
      </Col>
      <Col>
        <TouchableOpacity style={[styles.loginIcon, appStyles.right]} onPress={() => props.navigation.push('Login')}><SvgIcon name="icoLock" /></TouchableOpacity>
      </Col>
    </Grid>
  </View>
)

const styles = StyleSheet.create({
  homeHeader: {
    padding: 20,
    height: 80,
    flex: 1
  },
  loginIcon: {
    width: 24,
    height: 24,
    alignSelf: 'center',
    top: 10
  },
})

export default withNavigation(MHeader);