import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { appVariables } from '../../../assets/styles';
import AButton from '../../../components/atoms/AButton';

const MFooterSummary = (props) => {
    let { totalCars, totalPrice, totalDays, selectAction, bookingType } = props;
    let { footerContainer, left, right, totalCarsStyle, totalPriceStyle, totalDaysStyle } = styles;
    let bookingWhat = 'car';
    let bookingWhatPlural = 'cars';

    if (bookingType) {
      if (bookingType === 'bus') {
        bookingWhat = 'bus'; bookingWhatPlural = 'busses';
      }
    }
    
    return (
        <View style={footerContainer}>
          <View style={left}>
            <Text style={totalCarsStyle}>Total {totalCars} {totalCars > 1 ? bookingWhatPlural : bookingWhat} price</Text>
            <Text style={totalPriceStyle}>Rp {totalPrice}<Text style={totalDaysStyle}>/ {totalDays} {totalDays > 1 ? 'days' : 'day'}</Text></Text>
          </View>
          <View style={right}>
            <AButton primary block onPress={selectAction}>BOOK {totalCars > 1 ? String(bookingWhatPlural).toUpperCase() : String(bookingWhat).toUpperCase() }</AButton>
          </View>
        </View>
    )
}

const styles = StyleSheet.create({
    footerContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', width: '100%', paddingHorizontal: 20, paddingVertical: 12 },
    left: { flex: 1 },
    right: { flex: 1, paddingLeft: 12 },
    totalCarsStyle: { fontSize: 14, fontFamily: appVariables.museo500, color: '#8A959E' },
    totalPriceStyle: { fontSize: 18, fontFamily: appVariables.museo700, color: '#2A2E36' },
    totalDaysStyle: { fontSize: 12, color: '#8A959E' }
})

export default MFooterSummary;