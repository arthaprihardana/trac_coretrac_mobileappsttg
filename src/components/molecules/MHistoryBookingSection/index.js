import React from 'react';
import { View} from 'react-native';
import { PropTypes } from 'prop-types';

class MHistoryBookingSection extends React.Component {
  render() {
    return (
        <View>
          { this.props.children }
        </View>
    );
  }
}
export default MHistoryBookingSection;