import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appMetrics, appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';

const MCounter = ({ value, minusAction, plusAction, positionFooter }) => {
    let { wrapper, valueStyle, defMinusStyle, defPlusStyle, positionFooterStyle, positionFooterWrapper } = styles;
    return (
        <View style={positionFooter ? positionFooterWrapper : wrapper}>
            <TouchableOpacity onPress={minusAction}>
                <View style={positionFooter ? positionFooterStyle : defMinusStyle}>
                    <SvgIcon name="icoMinus" />
                </View>
            </TouchableOpacity>
            {
                positionFooter
                    ? <View style={{ borderColor: '#2A2E36', borderWidth: 0.3, borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', width: (appMetrics.windowWidth / 2 - 30) - 100 }}>
                        <Text style={{ fontSize: 14, fontFamily: appVariables.museo700, color: '#2A2E36' }}>{value}</Text>
                    </View> :
                    <Text style={valueStyle}>{value}</Text>
            }
            <TouchableOpacity onPress={plusAction}>
                <View style={positionFooter ? positionFooterStyle : defPlusStyle}>
                    <SvgIcon name="icoPlus" />
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: { flexDirection: 'row', borderRadius: 100, borderWidth: 1, borderColor: appVariables.colorBorder, backgroundColor: 'white', width: 125, justifyContent: 'space-between', alignItems: 'center' },
    positionFooterWrapper: { flexDirection: 'row', justifyContent: "space-between" },
    valueStyle: { fontSize: 18, fontFamily: appVariables.museo700 },
    defMinusStyle: { width: 43, height: 43, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 100, borderBottomLeftRadius: 100, backgroundColor: 'white' },
    defPlusStyle: { width: 43, height: 43, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 100, borderBottomRightRadius: 100, backgroundColor: 'white' },
    positionFooterStyle: { borderTopColor: '#2A2E36', borderTopWidth: 0.3, width: 50, height: 50, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20, paddingVertical: 23, width: 50 },
})

MCounter.propTypes = {
    value: PropTypes.number.isRequired,
    minusAction: PropTypes.func.isRequired,
    plusAction: PropTypes.func.isRequired,
    positionFooter: PropTypes.bool
}

export default MCounter;