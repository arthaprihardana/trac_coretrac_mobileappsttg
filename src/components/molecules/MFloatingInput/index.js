import React from 'react';
import { Item, Label, Input } from 'native-base';
import { View, Text, StyleSheet } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

class MFloatingInput extends React.Component {
  render() {
    let notes = null;
    if (this.props.notes) { notes = <Text style={styles.inputNote}>{this.props.notes}</Text>; }

    return (
      <View style={styles.inputWrapper}>
        <Item floatingLabel style={styles.item} error={(this.props.error == true) ? true : null}>
          <Label style={styles.label}>{this.props.label}</Label>
          <Input style={styles.input} 
            autoCorrect={false}
            autoCapitalize={'none'}
            secureTextEntry={this.props.secure}
            placeholder={this.props.placeholder}
          />
        </Item>
        { notes }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputWrapper: {
    flex: 1,
    marginBottom: 20
  },
  label: {
    fontFamily: appVariables.museo500,
    color: '#8A959E'
  },
  item: {
    marginLeft: 0,
    paddingLeft: 0
  },
  input: {
    paddingBottom: 5,
    fontSize: 16,
    fontFamily: appVariables.museo500,
  },
  inputNote: {
    fontSize: 10,
    color: appVariables.colorGray,
    marginTop: 5,
    fontFamily: appVariables.museo500,
  }
});

export default MFloatingInput;