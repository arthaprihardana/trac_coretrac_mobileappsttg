import { Button, Input, Text } from 'native-base';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const MApplyPromo = () => (
  <View style={styles.container}>
    <View><Text style={styles.caption}>Do you have Promo Code or Referral Code?</Text></View>
    <View style={styles.formContainer}>
      <View style={styles.inputForm}>
        <Input style={styles.inputText} />
      </View>
      <View style={styles.buttonContainer}>
        <Button bordered dark full><Text>Apply</Text></Button>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: { marginTop: 0, marginBottom: 20, flexDirection: 'column', backgroundColor: '#F6F6F8', borderWidth: 1, borderColor: '#979797', borderRadius: 10, padding: 20, },
  caption: { fontSize: 16, flex: 1, fontFamily: appVariables.museo500 },
  formContainer: { flexDirection: 'row', marginTop: 30 },
  inputForm: { flex: 6, left: 0, margin: 0, padding: 0, borderBottomWidth: 1, borderBottomColor: appVariables.colorBorderColor, marginRight: 10, },
  inputText: { fontSize: 16, fontFamily: appVariables.museo500, paddingLeft: 0, marginLeft: 0, height: 44, left: 0 },
  buttonContainer: { flex: 4 }
});

export default MApplyPromo;