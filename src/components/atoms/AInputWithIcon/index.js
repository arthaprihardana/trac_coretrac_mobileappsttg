import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';

const AInputWithIcon = ({ label, iconName, onChangeText }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.inputLabel}>{label}</Text>
      <View style={styles.inputWrapper}>
        <SvgIcon style={styles.icon} name={iconName} />
        <TextInput
          style={styles.textInput}
          onChangeText={onChangeText}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginBottom: 0,
  },
  icon: {
    marginTop: 16,
    flex: 1,
  },
  inputWrapper: {
    borderWidth: 1,
    borderBottomColor: appVariables.colorBorderBottom,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    flexDirection: 'row',
  },
  inputLabel: {
    fontSize: 12,
    color: appVariables.colorTabInactive,
    marginBottom: 10
  },
  textInput: {
    fontSize: 24,
    color: appVariables.colorWhite,
    flex: 9
  },
});

AInputWithIcon.propTypes = {
  label: PropTypes.string,
  iconName: PropTypes.string,
  onChangeText: PropTypes.func
}

export default AInputWithIcon;