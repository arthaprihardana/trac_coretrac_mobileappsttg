import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import SvgIcon from '../../../utils/SvgIcon';

const ACircleIconButton = ({ bgColor, iconName }) => (
  <TouchableOpacity style={[styles.wrapper, { backgroundColor: bgColor }]}>
    <View>
      <SvgIcon name={iconName} />
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  wrapper: {
    width: 32,
    height: 32,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'transparent',
    marginLeft: 5,
    marginRight: 5
  },
});

ACircleIconButton.propTypes = {
  bgColor: PropTypes.string.isRequired,
  iconName: PropTypes.string.isRequired,
}
export default ACircleIconButton;