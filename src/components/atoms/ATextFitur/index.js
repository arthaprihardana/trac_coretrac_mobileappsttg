import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { appVariables } from '../../../assets/styles';

const ATextFitur = ({ value, label }) => {
    let { wrapper, valueStyle, labelStyle } = styles;
    return (
        <View style={wrapper}>
            <Text style={valueStyle}>{value}</Text>
            <Text style={labelStyle}>{label}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: { flexDirection: 'row', alignItems: 'flex-end' },
    valueStyle: { fontSize: 14, fontFamily: appVariables.museo700, color: '#2A2E36' },
    labelStyle: { fontSize: 12, fontFamily: appVariables.museo500, color: '#8A959E', marginLeft: 8 }
})

ATextFitur.propTypes = {
    value: PropTypes.any.isRequired,
    label: PropTypes.string.isRequired
}

export default ATextFitur;