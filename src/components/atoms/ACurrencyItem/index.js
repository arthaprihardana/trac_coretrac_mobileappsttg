import PropTypes from 'prop-types';
import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../assets/styles';

const ACurrencyItem = ({ flagUri, active, noBorder, label, onPress }) => {
    let { activeStyle, labelStyle, flagWrapper, flagStyle, currencyWrapper, noBorderStyle } = styles;
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={[currencyWrapper, noBorder ? noBorderStyle : null]}>
                <View style={flagWrapper}>
                    <Image source={flagUri} style={flagStyle} />
                </View>
                <Text style={[labelStyle, active ? activeStyle : null]}>{label}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    currencyWrapper: { flexDirection: 'row', alignItems: 'center', borderBottomColor: '#C3C8D3', borderBottomWidth: 1, paddingBottom: 13, paddingHorizontal: 8, paddingTop: 13 },
    noBorderStyle: { borderBottomWidth: 0 },
    flagWrapper: { marginRight: 8, width: 19, height: 19, borderRadius: 100 },
    flagStyle: { flex: 1, width: undefined, height: undefined, resizeMode: 'contain' },
    labelStyle: { fontSize: 14, fontFamily: appVariables.museo500, color: '#8A959E' },
    activeStyle: { color: '#242842' },
})

ACurrencyItem.propTypes = {
    flagUri: PropTypes.any,
    active: PropTypes.bool,
    noBorder: PropTypes.bool,
    label: PropTypes.string,
    onPress: PropTypes.func
}

export default ACurrencyItem;