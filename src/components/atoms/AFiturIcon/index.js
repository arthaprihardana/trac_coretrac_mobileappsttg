import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import SvgIcon from '../../../utils/SvgIcon';
import appVariables from '../../../assets/styles/appVariables';

class AFiturIcon extends Component {
  render() {
    let textColor = appVariables.colorDark;
    if (this.props.white) {
      textColor = appVariables.colorWhite;
    }
    let content = <Text style={[styles.label, {color: textColor}]}>{this.props.label}</Text>;

    if (this.props.check === true) {
      let checkIcon = 'icoCheckBlack';
      if (this.props.white === true) {
        checkIcon = 'icoCheckWhite';
      }   

      content = <SvgIcon name={checkIcon} style={styles.icon} />;
    }

    return(
      <View style={styles.container}>
        <SvgIcon name={this.props.iconName} />
        {content}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  icon: {
    marginLeft: 5,
    marginTop: 2,
  },
  label: {
    marginLeft: 5,
    fontSize: 16,
    fontFamily: appVariables.museo700,
  }
})

export default AFiturIcon;