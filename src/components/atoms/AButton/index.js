import { Button, Text, View } from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import SvgIcon from '../../../utils/SvgIcon';

const ButtonWithArrow = ({ title, onPress }) => {
  let { buttonWrapper, buttonTitleStyle, buttonIcon } = styles;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={buttonWrapper}>
        <Text style={buttonTitleStyle}>{title}</Text>
        <View style={buttonIcon}>
          <SvgIcon name="icoNext" />
        </View>
      </View>
    </TouchableOpacity>
  )
}

const ButtonDefault = (props) => {
  return (
    <Button {...props} style={{ width: '100%' }}>
      <Text>{props.children}</Text>
    </Button>
  )
}

const AButton = (props) => {
  if (props.arrow) {
    return <ButtonWithArrow {...props} />
  }
  return (
    <ButtonDefault {...props} />
  )
};

const styles = StyleSheet.create({
  buttonWrapper: { flexDirection: 'row', alignItems: 'center', paddingRight: 10 },
  buttonTitleStyle: { fontSize: 12, color: '#2A2E36', fontWeight: '700' },
  buttonIcon: { marginLeft: 13 }
});

AButton.propTypes = {
  title: PropTypes.string, 
  onPress: PropTypes.func
}

export default AButton;