import React, { Component } from 'react';
import { Text } from 'react-native';

const ABold = (props) => (
  <Text style={{ fontWeight: '900' }}>{props.children}</Text>
)

export default ABold;