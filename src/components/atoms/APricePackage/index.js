import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { appVariables } from '../../../assets/styles';

const APricePackage = ({ price }) => <Text style={styles.priceStyle}>Rp {price}<Text style={styles.packageStyle}> / Package</Text></Text>

const styles = StyleSheet.create({
    priceStyle: { fontSize: 18, fontFamily: appVariables.museo700, color: '#F27D20' },
    packageStyle: { fontSize: 12, color: '#8A959E' }
})

APricePackage.propTypes = {
    price: PropTypes.number
}

export default APricePackage