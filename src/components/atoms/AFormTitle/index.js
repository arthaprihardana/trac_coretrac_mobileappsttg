import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const AFormTitle = ({ notes, title }) => {
  let notesShow = null;
  if (notes) {
    notesShow = <Text style={styles.notes}>{notes}</Text>
  }
  return (
    <View style={styles.container}>
      <Text style={styles.sTitle}>{title}</Text>
      {notesShow}
    </View>
  )
}

const styles = StyleSheet.create({
  sTitle: {
    fontFamily: appVariables.museo700,
    fontSize: 24,
    color: appVariables.colorDark,
  },
  container: {
    marginTop: 20,
    marginBottom: 10,
  },
  notes: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
    color: appVariables.colorGray,
    marginTop: 5
  }
});

AFormTitle.propTYpes = {
  notes: PropTypes.string,
  title: PropTypes.string
}

export default AFormTitle;