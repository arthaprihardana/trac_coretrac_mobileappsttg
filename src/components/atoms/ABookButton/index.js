import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';

const ABookButton = ({ searchIcon, isError, onPress, bgColor, label, textValue }) => {
  let disp = null;
  if (searchIcon === true) {
    disp = <SvgIcon name="icoSearchBig" style={[styles.inputIcon]} />;
  }

  let err = null;
  if (isError === true) {
    err = <SvgIcon name="icoInvalid" style={styles.error} />;
  }

  return (
    <TouchableOpacity style={styles.container} onPress={() => onPress()}>
      <View style={[styles.buttonWrapper, { backgroundColor: bgColor }]}>
        <Text style={styles.buttonLabel}>{label}</Text>
        <View style={styles.inputWrapper}>
          {disp}
          <Text style={styles.buttonValue}>{textValue}</Text>
          {err}
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  error: {
    marginLeft: 10,
  },
  inputWrapper: {
    flexDirection: 'row',
  },
  inputIcon: {
    marginRight: 10,
  },
  buttonWrapper: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 30,
    paddingBottom: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  buttonLabel: {
    fontSize: 12,
    color: appVariables.colorWhite,
    opacity: .4,
    marginBottom: 16
  },
  buttonValue: {
    fontSize: 20,
    color: appVariables.colorWhite,
    fontFamily: appVariables.museo500
  }
});

ABookButton.propTypes = {
  searchIcon: PropTypes.bool,
  isError: PropTypes.bool,
  onPress: PropTypes.func,
  bgColor: PropTypes.string,
  label: PropTypes.string,
  textValue: PropTypes.string
}

export default ABookButton;