import React, { Component } from 'react';
import { Footer, Button, Text } from 'native-base';
import { StyleSheet } from 'react-native';

class AFooterButton extends Component {
  render() {
    return (
      <Footer style={styles.container}>
        <Button style={styles.button} primary onPress={() => {this.props.onPress()}}>
          <Text style={styles.text}>{this.props.children}</Text>
        </Button>
      </Footer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 20, 
    paddingRight: 20,
    backgroundColor: 'transparent'
  },
  button: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    alignSelf: 'center',
  }
});

export default AFooterButton;