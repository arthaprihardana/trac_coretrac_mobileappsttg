import PropTypes from 'prop-types';
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';


const ACar = ({ carUri, statusNote, statusCar }) => {
    let { carWrapper, carStyle, carLabelStyle, onBookingStyle, carLeftStyle } = styles;
    let showLabel = null
    if (statusCar === 2) {
        showLabel = (
            <View style={[carLabelStyle, carLeftStyle]}>
                <Text style={{ color: 'white' }}>{statusNote.toUpperCase()}</Text>
            </View>
        )
    }
    if (statusCar === 3) {
        showLabel = (
            <View style={[carLabelStyle, onBookingStyle]}>
                <Text style={{ color: 'white' }}>{statusNote.toUpperCase()}</Text>
            </View>
        )
    }
    return (
        <View style={carWrapper}>
            <Image source={carUri} style={carStyle} />
            {showLabel}
        </View>
    )
}

const styles = StyleSheet.create({
    carWrapper: { marginHorizontal: 20, height: 115, marginTop: 42, marginBottom: 11, position: 'relative' },
    carStyle: { flex: 1, width: undefined, height: undefined, resizeMode: 'contain' },
    carLabelStyle: { position: 'absolute', padding: 10, paddingHorizontal: 16, borderRadius: 100, backgroundColor: 'white', bottom: 0 },
    onBookingStyle: { backgroundColor: '#9C1F3C' },
    carLeftStyle: { backgroundColor: '#2246A8' }
});

ACar.propTypes = {
    carUri: PropTypes.any.isRequired,
    statusNote: PropTypes.string,
    statusCar: PropTypes.number
}

export default ACar;