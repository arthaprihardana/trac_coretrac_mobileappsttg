import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';

const ACornerButton = ({ onPress, bgColor, iconName, label }) => (
  <TouchableOpacity style={styles.container} onPress={() => onPress()}>
    <View style={[{ backgroundColor: bgColor }, styles.buttonWrapper]}>
      <SvgIcon name={iconName} />
      <Text style={styles.label}>{label}</Text>
    </View>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  buttonWrapper: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 40,
    paddingRight: 40,
    flexDirection: 'row',
  },
  addFlex: {
    flex: 1,
  },
  label: {
    color: appVariables.colorWhite,
    marginLeft: 5,
    fontFamily: appVariables.museo700,
    fontSize: 16,
  }
});

ACornerButton.propTypes = {
  onPress: PropTypes.func,
  bgColor: PropTypes.string,
  iconName: PropTypes.string,
  label: PropTypes.string
}

export default ACornerButton;