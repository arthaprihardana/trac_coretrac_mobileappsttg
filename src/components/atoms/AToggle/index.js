import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const AToggle = ({ onPress, isOn }) => (
  <TouchableOpacity
    onPress={() => onPress()}
  >
    <View style={[styles.toggleWrapper, (isOn) ? styles.backgroundActive : null]}>
      <View style={styles.toggleSwitch}></View>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  toggleWrapper: {
    width: 47,
    height: 27,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderRadius: 40,
    backgroundColor: '#D7D7D7',
    padding: 2,
  },
  backgroundActive: {
    backgroundColor: '#05BF71',
    alignItems: 'flex-end',
  },
  toggleSwitch: {
    width: 23,
    height: 23,
    backgroundColor: appVariables.colorWhite,
    borderRadius: 40,
  }
});

AToggle.propTypes = {
  onPress: PropTypes.func,
  isOn: PropTypes.bool
}

export default AToggle;