import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';

class ABackButtonCircle extends React.Component {
  render() {
    let { transparent, dark } = this.props;

    let bg = styles.orangeBg;
    if (transparent) { bg = styles.transparentBg;}

    let svgIcon = 'backArrow';
    if (dark) {svgIcon = 'backArrowBold';}

    return (
      <TouchableOpacity onPress={() => this.props.onPress()}>
        <View style={[styles.buttonColor, bg]}>
          <SvgIcon name={svgIcon} />
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  orangeBg: { backgroundColor: appVariables.colorOrange },
  transparentBg: { backgroundColor: 'transparent' },
  buttonColor: {
    width: 35,
    height: 35,
    alignSelf: 'flex-start',
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

ABackButtonCircle.propTypes = {
  onPress: PropTypes.func
}

export default ABackButtonCircle;