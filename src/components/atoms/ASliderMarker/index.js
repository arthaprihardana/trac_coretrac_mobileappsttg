import React from 'react';
import { StyleSheet, View } from 'react-native';

const ASliderMarker = () => <View style={styles.markerStyle} />

const styles = StyleSheet.create({
    markerStyle: {
        width: 20,
        height: 20,
        backgroundColor: 'white',
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        top: 1,
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6
    }
})

export default ASliderMarker;