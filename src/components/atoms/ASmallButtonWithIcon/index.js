import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';

const ASmallButtonWithIcon = ({ label, iconName }) => (
  <TouchableOpacity style={styles.moreButton}>
    <Text style={styles.moreButtonText}>{label}</Text>
    <SvgIcon style={styles.moreButtonIcon} name={iconName} />
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  moreButton: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 6,
    paddingBottom: 6,
    backgroundColor: appVariables.colorOrange,
    width: 83,
    height: 36,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
  },
  moreButtonText: {
    fontSize: appVariables.fontNormal,
    color: appVariables.colorWhite,
  },
  moreButtonIcon: {
    top: 5,
    marginLeft: 5,
    marginRight: 5
  },
})

ASmallButtonWithIcon.propTypes = {
  label: PropTypes.string,
  iconName: PropTypes.string
}

export default ASmallButtonWithIcon;