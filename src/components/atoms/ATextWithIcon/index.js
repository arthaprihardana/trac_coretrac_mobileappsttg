import { Text } from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';

const ATextWithIcon = ({ align, bgColor, icon, label, disabled, onPress }) => {
  let styleAddition = { justifyContent: 'flex-start', };
  if (align == 'right') styleAddition = { justifyContent: 'flex-end' };
  let contentText = (
    <View style={[{ backgroundColor: bgColor }, styleAddition, styles.tabWrapper]}>
      <SvgIcon style={styles.iconWrapper} name={icon} />
      <Text style={styles.label}>
        {label}
      </Text>
    </View>
  )
  if (disabled) return contentText;
  return (
    <TouchableOpacity style={styles.buttonWrapper} onPress={onPress}>
      {contentText}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  buttonWrapper: {
    flex: 1,
  },
  tabWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  label: {
    color: appVariables.colorWhite,
    fontSize: 12,
    paddingTop: 16,
    marginRight: 20
  },
  iconWrapper: {
    alignItems: 'center',
    marginLeft: 20,
    paddingTop: 12,
    marginRight: 10,
  }
})

ATextWithIcon.propTypes = {
  align: PropTypes.string,
  bgColor: PropTypes.string,
  icon: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  onPress: PropTypes.func
}

export default ATextWithIcon;