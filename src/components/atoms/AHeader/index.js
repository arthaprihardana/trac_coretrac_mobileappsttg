import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, View } from 'react-native';

const AHeader = ({ bgColor, children }) => {
  let bgColorStyle = {};
  if (bgColor) {
    bgColorStyle = { backgroundColor: bgColor }
  };

  return (
    <View style={[styles.headerWrapper, bgColorStyle]}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  headerWrapper: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
  }
});

AHeader.propTypes = {
  bgColor: PropTypes.string,
  children: PropTypes.any
}

export default AHeader;