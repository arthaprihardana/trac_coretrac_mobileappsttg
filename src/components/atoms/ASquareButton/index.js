import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';

const ASquareButton = ({ onPress, bgColor, icon, label }) => (
  <TouchableOpacity style={styles.button} onPress={() => onPress()}>
    <View style={[{ backgroundColor: bgColor }, styles.wrapper]}>
      <View style={styles.iconWrapper}>
        <SvgIcon name={icon} />
      </View>
      <Text style={styles.label}>{label}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignSelf: 'stretch',
    height: 110,
  },
  label: {
    color: 'white',
    fontSize: appVariables.fontSmall,
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  iconWrapper: {
    height: 34,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 10
  }
})

ASquareButton.propTypes = {
  onPress: PropTypes.func,
  bgColor: PropTypes.string,
  icon: PropTypes.string,
  label: PropTypes.string
}

export default ASquareButton;