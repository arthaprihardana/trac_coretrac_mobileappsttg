import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const AList = ({ children }) => (
  <View style={styles.container}>
    {children}
  </View>
);

const styles = StyleSheet.create({
  container: {
    borderBottomColor: '#2A2E36',
    borderBottomWidth: .3,
    padding: appVariables.padding,
    flex: 1,
  }
});

AList.propTypes = {
  children: PropTypes.any
}

export default AList;