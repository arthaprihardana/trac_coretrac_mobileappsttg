import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const ARentTitle = ({ bgColor, title }) => (
  <View style={[styles.titleWrapper, { backgroundColor: bgColor }]}>
    <Text style={styles.title}>{title}</Text>
  </View>
)

const styles = StyleSheet.create({
  titleWrapper: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
  },
  title: {
    fontSize: 24,
    fontFamily: appVariables.museo700,
    color: appVariables.colorWhite,
  }
});

ARentTitle.propTypes = {
  bgColor: PropTypes.string,
  title: PropTypes.string
}

export default ARentTitle;