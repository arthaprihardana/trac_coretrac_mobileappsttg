import PropTypes from 'prop-types';
import React from 'react';
import { Image, View } from 'react-native';
import { appStyles } from '../../../assets/styles';

const AFullScreenImage = ({ image }) => (
  <View style={appStyles.appImageBackground}>
    <Image style={appStyles.fullScreen} source={image} resizeMode='cover' />
  </View>
);

AFullScreenImage.propTypes = {
  image: PropTypes.any.isRequired
}

export default AFullScreenImage;