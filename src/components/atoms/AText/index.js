import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { appVariables } from '../../../assets/styles';

const TextCarName = ({ carName }) => {
    let { titleCarStyle } = styles;
    return <Text style={titleCarStyle}>{carName}</Text>
}

const AText = (props) => {
    if (props.carName) return <TextCarName {...props} />
    return <Text {...props}>{props.children}</Text>
}

const styles = StyleSheet.create({
    titleCarStyle: { fontSize: 18, color: '#2A2E36', fontFamily: appVariables.museo700 },
})

AText.propTypes = {
    carName: PropTypes.string
}
export default AText;