import React from 'react';
import { StyleSheet, View } from 'react-native';

const ASeparator = () => (
  <View style={styles.separator}></View>
);

const styles = StyleSheet.create({
  separator: {
    paddingTop: 20,
    flex: 1,
  }
});

export default ASeparator;