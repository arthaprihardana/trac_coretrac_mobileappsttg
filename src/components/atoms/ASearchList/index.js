import { Text } from 'native-base';
import React, { Component } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

class ASearchList extends Component {
  _renderItem(item, index, type) {
    let listStyle = {fontSize: 24};
    if (type == 'small') {listStyle = {fontSize: 16}}

    return (
      <TouchableOpacity 
        onPress={() => this.props.onPress(item.city)}
        key={index}
      >
        <Text id={item.id} style={[styles.text, listStyle]}>{item.city}</Text>
      </TouchableOpacity>
    );
  }

  _keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <FlatList 
        data={this.props.data}
        keyExtractor={this._keyExtractor}
        renderItem={({item, index}) => this._renderItem(item, index, this.props.type)}
      />
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: appVariables.colorTabInactive,
    paddingTop: 20,
    paddingBottom: 20,
    flex: 1,
    paddingLeft: 60,
  }
});

export default ASearchList;