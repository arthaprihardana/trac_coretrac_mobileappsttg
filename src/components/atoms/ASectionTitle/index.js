import { PropTypes } from 'prop-types';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';

const ASectionTitle = (props) => (
  <View style={styles.section}><Text style={styles.title}>{props.title}</Text></View>
)

const styles = StyleSheet.create({
  section: {
    marginBottom: 30,
  },
  title: {
    fontSize: 24,
    color: '#2A2E36',
    fontFamily: appVariables.museo700,
  }
});

ASectionTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default ASectionTitle;