/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 15:00:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 08:30:33
 */
import _ from "lodash";
import Lang from '../assets/lang';

export function objectToQueryString(obj) {
    var qs = _.reduce(obj, (result, value, key) => {
        if (!_.isNull(value) && !_.isUndefined(value)) {
            if (_.isArray(value)) {
                result += _.reduce(value, (result1, value1) => {
                    if (!_.isNull(value1) && !_.isUndefined(value1)) {
                        result1 += key + '=' + value1 + '&';
                        return result1
                    } else {
                        return result1;
                    }
                }, '')
            } else {
                result += key + '=' + value + '&';
            }
            return result;
        } else {
            return result
        }
    }, '').slice(0, -1);
    return qs;
};

export function greetings(lang) {
    var today = new Date();
    var hourNow = today.getHours();
    var greeting ;
    
    if (hourNow >= 0 && hourNow < 11) {
        greeting = Lang.goodMorning[lang];
    } else if (hourNow >= 11 && hourNow < 14) {
        greeting = Lang.goodDay[lang];
    } else if (hourNow >= 14 && hourNow < 18) {
        greeting = Lang.goodAfternoon[lang];
    } else if (hourNow >= 18) {
        greeting = Lang.goodNight[lang];
    }

    return greeting;
}
