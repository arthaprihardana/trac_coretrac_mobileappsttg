import React, { Component } from 'react';
import { Container, Content, Item, Label, Input, Footer, Button, Text,Left, Right, Header, Body, Title } from 'native-base';
import { Platform, Modal,StatusBar, View, TouchableOpacity } from 'react-native';
import styles from './style';
import appVariables from '../../../assets/styles/appVariables';
import SvgIcon from '../../../utils/SvgIcon';
import ModalChangePassword from './thisModals/ModalChangePassword';

class ChangePassword extends Component {
  state = {
    modalChangePassword:false,
  }

  handleChangePasswordNotify=()=>{
    this.setState({modalChangePassword:false})
  }
  
  render() {

    return (
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <Header>
          <Left style={{ flex: 1 }}>

          </Left>
          <Body style={{ flex: Platform.OS=="ios" ? 3 : 1 }}>
            <Title>
              <Text style={styles.headerLabel}>Change Password</Text>
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button transparent>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileLanding')}>
                <SvgIcon name="icoClose" />
              </TouchableOpacity>
            </Button>
          </Right>

        </Header>

        <Content style={{ backgroundColor: '#FFFFFF' }}>
          <View style={{ marginTop: 30 }} >
            <Item floatingLabel style={styles.floatingLabel}>
              <Label style={styles.titleLabel}>Old Password</Label>
              <Input secureTextEntry={true} />
            </Item>
          </View>
          <View style={{ marginTop: 20 }} >
            <Item floatingLabel style={styles.floatingLabel}>
              <Label style={styles.titleLabel}>New Password</Label>
              <Input secureTextEntry={true} />
            </Item>
          </View>
          <View style={{ marginTop: 20 }} >
            <Item floatingLabel style={styles.floatingLabel}>
              <Label style={styles.titleLabel}>Confirm New Password</Label>
              <Input secureTextEntry={true} />
            </Item>
          </View>
        </Content>

        <Modal animationType="none"  visible={this.state.modalChangePassword} onRequestClose={() => { }} >
            <ModalChangePassword action={this.handleChangePasswordNotify} />
        </Modal>

        <Footer style={{ backgroundColor: Platform.OS=="ios" ? '#FFFFFF' : '#12265B'  }}>
          <Button iconLeft full primary style={styles.bottomButton} onPress={()=> this.setState({modalChangePassword:true})}>
            <Text>CHANGE PASSWORD</Text>
          </Button>
        </Footer>
      </Container>
    )
  }
}

export default ChangePassword;