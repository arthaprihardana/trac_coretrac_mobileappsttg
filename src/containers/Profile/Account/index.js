import React, { Component } from 'react';
import { Container, Content, Thumbnail, Button, Text, Textarea } from 'native-base';
import { StatusBar, View, Image } from 'react-native';
import { ThisHeader } from '../../../containers/Explore/BusRental/FindBus/thisComponents'
import { appVariables } from '../../../assets/styles';
import { phonePrefix } from '../../../utils/DummyData';
import { Grid, Row, Col } from 'react-native-easy-grid';
import SvgIcon from '../../../utils/SvgIcon';
import ModalDropdown from 'react-native-modal-dropdown';
import styles from './style';
import Mask from 'react-native-mask';

class Account extends Component {
  state = {
    phoneData: phonePrefix,
    selectedPhoneNumber: phonePrefix[0].value,
    selectedPhoneImage: phonePrefix[0].image,
  }

  _renderPhoneNumber = (option, index) => {
    return (
      <View key={index} style={styles.dropdownItemWrapper}>
        <Mask shape={'circle'}>
          <Image source={option.image} style={styles.dropdownImage} />
        </Mask>
        <Text style={styles.dropdownText}>{option.value}</Text>
      </View>
    );
  }

  _phoneNumberSelected = (idx, value) => {
    this.setState({
      selectedPhoneNumber: value.value,
      selectedPhoneImage: value.image
    });
  }

  render() {
    const nav = this.props.navigation;

    return (
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ProfileLanding')} title="Account" />
        <Content style={{ backgroundColor: '#FFFFFF' }}>
          <Grid>
            <Row style={{ marginLeft: 10, marginTop: 20 }}>
              <Col style={{ width: '20%' }}>
                <Thumbnail style={{ width: 97, height: 97 }} source={require('../../../assets/images/png/avatar/changePhotoAvatar.png')} />
              </Col>
              <Col style={{ width: '80%', marginTop: 40, marginLeft: 30 }}>
                <Button bordered dark style={{ height: '70%' }}>
                  <Text style={{ color: '#000000' }}>CHANGE PHOTO</Text>
                </Button>
              </Col>
            </Row>
          </Grid>
          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Display Name</Text>
              </View>
            </View>

            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>ID Number</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>

              <View style={styles.labelContent}>
                <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="Bryan Barry" />
              </View>
              <View style={styles.editIcon}>
                <SvgIcon name="icoEdit" />
              </View>
            </View>

            <View style={styles.subContainer}>

              <View style={styles.labelContent}>
                <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="03256" />
              </View>
              <View style={styles.editIcon}>
                <SvgIcon name="" />
              </View>
            </View>
          </View>


          <Text style={styles.title}>Email</Text>
          <View style={styles.container}>
            <View style={styles.labelContent}>
              <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="bryanbarry@mail.com" />
            </View>
            <View style={styles.editIcon}>
              <SvgIcon name="icoEdit" />
            </View>
          </View>


          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Phone Number</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: 'column' }}>
              <View style={{ flex: 1 }}>
                <ModalDropdown
                  style={{ marginLeft: 20, flex: 1 }}
                  ref="phone_number"
                  options={this.state.phoneData}
                  onSelect={(idx, value) => this._phoneNumberSelected(idx, value)}
                  renderRow={this._renderPhoneNumber}>
                  <View style={styles.sortWrapper}>
                    <Mask style={styles.itemImage} shape={'circle'}><Image style={styles.image} source={this.state.selectedPhoneImage} /></Mask>
                    <Text style={styles.labelPhoneStyle}>{this.state.selectedPhoneNumber}</Text>
                    <SvgIcon name="icoSort" style={styles.iconWrapper} />
                  </View>
                </ModalDropdown>
              </View>
              <View style={{ flex: 3, flexDirection: 'row', marginLeft: 20, width: 73, borderBottomWidth: 0.5, borderBottomColor: appVariables.colorGray }}></View>
            </View>
            <View style={styles.subContainer}>
              <View style={styles.labelContent}>
                <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="812 1234 1234" />
              </View>
              <View style={styles.editIcon}>
                <SvgIcon name="icoEdit" />
              </View>
            </View>

          </View>
        </Content>
      </Container>
    )
  }
}

export default Account;