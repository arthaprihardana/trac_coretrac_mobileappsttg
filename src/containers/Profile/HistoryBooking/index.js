import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import { StatusBar } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import { ThisHeader } from '../../../containers/Explore/BusRental/FindBus/thisComponents'
import { OHistoryBookingSection } from '../../../components/organisms';

class HistoryBooking extends Component {
  render() {
    return (
      <Container>
         <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ProfileLanding')} title="History Booking" />
        <Content style={{backgroundColor:'white'}}>
        <OHistoryBookingSection />
        </Content>
      </Container>
    )
  }
}

export default HistoryBooking;