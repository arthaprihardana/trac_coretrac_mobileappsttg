import React, { Component } from 'react';
import { Container, Content, ListItem, Text, Left, Body, Thumbnail, List } from 'native-base';
import SvgIcon from '../../utils/SvgIcon';
import { StatusBar, View, TouchableOpacity } from 'react-native';
import { appVariables } from '../../assets/styles';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { ThisHeader } from '../../containers/Explore/BusRental/FindBus/thisComponents';
import listItems from './list';

class Profile extends Component {
  _renderListitem(iconList, textList, onPress, index) {
    return (
      <TouchableOpacity style={{
        flex: -1,
        flexDirection: "row",
        borderBottomWidth: 1,
        height: 50,
        alignItems: "center",
        borderColor: "#D8D8D8"
      }}
        onPress={() => onPress()}
        key={index}>
        <View style={{
          flex: -1
        }}>
          <SvgIcon name={iconList} />
        </View>
        <View style={{
          flex: 1,
          paddingLeft: 10
        }}>
          <Text style={{ fontSize: 12 }}>{textList}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const nav = this.props.navigation;

    return (
      <Container >
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ExploreLanding')} title="Profile" />

        <Content style={{ backgroundColor: 'white' }}>
          <Grid>
            <Row style={{ backgroundColor: '#21419A', height: 160 }}>
              <Col style={{ justifyContent: 'center', alignItems: 'center', width: '25%', marginLeft: 10, backgroundColor: '#21419A' }}>
                <Thumbnail style={{ width: 74, height: 74 }} source={require('../../assets/images/png/avatar/profileAvatar.png')} />
              </Col>
              <Col style={{ justifyContent: 'center', alignItems: 'center', width: '75%', marginLeft: 10, marginTop: 30, backgroundColor: '#21419A' }}>
                <Row>
                  <Col style={{ width: '40%', backgroundColor: '#21419A' }}>
                    <Text style={{ color: '#8A959E', fontSize: 12 }}>ID NUMBER</Text>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>03256</Text>
                  </Col>
                  <Col style={{ width: '60%', backgroundColor: '#21419A' }}>
                    <Text style={{ color: '#8A959E', fontSize: 12 }}>PHONE NUMBER</Text>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>089612334576</Text>
                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '100%', backgroundColor: '#21419A' }}>
                    <Text style={{ color: '#8A959E', fontSize: 12 }}>EMAIL</Text>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>bryanbarry@mail.com</Text>
                  </Col>
                </Row>
              </Col>
              <Col>
              </Col>
            </Row>
          </Grid>

          <List style={{ backgroundColor: 'white', paddingHorizontal: 20, }}>
            {listItems.map((item, index) => {
              return this._renderListitem(item.iconList, item.textList, () => this.props.navigation.navigate(item.listTarget), index);
            })}
          </List>
        </Content>
      </Container>
    )
  }
}

export default Profile;