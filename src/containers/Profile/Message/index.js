import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import { StatusBar, View, TouchableOpacity,Text } from 'react-native';
import appVariables from '../../../assets/styles/appVariables';
import { ThisHeader } from '../../../containers/Explore/BusRental/FindBus/thisComponents'
import { OMessageSection } from '../../../components/organisms';

class Message extends Component {
  
  render() {
    return (
      <Container>
         <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ProfileLanding')} title="Message" />
        <Content style={{backgroundColor:'white'}}>
        <OMessageSection />
        </Content>
      </Container>
    )
  }
}

export default Message;