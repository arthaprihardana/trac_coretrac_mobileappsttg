import React, { Component } from 'react';
import { Container, Content, Button, Text } from 'native-base';
import { StatusBar, View, TouchableOpacity, Linking, Image } from 'react-native';
import { ThisHeader } from '../../../containers/Explore/BusRental/FindBus/thisComponents';
import { appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';
import styles from './style';

class CreditCard extends Component {

  render() {
    const nav = this.props.navigation;

    return (
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ProfileLanding')} title="Credit Card" />
        <Content style={{ backgroundColor: '#FFFFFF' }}>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.openingWord}>We’ll autofill your details and make it easier for you to rent services through TRAC Your payment details are stored securely</Text>
          </View>

          <View style={styles.container}>
            <View style={{ marginLeft: 10, marginTop:5}}>
              <Image source={require('../../../assets/images/png/icoCheckBlue.png')} />
            </View>

            <View style={styles.subContainer}>
              <View>
                <Image source={require('../../../assets/images/png/payment/visaCard.png')} />
              </View>
            </View>

            <View style={styles.editIcon}>
              <View>
                <SvgIcon name="icoEdit" />
              </View>
            </View>
          </View>


          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Credit Card Number</Text>
              </View>
            </View>

            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Expiry Date</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.labelContent}>•••• 2222</Text>
              </View>
            </View>

            <View style={styles.subContainer}>
              <View>
                <Text style={styles.labelContent}>08/21</Text>
              </View>
            </View>
          </View>


          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Name On Card</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.labelContent}>Bryan Barry Borang</Text>
              </View>
            </View>
          </View>



          <View style={styles.lineSeparator}>
          </View>


          <View style={styles.container}>

            <View style={{ marginLeft: 10, marginTop:5 }}>
              <View>
                <Image source={require('../../../assets/images/png/icoCheckBlue.png')} />
              </View>
            </View>
            <View style={styles.subContainer}>
              <View>
                <Image source={require('../../../assets/images/png/payment/masterCard.png')} />
              </View>
            </View>

            <View style={styles.editIcon}>
              <View>
                <SvgIcon name="icoEdit" />
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Credit Card Number</Text>
              </View>
            </View>

            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Expiry Date</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.labelContent}>•••• 2222</Text>
              </View>
            </View>

            <View style={styles.subContainer}>
              <View>
                <Text style={styles.labelContent}>08/21</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.title}>Name On Card</Text>
              </View>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.subContainer}>
              <View>
                <Text style={styles.labelContent}>Bryan Barry Borang</Text>
              </View>
            </View>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <View>
                <TouchableOpacity onPress={() => Linking.openURL('http://google.com')}>
                  <Text style={{ fontSize: 14, color: appVariables.colorBlack, fontFamily: appVariables.museo500, flex: 1, lineHeight: 20, marginBottom: 5, marginTop: 5, marginLeft: 20, textDecorationLine: 'underline' }}>
                    Click here
                  </Text>

                </TouchableOpacity>
              </View>
            </View>
            <View style={{ flex: 5 }}>
              <View>
                <Text style={styles.closingWord}>to remove your TRAC Account.</Text>
              </View>
            </View>
          </View>



          <Text style={styles.closingWord}>We’ll be sad to see you go</Text>


          <Button bordered dark style={{ marginLeft: 20, marginTop: 30, width: 120 }}>
            <Text style={{ color: '#000000' }}>ADD CARD</Text>
          </Button>

        </Content>
      </Container>
    )
  }
}

export default CreditCard;