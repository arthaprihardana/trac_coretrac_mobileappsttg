import {StyleSheet} from 'react-native';
import { appVariables } from '../../../assets/styles';

const styles = StyleSheet.create({
  title:{
    fontSize: 14,
    color: appVariables.colorGray,
    fontFamily: appVariables.museo500,
    flex: 1,
    lineHeight: 20,
    marginBottom: 10,
    marginTop:10,
    marginLeft:45,
  },
  openingWord:{
    fontSize: 14,
    color: appVariables.colorGray,
    fontFamily: appVariables.museo500,
    flex: 1,
    lineHeight: 20,
    marginBottom: 10,
    marginTop:10,
    marginLeft:20,
  },
  closingWord:{
    fontSize:14,
    color: appVariables.colorBlack,
    fontFamily: appVariables.museo500,
    flex:1,
    lineHeight: 20,
    marginBottom: 5,
    marginTop:5,
    marginLeft:20,
  },
  container:{
    flexDirection: 'row',
  },
  subContainer:{
    flex:1,
    flexDirection: 'row',
  },
  labelContent: {
    flex: 9,
    height:35,
    borderBottomColor: appVariables.colorBlack,
    marginLeft:45,
    marginTop:-5, 
  },
  editIcon: {
    flex: 1,
    marginRight:20,
    alignItems: 'flex-end',
  },
  lineSeparator:{
    borderBottomColor: appVariables.colorGray,
    borderBottomWidth: 1,
    marginLeft:20,
    marginRight:20,
    marginBottom:20,
  }
})

export default styles;