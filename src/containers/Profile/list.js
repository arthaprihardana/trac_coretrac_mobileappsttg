export default [
  {
    iconList: "icoAccount",
    textList : "Account",
    listTarget :'AccountLanding',
  },
  {
    iconList: "icoIdentificationInformation",
    textList : "Identification Information",
    listTarget : 'IdentificationInformationLanding',
  },
  {
    iconList: "icoCreditCards",
    textList : "Credit Cards",
    listTarget : 'CreditCardLanding',
  },
  {
    iconList: "icoChangePassword",
    textList : "Change Password",
    listTarget : 'ChangePasswordLanding',
  },
  {
    iconList: "icoHistoryBooking",
    textList : "History Booking",
    listTarget : 'HistoryBookingLanding',
  },
  {
    iconList: "icoMessage",
    textList : "Message",
    listTarget : 'MessageLanding',
  },
  {
    iconList: "icoMyVoucher",
    textList : "My Voucher",
    listTarget : '',
  },
  {
    iconList: "icoLogout",
    textList : "Logout",
    listTarget : 'ExploreLanding',
  },
]

            

        
              
            