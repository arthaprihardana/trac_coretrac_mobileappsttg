import {StyleSheet} from 'react-native';
import { appVariables } from '../../../assets/styles';

const styles = StyleSheet.create({
  title:{
    fontSize: 12,
    color: appVariables.colorGray,
    fontFamily: appVariables.museo500,
    flex: 1,
    lineHeight: 20,
    marginBottom: 10,
    marginTop:20,
    marginLeft:20,
  },
  container:{
    flexDirection: 'row',
  },
  subContainer:{
    flex:1,
    flexDirection: 'row',
  },
  labelContent: {
    flex: 9,
    borderBottomWidth: .4,
    height:35,
    borderBottomColor: appVariables.colorGray,
    marginLeft:20,
    marginTop:-5, 
  },
  editIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textarea: { 
    paddingLeft: 0, left: 0, marginLeft: 0,
    fontSize: 12,
    color: appVariables.colorDark,
    fontFamily: appVariables.museo500,
    lineHeight: 20,
  },
  dropdownItemWrapper: { flexDirection: 'row', alignItems: 'center', borderBottomColor: '#C3C8D3', borderBottomWidth: 1, padding: 10, },
  dropdownImage: { width: 20, height: 20},
  dropdownText: { fontSize: 12, fontFamily: appVariables.museo500, color: '#242842', marginLeft: 10 },
  sortWrapper: { flexDirection: 'row', height: 30, alignItems: 'center' },
  itemImage: { marginRight: 5},
  image: { width: 20, height: 20 },
  labelPhoneStyle: { fontSize: 12, fontFamily: appVariables.museo500, marginLeft: 10, marginRight: 10, marginTop: 0, },
  iconWrapper: { marginTop: 0 },
  scanSquare: { flex: 1, alignSelf: 'stretch', borderWidth: 1, marginRight: 0, justifyContent: 'center', alignItems: 'center', borderColor: appVariables.colorGray, borderStyle: 'dashed', borderRadius: 10, padding: 20, width: 135, height: 90.08 },
  scanIcon: { justifyContent: 'center', alignItems: 'center'},
  photoFrame: {position:'absolute', top: 0, left: 0,justifyContent: 'center',alignItems:'center'},
  scanWording: {marginLeft:5,marginTop:5,fontSize:12, fontFamily:appVariables.museo500,color:appVariables.colorGray, textAlign:'center'}
})

export default styles;