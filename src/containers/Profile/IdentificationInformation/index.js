import React, { Component } from 'react';
import { Container, Content, Text, Textarea } from 'native-base';
import { StatusBar, View, Image, TouchableOpacity } from 'react-native';
import { ThisHeader } from '../../../containers/Explore/BusRental/FindBus/thisComponents';
import { appVariables } from '../../../assets/styles';
import SvgIcon from '../../../utils/SvgIcon';
import styles from './style';
import Mask from 'react-native-mask';
// import MCamera from '../../../components/molecules/MCamera';

class IdentificationInformation extends Component {

  // state={
  //   showModalCamera:false,
  //   cameraFor:"photoSIM",
  //   photoSIM: null,
  // }

  // openCamera = (cameraFor) => {
  //   this.setState({ cameraFor });
  //   this.setState({ showModalCamera: true });
  // }

  // closeCamera = () => {
  //   this.setState({ showModalCamera: false });
  // }

  // takePicture = (pic) => {
  //   const b64 = JSON.parse(pic);
  //   let theImage = b64.base64;
  //   this.setState({ [this.state.cameraFor]: `data:image/png;base64,${theImage}` })
  // }

  render() {
    const nav = this.props.navigation;

    return (
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ProfileLanding')} title="Identification Information" />
        <Content style={{ backgroundColor: '#FFFFFF' }}>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.title}>We’ll autofill your details and make it easier for you to rent services through TRAC. Your payment details are stored securely</Text>
          </View>

          <View style={styles.container}>
          {/* onPress={() => this.openCamera("photoSIM")} */}
            <TouchableOpacity style={{ flex: 1,marginLeft:20,marginTop:20 }}>
              <View style={styles.scanSquare}>
                <SvgIcon name="icoAddLicense" style={styles.scanIcon} />
                <Text style={styles.scanWording}>Add License Card</Text>
                <View style={styles.photoFrame}>
                  <Mask shape="rounded">
                    {/* <Image source={{ uri: this.state.photoSIM }} style={styles.imageSIM} resizeMode="cover" /> */}
                  </Mask>
                </View>
              </View>
            </TouchableOpacity>
          </View>

          <Text style={styles.title}>KTP Number</Text>
          <View style={styles.container}>
            <View style={styles.labelContent}>
              <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="123123176467249813" />
            </View>
            <View style={styles.editIcon}>
              <SvgIcon name="icoEdit" />
            </View>
          </View>

          <Text style={styles.title}>License Number (A)</Text>
          <View style={styles.container}>
            <View style={styles.labelContent}>
              <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="-" />
            </View>
            <View style={styles.editIcon}>
              <SvgIcon name="icoEdit" />
            </View>
          </View>

          <Text style={styles.title}>Address</Text>
          <View style={styles.container}>
            <View style={styles.labelContent}>
              <Textarea rowSpan={2} placeholder="" style={styles.textarea} value="Jl. Pecenongan Raya no. 123, Jakarta Pusat. 90384" />
            </View>
            <View style={styles.editIcon}>
              <SvgIcon name="icoEdit" />
            </View>
          </View>

        </Content>

        {/* <MCamera 
          visible={this.state.showModalCamera}
          onCapture={this.takePicture}
          onClose={this.closeCamera}
        /> */}

      </Container>
    )
  }
}

export default IdentificationInformation;