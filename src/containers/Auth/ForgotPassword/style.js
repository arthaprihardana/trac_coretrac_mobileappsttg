import { StyleSheet } from 'react-native';
import { appVariables } from '../../../assets/styles';

const styles = StyleSheet.create({
  link: {
    alignSelf: 'center',
    marginLeft: 5,
  },
  linkText: {
    color: appVariables.colorLink,
    fontFamily: appVariables.museo700,
  },
  bottomText: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  orText: {
    color: appVariables.colorGray
  },
  orContainer: {
    alignSelf: 'center', 
    flex: 1
  },
  buttonForgotPassword: {
    marginTop: 5, 
    fontSize: 12
  },
  inlineWrapper: {
    flex: 1, 
    flexDirection: 'row',
  },
  buttonRememberMe: {
    marginLeft: 8, 
    marginTop: 5, 
    fontSize: 12
  },
  socmedButtonWrapper: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  wrapper: {
    padding: appVariables.padding
  },
  card: {
    paddingBottom: appVariables.padding
  },
  cardBody: {
    paddingTop: 40,
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 10,
  },
})

export default styles;