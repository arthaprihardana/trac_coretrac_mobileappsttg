import { Body, Card, CardItem, Container, Content, Form, Input, Item, Label, Left, Footer} from 'native-base';
import React, { Component } from 'react';
import { Text } from 'react-native';
import { ABackButtonCircle, AButton, AHeader, ASeparator } from '../../../components/atoms';
import { MTitle } from '../../../components/molecules';
import styles from './style';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: false
    }
  }

  render() {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{ backgroundColor: 'white' }}>
        <AHeader>
          <Left>
            <ABackButtonCircle dark transparent onPress={() => goBack()} />
          </Left>
        </AHeader>
        
        <Content style={{ padding: 30 }}>
          
          <MTitle title="Forgot Password" left description={<Text>Enter your email address to reset your password</Text>} />
      
          <Form>
            <Body style={styles.cardBody}>
              <Item floatingLabel>
                <Label>Email Address</Label>
                <Input />
              </Item>

              <ASeparator />
            </Body>
          </Form>

        </Content>

        <Footer style={{ backgroundColor: 'transparent', paddingLeft: 30, paddingRight: 30 }}>
          <AButton primary block onPress={() => this.props.navigation.navigate('Otp')}>RESET PASSWORD</AButton>
        </Footer>
      </Container>
    )
  }
}

export default ForgotPassword;