import { Body, Card, CardItem, Container, Content, Form, Left, Footer } from 'native-base';
import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import { ABackButtonCircle, ABold, AButton, AHeader, ASeparator } from '../../../components/atoms';
import { MTitle } from '../../../components/molecules';
import styles from './style';

class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: false
    }
  }

  getOtp(code) {
    console.log(code);
  }

  render() {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{ backgroundColor: 'white' }}>
        <AHeader>
          <Left>
            <ABackButtonCircle dark transparent onPress={() => goBack()} />
          </Left>
        </AHeader>

        
        <Content style={{ padding: 30 }}>
          <Form>
            <MTitle title="Verification" left description={<Text>We have sent the verification code to <ABold>username@gmail.com</ABold> enter the code to verify.</Text>} />
            
            <Body style={styles.cardBody}>
              <CodeInput
                ref="otpLogin"
                codeLength={6}
                activeColor='transparent'
                inactiveColor='transparent'
                autoFocus={false}
                ignoreCase={true}
                inputPosition='left'
                onFulfill={(code) => this.getOtp(code) }
                containerStyle={styles.otpContainer}
                codeInputStyle={styles.otpInputContainer}
              />

              <TouchableOpacity><Text style={styles.resendTextButton}>Resend verification code?</Text></TouchableOpacity>
            </Body>
          </Form>
        </Content>

        <Footer style={{ backgroundColor: 'transparent', paddingLeft: 30, paddingRight: 30 }}>
          <AButton primary block onPress={() => {}}>SEND</AButton>
        </Footer>
      </Container>
    )
  }
}

export default Otp;