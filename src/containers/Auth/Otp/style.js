import { StyleSheet } from 'react-native';
import { appVariables } from '../../../assets/styles';

const styles = StyleSheet.create({
  link: {
    alignSelf: 'center',
    marginLeft: 5,
  },
  linkText: {
    color: appVariables.colorLink,
    fontFamily: appVariables.museo700,
  },
  bottomText: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  orText: {
    color: appVariables.colorGray
  },
  orContainer: {
    alignSelf: 'center', 
    flex: 1
  },
  buttonForgotPassword: {
    marginTop: 5, 
    fontSize: 12
  },
  inlineWrapper: {
    flex: 1, 
    flexDirection: 'row',
  },
  buttonRememberMe: {
    marginLeft: 8, 
    marginTop: 5, 
    fontSize: 12
  },
  socmedButtonWrapper: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  wrapper: {
    padding: appVariables.padding
  },
  loginHeader: {
    alignSelf: 'center',
    fontFamily: appVariables.museo700,
    marginBottom: 20,
  },
  card: {
    paddingBottom: appVariables.padding
  },
  cardBody: {
    paddingTop: 40,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    flex: 1,
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  content: {
    paddingLeft: 20, 
    paddingRight: 20,
  },
  otpContainer: { 
    marginBottom: 20, 
    alignSelf: 'flex-start',
    width: '100%', 
    flexDirection: 'row', 
    justifyContent: 'flex-start', 
    alignItems: 'flex-start',
  },
  otpInputContainer: { 
    color: appVariables.colorDark, 
    borderWidth: 1.5, 
    borderBottomColor: appVariables.colorGray, 
    alignSelf: 'flex-start',
    fontSize: 24,
  },
  resendTextButton: {
    color: appVariables.colorLink, 
    fontFamily: appVariables.museo700, 
    flex: 1,
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',
    textAlign: 'left'
  }
})

export default styles;