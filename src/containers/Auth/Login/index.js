import { Container, Content, H1, Left } from 'native-base';
import React, { Component } from 'react';
import { ABackButtonCircle, AHeader } from '../../../components/atoms';
import { OLogin } from '../../../components/organisms';
import styles from './style';

class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { goBack } = this.props.navigation;

    return (
      <Container>
        <AHeader>
          <Left>
            <ABackButtonCircle dark transparent onPress={() => this.props.navigation.navigate('TabNavigator')} />
          </Left>
        </AHeader>
        
        <Content style={{paddingLeft: 20, paddingRight: 20}}>
        
          <H1 style={styles.loginHeader}>Login</H1>

          <OLogin />
          
        </Content>
      </Container>
    )
  }
}

export default Login;