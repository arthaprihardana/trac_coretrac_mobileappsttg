import { Body, Card, CardItem, Container, Content, Form, Input, Item, Label, Left, Footer} from 'native-base';
import React, { Component } from 'react';
import { Text, View, ImageBackground, StatusBar, TouchableOpacity } from 'react-native';
import { ABackButtonCircle, AButton, AHeader, ASeparator } from '../../../components/atoms';
import { MTitle } from '../../../components/molecules';
import MBlueRadio from '../../../components/molecules/MBlueRadio';
import styles from './style';
import { appVariables } from '../../../assets/styles';
import ACircleIconButton from '../../../components/atoms/ACircleIconButton';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: false
    }
  }

  render() {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{ backgroundColor: 'white' }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={'#F7F7F7'}
        />

        <AHeader bgColor="#F7F7F7">
          <Left>
            <ABackButtonCircle dark transparent onPress={() => goBack()} />
          </Left>
        </AHeader>

        <Content>
          <View style={styles.headerImageContainer}>
            <ImageBackground source={require('../../../assets/images/png/register-header.png')} style={styles.bgImage}>
              <Text style={styles.registerTitle}>Register</Text>
            </ImageBackground>
          </View>

          <Form style={{ padding: 30 }}>
            <Body style={styles.cardBody}>
              <Item floatingLabel>
                <Label>Email Address</Label>
                <Input />
              </Item>

              <ASeparator/>
              <ASeparator/>

              <Item floatingLabel>
                <Label>Create Password</Label>
                <Input secureTextEntry={true} />
              </Item>

              <ASeparator />
            </Body>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <MBlueRadio 
                onPress={() => {this.setState({radio: !this.state.radio })}} 
                active={this.state.radio} 
                label={
                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text>I have read and understood the Astra Trac </Text>
                    <TouchableOpacity>
                      <Text style={{ color: appVariables.colorLink, fontFamily: appVariables.museo700}}>Privacy Policy and Term Condition</Text>
                    </TouchableOpacity>
                  </View>
                }
              />
            </View>

            <ASeparator />
            <ASeparator/>

            <View style={{ width: '100%'}}>
              <AButton block primary onPress={() => this.props.navigation.navigate('Otp')} >Register</AButton>
            </View>

            <ASeparator />

            <View style={{ width: '100%'}}>
              <Text style={{ alignSelf: 'center', flex: 1}}>or with</Text>
            </View>

            <ASeparator/>

            <View style={styles.socmedButtonWrapper}>
              <ACircleIconButton iconName={'icoFacebook'} bgColor={'#2C4F7E'} />
              <ACircleIconButton iconName={'icoGplus'} bgColor={'#AE2920'} />
            </View>

            <ASeparator/>
          </Form>

          <View style={{flex: 1, backgroundColor: '#F7F7F7', paddingHorizontal: 20, paddingVertical: 14, flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={{fontSize: 14}}>Already have an account? </Text><TouchableOpacity><Text style={{ color: appVariables.colorLink, paddingTop: 2, fontFamily: appVariables.museo700, fontSize: 14 }}>Login Here</Text></TouchableOpacity>
          </View>
        </Content>
      </Container>
    )
  }
}

export default Register;