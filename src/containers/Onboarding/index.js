/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 13:19:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 11:47:31
 */
import { Container, H2, Text } from 'native-base';
import React, { Component } from 'react';
import { ImageBackground, StatusBar, TouchableOpacity, View } from 'react-native';
import { Actions } from "react-native-router-flux";
// import Swiper from 'react-native-swiper';
import Carousel, { Pagination } from 'react-native-snap-carousel';
// import { connect } from "react-redux";
import { appStyles, appVariables, appMetrics } from '../../assets/styles';
import { AButton } from '../../components/atoms';
import SvgIcon from '../../utils/SvgIcon';
import styles from './style';

export default class Onboarding extends Component {
  state = {
    activeSlide: 0,
    sliderData: [
      {
        title: 'Transportation services on the go with TRAC to go',
        subtitle: 'From airport to your holiday destination, we always keep you company without hassle-free.',
        image: require("../../assets/images/png/onboarding/1.png")
      },
      {
        title: "Track your driver's location anywhere",
        subtitle: 'Wherever and whenever you are ready, our driver always accessible through your phone to get you to your next destination!',
        image: require("../../assets/images/png/onboarding/2.png")
      },
      {
        title: 'Various add ons to keep you moving on',
        subtitle: 'Customize your booking car based on your needs, we got them covered.',
        image: require("../../assets/images/png/onboarding/3.png")
      },
    ]
  }
  
  _renderItem = ({item, index}) => {
    let btn = null;
    if (index == 2) {
      btn = (
        <View style={{ paddingTop: 20}}>
        <AButton primary block style={styles.cta} onPress={() => {
          Actions.reset('Home', { first: true });
        }}>Get Started</AButton>
        <TouchableOpacity style={appStyles.mt20} onPress={() => {
          Actions.reset('Login', { first: true });
        }}><Text style={[styles.p, appStyles.alignCenter]}>I Already Have an Account</Text></TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={appStyles.appContainer}>
        <View style={appStyles.appImageBackground}>
          <ImageBackground style={appStyles.fullScreen} source={item.image} />
        </View>
        <View style={styles.logo}><SvgIcon name="tracLogoWhite" /></View>
        <View style={[appStyles.p20, styles.content]}>
          <H2 style={styles.h2}>{item.title}</H2>
          <Text style={styles.p}>{item.subtitle}</Text>
          { btn }
        </View>
      </View>
    )
  }

  get pagination () {
    const { sliderData, activeSlide } = this.state;
    
    return (
      <Pagination
        dotsLength={sliderData.length}
        activeDotIndex={activeSlide}
        containerStyle={{ backgroundColor: 'transparent', position: 'absolute', bottom: 0 }}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          marginHorizontal: 0,
          backgroundColor: 'rgba(255, 255, 255, 0.92)',
          alignSelf: 'center'
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.8}
      />
    );
  }

  render() {
    return (
      <Container style={appStyles.appContainer}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={appVariables.colorBlack}
        />

        <Carousel
          data={this.state.sliderData}
          renderItem={this._renderItem}
          sliderWidth={appMetrics.windowWidth}
          itemWidth={appMetrics.windowWidth}
          activeSlideAlignment='start'
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          onSnapToItem={(index) => this.setState({ activeSlide: index }) }
        />

        { this.pagination }
        
        <TouchableOpacity style={styles.skip} onPress={() => {
          Actions.reset('Home', { first: true });
        }}><Text style={styles.small}>Skip</Text></TouchableOpacity>
      </Container>
    )
  }
}

// const mapStateToProps = ({ isSkipWelcomeScreen }) => {
//   const { isSkip, goTo } = isSkipWelcomeScreen;
//   return { isSkip, goTo };
// }

// export default connect(mapStateToProps, {})(Onboarding);