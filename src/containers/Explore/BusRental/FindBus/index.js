import { Container, Content, Text } from 'native-base';
import React, { Component } from 'react';
import { Modal, StatusBar, StyleSheet, View, TouchableOpacity} from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ATextWithIcon } from '../../../../components/atoms';
import { OCurrency } from '../../../../components/organisms';
import { busList, currencyList } from '../../../../utils/DummyData';
import { BusItem, Sort, ThisFooter, ThisHeader } from './thisComponents';
import { ModalBusDetail, ModalFilter } from './thisModals';
import SvgIcon from '../../../../utils/SvgIcon';

class FindBus extends Component {
  state = {
    modalBusDetail: false,
    modalFilter: false,
    modalNotify: false,
    currencyListShow: false,
    currencyList: currencyList,
    idCurrency: 'IDR',
    busses: busList,
    totalBook: 0,
    totalPrice: 0,
    selectedBus: {}
  }
  handleSubmitNotify = () => {
    this.setState({ modalNotify: false })
  }
  currencyChange = (newCurrencyList) => {
    let IDCurrency = newCurrencyList.map(cty => {
      if (cty.selected) return cty.id
    })
    this.setState({
      currencyListShow: false,
      currencyList: newCurrencyList,
      idCurrency: IDCurrency
    })
  }
  counterChange = (newValue, changedBus) => {
    let newBusList = [...this.state.busses];
    let totalBook = 0;
    let totalPrice = 0;
    newBusList.map(bus => {
      if (bus.id === changedBus.id) {
        bus.numBook = newValue
      }
      totalBook = totalBook + bus.numBook;
      totalPrice = totalPrice + (bus.numBook * bus.price);
    })

    this.setState({
      bussess: newBusList,
      totalBook: totalBook,
      totalPrice: totalPrice
    })
  }

  seeDetail = (selectedBus) => {
    this.setState({
      selectedBus: selectedBus
    }, () => {
      this.setState({ modalBusDetail: true })
    })
  }

  render() {
    let { filterWrapper, col } = styles;
    let recommendedBussesList = this.state.busses.map((bus, i) => {
      if (bus.recommended === true) {
        return (
          <BusItem 
            key={i} 
            imgCar={bus.img} 
            carName={bus.name} 
            price={bus.price} 
            pessenger={bus.pessenger} 
            statusCar={bus.status} 
            statusNote={bus.statusNote} 
            karaoke={bus.karaoke}
            charger={bus.charger}
            wifi={bus.wifi}
            onCounterChange={(value) => this.counterChange(value, bus)} 
            seeDetail={() => this.seeDetail(bus)} 
            notifyMe={() => this.setState({ modalNotify: true })} 
          />
        );
      }
    })

    let optionalBussesList = this.state.busses.map((bus, i) => {
      if (bus.recommended === 'undefined' || bus.recommended == undefined || bus.recommended === false) {
        return (
          <BusItem 
            key={i} 
            imgCar={bus.img} 
            carName={bus.name} 
            price={bus.price} 
            pessenger={bus.pessenger} 
            statusCar={bus.status} 
            statusNote={bus.statusNote} 
            karaoke={bus.karaoke}
            charger={bus.charger}
            wifi={bus.wifi}
            onCounterChange={(value) => this.counterChange(value, bus)} 
            seeDetail={() => this.seeDetail(bus)} 
            notifyMe={() => this.setState({ modalNotify: true })} 
          />
        );
      }
    });

    let footerShow = null;
    if (this.state.totalBook > 0) footerShow = <ThisFooter totalCars={this.state.totalBook} totalPrice={this.state.totalPrice} totalDays={3} selectAction={() => this.props.navigation.navigate('OrderDetailBusRental')} />
    return (
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ExploreLanding')} title="Bus Rental" />
        <View style={filterWrapper}>
          <TouchableOpacity style={col}>
            <ATextWithIcon disabled label="BOOKING DETAIL" icon="icoOrderInformation" bgColor="#1A3681" align="left" />
            <View style={styles.rightArrow}><SvgIcon name="icoArrowRightWhite" /></View>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', borderWidth: 1, borderColor: '#D8D8D8', position: 'relative' }}>
          <Sort label="FILTER" icoSort="icoFilterDark" withoutValue onPress={() => this.setState({ modalFilter: true })}/>
          <Sort label="SORT" withoutValue />
          <Sort label="CURRENCY" value={this.state.idCurrency} noborder onPress={() => this.setState({ currencyListShow: !this.state.currencyListShow })} />
        </View>
        <Content>
          <View style={{ paddingBottom: 19 }}>
            <View style={[styles.headerRecommended, styles.subHeader]}>
              <Text style={[styles.textSubHeader, {color: appVariables.colorWhite}]}>Recommended For You!</Text>
            </View>
            {recommendedBussesList}
            <View style={[styles.subHeader, styles.headerOptional]}>
              <Text style={styles.textSubHeader}>Other Bus Options For You</Text>
            </View>
            {optionalBussesList}
          </View>
        </Content>
        {
          this.state.currencyListShow ?
            <OCurrency currencyList={this.state.currencyList} onChange={(idCurrency) => this.currencyChange(idCurrency)} />
            : null
        }
        <Modal animationType="slide" transparent={false} visible={this.state.modalBusDetail} onRequestClose={() => { }} >
          <ModalBusDetail busDetail={this.state.selectedBus} onClose={() => this.setState({ modalBusDetail: false })} />
        </Modal>

        <Modal animationType="slide" transparent={false} visible={this.state.modalFilter} onRequestClose={() => { }} >
          <ModalFilter onClose={() => this.setState({ modalFilter: false })} />
        </Modal>
        
        <Modal animationType="fade" transparent={true} visible={this.state.modalNotify} onRequestClose={() => { }} >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%' }}>
          </View>
        </Modal>
        {footerShow}
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  filterWrapper: {
    flexDirection: 'row',
    height: 42,
    borderWidth: 1,
    borderTopColor: appVariables.colorTabInactive,
    backgroundColor: '#1A3681'
  },
  col: {
    flex: 1,
    flexDirection: 'row',
  },
  rightArrow: {
    marginRight: 20,
    marginTop: 16
  },
  subHeader: {
    padding: 20
  },
  headerRecommended: {
    backgroundColor: appVariables.colorBlueHeader
  },
  headerOptional: {
    backgroundColor: '#E2E6EA'
  },
  textSubHeader: {
    fontFamily: appVariables.museo700,fontSize:20
  },
})

export default withNavigation(FindBus);