import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { AButton, ACar, APricePackage, AText, AFiturIcon } from '../../../../../components/atoms';
import { OCounter } from '../../../../../components/organisms';
import SvgIcon from '../../../../../utils/SvgIcon';

class BusItem extends Component {
    render() {
        let { seeDetail, carName, price, pessenger, karaoke, charger, wifi, imgCar, statusNote, statusCar, onCounterChange, notifyMe } = this.props;
        return (
            <View style={{ backgroundColor: 'white' }}>
              <ACar carUri={imgCar} statusNote={statusNote} statusCar={statusCar} />
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', marginHorizontal: 20 }}>
                <AText carName={carName} />
                <APricePackage price={price} />
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopColor: '#D8D8D8', borderTopWidth: 1, marginHorizontal: 20, paddingTop: 11, marginTop: 12, paddingBottom: 10 }}>
                <AFiturIcon label={pessenger} iconName="icoPeople"/>
                <AFiturIcon label={karaoke} iconName="karaoke" check={true} />
                <AFiturIcon label={wifi} iconName="icoWifiGray" check={true} />
                <AFiturIcon label={charger} iconName="icoElectricity" check={true} />
              </View>
              <View style={styles.seatContainer}>
                <View><Text>Seating Arrangement</Text></View>
                <View style={styles.seats}>
                  <SvgIcon name="icoSeats" style={styles.k} />
                  <SvgIcon name="icoSeats" style={styles.k} />
                  <SvgIcon name="icoSeats" style={styles.k} />
                  <View style={styles.seatSeparator}></View>
                  <SvgIcon name="icoSeats" style={styles.k} />
                  <SvgIcon name="icoSeats" style={styles.k} />
                </View>
              </View>
              <View style={{ backgroundColor: '#F3F4F5', flexDirection: 'row', paddingVertical: 8, justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center' }}>
                <View>
                  <AButton arrow title="SEE DETAIL" onPress={seeDetail} />
                </View>
                <View>
                  <OCounter onChange={onCounterChange} />
                </View>
              </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  seatContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 20
  },
  seats: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
  seatSeparator: {
    marginHorizontal: 5
  },
  k: {
    marginHorizontal: 1
  }
});

BusItem.propTypes = {
    seeDetail: PropTypes.func, 
    carName: PropTypes.string, 
    price: PropTypes.number, 
    passanger: PropTypes.number, 
    suitcase: PropTypes.number, 
    transmition: PropTypes.string, 
    imgCar: PropTypes.any, 
    statusNote: PropTypes.string, 
    statusCar: PropTypes.number, 
    onCounterChange: PropTypes.func, 
    notifyMe: PropTypes.func
}

export default BusItem;