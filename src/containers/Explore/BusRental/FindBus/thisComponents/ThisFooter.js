import { Footer } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { MFooterSummary } from '../../../../../components/molecules';

class ThisFooter extends Component {
    render() {
        let { totalCars, totalPrice, totalDays, selectAction } = this.props;
        let { footerWrapper } = styles;
        return (
            <Footer style={footerWrapper}>
              <MFooterSummary bookingType='bus' totalCars={totalCars} totalPrice={totalPrice} totalDays={totalDays} selectAction={selectAction} />
            </Footer>
        )
    }
}

const styles = StyleSheet.create({
    footerWrapper: { height: 'auto' },
})

ThisFooter.propTypes = {
    totalCars: PropTypes.number,
    totalPrice: PropTypes.number,
    totalDays: PropTypes.number,
    selectAction: PropTypes.func
}

export default ThisFooter;