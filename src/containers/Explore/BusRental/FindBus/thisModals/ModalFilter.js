import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Container, Content, Footer } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../../assets/styles';
import { AButton, ASliderMarker } from '../../../../../components/atoms';
import { OFilterCarSelect, ORadioCircle } from '../../../../../components/organisms';
import SvgIcon from '../../../../../utils/SvgIcon';
import MBlueRadio from '../../../../../components/molecules/MBlueRadio';

const SectionHeader = ({ label, actionClear }) => {
  return (
    <View style={styles.sectionHeaderStyle}>
      <Text style={styles.titleSectionHeaderStyle}>{label.toUpperCase()}</Text>
      <TouchableOpacity onPress={actionClear}>
        <Text style={styles.clearSetionHeaderStyle}>clear</Text>
      </TouchableOpacity>
    </View>
  )
}

const PriceCompareItem = ({label, value}) => {
  return (
    <View>
      <Text style={styles.labelPriceCompare}>{label}</Text>
      <Text style={styles.valuePriceCompare}>RP {value}</Text>
    </View>
  )
}

class ModalFilter extends Component {
  state = {
    carType: [
      {
        id: 1,
        name: "MVP"
      },
      {
        id: 2,
        name: "City Car"
      },
      {
        id: 3,
        name: "Luxury Car"
      }
    ],
    facilities: [
      {
        id: 1,
        name: "KARAOKE"
      },
      {
        id: 2,
        name: "MEETING"
      },
      {
        id: 3,
        name: "TYPE 1"
      },
      {
        id: 4,
        name: "TYPE 2"
      }
    ],
    selectedFacilities: null,
    selectedBusType: null,
    busType: [
      {
        id: 1,
        type: 'LUXURY BUS',
        busses: [
          {
            id: 1,
            name: "Type A",
            selected: false
          },
          {
            id: 2,
            name: "Type B",
            selected: false
          },
        ]
      },
      {
        id: 2,
        type: 'BIG BUS',
        busses: [
          {
            id: 1,
            name: "59 Seat",
            selected: false
          },
          {
            id: 2,
            name: "40 Seat (Toilet)",
            selected: false
          },
          {
            id: 3,
            name: "48 Seat",
            selected: false
          }
        ]
      },
      {
        id: 3,
        type: 'MEDIUM BUS',
        busses: [
          {
            id: 1,
            name: "35 Seat Long",
            selected: false
          },
          {
            id: 2,
            name: "35 Seat Short",
            selected: false
          },
          {
            id: 3,
            name: "31 Seat",
            selected: false
          },
          {
            id: 4,
            name: "29 Seat",
            selected: false
          }
        ]
      },
      {
        id: 4,
        type: 'COASTER',
        busses: [
          {
            id: 1,
            name: "23 Seat",
            selected: false
          },
          {
            id: 2,
            name: "18 Seat",
            selected: false
          }
        ]
      },
      {
        id: 5,
        type: 'SMALL BUS',
        busses: [
          {
            id: 1,
            name: "15 Seat",
            selected: false
          },
          {
            id: 2,
            name: "13 Seat",
            selected: false
          },
          {
            id: 3,
            name: "11 Seat",
            selected: false
          }
        ]
      }
    ]
  }

  render() {
    let { onClose } = this.props;
    let { minPriceShow, maxPriceShow } = this.state;
    let { containerStyle, headerWrapper, contentStyle, sectionWrapper, priceWrapper, footerWrapper } = styles;

    const facilities = this.state.facilities.map((item) => {
      return (
        <View style={styles.radio}>
          <MBlueRadio 
            touchText 
            label={item.name} 
            active={this.state.selectedFacilities == item.name} 
            key={item.id} 
            onPress={() => this.setState({selectedFacilities: item.name})}  
            style={styles.radio}
          />
        </View>
      )
    });

    const busType = this.state.busType.map((item) => {
      let theBus = item.busses.map((kind) => {
        return (
          <View style={styles.radio}>
            <MBlueRadio 
              touchText 
              key={kind.id}
              label={kind.name} 
              active={this.state.selectedBusType == kind.name} 
              key={kind.id} 
              style={styles.radio}
              onPress={() => this.setState({selectedBusType: kind.name})}  
            />
          </View>
        )
      });

      return (
        <View style={styles.busWrapper}>
          <View style={styles.radio}>
            <MBlueRadio 
              touchText 
              key={item.id}
              label={item.type} 
              active={this.state.selectedCarType == item.type} 
              key={item.id} 
              onPress={() => this.setState({selectedCarType: item.type})}  
            />
          </View>

          <View style={styles.kind}>
            {theBus}
          </View>
        </View>
      )
    });

    return (
      <Container style={containerStyle}>
        <View style={headerWrapper}>
          <TouchableOpacity onPress={onClose}>
            <SvgIcon name="icoCloseDark" />
          </TouchableOpacity>
        </View>
        <Content>
          <View style={contentStyle}>
            <View style={sectionWrapper}>
              <SectionHeader label="Type" actionClear={() => alert('action clear')} />
              { facilities }
            </View>
            <View style={sectionWrapper}>
              <SectionHeader label="Type" actionClear={() => alert('action clear')} />
              { busType }
            </View>
          </View>
        </Content>
        <Footer style={footerWrapper}>
          <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', paddingVertical: 12 }}>
            <View style={{ flex: 1, marginHorizontal: 10, marginLeft: 20 }}>
              <AButton bordered info block onPress={() => { }}>RESET</AButton>
            </View>
            <View style={{ flex: 1, marginHorizontal: 10, marginRight: 20 }}>
              <AButton info block onPress={onClose}>APPLY</AButton>
            </View>
          </View>
        </Footer>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  containerStyle: { backgroundColor: '#F3F4F5' },
  headerWrapper: { width: '100%', justifyContent: 'flex-end', flexDirection: 'row', padding: 20 },
  contentStyle: { paddingBottom: 40 },
  sectionWrapper: { borderBottomColor: '#2A2E36', borderBottomWidth: 0.31, paddingBottom: 30, paddingHorizontal: 20, marginTop: 20 },
  sectionHeaderStyle: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 20 },
  titleSectionHeaderStyle: { fontSize: 12, fontFamily: appVariables.museo900, color: '#2A2E36' },
  clearSetionHeaderStyle: { fontSize: 12, fontFamily: appVariables.museo500, color: '#2246A8' },
  priceWrapper: { flexDirection: 'row', justifyContent: 'center', marginTop: 20 },
  labelPriceCompare: { textAlign: 'center', color: '#F27C21', fontFamily: appVariables.museo900, fontSize: 12 },
  valuePriceCompare: { textAlign: 'center', color: '#2A2E36', fontFamily: appVariables.museo900, fontSize: 12, marginTop: 8 },
  footerWrapper: { height: 'auto', shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 10},
  kind: {
    flex: 1, 
    flexWrap: 'wrap',
    flexDirection: 'row', 
    marginTop: 5,
  },
  radio: {
    marginBottom: 10,
    marginRight: 20
  },
  busWrapper: {
    marginBottom: 30,
  }
})

ModalFilter.propTYpes = {
  onClose: PropTypes.func
}

export default withNavigation(ModalFilter);