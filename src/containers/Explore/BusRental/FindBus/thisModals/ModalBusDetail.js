import { Container, Content } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image } from 'react-native';
import { ACar, APricePackage, AText, ATextFitur } from '../../../../../components/atoms';
import { MList } from '../../../../../components/molecules';
import SvgIcon from '../../../../../utils/SvgIcon';
import Carousel from 'react-native-snap-carousel'; 
import AFiturIcon from '../../../../../components/atoms/AFiturIcon';
import Mask  from 'react-native-mask';
import appMetrics from '../../../../../assets/styles/appMetrics';

class ModalBusDetail extends Component {
  _renderItem({item, index}) {
    return (
      <Mask shape={'square'}>
        <Image source={item.image} style={styles.promoImage} resizeMode='cover' />
      </Mask>
    );
  }

  render() {
    let { onClose, busDetail } = this.props;
    return (
      <Container>
        <View style={{ width: '100%', justifyContent: 'flex-end', flexDirection: 'row', padding: 20 }}>
          <TouchableOpacity onPress={onClose}>
            <SvgIcon name="icoCloseDark" />
          </TouchableOpacity>
        </View>
        <Content>
          <View style={{ paddingBottom: 40 }}>
            <View style={{alignItems: 'center'}}>
              <AText carName={busDetail.name} />
              <APricePackage price={busDetail.price} />
            </View>
            <ACar carUri={busDetail.img} label="CAR ON BOOKING" onBooking />
            <View style={{ flexDirection: 'row', marginBottom: 30, justifyContent: 'space-between', borderTopColor: '#D8D8D8', borderTopWidth: 1, borderBottomColor: '#D8D8D8', borderBottomWidth: 1, marginHorizontal: 20, paddingTop: 11, marginTop: 12, paddingBottom: 10 }}>
              <AFiturIcon label={'6'} iconName="icoPeople"/>
              <AFiturIcon label={'3'} iconName="karaoke" check={true} />
              <AFiturIcon label={''} iconName="icoWifiGray" check={true} />
              <AFiturIcon label={''} iconName="icoElectricity" check={true} />
            </View>
            <View>
              <Carousel
                data={busDetail.interior}
                renderItem={this._renderItem}
                sliderWidth={appMetrics.windowWidth}
                itemWidth={146}
                activeSlideAlignment='start'
                containerCustomStyle={{paddingLeft: 20, marginBottom: 20}}
                slideStyle={{marginRight: 10}}
                inactiveSlideOpacity={1}
                inactiveSlideScale={1}
              />
            </View>
            {
              busDetail.moreDetail.map((detail, i) => {
                return <MList key={i} label={detail.labelDetail} listItem={detail.contents} />
              })
            }
          </View>
        </Content>
      </Container>
    )
  }
}

ModalBusDetail.propTypes = {
  onClose: PropTypes.func, 
  carDetail: PropTypes.object
}

const styles = StyleSheet.create({
  promoImage: {
    width: 146, 
    height: 91
  },
});

export default ModalBusDetail;