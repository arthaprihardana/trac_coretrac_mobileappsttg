import { Body, Button, Container, Content, Form, Header, Input, Item, Left, Text, Title } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import { appVariables } from '../../../../assets/styles';
import { AFormTitle, ASeparator } from '../../../../components/atoms';
import { MBlueRadio, MFloatingInput, MFooterTotalPayment, MSortDropdown } from '../../../../components/molecules';
import { phonePrefix } from '../../../../utils/DummyData';
import SvgIcon from '../../../../utils/SvgIcon';
import ModalOrderDetail from '../modalOrderDetail';
import ModalDropdown from 'react-native-modal-dropdown';
import styles from './style';
import Mask from 'react-native-mask';
import MStepper from '../../../../components/molecules/MStepper';

class PaymentInfo extends Component {
  state = {
    togglePassenger: false,
    toggleSwitch: false,
    showModalOrderDetail: false,
    phoneData: phonePrefix,
    selectedPhoneNumber: phonePrefix[0].value,
    selectedPhoneImage: phonePrefix[0].image,
    selectedPhoneNumberPassenger: phonePrefix[0].value,
    selectedPhoneImagePassenger: phonePrefix[0].image,
  }

  togglePassenger = () => {
    this.setState({
      togglePassenger: !this.state.togglePassenger
    })
  }

  _renderPhoneNumber = (option, index, isSelected) => {
    return (
      <View key={index} style={styles.dropdownItemWrapper}>
        <Mask shape={'circle'}>
          <Image source={option.image} style={styles.dropdownImage}  />
        </Mask>
        <Text style={styles.dropdownText}>{option.value}</Text>
      </View>
    );
  }

  _phoneNumberSelected = (idx, value) => {
    this.setState({
      selectedPhoneNumber: value.value,
      selectedPhoneImage: value.image
    });
  }

  _phoneNumberPassengerSelected = (idx, value) => {
    this.setState({
      selectedPhoneNumberPassenger: value.value,
      selectedPhoneImagePassenger: value.image
    });
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <Header>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body>
            <Title>Personal Detail</Title>
          </Body>
        </Header>

        <MStepper step={2} labels={['Bus Order', 'Personal Data', 'Payment']} />

        <Content padder>
          <Form>
            {/* <AToggle isOn={this.state.toggleSwitch} onPress={() => this.setState({toggleSwitch: !this.state.toggleSwitch})}  /> */}

            <AFormTitle title={'Personal Information'} />

            <MFloatingInput 
              label={'Full Name'}
              notes={'As on ID Card/Passport/driving license'}
            />

            <View style={{flexDirection: 'column', marginTop: 20,}}>
              <Text style={{fontFamily: appVariables.museo500, fontSize: 16, flex: 1, marginBottom: 10, color: appVariables.colorLabelActive}}>Phone Number</Text>
              <View style={{flex: 1, flexDirection: 'row',}}>
                <View style={{flex: 1, height: 30, borderBottomWidth: 1, borderBottomColor: appVariables.colorBorderColor,}}>
                  <ModalDropdown
                    ref="phone_number"
                    options={this.state.phoneData}
                    onSelect={(idx, value) => this._phoneNumberSelected(idx, value)}
                    renderRow={this._renderPhoneNumber}
                    >
                    <View style={styles.sortWrapper}>
                      <Mask style={styles.itemImage} shape={'circle'}><Image style={styles.image} source={this.state.selectedPhoneImage} /></Mask>
                      <Text style={styles.labelStyle}>{this.state.selectedPhoneNumber}</Text>
                      <SvgIcon name="icoSort" style={styles.iconWrapper} />
                    </View>
                  </ModalDropdown>
                </View>
                <View style={{flex: 3, height: 30,}}>
                  <Item style={{ margin: 0, padding: 0}} >
                    <Input style={{ height: 29, paddingBottom: 2, paddingTop: 0, marginTop: 0, marginBottom: 0, }} />
                  </Item>
                </View>
              </View>
            </View>

            <ASeparator />
            <ASeparator />
            <View>
              <MBlueRadio  
                touchText={true}
                label={<Text style={styles.label} onPress={() => this.togglePassenger()}>I'm booking for someone else</Text>}
                active={this.state.togglePassenger}
                onPress={() => this.togglePassenger()}
              />
            </View>

            <ASeparator />
            <ASeparator />

            {
              ((this.state.togglePassenger === true) ? (
                <View>
                  <AFormTitle title={'Pessenger Information'} notes={"Please fill in the detail of traveler's name"} />
                  <MFloatingInput 
                    label={'Full Name'}
                    notes={'As on ID Card/Passport/driving license'}
                  />

                  <View style={{flexDirection: 'column', marginTop: 20,}}>
                    <Text style={{fontFamily: appVariables.museo500, fontSize: 16, flex: 1, marginBottom: 10, color: appVariables.colorLabelActive}}>Phone Number</Text>
                    <View style={{flex: 1, flexDirection: 'row',}}>
                      <View style={{flex: 1, height: 30, borderBottomWidth: 1, borderBottomColor: appVariables.colorBorderColor,}}>
                        <ModalDropdown
                          ref="phone_number"
                          options={this.state.phoneData}
                          onSelect={(idx, value) => this._phoneNumberPassengerSelected(idx, value)}
                          renderRow={this._renderPhoneNumber}
                          >
                          <View style={styles.sortWrapper}>
                            <Mask style={styles.itemImage} shape={'circle'}><Image style={styles.image} source={this.state.selectedPhoneImagePassenger} /></Mask>
                            <Text style={styles.labelStyle}>{this.state.selectedPhoneNumberPassenger}</Text>
                            <SvgIcon name="icoSort" style={styles.iconWrapper} />
                          </View>
                        </ModalDropdown>
                      </View>
                      <View style={{flex: 3, height: 30,}}>
                        <Item style={{ margin: 0, padding: 0}}>
                          <Input style={{ height: 29, paddingBottom: 2, paddingTop: 0, marginTop: 0, marginBottom: 0, }} />
                        </Item>
                      </View>
                    </View>
                  </View>
                </View>
              )
              : null)
            }

            <ASeparator />
            <ASeparator />

            <Button primary block onPress={() => this.props.navigation.navigate('BusRentalPaymentPage')}><Text>CONTINUE TO PAYMENT</Text></Button>

            <ASeparator />
          </Form>
        </Content>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

        <ModalOrderDetail 
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({showModalOrderDetail: false})}
        />

      </Container>
    )
  }
}

export default PaymentInfo;