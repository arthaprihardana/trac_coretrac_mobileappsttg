import { Container, Content, Footer, Tab, Tabs, ScrollableTab } from 'native-base';
import React, { Component } from 'react';
import { Image, Modal, ScrollView, StatusBar, StyleSheet, Text, View, ImageBackground } from 'react-native';
import { appVariables, appMetrics } from '../../../../assets/styles';
import { APricePackage } from '../../../../components/atoms';
import { MFooterSummary, MList } from '../../../../components/molecules';
import { OCarShowDetail } from '../../../../components/organisms';
import { bookedBusList } from '../../../../utils/DummyData';
import { ThisHeader } from './thisComponent';
import { ModalOrderReview, ModalTerms } from './thisModals';
import AFiturIcon from '../../../../components/atoms/AFiturIcon';
import SvgIcon from '../../../../utils/SvgIcon';
import MAddExtras from '../../../../components/molecules/MAddExtras';
import ExtraPage from './thisComponent/ExtraPage';

class OrderDetailBusRental extends Component {
    state = {
        modalReview: false,
        cars: bookedBusList,
        activeCar: {},
        totalPrice: 0,
        showDetailCar: false,
        addFood: false,
        addAccompanion: false,
        addPartner: false,
        modalTerms: false,
    }
    
    selectCarActive = () => {
        let carList = [...this.state.cars];
        let indexCarActive = carList.findIndex(car => car.isActive);
        this.setState({
            activeCar: carList[indexCarActive]
        })
    }

    handleCarSelect = (newCarsList) => {
        this.setState({
            cars: newCarsList
        }, () => this.selectCarActive())
    }

    handleExtraChange = (value, id) => {
        let carActive = this.state.activeCar;
        let carList = [...this.state.cars];
        let carActiveIndex = carList.findIndex(car => car.id === carActive.id);

        let extras = carList[carActiveIndex].extras;
        let extrasIndex = extras.findIndex(extra => extra.id === id);
        extras[extrasIndex].value = value;

        this.setState({
            cars: carList
        }, () => {
            this.calculateTotalPrice()
        })
    }

    calculateTotalPrice = () => {
        let totalPrice = 0;
        this.state.cars.map(car => {
            totalPrice = totalPrice + car.price;
            car.extras.map(extra => {
                if (typeof extra.value == 'number') {
                    totalPrice = totalPrice + (extra.value * extra.priceFitur);
                }
                if (typeof extra.value == 'boolean') {
                    if (extra.value) {
                        totalPrice = totalPrice + extra.priceFitur
                    }
                }
            })
        })
        this.setState({
            totalPrice: totalPrice
        })
    }

    componentDidMount() {
        this.selectCarActive();
        this.calculateTotalPrice();
    }

    render() {
        let { activeCar, showDetailCar } = this.state;
        let { lineStyle, carNameStyle, providerStyle, mainFiturWrapper, mainDescWrapper, sectionCarWrapper, carImgWrapper, carImgStyle, footerWrapper, extraItemsWrapper, extraItemsTitleStyle, extraItemContent, priceContainer, priceDetails, promoPriceContainer, promoPrice, activePrice} = styles;

        let listDetailCar = null;
        if (activeCar.moreDetail && showDetailCar) {
            listDetailCar = activeCar.moreDetail.map((detail, i) => {
                return (
                    <View key={i} style={{marginTop: 20}}>
                        <MList label={detail.labelDetail} listItem={detail.contents} />
                    </View>
                )
            })
        }

        return (
            <Container>
                <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
                <ThisHeader backAction={() => this.props.navigation.navigate('FindBus')} removeAction={() => this.props.navigation.navigate('FindBus')} />
                <Content>
                    <View>
                        <View style={mainDescWrapper}>
                          <Text style={carNameStyle}>{activeCar.name}</Text>
                          <Text style={providerStyle}>Bus Rental</Text>
                          <View style={mainFiturWrapper}>
                            <AFiturIcon label={'6'} white iconName="icoPeople"/>
                            <AFiturIcon label={'3'} white iconName="karaoke" check={true} />
                            <AFiturIcon label={''} white iconName="icoWifiGray" check={true} />
                            <AFiturIcon label={''} white iconName="icoElectricity" check={true} />
                          </View>
                          <View style={styles.seatContainer}>
                            <View><Text style={{color: appVariables.colorWhite}}>Seating Arrangement</Text></View>
                            <View style={styles.seats}>
                              <SvgIcon name="icoSeats" style={styles.k} />
                              <SvgIcon name="icoSeats" style={styles.k} />
                              <SvgIcon name="icoSeats" style={styles.k} />
                              <View style={styles.seatSeparator}></View>
                              <SvgIcon name="icoSeats" style={styles.k} />
                              <SvgIcon name="icoSeats" style={styles.k} />
                            </View>
                          </View>
                        </View>
                        <View style={sectionCarWrapper}>
                            <View style={carImgWrapper}>
                                <Image source={activeCar.img} style={carImgStyle} />
                            </View>
                        </View>
                        <View style={priceContainer}>
                            <View style={priceDetails}>
                                <View style={promoPriceContainer}>
                                  <Text style={promoPrice}>Rp. {activeCar.price}</Text>
                                </View>

                                <View style={activePrice}>
                                  <APricePackage price={activeCar.price} />
                                </View>
                            </View>
                        </View>
                        <View style={extraItemsWrapper}>
                          <Text style={extraItemsTitleStyle}>WOULD YOU LIKE ANY EXTRAS ITEMS?</Text>
                          <View style={extraItemContent}>
                            <MAddExtras
                              icoName="icoFood"
                              label="FOOD AND BEVERAGE"
                              selected={this.state.addFood}
                              onPress={() => this.setState({
                                addFood: !this.state.addFood,
                                addAccompanion: false,
                                addPartner: false
                              })}
                            />
                            <MAddExtras
                              icoName="icoPlayground"
                              label="TRAVEL ACCOMPANION"
                              selected={this.state.addAccompanion}
                              onPress={() => this.setState({
                                addAccompanion: !this.state.addAccompanion,
                                addFood: false,
                                addPartner: false
                              })}
                            />
                            <MAddExtras
                              icoName="icoBuilding"
                              label="OUR PARTNER"
                              selected={this.state.addPartner}
                              onPress={() => this.setState({
                                addPartner: !this.state.addPartner,
                                addFood: false,
                                addAccompanion: false
                              })}
                            />
                          </View>
                        </View>

                        <View style={styles.extraItemsContainer}>
                          <Tabs renderTabBar={()=> <ScrollableTab /> }>
                            <Tab heading="Mc Donald's" tabStyle={ styles.tabStyle } activeTabStyle={ styles.tabStyle }>
                              <ExtraPage />
                            </Tab>
                            <Tab heading="Dapur Solo" tabStyle={ styles.tabStyle } activeTabStyle={ styles.tabStyle }>
                              <ExtraPage />
                            </Tab>
                            <Tab heading="KFC" tabStyle={ styles.tabStyle } activeTabStyle={ styles.tabStyle }>
                              <ExtraPage />
                            </Tab>
                            <Tab heading="Burger King" tabStyle={ styles.tabStyle } activeTabStyle={ styles.tabStyle }>
                              <ExtraPage />
                            </Tab>
                            <Tab heading="Padang Merdeka" tabStyle={ styles.tabStyle } activeTabStyle={ styles.tabStyle }>
                              <ExtraPage />
                            </Tab>
                          </Tabs>
                        </View>

                    </View>
                </Content>
                <Modal animationType="slide" transparent={false} visible={this.state.modalReview} onRequestClose={() => { }} >
                    <ModalOrderReview 
                      onPress={() => this.setState({ modalReview: false })} 
                      confirmAction={() => this.setState({modalReview: false}, ()=> this.props.navigation.navigate('PickupDetail'))}
                      tncButtonClicked={() => this.setState({modalTerms: true})}
                    />
                </Modal>
                <Modal animationType="slide" transparent={false} visible={this.state.modalTerms} onRequestClose={() => { }} >
                    <ModalTerms onPress={() => this.setState({ modalTerms: false })} confirmAction={() => this.setState({modalTerms: false})}/>
                </Modal>
                <Footer style={footerWrapper}>
                    <View style={{ flex: 1 }}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                            <OCarShowDetail listCar={this.state.cars} onChange={(value) => this.handleCarSelect(value)} />
                        </ScrollView>
                        <MFooterSummary bookingType="bus" totalCars={3} totalPrice={this.state.totalPrice} totalDays={3} selectAction={() => this.setState({ modalReview: true })} />
                    </View>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    lineStyle: { width: 0.4, backgroundColor: 'white', marginHorizontal: 39 },
    carNameStyle: { fontSize: 18, fontFamily: appVariables.museo900, color: 'white', textAlign: 'center', marginBottom: 6 },
    providerStyle: { fontSize: 14, fontFamily: appVariables.museo500, color: 'white', textAlign: 'center' },
    mainFiturWrapper: { marginTop: 26, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '70%', alignSelf: 'center'},
    mainDescWrapper: { backgroundColor: appVariables.colorBlueHeader, paddingBottom: 111 },
    sectionCarWrapper: { position: 'relative', backgroundColor: 'white', height: 76 },
    carImgWrapper: { width: 285, height: 130, position: 'absolute', bottom: 25, width: '100%' },
    carImgStyle: { flex: 1, width: undefined, height: undefined, resizeMode: 'contain' },
    footerWrapper: { backgroundColor: 'white', height: 'auto', shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 10, height: 'auto' },
    extraItemsWrapper: { backgroundColor: 'white', padding: 20 },
    extraItemsTitleStyle: { fontSize: 12, textAlign: 'center', flex: 1, fontFamily: appVariables.museo700, color: '#2A2E36' },
    extraItemContent: { marginTop: 20, flexDirection: 'row', flex: 1, justifyContent: 'space-between' },
    priceContainer: { justifyContent: 'center', alignItems: 'center', borderTopColor: appVariables.colorBorderColor, borderTopWidth: 1, borderBottomColor: appVariables.colorBorderColor, borderBottomWidth: 1, backgroundColor: 'white', paddingVertical: 25 },
    priceDetails: { marginHorizontal: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    promoPriceContainer: {alignItems: 'flex-end', marginRight: 10, color: appVariables.colorDark},
    promoPrice: {textDecorationLine: 'line-through', fontSize: 14,},
    activePrice: { alignItems: 'flex-start' },
    extraItemsContainer: {marginTop: 10},
    tabStyle: { flex: -1, width: 180 },
    seatContainer: {
      flex: 1,
      flexDirection: 'row',
      padding: 20,
      alignItems: 'center',
      width: '70%',
      alignSelf: 'center'
    },
    seats: {
      flex: 1,
      flexDirection: 'row',
      marginLeft: 10,
      alignItems: 'center',
    },
    seatSeparator: {
      marginHorizontal: 5
    },
    k: {
      marginHorizontal: 1
    },
    activeTextStyle: {
      fontFamily: appVariables.museo500,
    }
})

export default OrderDetailBusRental;