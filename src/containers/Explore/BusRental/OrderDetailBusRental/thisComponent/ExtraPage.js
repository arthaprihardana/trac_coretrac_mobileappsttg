import React from 'react';
import { StyleSheet, Image, View, Text, TextInput } from 'react-native';
import appVariables from '../../../../../assets/styles/appVariables';

const imgBurger = require('../../../../../assets/images/png/extras/extraBurger.png');
const imgCoke = require('../../../../../assets/images/png/extras/extraCoke.png');

const ExtraPage = () => {
  return (
    <View style={styles.pageContainer}>

      <View style={styles.cardContainer}>
        <View style={styles.topContainer}>
          <View style={styles.topImage}><Image source={imgBurger}/></View>
          <View style={styles.topDescription}>
            <Text style={styles.extraTitle}>Cheese Burger Alacarte</Text>
            <Text style={styles.extraDesc}>Lorem ipsum dolor sit amet simply lorem dummy text ipsum.</Text>
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.bitem}><Text style={styles.textPrice}>Rp. 60.000</Text><Text style={styles.textPcs}> /pcs</Text></View>
          <View style={{flex: 1, flexDirection: 'column',}}>
            <View style={[styles.bitem, { alignSelf: 'flex-end',}]}>
              <Text>Total</Text>
              <TextInput style={styles.input} editable={true} maxLength={2} placeholder="0" />
            </View>
          </View>
        </View>
      </View>
      
      <View style={styles.cardContainer}>
        <View style={styles.topContainer}>
          <View style={styles.topImage}><Image source={imgCoke}/></View>
          <View style={styles.topDescription}>
            <Text style={styles.extraTitle}>Coca Cola</Text>
            <Text style={styles.extraDesc}>Lorem ipsum dolor sit amet simply lorem dummy text ipsum.</Text>
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.bitem}><Text style={styles.textPrice}>Rp. 16.000</Text><Text style={styles.textPcs}> /pcs</Text></View>
          <View style={{flex: 1, flexDirection: 'column',}}>
            <View style={[styles.bitem, { alignSelf: 'flex-end',}]}>
              <Text>Total</Text>
              <TextInput style={styles.input} editable={true} maxLength={2} placeholder="0" />
            </View>
          </View>
        </View>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  cardContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    padding: 20,
    marginBottom: 10,
    marginTop: 10,
  },
  input: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: appVariables.colorBorderColor,
    marginLeft: 10,
    paddingHorizontal: 20,
    textAlign: 'center'
  },
  topContainer: {
    flex: 1.5,
    flexDirection: 'row'
  },
  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  topImage: {
    flex: 1.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  topDescription: {
    flex: 3,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  extraTitle: {
    color: '#2A2E36',
    fontSize: 16,
    fontFamily: appVariables.museo700,
    marginBottom: 5
  },
  extraDesc: {
    color: '#8A959E',
    fontSize: 12,
    lineHeight: 16,
    fontFamily: appVariables.museo500
  },
  textPrice: {
    color: '#F27C21',
    fontSize: 18,
    fontFamily: appVariables.museo700,
  },
  textPcs: {
    color: '#8A959E',
    fontSize: 14,
    fontFamily: appVariables.museo500,
  },
  bitem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 20
  }
})

export default ExtraPage;