import { Container, Content, Footer, Textarea } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AButton, AList } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';


class ModalOrderReview extends Component {
    state = {
      modalTerms: false,
    }

    render() {
        let { onPress, confirmAction } = this.props;
        return (
            <Container>
                <View style={{ width: '100%', justifyContent: 'flex-end', flexDirection: 'row', padding: 20 }}>
                    <TouchableOpacity onPress={onPress}>
                        <SvgIcon name="icoCloseDark" />
                    </TouchableOpacity>
                </View>
                <Content>
                    <AList>
                        <Text style={styles.mainTitle}>Order Review</Text>
                    </AList>

                    <AList>
                        <Text style={styles.title}>PACKAGES</Text>
                        <Text style={styles.text}>12 Hours / Day</Text>
                    </AList>

                    <AList>
                        <Text style={styles.title}>DATES</Text>
                        <Text style={styles.text}>12 Sept 2018 -> 12 Sept 2018</Text>
                        <Text style={styles.text}>Start 10 AM - End 10 PM</Text>
                    </AList>

                    <AList>
                        <Text style={styles.title}>PICK UP LOCATION</Text>
                        <Text style={styles.text}>Bali Jl. Pulau Alor No. 17, Dauh Puri, Denpasar, Bali 12345</Text>
                    </AList>

                    <AList>
                        <Text style={styles.title}>NOTES/REQUEST</Text>
                        <View style={styles.notesContainer}>
                            <View style={styles.notes}>
                                <Textarea rowSpan={2} placeholder="" style={styles.textarea} />
                            </View>
                            <View style={styles.notesIcon}>
                                <SvgIcon name="icoEdit" />
                            </View>
                        </View>
                        <View>
                            <Text style={styles.catatan}>*Lorem ipsum dolor sit amet sid ut parnum sadum par et um</Text>
                        </View>
                    </AList>

                    <AList>
                        <Text style={styles.title}>PAYMENT DETAILS</Text>

                        <View style={styles.listCar}>
                            <View><Text style={styles.text}>TOYOTA AVANZA</Text></View>
                            <View style={styles.priceList}>
                                <View style={styles.priceContainer}>
                                    <View style={styles.priceDesc}><Text style={styles.text}>Rp 325,000 x 3 Day(s)</Text></View>
                                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 975,000</Text></View>
                                </View>
                                <View style={styles.priceContainer}>
                                    <View style={styles.priceDesc}><Text style={styles.text}>Baby Car Seat: Rp 50,000 x 3 Day(s)</Text></View>
                                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 150,000</Text></View>
                                </View>
                            </View>
                        </View>

                        <View style={styles.listCar}>
                            <View><Text style={styles.text}>TOYOTA INNOVA</Text></View>
                            <View style={styles.priceList}>
                                <View style={styles.priceContainer}>
                                    <View style={styles.priceDesc}><Text style={styles.text}>Rp 325,000 x 3 Day(s)</Text></View>
                                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 975,000</Text></View>
                                </View>
                                <View style={styles.priceContainer}>
                                    <View style={styles.priceDesc}><Text style={styles.text}>Baby Car Seat: Rp 50,000 x 3 Day(s)</Text></View>
                                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 150,000</Text></View>
                                </View>
                            </View>
                        </View>

                        <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 22 }}>
                            <Text style={{ fontSize: 14, fontFamily: appVariables.museo700, color: '#2A2E36' }}>Total Payment</Text>
                            <Text style={{ fontSize: 18, fontFamily: appVariables.museo700, color: '#2A2E36' }}>Rp 1,025,000</Text>
                        </View>

                        <TouchableOpacity onPress={this.props.tncButtonClicked} style={styles.tnc}>
                          <Text>TERMS AND CONDITIONS</Text>
                          <SvgIcon style={{position: 'absolute', right: 0}} name="icoRightBlue" />
                        </TouchableOpacity>
                    </AList>
                </Content>
                <Footer style={{ height: 'auto' }}>
                    <AButton primary full onPress={confirmAction}>CONFIRM</AButton>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        margin: 0,
        padding: 0,
        width: appMetrics.windowWidth,
        height: appMetrics.windowHeight,
        backgroundColor: appVariables.colorWhite,
    },
    tnc: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      borderTopWidth: 1,
      borderTopColor: appVariables.colorBorderColor,
      paddingVertical: 20,
    },
    content: {
        padding: appVariables.padding,
    },
    closeButton: {
        top: 0
    },
    mainTitle: {
        color: appVariables.colorDark,
        fontSize: 24,
        fontFamily: appVariables.museo700,
    },
    text: {
        fontSize: 12,
        color: appVariables.colorDark,
        fontFamily: appVariables.museo500,
        flex: 1,
        lineHeight: 20,
    },
    title: {
        fontSize: 12,
        color: appVariables.colorDark,
        fontFamily: appVariables.museo500,
        flex: 1,
        lineHeight: 20,
        marginBottom: 10,
    },
    notesContainer: {
        flexDirection: 'row',
    },
    notes: {
        flex: 9,
        borderBottomWidth: .4,
        borderBottomColor: appVariables.colorGray,
        marginBottom: 10,
    },
    notesIcon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textarea: {
        paddingLeft: 0, left: 0, marginLeft: 0,
        fontSize: 12,
        color: appVariables.colorDark,
        fontFamily: appVariables.museo500,
        lineHeight: 20,
    },
    catatan: {
        fontSize: 10,
    },
    listCar: {
        flexDirection: 'column',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: .4,
        borderBottomColor: appVariables.colorGray,
    },
    priceList: {
        flexDirection: 'column',
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    priceDesc: {
        flex: 8,
    },
    price: {
        flex: 2,
        alignSelf: 'flex-end'
    },
    totalPayment: {
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
    },
    tpDesc: {
        flex: 6,
    },
    tpNum: {
        flex: 4,
    },
    totalPaymentNumber: {
        fontSize: 18,
        fontFamily: appVariables.museo700,
        alignSelf: 'flex-end',
        color: appVariables.colorDark
    },
    carList: {
        flexDirection: 'row',
    },
    image: {
        width: 120,
        resizeMode: 'contain'
    },
    carImageContainer: {
        flex: 4,
        backgroundColor: '#F0F0F3',
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    carDescriptions: {
        flex: 6,
        paddingLeft: 15,
        paddingTop: 5,
        paddingBottom: 0,
        flexDirection: 'column',
    },
    carTitle: {
        fontSize: 18,
        fontFamily: appVariables.museo700,
        marginBottom: 5,
        color: appVariables.colorDark,
        flex: 1,
    },
    serviceType: {
        marginBottom: 10,
        flex: 1,
    },
    carDetails: {
        flexDirection: 'row',
        flex: 8
    },
    carDetailList: {
        flex: 1,
        flexDirection: 'column',
    },
    carDetailText: {
        flexDirection: 'row',
    },
    bold: {
        fontFamily: appVariables.museo700,
        color: appVariables.colorDark
    },
    first: {
        flex: 1,
        marginRight: 4
    },
    last: {
        flex: 9
    }
})

ModalOrderReview.propTypes = {
    onPress: PropTypes.func
}

export default ModalOrderReview;
