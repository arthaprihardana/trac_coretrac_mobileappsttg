import { Container, Content, Footer, Textarea } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AButton, AList } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';

class ModalTerms extends Component {
  render() {
    return (
      <Container>
        <View style={{ width: '100%', justifyContent: 'flex-end', flexDirection: 'row', padding: 20 }}>
          <TouchableOpacity onPress={this.props.onPress}>
            <SvgIcon name="icoCloseDark" />
          </TouchableOpacity>
        </View>
        <Content padder >
          <Text style={styles.text}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</Text>
          <Text style={styles.text}>labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</Text>
          <Text style={styles.text}>ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</Text>
          <Text style={styles.text}>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui</Text>
          <Text style={styles.text}>officia deserunt mollit anim id est laborum</Text>
          <Text style={styles.text}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</Text>
          <Text style={styles.text}>dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</Text>
          <Text style={styles.text}>ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu</Text>
          <Text style={styles.text}>fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</Text>
          <Text style={styles.text}>mollit anim id est laborum</Text>
        </Content>
        <Footer style={{ height: 'auto' }}>
          <AButton primary full onPress={this.props.confirmAction}>I AGREE</AButton>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        margin: 0,
        padding: 0,
        width: appMetrics.windowWidth,
        height: appMetrics.windowHeight,
        backgroundColor: appVariables.colorWhite,
    },
    content: {
      padding: appVariables.padding,
    },
    closeButton: {
      top: 0
    },
    mainTitle: {
      color: appVariables.colorDark,
      fontSize: 24,
      fontFamily: appVariables.museo700,
    },
    title: {
      fontSize: 12,
      color: appVariables.colorDark,
      fontFamily: appVariables.museo500,
      flex: 1,
      lineHeight: 20,
      marginBottom: 10,
    },
    text: {
      fontSize: 16,
      color: '#8A959E',
      lineHeight: 20,
      fontFamily: appVariables.museo500,
      marginBottom: 20
    }
})

export default ModalTerms;
