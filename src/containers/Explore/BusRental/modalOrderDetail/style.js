import { StyleSheet } from 'react-native';
import { appMetrics, appVariables } from '../../../../assets/styles';

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    margin: 0,
    padding: 0,
    width: appMetrics.windowWidth,
    height: appMetrics.windowHeight,
    backgroundColor: appVariables.colorWhite,
  },
  content: {
    padding: appVariables.padding,
  },
  closeButton: {
    top: 0
  },
  mainTitle: {
    color: appVariables.colorDark,
    fontSize: 24,
    fontFamily: appVariables.museo700,
  },
  text: {
    fontSize: 12,
    color: appVariables.colorDark,
    fontFamily: appVariables.museo500,
    flex: 1,
    lineHeight: 20,
  },
  carNumber: {
    fontSize: 12,
    color: '#8A959E',
    fontFamily: appVariables.museo500,
  },
  title: {
    fontSize: 12,
    color: appVariables.colorDark,
    fontFamily: appVariables.museo500,
    flex: 1,
    lineHeight: 20,
    marginBottom: 10,
  },
  notesContainer: {
    flexDirection: 'row',
  },
  notes: {
    flex: 9,
    borderBottomWidth: .4,
    borderBottomColor: appVariables.colorGray,
    marginBottom: 10,
  },
  notesIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textarea: { 
    paddingLeft: 0, left: 0, marginLeft: 0,
    fontSize: 12,
    color: appVariables.colorDark,
    fontFamily: appVariables.museo500,
    lineHeight: 20,
  },
  catatan: {
    fontSize: 10, 
  },
  listCar: {
    flexDirection: 'column',
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: .4,
    borderBottomColor: appVariables.colorGray,
  },
  priceList: {
    flexDirection: 'column',
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  priceDesc: {
    flex: 8,
  },
  price: {
    flex: 2,
    alignSelf: 'flex-end'
  },
  totalPayment: {
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 15,
  },
  tpDesc: {
    flex: 6,
  },
  tpNum: {
    flex: 4,
  },
  totalPaymentNumber: {
    fontSize: 18,
    fontFamily: appVariables.museo700,
    alignSelf: 'flex-end',
    color: appVariables.colorDark
  },
  carList: {
    flexDirection: 'row',
  },
  image: {
    width: 120,
    resizeMode: 'contain'
  },
  carImageContainer: {
    flex: 4,
    backgroundColor: '#F0F0F3',
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  carDescriptions: {
    flex: 6,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 0,
    flexDirection: 'column',
  },
  carTitle: {
    fontSize: 18,
    fontFamily: appVariables.museo700,
    marginBottom: 5,
    color: appVariables.colorDark,
  },
  serviceType: {
    marginBottom: 10,
    flex: 1,
  },
  carDetails: {
    flexDirection: 'row',
    flex: 8
  },
  carDetailList: {
    flex: 1,
    flexDirection: 'column',
  },
  carDetailText: {
    flexDirection: 'row',
  },
  bold: {
    fontFamily: appVariables.museo700,
    color: appVariables.colorDark
  },
  first: {
    flex: 1,
    marginRight: 4
  },
  last: {
    flex: 9
  }
})

export default styles;