import { Body, Button, Container, Content, Form, Header, Input, Item, Left, Text, Title } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import { appVariables } from '../../../../assets/styles';
import { AFormTitle, ASeparator } from '../../../../components/atoms';
import { MBlueRadio, MFloatingInput, MFooterTotalPayment, MSortDropdown } from '../../../../components/molecules';
import SvgIcon from '../../../../utils/SvgIcon';
import styles from './style';
import MStepper from '../../../../components/molecules/MStepper';

class PickupDetail extends Component {
  state = {
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <Header>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body style={{flex: 2}}>
            <Title>Bus Rental</Title>
          </Body>
        </Header>

        <MStepper step={2} labels={['Bus Order', 'Personal Data', 'Payment']} />

        <Content padder>
          <Form>
            <AFormTitle title={'Pickup Details'} notes={'Please enter the pickup detail information for a more approximate pickup'} />

            <MFloatingInput 
              placeholder={'eg. Lobby utama Hotel Kempinski'}
            />

            <ASeparator />

            <Button primary block onPress={() => this.props.navigation.navigate('OrderLoginBusRental')}><Text>CONTINUE</Text></Button>

            <ASeparator />
          </Form>
        </Content>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

      </Container>
    )
  }
}

export default PickupDetail;