import { Body, Button, Card, Form, Text, H1, Accordion } from 'native-base';
import React, { Component } from 'react';
import { Image, TouchableOpacity, View, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../../assets/styles';
import { ASeparator } from '../../../../../components/atoms';
import { MApplyPromo, MBlueRadio } from '../../../../../components/molecules';

const dataVirtualPayment = [
  {
    title: 'Payment via ATM',
    content: 
`1. Choose Other Transaction
2. Choose Transfer
3. Choose Permata Virtual Account
4. Enter Virtual Account Number 5603 3028 6739 2820
5. Choose Correct and Press Yes
6. Keep Payment Proof
7. Payment Done
`
  },
  {
    title: 'Payment via Internet Banking',
    content: 
`1. Choose Other Transaction
2. Choose Transfer
3. Choose Permata Virtual Account
4. Enter Virtual Account Number 5603 3028 6739 2820
5. Choose Correct and Press Yes
6. Keep Payment Proof
7. Payment Done
`
  },
  {
    title: 'Payment via ATM',
    content: 
`1. Choose Other Transaction
2. Choose Transfer
3. Choose Permata Virtual Account
4. Enter Virtual Account Number 5603 3028 6739 2820
5. Choose Correct and Press Yes
6. Keep Payment Proof
7. Payment Done
`
  }
];

class VirtualAccount extends Component {
  state = {
    saveCardInfo: false,
    agree: false,
    selectedBank: null,
    paymentConfirmation: false
  }

  _renderVirtualPayment = () => {
    if (!this.state.paymentConfirmation) {
      return (
        <Form>
          <View style={{ padding: appVariables.padding }}>
            <View style={{ flexDirection: 'column', }}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => this.setState({ selectedBank: 'bca' })}>
                  <Card style={[styles.bankCard, { marginRight: 7, }]}>
                    <View>
                      <MBlueRadio 
                        active={this.state.selectedBank == 'bca'}
                      />
                    </View>
                    <View style={{ width: 110, justifyContent:'center', marginTop: 3, marginLeft: 5 }}>
                      <Image source={require('../../../../../assets/images/png/bank/bca.png')} />
                    </View>
                  </Card>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({ selectedBank: 'permata' })}>
                  <Card style={[styles.bankCard, { marginLeft: 7, }]}>
                    <View>
                      <MBlueRadio 
                        active={this.state.selectedBank == 'permata'}
                      />
                    </View>
                    <View style={{ width: 110, justifyContent:'center', marginLeft: 7 }}>
                      <Image source={require('../../../../../assets/images/png/bank/permata.png')} />
                    </View>
                  </Card>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1, flexDirection: 'row',}}>
                <TouchableOpacity onPress={() => this.setState({ selectedBank: 'cimb' })}>
                  <Card style={[styles.bankCard, { marginRight: 7, }]}>
                    <View>
                      <MBlueRadio 
                        active={this.state.selectedBank == 'cimb'}
                      />
                    </View>
                    <View style={{ width: 110, justifyContent:'center', marginTop: 5, marginLeft: 5 }}>
                      <Image source={require('../../../../../assets/images/png/bank/cimb.png')} />
                    </View>
                  </Card>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({ selectedBank: 'mandiri' })}>
                  <Card style={[styles.bankCard, { marginLeft: 7, }]}>
                    <View>
                      <MBlueRadio 
                        active={this.state.selectedBank == 'mandiri'}
                      />
                    </View>
                    <View style={{ width: 110, justifyContent:'center', marginLeft: 5 }}>
                      <Image source={require('../../../../../assets/images/png/bank/mandiri.png')} />
                    </View>
                  </Card>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={{ padding: appVariables.padding, }}>
            <MApplyPromo />

            <ASeparator />
            <MBlueRadio
              active={this.state.agree}
              onPress={() => this.setState({agree: !this.state.agree})}
              label={<Text style={styles.label} onPress={() => this.setState({agree: !this.state.agree})}>I understand the Terms and Conditions</Text>}
            />

            <ASeparator />
            <ASeparator />

            <Button primary block onPress={() => this.setState({paymentConfirmation: true})}><Text>MAKE PAYMENT</Text></Button>
          </View>
        </Form>
      )
    } else {
      return (
        <View style={{ padding: 20 }}>
          <H1 style={{fontSize: 24, marginBottom: 20, fontFamily: appVariables.museo700, color: appVariables.colorDark}}>Permata Bank Virtual Account</H1>
          <View style={{marginBottom: 20}}>
            <Text style={{fontSize: 14, color: appVariables.colorGray, marginBottom: 5}}>Please complete your payment in</Text>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{color: appVariables.colorDark, fontFamily: appVariables.museo700, fontSize: 14 }}>58 minutes 25 seconds</Text>
              <TouchableOpacity><Text style={{ color: '#2246A8', fontSize: 14, marginLeft: 10 }}>Copy</Text></TouchableOpacity>
            </View>
          </View>
          <View style={{ marginBottom: 20 }}>
            <Text style={{fontSize: 14, color: appVariables.colorGray, marginBottom: 5}}>Virtual Account Number</Text>
            <Text style={{color: appVariables.colorDark, fontFamily: appVariables.museo700, fontSize: 14 }}>5603 3028 6739 2820</Text>
          </View>
          <View>
            <Accordion 
              dataArray={dataVirtualPayment} 
              expanded={0}
              contentStyle={{paddingHorizontal: 20, lineHeight: 24, marginHorizontal: 0, color: '#8A959E'}}
              headerStyle={{backgroundColor: '#FFFFFF', fontSize: 16, borderTopWidth: 0, borderBottomWidth: 1, borderBottomColor: appVariables.colorBorder, fontFamily: appVariables.museo500, color: appVariables.colorDark }}
            />
           
          {/* this.props.navigation.navigate('BusRentalPaymentFinish') */}
          </View>
        </View>
      )
    }
  }

  render = () => {
    return (
      <View>
        { this._renderVirtualPayment() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  label: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
    flex: 1,
    lineHeight: 24,
  },
  headerTitle: {
    color: appVariables.colorWhite,
    fontFamily: appVariables.museo500,
    marginTop: 0,
    fontSize: 12
  },
  bankCard: { 
    flexDirection: 'row', 
    flex: 1, 
    padding: 14,
    justifyContent: 'center', 
    alignItems: 'center', 
    marginBottom: 10
  },
})

export default withNavigation(VirtualAccount);