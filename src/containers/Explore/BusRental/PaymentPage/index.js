/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 13:59:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-24 15:39:56
 */
import { Body, Button, Container, Content, Form, Header, Left, Text, Title, Tab, Tabs } from 'native-base';
import React, { Component } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ASeparator, AToggle } from '../../../../components/atoms';
import { MApplyPromo, MBlueRadio, MFloatingInput, MFooterTotalPayment, MPaymentHeader } from '../../../../components/molecules';
import SvgIcon from '../../../../utils/SvgIcon';
import ModalOrderDetail from '../modalOrderDetail';
import styles from './style';
import MStepper from '../../../../components/molecules/MStepper';
import CreditCard from './CreditCard';
import VirtualAccount from './VirtualAccount';

class PaymentPage extends Component {
  state = {
    saveCardInfo: false,
    agree: false,
    showModalOrderDetail: false,
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <Header>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body style={{flex: 3}}>
            <Title>Payment</Title>
          </Body>
        </Header>

        <MStepper step={3} labels={['Bus Order', 'Personal Data', 'Payment']} />

        <Content>
          <Tabs>
            <Tab heading="Credit/Debit Card" tabStyle={styles.tabStyle} activeTabStyle={styles.tabActiveStyle} textStyle={styles.tabText} activeTextStyle={styles.tabTextActive}>
              <CreditCard />
            </Tab>
            <Tab heading="Virtual Account" tabStyle={styles.tabStyle} activeTabStyle={styles.tabActiveStyle} textStyle={styles.tabText} activeTextStyle={styles.tabTextActive}>
              <VirtualAccount />
            </Tab>
          </Tabs>
        </Content>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

        <ModalOrderDetail 
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({showModalOrderDetail: false})}
        />

      </Container>
    )
  }
}

export default withNavigation(PaymentPage);