import { StyleSheet } from 'react-native';
import {appVariables} from '../../../../assets/styles';

const styles = StyleSheet.create({
  label: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
    flex: 1,
    lineHeight: 24,
  },
  tabStyle: {
    backgroundColor: '#12265B',
  },
  tabActiveStyle: {
    backgroundColor: '#0B1C4A',
  },
  tabText: {
    color: '#FFFFFF',
  },
  tabTextActive: {
    color: '#FFFFFF'
  }
})

export default styles;