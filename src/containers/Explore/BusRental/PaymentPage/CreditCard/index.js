import { Body, Button, Container, Content, Form, Text } from 'native-base';
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { appVariables } from '../../../../../assets/styles';
import { MBlueRadio, MFloatingInput } from '../../../../../components/molecules';
import ASeparator from '../../../../../components/atoms/ASeparator';
import AToggle from '../../../../../components/atoms/AToggle';
import MApplyPromo from '../../../../../components/molecules/MApplyPromo';
import { withNavigation } from 'react-navigation';

class CreditCard extends Component {
  state = {
    saveCardInfo: false,
    agree: false,
    showModalOrderDetail: false,
  }

  render = () => {
    return (
      <View>
        <Form>
          <View style={{padding: 20}}>
            <MFloatingInput 
              label={'Credit/Debit Card Number'}
              secure={true}
            />

            <MFloatingInput 
              label={'Name on Card'}
            />

            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{flex: 1, marginRight: 5}}>
                <MFloatingInput 
                  label={'Exp Date (MM/YY)'}
                />
              </View>

              <View style={{flex: 1, marginLeft: 5}}>
                <MFloatingInput 
                  label={'CVV'}
                  secure={true}
                />
                <Text style={{color: '#00C0F2', fontSize: 12, fontFamily: appVariables.museo500, marginTop: -15, paddingTop: 0}}>What is CVV?</Text>
              </View>
            </View>

            <ASeparator />
            <ASeparator />
            <View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{marginTop: 5, fontSize: 16}}>Save this card information</Text>
                <AToggle isOn={this.state.saveCardInfo} style={{alignSelf: 'flex-end'}} onPress={() => this.setState({saveCardInfo: !this.state.saveCardInfo})} />
              </View>
            </View>
            <ASeparator />
            <ASeparator />

            <MBlueRadio
              active={this.state.agree}
              onPress={() => this.setState({agree: !this.state.agree})}
              label={<Text style={styles.label} onPress={() => this.setState({agree: !this.state.agree})}>I have read and understood the Astra Trac Privacy Policy and Term Condition</Text>}
            />
            <ASeparator />
          </View>

          <View style={styles.promoContainer}>
            <View style={{padding: 20}}>
              <MApplyPromo />

              <ASeparator />

              <Button primary block onPress={() => this.props.navigation.navigate('BusRentalPaymentFinish')}><Text>MAKE PAYMENT</Text></Button>
            </View>
          </View>

        </Form>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  label: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
    flex: 1,
    lineHeight: 24,
  },
  promoContainer: {
    borderTopWidth: 1,
    borderTopColor: appVariables.colorBorderColor,
    paddingTop: 10,
    flex: 1,
  }
})

export default withNavigation(CreditCard);