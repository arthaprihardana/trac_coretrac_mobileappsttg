import { Container, Content, Footer, Left, Button, Text } from 'native-base';
import React, { Component } from 'react';
import { Modal, StatusBar, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ABackButtonCircle, ABookButton, AHeader, ARentTitle } from '../../../../components/atoms';
import { GetShortMonth } from '../../../../utils/Helpers';
import { ModalAddress, ModalAddressDetail, ModalDateTime, ModalPassenger, ModalDestination, ModalDestinationDetail } from './modals';
import { connect } from 'react-redux';
import { resetBusRentalData } from '../../../../redux/ducks/BusRental';
import styles from './style';
import SvgIcon from '../../../../utils/SvgIcon';

class BusRentalSelectOptions extends Component {
  state = {
    modalAddress: false,
    modalAddressDetail: false,
    modalDestination: false,
    modalDestinationDetail: false,
    modalPassenger: false,
    modalDateTime: false,

    selectedAddressError: false,
    selectedDestinationError: false,
    selectedPassengerError: false,
    selectedDateError: false,

    selectedCity: null,
    selectedAddress: null,
    selectedDestination: null,
    selectedDestinationDetail: null,
    selectedPassenger: null,
    startDate: null,
    endDate: null,
    pickupTime: null
  };

  componentDidMount = () => {
    this.props.resetBusRentalData();
  }

  getMonth = (month) => {
    return GetShortMonth(month);
  }

  formatDateOrder = () => {
    const sDate = this.state.startDate;
    const eDate = this.state.endDate;

    if (!sDate || !eDate) {
      return null;
    }

    const sSDay = sDate.getDate();
    const mSDay = sDate.getMonth() + 1;
    const sEDay = eDate.getDate();
    const mEDay = eDate.getMonth() + 1;
    const sSYear = sDate.getFullYear();

    let sCombine = '';

    // if start date === end date, it means the user only order for 1 day
    if ((sDate.getDate() + sDate.getMonth() + sDate.getFullYear()) == (eDate.getDate() + eDate.getMonth() + eDate.getFullYear())) {
      // only a day
      sCombine = `${sSDay} ${this.getMonth(mSDay)}, ${sSYear} at ${this.state.pickupTime}`;
    } else {
      // multipe days
      sCombine = `${sSDay} ${this.getMonth(mSDay)} - ${sEDay} ${this.getMonth(mEDay)}, ${sSYear} at ${this.state.pickupTime}`;
    }

    return sCombine;
  }

  changePassenger = (value) => {
    this.setState({
      selectedPassenger: value
    })

    this.setState({selectedPassengerError: false});
  }

  changeCity = (value) => {
    this.setState({
      selectedCity: value
    })

    this.setState({selectedCityError: false});
  }

  changeAddress = (value) => {
    this.setState({
      selectedAddress: value
    })

    this.setState({selectedAddressError: false});
  }

  changeDestination = (value) => {
    this.setState({
      selectedDestination: value
    })

    this.setState({selectedDestinationError: false});
  }

  changeDestinationDetail = (value) => {
    this.setState({
      selectedDestinationDetail: value
    })

    this.setState({selectedDestinationDetailError: false});
  }

  changeDate = (startDate, endDate, pickupTime) => {
    this.setState({
      startDate,
      endDate,
      pickupTime
    });

    this.setState({selectedDateError: false});
  }

  findCarClicked = () => {
    let error = 0;
    alert(JSON.stringify(this.props.selectOptions));

    if (!this.state.selectedAddress) { this.setState({selectedAddressError: true}); error = 1; }
    if (!this.state.selectedDestination) { this.setState({selectedDestinationError: true}); error = 1; }
    if (!this.state.startDate) { this.setState({selectedDateError: true}); error = 1; }
    if (!this.state.selectedPassenger) { this.setState({selectedPassengerError: true}); error = 1; }

    if (error == 0) {
      this.props.navigation.navigate('FindBus');
    }
  }

  render() {
    const nav = this.props.navigation;

    return (
      <Container style={{backgroundColor: '#12265B'}}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={appVariables.colorBlueHeader}
        />

        <AHeader bgColor={appVariables.colorBlueHeader}>
          <Left>
            <ABackButtonCircle transparent onPress={() => nav.navigate('ExploreLanding')} />
          </Left>
        </AHeader>

        <Content>
          <ARentTitle title="Bus Rental" bgColor={appVariables.colorBlueHeader} />
          <View style={styles.ButtonWrapper}>
            <ABookButton label="Pickup City and Address" searchIcon={true} textValue={this.props.selectOptions.selectedAddress ? String(this.props.selectOptions.selectedAddress) : 'Select Your City'} bgColor="#2246A8" onPress={() => this.setState({modalAddress: true})} isError={this.state.selectedAddressError} />
            <ABookButton label="Destination (Airport, Area, City)" searchIcon={true} textValue={this.props.selectOptions.selectedDestinationDetail ? String(this.props.selectOptions.selectedDestinationDetail) : 'Select Area'} bgColor="#1F419B" onPress={() => this.setState({modalDestination: true})} isError={this.state.selectedDestinationError} />
            <ABookButton label="Date and Time" textValue={this.formatDateOrder() ? this.formatDateOrder() : 'Select Date & Time'} bgColor="#1A3681" onPress={() => this.setState({modalDateTime: true})} isError={this.state.selectedDateError} />
            <ABookButton label="Pessengers (optional)" textValue={this.props.selectOptions.passengerNumber ? String(this.props.selectOptions.passengerNumber) : 'Input Amount'} bgColor="#12265B" onPress={() => this.setState({modalPassenger: true})} isError={this.state.selectedPassengerError} />
          </View>
        </Content>

        <Footer style={{backgroundColor: '#12265B'}}>
          <Button iconLeft full primary style={styles.bottomButton} onPress={() => this.findCarClicked()}>
            <SvgIcon name='icoSearch' />
            <Text>SEARCH</Text>
          </Button>
        </Footer>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalAddress}
          onRequestClose={() => {}}
        >
          <ModalAddress onClose={() => this.setState({ modalAddress: false, modalAddressDetail: true })} onSelectOptions={this.changeCity} bgColor="#2246A8" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalAddressDetail}
          onRequestClose={() => {}}
        >
          <ModalAddressDetail onClose={() => this.setState({ modalAddressDetail: false })} onSelectOptions={this.changeAddress} bgColor="#2246A8" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalDestination}
          onRequestClose={() => {}}
        >
          <ModalDestination onClose={() => this.setState({ modalDestination: false, modalDestinationDetail: true })} onSelectOptions={this.changeDestination} bgColor="#1F419B" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalDestinationDetail}
          onRequestClose={() => {}}
        >
          <ModalDestinationDetail onClose={() => this.setState({ modalDestinationDetail: false })} onSelectOptions={this.changeDestinationDetail} bgColor="#1F419B" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalDateTime}
          onRequestClose={() => {}}
        >
          <ModalDateTime onClose={() => this.setState({ modalDateTime: false })} onSelectOptions={this.changeDate} bgColor='#1A3681' />
        </Modal>
        
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalPassenger}
          onRequestClose={() => {}}
        >
          <ModalPassenger onClose={() => this.setState({ modalPassenger: false })} onSelectOptions={this.changePassenger} bgColor="#12265B" />
        </Modal>
      </Container>
    )
  }
};

const mapStateToProps = state => {
  return {
    selectOptions: state.BusRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    resetBusRentalData: () => {
      dispatch(resetBusRentalData());
    }
  }
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(BusRentalSelectOptions));