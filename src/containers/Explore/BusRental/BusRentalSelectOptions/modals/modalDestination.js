import { Content, Right } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AHeader, AInputWithIcon, ASearchList } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';
import { updateSelectedDestination } from '../../../../../redux/ducks/BusRental';
import { connect } from 'react-redux';

class ModalDestination extends Component {
  state = {
    cityData: [
      {city: 'Ambon'},
      {city: 'Balikpapan'},
      {city: 'Banda Aceh'},
      {city: 'Bandar Lampung'},
      {city: 'Bandung'},
      {city: 'Banjar'},
      {city: 'Banjarbaru'},
      {city: 'Banjarmasin'},
      {city: 'Batam'},
      {city: 'Batu'},
      {city: 'Bau-Bau'},
      {city: 'Bekasi'},
      {city: 'Bengkulu'},
      {city: 'Bima'},
      {city: 'Binjai'},
      {city: 'Bitung'},
      {city: 'Blitar'},
      {city: 'Bogor'},
      {city: 'Bontang'},
      {city: 'Bukittinggi'},
      {city: 'Cilegon'},
      {city: 'Cimahi'},
      {city: 'Cirebon'},
      {city: 'Denpasar'},
      {city: 'Depok'},
      {city: 'Dumai'},
      {city: 'Gorontalo'},
      {city: 'Gunungsitoli'},
      {city: 'Jakarta Barat'},
      {city: 'Jakarta Pusat'},
      {city: 'Jakarta Selatan'},
      {city: 'Jakarta Timur'},
      {city: 'Jakarta Utara'},
      {city: 'Jambi'},
      {city: 'Jayapura'},
      {city: 'Kediri'},
      {city: 'Kotamobagu'},
      {city: 'Kupang'},
      {city: 'Langsa'},
      {city: 'Lhokseumawe'},
      {city: 'Lubuklinggau'},
      {city: 'Madiun'},
      {city: 'Magelang'},
      {city: 'Makassar'},
      {city: 'Malang'},
      {city: 'Manado'},
      {city: 'Mataram'},
      {city: 'Medan'},
      {city: 'Metro'},
      {city: 'Mojokerto'},
      {city: 'Padang'},
      {city: 'Padangpanjang'},
      {city: 'Padangsidempuan'},
    ],
    searchData: [],
    cityValue: '',
    notFound: false
  }

  componentWillMount() {
    this.setState({
      searchData: this.state.cityData
    })
  }

  searchCity = (text) => {
    let cityData = this.state.cityData;

    let result = cityData.filter((item) => {
      if ((String(item.city).toLowerCase()).search(text.toLowerCase()) > -1) {
        return item;
      }
    });

    if (result.length <= 0) {
      this.setState({notFound: true});
    } else {
      this.setState({notFound: false});
    }

    this.setState({
      searchData: result
    });
  }

  setCity = (city) => {
    this.props.onClose();
    this.props.onSelectOptions(city);
    this.props.updateSelectedDestination({selectedDestination: city});
    this.setState({
      dataCity: city
    })
  }

  render = () => {
    if (this.state.notFound) {
      content = (
        <View style={styles.notFoundWrapper}>
          <SvgIcon name="icoNotFound" />
          <Text style={styles.notFound}>We're sorry, the area that you search is out of our coverage area</Text>
        </View>
      );
    } else {
      content = (
        <ASearchList 
          data={this.state.searchData}
          type={'big'}
          onPress={this.setCity}
        />
      )
    }

    return (
      <View style={[styles.modalContainer, {backgroundColor: this.props.bgColor}]}>
        <AHeader bgColor={this.props.bgColor}>
          <Right>
            <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
              <SvgIcon name="icoClose" />
            </TouchableOpacity>
          </Right>
        </AHeader>
        <View>
          <View style={styles.content}>
            <AInputWithIcon 
              label="Find Area, Building, Airport, Address"
              iconName="icoSearchBig"
              onChangeText={this.searchCity}
              value={this.state.cityValue}
            />
          </View>
        </View>
        <Content>
          { content }
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1,
    margin: 0,
    padding: 0,
    width: appMetrics.windowWidth,
    height: appMetrics.windowHeight
  },
  listContainer: {
    paddingBottom: 40,
  },
  content: {
    padding: appVariables.padding,
  },
  closeButton: {
    top: 10
  },
  notFoundWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  notFound: {
    color: appVariables.colorWhite,
    fontSize: 14,
    textAlign: 'center',
    width: '60%',
    alignSelf: 'center',
    marginTop: 10,
  }
});

const mapStateToProps = state => {
  return {
    selectOptions: state.CarRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateSelectedDestination: (payload) => {
      dispatch(updateSelectedDestination(payload))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalDestination);