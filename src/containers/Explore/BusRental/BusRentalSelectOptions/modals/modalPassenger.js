import { Content, Radio, Right, Text, Button } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, TextInput } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AHeader, ARentTitle } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';
import { updatePassenger } from '../../../../../redux/ducks/BusRental';
import { connect } from 'react-redux';

class ModalPassenger extends Component {
  state = {
    selectedPackage: '4 Hours / Day',
    passengerNumber: '100'
  }

  toggleSelected = (value) => {
    const _this = this;
    setTimeout(function() {
      _this.props.onClose()
    }, 200);
  }

  doneClicked = () => {
    this.props.onClose();
    this.props.onSelectOptions(this.state.passengerNumber);
    this.props.updatePassenger({passengerNumber: this.state.passengerNumber});
  }

  addPassenger = () => {
    let jml = parseInt(this.state.passengerNumber);
    this.setState({passengerNumber: String(jml + 1)});
  }

  remPassenger = () => {
    let jml = parseInt(this.state.passengerNumber);
    this.setState({passengerNumber: String(jml - 1)});
  }

  render = () => {
    return (
      <View style={[styles.modalContainer, {backgroundColor: this.props.bgColor}]}>
        <AHeader bgColor={this.props.bgColor}>
          <Right>
            <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
              <SvgIcon name="icoClose" />
            </TouchableOpacity>
          </Right>
        </AHeader>
        <Content>
          <View style={styles.content}>
            <View><Text style={styles.label}>Passenger (optional)</Text></View>
            <View style={styles.btWrapper}>
              <TextInput
                style={styles.bigText}
                onChangeText={(text) => this.setState({passengerNumber: text})}
                value={this.state.passengerNumber}
              />
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity style={styles.plusMin} onPress={this.remPassenger}><Text style={styles.plusMinText}>-</Text></TouchableOpacity>
              <TouchableOpacity style={styles.plusMin} onPress={this.addPassenger}><Text style={styles.plusMinText}>+</Text></TouchableOpacity>
            </View>
            <View style={styles.doneWrapper}>
              <Button onPress={this.doneClicked} full><Text style={styles.buttonDoneText}>DONE</Text></Button>
            </View>
          </View>
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1, 
    margin: 0, 
    padding: 0,
    width: appMetrics.windowWidth, 
    height: appMetrics.windowHeight 
  },
  content: {
    padding: appVariables.padding,
    alignSelf: 'center',
    flexDirection: 'column',
    width: '80%'
  },
  closeButton: {
    top: 0,
  },
  label: {
    fontSize: 12,
    color: appVariables.colorGray,
    marginBottom: 20
  },
  btWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: appVariables.colorBorderBottom,
  },
  bigText: {
    fontSize: 74,
    fontFamily: appVariables.museo700,
    alignSelf: 'center',
    color: appVariables.colorWhite,
    marginBottom: 0,
    width: '100%',
    textAlign: 'center'
  },
  buttonWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20
  },
  plusMin: {
    flex: 1,
    borderWidth: 1,
    borderColor: appVariables.colorBorderBottom,
    padding: 20,
    textAlign: 'center'
  },
  plusMinText: {
    fontSize: 60,
    fontFamily: appVariables.museo700,
    color: appVariables.colorWhite,
    textAlign: 'center',
    alignSelf: 'center'
  },
  doneWrapper: {
    width: '100%',
    marginTop: 40
  }
});

const mapStateToProps = state => {
  return {
    selectOptions: state.BusRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updatePassenger: (payload) => {
      dispatch(updatePassenger(payload))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalPassenger);