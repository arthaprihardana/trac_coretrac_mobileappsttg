import { Button, Container, Content, Text, Header, Left, Body, Title } from 'native-base';
import React, { Component } from 'react';
import { Image, StatusBar, TouchableOpacity, View, Clipboard } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ASeparator } from '../../../../components/atoms';
import styles from './style';
import SvgIcon from '../../../../utils/SvgIcon';

class PaymentFinish extends Component {
  _copy = (code) => {
    Clipboard.setString(code);
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return(
      <Container style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <Header>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body style={{flex: 3}}>
            <Title>Payment</Title>
          </Body>
        </Header>

        <Content>
          <View style={styles.bigHeader}>
            <Image source={require('../../../../assets/images/png/bus-rental-finish-payment.png')} />
            <Text style={[styles.paddingLeftRight, styles.title]}>Congratulation Bryan Barry! You have succesfully rent a car with TRAC To Go</Text>            
            <View style={styles.headerSeparator}></View>

            <View>
              <Text style={[styles.paddingLeftRight, styles.bookingNumberLabel]}>Your booking Number is</Text>
              <Text style={[styles.paddingLeftRight, styles.bookingNumber]}>A09E82093U</Text>
            </View>
          </View>

          <View style={{ padding: appVariables.padding }}>
            <Text style={styles.text}>Detail of your booking and an Email Verification is sent to</Text>
            <Text style={styles.email}>barry@gmail.com</Text>
            <Text style={styles.text}>Please verify your email to get more benefit from TRAC</Text>

            <ASeparator />
            <Text style={[styles.text, styles.dark]}>Referral Code: </Text>
            <Text style={styles.text}>Share this code to your friend. When they use is, you'll get 25% off for your next purchase.</Text>

            <ASeparator />
            <TouchableOpacity onPress={this._copy('SHARE205')}>
              <View style={styles.shareBox}>
                <Text>SHARE205</Text>
              </View>
            </TouchableOpacity>

            <Text style={[styles.copyText, styles.text]}>Copy Code</Text>
          </View>
        </Content>
        
        <Button primary full onPress={() => this.props.navigation.navigate('ExploreLanding')}><Text>CONTINUE</Text></Button>
      </Container>
    )
  }
}

export default withNavigation(PaymentFinish);