/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 07:56:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-08 10:40:16
 */
import { Container, Content, H1, Text, Grid, Right } from 'native-base';
import React, { Component } from 'react';
import { StatusBar, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import _ from "lodash";
import { appStyles, appVariables, appMetrics } from '../../assets/styles';
import { OBlogSection, OHomeBanner, OPopularVehiclesSection, OPromoSection, OWeAreDifferentSection } from '../../components/organisms';
import styles from './style';
import AFullScreenImage from '../../components/atoms/AFullScreenImage';
import AHeader from '../../components/atoms/AHeader';
import MHeader from '../../components/molecules/MHeader';
import ASquareButton from '../../components/atoms/ASquareButton';
import SvgIcon from '../../utils/SvgIcon';
import { getProductService } from '../../actions';

const landingBanner = require("../../assets/images/png/landing-banner.png");

class Explore extends Component {
  state = {
    modalCarRental: false,
    products: []
  }

  componentDidMount = () => {
    if(this.props.products) {
      this.setState({
        products: this.props.products
      });
    } else {
      this.props.getProductService();
    }
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.products !== prevProps.products) {
      return { products: this.props.products }
    }
    return null; 
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.products !== undefined) {
        this.setState({
          products: snapshot.products
        }, () => this.forceUpdate())
      }
    }
  }

  render() {
    const { products } = this.state;
    return (
      <Container style={appStyles.appContainer}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={appVariables.colorBlack}
        />
        <Content>
          
          {/* HOME BANNER */}
          <View style={styles.mainBanner}>
            <AFullScreenImage image={landingBanner} />

            <View style={styles.homeBanner}>
              <MHeader />
            </View>
            
            <View style={styles.homeContent}>
              <H1 style={[styles.h1, appStyles.bold, appStyles.applyFont]}>Explore & Discover with TRAC To Go</H1>
              <Text style={styles.p}>Customize your booking car based on your needs, we got them covered.</Text>

              <View style={styles.homeButtons}>
                <Grid>
                  { products.length > 0  && _.map(products, (v, k) => 
                      <ASquareButton key={k} label={v.MsProductName} icon={v.icon} bgColor="#2246A8" onPress={() => this.props.navigation.push(v.pushTo)} /> ) 
                  }
                </Grid>
              </View>
            </View>
          </View>

          {/* HOME SECTION PROMO */}
          <OPromoSection />

          {/* HOME SECTION WE ARE DIFFERENT */}
          <OWeAreDifferentSection />

          {/* HOME SECTION POPULAR VEHICLES */}
          <OPopularVehiclesSection />

          {/* HOME SECTION BLOG */}
          <OBlogSection />

        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ productService }) => {
  const { loading, products } = productService;
  return { loading, products };
}

export default connect(mapStateToProps, {
  getProductService
})(Explore);