import { StyleSheet } from 'react-native';
import { appMetrics, appVariables } from '../../assets/styles';

const styles = StyleSheet.create({
  mainBanner: {
    width: appMetrics.windowWidth,
    // height: appMetrics.windowHeight - 60,
    height: 610,
    flex: 1,
    overflow: 'hidden'
  },
  homeBanner: {
    flex: 2
  },
  homeContent: {
    flex: 2,
    padding: 20
  },
  homeButtons: {
    height: 110,
    marginTop: 20,
  },
  h1: {
    color: appVariables.colorWhite,
    marginBottom: 10
  },
  p: {
    color: appVariables.colorWhite
  }
});

export default styles;