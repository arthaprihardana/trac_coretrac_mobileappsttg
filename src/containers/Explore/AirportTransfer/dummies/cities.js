export default [
	{
		label: 'Bandung',
		value: 'Bandung',
	},
	{
		label: 'Banda Aceh',
		value: 'Banda Aceh',
	},
	{
		label: 'Bangka',
		value: 'Bangka',
	},
	{
		label: 'Balikpapan',
		value: 'Balikpapan',
	},
	{
		label: 'Banjarmasin',
		value: 'Banjarmasin',
	},
];