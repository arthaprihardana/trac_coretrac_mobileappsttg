export default [
	{
		code: "TYT-AVZ",
		name: "HD Big Bus", 
		original_price: 325000,
		price: 325000,
		stock: 23,
		seating_arrangement: [3, 3],
		image_url: "http://pngimg.com/uploads/bus/bus_PNG8629.png",
		type: "mvp",
		features: [
	    {
	      type: "passanger",
	      value: 45,
	    },
	    {
	      type: "karaoke",
	      value: true,
	    },
	    {
	      type: "wifi",
	      value: true,
	    },
	    {
	      type: "power_socket",
	      value: true,
	    },
	  ],
	  detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \n" +
            "nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\n" +
            "ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \n" +
            "pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
    galleries: [
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    ],
	  is_special_deal: false,
	},
	{
		code: "DHT-XNI", 
		name: "HD Small Bus",
		original_price: 220000,
		price: 185000,
		stock: 4,
		seating_arrangement: [3, 2],
		image_url: "http://pluspng.com/img-png/tour-bus-png-hd-we-do-coach-bus-repair-san-antonio-bus-companies-need-at-prices-they-can-afford-811.png",
		type: "luxury",
		features: [
	    {
	      type: "passanger",
	      value: 56,
	    },
	    {
	      type: "karaoke",
	      value: true,
	    },
	    {
	      type: "wifi",
	      value: false,
	    },
	    {
	      type: "power_socket",
	      value: true,
	    },
	  ],
	  detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \n" +
            "nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\n" +
            "ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \n" +
            "pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
    galleries: [
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    ],
	  is_special_deal: true,
	},
	{
		code: "DHT-XNI", 
		name: "HD Medium Bus",
		original_price: 275000,
		price: 275000,
		stock: 0,
		seating_arrangement: [2, 2],
		image_url: "https://pngimage.net/wp-content/uploads/2018/06/luxury-bus-png-2.png",
		type: "mvp",
		features: [
	    {
	      type: "passanger",
	      value: 45,
	    },
	    {
	      type: "karaoke",
	      value: false,
	    },
	    {
	      type: "wifi",
	      value: true,
	    },
	    {
	      type: "power_socket",
	      value: false,
	    },
	  ],
	  detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \n" +
            "nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\n" +
            "ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \n" +
            "pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
    galleries: [
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    	"https://i.pinimg.com/originals/5c/00/a5/5c00a5936fe9b59eafa357b7008c2be5.jpg",
    ],
	  is_special_deal: false,
	},
];