export default [
	{
		code: "TYT-AVZ",
		name: "Toyota Avanza", 
		original_price: 325000,
		price: 325000,
		stock: 23,
		image_url: "http://pluspng.com/img-png/png-hd-of-car-car-png-hd-1195.png",
		type: "mvp",
		features: [
	    {
	      type: "passanger",
	      value: 6,
	    },
	    {
	      type: "baggage",
	      value: 3,
	    },
	    {
	      type: "transmission",
	      value: "MT",
	    },
	    {
	      type: "airbags",
	      value: true,
	    },
	    {
	      type: "air-conditioner",
	      value: true,
	    },
	  ],
	  detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \n" +
            "nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\n" +
            "ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \n" +
            "pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
	  is_special_deal: false,
	},
	{
		code: "DHT-XNI", 
		name: "Daihatsu Xenia",
		original_price: 220000,
		price: 185000,
		stock: 4,
		image_url: "http://pngimg.com/uploads/tesla_car/tesla_car_PNG24.png",
		type: "luxury",
		features: [
	    {
	      type: "passanger",
	      value: 5,
	    },
	    {
	      type: "baggage",
	      value: 2,
	    },
	    {
	      type: "transmission",
	      value: "AT",
	    },
	    {
	      type: "airbags",
	      value: true,
	    },
	    {
	      type: "air-conditioner",
	      value: false,
	    },
	  ],
	  detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \n" +
            "nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\n" +
            "ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \n" +
            "pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
	  is_special_deal: true,
	},
	{
		code: "DHT-XNI", 
		name: "Honda Jazz",
		original_price: 275000,
		price: 275000,
		stock: 0,
		image_url: "https://i.pinimg.com/originals/af/a4/00/afa400007496b0014137d94dd1e4a496.png",
		type: "mvp",
		features: [
	    {
	      type: "passanger",
	      value: 4,
	    },
	    {
	      type: "baggage",
	      value: 4,
	    },
	    {
	      type: "transmission",
	      value: "AT",
	    },
	    {
	      type: "airbags",
	      value: false,
	    },
	    {
	      type: "air-conditioner",
	      value: true,
	    },
	  ],
	  detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \n" +
            "nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\n" +
            "ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \n" +
            "pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
	  is_special_deal: false,
	},
];