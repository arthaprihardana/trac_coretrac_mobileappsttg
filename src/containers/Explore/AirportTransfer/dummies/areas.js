export default [
	{
		parent: "Bandung",
		label: 'Bandung Indah Plaza, Citarum, Kota Bandung, Jawa Barat',
		value: 'Bandung Indah Plaza, Citarum, Kota Bandung, Jawa Barat',
	},
	{
		parent: "Bandung",
		label: 'Istana BEC, Babakan Ciamis, Kota Bandung, Jawa Barat',
		value: 'Istana BEC, Babakan Ciamis, Kota Bandung, Jawa Barat',
	},
	{
		parent: "Bandung",
		label: 'GOR Pajajaran, Jalan Pajajaran, Pasir Kaliki, Kota Bandung, Jawa Barat',
		value: 'GOR Pajajaran, Jalan Pajajaran, Pasir Kaliki, Kota Bandung, Jawa Barat',
	},
];