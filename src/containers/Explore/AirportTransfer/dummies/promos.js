export default [
	{
		id: 1,
		title: "PROMO AYO MUDIK! 2019",
		description: "Mudik lebaran bareng TRAC lebih hemat dengan promo Ayo Mudik! Dapatkan paket penawaran spesial. Jangan sampai kehabisan!",
		image: require("@images/png/promo/promo1.png"),
		paketInfo: "",
		paket: [{
			title: "Early Bird (Bonus E-Toll Card senilai Rp. 100.000) : 1 -30 April 2019",
			detail: [
				"Paket 3 hari : Avanza Rp. 1.200.000 | Innova Rp. 1.695.000",
				"Paket 7 hari : Avanza RP. 2.750.000 | Innova Rp. 3.850.000",
				"Paket 10 hari : Avanza Rp. 3.850.000 | Innova Rp. 5.390.000"
			]
		}, {
			title: "Regular : 1 -18 Mei 2019",
			detail: [
				"Paket 3 hari : Avanza Rp. 1.500.000 | Innova Rp. 2.045.000",
				"Paket 7 hari : Avanza RP. 3.350.000 | Innova Rp. 4.450.000",
				"Paket 10 hari : Avanza Rp. 4.850.000 | Innova Rp. 6.400.000"
			]
		}, {
			title: "Late : 19 - 31 Mei 2019",
			detail: [
				"Paket 3 hari : Avanza Rp. 1.800.000 | Innova Rp. 2.250.000",
				"Paket 7 hari : Avanza RP. 3.850.000 | Innova Rp. 4.950.000",
				"Paket 10 hari : Avanza Rp. 5.650.000 | Innova Rp. 7.200.000"
			]
		}],
		syaratKetentuan: [
			"Berlaku di seluruh cabang Trac Rental di Indonesia",
			"Reservasi secara offline ke 021 877 877 87 (call center) atau 0811 1177 087 (WhatsApp) atau bisa langsung datang ke cabang terdekat",
			"Hanya untuk layanan sewa mobil lepas kunci Toyota Avanza dan Innova",
			"Harga tidak dapat digabungkan dengan promo lainnya",
			"Periode pemakaian 27 Mei - 13 Juni 2019"
		]
	},
	{
		id: 2,
		title: "Paket Wisata TRAC Bus Services: Jelajah Bandung",
		description: "Ayo liburan keliling Bandung bareng keluarga atau sahabat bersama TRAC Bus Services. Liburan jadi lebih gampang karena Anda tidak perlu memikirkan biaya tambahan seperti tol, parkir, dan tiket masuk wisata. Anda tinggal duduk dan menikmati keseruan perjalanan bareng keluarga atau sahabat.",
		image: require("@images/png/promo/promo2.jpg"),
		paketInfo: "Harga paket sewa Bus pariwisata ke Bandung Mulai dari Rp 380.000/orang* minimal 13 Pax",
		paket: [{
			title: "Paket 1",
			detail: [
				"The Lodge Maribaya",
				"Rumah Mode",
				"Sudirman Streen/Paskal Food Market"
			]
		}, {
			title: "Paket 2",
			detail: [
				"Farm House",
				"Rumah Mode",
				"Sudirman Streen/Paskal Food Market"
			]
		}, {
			title: "Paket 3",
			detail: [
				"Dusun Bambu",
				"Rumah Mode",
				"Sudirman Streen/Paskal Food Market"
			]
		}, {
			title: "Paket 4",
			detail: [
				"The Ranchz",
				"Rumah Mode",
				"Sudirman Streen/Paskal Food Market"
			]
		}],
		syaratKetentuan: [
			"Minimal pemesanan untuk 11 orang",
			"Penjemputan hanya bisa dilakukan untuk area Jakarta dan sekitarnya",
			"Harga termasuk biaya sewa bus, tol, parkir, dan tiket masuk wisata",
			"Harga belum termasuk konsumsi",
			"Harga tidak bisa digabungkan dengan promo lainnya",
			"Reservasi dapat dilakukan minimal H-3 selama persediaan masih ada",
			"Reservasi bisa melalui Reservation Call Center TRAC Bus Services (021) 2983 5555 atau email ke bus@trac.astra.co.id",
			"Promo berlaku sampai 31 Desember 2019",
		]
	},
	{
		id: 3,
		title: "Paket Wisata TRAC Bus Services: Jelajah Surabaya – Malang",
		description: "Mau menjelajahi kota Surabaya atau jalan-jalan ke destinasi kece di Batu, Malang bersama TRAC Bus Services? Kami jamin liburan Anda tambah seru dan tanpa ribet. Anda tinggal duduk dan menikmati perjalanan bareng keluarga atau sahabat, tidak perlu repot memikirkan biaya tambahan seperti tol, parkir, dan tiket masuk wisata.",
		image: require("@images/png/promo/promo3.jpg"),
		paketInfo: "Harga Paket Bus Pariwisata: Mulai dari Rp 470.000/orang* Minimal 13 Pax",
		paket: [{
			title: "Paket 1 Jelajah Surabaya",
			detail: [
				"Monumen Kapal Selam (Monkasel)",
				"Museum House of Sampoerna",
				"Tugu Surabaya & Tugu Pahlawan",
				"Patung Four Face Buddha",
				"Jembatan Suramadu",
				"Wisata Oleh-Oleh Morita Batik/Pasar Atom",
			]
		}, {
			title: "Paket 2 Jelajah Batu – Malang",
			detail: [
				"Batu Secret Zoo",
				"Museum Satwa Eco Green Park",
				"Batu Night Spektakular",
				"Museum Angkut",
				"Air Terjun Coban Rondo",
			]
		}],
		syaratKetentuan: [
			"Minimal pemesanan untuk 11 orang",
			"Penjemputan hanya bisa dilakukan di bandara atau stasiun di Surabaya atau Malang",
			"Harga termasuk biaya sewa bus, tol, parkir, dan tiket masuk wisata",
			"Harga belum termasuk konsumsi",
			"Harga tidak bisa digabungkan dengan promo lainnya",
			"Reservasi dapat dilakukan minimal H-3 selama persediaan masih ada",
			"Reservasi bisa melalui Reservation Call Center TRAC Bus Services (021) 2983 5555 atau email ke bus@trac.astra.co.id",
			"Promo berlaku sampai 31 Desember 2019"
		]
	},
	{
		id: 4,
		title: "Paket Wisata TRAC Bus Services: Petualangan Taman safari",
		description: `Berpetualang ke Taman Safari bersama keluarga atau sahabat yuk! Buat perjalanan Anda lebih nyaman dan mengasyikan dengan paket wisata “Petualangan Taman Safari” dari TRAC Bus Services.
 
		Anda tidak perlu repot memikirkan biaya tambahan seperti tol, parkir, dan tiket masuk wisata. Semua TRAC yang atur, Anda tinggal duduk dan menikmati perjalanan.
		 
		Sewa Bus TRAC sekarang juga!`,
		image: require("@images/png/promo/promo4.jpg"),
		paketInfo: "Paket Wisata Petualangan Taman Safari -> Mulai dari Rp 450.000/orang* minimum 13 Pax",
		paket: [],
		syaratKetentuan: [
			"Minimal pemesanan untuk 11 orang",
			"Harga belum termasuk tiket masuk ke istana panda dan wahana permainan anak",
			"Peket hanya berlaku untuk pemakaian unit luxury bus",
			"Anak usia satu tahun ke atas dikenakan harga normal",
			"Penjemputan hanya bisa dilakukan di area Jakarta dan sekitarnya",
			"Durasi perjalanan satu hari dari pukul 05.00 sampai 22.00 WIB",
			"Harga termasuk biaya sewa bus, tol, parkir, dan tiket masuk ke Taman Safari",
			"Harga belum termasuk konsumsi",
			"Pembayaran uang muka minimal 80 persen dari total harga",
			"Harga tidak bisa digabungkan dengan promo lainnya",
			"Reservasi dapat dilakukan minimal H-1 selama persediaan masih ada",
			"Reservasi bisa melalui Reservation Call Center TRAC Bus Services (021) 2983 5555 atau email ke bus@trac.astra.co.id",
			"Promo berlaku sampai 31 Desember 2019"
		]
	},
	{
		id: 5,
		title: "Paket Wisata TRAC Bus Services: Rafting di Sungai Citarik",
		description: `Menantang arus sungai Citarik bersama sahabat pasti asyik. Yuk pesan paket wisata “Rafting di Sungai Citarik” dari TRAC Bus Services.
 
		Liburan jadi lebih mudah karena Anda tidak perlu memikirkan biaya tambahan seperti tol, parkir, dan bensin. Tambah lagi, paket ini sudah termasuk biaya instruktur rafting, sewa perlengkapan, asuransi, rescue team, fasilitas kebersihan, dan sertifikat rafting. Jadi, Anda tinggal menikmati liburan saja.
		 
		Sewa Bus TRAC sekarang ya!`,
		image: require("@images/png/promo/promo5.jpg"),
		paketInfo: "Paket Wisata Rafting di Sungai Citarik -> Mulai dari Rp 475.000/orang* minimal 30 Pax",
		paket: [],
		syaratKetentuan: [
			"Minimal pemesanan untuk 11 orang",
			"Anak usia satu tahun ke atas dikenakan harga normal",
			"Penjemputan hanya bisa dilakukan di area Jakarta dan sekitarnya",
			"Durasi perjalanan satu hari dari pukul 05.00 sampai 22.00 WIB",
			"Harga termasuk biaya sewa bus, tol, parkir, dan biaya rafting",
			"Harga sudah termasuk makan siang dan snack",
			"Pembayaran uang muka minimal 80 persen dari total harga",
			"Harga tidak bisa digabungkan dengan promo lainnya",
			"Reservasi dapat dilakukan minimal H-1 selama persediaan masih ada",
			"Reservasi bisa melalui Reservation Call Center TRAC Bus Services (021) 2983 5555 atau email ke bus@trac.astra.co.id",
			"Promo berlaku sampai 31 Desember 2019",
		]
	},
	{
		id: 6,
		title: "Paket Wisata TRAC Bus Services: Jelajah Cirebon",
		description: "Jalan-jalan keliling destinasi asyik di Cirebon yuk! TRAC Bus Services punya paket wisata untuk perjalanan liburan Anda bersama keluarga atau sahabat. Anda tidak perlu repot memikirkan biaya tambahan seperti tol, parkir, dan tiket masuk wisata. Semua TRAC yang atur, Anda tinggal duduk dan menikmati perjalanan.",
		image: require("@images/png/promo/promo6.jpg"),
		paketInfo: "",
		paket: [{
			title: "Paket 1",
			detail: [
				"Goa Sunyaragi",
				"Rumah Kerang",
				"Wisata kuliner Nasi Jamblang & Empal Gentong",
				"Wisata oleh-oleh batik Trusmi",
			]
		}, {
			title: "Paket 2",
			detail: [
				"Keraton Kasepuhan",
				"Rumah Kerang",
				"Wisata kuliner Nasi Jamblang & Empal Gentong",
				"Wisata oleh-oleh batik Trusmi",
			]
		}, {
			title: "Paket 3",
			detail: [
				"Masjid Agung Sang Cipta Rasa",
				"Makam Sunan Gunung Jati",
				"Wisata kuliner Nasi Jamblang & Empal Gentong",
				"Wisata oleh-oleh batik Trusmi",
			]
		}, {
			title: "Paket 4",
			detail: [
				"Masjid At Taqwa",
				"Makam Sunan Gunung Jati",
				"Wisata kuliner Nasi Jamblang & Empal Gentong",
				"Wisata oleh-oleh batik Trusmi",
			]
		}],
		syaratKetentuan: [
			"Minimal pemesanan untuk 30 orang",
			"Anak usia satu tahun ke atas dikenakan harga normal",
			"Penjemputan hanya bisa dilakukan di area Jakarta dan sekitarnya",
			"Durasi perjalanan satu hari dari pukul 05.00 sampai 22.00 WIB",
			"Harga termasuk biaya sewa bus, tol, parkir, dan biaya rafting",
			"Harga sudah termasuk makan siang ",
			"Pembayaran uang muka minimal 80 persen dari total harga",
			"Harga tidak bisa digabungkan dengan promo lainnya",
			"Reservasi dapat dilakukan minimal H-1 selama persediaan masih ada",
			"Reservasi bisa melalui Reservation Call Center TRAC Bus Services (021) 2983 5555 atau email ke bus@trac.astra.co.id",
			"Promo berlaku sampai 31 Desember 2019",
		]
	}
];
