export default [
	{
		label: 'CGK, Soekarno Hatta (Terminal 1)',
		value: 'CGK1',
	},
	{
		label: 'CGK, Soekarno Hatta (Terminal 2)',
		value: 'CGK2',
	},
	{
		label: 'CGK, Soekarno Hatta (Terminal 3)',
		value: 'CGK3',
	},
	{
		label: 'Balikpapan',
		value: 'BPP',
	},
	{
		label: 'Banjarmasin',
		value: 'BJB',
	},
	{
		label: 'Bangka',
		value: 'BNK',
	},
];