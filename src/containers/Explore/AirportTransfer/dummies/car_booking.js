export default {
    "serviceType": "Car Rental - Chauffeur Service",
    "list": [
        {
            "direction":
            {
                "from": "Bandung Indah Plaza, Citarum, Kota Bandung, Jawa Barat",
                "to": ""
            },
            "tripIndex": 0,
            "vehicle":
            {
                "code": "TYT-AVZ",
                "name": "Toyota Avanza",
                "original_price": 325000,
                "price": 325000,
                "stock": 23,
                "image_url": "http://pluspng.com/img-png/png-hd-of-car-car-png-hd-1195.png",
                "type": "mvp",
                "features": [
                    {
                        "type": "passanger",
                        "value": 6
                    },
                    {
                        "type": "baggage",
                        "value": 3
                    },
                    {
                        "type": "transmission",
                        "value": "MT"
                    },
                    {
                        "type": "airbags",
                        "value": true
                    },
                    {
                        "type": "air-conditioner",
                        "value": true
                    }],
                "detail": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \nnec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\nullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \npulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
                "is_special_deal": false
            },
            "code": "TYT-AVZ",
            "price": 325000,
            "extras": [
                {
                    "key": "extra_hour",
                    "title": "Add Hour",
                    "price": 150000,
                    "unit": "hour",
                    "icon": 8,
                    "type": "number",
                    "default": 0,
                    "value": 0
                },
                {
                    "key": "out_of_town",
                    "title": "Out of Town",
                    "price": 50000,
                    "unit": "trip",
                    "icon": 9,
                    "type": "number",
                    "default": 0,
                    "value": 0
                },
                {
                    "key": "overnight",
                    "title": "Overnight",
                    "price": 150000,
                    "unit": "hour",
                    "icon": 10,
                    "type": "number",
                    "default": 0,
                    "value": 0
                },
                {
                    "key": "fuel_plan",
                    "title": "Fuel Plans",
                    "price": 50000,
                    "unit": "trip",
                    "icon": 11,
                    "type": "toggle",
                    "default": false,
                    "value": false
                }]
        },
        {
            "direction":
            {
                "from": "Bandung Indah Plaza, Citarum, Kota Bandung, Jawa Barat",
                "to": ""
            },
            "tripIndex": 0,
            "vehicle":
            {
                "code": "DHT-XNI",
                "name": "Daihatsu Xenia",
                "original_price": 220000,
                "price": 185000,
                "stock": 4,
                "image_url": "http://pngimg.com/uploads/tesla_car/tesla_car_PNG24.png",
                "type": "luxury",
                "features": [
                    {
                        "type": "passanger",
                        "value": 5
                    },
                    {
                        "type": "baggage",
                        "value": 2
                    },
                    {
                        "type": "transmission",
                        "value": "AT"
                    },
                    {
                        "type": "airbags",
                        "value": true
                    },
                    {
                        "type": "air-conditioner",
                        "value": false
                    }],
                "detail": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \nnec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\nullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \npulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
                "is_special_deal": true
            },
            "code": "DHT-XNI",
            "price": 185000,
            "extras": [
                {
                    "key": "extra_hour",
                    "title": "Add Hour",
                    "price": 150000,
                    "unit": "hour",
                    "icon": 8,
                    "type": "number",
                    "default": 0,
                    "value": 0
                },
                {
                    "key": "out_of_town",
                    "title": "Out of Town",
                    "price": 50000,
                    "unit": "trip",
                    "icon": 9,
                    "type": "number",
                    "default": 0,
                    "value": 0
                },
                {
                    "key": "overnight",
                    "title": "Overnight",
                    "price": 150000,
                    "unit": "hour",
                    "icon": 10,
                    "type": "number",
                    "default": 0,
                    "value": 0
                },
                {
                    "key": "fuel_plan",
                    "title": "Fuel Plans",
                    "price": 50000,
                    "unit": "trip",
                    "icon": 11,
                    "type": "toggle",
                    "default": false,
                    "value": false
                }]
        }],
    "activeList":
    {
        "direction":
        {
            "from": "Bandung Indah Plaza, Citarum, Kota Bandung, Jawa Barat",
            "to": ""
        },
        "tripIndex": 0,
        "vehicle":
        {
            "code": "TYT-AVZ",
            "name": "Toyota Avanza",
            "original_price": 325000,
            "price": 325000,
            "stock": 23,
            "image_url": "http://pluspng.com/img-png/png-hd-of-car-car-png-hd-1195.png",
            "type": "mvp",
            "features": [
                {
                    "type": "passanger",
                    "value": 6
                },
                {
                    "type": "baggage",
                    "value": 3
                },
                {
                    "type": "transmission",
                    "value": "MT"
                },
                {
                    "type": "airbags",
                    "value": true
                },
                {
                    "type": "air-conditioner",
                    "value": true
                }],
            "detail": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus \nnec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce \n\nullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla \npulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.",
            "is_special_deal": false
        },
        "code": "TYT-AVZ",
        "price": 325000,
        "extras": [
            {
                "key": "extra_hour",
                "title": "Add Hour",
                "price": 150000,
                "unit": "hour",
                "icon": 8,
                "type": "number",
                "default": 0,
                "value": 0
            },
            {
                "key": "out_of_town",
                "title": "Out of Town",
                "price": 50000,
                "unit": "trip",
                "icon": 9,
                "type": "number",
                "default": 0,
                "value": 0
            },
            {
                "key": "overnight",
                "title": "Overnight",
                "price": 150000,
                "unit": "hour",
                "icon": 10,
                "type": "number",
                "default": 0,
                "value": 0
            },
            {
                "key": "fuel_plan",
                "title": "Fuel Plans",
                "price": 50000,
                "unit": "trip",
                "icon": 11,
                "type": "toggle",
                "default": false,
                "value": false
            }]
    },
    "activeListIndex": 0,
    "checkoutData":
    {
        "currency": "Rp",
        "totalPrice": 1020000,
        "digitDelimiter": ".",
        "days": 2,
        "isDisabled": false,
        "listCount": 2,
        "hasNext": false
    },
};