/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 08:03:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 13:44:12
 */
import React, { Component } from 'react';
import { Content, Thumbnail, Button, Text, Textarea } from 'native-base';
import { View, Image, TouchableOpacity, ScrollView, Keyboard, Alert, KeyboardAvoidingView } from 'react-native';
import { appVariables } from '@variables';
import { phonePrefix } from '../../../../../utils/DummyData';
import MCameraProfile from '../../../../../components/molecules/MCameraProfile';
import SvgIcon from '../../../../../utils/SvgIcon';
import ModalDropdown from 'react-native-modal-dropdown';
import styles from './style';
import Mask from 'react-native-mask';
import TextInput from "@airport-transfer-fragments/TextInput";
import Icon from "@airport-transfer-fragments/Icon";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import MainButton from "@airport-transfer-fragments/MainButton";
import { connect } from "react-redux";
import { updateUserProfile, getUserProfile, setNewUser, openCameraForProfile, updateUserProfileSuccess } from "../../../../../actions";
import { Actions } from 'react-native-router-flux';
import Lang from '../../../../../assets/lang';

class Account extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  state = {
    displayName: `${this.props.login.Data.FirstName} ${this.props.login.Data.LastName}`,
    idNumber: this.props.login.Data.NoKTP,
    phoneData: phonePrefix,
    selectedPhoneNumber: phonePrefix[0].value,
    selectedPhoneImage: phonePrefix[0].image,
    phoneNumber: this.props.login.Data.NoHandphone !== null ? this.props.login.Data.NoHandphone.split(/^(\+62|0)/g)[2] : null,
    image: this.props.login.Data.ImagePath !== null ? 
      { uri: `${this.props.login.Data.ImagePath}?random_number=${new Date().getTime()}` } : 
      require("@images/png/avatar/changePhotoAvatar.png")
  }

  onSubmit = () => {
    Keyboard.dismiss();
    this.props.updateUserProfileSuccess(false);
    let n = this.state.displayName.split(" ")
    let fn = n[0];
    let ln = n.length > 2 ? _.slice(n, 1, n.length).join(" ") : n[1];
    let FormData = {
      FirstName: fn,
      LastName: ln,
      NoHandphone: `${this.state.selectedPhoneNumber}${this.state.phoneNumber}`,
      NoKTP: this.state.idNumber,
    }
    if(this.props.captureProfile !== null) {
      if(this.props.captureProfile.type === "galeri") {
        FormData.ImagePath = this.props.captureProfile.base64;
      } else {
        let ext = this.props.captureProfile.uri.split(".");
        FormData.ImagePath = `data:image/${ext[ext.length - 1]};base64, ${this.props.captureProfile.base64}`
      }
    }
    this.props.updateUserProfile({
      FormData: FormData,
      token: this.props.login.token,
      UserId: this.props.login.Data.Id
    })
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.updateUser !== prevProps.updateUser) {
      return { updateUser: this.props.updateUser }
    }
    if(this.props.user !== prevProps.user) {
      return { user: this.props.user }
    }
    if(this.props.login !== prevProps.login) {
      return { login: this.props.login }
    }
    return null; 
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.updateUser !== undefined) {
        if(snapshot.updateUser) {
          this.props.getUserProfile({
            UserId: this.props.login.Data.Id,
            token: this.props.login.token
          });
        }
      }
      if(snapshot.user !== undefined) {
        this.props.setNewUser({
          Data: snapshot.user,
          token: this.props.login.token
        });
        Actions.pop();
      }
    }
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderPhoneNumber = (option, index) => {
    return (
      <View key={index} style={styles.dropdownItemWrapper}>
        <Mask shape={'circle'}>
          <Image source={option.image} style={styles.dropdownImage} />
        </Mask>
        <Text style={styles.dropdownText}>{option.value}</Text>
      </View>
    );
  }

  // ---------------------------------------------------

  _phoneNumberSelected = (idx, value) => {
    this.setState({
      selectedPhoneNumber: value.value,
      selectedPhoneImage: value.image
    });
  }

  // ---------------------------------------------------

  _renderChangePhoto() {
    let image = this.state.image;
    if(this.props.captureProfile !== null) {
      image = { uri: this.props.captureProfile.uri }
    }
    return (
      <View style={{
        flex: 1,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
      }}>
        <View style={{
          flex: -1,
          paddingHorizontal: 20
        }}>
          <View style={{width: 97, height: 97, borderRadius: 49.5, backgroundColor: '#cccccc'}}>
            <Thumbnail style={{ width: 97, height: 97, borderRadius: 49.5 }} source={image} />
          </View>
        </View>
        <View style={{
          flex: 3,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <Button onPress={() => {
            Alert.alert(
              'Info',
              'Dari mana anda akan mengambil gambar?',
              [
                {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                {
                  text: 'Galeri',
                  onPress: () => Actions.push("Galeri", { route: "Account" }),
                  style: 'cancel',
                },
                {text: 'Kamera', onPress: () => this.props.openCameraForProfile(true)},
              ],
              {cancelable: false},
            );
            }} bordered dark style={{ height: '50%', borderRadius: 7 }}>
            <Text style={{ color: '#000000' }}>{Lang.changePhoto[this.props.lang]}</Text>
          </Button>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderTextInput(label, icon) {
    return (
      <View style={{
        paddingHorizontal: 20,
        paddingVertical: 20,
      }}>

        <Text
          style={{
            fontSize: 12,
            color: appVariables.colorGray
          }}>
          {label}
        </Text>

        <View style={{
          flex: -1,
          flexDirection: "row",
          paddingTop: 10,
          borderBottomWidth: 1,
          borderColor: "#aaaaaa",
          marginBottom: 5,
        }}>

          <TextInput
            style={{
              flex: 1,
              paddingHorizontal: 10,
              height: 40,
              justifyContent: "center"
            }}
            value={this.state.displayName}
            onChangeText={text => this.setState({ displayName: text })} />

          <TouchableOpacity
            style={{ paddingLeft: 20 }}>
            <Icon name={icon} color={"#aaaaaa"} size={20} />
          </TouchableOpacity>

        </View>
      </View>
    );
  }

  _renderIdNumber(label, icon) {
    return (
      <View style={{
        paddingHorizontal: 20,
        paddingVertical: 20,
      }}>

        <Text
          style={{
            fontSize: 12,
            color: appVariables.colorGray
          }}>
          {label}
        </Text>

        <View style={{
          flex: -1,
          flexDirection: "row",
          paddingTop: 10,
          borderBottomWidth: 1,
          borderColor: "#aaaaaa",
          marginBottom: 5,
        }}>

          <TextInput
            style={{
              flex: 1,
              paddingHorizontal: 10,
              height: 40,
              justifyContent: "center"
            }}
            value={this.state.idNumber}
            maxLength={16}
            keyboardType="number-pad"
            onChangeText={text => this.setState({ idNumber: text })} />

          <TouchableOpacity
            style={{ paddingLeft: 20 }}>
            <Icon name={icon} color={"#aaaaaa"} size={20} />
          </TouchableOpacity>

        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderEmailandIDNumber(label, content) {
    return (
      <View style={{
        paddingHorizontal: 20,
        paddingVertical: 20
      }}>

        <Text
          style={{
            fontSize: 12,
            color: appVariables.colorGray,
          }}>
          {label}
        </Text>

        <View style={{
          flex: -1,
          flexDirection: "row",
          paddingTop: 10,
          height: 40,
          borderColor: "#aaaaaa",
          marginBottom: 5,
        }}>

          <Text>{content}</Text>

        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderFieldPhoneNumber() {
    return (
      <View style={{ paddingBottom: 20 }}>
        <View style={styles.container}>
          <View style={styles.subContainer}>
            <View>
              <Text style={styles.title}>{Lang.phoneNumber[this.props.lang]}</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: 'column' }}>
            <View style={{ flex: 1 }}>
              <ModalDropdown
                style={{
                  marginLeft: 20,
                  flex: 1
                }}
                ref="phone_number"
                options={this.state.phoneData}
                onSelect={(idx, value) => this._phoneNumberSelected(idx, value)}
                renderRow={this._renderPhoneNumber}>
                <View style={styles.sortWrapper}>
                  <Mask style={styles.itemImage} shape={'circle'}>
                    <Image style={styles.image} source={this.state.selectedPhoneImage} />
                  </Mask>
                  <Text style={styles.labelPhoneStyle}>{this.state.selectedPhoneNumber}</Text>
                  <SvgIcon name="icoSort" style={styles.iconWrapper} />
                </View>
              </ModalDropdown>
            </View>

            <View style={{
              flex: 3,
              flexDirection: 'row',
              borderBottomWidth: 1,
              marginLeft: 20,
              borderBottomColor: "#aaaaaa"
            }}>
            </View>
          </View>
          <View style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: 20,
          }}>
            <View style={{
              flex: 1,
              borderBottomWidth: 1,
              height: 35,
              borderBottomColor: "#aaaaaa",
              marginRight: 5,
              marginTop: -5,
            }}>
              <Textarea rowSpan={5} placeholder="" maxLength={12} style={styles.textarea} keyboardType="number-pad" value={this.state.phoneNumber} onChangeText={ text => this.setState({ phoneNumber: text }) } />
            </View>
            <View style={styles.editIcon}>
              <Icon name="pencil" color={"#aaaaaa"} size={20} />
            </View>
          </View>
        </View>
      </View>
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={Lang.accountSettings[this.props.lang]}
          onBackPress={() => Actions.pop()}
        />
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ backgroundColor: '#FFFFFF' }}
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
            >
            <KeyboardAvoidingView behavior="position" enabled style={{ flex: 1 }} keyboardVerticalOffset={100}>
            {this._renderChangePhoto()}
            {/* <View style={{
              flex: 1,
              flexDirection: "row"
            }}>
              <View style={{ flex: 1 }}>
                {this._renderTextInput(Lang.name[this.props.lang], "pencil")}
              </View>
              <View style={{ flex: 1 }}>
                {this._renderIdNumber(Lang.ktpNumber[this.props.lang], "pencil")}
              </View>
            </View> */}

            {this._renderTextInput(Lang.name[this.props.lang], "pencil")}

            {this._renderIdNumber(Lang.ktpNumber[this.props.lang], "pencil")}


            {this._renderEmailandIDNumber(Lang.email[this.props.lang], this.props.login.Data.EmailPersonal)}
            {this._renderFieldPhoneNumber()}
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
        { this.props.openCameraProfile && 
        <MCameraProfile 
          visible={this.props.openCameraProfile}
          onCapture={() => console.log('on capture')}
          onClose={() => this.props.openCameraForProfile(false) }
        /> }
        <View style={{ width: '100%' }}>
          <MainButton 
            isLoading={this.props.loading}
            label={Lang.sendButton[this.props.lang]}
            onPress={ () => !this.props.loading ?  this.onSubmit() : {} }
          />
        </View>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ userProfile, authentication, camera, language }) => {
  const { loading, user, updateUser } = userProfile;
  const { login } = authentication;
  const { openCameraProfile, captureProfile } = camera;
  const { lang } = language;
  return { loading, user, updateUser, login, openCameraProfile, captureProfile, lang }
}

export default connect(mapStateToProps, {
  updateUserProfile,
  getUserProfile,
  setNewUser,
  openCameraForProfile,
  updateUserProfileSuccess
})(Account);
