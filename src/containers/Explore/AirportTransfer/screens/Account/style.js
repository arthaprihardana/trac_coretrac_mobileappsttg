import {StyleSheet} from 'react-native';
import { appVariables } from '@variables';

const styles = StyleSheet.create({
  title:{
    fontSize: 12,
    color: appVariables.colorGray,
    flex: 1,
    lineHeight: 20,
  },
  container:{
    flexDirection: 'row',
  },
  subContainer:{
    flex:1,
    flexDirection: 'row',
    paddingHorizontal:20,
  },
  labelContent: {
    flex: 9,
    borderBottomWidth: 1,
    height:35,
    borderBottomColor: '#aaaaaa',
    marginLeft:20,
    marginTop:-5, 
    borderWidth:1,
  },
  editIcon: {
    flex: -1,
    paddingBottom:5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textarea: { 
    paddingLeft: 0, left: 0, marginLeft: 0,
    fontSize: 16,
    color: appVariables.colorDark,
    fontFamily: appVariables.museo500,
    lineHeight: 20,
  },
  dropdownItemWrapper: { 
    flexDirection: 'row', 
    alignItems: 'center', 
    borderBottomColor: '#C3C8D3', 
    borderBottomWidth: 2, 
    padding: 10, 
  },
  dropdownImage: { 
    width: 20, 
    height: 20
  },
  dropdownText: { 
    fontSize: 16, 
    fontFamily: appVariables.museo500, 
    color: '#242842', 
    marginLeft: 10 },
  sortWrapper: { 
    flexDirection: 'row', 
    height: 30, 
    alignItems: 'center' 
  },
  itemImage: { marginRight: 5},
  image: { width: 20, height: 20 },
  labelPhoneStyle: { 
    fontSize: 16 ,
    fontFamily: appVariables.museo500, 
    marginLeft: 10, 
    marginRight: 10, 
    marginTop: 0, },
  iconWrapper: { marginTop: 0 },
})

export default styles;