import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import MainButton from "@airport-transfer-fragments/MainButton";
import AuthContainer from "@airport-transfer-fragments/AuthContainer";

import styles from "./style";


export default class ResetPassword extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        new_password: "",
        new_password_confirm: "",
      },
    };
  }


// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// --------------------------------------------------- 

  goToOtp() {
    this.props.navigation.pop(2);
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderForm() {
    return (
      <View>
        <FloatingTextInput
          label={ "New Password" }
          onChangeText={ (value) => this.onChangeForm("new_password", value) }
          value={ this.state.form.new_password }
          secureTextEntry
          note="Uppercase, Lowercase, adn Number"
        />

        <FloatingTextInput
          label={ "Confirm New Password" }
          onChangeText={ (value) => this.onChangeForm("new_password_confirm", value) }
          value={ this.state.form.new_password_confirm }
          secureTextEntry
        />

        <MainButton
          label={ "SUBMIT" }
          onPress={ () => this.goToOtp() }
          rounded
          style={{
            marginTop: 40,
          }}
        />

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <AuthContainer 
        { ... this.props }
        title={ "Reset Password" }
      >

        { this._renderForm() }        

      </AuthContainer>
    );
  }
}
