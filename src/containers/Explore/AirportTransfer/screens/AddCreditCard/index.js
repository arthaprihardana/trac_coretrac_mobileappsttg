/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-05 00:28:18 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-12 08:04:30
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  Alert
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import MainButton from "@airport-transfer-fragments/MainButton";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { saveCreditCard, setPrimaryCreditCard } from "../../../../../actions"
import _ from "lodash";
import { CREDIT_CARD_LIBRARY } from "../../../../../constant";

class AddCreditCard extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  state = {
    form: {
      number: "",
      name: "",
      expiration: "",
      bank: ""
      // cvv: "",
    },
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------
  componentWillUnmount = () => {
    Actions.refresh({ refresh: true });
  }
  
  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.saveCC !== prevProps.saveCC) {
      return { saveCC: this.props.saveCC }
    }
    if(this.props.primaryCC !== prevProps.primaryCC) {
      return { primaryCC: this.props.primaryCC }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.saveCC !== undefined) {
        if(snapshot.saveCC.Id) {
          this.props.setPrimaryCreditCard({
            FormData: {Id: snapshot.saveCC.Id},
            token: this.props.login.token,
            UserLoginId: this.props.login.Data.Id
          });
        } else {
          Alert.alert(
            'Information',
            'Oops! Something went wrong!',
            [
              {text: 'OK', onPress: () => {}},
            ],
            {cancelable: false},
          );
        }
      }
      if(snapshot.primaryCC !== undefined) {
        Alert.alert(
          'Information',
          'Credit Card Succesfully saved',
          [
            {text: 'OK', onPress: () => Actions.popTo("ProfileCreditCard")},
          ],
          {cancelable: false},
        );
      }
    }
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  formatCreditCardNumber(value) {
    value = value + "";
    value = value.replace(/\-/g, "");

    let split = [];
    while (value.length > 0) {
      split.push(value.substring(0, 4));

      value = value.substring(4);
    }

    return split.join("-").substring(0, 19);
  }

  formatExpiration(value) {
    value = value + "";
    value = value.replace(/\//g, "");

    let split = [];
    while (value.length > 0) {
      split.push(value.substring(0, 2));

      value = value.substring(2);
    }

    if ((split[0] * 1) > 12) {
      return split[0].substring(0, 1);
    }

    return split.join("/").substring(0, 5);
  }

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }
  
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderContent() {
    return (
      <View style={ styles.topSectionContainer }>
        <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
          <FloatingTextInput
            label={ "Credit/Debit Card Number" }
            keyboardType={ "numeric" }
            onChangeText={ (value) => this.onChangeForm("number", value) }
            value={ this.state.form.number }
            customFormat={ (value) => this.formatCreditCardNumber(value) }
          />

          <FloatingTextInput
            label={ "Name on Card" }
            onChangeText={ (value) => this.onChangeForm("name", value) }
            value={ this.state.form.name }
          />

          <View style={ styles.splitFormContainer }>
            <FloatingTextInput
              label={ "Exp Date (MM/YY)" }
              keyboardType={ "numeric" }
              onChangeText={ (value) => this.onChangeForm("expiration", value) }
              value={ this.state.form.expiration }
              customFormat={ (value) => this.formatExpiration(value) }
            />

            <View>
              <FloatingTextInput
                label={ "Bank" }
                onChangeText={ (value) => this.onChangeForm("bank", value) }
                value={ this.state.form.bank }
              />

              {/* <TouchableOpacity
                style={ styles.cvvButton.container }
              >
                <Text color={ "#00c0c0" } size={ 12 }>
                  What is CVV?
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }

  onSubmit = () => {
    let x = _.filter(CREDIT_CARD_LIBRARY, v => v.reg.test(_.replace(this.state.form.number, /-/g, "")));
    this.props.saveCreditCard({
      FormData: {
        UserId: this.props.login.Data.Id,
        CreditCard : this.state.form.number,
        CardExpired : this.state.form.expiration,
        CardPublisher : this.state.form.bank,
        CardImage : x.length > 0 ? x[0].img : null,
        CardType : x.length > 0 ? x[0].type : null
      },
      token: this.props.login.token
    })
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Credit/Debit Card"}
          onBackPress={() => this.props.navigation.pop()}
        />

        {this._renderContent()}

        <MainButton
            isLoading={this.props.loading}
            label={"SAVE CREDIT CARD"}
            onPress={()=> this.onSubmit() }>
        </MainButton>

      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ creditCard, authentication }) => {
  const { saveCC, primaryCC, loading } = creditCard;
  const { login } = authentication;
  return { saveCC, primaryCC, loading, login };
}

export default connect(mapStateToProps, {
  saveCreditCard,
  setPrimaryCreditCard
})(AddCreditCard);