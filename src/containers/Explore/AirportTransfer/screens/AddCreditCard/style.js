export default {
  contentContainer: {
    flex: -1,
    flexDirection: 'column',
    paddingVertical: 20,
    paddingHorizontal: 20
  },
  buttonWrapper: {
    borderRadius: 7,
    height: 47,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    backgroundColor: 'white',
    paddingHorizontal: 50,
  },
  topSectionContainer: {
    flex: 1,
    paddingHorizontal: 20,
    // borderBottomWidth: 1,
    // borderColor: "#cccccc",
  },
  bottomSectionContainer: {
    paddingVertical: 10,
    paddingHorizontal: 20,
  },

  splitFormContainer: {
    flex: -1,
    flexDirection: "row",
    justifyContent: "space-between",
  },

  ccSave: {
    container: {
      flex: -1,
      flexDirection: "row",
      marginTop: 40,
      paddingVertical: 20,
      justifyContent: "space-between",
    },
  },

  cvvButton: {
    container: {
      position: "absolute",
      bottom: -24,
    },
  },

  agreement: {
    container: {
      paddingVertical: 20,
    },
  },
}