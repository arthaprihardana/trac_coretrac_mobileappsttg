import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
  },

  innerContainer: {
    flex: 1,
    padding: 20,
    paddingTop: 10,
    backgroundColor: appVariables.colorWhite,
  },

  innerContentContainer: {
    paddingBottom: 40,
  },

  form: {
    extra: {
      container: {
        flex: -1,
        flexDirection: "row",
        paddingVertical: 20,
        justifyContent: "space-between",
      },
    },

    bottom: {
      container: {
        flex: -1,
        alignItems: "center",
        marginTop: 15,
      },

      register: {
        container: {
          flex: -1,
          flexDirection: "row",
          justifyContent: "center",
          paddingVertical: 5,
        },

        button: {
          container: {
            flex: -1,
            marginLeft: 3,
          },
        },
      },
    },
  },

  picker: {
    container: {
      flex: 1,
    },

    nativePicker: { 
      borderWidth: 1,
      // borderWidth: 0,
      // position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
    },
  },
}