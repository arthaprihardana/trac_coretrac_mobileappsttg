import { Container, Content } from "native-base";
import { Actions } from "react-native-router-flux";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  KeyboardAvoidingView,
} from "react-native";
import DatePicker from "react-native-datepicker";
import moment from "moment";
import "moment/locale/id";
import {
  appVariables,
  appMetrics,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import Header from "@airport-transfer-fragments/Header";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import MainButton from "@airport-transfer-fragments/MainButton";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";
import SocialMediaLogin from "@airport-transfer-fragments/SocialMediaLogin";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";

import styles from "./style";


export default class FlightDetail extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    const navData = props.navData;
    const tripsData = navData.summaryData.submittedData.trips;
    const trips = [];
    for (x in tripsData) {
      trips.push({
        key: x,
        from: (tripsData[x].from_airport ? tripsData[x].from_airport : (tripsData[x].from_destination ? tripsData[x].from_destination : "")),
        to: (tripsData[x].to_destination ? tripsData[x].to_destination : (tripsData[x].to_airport ? tripsData[x].to_airport : "")),
        code: tripsData[x].flight.name,
        number: tripsData[x].flight.number,
        hour: tripsData[x].arrival_schedule.time,
      });
    }

    this.state = {
      forms: trips,

      navData: navData,
    };
  }


// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeForm(name, value, formIndex) {
    const forms = this.state.forms;
    forms[formIndex][name] = value;

    this.setState({
      forms
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  continue() {
    const data = this.state.forms;

    if (Object.keys(this.props.navData).length == 0) {
      this.props.navigation.pop();

      return true;
    }

    Actions.Login({
      navData: this.props.navData,
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderForm(index) {
    return (
      <View
        key={ index }
        style={{flex: -1, marginVertical: 30,}}
      >
        <Text size={ 20 }>
          { this.state.forms[index].from } - { this.state.forms[index].to }
        </Text>


        <View style={{
          flex: -1,
          flexDirection: "row",
          justifyContent: "space-between",
        }}>
          <FloatingTextInput
            label={ "Airline Code" }
            onChangeText={ (value) => this.onChangeForm("code", value, index) }
            value={ this.state.forms[index].code }
            autoCapitalize={ "characters" }
            half
          />

          <FloatingTextInput
            label={ "Flight Number" }
            onChangeText={ (value) => this.onChangeForm("number", value, index) }
            value={ this.state.forms[index].number }
            keyboardType={ "numeric" }
            half
          />
        </View>

        <FloatingTextInput
          label={ "Pickup Hour" }
          onChangeText={ (value) => this.onChangeForm("hour", value, index) }
          value={ this.state.forms[index].hour }
          timePicker
          half
          note={ "You may chnage the pickup hour if you wish to" }
        />    
      </View>
    );
  }

// ---------------------------------------------------
  
  _renderMain() {
    return (
      <KeyboardAvoidingView
        behavior={ "padding" }
        enabled={false}
        style={ styles.container }
      >
        <ScrollView
          style={ styles.innerContainer }
          contentContainerStyle={ styles.innerContentContainer }
        >

          <Text color={ appVariables.colorGray } style={{paddingTop: 20,}}>
            Please confirm the flight detail for a more approximatepickup hour
          </Text>

          { this.state.forms.map((item, index) => {
            return this._renderForm(index);
          }) }

          <MainButton
            label={ "CONTINUE" }
            onPress={ () => this.continue() }
            rounded
          />
        
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ProgressedContainer
        header={{
          label: "Flight Detail",
          onBackPress: () => Actions.pop(),
        }}
        steps= { this.state.navData.processSteps }
        currentStepIndex={ this.state.navData.processSteps.indexOf("Flight Detail") }
        { ... this.props }
      >

        { this._renderMain() }

      </ProgressedContainer>
    );
  }
}
