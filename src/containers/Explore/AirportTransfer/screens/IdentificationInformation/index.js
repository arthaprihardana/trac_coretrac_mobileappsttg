/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 08:07:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 13:46:46
 */
import React, { Component } from 'react';
import { Content } from 'native-base';
import { View, TouchableOpacity, Image, ScrollView, Keyboard, Platform, Alert, KeyboardAvoidingView } from 'react-native';
import { appVariables } from '@variables';
import SvgIcon from '../../../../../utils/SvgIcon';
import MCameraSim from '../../../../../components/molecules/MCameraSim';
import MCameraKtp from '../../../../../components/molecules/MCameraKtp';
import styles from './style';
import Mask from 'react-native-mask';
import TextInput from "@airport-transfer-fragments/TextInput";
import Icon from "@airport-transfer-fragments/Icon";
import Header from "@airport-transfer-fragments/Header";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import _ from "lodash";
import { connect } from "react-redux";
import { openCameraForSim, openCameraForKtp, updateUserProfile, getUserProfile, setNewUser, clearCaptureForAll, updateUserProfileSuccess } from "../../../../../actions"; 
import { Actions } from 'react-native-router-flux';
import Lang from '../../../../../assets/lang';
// import MCamera from '../../../components/molecules/MCamera';

class IdentificationInformation extends Component {

  state = {
    activeCitizen: parseInt(this.props.login.Data.IsForeigner),
    NoKTP: this.props.login.Data.NoKTP,
    NoSIM: this.props.login.Data.NoSIM,
    Address: this.props.login.Data.Address,
    ImageSim: this.props.login.Data.ImageSIM !== null ? `${this.props.login.Data.ImageSIM}?random_number=${new Date().getTime()}` : null,
    ImageKtp: this.props.login.Data.ImageKTP !== null ? `${this.props.login.Data.ImageKTP}?random_number=${new Date().getTime()}` : null
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.updateUser !== prevProps.updateUser) {
      return { updateUser: this.props.updateUser }
    }
    if(this.props.user !== prevProps.user) {
      return { user: this.props.user }
    }
    if(this.props.login !== prevProps.login) {
      return { login: this.props.login }
    }
    if(this.props.captureSim !== prevProps.captureSim) {
      return { captureSim: this.props.captureSim }
    }
    if(this.props.captureKtp !== prevProps.captureKtp) {
      return { captureKtp: this.props.captureKtp }
    }
    return null; 
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.updateUser !== undefined) {
        if(snapshot.updateUser) {
          this.props.getUserProfile({
            UserId: this.props.login.Data.Id,
            token: this.props.login.token
          });
        }
      }
      if(snapshot.user !== undefined) {
        this.props.clearCaptureForAll();
        this.props.setNewUser({
          Data: snapshot.user,
          token: this.props.login.token
        });
        Actions.pop();
      }
      if(snapshot.captureSim !== undefined) {
        this.setState({
          ImageSim: snapshot.captureSim.uri
        })
      }
      if(snapshot.captureKtp !== undefined) {
        this.setState({
          ImageKtp: snapshot.captureKtp.uri
        })
      }
    }
  }

  onSubmit = () => {
    Keyboard.dismiss();
    this.props.updateUserProfileSuccess(false);
    let FormData = {
      IsForeigner: this.state.activeCitizen,
      NoKTP: this.state.NoKTP,
      NoSIM: this.state.NoSIM,
      Address: this.state.Address
    };
    if(this.props.captureSim !== null) {
      if(this.props.captureSim.type === "galeri") {
        FormData.ImageSIM = this.props.captureSim.base64
      } else {
        var ext = this.props.captureSim.uri.split(".");
        FormData.ImageSIM = `data:image/${ext[ext.length - 1]};base64, ${this.props.captureSim.base64}`
      }
    }
    if(this.props.captureKtp !== null) {
      if(this.props.captureKtp.type === "galeri") {
        FormData.ImageKTP = this.props.captureKtp.base64
      } else {
        var ext = this.props.captureKtp.uri.split(".");
        FormData.ImageKTP = `data:image/${ext[ext.length - 1]};base64, ${this.props.captureKtp.base64}`
      }
    }
    this.props.updateUserProfile({
      FormData: FormData,
      token: this.props.login.token,
      UserId: this.props.login.Data.Id
    })
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderTextInput(label, value, icon, field) {
    return (
      <View style={{
        paddingHorizontal: 20,
        paddingVertical: 20
      }}>

        <Text
          style={{fontSize: 12,color: appVariables.colorGray}}>
          {label}
        </Text>

        <View style={styles.textInputWrapper}>
          <TextInput
            style={{
              flex: 1,
              paddingHorizontal: 10,
              justifyContent: "center",
              height:40,
            }} 
            value={value} 
            onChangeText={text => this.handleChange(field, text) } />

          <TouchableOpacity
            style={{ paddingLeft: 20 }}>
            <Icon name={icon} color={"#aaaaaa"} size={20} />
          </TouchableOpacity>

        </View>
      </View>
    );
  }

  _renderNumberInput(label, value, icon, field, max) {
    return (
      <View style={{
        paddingHorizontal: 20,
        paddingVertical: 20
      }}>

        <Text
          style={{fontSize: 12,color: appVariables.colorGray}}>
          {label}
        </Text>

        <View style={styles.textInputWrapper}>
          <TextInput
            style={{
              flex: 1,
              paddingHorizontal: 10,
              justifyContent: "center",
              height:40,
            }} 
            value={value}
            keyboardType="number-pad"
            maxLength={max}
            onChangeText={text => this.handleChange(field, text) }  />

          <TouchableOpacity
            style={{ paddingLeft: 20 }}>
            <Icon name={icon} color={"#aaaaaa"} size={20} />
          </TouchableOpacity>

        </View>
      </View>
    );
  }

  handleChange = (field, value) => {
    let f = this.state;
    f[field] = value;
    this.setState({ f });
  }

  // ---------------------------------------------------
  _renderCitizen(label, index) {
    return (
      <View style={styles.citizenWrapper}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: -1,
            flexDirection: 'column',
          }}>
            <RoundedCheck
              value={this.state.activeCitizen === index}
              onPress={() => this.setState({
                activeCitizen: index
              }) }>
            </RoundedCheck>
          </View>

          <View style={{
            flex: 10,
            flexDirection: 'column',
          }}>
            <Text
              color={appVariables.colorBlack}
              size={12}>
              {label}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header title={Lang.identificationInformation[this.props.lang]} onBackPress={() => this.props.navigation.pop()}
        />
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ backgroundColor: '#FFFFFF' }}
            keyboardShouldPersistTaps="always">
            <KeyboardAvoidingView behavior="position" enabled style={{ flex: 1 }} keyboardVerticalOffset={100}>
              <View style={{ marginTop: 20 }}>
                <Text style={styles.title}>{Lang.identificationInformationQuote[this.props.lang]}</Text>
              </View>

              <View style={{
                flex: 1,
                flexDirection: 'row'
              }}>
                <View style={{
                  flex: 1,
                  flexDirection: 'column'
                }}>
                  {this._renderCitizen(Lang.wni[this.props.lang], 0)}
                </View>
                <View style={{
                  flex: 1,
                  flexDirection: 'column'
                }}>
                  {this._renderCitizen(Lang.wna[this.props.lang], 1)}
                </View>
              </View>

              <View style={styles.container}>
                { this.props.login.Data.ImageKTP !== null || this.props.captureKtp !== null ?
                <TouchableOpacity onPress={ () => {
                    Alert.alert(
                      'Info',
                      'Dari mana anda akan mengambil gambar?',
                      [
                        {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                        {
                          text: 'Galeri',
                          onPress: () => Actions.push("Galeri", { route: "IdentificationInformation", type: "ktp" }),
                          style: 'cancel',
                        },
                        {text: 'Kamera', onPress: () => this.props.openCameraForKtp(true)},
                      ],
                      {cancelable: false},
                    );  
                  }} style={{ marginTop: 20, marginRight: 20, borderWidth: 1, borderColor: appVariables.colorGray, borderStyle: 'dashed', borderRadius: 10, justifyContent: 'center', alignItems: 'center', padding: 5, width: 135, height: 90.08 }}>
                  <View style={{ width: 125, height: 80.08, borderRadius: 6, backgroundColor: '#cccccc' }}>
                    <Image 
                      source={{ uri: this.state.ImageKtp }} 
                      style={{ width: 125, height: 80.08, borderRadius: 6 }} 
                      resizeMode="cover"
                      progressiveRenderingEnabled={true} />
                  </View>
                  <View style={{ position: 'absolute', bottom: 5, left: 5, width: 125, height: 20, backgroundColor: 'rgba(0,0,0,0.3)', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 10 }}>{Lang.imageKtp[this.props.lang]}</Text>
                  </View>
                </TouchableOpacity> : 
                <TouchableOpacity onPress={ () => {
                    Alert.alert(
                      'Info',
                      'Dari mana anda akan mengambil gambar?',
                      [
                        {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                        {
                          text: 'Galeri',
                          onPress: () => Actions.push("Galeri", { route: "IdentificationInformation", type: "ktp" }),
                          style: 'cancel',
                        },
                        {text: 'Kamera', onPress: () => this.props.openCameraForKtp(true)},
                      ],
                      {cancelable: false},
                    );  
                  }} style={{ marginTop: 20, marginRight: 20 }}>
                  <View style={styles.scanSquare}>
                    <SvgIcon name="icoAddLicense" style={styles.scanIcon} />
                    <Text style={styles.scanWording}>{this.props.login.Data.ImageKTP !== null || this.props.captureKtp !== null ? Lang.update[this.props.lang] : Lang.add[this.props.lang] } {Lang.imageKtp[this.props.lang]}</Text>
                    <View style={styles.photoFrame}>
                      <Mask shape="rounded"></Mask>
                    </View>
                  </View>
                </TouchableOpacity>
                }

                { this.props.login.Data.ImageSIM !== null || this.props.captureSim !== null ?
                <TouchableOpacity onPress={ () => {
                  Alert.alert(
                      'Info',
                      'Dari mana anda akan mengambil gambar?',
                      [
                        {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                        {
                          text: 'Galeri',
                          onPress: () => Actions.push("Galeri", { route: "IdentificationInformation", type: "sim" }),
                          style: 'cancel',
                        },
                        {text: 'Kamera', onPress: () => this.props.openCameraForSim(true) },
                      ],
                      {cancelable: false},
                    );                
                  }} style={{ marginTop: 20, marginRight: 20, borderWidth: 1, borderColor: appVariables.colorGray, borderStyle: 'dashed', borderRadius: 10, justifyContent: 'center', alignItems: 'center', padding: 5, width: 135, height: 90.08 }}>
                  <View style={{ width: 125, height: 80.08, borderRadius: 6, backgroundColor: '#cccccc' }}>
                    <Image 
                      source={{ uri: this.state.ImageSim }} 
                      style={{ width: 125, height: 80.08, borderRadius: 6 }} 
                      resizeMode="cover"
                      progressiveRenderingEnabled={true} />
                  </View>
                  <View style={{ position: 'absolute', bottom: 5, left: 5, width: 125, height: 20, backgroundColor: 'rgba(0,0,0,0.3)', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 10 }}>{Lang.imageSim[this.props.lang]}</Text>
                  </View>
                </TouchableOpacity> : 
                <TouchableOpacity onPress={ () => {
                  Alert.alert(
                      'Info',
                      'Dari mana anda akan mengambil gambar?',
                      [
                        {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                        {
                          text: 'Galeri',
                          onPress: () => Actions.push("Galeri", { route: "IdentificationInformation", type: "sim" }),
                          style: 'cancel',
                        },
                        {text: 'Kamera', onPress: () => this.props.openCameraForSim(true) },
                      ],
                      {cancelable: false},
                    );                
                  }} style={{ marginTop: 20, marginRight: 20 }}>
                  <View style={styles.scanSquare}>
                    <SvgIcon name="icoAddLicense" style={styles.scanIcon} />
                    <Text style={styles.scanWording}>{this.props.login.Data.ImageSIM !== null || this.props.captureSim !== null ? Lang.update[this.props.lang] : Lang.add[this.props.lang] } {Lang.imageSim[this.props.lang]}</Text>
                    <View style={styles.photoFrame}>
                      <Mask shape="rounded"></Mask>
                    </View>
                  </View>
                </TouchableOpacity>
                }
              </View>

              {this._renderNumberInput(Lang.ktpNumber[this.props.lang], this.state.NoKTP, "pencil", "NoKTP", 16)}
              {this._renderNumberInput(Lang.licenseNumber[this.props.lang], this.state.NoSIM, "pencil", "NoSIM", 12)}
              {this._renderTextInput(Lang.address[this.props.lang], this.state.Address, "pencil", "Address")}

              { this.props.openCameraSim && 
              <MCameraSim 
                visible={this.props.openCameraSim}
                onCapture={() => console.log('on capture')}
                onClose={() => this.props.openCameraForSim(false) }
              /> }

              { this.props.openCameraKtp && 
              <MCameraKtp 
                visible={this.props.openCameraKtp}
                onCapture={() => console.log('on capture')}
                onClose={() => this.props.openCameraForKtp(false) }
              /> }
              </KeyboardAvoidingView>
          </ScrollView>
        </View>
        <View style={{ width: '100%' }}>
          <MainButton 
            isLoading={this.props.loading}
            label={Lang.sendButton[this.props.lang]}
            onPress={ () => !this.props.loading ? this.onSubmit() : {} }
          />
        </View>

      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ authentication, camera, userProfile, language }) => {
  const { loading, user, updateUser } = userProfile;
  const { openCameraSim, captureSim, openCameraKtp, captureKtp } = camera;
  const { login } = authentication;
  const { lang } = language;
  return { login, openCameraSim, captureSim, loading, user, updateUser, openCameraKtp, captureKtp, lang }
}

export default connect(mapStateToProps, {
  openCameraForSim,
  updateUserProfile,
  getUserProfile,
  setNewUser,
  clearCaptureForAll,
  openCameraForKtp,
  updateUserProfileSuccess
})(IdentificationInformation);
