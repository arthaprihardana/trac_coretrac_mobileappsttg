import {StyleSheet} from 'react-native';
import { appVariables } from '@variables';

const styles = StyleSheet.create({
  title:{
    color: appVariables.colorGray,
    flex: 1,
    lineHeight: 20,
    paddingVertical:20,
    paddingHorizontal:20,
    fontSize:14,
  },
  container:{
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 20
  },
  textInputWrapper:{
    flex: -1,
    flexDirection: "row",
    paddingTop: 10,
    height: 40,
    borderBottomWidth: 1,
    borderColor: "#aaaaaa",
    marginBottom: 5,
  },
  citizenWrapper:{
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  scanSquare: { 
    flex: 1, 
    alignSelf: 'stretch', 
    borderWidth: 1, 
    marginRight: 0, 
    justifyContent: 'center', 
    alignItems: 'center', 
    borderColor: appVariables.colorGray, 
    borderStyle: 'dashed', 
    borderRadius: 10, 
    padding: 20, 
    width: 135, 
    height: 90.08 
  },
  scanIcon: { 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  photoFrame: {
    position:'absolute',
    top: 0, 
    left: 0,
    justifyContent: 'center',
    alignItems:'center'
  },
  scanWording: {
    marginTop:20,
    color:appVariables.colorGray, 
    textAlign:'center',
    fontSize:12,
  }
})

export default styles;