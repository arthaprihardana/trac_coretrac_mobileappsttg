import { Body, Button, Header, Left, Title } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { appVariables } from '../../../../../assets/styles';
import SvgIcon from '../../../../../utils/SvgIcon';

class ThisHeader extends Component {
    render() {
        let { backAction, title } = this.props;
        let { left, body } = styles;
        return (
            <Header>
                <Left style={left}>
                    <Button transparent>
                        <TouchableOpacity onPress={backAction}>
                            <SvgIcon name="backArrow" />
                        </TouchableOpacity>
                    </Button>
                </Left>
                <Body style={body}>
                    <Title>
                        <Text style={{fontFamily: appVariables.museo500}}>{title}</Text>
                    </Title>
                </Body>
            </Header>
        )
    }
}

const styles = StyleSheet.create({
    left: {
        flex: 1
    },
    body: {
        flex: 7
    }
})

ThisHeader.propTypes = {
    backAction: PropTypes.func,
    title: PropTypes.string
}

export default ThisHeader;