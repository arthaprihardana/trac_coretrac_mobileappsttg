import { Input, Item, Label } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { appVariables } from '../../../../../assets/styles';
import { OModalAction } from '../../../../../components/organisms';

class ModalNotifyMe extends Component {
    render() {
        let { action } = this.props;
        let { formWrapper, labelForm, inputForm } = styles;
        return (
            <OModalAction action={action}>
                <Item floatingLabel style={formWrapper}>
                    <Label style={labelForm}>Email Address</Label>
                    <Input style={inputForm} />
                </Item>
            </OModalAction>
        )
    }
}

const styles = StyleSheet.create({
    formWrapper: { marginBottom: 60 },
    labelForm: { top: 10, fontSize: 12, fontFamily: appVariables.museo500, color: '#8A959E' },
    inputForm: { paddingLeft: 0, paddingBottom: 0, fontSize: 16, fontFamily: appVariables.museo700, color: '#2A2E36' },

})

ModalNotifyMe.propTypes = {
    action: PropTypes.func
}

export default ModalNotifyMe;