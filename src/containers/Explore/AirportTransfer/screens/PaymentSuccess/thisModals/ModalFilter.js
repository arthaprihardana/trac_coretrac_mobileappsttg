import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Container, Content, Footer } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../../assets/styles';
import { AButton, ASliderMarker } from '../../../../../components/atoms';
import { OFilterCarSelect, ORadioCircle } from '../../../../../components/organisms';
import SvgIcon from '../../../../../utils/SvgIcon';
import MBlueRadio from '../../../../../components/molecules/MBlueRadio';

const SectionHeader = ({ label, actionClear }) => {
    return (
        <View style={styles.sectionHeaderStyle}>
            <Text style={styles.titleSectionHeaderStyle}>{label.toUpperCase()}</Text>
            <TouchableOpacity onPress={actionClear}>
                <Text style={styles.clearSetionHeaderStyle}>clear</Text>
            </TouchableOpacity>
        </View>
    )
}

const PriceCompareItem = ({label, value}) => {
    return (
        <View>
            <Text style={styles.labelPriceCompare}>{label}</Text>
            <Text style={styles.valuePriceCompare}>RP {value}</Text>
        </View>
    )
}

class ModalFilter extends Component {
    state = {
        minPrice: 200000,
        maxPrice: 5000000,
        minPriceShow: 200000,
        maxPriceShow: 5000000,
        carType: [
          {
            id: 1,
            name: "MVP"
          },
          {
            id: 2,
            name: "City Car"
          },
          {
            id: 3,
            name: "Luxury Car"
          }
        ],
        selectedCarType: null,
        brandCars: [
            {
                labelBrand: 'Toyota',
                cars: [
                    {
                        id: 1,
                        name: "Toyota Avanza",
                        selected: false
                    },
                    {
                        id: 2,
                        name: "Toyota Innova",
                        selected: false
                    },
                    {
                        id: 3,
                        name: "Toyota Alphard",
                        selected: false
                    },
                    {
                        id: 4,
                        name: "Toyota Agya",
                        selected: false
                    },
                    {
                        id: 5,
                        name: "Toyota Ayla",
                        selected: false
                    },
                    {
                        id: 6,
                        name: "Toyota Yaris",
                        selected: false
                    },
                    {
                        id: 7,
                        name: "Toyota Fortuner",
                        selected: false
                    }
                ]
            },
            {
                labelBrand: 'BMW',
                cars: [
                    {
                        id: 1,
                        name: "BMW CS 5",
                        selected: false
                    },
                    {
                        id: 2,
                        name: "BMW CS 5 (2)",
                        selected: false
                    },
                    {
                        id: 3,
                        name: "BMW CS 5 (3)",
                        selected: false
                    }
                ]
            }
        ]
    }
    onSliderChange = (value) => {
        this.setState({
            minPriceShow: value[0] * this.state.minPrice,
            maxPriceShow: value[1] * this.state.maxPrice / 10
        })
    }
    render() {
        let { onClose } = this.props;
        let { minPriceShow, maxPriceShow } = this.state;
        let { containerStyle, headerWrapper, contentStyle, sectionWrapper, priceWrapper, footerWrapper } = styles;

        const carType = this.state.carType.map((item) => {
          return <MBlueRadio touchText label={item.name} active={this.state.selectedCarType == item.name} key={item.id} onPress={() => this.setState({selectedCarType: item.name})}  />;
        });

        return (
            <Container style={containerStyle}>
                <View style={headerWrapper}>
                    <TouchableOpacity onPress={onClose}>
                        <SvgIcon name="icoCloseDark" />
                    </TouchableOpacity>
                </View>
                <Content>
                    <View style={contentStyle}>
                        <View style={sectionWrapper}>
                            <SectionHeader label="price" actionClear={() => alert('action clear')} />
                            <View style={{ paddingLeft: 10, justifyContent: 'center', alignItems: 'center', }}>
                                <MultiSlider isMarkersSeparated={true} min={1} max={10} values={[1, 10]} sliderLength={300} selectedStyle={{ backgroundColor: '#1F419B', }} unselectedStyle={{ backgroundColor: '#D6DAE2', }} trackStyle={{ height: 4, borderRadius: 100 }} customMarkerLeft={() => <ASliderMarker />} customMarkerRight={() => <ASliderMarker />} onValuesChange={(val) => this.onSliderChange(val)} />
                            </View>
                            <View style={priceWrapper}>
                                <PriceCompareItem label="min" value={minPriceShow} />
                                <View style={{ width: 1, backgroundColor: '#2A2E36', marginHorizontal: 21 }}></View>
                                <PriceCompareItem label="max" value={maxPriceShow} />
                            </View>
                        </View>
                        {/* <View style={sectionWrapper}>
                            <SectionHeader label="passanger" actionClear={() => alert('action clear')} />
                            <ORadioCircle maxNumb={8} defaultId={3} onChange={(val)=> console.log(val)} />
                        </View>
                        <View style={sectionWrapper}>
                            <SectionHeader label="suitecase" actionClear={() => alert('action clear')} />
                            <ORadioCircle maxNumb={4} defaultId={3} onChange={(val)=> console.log(val)} />
                        </View> */}
                        <View style={sectionWrapper}>
                            <SectionHeader label="car type" actionClear={() => alert('action clear')} />
                            {/* <OFilterCarSelect listBrandCars={this.state.brandCars} /> */}
                            { carType }
                        </View>
                    </View>
                </Content>
                <Footer style={footerWrapper}>
                    <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', paddingVertical: 12 }}>
                        <View style={{ flex: 1, marginHorizontal: 10, marginLeft: 20 }}>
                            <AButton bordered info block onPress={() => { }}>RESET</AButton>
                        </View>
                        <View style={{ flex: 1, marginHorizontal: 10, marginRight: 20 }}>
                            <AButton info block onPress={onClose}>APPLY</AButton>
                        </View>
                    </View>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: { backgroundColor: '#F3F4F5' },
    headerWrapper: { width: '100%', justifyContent: 'flex-end', flexDirection: 'row', padding: 20 },
    contentStyle: { paddingBottom: 40 },
    sectionWrapper: { borderBottomColor: '#2A2E36', borderBottomWidth: 0.31, paddingBottom: 30, paddingHorizontal: 20, marginTop: 20 },
    sectionHeaderStyle: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 20 },
    titleSectionHeaderStyle: { fontSize: 12, fontFamily: appVariables.museo900, color: '#2A2E36' },
    clearSetionHeaderStyle: { fontSize: 12, fontFamily: appVariables.museo500, color: '#2246A8' },
    priceWrapper: { flexDirection: 'row', justifyContent: 'center', marginTop: 20 },
    labelPriceCompare: { textAlign: 'center', color: '#F27C21', fontFamily: appVariables.museo900, fontSize: 12 },
    valuePriceCompare: { textAlign: 'center', color: '#2A2E36', fontFamily: appVariables.museo900, fontSize: 12, marginTop: 8 },
    footerWrapper: { height: 'auto', shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 10}
})

ModalFilter.propTYpes = {
    onClose: PropTypes.func
}

export default withNavigation(ModalFilter);