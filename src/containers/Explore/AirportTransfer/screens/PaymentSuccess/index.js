/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 14:07:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 15:17:19
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Clipboard,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Text from "@airport-transfer-fragments/Text";
import Header from "@airport-transfer-fragments/Header";
import MainButton from "@airport-transfer-fragments/MainButton";

import styles from "./style";

import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import { Actions } from "react-native-router-flux";
import Lang from "../../../../../assets/lang";

class PaymentSuccess extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props){
    super(props);
    this.shareCodeRef = React.createRef();
    this.state = {
      text:'SHARE205',
      shareCodeContent:null,
    };
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------
  getShareCode = async() => {
    //copy the text from shareCode
    const currentShareCode = this.shareCodeRef.current;
    await Clipboard.setString(this.state.text);
    alert('Share code copied : '+this.state.text);
  };
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer backgroundColor={appVariables.colorBlueHeader}>

        <Header
          title={Lang.payment[this.props.lang]}
        />

        <ScrollView 
          style={{
            flex: 1,
            backgroundColor: appVariables.colorWhite,
            marginBottom: 60
          }}
          showsVerticalScrollIndicator={false}>

          <View style={{
            flex: -1,
            height: 200,
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 20,
          }}>

            <Image style={{
              flex: 1,
            }}
              source={require("@images/png/success.png")}>

            </Image>

          </View>

          <View style={{
            flex: 1,
            padding: 20,
          }}>
            <View style={{
              flex: -1,
              height: 100,
              justifyContent: "space-between",
            }}>
              <Text size={25}>
                {Lang.Congratulations[this.props.lang]} {this.props.login.Data.FirstName}!
                {/* Congratulations {this.props.reservation.PICName}! */}
              </Text>
              <Text size={25}>
                {Lang.youHaveSuccessfullyRent[this.props.lang]}
              </Text>
            </View>

            <View style={{
              flex: -1,
              flexDirection: "row",
            }}>
              <Text color={"#888888"} style={{
                paddingTop: 20,
              }}>
                {Lang.yourReservationNumber[this.props.lang]}
              </Text>
              <Text style={{
                paddingTop: 20,
                paddingLeft: 6,
              }}>
                {this.props.reservation.ReservationId}
              </Text>
            </View>

            <Text color={"#888888"} style={{
              paddingTop: 20,
            }}>
              {Lang.detailOfYourBookingSent[this.props.lang]}
            </Text>

            <Text size={16} style={{
              paddingTop: 20,
            }}>
              {this.props.login.Data.EmailPersonal}
            </Text>

            <Text color={"#888888"} style={{
              paddingTop: 20,
            }}>
              {Lang.pleaseVerifyYourEmail[this.props.lang]}
            </Text>

            {/* <Text size={14} style={{
              paddingTop: 20,
            }}>
              Referral Code :
            </Text>

            <Text color={"#888888"} style={{
              paddingTop: 20,
            }}>
              Share this code to your friend. When they use is, you’ll get 25% off for your next purchase. 
            </Text>

            <View style={styles.shareBox}>
              <Text 
                ref={this.shareCodeRef}
                style={{ fontSize: 20 }}>SHARE205</Text>
            </View>

            <TouchableOpacity
              onPress={this.getShareCode}>
            <Text color={"#888888"} style={{
              paddingTop: 20,
            }}>
              Copy code
            </Text>
            </TouchableOpacity> */}
          </View>

        </ScrollView>

        <View style={{position: 'absolute', width: '100%', bottom: 0}}>
          <MainButton 
            label={Lang.continue[this.props.lang]}
            onPress={ () => {
              // Actions.reset("Home") 
              Actions.reset("MyBooking") 
            }}
          />
        </View>

      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ reservationSave, authentication, language }) => {
  const {reservation} = reservationSave;
  const { login } = authentication;
  const { lang } = language;
  return {reservation, login, lang}
}

export default connect(mapStateToProps, {})(PaymentSuccess);