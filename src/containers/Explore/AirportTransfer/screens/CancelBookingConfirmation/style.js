import { appVariables,appMetrics } from "@variables";

export default {
  container:{
    flex: 1,
    paddingVertical: appMetrics.windowHeight/4,
    paddingHorizontal: 30,
    backgroundColor: appVariables.colorWhite,
  },
  imageWrapper:{
    flex: 1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "center",
  },
  textWrapper:{
    flex: -1,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
  },
  buttonWrapper:{
    flex: -1,
    flexDirection: 'row',
    paddingHorizontal:20,
    alignItems: "center",
    justifyContent: "center",
  },
}