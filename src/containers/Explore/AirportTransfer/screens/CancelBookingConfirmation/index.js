/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 22:23:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-31 23:15:55
 */
import React, { Component } from "react";
import {
  View,
  Image,
} from "react-native";
import {
  appVariables,
} from "@variables";
import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";

export default class CancelBookingConfirmation extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderText() {
    return (
      <View style={styles.textWrapper}>
        <Text
          size={20}
          color={appVariables.colorBlack}>
          Your ride have been cancelled
        </Text>
        <Text
          style={{ paddingTop: 10 }}
          size={14}
          color={appVariables.colorGray}>
          Thank you for making reservation with us.
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderImage() {
    return (
      <View style={styles.imageWrapper}>
        <Image
          source={require("@images/png/cancelled-booking-popup.png")}>
        </Image>
      </View>
    );
  }


  // ---------------------------------------------------
  _renderButton() {
    return (
      <View style={styles.buttonWrapper}>
        <MainButton
          onPress={() => Actions.reset('Home')}
          label={"DONE"}
          style={{ height: 50, width: 300, alignItems: "center", justifyContent: "center", }}
          rounded />
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------
  render() {
    return (
      <View style={styles.container}>
        {this._renderImage()}
        {this._renderText()}
        {this._renderButton()}
      </View>
    );
  }
}