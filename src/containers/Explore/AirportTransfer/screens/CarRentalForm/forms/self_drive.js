/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 22:21:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 20:34:41
 */
import moment from "moment";
import "moment/locale/id";
import Lang from '../../../../../../assets/lang';

export default (lang) => { 
  return [
    {
      type: "type", 
      label: Lang.typeOfService[lang],
      modalTitle: Lang.typeOfService[lang],
      placeholder: Lang.selectType[lang], 
      backgroundColor: "#2246A8",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value ? null : formsState.type.options.filter((option) => option.value == value)[0].label,
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null, 
    },
    {
      type: "delivery_method", 
      label: Lang.typeOfPickup[lang], 
      modalTitle: Lang.howTheCarDeliver[lang], 
      placeholder: Lang.typeOfPickup[lang], 
      backgroundColor: "#1F419B",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value || !formsState.pickup_method.value ? null : formsState.delivery_method.options.filter((option) => option.value == value)[0].label + "\n" + formsState.pickup_method.options.filter((option) => option.value == formsState.pickup_method.value)[0].label,
      optionsModifier: null,
      onSuccessChain: "pickup_method", 
      onCancelChain: null, 
    },
    {
      type: "pickup_method", 
      label: Lang.pickupAtpool[lang], 
      modalTitle: Lang.howTheCarReturn[lang],
      placeholder: null, 
      backgroundColor: null,
      hasSearchIcon: false,
      isHidden: true,
      isRequired: true,
      isDisabled: false,
      modifier: null,
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: "delivery_method", 
    },
    {
      type: "from_city", 
      label: Lang.selectYourCity[lang], 
      modalTitle: Lang.selectCity[lang], 
      placeholder: Lang.selectYourCity[lang], 
      backgroundColor: "#1A3681",
      hasSearchIcon: true,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value || !formsState.from_location.value ? null : formsState.from_location.options.filter((option) => option.value == formsState.from_location.value)[0].label,
      optionsModifier: null,
      onSuccessChain: "from_location", 
      onCancelChain: null, 
    },
    {
      type: "from_location", 
      label: null, 
      modalTitle: Lang.findYourAddress[lang], 
      placeholder: null, 
      backgroundColor: null,
      hasSearchIcon: false,
      isHidden: true,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => {
        console.log('value ==>', value);
        console.log('formsState ==>', formsState);
      },
      optionsModifier: null,
      // optionsModifier: (options, formsState) => options.filter((option) => option.parent == formsState.from_city.value),
      onSuccessChain: null, 
      onCancelChain: "from_city", 
    },
    {
      type: "package", 
      label: Lang.rentalPackage[lang], 
      modalTitle: Lang.rentalPackage[lang], 
      placeholder: Lang.rentalPackage[lang], 
      backgroundColor: "#1A3681",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value ? null : formsState.package.options.filter((option) => option.value == value)[0].label,
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null, 
    },
    {
      type: "pickup_schedule", 
      label: Lang.selectDateTime[lang], 
      modalTitle: Lang.selectDateTime[lang], 
      placeholder: Lang.selectDateTime[lang], 
      backgroundColor: "#12265B",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => {
        if(formsState.package.value <= 12) {
          return (!value.date || !value.time) ? null :
              (new moment(value.date + " " + value.time, "DD-MM-YYYY hh:mm")).format("LLL") + "\n" + 
              (new moment(value.date + " " + value.time, "DD-MM-YYYY hh:mm")).add(formsState.package.value, 'hour').format("LLL")
        } else {
          return (!value.startDate || !value.startTime) ? null :
              (new moment(value.startDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).format("LLL") + "\n" +
              (new moment(value.endDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).add(formsState.package.value, 'hour').format("LLL")
        }
      },
      // modifier: (value, formsState) => (!value.startDate || !value.startTime || !value.endDate || !value.endTime) ? null : (new moment(value.startDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).format("ddd, DD MMM YYYY [at] hh:mm A") + "\n" + (new moment(value.endDate + " " + value.endTime, "DD-MM-YYYY hh:mm")).format("ddd, DD MMM YYYY [at] hh:mm A"),
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null, 
    },
  ];
}