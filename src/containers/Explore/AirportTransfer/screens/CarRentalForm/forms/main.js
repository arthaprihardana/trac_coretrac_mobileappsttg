/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 21:56:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 20:29:13
 */
import moment from "moment";
import "moment/locale/id";
import Lang from '../../../../../../assets/lang'

export default (lang) => {
  return [
    {
      type: "type", 
      label: Lang.typeOfService[lang],
      modalTitle: Lang.typeOfService[lang],
      placeholder: Lang.selectType[lang], 
      backgroundColor: "#2246A8",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value ? null : formsState.type.options.filter((option) => option.value == value)[0].label,
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null, 
    },
    {
      type: "from_city", 
      label: Lang.selectYourCity[lang], 
      modalTitle: Lang.selectCity[lang], 
      placeholder: Lang.selectYourCity[lang], 
      backgroundColor: "#1F419B",
      hasSearchIcon: true,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value || !formsState.from_location.value ? null : formsState.from_location.options.filter((option) => option.value == formsState.from_location.value)[0].label,
      optionsModifier: null,
      onSuccessChain: "from_location", 
      onCancelChain: null, 
    },
    {
      type: "from_location", 
      label: null, 
      modalTitle: Lang.findYourAddress[lang], 
      placeholder: null, 
      backgroundColor: null,
      hasSearchIcon: false,
      isHidden: true,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => {
        console.log('value ==>', value);
        console.log('formsState ==>', formsState);
      },
      optionsModifier: null,
      // optionsModifier: (options, formsState) => options.filter((option) => option.parent == formsState.from_city.value),
      onSuccessChain: null, 
      onCancelChain: "from_city", 
    },
    {
      type: "package", 
      label: Lang.rentalPackage[lang], 
      modalTitle: Lang.rentalPackage[lang], 
      placeholder: Lang.rentalPackage[lang], 
      backgroundColor: "#1A3681",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value ? null : formsState.package.options.filter((option) => option.value == value)[0].label,
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null, 
    },
    {
      type: "pickup_schedule", 
      label: Lang.selectDateTime[lang], 
      modalTitle: Lang.selectDateTime[lang], 
      placeholder: Lang.selectDateTime[lang], 
      backgroundColor: "#12265B",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => {
        if(formsState.package.value <= 12) {
          return (!value.date || !value.time) ? null :
              (new moment(value.date + " " + value.time, "DD-MM-YYYY hh:mm")).format("LLL") + "\n" + 
              (new moment(value.date + " " + value.time, "DD-MM-YYYY hh:mm")).add(formsState.package.value, 'hour').format("LLL")
        } else {
          return (!value.startDate || !value.startTime) ? null :
              (new moment(value.startDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).format("LLL") + "\n" +
              (new moment(value.endDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).add(formsState.package.value, 'hour').format("LLL")
        }
      },
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null, 
    },
  ];
}