/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 12:53:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 23:16:13
 */
import React, { Component } from "react";
import { Alert } from "react-native"
import { connect } from "react-redux";
import moment from "moment";
import _ from "lodash";

import mainForm from "./forms/main";
import selfDriveForm from "./forms/self_drive";

import MainFormContainer from "@airport-transfer-fragments/MainFormContainer";

import { getRentalPackage, getMasterCarType, getExtras, setRequestStockList, setTempForm } from "../../../../../actions";
import { RENTAL_TIMEBASE } from "../../../../../constant";

import "moment/locale/id";
import { Actions } from "react-native-router-flux";
import Lang from '../../../../../assets/lang'

const INITIAL_FORMS_STATE = {
  type: {
    value: null,
    isError: false,
    modalType: "radio",
    isModalOpened: false,
    options: [],
  },
  delivery_method: {
    value: null,
    isError: false,
    modalType: "radio",
    isModalOpened: false,
    options: [
      {
        label: "Pick up at Pool",
        value: "1",
      },
    ],
  },
  pickup_method: {
    value: null,
    isError: false,
    modalType: "radio",
    isModalOpened: false,
    options: [
      {
        label: "Deliver to Pool",
        value: "1",
      },
    ],
  },
  from_city: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  from_location: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  package: {
    value: null,
    isError: false,
    modalType: "radio",
    isModalOpened: false,
    options: [],
  },
  pickup_schedule: {
    value: {
      date: null,
      time: null,
      startDate: null,
      startTime: null,
      endDate: null,
      endTime: null,
    },
    // value: null,
    isError: false,
    modalType: "singleSchedule",
    isModalOpened: false,
    options: [],
  },
};


class CarRentalForm extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
    let serviceType = _.map(product[0].product_service, v => {
      return {
        label: v.ProductServiceName,
        value: v.ProductServiceId
      }
    });
  
    this.state = {
      BusinessUnitId: product[0].BusinessUnitId,
      forms: {
        type: {
          value: null,
          isError: false,
          modalType: "radio",
          isModalOpened: false,
          options: serviceType
        },
        delivery_method: {
          value: null,
          isError: false,
          modalType: "radio",
          isModalOpened: false,
          options: [
            {
              label: "Pick up at Pool",
              value: "1",
            },
          ],
        },
        pickup_method: {
          value: null,
          isError: false,
          modalType: "radio",
          isModalOpened: false,
          options: [
            {
              label: "Deliver to Pool",
              value: "1",
            },
          ],
        },
        from_city: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        from_location: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        package: {
          value: null,
          isError: false,
          modalType: "radio",
          isModalOpened: false,
          options: [],
        },
        pickup_schedule: {
          value: {
            date: null,
            time: null,
            startDate: null,
            startTime: null,
            endDate: null,
            endTime: null,
          },
          // value: null,
          isError: false,
          modalType: "singleSchedule",
          isModalOpened: false,
          options: [],
        },
      },

      missingForms: [],
    };
  }

// ---------------------------------------------------

  componentDidMount() {
    this.getOptions();
    this.props.getRentalPackage();
  }

// ---------------------------------------------------

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.cityCoverage !== prevProps.cityCoverage) {
      return { cityCoverage: this.props.cityCoverage }
    }
    if(this.props.address !== prevProps.address) {
      return { address: this.props.address }
    }
    if(this.props.rentalDuration !== prevProps.rentalDuration) {
      return { rentalDuration: this.props.rentalDuration }
    }
    if(this.props.placeDetail !== prevProps.placeDetail) {
      return { coords: this.props.placeDetail }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.cityCoverage !== undefined) {
        this.manageCityCoverage(snapshot.cityCoverage)
      }
      if(snapshot.address !== undefined) {
        this.manageAddress(snapshot.address)
      }
      if(snapshot.rentalDuration !== undefined) {
        this.manageRentalDuration(snapshot.rentalDuration)
      }
      if(snapshot.coords !== undefined) {
        const forms = this.state.forms;
        const fromLocation = forms.from_location;
        fromLocation.coords = snapshot.coords;
        this.setState({
          forms
        })
      }
    }
  }

  manageCityCoverage = cities => {
    const forms = this.state.forms;
    const fromCity = forms.from_city;
    fromCity.options = _.map(cities, (v, k) => {
      return {
        label: _.startCase(_.lowerCase(v.MsCityName)),
        value: _.startCase(_.lowerCase(v.MsCityName)),
        BranchId: v.BranchId,
        CityId: v.CityId
      }
    });
    this.setState({
      forms
    });
  }

  manageAddress = address => {
    const forms = this.state.forms;
    const fromLocation = forms.from_location;
    fromLocation.options = address;
    this.setState({
      forms
    })
  }

  manageRentalDuration = packages => {
    const forms = this.state.forms;
    const pkg = forms.package;
    pkg.options = _.map(packages, v => {
      return {
        label: v.Name,
        value: v.Duration,
      }
    });
    this.setState({ forms });
  }

  // componentDidUpdate = (prevProps, prevState) => {
  //   if (!this.state.isInitiated) {
  //     this.getOptions();
  //   }
  // }
  

// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onFormsStateChange(forms) {
    if(forms.type.isModalOpened) {
      if(forms.package.value !== null) {
        forms.package.value = null;
        forms.pickup_schedule.value = {
          date: null,
          time: null,
          startDate: null,
          startTime: null,
          endDate: null,
          endTime: null
        }
      }
    }
    if(forms.type.value === "PSV0001") {
      let x = _.filter(this.props.rentalDuration, { Duration: "24" });
      this.manageRentalDuration(x);
    } else {
      let x = _.filter(this.props.rentalDuration, v => v.Duration !== "24" );
      this.manageRentalDuration(x);
    }
    if(forms.package.value !== null) {
      if(forms.package.value <= 12) {
        forms.pickup_schedule.modalType = "singleSchedule";
      } else {
        forms.pickup_schedule.modalType = "rangeSchedule";
      }
    }
    if(forms.package.isModalOpened) {
      if( (forms.pickup_schedule.value.date && forms.pickup_schedule.value.date !== null) || (forms.pickup_schedule.value.startDate && forms.pickup_schedule.value.startDate !== null) ) {
        forms.pickup_schedule.value = {
          date: null,
          time: null,
          startDate: null,
          startTime: null,
          endDate: null,
          endTime: null
        }
      }
    }
    
    this.setState({
        forms,
        missingForms: []
    })
  }

// ---------------------------------------------------

  onChainingModalTriggered(key, chainKey, isForwardChain = false) {
    const forms = this.state.forms;
    
    forms[chainKey].isModalOpened = true;

    if (isForwardChain) {
      forms[chainKey].value = INITIAL_FORMS_STATE[chainKey].value;
    }

    this.setState({
      forms
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN METHODS
// ---------------------------------------------------

  getOptions() {
    const forms = this.state.forms;
    
    this.setState({
      forms,
      isInitiated: true,
    });
  }

// ---------------------------------------------------

  getForm() {
    const forms = this.state.forms;
    let formConfig = mainForm(this.props.lang);

    if (forms.type.value == "PSV0001") {
      formConfig = selfDriveForm(this.props.lang);
      // console.log('forms.package.value ==>', forms.package.value);
      // if(forms.package.value !== null) forms.package.value = null
    }

    return formConfig;
  }

// ---------------------------------------------------

  getInvalidForms(formTemplate, formData) {
    const missingForms = [];
    formTemplate.map((form) => {
      if (form.isRequired) {
        if (!formData[form.type].value) {
          missingForms.push(form.type);
        } else if (typeof formData[form.type].value == "object") {
          for (x in formData[form.type].value) {
            if (!formData[form.type].value[x]) {
              missingForms.push(form.type);

              break;
            }
          }
        }
      }
    });

    return missingForms;
  }

// ---------------------------------------------------

  submit() {
    let forms = [];
    let stacks = this.state.formStacks;
    let data = {};
    let missingForms = [];
    let navTripsData = [];

    data.trips = {};
    data.trips.trip1 = {};
    forms = this.getForm();
    forms.map((form, index) => {
      if (form.type != "type") {
        data.trips.trip1[form.type] = this.state.forms[form.type].value;
      }
    });

    missingForms = this.getInvalidForms(forms, this.state.forms);

    if(this.state.forms.package.value !== null && this.state.forms.package.value <= 12) {
      navTripsData = [{
        from: this.state.forms.from_location.value,
        fromLabel: _.filter(this.state.forms.from_location.options, { value: this.state.forms.from_location.value })[0].label,
        to: "",
        start: this.state.forms.pickup_schedule.value.date
      }];
    } else if(this.state.forms.package.value !== null && this.state.forms.package.value >= 12) {
      navTripsData = [{
        from: this.state.forms.from_location.value,
        fromLabel: _.filter(this.state.forms.from_location.options, { value: this.state.forms.from_location.value })[0].label,
        to: "",
        start: this.state.forms.pickup_schedule.value.startDate,
        end: this.state.forms.pickup_schedule.value.endDate
      }];
    } else {
      missingForms = this.getInvalidForms(this.getForm(), this.state.forms);
    }
    
    // switch (this.state.forms.package.value) {
    //   case 4:
    //     navTripsData = [
    //       {
    //         from: this.state.forms.from_location.value,
    //         fromLabel: _.filter(this.state.forms.from_location.options, { value: this.state.forms.from_location.value })[0].label,
    //         to: "",
    //         start: this.state.forms.pickup_schedule.value.date
    //       },
    //     ];
    //     break;
    //   case 12:
    //     navTripsData = [
    //       {
    //         from: this.state.forms.from_location.value,
    //         fromLabel: _.filter(this.state.forms.from_location.options, { value: this.state.forms.from_location.value })[0].label,
    //         to: "",
    //         start: this.state.forms.pickup_schedule.value.startDate,
    //         end: this.state.forms.pickup_schedule.value.endDate
    //       },
    //     ];
    //     break;
    //   default:
    //     missingForms = this.getInvalidForms(this.getForm(), this.state.forms);
    //     break;
    // }

    if (missingForms.length > 0) {
      this.setState({
        missingForms,
      });

      return;
    }

    let title = _.filter(this.state.forms.type.options, { value: this.state.forms.type.value });
    let branch = _.filter(this.state.forms.from_city.options, { value: this.state.forms.from_city.value });
    this.props.setTempForm(this.state.forms);
    this.props.getMasterCarType(this.state.BusinessUnitId);
    this.props.getExtras({
      BusinessUnitId: this.state.BusinessUnitId, 
      BranchId: branch[0].BranchId
    });
    let startDate = this.state.forms.package.value <= 12 ? 
      (new moment(this.state.forms.pickup_schedule.value.date + " " + this.state.forms.pickup_schedule.value.time, "DD-MM-YYYY hh:mm")).toISOString() : 
      (new moment(this.state.forms.pickup_schedule.value.startDate + " " + this.state.forms.pickup_schedule.value.startTime, "DD-MM-YYYY hh:mm")).toISOString();
    let endDate = this.state.forms.package.value <= 12 ? 
      (new moment(this.state.forms.pickup_schedule.value.date + " " + this.state.forms.pickup_schedule.value.time, "DD-MM-YYYY hh:mm")).add(this.state.forms.package.value, 'hour').toISOString() : 
      (new moment(this.state.forms.pickup_schedule.value.endDate + " " + this.state.forms.pickup_schedule.value.startTimetime, "DD-MM-YYYY hh:mm")).add(this.state.forms.package.value, 'hour').toISOString();
    let requestStock = {
      BusinessUnitId: this.state.BusinessUnitId,
      BranchId: branch[0].BranchId,
      StartDate: startDate,
      EndDate: endDate,
      IsWithDriver: /^[a-z|A-Z|0-9]+[^I]\s?(1){1}$/.test(this.state.forms.type.value) ? 0 : 1, // 0 = self driver 1 = pakai driver
      RentalDuration: this.state.forms.package.value,
      RentalPackage: this.state.forms.package.value,
      Uom: 'hr',
      ServiceTypeId: RENTAL_TIMEBASE,  // Rental Time Based
      ValidateAttribute: 1,
      ValidateContract: 1,
      ProductServiceId: this.state.forms.type.value,  // buat exception pada saat request
      CityId: branch[0].CityId  // buat exception pada saat request
    }
    this.props.setRequestStockList(requestStock);
    Actions.push("CarList", {
      navData: {
        submittedData: data,
        viewData: {
          type: data.type,
          title: `${Lang.carRental[this.props.lang]} - ${title[0].label}`,
          tripIndex: 0,
          addedList: [],
          trips: navTripsData.filter((item) => !!item),
        },
      },
    });

  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <MainFormContainer
        title={ Lang.carRental[this.props.lang] }
        onBackPressed={ () => this.props.navigation.pop() }
        forms={ this.getForm() }
        missingForms={ this.state.missingForms }
        formsState={ this.state.forms }
        onSubmitPressed={ this.state.stackType == "return" && this.state.formStacks.length === 1 ? null : () => this.submit() }
        onFormsStateChange={ (forms) => this.onFormsStateChange(forms) }
        onChainingModalTriggered={ (key, chainKey, isForwardChain) => this.onChainingModalTriggered(key, chainKey, isForwardChain) }
      />
    );
  }
};

const mapStateToProps = ({ productService, cities, addressFromGoogle, rentalPackage, authentication, language }) => {
  const { products, selectedProduct } = productService;
  const { cityCoverage } = cities;
  const { address, placeDetail } = addressFromGoogle;
  const { rentalDuration } = rentalPackage;
  const { isLogin, login } = authentication;
  const { lang } = language;
  return { products, selectedProduct, cityCoverage, address, rentalDuration, placeDetail, isLogin, login, lang };
}

export default connect(mapStateToProps, {
  getRentalPackage,
  getMasterCarType,
  getExtras,
  setRequestStockList,
  setTempForm
})(CarRentalForm)