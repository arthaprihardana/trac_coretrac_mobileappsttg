/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-12 13:26:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 17:22:46
 */
import { Container, Content } from "native-base";
import { Actions } from "react-native-router-flux";
import React, { Component } from "react";
import { connect } from 'react-redux';
import { 
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator
} from "react-native";
import _ from "lodash";
import {
  appVariables,
  appMetrics,
} from "@variables";
import firebase from 'react-native-firebase';

import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import MainButton from "@airport-transfer-fragments/MainButton";
import SocialMediaLogin from "@airport-transfer-fragments/SocialMediaLogin";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Lang from '../../../../../assets/lang';

import PromoDummies from "@airport-transfer-dummies/promos";
import NewsDummies from "@airport-transfer-dummies/news";
import Placeholder from 'rn-placeholder';

import { getProductService, setSelectedProductService, getMasterBranch, getAdjustmentTime, getRefreshToken, setNewToken, isSkipWelcomeScreen, postLogout, getContentArticle, setNotes, getPromo } from '../../../../../actions';

import styles from "./style";
import { greetings } from "../../../../../helpers";
import { BUS_RENTAL } from "../../../../../constant";

class Home extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      products: [],
      form: {
        email: "",
        password: "",
        shouldRemember: false,
      },
      datacontent: [],
      promoContent: []
    };
  }

  componentWillUnmount() {
    Actions.refresh();
  }
  
  componentDidMount = () => {
    this.props.getAdjustmentTime();
    this.props.getContentArticle();
    this.props.getPromo();
    this.props.setNotes(null);
    // if(this.props.isLogin && this.props.rememberMe) this.props.getRefreshToken(this.props.login.token);
    if(this.props.isLogin) {
      this.setTopic();
      this.props.getRefreshToken(this.props.login.token);
    }
    if(this.props.first) this.props.isSkipWelcomeScreen(true);
    if(this.props.products) {
      this.setState({
        products: this.props.products
      });
    } else {
      this.props.getProductService();
    }
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.products !== prevProps.products) {
      return { products: this.props.products }
    }
    if(this.props.refreshToken !== prevProps.refreshToken) {
      return { refreshToken: this.props.refreshToken }
    }
    if(this.props.articles !== prevProps.articles) {
      return { articles: this.props.articles }
    }
    if(this.props.promo !== prevProps.promo) {
      return { promo: this.props.promo }
    }
    return null; 
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.products !== undefined) {
        this.setState({
          products: snapshot.products
        }, () => this.forceUpdate())
      }
      if(snapshot.refreshToken !== undefined) {
        if(snapshot.refreshToken.token !== null) {
          // this.props.setNewToken({
          //   Data: this.props.login.Data,
          //   token: snapshot.refreshToken.token
          // })
        } else {
          firebase.messaging().unsubscribeFromTopic(this.props.login.Data.Id);
          this.props.postLogout();
          Actions.push("Login", { isExpired: true });
        }
      }
      if(snapshot.articles) {
        this.setState({
          datacontent: snapshot.articles
        })
      }
      if(snapshot.promo) {
        this.setState({
          promoContent: snapshot.promo
        })
      }
    }
  }

  setTopic = () => {
		if(this.props.isLogin) {
			firebase.messaging().subscribeToTopic(this.props.login.Data.Id);
		}
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderBackground() {
    return (
      <View style={ styles.background.container }>
        <View style={{
          flex: 1,
          position: "absolute",
          width:appMetrics.screenWidth,
        }}>
          <Image
            style={{
              flex:1,
              width:appMetrics.screenWidth-0.5,
            }}
            resizeMode={ "cover" }
            source={ require("@images/png/order-detail-header.png") }
          />
        </View>

        <View style={ styles.background.top.container }>
          <View style={ styles.background.top.logo.container }>
            <Image
              resizeMode={ "contain" }
              style={{ width: 100, height: 50 }}
              source={ require("@images/png/trac-logo-white.png") }
            />
          </View>
          { this.props.isLogin ? 
          <TouchableOpacity style={ styles.background.top.icon.container } onPress={() => Actions.push("Notification") }>
            <Icon name="bell" size={ 20 } color={ appVariables.colorWhite }/>
          </TouchableOpacity>
          : 
          <TouchableOpacity style={ styles.background.top.icon.container } onPress={() => Actions.push('Login') }>
            <Icon name="lock" size={ 20 } color={ appVariables.colorWhite }/>
          </TouchableOpacity>
          }
          
        </View>

        <View style={ styles.background.bottom.grouper }>
          <Text color={ appVariables.colorWhite } size={20} bold>{greetings(this.props.lang)} {this.props.isLogin ? this.props.login.Data.FirstName : Lang.guest[this.props.lang]}</Text>
          <Text color={ appVariables.colorWhite } size={12}>{Lang.dreamExploreDiscover[this.props.lang]}</Text>
          <Text color={ appVariables.colorWhite } size={12}>{_.join([ Lang.carRental[this.props.lang], Lang.airportTransfer[this.props.lang], Lang.busRental[this.props.lang] ], ', ')}</Text>
        </View>

      </View>
    );
  }

// ---------------------------------------------------

  _renderBookingItem(product, label, bu, icon, key) {
    return (
      <TouchableOpacity
        key={key}
        style={ styles.booking.container }
        onPress={ () => {
          this.props.setSelectedProductService(product);
          if(product === BUS_RENTAL) {
            // if(this.props.branch === null) {
            this.props.getMasterBranch(bu);
            // }
          }
          Actions.push(key) 
        }}
      >
        <View style={ styles.booking.icon.container }>
          <Image
            resizeMode={ "contain" }
            source={ icon }
          />
        </View>
        <Text>{ label }</Text>
      </TouchableOpacity>
    )
  }

// ---------------------------------------------------

  _renderBookings() {
    const { products } = this.state;
    let icon = '';
    return (
      <View style={ styles.booking.grouper }>
        { products.length > 0 && _.map(products, (v, k) => {
          switch (v.icon) {
            case "airport_transfer":
              icon = require("@images/png/explore/airport_transfer.png")
              break;
            case "bus_rental":
              icon = require("@images/png/explore/bus_rental.png")
              break;
            default:
              icon = require("@images/png/explore/car_rental.png");
              break;
          }
          return this._renderBookingItem(v.MsProductId, v.MsProductName, v.BusinessUnitId, icon, v.pushTo) 
        })
        }
        { this.props.loading && <ActivityIndicator size="small" color={appVariables.colorOrange} /> }
      </View>
    );
  }

// ---------------------------------------------------

  _renderPromoItem(item, index) {
    return (
      <TouchableOpacity key={ index } style={ styles.promo.main.container } onPress={() => Actions.push("PromoDetail", { item: item })}>
        <Image
          style={{ flex: 1, height: 170, width: 300, }}
          resizeMode="contain"
          source={{uri: item.ImageThumbnail }}
        />
      </TouchableOpacity>
    );
  }

// ---------------------------------------------------

  _renderContentHeader(title, onPress) {
    return (
      <View style={ styles.contentHeader.container }>
        <Text size={ 18 } bold>{ title }</Text>

        <TouchableOpacity style={ styles.contentHeader.seeAll.container } onPress={() => Actions.push(onPress) }>
          <Text>{Lang.seeAll[this.props.lang]} </Text>
          <Icon name="arrow-right" color={ appVariables.colorOrange }/>
        </TouchableOpacity>
      </View>
    );
  }

// ---------------------------------------------------

  _renderPromo() {
    return (
      <View style={ styles.promo.outerContainer }>
        { this._renderContentHeader(Lang.promo[this.props.lang], "Promo") }

        <ScrollView
          style={ styles.promo.main.grouper }
          contentContainerStyle={ styles.promo.main.innerGrouper }
          horizontal
          showsHorizontalScrollIndicator={ false }>
          { this.state.promoContent.length > 0 ?
            this.state.promoContent.map((promo, index) => {
              return this._renderPromoItem(promo, index)
            }) : [1,2].map((promo, index) => (
              <View key={ index } style={ styles.promo.main.container }>
                <View style={{ flex: 1, height: 170, width: 300, backgroundColor: '#ccc' }} />
              </View>
            ))
          }
        </ScrollView>
      </View>
    );
  }

// ---------------------------------------------------

  _renderNewsItem(item, index) {
    return (
      <TouchableOpacity key={ index } style={ styles.news.container } onPress={() => Actions.push("NewsDetail", { item: item }) }>
        <View style={ styles.news.description.container }>

          <Text bold>
            { item.title }
          </Text>
          <Text color={ appVariables.colorGray } size={ 12 }>
            { item.date }
          </Text>

        </View>

        <View style={ styles.news.image.container }>
          <Image
            style={{ flex: 1,}}
            // resizeMode={ "center" }
            source={{ uri: item.image }}
          />
        </View>
      </TouchableOpacity>
    );
  }

  _renderLoadingNewsItem(index) {
    return (
      <View key={ index } style={ styles.news.container }>
        <View style={ styles.news.description.container }>
          <Placeholder.Line
            color="#1b1f230d"
            width="77%"
            textSize={14}
            onReady={!this.props.loadingArticle}
            />
          <View style={{ marginBottom: 10 }} />
          <Placeholder.Line
            color="#1b1f230d"
            width="60%"
            textSize={14}
            onReady={!this.props.loadingArticle}
            />
          <View style={{ marginBottom: 10 }} />
          <Placeholder.Line
            color="#1b1f230d"
            width="40%"
            textSize={12}
            onReady={!this.props.loadingArticle}
            />
        </View>
        <View style={ styles.news.image.container }>
          <Placeholder.Media
            color="#1b1f230d"
            size={170}
            onReady={!this.props.loadingArticle}
            />
        </View>
      </View>
    )
  } 

// ---------------------------------------------------

  _renderNews() {
    return (
      <View style={ styles.news.outerContainer }>
        { this._renderContentHeader(Lang.news[this.props.lang], "NewsEvent") }

        <View>
          { this.props.loadingArticle ? 
              [1,2,3].map((news, index) => this._renderLoadingNewsItem(index)) : 
              _.sampleSize( this.state.datacontent, 3).map((news, index) => this._renderNewsItem(news, index)) 
          }
        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderMain() {
    return (
      <View style={ styles.main.innerContainer }>

        { this._renderBookings() }

        { this._renderPromo() }

        { this._renderNews() }
      
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BaseContainer
        hasFooter 
      >

        <ScrollView
          style={ styles.main.container }
          contentContainerStyle={ styles.main.contentContainer }
          showsVerticalScrollIndicator={false}
        >
          { this._renderBackground() }

          <View
            style={ styles.main.rounderContainer }
          />

          <View style={ styles.main.mainContainer }>
            { this._renderMain() }
          </View>
        </ScrollView>

      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ productService, authentication, masterBranch, contentArticle, language, discountPromo }) => {
  const { loading, products } = productService;
  const { isLogin, login, refreshToken, rememberMe } = authentication;
  const { branch } = masterBranch;
  const { articles } = contentArticle;
  const loadingArticle = contentArticle.loading;
  const { lang } = language;
  const { promo } = discountPromo;
  return { loading, products, isLogin, login, branch, refreshToken, rememberMe, articles, loadingArticle, lang, promo };
}

export default connect(mapStateToProps, {
  getProductService,
  setSelectedProductService,
  getMasterBranch,
  getAdjustmentTime,
  getRefreshToken,
  setNewToken,
  isSkipWelcomeScreen,
  postLogout,
  getContentArticle,
  setNotes,
  getPromo
})(Home);
