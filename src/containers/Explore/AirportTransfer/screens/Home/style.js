import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  background: {
    container: {
      position: "absolute",
      flex: -1,
      width: appMetrics.screenWidth,
      height: 300,
      paddingHorizontal: 20,
      backgroundColor: "navy",
    },

    top: {
      container: {
        flex: -1,
        flexDirection: "row",
        height: 60,
        justifyContent: "space-between",
        alignItems: "center",
      },

      logo: {
        container: {
          flex: -1,
          width: 100,
          height: 60,
          justifyContent: "center",
          alignItems: "center",
        },
      },

      icon: {
        container: {
          flex: -1,
          width: 60,
          height: 60,
          justifyContent: "center",
          alignItems: "flex-end"
        },
      },
    },

    bottom: {
      grouper: {
        flex: -1,
        height: 100,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

  main: {
    container: {
      flex: 1,
    },

    contentContainer: {
      paddingTop: 130,
      paddingBottom: 10,
    },

    rounderContainer: {
      flex: -1,
      bottom: -30,
      height: 50,
      borderRadius: 50,
      // backgroundColor: appVariables.colorWhite,
      backgroundColor: "#F6F7FA",
    },

    mainContainer: {
      flex: -1,
      // backgroundColor: appVariables.colorWhite,
      backgroundColor: "#F6F7FA",
    },

    innerContainer: {
      flex: -1,
      paddingHorizontal: 20,
    },
  },

  booking: {
    grouper: {
      height: 120,
      flexDirection: "row",
      justifyContent: "space-around",
    },

    container: {
      flex: -1,
      justifyContent: "center",
      alignItems: "center",
    },

    icon: {
      container: {
        flex: -1,
        height: 80,
        width: 80,
        borderRadius: 50,
        backgroundColor: appVariables.colorBlueHeader,
        marginBottom: 10,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

  contentHeader: {
    container: {
      flex: -1,
      flexDirection: "row",
      justifyContent: "space-between",
    },

    seeAll: {
      container: {
        flex: -1,
        flexDirection: "row",
        alignItems: "center",
      },
    },
  },

  promo: {
    outerContainer: {
      marginTop: 30,
    },

    main: {
      grouper: {
        marginTop: 10,
        flex: -1,
        left: -20,
        width: appMetrics.screenWidth,
      },

      innerGrouper: {
        paddingLeft: 20,
      },

      container: {
        flex: -1,
        height: 170,
        width: 300,
        borderRadius: 10,
        marginRight: 20,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

  news: {
    outerContainer: {
      marginTop: 30,
    },

    container: {
      flex: -1,
      flexDirection: "row",
      borderRadius: 10,
      overflow: "hidden",
      marginVertical: 7,
      borderColor: "#dddddd",
      borderWidth: 1,
      backgroundColor: appVariables.colorWhite,
      shadowOffset:{  width: 0,  height: 0,  },
      shadowColor: 'black',
      shadowOpacity: .2,
      shadowRadius: 7,
      elevation:2,
    },

    description: {
      container: {
        flex: 1,
        justifyContent: "center",
        padding: 10,
      },
    },

    image: {
      container: {
        flex: -1,
        height: 120,
        width: 120,
      },
    },
  },

}