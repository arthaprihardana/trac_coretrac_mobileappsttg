export default {
  mainContainer:{
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 30,
  },
  button:{
    flex: -1,
    paddingTop:30,
    paddingBottom:30,
  }
}