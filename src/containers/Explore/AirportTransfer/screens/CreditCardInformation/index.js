/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-12 07:32:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 16:59:49
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import {
  View,
  ScrollView,
  Alert
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";

import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";

import styles from "./style";
import { CREDIT_CARD_LIBRARY } from "../../../../../constant";
import { connect } from 'react-redux';
import _ from 'lodash';
import { saveCreditCard, setPrimaryCreditCard } from "../../../../../actions";

class CreditCardInformation extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      form: {
        number: "",
        name: "",
        expiration: "",
        bank: ""
      },
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.saveCC !== prevProps.saveCC) {
      return { saveCC: this.props.saveCC };
    }
    if(this.props.primaryCC !== prevProps.primaryCC) {
      return { primaryCC: this.props.primaryCC }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.saveCC !== undefined) {
        if(snapshot.saveCC.Id) {
          this.props.setPrimaryCreditCard({
            FormData: {Id: snapshot.saveCC.Id},
            token: this.props.login.token,
            UserLoginId: this.props.login.Data.Id
          });
        } else {
          Alert.alert(
            'Information',
            'Oops! Something went wrong!',
            [
              {text: 'OK', onPress: () => Actions.push("Payment", {
                navData: this.props.navData
              })},
            ],
            {cancelable: false},
          );
        }
      }
      if(snapshot.primaryCC !== undefined) {
        Alert.alert(
          'Information',
          'Credit Card Succesfully saved',
          [
            {text: 'OK', onPress: () => Actions.push("Payment", {
              navData: this.props.navData
            })},
          ],
          {cancelable: false},
        );
      }
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------
  
  submit() {
    let x = _.filter(CREDIT_CARD_LIBRARY, v => v.reg.test(_.replace(this.state.form.number, /-/g, "")));
    this.props.saveCreditCard({
      FormData: {
        UserId: this.props.login.Data.Id,
        CreditCard : this.state.form.number,
        CardExpired : this.state.form.expiration,
        CardPublisher : this.state.form.bank,
        CardImage : x.length > 0 ? x[0].img : null,
        CardType : x.length > 0 ? x[0].type : null
      },
      token: this.props.login.token
    })
  }

// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }

// ---------------------------------------------------

  formatCreditCardNumber(value) {
    value = value + "";
    value = value.replace(/\-/g, "");

    let split = [];
    while (value.length > 0) {
      split.push(value.substring(0, 4));

      value = value.substring(4);
    }

    return split.join("-").substring(0, 19);
  }

// ---------------------------------------------------

  formatExpiration(value) {
    value = value + "";
    value = value.replace(/\//g, "");

    let split = [];
    while (value.length > 0) {
      split.push(value.substring(0, 2));

      value = value.substring(2);
    }

    if ((split[0] * 1) > 12) {
      return split[0].substring(0, 1);
    }

    return split.join("/").substring(0, 5);
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------
  _renderDescription() {
    return (
      <View>
        <Text
          size={16}
          color={appVariables.colorGray}>
          This only for verification purposes, Your credit card will not be charged for this transaction.
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderField() {
    return (
      <View>
        <FloatingTextInput
          label={"Credit/Debit Card Number"}
          keyboardType={"numeric"}
          onChangeText={(value) => this.onChangeForm("number", value)}
          customFormat={(value) => this.formatCreditCardNumber(value)}
          value={this.state.form.number}
        />
        <FloatingTextInput
          label={"Valid Until"}
          keyboardType={"numeric"}
          onChangeText={(value) => this.onChangeForm("expiration", value)}
          value={this.state.form.expiration}
          customFormat={(value) => this.formatExpiration(value)}
          half
        />
        <FloatingTextInput
          label={"Name on Card"}
          onChangeText={(value) => this.onChangeForm("name", value)}
          value={this.state.form.name}
        />
        <FloatingTextInput
          label={ "Bank" }
          onChangeText={ (value) => this.onChangeForm("bank", value) }
          value={ this.state.form.bank }
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderButton() {
    return (
      <View style={styles.button}>
        <MainButton
          label={"CONTINUE"}
          rounded
          onPress={ () => this.submit() }
          isLoading={this.props.loading}
        />
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------
  render() {
    const navData = this.props.navData;

    return (
      <ProgressedContainer
        header={{
          label: "Credit Card Information",
          onBackPress: () => Actions.pop(),
        }}
        steps={navData.processSteps}
        currentStepIndex={navData.processSteps.indexOf("Payment")}
        {... this.props}
      >
        <ScrollView style={{
          flex: 1,
          paddingHorizontal: 20,
          paddingVertical: 20,
        }}>
          {this._renderDescription()}
        
          {this._renderField()}
        
          {this._renderButton()}
        </ScrollView>
      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ creditCard, authentication }) => {
  const { saveCC, primaryCC, loading } = creditCard;
  const { login } = authentication;
  return { saveCC, primaryCC, loading, login };
}

export default connect(mapStateToProps, {
  saveCreditCard,
  setPrimaryCreditCard
})(CreditCardInformation);