/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 23:38:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-05 15:01:58
 */
import React, { Component } from 'react';
import { Content, Text } from 'native-base';
import { View, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import {appVariables} from '@variables';
import styles from './style';
import Header from "@airport-transfer-fragments/Header";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { getCreditCard, setPrimaryCreditCard, deleteCreditCard } from "../../../../../actions";
import Icon from 'react-native-vector-icons/Ionicons';
import _ from "lodash";

class ViewCreditCard extends Component {

  state = {
    listCC: [],
    activeCreditCard: null
  }

  componentDidMount = () => {
    this.props.getCreditCard({
      UserLoginId: this.props.login.Data.Id,
      token: this.props.login.token
    })
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.cc !== prevProps.cc) {
      return { cc: this.props.cc }
    }
    if(this.props.deleteCC !== prevProps.deleteCC) {
        return { deleteCC: this.props.deleteCC }
    }
    return null; 
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.cc !== undefined) {
        let x = _.filter(snapshot.cc, v => v.IsPrimary === "1");
        this.setState({
          listCC: snapshot.cc,
          activeCreditCard: x[0].Id
        });
      }
      if(snapshot.deleteCC !== undefined) {
        this.props.getCreditCard({
            UserLoginId: this.props.login.Data.Id,
            token: this.props.login.token
        })
      }
    }
  }

  componentWillUnmount = () => {
    Actions.refresh({ refresh: true });
  }
  

  onChangeForm(name, value) {
    const form = this.state;
    form[name] = value;

    this.setState({
      form
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------
  goToDeleteCreditCardConfirmation(){
    Actions.DeleteCreditCardConfirmation();
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderOpeningWording() {
    return (
      <View style={{ 
          marginTop: 20,
          borderBottomWidth:1,
          borderBottomColor:"#D8D8D8",
        }}>
        <Text style={styles.openingWord}>
          Please enter a credit or debit card to make it easier for you to order.
          Your payment details are stored safely
      </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderLabel() {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <View>
            <Text style={styles.title}>Credit Card Number</Text>
          </View>
        </View>

        <View style={styles.subContainer}>
          <View>
            <Text style={styles.title}>Expiry Date</Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderLabelContent(cc) {
    let x = _.replace(cc.DecryptedCreditCard, /-/g, " ");
    let y = _.replace(x, /^\d{4}\ \d{4}/g, "**** ****");
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <View>
            <Text style={styles.contentWrapper}>{y}</Text>
          </View>
        </View>

        <View style={styles.subContainer}>
          <View>
            <Text style={styles.contentWrapper}>{cc.DecryptedCardExpired}</Text>
          </View>
        </View>
      </View>

    );
  }

  // ---------------------------------------------------
  _renderVisaCard(CardImage, index) {
    return (
      <View style={styles.visaCardWrapper}>
        <View style={styles.subContainer}>
          <View>
            <Image source={{ uri: CardImage }} style={{ width: 40, height: 20 }} resizeMode="cover" />
          </View>
          <View>
          <RoundedCheck
            value={ this.state.activeCreditCard === index }
            onPress={ () => {
                this.onChangeForm("activeCreditCard", index);
                this.props.setPrimaryCreditCard({
                    FormData: {Id: this.state.activeCreditCard},
                    token: this.props.login.token,
                    UserLoginId: this.props.login.Data.Id
                });
                setTimeout(() => Actions.popTo("ProfileCreditCard") , 500 );
            }}>
            <View>
                <Text style={{
                    fontSize: 12,
                    color: appVariables.colorGray,
                }}>Set as Primary</Text>
            </View>
            </RoundedCheck>
          </View>
        </View>
      </View>
    );
  }

  _renderNameonCard(cc) {
    return (
      <View style={{
        paddingVertical: 20
      }}>
        <View style={styles.container}>
          <View style={styles.subContainer}>
            <View>
              <Text style={styles.title}>Name On Card</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.subContainer}>
            <View>
              <Text style={styles.contentWrapper}>Bryan Barry Borang</Text>
            </View>
          </View>
        </View>

      </View>
    );
  }

  // ---------------------------------------------------
  _renderClosingWording() {
    return (
      <TouchableOpacity 
        style={{paddingVertical:20}}
        onPress={()=> this.goToDeleteCreditCardConfirmation()}>
        <Text style={styles.closingWord}>
          Tap and hold to remove your card
          </Text>
      </TouchableOpacity>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Credit/Debit Card"}
          onBackPress={() => this.props.navigation.pop()} />

        <Content style={{ backgroundColor: '#FFFFFF' }}>

            {this._renderOpeningWording()}
            { this.props.loading && <View style={{ width: '100%', padding: 20, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="small" color="#2A2E36" /></View> }
            { this.state.listCC.length > 0 &&
                _.map(this.state.listCC, (v, k) => (
                    <View key={k} style={{
                        paddingVertical:20,        
                        borderBottomWidth:1,
                        borderBottomColor:"#D8D8D8"
                        }}>
                            {this._renderVisaCard(this.state.listCC[k].CardImage, this.state.listCC[k].Id)}
                            {this._renderLabel()}
                            {this._renderLabelContent(this.state.listCC[k])}
                            {this.state.listCC[k].IsPrimary !== "1" && 
                            <TouchableOpacity onPress={() => this.props.deleteCreditCard({Id: v.Id, token: this.props.login.token})} style={{ paddingLeft: 30, paddingRight: 30, marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                <Icon name="md-trash" size={22} color={appVariables.colorOrange} />
                                <Text style={{
                                    fontSize: 12,
                                    color: appVariables.colorGray,
                                    marginLeft: 10
                                }}>Remove Card</Text>
                            </TouchableOpacity> }
                            
                            {/* {this._renderNameonCard(this.state.listCC)} */}
                    </View>
                ))
            }
          {/* {this._renderClosingWording()} */}
        </Content>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ creditCard, authentication }) => {
  const { cc, loading, deleteCC } = creditCard;
  const { login } = authentication;
  return { cc, loading, login, deleteCC }
}

export default connect(mapStateToProps, {
  getCreditCard,
  setPrimaryCreditCard,
  deleteCreditCard
})(ViewCreditCard);