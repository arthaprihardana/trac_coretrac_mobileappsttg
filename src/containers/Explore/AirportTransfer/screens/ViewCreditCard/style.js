import {StyleSheet} from 'react-native';
import { appVariables } from '@variables';

const styles = StyleSheet.create({
  title:{
    fontSize: 12,
    color: appVariables.colorGray,
    flex: 1,
    paddingVertical:10,
    paddingHorizontal:30,
  },
  openingWord:{
    fontSize: 14,
    color: appVariables.colorGray,
    flex: 1,
    paddingVertical:20,
    paddingHorizontal:30,
  },
  closingWord:{
    fontSize:14,
    color: appVariables.colorGray,
    flex:1,
    paddingHorizontal:30,
  },
  container:{
    flexDirection: 'row',
  },
  subContainer:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  visaCardWrapper:{
    flex:1,
    flexDirection:'row',
    paddingHorizontal:20,
  },
  contentWrapper: {
    paddingHorizontal:30,
  },
  buttonWrapper: {
    borderRadius: 7,
    height: 47,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    backgroundColor: 'white',
    paddingHorizontal: 40,
  },
})

export default styles;