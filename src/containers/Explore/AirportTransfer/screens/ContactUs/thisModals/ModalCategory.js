import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { OChangePassword } from '../../../../../components/organisms';

class ModalCategory extends Component {
    render() {
        let { action } = this.props;
        return (
            <OChangePassword action={action}>
            </OChangePassword>
        )
    }
}

ModalChangePassword.propTypes = {
    action: PropTypes.func
}

export default ModalCategory;