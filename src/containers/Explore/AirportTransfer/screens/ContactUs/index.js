/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 22:27:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 16:53:09
 */
import React, { Component } from 'react';
import { Item, Label, Input } from 'native-base';
import { Alert,Modal,View,TouchableHighlight,TouchableOpacity,TouchableWithoutFeedback,ScrollView} from 'react-native';
import styles from './style';
//import ModalChangePassword from './thisModals/ModalChangePassword';
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import PickerInput from "@airport-transfer-fragments/Picker";
import MainButton from "@airport-transfer-fragments/MainButton";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import RNPickerSelect from 'react-native-picker-select';
import Icon from '@airport-transfer-fragments/Icon';
import { connect } from "react-redux";
import { postContactUs } from "../../../../../actions";
import _ from "lodash";
import { Actions } from 'react-native-router-flux';
import Lang from '../../../../../assets/lang';
import {
  appVariables,
  appMetrics,
} from "@variables";

const categoryList = [
  {
    label: 'PERTANYAAN',
    value: 'pertanyaan',
  },
  {
    label: 'KOMPLAIN',
    value: 'komplain',
  },
  {
    label: 'PEMESANAN',
    value: 'pemesanan',
  }
];

const subcategoryList = [
  {
    label: 'UNIT',
    value: 'unit',
  },
  {
    label: 'DRIVER',
    value: 'driver',
  },
  {
    label: 'UNIT & DRIVER',
    value: 'unitdriver',
  },
];

class ContactUs extends Component {

  constructor(props) {
    super(props);
    this.inputCategory = {
      category: null,
    };
    this.inputSubCategory = {
      subcategory: null,
    };

    this.state = {   
      form: {
        attachments: [],
        category: null,
        subcategory: null,
        fromNames: `${this.props.login.Data.FirstName} ${this.props.login.Data.LastName}`,
        from: this.props.login.Data.EmailPersonal,
        noTelp: this.props.login.Data.NoHandphone,
        message: null,
      },
      readyToSubmit: false
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.sendContactUs !== prevProps.sendContactUs) {
      return { sendContactUs: this.props.sendContactUs }
    }
    return null;
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
     if(snapshot !== null) {
       if(snapshot.sendContactUs) {
        Alert.alert(
          'Successfully',
          'Send The Message',
          [
            {text: 'OK', onPress: () => Actions.reset('Home') },
          ],
          {cancelable: false},
        );
       }
     }
  }

  _onPressButton() {
    // Alert.alert('Saved')
    // let params = _.omit(this.state.form, ['readyToSubmit']);
    // console.log('params ==>', params);
    
    this.props.postContactUs({
      FormData: this.state.form,
      token: this.props.login.token
    });
  }

  onValidate = async current => {
    let except = ['attachments'];
    except.push(current);
    let o = _.omit(this.state.form, except);
    let a = await _.findKey(o, val => {
      if(_.isEmpty(val)) { return true }
    });
    return a;
  }

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }

  render() {
    // const nav = this.props.navigation;

    // const subcategoryplaceholder = {
    //   label: 'Sub Category',
    //   value: null,
    //   color: 'black'
    // };

    // const categoryplaceholder = {
    //   label: 'Category',
    //   value: null,
    //   color: 'black'
    // };

    return (
      <BaseContainer>
        <Header
          title={Lang.contactUs[this.props.lang]} onPress={this._onPressButton}
          onBackPress={ () => this.props.navigation.pop()}/>
        <ScrollView 
          style={{
            flex: 1,
            backgroundColor: '#FFFFFF',
            paddingHorizontal: 20,
            marginBottom: 20
          }}
          showsVerticalScrollIndicator={false}>
          <FloatingTextInput
            refs={ref => this._fromNames = ref}
            label={ Lang.name[this.props.lang] }
            onChangeText={ (value) => {
              this.onValidate("fromNames").then(val => {
                this.onChangeForm("fromNames", value) 
                this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
              });
            }}
            value={ this.state.form.fromNames }
            keyboardType={ "default" }
            returnKeyType="next"
            onSubmitEditing={() => this._from.ref.focus()}
          />
          <FloatingTextInput
            refs={ref => this._from = ref}
            label={ Lang.email[this.props.lang] }
            onChangeText={ (value) => {
              this.onValidate("from").then(val => {
                this.onChangeForm("from", value) 
                this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
              });
            }}
            value={ this.state.form.from }
            keyboardType={ "email-address" }
            returnKeyType="next"
            onSubmitEditing={() => this._noTelp.ref.focus()}
          />
          <FloatingTextInput
            refs={ref => this._noTelp = ref}
            label={ Lang.phoneNumber[this.props.lang] }
            onChangeText={ value => {
              this.onValidate("noTelp").then(val => {
                this.onChangeForm("noTelp", value);
                this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
              });
            }}
            value={ this.state.form.noTelp }
            keyboardType="phone-pad"
            maxLength={13}
          />
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ width: '50%', paddingRight: 5 }}>
              <PickerInput 
                refs={ref => this._category = ref}
                placeholder={Lang.category[this.props.lang]}
                value={this.state.form.category !== null ? this.state.form.category.toUpperCase() : null}
                selectedValue={ this.state.form.category }
                item={categoryList}
                onValueChange={(itemValue, itemLabel) => {
                  this.onValidate("category").then(val => {
                    this.onChangeForm("category", itemValue);
                    this.setState({ readyToSubmit: itemValue !== "" && val === undefined ? true : false })
                  });
                }}
                />
            </View>
            <View style={{ width: '50%', paddingLeft: 5 }}>
              <PickerInput 
                refs={ref => this._subcategory = ref}
                placeholder={Lang.subCategory[this.props.lang]}
                value={this.state.form.subcategory !== null ? this.state.form.subcategory.toUpperCase() : null }
                selectedValue={ this.state.form.subcategory }
                item={subcategoryList}
                onValueChange={(itemValue, itemIndex) => {
                  this.onValidate("subcategory").then(val => {
                    this.onChangeForm("subcategory", itemValue);
                    this.setState({ readyToSubmit: itemValue !== "" && val === undefined ? true : false })
                  });
                }}
                />
            </View>
          </View>
          <FloatingTextInput
            refs={ref => this._message = ref}
            label={ Lang.message[this.props.lang] }
            onChangeText={ value => {
              this.onValidate("message").then(val => {
                this.onChangeForm("message", value);
                this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
              });
            }}
            value={ this.state.form.message }
            keyboardType="default"
            // multiline = {true}
            // numberOfLines = {4}
            // maxLength = {250}
          />
        </ScrollView>

        {/* <Modal animationType="none"  visible={this.state.modalChangePassword} onRequestClose={() => { }} >
            <ModalChangePassword action={this.handleChangePasswordNotify} />
        </Modal>  */}
        <MainButton 
          label={Lang.sendButton[this.props.lang]} 
          isLoading={this.props.loading}
          onPress={ () => this.state.readyToSubmit ? this._onPressButton() : {}}
          disabled={!this.state.readyToSubmit} />
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ contactUs, authentication, language }) => {
  const { loading, postContactUs, sendContactUs } = contactUs;
  const { login } = authentication;
  const { lang } = language;
  return { loading, postContactUs, sendContactUs, login, lang };
}

export default connect(mapStateToProps, {
  postContactUs
})(ContactUs);
