/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 22:26:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-08 08:42:34
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  Modal,
  Keyboard
} from "react-native";
import {
  appVariables,
} from "@variables";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import styles from "./style";
import Icon from "@airport-transfer-fragments/Icon";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { getBank, postCancelReservation } from "../../../../../actions";

class CancelBookingRefund extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      bank: [],
      readyToSubmit: false,
      modalOpen: false,
      selectedBank: null,
      form: {
        BankAccount: null,
        NameAccount: null
      }
    }
  }

  componentDidMount = () => {
    this.props.getBank({ BusinessUnitId: this.props.item.BusinessUnitId }); 
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.bank !== prevProps.bank) {
      return { bank: this.props.bank }
    }
    if(this.props.cancel !== prevProps.cancel) {
      return { cancel: this.props.cancel }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.bank !== undefined) {
        this.setState({
          bank: snapshot.bank,
          selectedBank: snapshot.bank[0].MsBankId
        })
      }
      if(snapshot.cancel !== undefined) {
        Actions.reset("CancelBookingConfirmation");
      }
    }
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------
  onValidate = async current => {
    let except = [];
    except.push(current);
    let o = _.omit(this.state.form, except);
    let a = await _.findKey(o, val => {
      if(_.isEmpty(val)) { return true }
    });
    return a;
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  goTopCancelBookingConfirmation(){
    Keyboard.dismiss();
    this.props.postCancelReservation({
      FormData: {
        CancellationReasonId: this.props.reasonId,
        CancellationReason: this.props.reason,
        ReservationId: this.props.item.ReservationId,
        NoRekening: this.state.form.BankAccount,
        BankName: _.filter(this.state.bank, { MsBankId: this.state.selectedBank })[0].MsBankName,
        BankAccountName: this.state.form.NameAccount
      },
      token: this.props.login.token
    });
    // Actions.CancelBookingConfirmation();
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderBankAccountTitle() {
    return (
      <View style={styles.bankAccountWrapper}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <Text
            color={appVariables.colorBlack}
            size={18}>
            Bank Account
            </Text>
          <Text
            color={appVariables.colorGray}
            style={{ paddingTop: 10 }}
            size={14}>
            Please enter your bank account to refund your order
          </Text>
        </View>
      </View>
    )
  }

  // ---------------------------------------------------
  _renderBankDropdown() {
    return (
      <View style={styles.dropdownWrapper}>
        <View style={styles.flexWrapper}>
          <Text
            color={appVariables.colorGray}
            size={14}>
            Select Bank
          </Text>
        </View>
        
        <View style={styles.flexWrapper}>
        <TouchableOpacity style={styles.buttonWrapper} onPress={() => this.setState({ modalOpen: true })}>
          <View style={{
            flex: -1,
            flexDirection: "column",
          }}>
            <Text
              color={appVariables.colorBlack}
              size={14} >
              { this.state.selectedBank !== null && _.filter(this.state.bank, { MsBankId: this.state.selectedBank })[0].MsBankName }
            </Text>
          </View>
          <View style={styles.iconCaretDown}>
            <Icon name="angle-down" size={16} />
          </View>
        </TouchableOpacity>
        </View>
      </View>
    )
  }

  // ---------------------------------------------------
  _renderBankAccountData() {
    return (
      <View style={styles.textInputWrapper}>
        <FloatingTextInput
          onChangeText={ (value) => {
            this.onValidate("BankAccount").then(val => {
              let f = this.state.form;
              f.BankAccount = value
              this.setState({ 
                form: f,
                readyToSubmit: value !== "" && val === undefined ? true : false 
              })
            });
          }}
          value={ this.state.form.BankAccount }
          keyboardType={ "phone-pad" }
          label={"Bank Account"} />
        <FloatingTextInput
          onChangeText={ (value) => {
            this.onValidate("NameAccount").then(val => {
              let f = this.state.form;
              f.NameAccount = value
              this.setState({ 
                form: f,
                readyToSubmit: value !== "" && val === undefined ? true : false 
              })
            });
          }}
          value={ this.state.form.NameAccount }
          label={"Name"} />
      </View>
    )
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Cancel Booking"}
          onBackPress={() => this.props.navigation.pop()}/>
        <ScrollView style={styles.mainContainer}>
          {this._renderBankAccountTitle()}
          {this._renderBankDropdown()}
          {this._renderBankAccountData()}
        </ScrollView>
        <MainButton
          isLoading={this.props.loading}
          label={"CANCEL BOOKING"}
          onPress={() => this.props.loading || !this.state.readyToSubmit ? {} : this.goTopCancelBookingConfirmation()}
          disabled={!this.state.readyToSubmit}/>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalOpen}
          onRequestClose={() => this.setState({ modalOpen: false })}>
          <View style={styles.container}>
              <View style={styles.wrapper}>
                  <TouchableOpacity onPress={() => this.setState({ modalOpen: false })} style={styles.closeButton}>
                      <Icon name="times" color={'#FFF'} size={20} />
                  </TouchableOpacity>
                  <View style={styles.navBarTitle}>
                      <Text bold style={{ color: '#FFF' }}>Bank</Text>
                  </View>
              </View>
              <ScrollView keyboardShouldPersistTaps="always">
                  { this.state.bank.length > 0 && _.map(this.state.bank, (v, i) => (
                    <TouchableOpacity key={i} onPress={() => this.setState({ selectedBank: v.MsBankId, modalOpen: false })} style={{ width: '100%', height: 40, paddingVertical: 20, paddingHorizontal: 20, justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text>{v.MsBankName}</Text>
                    </TouchableOpacity>
                  )) }
              </ScrollView>
          </View>
        </Modal>
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ masterBank, cancelReservation, authentication }) => {
  const { loading, cancel } = cancelReservation;
  const { bank } = masterBank;
  const { login } = authentication;
  return { loading, bank, cancel, login }
}

export default connect(mapStateToProps, { 
  getBank,
  postCancelReservation
})(CancelBookingRefund);