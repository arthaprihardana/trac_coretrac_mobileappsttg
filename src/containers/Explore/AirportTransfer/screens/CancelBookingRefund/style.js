import { Platform } from "react-native";
import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  mainContainer:{
    backgroundColor: appVariables.colorWhite,
    paddingVertical: 20,
    paddingHorizontal: 20
  },
  bankAccountWrapper:{
    flex: 1,
    flexDirection: 'row',
    // paddingVertical: 10,
    // paddingHorizontal: 20,
  },
  buttonWrapper: {
    flex: -1,
    flexDirection: "row",
    borderWidth: 1,
    borderRadius: 7,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderColor: appVariables.colorGray,
  },
  dropdownWrapper:{
    flex: -1,
    flexDirection: 'column',
    // paddingVertical: 10,
    // paddingHorizontal: 20,
  },
  flexWrapper:{
    flex: 1,
    flexDirection: "row",
    paddingVertical: 10,
  },
  iconCaretDown:{
    flex: -1,
    flexDirection: "column",
    alignItems: "center",
    paddingLeft:30,
  },
  textInputWrapper:{
    flex: -1,
    flexDirection: 'column',
    paddingVertical: 10,
    // paddingHorizontal: 20,
  },

  container: {
    flex: 1
  },
  wrapper: {
      width: '100%', 
      flexDirection: 'row', 
      backgroundColor: appVariables.colorBlueHeader, 
      height: Platform.OS == "ios" ? 65 : 56, 
      paddingTop: Platform.OS == "ios" ? 15 : 0,
      alignItems: 'center', 
      justifyContent: 'flex-start'
  },
  closeButton: { 
      width: 56, 
      height: 56, 
      justifyContent: 'center',
      alignItems: 'center' 
  },
  navBarTitle: { 
      height: 56, 
      width: '80%', 
      alignItems: 'flex-start', 
      justifyContent: "center"
  }
}