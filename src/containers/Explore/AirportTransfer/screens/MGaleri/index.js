/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-20 13:23:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-24 03:14:53
 */
import React, { Component } from 'react';
import { Content, Thumbnail, Button, Text, Textarea } from 'native-base';
import { View, Image, TouchableOpacity, ScrollView, Keyboard, Alert, PermissionsAndroid, FlatList, Dimensions, CameraRoll, Platform } from 'react-native';
// import CameraRoll from "@react-native-community/cameraroll";
import { appVariables } from '@variables';
import { connect } from "react-redux";
import _ from 'lodash';
import fs from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';
import { Actions } from 'react-native-router-flux';
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import { setCaptureForProfile, setCaptureForSim, setCaptureForKtp, setCaptureCamera } from "../../../../../actions"

const { width } = Dimensions.get("window");

class MGaleri extends Component {

    state = {
        photos: [],
        page: {},
        isInfinityScroll: false,
        isNoMoreData: false,
        showInfoNoMoreData: false,
        capture: {}
    }

    componentDidMount() {
        this.requestReadStoragePermission();
    }

    componentWillUnmount() {
        Actions.refresh();
    }

    async requestReadStoragePermission() {
        try {
            if(Platform.OS === "android") {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
                        title: 'TRAC To Go Read Galery Permission',
                        message:'TRAC To Go needs access to read galery.',
                        buttonNeutral: 'Tanya Nanti',
                        buttonNegative: 'Batal',
                        buttonPositive: 'Setuju',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.getCameraRoll();
                } else {
                    console.log('denied');
                }
            } else {
                this.getCameraRoll();
            }
        } catch (err) {
            console.log(err);
        }
    }

    getCameraRoll = has_next_page => {
        let options = {
            first: 20,
            assetType: 'Photos'
        };
        if(has_next_page) {
            options.after = this.state.page.end_cursor
        }
        CameraRoll.getPhotos(options).then(r => {
            this.setState(prevState => ({ 
                photos: _.concat(prevState.photos, r.edges),
                page: r.page_info,
                isNoMoreData: !r.page_info.has_next_page,
                showInfoNoMoreData: !r.page_info.has_next_page
            }), () => {
                if(this.state.isNoMoreData) {
                    setTimeout(() => {
                        this.setState({ showInfoNoMoreData: false })
                    }, 2000);
                }
            });
        }).catch((err) => {
            console.log('err==>', err);
        });
    }

    renderFooterComponent = () => {
        if (!this.state.isInfinityScroll) return null;
        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    paddingVertical: 10,
                    borderTopWidth: 0.5,
                    borderColor: '#ccc',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{ fontSize: 12 }}>Sedang memuat gambar ...</Text>
            </View>
          );
    }

    handleLoadMore = () => {
        if(!this.state.isNoMoreData) {
            this.setState(prevState => ({
                isInfinityScroll: true
            }), () => this.getCameraRoll(this.state.page.has_next_page));
        }
    }
    
    onSelectedImage = async node => {
        let data = null;
        console.log('node ==>', node);
        if(Platform.OS === "ios") {
            const dest = `${fs.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
            try {
                let absolutePath = await fs.copyAssetsFileIOS(node.image.uri, dest, 0, 0, 1, 0.3);
                data = await fs.readFile(absolutePath, 'base64');
            } catch(err) {
                console.log(err)
            } 
        } else {
            data = await fs.readFile(node.image.uri, 'base64');
        }
        // console.log('data ==>', data);
        switch (this.props.route) {
            case "Account":
                this.props.setCaptureForProfile({
                    uri: node.image.uri,
                    base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                    type: "galeri"
                });
                break;
            case "IdentificationInformation":
                if(this.props.type === "ktp") {
                    this.props.setCaptureForKtp({
                        uri: node.image.uri,
                        base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                        type: "galeri"
                    });
                } else {
                    this.props.setCaptureForSim({
                        uri: node.image.uri,
                        base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                        type: "galeri"
                    });
                }
                break;
            case "PassangerDetail":
                this.props.setCaptureCamera({
                    uri: node.image.uri,
                    base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                    type: "galeri"
                });
                break;
            case "DriverInformation":
                if(this.props.type === "ktp") {
                    this.props.setCaptureForKtp({
                        uri: node.image.uri,
                        base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                        type: "galeri"
                    });
                } else {
                    this.props.setCaptureForSim({
                        uri: node.image.uri,
                        base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                        type: "galeri"
                    });
                }
                break;
            case "CoordinatorInformation":
                this.props.setCaptureForKtp({
                    uri: node.image.uri,
                    base64: `data:${Platform.OS === "ios" ? "image/jpeg" : node.type};base64, ${data}`,
                    type: "galeri"
                });
                break;
        }
        Actions.popTo(this.props.route);
    }

    render() {
        return (
            <BaseContainer>
                <Header title={"Galeri"} onBackPress={() => Actions.popTo(this.props.route) } />
                <View style={{ flex: 1 }}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.photos}
                        numColumns={2}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={() => this.renderFooterComponent()}
                        renderItem={({item, index}) => <TouchableOpacity onPress={() => this.onSelectedImage(item.node)}><Image
                            key={index}
                            style={{
                                width: width / 2,
                                height: width / 2,
                            }}
                            source={{ uri: item.node.image.uri }}
                            resizeMode="cover"
                            loadingIndicatorSource={require('../../../../../assets/images/png/default.png')}
                            /></TouchableOpacity>
                        }
                        />
                    { this.state.showInfoNoMoreData && 
                    <View style={{ position: 'absolute', bottom: 0, width: width, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#f08080' }}>
                        <Text style={{ fontSize: 10, color: '#fff' }}>Tidak ada lagi gambar untuk ditampilkan</Text>
                    </View> }
                </View>
            </BaseContainer>
        )
    }
}

const mapStateToProps = ({  }) => {
    return {}
}

export default connect(mapStateToProps, {
    setCaptureForProfile,
    setCaptureForSim, 
    setCaptureForKtp,
    setCaptureCamera
})(MGaleri);