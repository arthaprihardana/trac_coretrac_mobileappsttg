/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-06 13:00:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 20:48:01
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Clipboard,
  ImageBackground,
} from "react-native";
import { H1, H2, p } from 'native-base';
import {
  appVariables,
  appMetrics
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import ImageHeader from "@airport-transfer-fragments/ImageHeader";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import Icon from "@airport-transfer-fragments/Icon";
import { Actions} from "react-native-router-flux";

export default class ProductService extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  state = {
    data: [{
      title: "Rental Mobil Korporat",
      description: "Manfaatkan solusi transportasi yang menyeluruh untuk beragam kebutuhan bisnis Anda. Kami menjamin layanan transportasi yang aman, andal, dan terukur. Anda pun dapat memfokuskan energi dan sumber daya Anda untuk mengembangkan bisnis dalam menghadapi tantangan masa depan.",
      image: "https://trac.astra.co.id/static/media/CorporateLease.bf13e284.jpg",
      detail: {
        title: "Kami Siap Mendukung Bisnis Anda",
        description: "Sistem transportasi yang terkelola dengan baik dan efisien menjadi salah satu pendukung kesuksesan bisnis. Meski demikian, kami mengerti bahwa kebutuhan tiap perusahaan tak selalu sama sehingga memerlukan solusi transportasi yang dirancang secara khusus. Oleh karen itu, TRAC telah menyediakan beragam solusi transportasi berbasis kontrak dengan jangka waktu kerja sama minimal satu tahun. Kami siap mendukung bisnis Anda dengan jaminan layanan yang aman, nyaman, dan terukur.",
        product: [{
          title: "Operating Lease",
          icon: require('@images/png/bannerContent/car-lease.png'),
          description: `Untuk mengakomodasi kebutuhan bisnis Anda, kami menyediakan layanan penyewaan kendaraan untuk masa sewa lebih dari 1 tahun. Layanan sewa kami juga ditunjang dengan Pusat-Bantuan-Pelanggan 24 jam dan jaringan layanan yang telah tersebar di seluruh nusantara.

Kami memiliki beragam pilihan moda transportasi yang dapat disesuaikan dengan kebutuhan transportasi bisnis Anda, mulai dari motor, mobil penumpang, mobil niaga hingga bus. Tak hanya itu, kami juga siap melayani kebutuhan transportasi untuk perusahaan yang aktivitas bisnisnya di remote area seperti industri pertambangan dan perkebunan. Apapun jenis bisnis Anda, TRAC siap melayani. Melengkapi layanan tersebut, kami juga memiliki layanan rental mobil berbasis syariah melalui TRAC Syariah yang merupakan pionir layanan transportasi Syariah di Indonesia.`,
          subProduct: [{
            title: "TRAC Syariah",
            description: `Layanan berbasis sistem syariah semakin menjadi kebutuhan. Bagi Anda yang juga memiliki kebutuhan ini, TRAC menyediakan pengelolaan transportasi dengan cara syariah yang mengedepankan transaksi tanpa riba. Seluruh aset dibiayai dan dijamin oleh institusi keuangan syariah dan Anda tidak akan dikenakan denda pembatalan. TRAC Syariah hanya membebankan Ta’widh dan Gharamah yang nantinya disumbangkan untuk kebaikan masyarakat.

Selain itu, TRAC Syariah menerapkan sistem pengembalian dana berdasarkan konsep pembagian risiko. Anda hanya membayar biaya perawatan yang masuk dalam deposit. Jika biaya yang digunakan tidak melebihi jumlah deposit, Anda akan mendapatkan kembali dana yang tak terpakai tersebut.

Melalui pengawasan dari divisi syariah yang beranggotakan Badan Penasihat Syariah (SAC), layanan TRAC Syariah konsisten menerapkan kaidah syariah yang berlaku. Kami merupakan perusahaan transportasi pertama yang memberikan layanan berbasis syariah secara resmi di Indonesia. Kami telah mendapatkan surat rekomendasi Nomor U-576/DSN-MUI/XI/2016 dari Dewan Syariah Nasional – Majelis Ulama Indonesia (DSN MUI).

Dengan layanan operating lease TRAC membantu perusahaan Anda mengelola kebutuhan transportasi secara optimal. Perusahaan bisa menghemat biaya serta sumber daya pengelolaan transportasi dan mengalihkannya untuk investasi yang penting bagi perkembangan bisnis. Hubungi Customer Assistance Center 1500009 untuk informasi lebih detail.`
          }]
        },{
          title: "Rental Mobil Harian Korporat",
          icon: require('@images/png/bannerContent/car-lease.png'),
          description: "Perusahaan Anda memerlukan transportasi untuk kegiatan operasional karyawan yang berbasis pemakaian harian? Rental Mobil Harian Korporat memungkinkan Anda menyewa mobil hanya ketika Anda membutuhkannya.",
          subProduct: [{
            title: "Durasi fleksibel",
            description: `Setiap pemesanan mobil dapat disesuaikan dengan durasi pemakaian yang Anda butuhkan. Minimal durasi pemakaian adalah 4 jam dan dapat ditambah sewaktu-waktu sesuai keperluan Anda.`
          }, {
            title: "Reservasi online",
            description: `Didukung aplikasi reservasi korporat yang bisa diunduh di smartphone, pemesanan mobil pun dapat dilakukan secara online dari mana saja. Karyawan yang sudah memiliki akses dapat melakukan login dan pemesanan secara mandiri melalui aplikasi korporat dari layar telepon pintar mereka.`
          }, {
            title: "Atur pemakaian",
            description: `Anda bisa mengatur sendiri siapa saja di perusahaan Anda yang memiliki akses melakukan pemesanan, untuk keperluan karyawan atau tamu penting. Tak hanya itu, Anda dapat memantau dan mengatur penggunaan mobil melalui dasbor khusus sehingga Anda memiliki kontrol penuh.`
          }, {
            title: "Analisis berkala",
            description: `Kami juga akan membantu Anda menganalisis pemakaian transportasi berdasarkan hasil report berkala untuk memudahkan Anda melakukan efisiensi biaya. Bahkan, Anda dapat mengatur biaya transportasi untuk setiap divisi di perusahaan Anda. Hubungi Customer Assistance Center 1500009 untuk informasi untuk informasi lebih detail.`
          }]
        }, {
          title: "Driver Service",
          icon: require('@images/png/bannerContent/drive-lease.png'),
          description: "Untuk menunjang kebutuhan transportasi perusahaan, TRAC juga menyediakan layanan sewa kendaraan dengan pengemudi yang telah terlatih secara profesional. Pengemudi TRAC wajib memiliki Surat Izin Mengemudi sesuai moda transportasi yang digunakan. Seluruh pengemudi kami telah melalui beragam pelatihan, di antaranya pelatihan defensive driving dari instruktur bersertifikat agar para pengemudi selalu mengedepankan keamanan berkendara. Semua pengemudi juga dibekali standar pelayanan TRAC. Anda bisa merasakan pelayanan yang ramah selama perjalanan.",
          subProduct: []
        }]
      }
    }, {
      title: "Rental Mobil Harian",
      description: "Kami menyediakan layanan Rental Mobil dan Airport Transfer untuk semua aktivitas harian Anda. Perjalanan dalam dan luar kota, liburan, bisnis, atau mobilitas sehari-hari kian mudah dengan reservasi online melalui situs web dan aplikasi TRAC. Pilih rental mobil dengan pengemudi terlatih kami atau kendarai sendiri mobil Anda. Mudah, aman, dan terjamin.",
      image: "https://trac.astra.co.id/static/media/CorporateLease.d9cde1c3.png",
      detail: {
        title: "Aktivitas Lancar, Perjalanan Tetap Nyaman",
        description: "Semua orang, termasuk Anda, berhak untuk menikmati layanan transportasi yang aman dan nyaman, termasuk untuk keperluan perjalanan jangka pendek. Kami menyediakan layanan sewa kendaraan harian untuk semua aktivitas Anda. Liburan, perjalanan bisnis di luar kota, maupun mobilitas sehari-hari menjadi lebih menyenangkan. Semua layanan kami terjamin aman, nyaman, dengan akses reservasi online yang dapat diakses kapan pun. Anda dapat melakukan pemesanan online melalui situs web atau aplikasi TRAC yang tersedia untuk pengguna Android dan iOS",
        product: [{
          title: "Rental Mobil Harian",
          icon: require('@images/png/bannerContent/car-lease.png'),
          description: "Ada kalanya Anda perlu melakukan perjalanan keluar kota, bahkan keluar pulau, bersama keluarga maupun untuk solo travelling. Atau, mobil Anda sedang perawatan ke bengkel, sedangkan Anda tetap butuh kendaraan yang senyaman mobil sendiri untuk mobilitas harian yang padat. Tenang saja, TRAC siap melayani beragam kebutuhan transportasi Anda setiap waktu. Anda bisa memilih sewa mobil dengan pengemudi (chauffeur) atau sewa mobil lepas kunci (self drive). Pilih layanan yang paling sesuai dengan kebutuhan Anda.",
          subProduct: [{
            title: "Sewa Mobil Lepas Kunci | Self Drive",
            description: "Bebas pergi ke mana pun dan bebas atur waktu perjalanan Anda dengan sewa mobil tanpa pengemudi. Tenang saja, keamanan Anda tetap terjaga dengan jaminan kondisi mobil prima dan asuransi perjalanan. Anda dapat menggunakan layanan ini dengan mendaftarkan diri menjadi member TRAC di outlet-outlet kami atau secara online di sini (hyperlink to member registration). Nikmati juga beragam keuntungan sebagai member TRAC"
          }, {
            title: "Sewa Mobil dengan Pengemudi | Chauffeur",
            description: "Ingin duduk santai menikmati perjalanan tanpa harus menyetir mobil sendiri? Manfaatkan sewa mobil dengan layanan pengemudi dari TRAC. Layanan ini sudah termasuk sewa mobil, layanan pengemudi, asuransi perjalanan, biaya bensin, tol, dan parkir. Pengemudi andal kami akan menjemput dan mengantarkan Anda sampai ke tujuan dengan mengedepankan ketepatan waktu juga layanan yang ramah dan profesional. Pilih paket sesuai kebutuhan Anda: 4 jam atau 12 jam."
          }]
        }, {
          title: "Airport Transfer",
          icon: require('@images/png/bannerContent/drive-lease.png'),
          description: `Ketika sampai di bandara, Anda tak perlu lagi repot mencari kendaraan, apalagi harus antre untuk menuju destinasi. TRAC siap menjemput Anda tepat waktu dan mengantarkan hingga tujuan. Layanan ini sudah termasuk sewa mobil, layanan pengemudi, asuransi perjalanan, biaya bensin, tol, dan parkir sehingga Anda tinggal duduk santai selama perjalanan.

Anda dapat memesan Airport Transfer secara online melalui situs web dan aplikasi TRAC atau secara offline dengan menghubungi Customer Assistance Center 1500009 maupun email ke rco.nasional@trac.astra.co.id. Mulai perjalanan mudah dan aman bersama TRAC, sekarang!`,
          subProduct: []
        }]
      }
    }, {
      title: "Sewa Bus",
      description: "Rencanakan perjalanan yang aman dan nyaman bersama sahabat, keluarga, atau kolega dengan layanan Sewa Bus dari TRAC. Liburan, arisan, reuni, trip kantor atau sekolah menjadi lebih menyenangkan. Reservasi pun mudah dengan pemesanan online via situs web dan aplikasi TRAC yang bisa diakses pengguna Android maupun iOS.",
      image: "https://trac.astra.co.id/static/media/bus_lease.51661d55.png",
      detail: {
        title: "SEWA BUS",
        description: `Dengan pengalaman melayani kebutuhan rental mobil seiring perkembangan waktu TRAC pun menghadirkan layanan sewa bus untuk melengkapi layanan yang ditawarkan. Layanan sewa Bus TRAC dapat menjadi pilihan untuk kebutuhan transportasi group atau rombongan baik untuk perjalanan di dalam ataupun luar kota, untuk kebutuhan korporasi maupun personal.

Sewa Bus Pariwisata
TRAC Bus dapat melayani beragam kebutuhan perjalanan mulai dari perjalanan wisata, acara keluarga, trip kantor/sekolah, dll.
Shuttle
Kami juga dapat melayani kebutuhan layanan shuttle baik untuk korporasi seperti antar jemput karyawan, shuttle residence, antar jemput sekolah, transportasi MICE, dll.

Setiap bus kami selalu dirawat dengan baik dan interior bus pun dibuat senyaman mungkin. Keamanan merupakan salah satu faktor yang menjadi perhatian kami oleh karena itu setiap seat dilengkapi dengan sabuk pengaman dan untuk kenyamanan penumpang beberapa bus dilengkapi dengan fasilitas wifi, LCD TV, toilet, perangkat karaoke dan pantry.

TRAC Bus menawarkan beragam variasi tipe bus mulai dari small bus, medium, big hingga luxury. Dan dengan pilihan konfigurasi jumlah kursi mulai dari 11 hingga 59 seats.

Keselamatan dan keamanan penumpang juga menjadi prioritas kami. Setiap armada TRAC Bus juga dilengkapi dengan palu pemecah kaca, pintu darurat, dan tabung pemadam kebakaran. Dan salah satu hal yang menjadi keunggulan kami adalah setiap pengemudi kami merupakan pengemudi yang terlatih dan sudah berpengalaman sehingga kenyamanan dan keselamatan penumpang lebih terjamin.

Layanan Sewa Bus TRAC saat ini berbasis di Jakarta dan Surabaya namun kemanapun tujuan Anda dapat kami layani.`,
        product: []
      }
    }, {
      title: "TRAC Fleet Management Solution",
      description: "Anda dapat mengelola kebutuhan transportasi perusahaan secara maksimal. Kini, kami menyediakan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things.",
      image: "https://trac.astra.co.id/static/media/AstraFMS.f5781b9c.jpg",
      detail: {
        title: "Sistem Transportasi Berbasis Teknologi",
        description: "Anda dapat mengelola kebutuhan transportasi perusahaan secara maksimal. Kini, kami menyediakan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things. Kami merancang ASTRA FMS untuk membantu Anda mengelola dan mengawasi transportasi dengan mudah. Efisiensi dari aspek transportasi pun dapat tercapai secara maksimal.",
        product: [{
          title: "TRAC Data",
          icon: require('@images/png/bannerContent/car-lease.png'),
          description: `Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.

TRAC Data akan membantu Anda untuk:

1. Melakukan penjadwalan perawatan kendaraan tepat waktu
2. Meningkatkan pengawasan terhadap aspek keamanan kendaraan
3. Memperoleh analisis tingkat produktivitas aset kendaraan
4. Memperbaiki kontrol aset kendaraan Anda
5. Meningkatkan pemakaian kendaraan yang lebih aman dan efisien
6. Mengetahui informasi kondisi kendaraan selama perjalanan`,
          subProduct: []
        }, {
          title: "TRAC Fleet",
          icon: require('@images/png/bannerContent/drive-lease.png'),
          description: `Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.

TRAC Data akan membantu Anda untuk:

1. Melakukan penjadwalan perawatan kendaraan tepat waktu
2. Meningkatkan pengawasan terhadap aspek keamanan kendaraan
3. Memperoleh analisis tingkat produktivitas aset kendaraan
4. Memperbaiki kontrol aset kendaraan Anda
5. Meningkatkan pemakaian kendaraan yang lebih aman dan efisien
6. Mengetahui informasi kondisi kendaraan selama perjalanan`,
          subProduct: []
        }, {
          title: "TRAC Smart",
          icon: require('@images/png/bannerContent/bus-lease.png'),
          description: `Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.

TRAC Data akan membantu Anda untuk:

1. Melakukan penjadwalan perawatan kendaraan tepat waktu
2. Meningkatkan pengawasan terhadap aspek keamanan kendaraan
3. Memperoleh analisis tingkat produktivitas aset kendaraan
4. Memperbaiki kontrol aset kendaraan Anda
5. Meningkatkan pemakaian kendaraan yang lebih aman dan efisien
6. Mengetahui informasi kondisi kendaraan selama perjalanan`,
          subProduct: []
        }]
      }
    }]
//     data: [{
//       title: "Corporate Lease",
//       description: "Sistem transportasi yang terkelola dengan baik dan efisien menjadi salah satu pendukung kesuksesan bisnis.",
//       image: "https://trac.astra.co.id/static/media/CorporateLease.bf13e284.jpg",
//       detail: {
//         title: "Kami Siap Mendukung Bisnis Anda",
//         description: "Sistem transportasi yang terkelola dengan baik dan efisien menjadi salah satu pendukung kesuksesan bisnis. Meski demikian, kami mengerti bahwa kebutuhan tiap perusahaan tak selalu sama sehingga memerlukan solusi transportasi yang dirancang secara khusus. Oleh karen itu, TRAC telah menyediakan beragam solusi transportasi berbasis kontrak dengan jangka waktu kerja sama minimal satu tahun. Kami siap mendukung bisnis Anda dengan jaminan layanan yang aman, nyaman, dan terukur.",
//         product: [{
//           title: "Car Lease",
//           icon: require('@images/png/bannerContent/car-lease.png'),
//           description: "Anda dapat memanfaatkan layanan sewa mobil yang pilihan armadanya bisa sesuai dengan kebutuhan bisnis. TRAC menyediakan beragam pilihan mobil, di antaranya sedan, MPV, SUV, juga compact. Tak hanya itu, kami juga siap melayani kebutuhan transportasi khusus untuk perusahaan yang aktivitas bisnisnya berada di remote area seperti industri pertambangan dan perkebunan. Apapun jenis bisnis Anda, TRAC siap melayani. Melengkapi layanan tersebut, TRAC juga menyediakan “Corporate App” dan “TRAC Syariah” khusus untuk kebutuhan bisnis.",
//           subProduct: [{
//             title: "Corporate App",
//             description: `Mengelola transportasi perusahaan menjadi lebih mudah dengan aplikasi berbasis teknologi digital dari TRAC. Karyawan Anda dapat melakukan pemesanan transportasi secara online, dari mana saja melalui smartphone.

// Tak hanya itu, Anda dapat memantau dan mengatur semua penggunaan kendaraan secara lengkap melalui dasbor khusus sehingga Anda memiliki kontrol penuh. Corporate App juga akan membantu Anda menganalisis pemakaian transportasi berdasarkan hasil report berkala. Anda pun lebih mudah melakukan cost efficiency.`
//           }, {
//             title: "TRAC Syariah",
//             description: `Layanan berbasis sistem syariah semakin menjadi kebutuhan. Bagi Anda yang juga memiliki kebutuhan ini, TRAC menyediakan pengelolaan transportasi dengan cara syariah yang mengedepankan transaksi tanpa riba. Seluruh aset dibiayai dan dijamin institusi keuangan syariah dan Anda tidak akan dikenakan denda pembatalan. TRAC Syariah hanya membebankan Ta’widh dan Gharamah yang nantinya disumbangkan untuk kebaikan masyarakat.

// Selain itu, TRAC Syariah menerapkan sistem pengembalian dana berdasarkan konsep pembagian risiko. Anda hanya membayar biaya perawatan yang masuk dalam deposit. Jika biaya yang digunakan tidak melebihi jumlah deposit, Anda akan mendapatkan kembali dana yang tak terpakai tersebut.
//             ,
// Melalui pengawasan dari divisi syariah yang beranggotakan Badan Penasihat Syariah (SAC), layanan TRAC Syariah konsisten menerapkan kaidah syariah yang berlaku. Kami merupakan perusahaan transportasi pertama yang memberikan layanan berbasis syariah secara resmi di Indonesia. Kami telah mendapatkan surat rekomendasi Nomor U-576/DSN-MUI/XI/2016 dari Dewan Syariah Nasional – Majelis Ulama Indonesia (DSN MUI).`
//           }]
//         }, {
//           title: "Driver Service",
//           icon: require('@images/png/bannerContent/drive-lease.png'),
//           description: "Untuk menunjang kebutuhan transportasi perusahaan, TRAC juga menyediakan layanan sewa kendaraan dengan pengemudi yang telah terlatih secara profesional. Pengemudi TRAC wajib memiliki Surat Izin Mengemudi sesuai moda transportasi yang digunakan. Seluruh pengemudi kami telah melalui beragam pelatihan, di antaranya pelatihan defensive driving dari instruktur bersertifikat agar para pengemudi selalu mengedepankan keamanan berkendara. Semua pengemudi juga dibekali standar pelayanan TRAC. Anda bisa merasakan pelayanan yang ramah selama perjalanan.",
//           subProduct: []
//         }, {
//           title: "Motorcycle Lease Service",
//           icon: require('@images/png/bannerContent/motor-lease.png'),
//           description: "Bagi banyak industri, moda transportasi roda dua menjadi faktor pendukung yang sangat vital. Oleh karena itu, TRAC juga menyediakan layanan sewa motor dengan jaminan kualitas unit yang prima. Layanan ini sangat tepat untuk bisnis Anda yang mengedepankan kecepatan dan ketepatan waktu di tengah-tengah kepadatan lalu lintas Ibu Kota. Anda pun tak perlu lagi repot mengelola dan melakukan perawatan unit, kami akan melakukannya untuk Anda.",
//           subProduct: []
//         }, {
//           title: "Bus Lease Service",
//           icon: require('@images/png/bannerContent/bus-lease.png'),
//           description: "Manfaatkan layanan sewa bus untuk mendukung kebutuhan transportasi perusahaan Anda, misalnya untuk antar-jemput karyawan atau transportasi shuttle bus di mall, bandara, dan area bisnis lainnya. Apapun kebutuhan Anda, kami siap melayani.",
//           subProduct: []
//         }]
//       }
//     }, {
//       title: "Personal Rental",
//       description: "Semua orang, termasuk Anda, berhak untuk menikmati layanan transportasi yang aman dan nyaman, termasuk untuk keperluan perjalanan jangka pendek.",
//       image: "https://trac.astra.co.id/static/media/CorporateLease.d9cde1c3.png",
//       detail: {
//         title: "Aktivitas Lancar, Perjalanan Tetap Nyaman",
//         description: "Semua orang, termasuk Anda, berhak untuk menikmati layanan transportasi yang aman dan nyaman, termasuk untuk keperluan perjalanan jangka pendek. Kami menyediakan layanan sewa kendaraan harian untuk semua aktivitas Anda. Liburan, perjalanan bisnis di luar kota, maupun acara bersama rombongan keluarga menjadi lebih menyenangkan. Semua layanan kami terjamin aman, nyaman, dengan akses reservasi yang mudah. Anda dapat melakukan reservasi secara online, via telpon, chat Whatsapp, maupun email. Semua serba mudah dan cepat.",
//         product: [{
//           title: "Daily Car Rental",
//           icon: require('@images/png/bannerContent/car-lease.png'),
//           description: "Ada kalanya Anda perlu melakukan perjalanan keluar kota, bahkan keluar pulau, bersama keluarga maupun untuk solo travelling. Atau, mobil Anda sedang perawatan ke bengkel, sedangkan Anda tetap butuh kendaraan yang senyaman mobil sendiri untuk mobilitas harian yang padat. Tenang saja, TRAC siap melayani beragam kebutuhan transportasi Anda setiap waktu. Anda bisa memilih sewa mobil dengan pengemudi (chauffeur) atau sewa mobil lepas kunci (self drive). Pilih layanan yang paling sesuai dengan kebutuhan Anda.",
//           subProduct: [{
//             title: "Sewa Mobil Lepas Kunci | Self Drive",
//             description: "Bebas pergi ke mana pun dan bebas atur waktu perjalanan Anda dengan sewa mobil tanpa pengemudi. Tenang saja, keamanan Anda tetap terjaga dengan jaminan kondisi mobil prima dan asuransi perjalanan. Anda dapat menggunakan layanan ini dengan mendaftarkan diri menjadi member TRAC di outlet-outlet kami atau secara online di sini (hyperlink to member registration). Nikmati juga beragam keuntungan sebagai member TRAC"
//           }, {
//             title: "Sewa Mobil dengan Pengemudi | Chauffeur",
//             description: "Ingin duduk santai menikmati perjalanan tanpa harus menyetir mobil sendiri? Manfaatkan sewa mobil dengan layanan pengemudi dari TRAC. Layanan ini sudah termasuk sewa mobil, layanan pengemudi, asuransi perjalanan, biaya bensin, tol, dan parkir. Pengemudi andal kami akan menjemput dan mengantarkan Anda sampai ke tujuan dengan mengedepankan ketepatan waktu juga layanan yang ramah dan profesional. Pilih paket sesuai kebutuhan Anda: 4 jam atau 12 jam."
//           }]
//         }, {
//           title: "Airport Transfer",
//           icon: require('@images/png/bannerContent/drive-lease.png'),
//           description: `Ketika sampai di bandara, Anda tak perlu lagi repot mencari kendaraan transportasi, apalagi harus antre. TRAC siap menjemput tepat waktu dan mengantarkan Anda hingga tujuan. Layanan ini sudah termasuk sewa mobil, layanan pengemudi, asuransi perjalanan, biaya bensin, tol, dan parkir sehingga Anda tinggal duduk santai selama perjalanan.

// Airport Transfer dapat Anda nikmati di 16 kota besar di Indonesia, yaitu Jakarta, Bali, Lombok, Semarang, Surabaya, Medan, Jambi, Pangkal Pinang, Palembang, Manado, Pontianak, Banjarmasin, Batam, Balikpapan, Solo, dan Yogyakarta.`,
//           subProduct: []
//         }, , {
//           title: "Bus Lease Service",
//           icon: require('@images/png/bannerContent/bus-lease.png'),
//           description: `Manfaatkan layanan sewa bus untuk mendukung kebutuhan transportasi perusahaan Anda, misalnya untuk antar-jemput karyawan atau transportasi shuttle bus di mall, bandara, dan area bisnis lainnya. Apapun kebutuhan Anda, kami siap melayani`,
//           subProduct: []
//         }]
//       }
//     }, {
//       title: "TRAC Fleet Management Solution",
//       description: "Anda dapat mengelola kebutuhan transportasi perusahaan secara maksimal. Kini, kami menyediakan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things.",
//       image: "https://trac.astra.co.id/static/media/AstraFMS.f5781b9c.jpg",
//       detail: {
//         title: "Sistem Transportasi Berbasis Teknologi",
//         description: "Anda dapat mengelola kebutuhan transportasi perusahaan secara maksimal. Kini, kami menyediakan sistem transportasi yang memadukan teknologi aplikasi berbasis cloud dan internet of things. Kami merancang ASTRA FMS untuk membantu Anda mengelola dan mengawasi transportasi dengan mudah. Efisiensi dari aspek transportasi pun dapat tercapai secara maksimal.",
//         product: [{
//           title: "TRAC Data",
//           icon: require('@images/png/bannerContent/car-lease.png'),
//           description: `Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.

// TRAC Data akan membantu Anda untuk:

// 1. Melakukan penjadwalan perawatan kendaraan tepat waktu
// 2. Meningkatkan pengawasan terhadap aspek keamanan kendaraan
// 3. Memperoleh analisis tingkat produktivitas aset kendaraan
// 4. Memperbaiki kontrol aset kendaraan Anda
// 5. Meningkatkan pemakaian kendaraan yang lebih aman dan efisien
// 6. Mengetahui informasi kondisi kendaraan selama perjalanan`,
//           subProduct: []
//         }, {
//           title: "TRAC Fleet",
//           icon: require('@images/png/bannerContent/drive-lease.png'),
//           description: `Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.

// TRAC Data akan membantu Anda untuk:

// 1. Melakukan penjadwalan perawatan kendaraan tepat waktu
// 2. Meningkatkan pengawasan terhadap aspek keamanan kendaraan
// 3. Memperoleh analisis tingkat produktivitas aset kendaraan
// 4. Memperbaiki kontrol aset kendaraan Anda
// 5. Meningkatkan pemakaian kendaraan yang lebih aman dan efisien
// 6. Mengetahui informasi kondisi kendaraan selama perjalanan`,
//           subProduct: []
//         }, {
//           title: "TRAC Smart",
//           icon: require('@images/png/bannerContent/bus-lease.png'),
//           description: `Anda memiliki aset kendaraan, tapi kesulitan mengelolanya? TRAC Data menjadi solusi tepat. Kami akan merangkum data penggunaan kendaraan Anda lalu membantu melakukan analisis data. Kemudian, kami akan mengirimkan hasil report secara berkala, yang dapat Anda gunakan sebagai dasar untuk melakukan pengelolaan aset yang lebih baik.

// TRAC Data akan membantu Anda untuk:

// 1. Melakukan penjadwalan perawatan kendaraan tepat waktu
// 2. Meningkatkan pengawasan terhadap aspek keamanan kendaraan
// 3. Memperoleh analisis tingkat produktivitas aset kendaraan
// 4. Memperbaiki kontrol aset kendaraan Anda
// 5. Meningkatkan pemakaian kendaraan yang lebih aman dan efisien
// 6. Mengetahui informasi kondisi kendaraan selama perjalanan`,
//           subProduct: []
//         }]
//       }
//     }]
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  _copy = (code) => {
    Clipboard.setString(code);
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderBannerPromo() {
    return (
      <View style={styles.bannerPromo}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <Image
            source={require('@images/png/bannerContent/bannerService.png')}
            style={{ height: 250, width: appMetrics.windowWidth }}
            resizeMode="stretch" />
        </View>
      </View>
    );
  }
  
 
  // ---------------------------------------------------
  _renderContentDetail(item, index) {
    return (      
      <View style={styles.promoDescription.wrapperbox} key={index}>
        <View style={styles.promoDescription.descriptionWrapper}>
          <Image
              style={{ marginRight: 10, width: '100%', height: 200 }}
              source={{ uri: item.image }}
              resizeMode="cover" />
        </View>

        <View style={{flexDirection: 'column'}}>
          <Text style={{marginHorizontal: 20, marginTop: 20, fontWeight: 'bold',}} size={20}>
            {item.title}
          </Text>
          <Text style={{marginHorizontal: 20, marginTop: 20,}} size={12} color={appVariables.colorGray}>
            {item.description}
          </Text>
          <TouchableOpacity
            style={{
              flex: 1,
              flexDirection: 'row',
              marginHorizontal:20,
              marginVertical: 20
            }}
            onPress={() => Actions.ProductServiceDetail({item:item})}>
            <View style={{
              flex: 3,
              flexDirection: 'row'}}>
              <Text
                size={12}
                color={appVariables.colorBlack}>
                READ MORE
              </Text>
            </View>
            <View style={{
              flex: 8,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems:'center'}}>
              <Icon
                name="arrow-right"
                size={12}
                color={appVariables.colorOrange} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <ImageBackground source={ require("@images/png/img-top-background.png") } style={{width: '100%'}}>
          <ImageHeader onBackPress={() => this.props.navigation.pop()} />        
          <View style={{marginHorizontal: 20, marginTop: 70, marginBottom: 50}}>
            <Text style={{fontSize: 40, color: 'white', fontWeight: 'bold'}}>Product And Services</Text>
          </View>
        </ImageBackground>
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          {this.state.data.map((item, index) => this._renderContentDetail(item, index) )}
        </ScrollView>
      </BaseContainer>
    );
  }
}
