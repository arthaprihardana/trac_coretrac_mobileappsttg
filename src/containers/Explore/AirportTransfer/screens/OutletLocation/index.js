/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 10:41:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 15:20:01
 */
import React, { Component } from 'react';
import { Content, Text,Thumbnail, List } from 'native-base';
import SvgIcon from '../../../../../utils/SvgIcon';
import { View, TouchableOpacity, Picker, StyleSheet, Dimensions } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { WebView } from 'react-native-webview';
import { Grid, Row, Col } from 'react-native-easy-grid';
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import RNPickerSelect from 'react-native-picker-select';
import Icon from '@airport-transfer-fragments/Icon';
import MainButton from "@airport-transfer-fragments/MainButton";
// import styles from "./style";
import { connect } from 'react-redux';
import Lang from '../../../../../assets/lang';

const { width, height } = Dimensions.get("window");

const branchList = [
  { label: 'SELECT BRANCH OR OUTLET', value: 'undefined' },
  { label: 'All', value: 'all' },
  { label: 'Branch', value: 'branch' },
  { label: 'Outlet', value: 'outlet' }
];

const provinceList = [
  { label: 'SELECT PROVINCE', value: 'undefined' },
  { label: "All", value: "all"},
  { label: "Bali", value: "Bali"},
  { label: "Banten", value: "Banten"},
  { label: "Jakarta", value: "Jakarta"},
  { label: "Jambi", value: "Jambi"},
  { label: "Jawa Barat", value: "Jabar"},
  { label: "Jawa Tengah", value: "Jateng"},
  { label: "Jawa Timur", value: "Jatim"},
  { label: "Yogyakarta", value: "DIY"},
  { label: "Kalimantan Barat", value: "Kalbar"},
  { label: "Kalimantan Selatan", value: "Kalsel"},
  { label: "Kalimantan Timur", value: "Kaltim"},
  { label: "Lampung", value: "Lampung"},
  { label: "Nusa Tenggara", value: "NTT"},
  { label: "Riau & Kepulauan Riau", value: "Kepri"},
  { label: "Sulawesi Selatan & Gorontalo", value: "Sulsel"},
  { label: "Sulawesi Tengah & Sulawesi Timur", value: "SulTim"},
  { label: "Sulawesi Tenggara", value: "SulTeng"},
  { label: "Sulawesi Utara", value: "Sulut"},
  { label: "Sumatra Barat", value: "Sumbar"},
  { label: "Sumatra Selatan & Bangka Belitung", value: "SumSel"},
  { label: "Sumatra Utara", value: "Sumut"}
];

class OutletLocation extends Component {
  constructor(props) {
    super(props);
    this.inputBranch = {
      branch: null,
    };
    this.inputProvince = {
      province: null,
    };

    this.state = {      
      branch: 'undefined',
      province: 'undefined'
    };
  }

  updateUser = (user) => {
    this.setState({ user: user })
  }

  // ---------------------------------------------------

  // _renderFilter() {     
  //   return (
  //     <View>
  //       <View style={{
  //         flex: 1,
  //         backgroundColor: '#FFFFFF',
  //       }}>
  //         <View style={{backgroundColor: '#12265B', padding: 20}}>
  //           <View style={{flexDirection: 'row', justifyContent: "space-between", borderBottomWidth: 1,
  //     borderBottomColor: "#D8D8D8" }} >
  //           <RNPickerSelect
  //             //placeholder={categoryplaceholder}
  //             items={branchList}
  //             onValueChange={value => {
  //               this.setState({
  //                 branch: value,
  //               });
  //             }}
  //             style={pickerStyle}
  //             value={this.state.branch}
  //             ref={el => {
  //               this.inputBranch.branch = el;
  //             }}
  //           />
  //           <Icon size={20} name={"caret-down"} color="#fff" style={{marginTop: 10}}/>
  //           </View>
  //           <View style={{flexDirection: 'row', justifyContent: "space-between", borderBottomWidth: 1,
  //     borderBottomColor: "#D8D8D8" , marginTop: 20}} >
  //           <RNPickerSelect
  //             //placeholder={subcategoryplaceholder}
  //             items={provinceList}
  //             onValueChange={value => {
  //               this.setState({
  //                 province: value,
  //               });
  //             }}
  //             style={pickerStyle}
  //             value={this.state.province}
  //             ref={el => {
  //               this.inputProvince.provinec = el;
  //             }}
  //           />
  //           <Icon size={20} name={"caret-down"} color="#fff" style={{marginTop: 10}}/>
  //           </View>

  //           <TouchableOpacity style={styles.couponCode.copyPromo}>
  //             <Text
  //               size={14}
  //               style={{color: "#fff"}}>
  //               SEARCH</Text>
  //           </TouchableOpacity>
  //         </View>
  //       </View>
  //     </View>
  //   );
  // }

  render() {

    return (
      <BaseContainer hasFooter={false}>
        <Header title={Lang.branchLocation[this.props.lang]}/>
        <View style={{ flex: 2}}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            region={{
              latitude: 106.8837832,
              longitude: -6.1500518,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}>
          </MapView>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ position: 'absolute', bottom: 0, left: 0, width: width, flexDirection: 'column', justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 10, backgroundColor: 'white', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
            <TouchableOpacity style={{ width: width, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ width: 30, height: 5, borderRadius: 2.5, backgroundColor: '#ccc' }} />
            </TouchableOpacity>
            <View style={{ width: (width) - 20, marginBottom: 10, marginTop: 30 }}>
              <TouchableOpacity style={{ borderWidth: 0.5, borderColor: '#ccc', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 40, borderRadius: 3, paddingHorizontal: 10 }}>
                <Text style={{ fontSize: 10, color: '#ccc', width: '80%' }}>Select Branch</Text>
                <Icon name="chevron-down" size={12} color="#ccc" />
              </TouchableOpacity>
            </View>
            <View style={{ width: (width) - 20, marginBottom: 10}}>
              <TouchableOpacity style={{ borderWidth: 0.5, borderColor: '#ccc', flexDirection: 'row',  justifyContent: 'space-between', alignItems: 'center', height: 40, borderRadius: 3, paddingHorizontal: 10 }}>
                <Text style={{ fontSize: 10, color: '#ccc', width: '80%' }}>Location</Text>
                <Icon name="chevron-down" size={12} color="#ccc" />
              </TouchableOpacity>
            </View>
            <View>
              <MainButton
                label={ "Search" }
                isLoading={false}
                onPress={ () => {}}
                rounded
              />
            </View>
          </View>
        </View>
      </BaseContainer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 300,
    width: width,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return {lang};
}

export default connect(mapStateToProps, {})(OutletLocation);