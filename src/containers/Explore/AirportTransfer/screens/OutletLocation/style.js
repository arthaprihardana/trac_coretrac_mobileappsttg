import {
	appVariables,
} from "@variables";

export default {
	
	couponCode:{		
		copyPromo:{ 
			flex: 1,
			flexDirection: "column",
			justifyContent: "flex-end",
			alignItems: "center",
			borderRadius: 5,
			backgroundColor: appVariables.colorOrange,
			paddingVertical: 10,
			paddingHorizontal: 10,
			marginTop: 30,
		},
	}
}