import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  progressBar:{
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    circleGray:{
      flex:-1,
      width:15,
      height:15,
      borderRadius:15/2,
      backgroundColor:"#D8D8D8",
    },
    circleOrange:{
      flex:-1,
      width:15,
      height:15,
      borderRadius:15/2,
      backgroundColor:"#F27C21",
    },
    rectangleOrange:{
      flex:-1,
      width:(appMetrics.screenWidth-20)/4,
      height:7,
      backgroundColor:"#F27C21",
    },
    rectangleGray:{
      flex:-1,
      width:(appMetrics.screenWidth-20)/4,
      height:7,
      backgroundColor:"#D8D8D8",
    },
  },
  circleWrapper:{
    flex: -1,
    flexDirection: 'column',
    alignItems: "center",
  },
  rectangleWrapper:{
    flex: 1,
    flexDirection: 'column',
    paddingTop: 5,
  },
  circleWhite:{
    flex:-1,
    width:20,
    height:20,
    borderRadius:20/2,
    borderWidth:1,
    backgroundColor:appVariables.colorWhite,
    borderColor:appVariables.colorGray,
  },
}