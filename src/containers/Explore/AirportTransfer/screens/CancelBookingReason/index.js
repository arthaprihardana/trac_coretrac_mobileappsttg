/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 21:22:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 23:45:24
 */
import React, { Component } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity
} from "react-native";
import {
  appVariables,
} from "@variables";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/id";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import NumberHelper from "@airport-transfer-helpers/Number";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import { postCancelReservation, getCancelReason } from "../../../../../actions"

class CancelBooking2 extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      activeReasonIndex: -1,
      reasonId: null,
      reason: null,
      readyToSubmit: false,
      data: []
    }
  }

  componentDidMount = () => {
    // let x = _.filter(this.props.products, { MsProductId: this.props.item.BusinessUnitId });
    this.props.getCancelReason({ BusinessUnitId: this.props.item.BusinessUnitId, token: this.props.login.token });
  }
  
  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.reason !== prevProps.reason) {
      return { reason: this.props.reason }
    } 
    if(this.props.cancel !== prevProps.cancel) {
      return { cancel: this.props.cancel }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.reason !== undefined) {
        this.setState({
          data: snapshot.reason
        });
      }
      if(snapshot.cancel !== undefined) {
        Actions.push('CancelBookingRefund');
      }
    }
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  goToCancelBookingRefund(){
    // this.props.postCancelReservation({
    //   FormData: {
    //     CancellationReasonId: this.state.reasonId,
    //     CancellationReason: this.state.reason,
    //     ReservationId: this.props.item.ReservationId
    //   },
    //   token: this.props.login.token
    // });
    Actions.push('CancelBookingRefund', { 
      item: this.props.item,
      reasonId: this.state.reasonId,
      reason: this.state.reason
    });
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderCancellationDetail() {
    let x = _.filter(this.props.time, { MsProductId: this.props.item.details[0].MsProductId });
    let CancelationTime = x[0].configuration_adjustment_retail_prices[0].CancelationTime;
    var toMin = moment.duration(CancelationTime).asMinutes();
    var setHour = (new moment(this.props.item.details[0].StartDate, "YYYY-MM-DD HH:mm:ss")).subtract(toMin, "minutes");
    var now = moment().unix();
    var targetDate = setHour.unix();
    var diffTime = targetDate - now;
    var duration = moment.duration(diffTime*1000, 'milliseconds');
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingTop: 20
        // paddingVertical: 20,
        // paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <Text
            color="#1A3681"
            size={14}>
            Cancellation is Free
            </Text>
          <Text
            color={appVariables.colorBlack}
            style={{ paddingTop: 10 }}
            size={12}>
            For: {duration.days() >= 0 ? duration.days() : 0} days {duration.hours() >= 0 ? duration.hours() : 0} hours {duration.minutes() >= 0 ? duration.minutes() : 0} minutes
          </Text>
        </View>
      </View>
    )
  }

   // ---------------------------------------------------
   _renderCancellationProgressBar() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        // paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          {this._renderCancellationProgressBar1()}
          {this._renderCancellationProgressBar2()}
          {this._renderCancellationProgressBar3()}
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar1() {
    let x = _.filter(this.props.time, { MsProductId: this.props.item.details[0].MsProductId });
    let ChargeCancelation = x[0].configuration_adjustment_retail_prices[0].ChargeCancelation;
    let CancelationFee = (parseInt(this.props.item.TotalPrice) * (parseInt(ChargeCancelation) / 100));
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          {/* <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={10}>
              FREE
            </Text>
          </View> */}

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={10}>
              Cancellation Fees
            </Text>
            <Text
              color={appVariables.colorGray}
              size={10}>
              Rp { NumberHelper.format(CancelationFee) }
            </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              style={{ paddingTop: 10 }}
              size={10}>
              Non-Refundable
            </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar2() {
    return (
      <View style={styles.progressBar}>
        <View style={{
          flex: 1,
          flexDirection: 'row'}}>
          <View style={styles.circleWrapper}>
            <View style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: "center",
            }}>
              <View style={styles.progressBar.circleOrange}>
              </View>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleOrange}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleOrange}>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleOrange}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleOrange}>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleGray}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleGray}>
            </View>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar3() {
    let x = _.filter(this.props.time, { MsProductId: this.props.item.details[0].MsProductId });
    let CancelationTime = x[0].configuration_adjustment_retail_prices[0].CancelationTime;
    var toMin = moment.duration(CancelationTime).asMinutes();
    var setHour = (new moment(this.props.item.details[0].StartDate, "YYYY-MM-DD HH:mm:ss")).subtract(toMin, "minutes");
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={10}>
              { moment().isSameOrBefore(setHour.format("YYYY-MM-DD HH:mm:ss")) ? "Today": null }
            </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={10}>
              {setHour.locale('en').format("lll")}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderCarPreference() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        // paddingVertical: 10,
        // paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          paddingVertical: 10,
        }}>
          <Text
            size={14}
            color={appVariables.colorBlack}>
            Tell us your reason for cancelling
          </Text>
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          paddingVertical: 10,
        }}>
          <Text
            size={12}
            color={appVariables.colorGray}>
            This is needed to complete your cancellation
          </Text>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderCarCard(label, index, value) {
    return (
      <View key={index} style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        // paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
          }}>
            <RoundedCheck
              value={this.state.activeReasonIndex === index}
              onPress={() => this.setState({
                reasonId: value,
                reason: label,
                activeReasonIndex: index,
                readyToSubmit: true
              })}>
            </RoundedCheck>
          </View>

          <TouchableOpacity style={{
            flex: 10,
            flexDirection: 'column'}}
            onPress={() => this.setState({
              reasonId: value,
              reason: label,
              activeReasonIndex: index,
              readyToSubmit: true
            })}>
            <Text
              color={appVariables.colorBlack}
              size={12}>
              {label}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Cancel Booking"}
          onBackPress={() => this.props.navigation.pop()}
        />
        <ScrollView style={{
          flex: 1,
          backgroundColor: appVariables.colorWhite,
          // paddingVertical: 20,
          paddingHorizontal: 20
        }}>
          {this._renderCancellationDetail()}
          {this._renderCancellationProgressBar()}
          <View style={{
            flex: 1,
            flexDirection: 'row',
            borderColor: appVariables.colorGray,
            borderBottomWidth: 1,
          }}>
          </View>
          {this._renderCarPreference()}
          { this.state.data.length > 0 && _.map(this.state.data, (v, i) => this._renderCarCard(v.Description, i, v.ReasonId) ) }
        </ScrollView>
        <MainButton
          isLoading={this.props.loading}
          label={"CANCEL BOOKING"}
          onPress={() => this.state.readyToSubmit ? this.goToCancelBookingRefund() : {}}
          disabled={!this.state.readyToSubmit}
        />
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ vehicleAttribute, adjustmentTime, authentication, cancelReservation, productService, cancelReason }) => {
  const { vehicle } = vehicleAttribute;
  const { time } = adjustmentTime;
  const { login } = authentication;
  const { loading, cancel } = cancelReservation;
  const { products, selectedProduct } = productService;
  const { reason } = cancelReason;
  const loadingReason = cancelReason.loading;
  return { vehicle, time, login, cancel, loading, products, selectedProduct, reason, loadingReason }
}

export default connect(mapStateToProps, {
  postCancelReservation,
  getCancelReason
})(CancelBooking2)