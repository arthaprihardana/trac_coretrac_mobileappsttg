/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-04 17:42:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 15:57:23
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  Alert
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import MainButton from "@airport-transfer-fragments/MainButton";
import SocialMediaLogin from "@airport-transfer-fragments/SocialMediaLogin";
import AuthContainer from "@airport-transfer-fragments/AuthContainer";
import Lang from '../../../../../assets/lang';

import TnCModal from "@airport-transfer-modals/TermsAndCondition";

import styles from "./style";

import { postRegister } from "../../../../../actions";
import { REGEX_PASSWORD, REGEX_EMAIL } from "../../../../../constant";
import { Actions } from "react-native-router-flux";

class Register extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        email: "",
        password: "",
        confirm_password: "",
      },
      errorEmail: null,
      errorPassword: null,
      errorConfirmPassword: null,
      aggreeTnc: false,
      isTnCModalVisible:  false,
      readyToSubmit: false
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.register !== prevProps.register) {
      return { register: this.props.register }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.register !== undefined) {
        if(typeof snapshot.register === "object") {
          Alert.alert(
            'Information',
            `Register Success, Check email ${snapshot.register.user_login.EmailPersonal} to confirm Registration`,
            [
              {text: 'OK', onPress: () => {
                Actions.pop();
              }},
            ],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Information',
            `${snapshot.register}`,
            [
              {text: 'OK', onPress: () => {}},
            ],
            {cancelable: false},
          );
        }
      }
    }
  }
  
// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;
    this.setState({
      form
    });
    switch (name) {
      case "email":
        this.setState({ errorEmail: REGEX_EMAIL.test(value) ? null : "Email didn't valid" })
        break;
      case "password":
        this.setState({ errorPassword: REGEX_PASSWORD.test(value) ? null : Lang.passwordPlaceholder[this.props.lang] });
        break;
      case "confirm_password":
        this.setState({ errorConfirmPassword: this.state.form.password !== value ? Lang.passwordNotMatch[this.props.lang] : null });
        break;
      default:
        break;
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  onValidate = async current => {
    let except = [];
    except.push(current);
    let o = _.omit(this.state.form, except);
    let a = await _.findKey(o, val => {
      if(_.isEmpty(val)) { return true }
    });
    return a;
  }

  register() {
    const data = {
      EmailPersonal: this.state.form.email,
      Password: this.state.form.password
    }

    this.props.postRegister(data)

    // if (Object.keys(this.props.navigation.getParam("navData", {})).length == 0) {
    //   this.props.navigation.pop();

    //   return true;
    // }
    // this.props.navigation.push("PassangerDetail", {
    //   navData: this.props.navigation.getParam("navData", {}),
    // });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderLoginButton() {
    return (
      <View style={ styles.form.bottom.register.container }>
        <Text>
          {Lang.alreadyHaveAccount[this.props.lang]}
        </Text>

        <TouchableOpacity
          style={ styles.form.bottom.register.button.container }
          onPress={ () => this.props.navigation.pop() }
        >
          <Text color={ "navy" }>
            {Lang.loginHere[this.props.lang]}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

// ---------------------------------------------------

  _renderForm() {
    return (
      <View>
        <FloatingTextInput
          refs={ref => this._email = ref}
          label={ Lang.email[this.props.lang] }
          onChangeText={ (value) => {
            this.onValidate("email").then(val => {
              this.onChangeForm("email", value) 
              this.setState({ readyToSubmit: value !== "" && val === undefined && this.state.errorEmail === null ? true : false })
            });
          }}
          value={ this.state.form.email }
          keyboardType={ "email-address" }
          note={this.state.errorEmail}
          returnKeyType="next"
          onSubmitEditing={() => this._password.ref.focus()}
          textContentType="emailAddress"
          // onEndEditing={() => this.onEndEditing("email")}
        />

        <FloatingTextInput
          refs={ref => this._password = ref}
          label={ "Password" }
          onChangeText={ (value) => {
            this.onValidate("password").then(val => {
              this.onChangeForm("password", value) 
              this.setState({ readyToSubmit: value !== "" && val === undefined && this.state.errorPassword === nulll ? true : false })
            });
          }}
          value={ this.state.form.password }
          secureTextEntry
          note={this.state.errorPassword}
          returnKeyType="next"
          onSubmitEditing={() => this._confirmPassword.ref.focus()}
          textContentType="password"
          // onEndEditing={() => this.onEndEditing("password")}
        />

        <FloatingTextInput
          refs={ref => this._confirmPassword = ref}
          label={ Lang.confirm_password[this.props.lang] }
          onChangeText={ (value) => {
            this.onValidate("confirm_password").then(val => {
              this.onChangeForm("confirm_password", value) 
              this.setState({ readyToSubmit: value !== "" && val === undefined && this.state.errorConfirmPassword === null ? true : false })
            });
          }}
          value={ this.state.form.confirm_password }
          secureTextEntry
          note={this.state.errorConfirmPassword}
          returnKeyType="next"
          onSubmitEditing={() => this.state.readyToSubmit ? this.register() : {} }
          textContentType="password"
          // onEndEditing={() => this.onEndEditing("confirm_password")}
        />

        <View style={ styles.form.extra.container }>
          {/* <RoundedCheck
            value={ this.state.aggreeTnc }
            onPress={ (value) => this.setState({aggreeTnc: !this.state.aggreeTnc}) }
          >
            <Text>
              I agree with the 
            </Text>
            <TouchableOpacity
              style={{
                paddingLeft: 2,
              }}
              onPress={ () => this.setState({isTnCModalVisible: true}) }
            >
              <Text color={ "navy" }>
                Terms and Conditions
              </Text>
            </TouchableOpacity>
          </RoundedCheck> */}
        </View>

        <MainButton
          label={ Lang.register[this.props.lang] }
          isLoading={this.props.loading}
          onPress={ () => this.state.readyToSubmit ? this.register() : {} }
          rounded
          disabled={!this.state.readyToSubmit}
        />

        <View style={ styles.form.bottom.container }>
          <Text>
            {Lang.orContinueWith[this.props.lang]}
          </Text>

          <SocialMediaLogin/>

          { this._renderLoginButton() }
        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderTnCModal() {
    return (
      <TnCModal
        isVisible={ this.state.isTnCModalVisible }
        onClosePress={ () => this.setState({isTnCModalVisible: false}) }
        onAgreePress={ () => this.setState({aggreeTnc: true, isTnCModalVisible: false}) }
      >
        { "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
          "\n\n" +
        "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque." }
      </TnCModal>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <AuthContainer 
        { ... this.props }
        title={ Lang.register[this.props.lang] }
      >

        { this._renderForm() }   

        { this._renderTnCModal() }

      </AuthContainer>
    );
  }
}

const mapStateToProps = ({ authentication, language }) => {
  const { loading, register } = authentication;
  const { lang } = language;
  return { loading, register, lang };
}

export default connect(mapStateToProps, {
  postRegister
})(Register)
