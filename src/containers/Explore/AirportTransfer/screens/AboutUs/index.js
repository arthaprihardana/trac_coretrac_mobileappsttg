import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Clipboard,
  ImageBackground,
} from "react-native";
import { H2, p } from 'native-base';
import {
  appVariables,
  appMetrics
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import ImageHeader from "@airport-transfer-fragments/ImageHeader";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";

export default class AboutUs extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  _copy = (code) => {
    Clipboard.setString(code);
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderBannerPromo() {
    return (
      <View style={styles.bannerPromo}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <Image
            source={require('@images/png/messageDetail/bannerPromo.png')}
            style={{ height: 250, width: appMetrics.windowWidth }}
            resizeMode="stretch" />
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderAboutDescription() {
    return (
      <View style={styles.promoDescription.wrapper}>
        <View style={{
          flex: 1,
          flexDirection: "row",
          paddingVertical: 20,
        }}>
        <View style={styles.promoDescription.descriptionWrapper}>
          <Text style={{fontWeight: 'bold',}} size={16} color={appVariables.colorGray}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus
            nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce
            ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla
            pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.
          </Text>
        </View>
        </View>
        <View style={styles.promoDescription.descriptionWrapper}>
          <Text size={16} color={appVariables.colorGray}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus
            nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce
            ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla
            pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.
         </Text>
        </View>
      </View>
    );
  }
 
  // ---------------------------------------------------
  _renderAboutDetail() {
    return (      
      <View style={styles.promoDescription.wrapper}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <View style={styles.promoDescription.descriptionWrapper}>
            <Text style={{fontWeight: 'bold',}} size={30} color={appVariables.colorGray}>
            Corporate Lease
            </Text>
          </View>
        </View>
        <View style={styles.promoDescription.descriptionWrapper}>
          <Image
              source={require('@images/png/aboutImage.png')}
              style={{ height: 250}}
              resizeMode="cover" />
        </View>
        <View style={styles.promoDescription.descriptionWrapper}>
          <Text size={16} color={appVariables.colorGray}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus
            nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce
            ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla
            pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.
         </Text>
        </View>
      </View>
    );
  }
  
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <ImageBackground source={ require("@images/png/img-top-background.png") } style={{width: '100%'}}>
        <ImageHeader
          //title={"About Us"}
          onBackPress={() => this.props.navigation.pop()} />
        {/* <ImageBackground source={ require("@images/png/img-top-background.png") } style={{width: '100%'}}> */}
        <View style={{marginHorizontal: 20, marginTop: 70, marginBottom: 50}}>
        <H2 style={{color: 'white'}}>About TRAC-Astra Rent</H2>
        <Text style={{color: 'white'}}>Menjadi Besar Bersama Anda</Text>
        </View>
        </ImageBackground>
        <ScrollView style={{
          flex: 1,
        }}>
          {this._renderAboutDescription()}
          {this._renderAboutDetail()}
          {this._renderAboutDetail()}
          {this._renderAboutDetail()}
        </ScrollView>
      </BaseContainer>
    );
  }
}
