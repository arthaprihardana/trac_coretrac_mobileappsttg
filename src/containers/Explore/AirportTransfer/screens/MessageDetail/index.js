/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-05 21:09:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 19:30:38
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Clipboard,
} from "react-native";
import {
  appVariables,
  appMetrics
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import { Actions } from "react-native-router-flux";

export default class MessageDetail extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  _copy = (code) => {
    Clipboard.setString(code);
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderBannerPromo() {
    return (
      <View style={styles.bannerPromo}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <Image
            source={require('@images/png/messageDetail/bannerPromo.png')}
            style={{ height: 250, width: appMetrics.windowWidth }}
            resizeMode="stretch" />
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderPromoDescription() {
    return (
      <View style={styles.promoDescription.wrapper}>
        <View style={{
          flex: 1,
          flexDirection: "row",
          paddingVertical: 20,
        }}>
          <Text size={30}>
            Promo HUT RI
         </Text>
        </View>
        <View style={styles.promoDescription.descriptionWrapper}>
          <Text size={16} color={appVariables.colorGray}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus
            nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce
            ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla
            pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.
         </Text>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderCopyPromo() {
    return (
      <View style={styles.copyPromo.wrapper}>
        <View style={styles.copyPromo.wrapperBackground}>
          {/* Period Promo */}
          <View style={{
            flex: 1,
            flexDirection: 'row',
          }}>
            <View style={{
              flex: 1,
              flexDirection: "column"
            }}>
              <Text size={10} color={appVariables.colorWhite}>
                Period Promo
            </Text>
            </View>
            <View style={{
              flex: 3,
              flexDirection: "column"
            }}>
              <Text size={10} color={appVariables.colorWhite}>
                16 - 19 Okt 2018
            </Text>
            </View>
          </View>
          {/* Coupon Code */}
          <View style={{
            flex: 1,
            flexDirection: 'row',
            paddingVertical: 10,
          }}>
            <View style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
            }}>
              <Text size={14} color={appVariables.colorWhite}>
                Kode Kupon
            </Text>
            </View>
            <View style={styles.copyPromo.trackPromoWrapper}>
              <View style={styles.copyPromo.trackPromoBackground}></View>
              <Text
                size={14}
                color={appVariables.colorWhite}
                style={{ paddingVertical: 15 }}>
                TRACKPROMO200K</Text>
            </View>
          </View>
          {/* promo guidance */}
          <View style={{
            flex: 1,
            flexDirection: 'row',
            paddingVertical: 10,
          }}>
            <View style={{
              flex: 1,
              flexDirection: "column"
            }}>
              <Text size={10} color={appVariables.colorWhite}>
                Enter this coupon code on the payment page or click Book with promo below
            </Text>
            </View>
          </View>
          {/* copy promo button */}
          <View>
            <TouchableOpacity style={styles.copyPromo.copyPromoButton}
              onPress={this._copy('TRACKPROMO200K')}>
              <Text
                size={14}
                color={appVariables.colorWhite}>
                COPY PROMO</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderOtherPromoTitle() {
    return (
      <View style={styles.otherPromo.title}>
        <Text
          size={20}>
          Promo Lainnya
        </Text>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderOtherPromoCard() {
    return (
      <View style={styles.otherPromoCard.wrapper}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <Image
            source={require('@images/png/messageDetail/promo1.png')}
            style={{ flex: -1 }}
            resizeMode="stretch" />
        </View>
        <View style={styles.otherPromoCard.title}>
          <Text
            size={16}>
            Promo HUT RI Dapatkan Voucher Diskon
        </Text>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderOtherPromos() {
    var otherPromos = [];
    var otherPromosAmount = 2;
    for (let i = 0; i < otherPromosAmount; i++) {
      otherPromos.push(
        <View key={i}>
          {this._renderOtherPromoCard()}
        </View>
      )
    }
    return (
      <View>
        {otherPromos}
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={this.props.item.data.title}
          onBackPress={() => Actions.pop() } />
          <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
            <View style={styles.copyPromo.wrapper}>
              <View style={styles.copyPromo.wrapperBackground}>
                {/* Period Promo */}
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      Reservation Number
                    </Text>
                  </View>
                  <View style={{ flex: 3, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      { this.props.item.data.body.ReservationId }
                    </Text>
                  </View>
                </View>
                {this.props.item.data.body.category === "detail" && 
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      Status
                    </Text>
                  </View>
                  <View style={{ flex: 3, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      {this.props.item.data.title}
                    </Text>
                  </View>
                </View> }
                {this.props.item.data.body.category === "list" && 
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      Virtual Account Number
                    </Text>
                  </View>
                  <View style={{ flex: 3, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      { this.props.item.data.body.VANumber }
                    </Text>
                  </View>
                </View> }
                {this.props.item.data.body.category === "list" && 
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      Product
                    </Text>
                  </View>
                  <View style={{ flex: 3, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      { this.props.item.data.body.MsProductId }
                    </Text>
                  </View>
                </View> }
                {this.props.item.data.body.category === "list" && 
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      Service Type
                    </Text>
                  </View>
                  <View style={{ flex: 3, flexDirection: "column" }}>
                    <Text size={10} color={appVariables.colorWhite}>
                      { this.props.item.data.body.MsProductServiceId }
                    </Text>
                  </View>
                </View> }
              </View>
            </View>
            {/* {this._renderBannerPromo()}
            {this._renderPromoDescription()}
            {this._renderCopyPromo()}
            <View style={styles.lineSeparator}>
            </View>
            {this._renderOtherPromoTitle()}
            {this._renderOtherPromos()} */}
          </ScrollView>
      </BaseContainer>
    );
  }
}
