/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-07 20:59:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 23:25:17
 */
import moment from "moment";
import "moment/locale/id";
import Lang from "../../../../../../assets/lang";

export default (lang) => {
  return [
    {
      type: "from_city", 
      label: Lang.pickUpLocation[lang], 
      modalTitle: Lang.selectCity[lang], 
      placeholder: Lang.selectYourCity[lang], 
      backgroundColor: "#1F419B",
      hasSearchIcon: true,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value || !formsState.from_location.value ? null : formsState.from_location.options.filter((option) => option.value == formsState.from_location.value)[0].label,
      optionsModifier: null,
      onSuccessChain: "from_location", 
      onCancelChain: null, 
    },
    {
      type: "from_location", 
      label: null, 
      modalTitle: Lang.findYourAddress[lang], 
      placeholder: null, 
      backgroundColor: null,
      hasSearchIcon: false,
      isHidden: true,
      isRequired: true,
      isDisabled: false,
      // modifier: null,
      modifier: (value, formsState) => {
        console.log('value ==>', value);
        console.log('formsState ==>', formsState);
      },
      optionsModifier: null,
      // optionsModifier: (options, formsState) => options.filter((option) => option.parent == formsState.from_city.value),
      onSuccessChain: null, 
      onCancelChain: "from_city", 
    },
    {
      type: "to_location", 
      label: Lang.destinationBus[lang], 
      modalTitle: Lang.destinationBus[lang], 
      placeholder: Lang.destinationBus[lang], 
      backgroundColor: "#1A3681",
      hasSearchIcon: true,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: null,
      modifier: (value, formsState) => {
        if(!value || !formsState.to_location.value) {
          return null;
        } else {
          let filter = formsState.to_location.options.filter((option) => option.value == formsState.to_location.value);
          if(filter.length > 0) {
            return filter[0].label;
          } else {
            return null
          }
        }
      },
      optionsModifier: null,
      onSuccessChain: null,
      onCancelChain: null,
    },
    {
      type: "pickup_schedule", 
      label: Lang.dateAndTime[lang], 
      modalTitle: Lang.dateAndTime[lang], 
      placeholder: Lang.dateAndTime[lang], 
      backgroundColor: "#12265B",
      hasSearchIcon: false,
      isRequired: true,
      isHidden: false,
      isDisabled: false,
      modifier: (value, formsState) => (!value.startDate || !value.startTime) ? null :
        (new moment(value.startDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).format("ddd, DD MMM YYYY [at] hh:mm A") + "\n" +
        (new moment(value.endDate + " " + value.startTime, "DD-MM-YYYY hh:mm")).format("ddd, DD MMM YYYY"),
      optionsModifier: null,
      onSuccessChain: null,
      onCancelChain: null,
    },
    {
      type: "passanger_count", 
      label: Lang.passengers[lang], 
      modalTitle: Lang.passengers[lang], 
      placeholder: Lang.inputAmount[lang], 
      backgroundColor: "#1B3373",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: null,
      optionsModifier: null,
      onSuccessChain: null,
      onCancelChain: null,
    },
  ];
}