/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 15:39:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 16:55:49
 */
import React, { Component } from "react";
import styles from "./style";
import _ from "lodash";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/id";

import mainForm from "./forms/main";

// import cityDummies from "@airport-transfer-dummies/cities";
// import areaDummies from "@airport-transfer-dummies/areas";

import MainFormContainer from "@airport-transfer-fragments/MainFormContainer";
import { getCityCoverage, getDistanceMatrixFromGoogleForBus, setActualBranchForBus, getBusZone, setTempForm, getMasterCarType, getExtras, setRequestStockList, getPlaceDetailForToLocationBus } from "../../../../../actions";
import { Actions } from "react-native-router-flux";
import { UAS_TIMEBASE } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

const INITIAL_FORMS_STATE = {
  from_city: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  from_location: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  to_location: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  pickup_schedule: {
    value: {
      date: null,
      time: null,
    },
    modalType: "rangeSchedule",
    isModalOpened: false,
    options: [],
  },
  passanger_count: {
    value: null,
    modalType: "amount",
    isModalOpened: false,
    options: [],
  },
};


class BusRentalForm extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
    this.state = {
      BusinessUnitId: product[0].BusinessUnitId,
      // forms: JSON.parse(JSON.stringify(INITIAL_FORMS_STATE)),
      forms: {
        from_city: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        from_location: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        to_location: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        pickup_schedule: {
          value: {
            date: null,
            time: null,
          },
          modalType: "rangeSchedule",
          isModalOpened: false,
          options: [],
        },
        passanger_count: {
          value: null,
          modalType: "amount",
          isModalOpened: false,
          options: [],
        },
      },

      missingForms: [],
    };
  }

// ---------------------------------------------------

  componentDidMount() {
    this.getOptions();
    let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
    this.props.getCityCoverage({ 
      BusinessUnitId: product[0].BusinessUnitId,
      MsProductId: product[0].MsProductId
    });
  }

// ---------------------------------------------------

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.cityCoverage !== prevProps.cityCoverage) {
      return { cityCoverage: this.props.cityCoverage }
    }
    if(this.props.address !== prevProps.address) {
      return { address: this.props.address }
    }
    if(this.props.placeDetail !== prevProps.placeDetail) {
      return { coords: this.props.placeDetail }
    }
    if(this.props.distanceMatrixForBus !== prevProps.distanceMatrixForBus) {
      // if(this.props.distanceMatrixForBus !== null) {
        return { nearDistance: this.props.distanceMatrixForBus }
      // }
    }
    if(this.props.distanceMatrix !== prevProps.distanceMatrix) {
      if(this.props.distanceMatrix !== null) {
        return { distance: this.props.distanceMatrix }
      }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.cityCoverage !== undefined) {
        this.manageCityCoverage(snapshot.cityCoverage)
      }
      if(snapshot.address !== undefined) {
        if(this.state.forms.from_location.isModalOpened) {
          this.manageFromLocation(snapshot.address);
        } else if(this.state.forms.to_location.isModalOpened) {
          this.manageToLocation(snapshot.address);
        }
      }
      if(snapshot.coords !== undefined) {
        const forms = this.state.forms;
        if(this.props.isToLocationBus) {
          const toLocation = forms.to_location;
          toLocation.coords = snapshot.coords;
        } else {
          const fromLocation = forms.from_location;
          fromLocation.coords = snapshot.coords;
        }
        this.props.getDistanceMatrixFromGoogleForBus({
          origins: _.map(this.props.branch, v => v.Latitude+","+v.Longitude).join("|"),
          destinations: `${snapshot.coords.lat},${snapshot.coords.lng}`
        });
        this.setState({
          forms
        });
      }
      if(snapshot.nearDistance !== undefined) {
        if(!this.props.isToLocationBus) {
          let branch = _.map(this.props.branch, (v, k) => {
            v.distance = snapshot.nearDistance[k];
            return v;
          });
          let getBranch = _.minBy(branch, v => v.distance);
          this.props.setActualBranchForBus(getBranch.BranchId);
          // this.props.getPlaceDetailForToLocationBus(false);
        }
      }
      if(snapshot.distance !== undefined) {
        this.props.getBusZone({
          BusinessUnitId: this.state.BusinessUnitId,
          MsProductId: this.props.selectedProduct,
          Distance: snapshot.distance
        });
      }
    }
    if (!this.state.isInitiated) {
      this.getOptions();
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onFormsStateChange(forms) {    
    this.setState({
      forms,
      missingForms: [],
    });
  }

// ---------------------------------------------------

  onChainingModalTriggered(key, chainKey, isForwardChain = false) {
    const forms = this.state.forms;
    
    forms[chainKey].isModalOpened = true;
    
    if (isForwardChain) {
      forms[chainKey].value = INITIAL_FORMS_STATE[chainKey].value;
    }

    this.setState({
      forms,
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN METHODS
// ---------------------------------------------------

  manageCityCoverage = cities => {
    const forms = this.state.forms;
    const fromCity = forms.from_city;
    fromCity.options = _.map(cities, (v, k) => {
      return {
        label: _.startCase(_.lowerCase(v.MsCityName)),
        value: _.startCase(_.lowerCase(v.MsCityName)),
        BranchId: v.BranchId,
        CityId: v.CityId
      }
    });
    this.setState({
      forms
    });
  }

  manageFromLocation = address => {
    const forms = this.state.forms;
    const fromLocation = forms.from_location;
    fromLocation.options = address;
    this.setState({
      forms
    })
  }

  manageToLocation = address => {
    const forms = this.state.forms;
    const toLocation = forms.to_location;
    toLocation.options = address;
    this.setState({
      forms
    })
  }

  getOptions() {
    const forms = this.state.forms;

    // const fromCity = forms.from_city;
    // fromCity.options = cityDummies;

    // const fromLocation = forms.from_location;
    // fromLocation.options = areaDummies;

    // const toLocation = forms.to_location;
    // toLocation.options = areaDummies;
    
    this.setState({
      forms,
      isInitiated: true,
    });
  }

// ---------------------------------------------------

  getForm() {
    const forms = this.state.forms;
    let formConfig = mainForm(this.props.lang);

    return formConfig;
  }

// ---------------------------------------------------

  getInvalidForms(formTemplate, formData) {
    const missingForms = [];
    formTemplate.map((form) => {
      if (form.isRequired) {
        if (!formData[form.type].value) {
          missingForms.push(form.type);
        } else if (typeof formData[form.type].value == "object") {
          for (x in formData[form.type].value) {
            if (!formData[form.type].value[x]) {
              missingForms.push(form.type);

              break;
            }
          }
        }
      }
    });

    return missingForms;
  }

// ---------------------------------------------------

  submit() {
    let forms = [];
    let stacks = this.state.formStacks;
    let data = {};
    let missingForms = [];
    let navTripsData = [];

    data.trips = {};
    data.trips.trip1 = {};
    forms = this.getForm();
    forms.map((form, index) => {
      if (form.type != "type") {
        data.trips.trip1[form.type] = this.state.forms[form.type].value;
      }
    });

    missingForms = this.getInvalidForms(forms, this.state.forms);

    if (missingForms.length > 0) {
      this.setState({
        missingForms,
      });

      return;
    }

    navTripsData = [
      {
        from: this.state.forms.from_location.value,
        fromLabel: _.filter(this.state.forms.from_location.options, { value: this.state.forms.from_location.value })[0].label,
        to: this.state.forms.to_location.value,
        toLabel: _.filter(this.state.forms.to_location.options, { value: this.state.forms.to_location.value })[0].label,
        start: this.state.forms.pickup_schedule.value.startDate,
        end: this.state.forms.pickup_schedule.value.endDate,
      },
    ];

    let city = _.filter(this.state.forms.from_city.options, { value: this.state.forms.from_city.value });
    let startDate = (new moment(this.state.forms.pickup_schedule.value.startDate + " " + this.state.forms.pickup_schedule.value.startTime, "DD-MM-YYYY hh:mm"))
    let endDate = (new moment(this.state.forms.pickup_schedule.value.endDate + " 22:00:00", "DD-MM-YYYY hh:mm"))
    let totalDay = moment.duration(endDate.diff(startDate)).asDays();
    this.props.setTempForm(this.state.forms);
    this.props.getMasterCarType(this.state.BusinessUnitId);
    this.props.getExtras({
      BusinessUnitId: this.state.BusinessUnitId, 
      BranchId: this.props.actualBranch
    });
    let requestStock = {
      BusinessUnitId: this.state.BusinessUnitId,
      BranchId: this.props.actualBranch,
      StartDate: startDate.toISOString(),
      EndDate: endDate.toISOString(),
      IsWithDriver: 1, // 0 = self driver 1 = pakai driver
      RentalDuration: _.ceil(totalDay + 1),
      RentalPackage: 1,
      Uom: 'day',
      ServiceTypeId: UAS_TIMEBASE,  // Rental Time Based
      ValidateAttribute: 1,
      ValidateContract: 1,
      // ProductServiceId: this.state.forms.type.value,  // buat exception pada saat request
      CityId: city[0].CityId  // buat exception pada saat request
    };
    this.props.setRequestStockList(requestStock);
    this.props.getPlaceDetailForToLocationBus(false);
    Actions.push("BusList", {
      navData: {
        submittedData: data,
        viewData: {
          type: "",
          title: Lang.busRental[this.props.lang],
          tripIndex: 0,
          addedList: [],
          trips: navTripsData.filter((item) => !!item),
        },
      },
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <MainFormContainer
        title={ Lang.busRental[this.props.lang] }
        onBackPressed={ () => this.props.navigation.pop() }
        forms={ this.getForm() }
        missingForms={ this.state.missingForms }
        formsState={ this.state.forms }
        onSubmitPressed={ this.state.stackType == "return" && this.state.formStacks.length === 1 ? null : () => this.submit() }
        onFormsStateChange={ (forms) => this.onFormsStateChange(forms) }
        onChainingModalTriggered={ (key, chainKey, isForwardChain) => this.onChainingModalTriggered(key, chainKey, isForwardChain) }
      />
    );
  }
};

const mapStateToProps = ({ cities, productService, addressFromGoogle, masterBranch, language }) => {
  const { products, selectedProduct } = productService;
  const { cityCoverage } = cities;
  const { address, placeDetail, distanceMatrixForBus, distanceMatrix, isToLocationBus } = addressFromGoogle;
  const { branch, actualBranch } = masterBranch;
  const { lang } = language;
  return { cityCoverage, products, selectedProduct, address, placeDetail, branch, distanceMatrixForBus, distanceMatrix, actualBranch, isToLocationBus, lang }
}

export default connect(mapStateToProps, {
  getCityCoverage,
  getDistanceMatrixFromGoogleForBus,
  setActualBranchForBus,
  getBusZone,
  setTempForm,
  getMasterCarType,
  getExtras,
  setRequestStockList,
  getPlaceDetailForToLocationBus
})(BusRentalForm);