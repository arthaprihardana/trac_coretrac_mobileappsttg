/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-06 18:44:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 13:34:37
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ListView,
  Clipboard,
  ImageBackground,
  FlatList,
  Alert,
} from "react-native";
import { H2, p, ListItem } from 'native-base';
import _ from "lodash";
import {
  appVariables,
  appMetrics
} from "@variables";
import { connect } from 'react-redux';

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import ImageHeader from "@airport-transfer-fragments/ImageHeader";
import Header from "@airport-transfer-fragments/Header";
import PromoDummies from "@airport-transfer-dummies/promos";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import NewsDetail from "../NewsDetail";

class Promo extends Component {
  
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props)
  {
    super(props);
    this.feature = [{id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}, {id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}];
    this.guidance = [{id: '1' , value: 'guidance in January'}, {id: '1' , value: 'guidance in February'}, {id: '1' , value: 'guidance in March'}, {id: '1' , value: 'guidance in April'}, {id: '1' , value: 'guidance in May'}, {id: '1' , value: 'guidance in June'}, {id: '1' , value: 'guidance in July'}, {id: '1' , value: 'guidance in August'}, {id: '1' , value: 'guidance in September'}, {id: '1' , value: 'guidance in October'}, {id: '1' , value: 'guidance in November'}, {id: '1' , value: 'guidance in December'}, {id: '1' , value: 'guidance in January'}, {id: '1' , value: 'guidance in February'}, {id: '1' , value: 'guidance in March'}, {id: '1' , value: 'guidance in April'}, {id: '1' , value: 'guidance in May'}, {id: '1' , value: 'guidance in June'}, {id: '1' , value: 'guidance in July'}, {id: '1' , value: 'guidance in August'}, {id: '1' , value: 'guidance in September'}, {id: '1' , value: 'guidance in October'}, {id: '1' , value: 'guidance in November'}, {id: '1' , value: 'guidance in December'}];
    this.news = [{id: '1' , value: 'news in January'}, {id: '1' , value: 'news in February'}, {id: '1' , value: 'news in March'}, {id: '1' , value: 'news in April'}, {id: '1' , value: 'news in May'}, {id: '1' , value: 'news in June'}, {id: '1' , value: 'news in July'}, {id: '1' , value: 'news in August'}, {id: '1' , value: 'news in September'}, {id: '1' , value: 'news in October'}, {id: '1' , value: 'news in November'}, {id: '1' , value: 'news in December'}, {id: '1' , value: 'news in January'}, {id: '1' , value: 'news in February'}, {id: '1' , value: 'news in March'}, {id: '1' , value: 'news in April'}, {id: '1' , value: 'news in May'}, {id: '1' , value: 'news in June'}, {id: '1' , value: 'news in July'}, {id: '1' , value: 'news in August'}, {id: '1' , value: 'news in September'}, {id: '1' , value: 'news in October'}, {id: '1' , value: 'news in November'}, {id: '1' , value: 'news in December'}];
    this.event = [{id: '1' , value: 'event in January'}, {id: '1' , value: 'event in February'}, {id: '1' , value: 'event in March'}, {id: '1' , value: 'event in April'}, {id: '1' , value: 'event in May'}, {id: '1' , value: 'event in June'}, {id: '1' , value: 'event in July'}, {id: '1' , value: 'event in August'}, {id: '1' , value: 'event in September'}, {id: '1' , value: 'event in October'}, {id: '1' , value: 'event in November'}, {id: '1' , value: 'event in December'}, {id: '1' , value: 'event in January'}, {id: '1' , value: 'event in February'}, {id: '1' , value: 'event in March'}, {id: '1' , value: 'event in April'}, {id: '1' , value: 'event in May'}, {id: '1' , value: 'event in June'}, {id: '1' , value: 'event in July'}, {id: '1' , value: 'event in August'}, {id: '1' , value: 'event in September'}, {id: '1' , value: 'event in October'}, {id: '1' , value: 'event in November'}, {id: '1' , value: 'event in December'}];
    this.state = { 
      btnOne : 3,
      btnTwo : 0,
      btnThre :  0,
      btnFour :  0,
      datacontent : this.feature,//[{id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}, {id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}],
      btnobj : [
        {label:"Aturan Privasi", id:1},
        {label:"Token TRAC", id:2},
        {label:"Cookie", id:3}
      ],
    };
    this.onPress = this.onPress.bind(this);
    
  }
  
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  onPress = (txt) => {
    switch(txt) {
      case '1':
        this._removeActive();
        this.setState({btnOne : 3, datacontent : this.feature});
        break;
      
      case '2':
        this._removeActive();
        this.setState({btnTwo : 3, datacontent : this.guidance});
        break;
 
      case '3':
        this._removeActive();
        this.setState({btnThree : 3, datacontent : this.news});
        break;
 
      case '4':
        this._removeActive();
        this.setState({btnFour : 3, datacontent : this.event});
        break;
      }
  }

  _removeActive = ()=> {
    this.setState({
      btnOne : 0, btnTwo : 0, btnThree : 0, btnFour : 0,
    });
  }
  
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  
  // ---------------------------------------------------
  _renderContent(item, key) {
    return (          
      <View style={{ width: "100%", backgroundColor: 'white'}} key={key}>  
        <View style={styles.otherPromoCard.wrapper}>
          <View style={{ width: '100%', height: 250, flexDirection: "row"}}>
            <Image source={{ uri: item.ImageThumbnail }} style={{ width: '100%', height: 250 }} resizeMode="contain" />
          </View>
          <View style={styles.otherPromoCard.title}>
            <View style={{ flex: 1, flexDirection: "column" }}>
              <Text size={26}>{item.Title}</Text>
              {/* <Text
                style={{marginTop: 30, marginBottom: 10}}
                size={12}>
                Periode Promo 
              </Text>
              <View style={styles.copyPromo.trackPromoWrapperTime}>
                <View style={styles.copyPromo.trackPromoTime}></View>
                <Text
                  size={14}
                  color={appVariables.colorWhite}
                  style={{ paddingVertical: 15 }}>
                  16 - 19 Okt 2018</Text>
              </View> */}
              <View style={{marginVertical: 20}}>
                <TouchableOpacity style={styles.copyPromo.copyDetailButton} onPress={() => Actions.push("PromoDetail", { item: item }) }>
                  <Text size={14}>SEE DETAIL</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
 

 
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    let sample = _.sampleSize(this.props.promo, 1);
    return (
      <BaseContainer>
        <ImageBackground source={{ uri: sample[0].ImageThumbnail }} style={{resizeMode: 'cover', width:'100%', height: 200}}>
          <ImageHeader onBackPress={() => this.props.navigation.pop()} />
          <View style={{marginHorizontal: 20, marginTop: 70, marginBottom: 50}} />
        </ImageBackground>
        <View style={{
            paddingHorizontal: 20,
            flexDirection: 'row',
            justifyContent: "space-evenly",
            borderBottomColor: "#D8D8D8", borderBottomWidth: 1
          }}>
          <TouchableOpacity style={{paddingHorizontal:10, flexDirection: 'row', alignItems:"flex-end", borderBottomColor: "#F47D00", borderBottomWidth: this.state.btnOne, flexGrow: 1, padding: 20}} onPress={() => this.onPress('1')} >
            <Image source={require('@images/png/bannerContent/icoAll.png')} 
              style={{marginRight: 10}}             
              resizeMode="contain" />
            <Text style={{fontSize: 10}}>All Promo</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity style={{paddingHorizontal:10, flexDirection: 'row', alignItems:"flex-end", borderBottomColor: "#F47D00", borderBottomWidth: this.state.btnTwo, flexGrow: 1, padding: 20}} onPress={() => this.onPress('2')} >
            <Image source={require('@images/png/bannerContent/icoCar.png')}     
              style={{marginRight: 10}}          
              resizeMode="cover" />
            <Text style={{fontSize: 10}}>Car Rental</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{paddingHorizontal:10, flexDirection: 'row', alignItems:"flex-end", borderBottomColor: "#F47D00", borderBottomWidth: this.state.btnThree, flexGrow: 1, padding: 20}} onPress={() => this.onPress('3')} >
            <Image source={require('@images/png/bannerContent/icoAirport.png')}   
              style={{marginRight: 10}}            
              resizeMode="cover" />
            <Text style={{fontSize: 10}}>Airport Transfer</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{paddingHorizontal:10, flexDirection: 'row', alignItems:"flex-end", borderBottomColor: "#F47D00", borderBottomWidth: this.state.btnThree, flexGrow: 1, padding: 20}} onPress={() => this.onPress('3')} >
            <Image source={require('@images/png/bannerContent/icoAirport.png')}   
              style={{marginRight: 10}}            
              resizeMode="cover" />
            <Text style={{fontSize: 10}}>Bus Rental</Text>
          </TouchableOpacity> */}
        </View>
        {/* <ScrollView >
          <View style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {
            this.state.datacontent.map(( item, key ) =>
            (
              this._renderContent(item.value, key)
            ))          
          }
          </View>
        </ScrollView> */}
        <FlatList
          data={this.props.promo}
          keyExtractor={(item, index) => item.Id.toString()}
          renderItem={({ item, index }) => this._renderContent(item, index) }
        />
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ discountPromo }) => {
  const { loading, promo } = discountPromo;
  return { loading, promo }
}

export default connect(mapStateToProps, {})(Promo)