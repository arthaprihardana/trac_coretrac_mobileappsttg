import {
	appVariables,
} from "@variables";

export default {
	myVoucher: {
		wrapper: {
			flex: 1,
			flexDirection: "column",
			paddingVertical: 20,
			paddingHorizontal: 20,
		},
	},
	periodPromo: {
		wrapper: {
			flex: 1,
			flexDirection: "column",
			paddingVertical: 20,
			paddingLeft: 20,
			paddingRight: 0,
			backgroundColor: "#F3F4F5",
		},
		textWrapper: {
			flex: 1,
			flexDirection: "column",
			alignItems: "center",
			justifyContent: "center",
			paddingVertical: 5,
		},
	},
	couponCode:{
		wrapper:{
			flex: 1,
			flexDirection: "column",
			paddingVertical: 20,
			backgroundColor: "#2246A8",
		},
		dashedLine:{
			flex: 1,
			borderWidth: 1,
			borderColor: appVariables.colorWhite,
			borderRadius: 5,
			borderStyle: 'dashed',
		},
		buttonWrapper:{
			flex: 1,
			flexDirection: "column",
			paddingHorizontal: 20,
			backgroundColor: "#2246A8",
		},
		couponCodeLabelWrapper:{
			flex: 1,
			flexDirection: "column",
			justifyContent: "flex-end",
			alignItems: "center",
			marginBottom: 5,
		},
		couponCodeLabelStyle:{
			backgroundColor: appVariables.colorWhite,
			opacity: 0.1,
			position: "absolute",
			left: 0,
			top: 0,
			width: '100%',
			height: 45,
			borderRadius: 5,
		},
		copyPromo:{
			flex: 1,
			flexDirection: "column",
			justifyContent: "flex-end",
			alignItems: "center",
			borderRadius: 5,
			backgroundColor: appVariables.colorOrange,
			paddingVertical: 10,
			paddingHorizontal: 10,
		},
	},
}