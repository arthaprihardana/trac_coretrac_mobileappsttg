import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  form: {
    topSectionContainer: {
      flex: -1,
      flexDirection: "row",
      justifyContent: 'space-between',
      flexWrap: "wrap",
      paddingLeft: 20,
      paddingVertical: 10,
      borderBottomWidth: 1,
      borderColor: "#dddddd",
    },

    bottomSectionContainer: {
      paddingVertical: 10,
      paddingHorizontal: 20,
    },

    bank: {
      container: {
        flex: -1,
        width: (appMetrics.screenWidth - 65) / 2,
        // width: '45%',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginRight: 20,
        marginVertical: 10,
        backgroundColor: appVariables.colorWhite,
        shadowOffset:{  width: 0,  height: 0,  },
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowRadius: 7
      },

      image: {
        container: {
          flex: 1,
          height: 30,
          justifyContent: "center"
        },
      },
    },

    agreement: {
      container: {
        paddingVertical: 20,
      },
    },
  },
}