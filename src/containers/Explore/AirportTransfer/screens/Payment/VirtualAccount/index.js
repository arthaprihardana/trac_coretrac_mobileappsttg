/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 15:03:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 14:50:09
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert
} from "react-native";
import { appVariables, appMetrics } from "@variables";
import { connect } from "react-redux";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import RedeemForm from "@airport-transfer-fragments/RedeemForm";
import MainButton from "@airport-transfer-fragments/MainButton";

import TnCModal from "@airport-transfer-modals/TermsAndCondition";

import BankData from '@airport-transfer-configs/data/Bank';

import styles from "./style";

import { getBank, getValidatePromo, checkPromoValid, setTotalPriceAfterDiscount } from "../../../../../../actions";
import Lang from '../../../../../../assets/lang';

class VirtualAccount extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        activeBankIndex: -1,
        promoCode: "",
      },
      paymentChannel: null,
      isTncAgreed: false,
      isTnCModalVisible: false,
      isPromoValid: false
    };
  }

  componentDidMount = () => {
    this.props.getBank({ BusinessUnitId: this.props.requestStock.BusinessUnitId })
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.discount !== prevProps.discount) {
      return {discount: this.props.discount};
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.discount) {
        if(!_.isArray(snapshot.discount)) {
          Alert.alert(
            'Information',
            'Congratulation, promo code is valid. Enjoy promo code from us.',
            [
              {text: 'OK', onPress: () => {}},
            ],
            {cancelable: false},
          );
          this.props.checkPromoValid(true);
          let summaryData = this.props.navData.summaryData;
          let disc = snapshot.discount;
          let price = [];
          _.map(summaryData.list, item => {
            if(disc.is_all_vehicle === 1) { // all vehicle
              switch (disc.type_value) {
                case "nominal":
                  var priceAfterDiscount = 0;
                  priceAfterDiscount = this.handlePrice(item, disc, summaryData, "nominal");
                  price.push(priceAfterDiscount);
                  break;
                default:  // persentasi
                  var priceAfterDiscount = 0;
                  priceAfterDiscount = this.handlePrice(item, disc, summaryData, "percentage");
                  price.push(priceAfterDiscount);
                  break;
              }
            } else {  // selected vehicle
              let vehicle = disc.vehicle_id;
              let filterVehicle = _.filter(vehicle, v => v.vehicle_id === item.vehicle.vehicleTypeId);
              let filterVehicleNoDiscount = _.filter(vehicle, v => v.vehicle_id !== item.vehicle.vehicleTypeId);
              if(filterVehicle.length > 0) {
                _.map(filterVehicle, v => {
                  if(v.vehicle_id === item.vehicle.vehicleTypeId) {
                    switch (disc.type_value) {
                      case "nominal":
                        var priceAfterDiscount = 0;
                        priceAfterDiscount = this.handlePrice(item, disc, summaryData, "nominal");
                        price.push(priceAfterDiscount);
                        break;
                      default:  // persentasi
                        var priceAfterDiscount = 0; 
                        priceAfterDiscount = this.handlePrice(item, disc, summaryData, "percentage");
                        price.push(priceAfterDiscount);
                        break;
                    }
                  }
                })
              }
              if(filterVehicleNoDiscount.length > 0) {
                _.map(filterVehicleNoDiscount, v => {
                  if(v.vehicle_id === item.vehicle.vehicleTypeId) {
                    var p = 0;
                    p = this.handlePrice(item, 0, summaryData);
                    price.push(p);
                  }
                })
              }
            }
          });
          let totalPrice = _.reduce(price, (sum, n) => sum + n, 0);
          this.props.setTotalPriceAfterDiscount(totalPrice);
        } else {
          Alert.alert(
            'Information',
            'Your promo code isn\'t valid',
            [
              {text: 'OK', onPress: () => {}},
            ],
            {cancelable: false},
          );
        }
      }
    }
  }

  handlePrice = (item, disc, summaryData, type_value) => {
    var price = 0;
    if(disc !== 0) {
      if(type_value === "nominal") {
        price = (item.vehicle.rentInfo.basePrice - parseInt(disc.value)) * summaryData.days;
      } else {
        price = (item.vehicle.rentInfo.basePrice - (item.vehicle.rentInfo.basePrice * (parseInt(disc.value)/100))) * summaryData.days;
      }
    } else {
      price = item.vehicle.rentInfo.basePrice * summaryData.days;
    }
    _.map(item.vehicle.extraItems, v => {
      switch (v.ValueType) {
        case "boolean":
          if(v.amounts) {
            price += parseInt(v.price)
          }
          break;
        default:
          price += parseInt(v.price) * v.amounts;
          break;
      }
    });
    return price;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderBankOption(code, image, index) {
    return (
      <View
        key={ index }
        style={ styles.form.bank.container }
      >
        <RoundedCheck
          value={ this.state.form.activeBankIndex === index }
          onPress={ () => {
            this.onChangeForm("activeBankIndex", index) 
            this.setState({ paymentChannel: code })
          }}
        >
          <View style={ styles.form.bank.image.container }>
            <Image
              source={{ uri: image }}
              style={{ height: 30, width: 100 }}
              resizeMode="contain"
            />
          </View>
        </RoundedCheck>
      </View>
    );
  }

// ---------------------------------------------------

  _renderTopForm() {
    return (
      <View style={ styles.form.topSectionContainer }>

        { this.props.bank ? this.props.bank.map((item, index) => {
          return this._renderBankOption(item.PaymentChannel, item.Images, index);
        }) : this.props.loadingBank && <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="small" color="#F27D20" />
        </View> }

      </View>
    );
  }

// ---------------------------------------------------

  _renderBottomForm() {
    return (
      <View style={ styles.form.bottomSectionContainer}>

        <RedeemForm
          isLoading={this.props.loadingPromo}
          // isPromoValid={this.state.isPromoValid}
          onApply={ (value) => {
            this.onChangeForm("promoCode", value);
            let prm = {
              PromoCode: value,
              business_unit_id: this.props.requestStock.BusinessUnitId,
              branch_id: this.props.requestStock.BranchId,
            };
            if(this.props.isLogin) {
              prm.user_id = this.props.login.Data.Id;
            }
            this.props.getValidatePromo(prm);
          }}
          onRemove={ () => {
            this.props.checkPromoValid(false);
            // this.setState({
            //   isPromoValid: false
            // })
          }}
        />
        
        <RoundedCheck
          style={ styles.form.agreement.container }
          value={ this.state.isTncAgreed }
          onPress={ () => this.setState({isTncAgreed: !this.state.isTncAgreed}) }
        >
          <View style={{ flexDirection: "row", width: '80%' }}>
            <Text>{Lang.IUnderstandThe[this.props.lang]} </Text>

            <TouchableOpacity onPress={ () => this.setState({isTnCModalVisible: true}) }>
              <Text color={ "navy" }>
                {Lang.termsAndConditions[this.props.lang]}
              </Text>
            </TouchableOpacity>
          </View>
        </RoundedCheck>
        
        <MainButton
          label={ Lang.makePayment[this.props.lang] }
          onPress={ () => !this.state.isTncAgreed || this.props.isLoading ? {} : this.props.onSelect(this.state.paymentChannel) }
          rounded
          disabled={!this.state.isTncAgreed || this.state.paymentChannel === null}
          isLoading={this.props.isLoading}
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderTnCModal() {
    return (
      <TnCModal
        isVisible={ this.state.isTnCModalVisible }
        onClosePress={ () => this.setState({isTnCModalVisible: false}) }
        onAgreePress={ () => this.setState({isTncAgreed: true, isTnCModalVisible: false}) }>
        {`1.Anda berhak memilih metode pembayaran yang telah Kami sediakan ketika melakukan pemesanan layanan secara online, yaitu menggunakan kartu kredit atau melalui transfer rekening Virtual Account (VA) atas nama PT Serasi Autoraya. Pilihan ini akan muncul pada proses “Pembayaran” setelah Anda melengkapi detail order dan data pribadi

2.Untuk metode pembayaran melalui VA, Anda dapat memilih bank rekanan kami yang telah terdaftar di website TRAC. Lalu, Anda akan mendapatkan nomor VA.

3.Pembayaran transaksi menggunakan VA tidak dikenakan biaya pelayanan.

4.Pembayaran dengan VA hanya berlaku untuk satu tagihan terbaru dan satu nomor VA hanya berlaku untuk satu tagihan

5.Anda wajib melakukan pembayaran terlebih dulu sesuai jumlah transaksi sebelum tanggal jatuh tempo yang tertera, melalui beragam channel pembayaran seperti mobile banking, internet banking, ATM, atau teller. Setelah pembayaran terkonfirmasi, Anda akan mendapatkan nomor reservasi, detail pemesanan, dan verifikasi melalui email yang terdaftar di akun TRAC Anda

6.Selain pembayaran via Virtual Account, Anda dapat memilih metode pembayaran dengan kartu kredit. Anda bisa memasukkan kartu kredit baru atau menggunakan salah satu kartu kredit yang telah terdaftar pada akun Anda. Namun, TRAC hanya menerima pembayaran transaksi via kartu kredit berlogo Visa dan Mastercard.

7.Saat memilih metode pembayaran ini, Anda akan dibawa ke laman Token TRAC. Fasilitas ini kami sediakan agar Anda dapat melakukan proses pembayaran dengan menggunakan kartu kredit secara aman, mudah, dan cepat. Fasilitas ini juga dilengkapi dengan sistem keamanan berlapis yang terintegrasi dengan payment gateway

8.Pembayaran dengan kartu kredit ini akan terverifikasi secara otomatis sehingga Anda dapat langsung menerima nomor reservasi, detail pemesanan, dan verifikasi melalui email yang terdaftar di akun TRAC Anda

9.Jika transaksi kartu kredit tidak berhasil, pastikan kartu kredit Anda berlogo Visa atau Mastercard dan pastikan limit kartu kredit Anda mencukupi. Jika kartu kredit tetap tidak bisa digunakan, silakan hubungi bank penerbit kartu kredit tersebut`}
      </TnCModal>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View>
        { this._renderTopForm() }

        { this._renderBottomForm() }

        { this._renderTnCModal() }
      </View>
    );
  }
}

const mapStateToProps = ({ masterBank, stockList, discountPromo, authentication, language }) => {
  const loadingBank = masterBank.loading;
  const loadingPromo = discountPromo.loading;
  const { bank } = masterBank;
  const { requestStock } = stockList;
  const { discount } = discountPromo;
  const { isLogin, login } = authentication;
  const { lang } = language;
  return { loadingBank, bank, requestStock, loadingPromo, discount, isLogin, login, lang };
}

export default connect(mapStateToProps, { 
  getBank,
  getValidatePromo,
  checkPromoValid,
  setTotalPriceAfterDiscount
})(VirtualAccount);