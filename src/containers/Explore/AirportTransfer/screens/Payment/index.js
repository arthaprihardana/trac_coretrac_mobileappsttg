/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 14:59:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-24 00:31:13
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Alert
} from "react-native";
import { appVariables, appMetrics } from "@variables";
import moment from "moment";
import "moment/locale/id";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import Header from "@airport-transfer-fragments/Header";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";

import CreditCardTabContent from "./CreditCard";
import VirtualAccountTabContent from "./VirtualAccount";
import VirtualAccountSelectedTabContent from "./VirtualAccountSelected";

import styles from "./style";
import { connect } from "react-redux";
import { saveReservation, getVirtualAccount, setRequestStockList, setTempStock, setTempStockComplete, setTempForm, setTempPersonalDetail, setNotes, checkPromoValid, getUserProfile, setNewUser } from "../../../../../actions";
import { SONotCreated, CAR_RENTAL, AIRPORT_TRANSFER, BUS_RENTAL, SELF_DRIVE, REGEX_VALID_URL } from "../../../../../constant";
import Lang from '../../../../../assets/lang';
import { Actions } from "react-native-router-flux";

class Payment extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      activetabIndex: 0,
      paymentChannel: null,
      isVirtualAccountSelected: false,
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.reservation !== prevProps.reservation) {
      return { reservation: this.props.reservation }
    }
    if(this.props.va !== prevProps.va) {
      return { va: this.props.va }
    }
    if(this.props.user !== prevProps.user) {
      return { user: this.props.user }
    }
    return null;    
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.reservation !== undefined) {
        if(snapshot.reservation !== "") {
          let params = {
            StatusInvoice: SONotCreated,
            CustomerId: snapshot.reservation.CustomerId,
            CustomerName: snapshot.reservation.CustomerName,
            PICUserInvoice: snapshot.reservation.PIC,
            PICUserInvoiceEmail: this.props.login.Data.EmailPersonal,
            SendMail: 0,
            PriceTotalInvoice: snapshot.reservation.TotalPrice,
            BusinessUnitId: snapshot.reservation.BusinessUnitId,
            PICUserInvoiceName: this.props.login.Data.FirstName !== null ? `${this.props.login.Data.FirstName} ${this.props.login.Data.LastName}` : snapshot.reservation.PICName,
            PaymentChannel: this.state.paymentChannel,
            detail: [{
                ReservationCode: snapshot.reservation.ReservationId,
                ContractSAPCode: snapshot.reservation.details[0].ContractId,
                MaterialSAPCode: snapshot.reservation.details[0].MaterialId,
                TotalDay: snapshot.reservation.details[0].TotalDay,
                Price: _.reduce(snapshot.reservation.details, (sum, n) => sum + parseInt(n.Price) ,0),
                PriceExtras: _.reduce(snapshot.reservation.details, (sum, n) => sum + parseInt(n.PriceExtras) ,0),
                ReservationDate: snapshot.reservation.created_at,
                PriceExtend: 0,
                TotalExtend: 0,
                QuantityOLC: 0,
                PriceOLC: 0,
                QuantityTrip: 0,
                PriceTrip: 0
            }]
          };
          this.props.getVirtualAccount({
            FormData: params,
            token: this.props.login.token
          });
          if(!REGEX_VALID_URL.test(this.props.login.Data.ImageKTP)) {
            this.props.getUserProfile({
              UserId: this.props.login.Data.Id,
              token: this.props.login.token
            });
          }
        } else {
          Alert.alert(
            'Information',
            'Oops! Something went wrong!',
            [
              {text: 'OK', onPress: () => {}},
            ],
            {cancelable: false},
          );
        }
      }
      if(snapshot.va !== undefined) {
        // this.setState({isVirtualAccountSelected: true});
        Actions.reset("VirtualAccountSelected", {
          navData: this.props.navData
        });
      }
      if(snapshot.user !== undefined) {
        this.props.setNewUser({
          Data: snapshot.user,
          token: this.props.login.token
        });
      }
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  manageReservation = paymentChannel => {
    const { requestStock, navData: { summaryData }, selectedProduct, login, tempPersonalDetail, notes, isPromoValid, totalPriceAfterDiscount, tempForm: { from_location, from_airport, to_location }, discount } = this.props;
    this.setState({ paymentChannel: paymentChannel });
    let startDate = new Date();
    let endDate = new Date();
    let StartDateArr = [];
    let pickupLocation;
    let dropLocation;
    let ZoneId = null;
    let TotalDistance = null;
    let CityName = null;
    let Duration = null;
    let IsStay = false;
    let OutTown = false;
    let ext = null;
    let ImageKTP = null;
    if(this.props.capture !== null) {
      if(this.props.capture.type === "galeri") {
        ImageKTP = this.props.capture.base64;
      } else {
        ext = this.props.capture.uri.split(".");
        ImageKTP = `data:image/${ext[ext.length - 1]};base64, ${this.props.capture.base64}`;
      }
    } else {
      ImageKTP = this.props.login.Data.ImageKTP;
    }
    switch (this.props.selectedProduct) {
      case CAR_RENTAL:
        startDate = summaryData.submittedData.trips.trip1.package <= 12 ? 
          new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.date} ${summaryData.submittedData.trips.trip1.pickup_schedule.time}`, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss") : 
          new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.startDate} ${summaryData.submittedData.trips.trip1.pickup_schedule.startTime}`, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss");
        endDate = summaryData.submittedData.trips.trip1.package <= 12 ?
          (new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.date} ${summaryData.submittedData.trips.trip1.pickup_schedule.time}`, "DD-MM-YYYY HH:mm:ss")).add(summaryData.submittedData.trips.trip1.package, 'hours').format("YYYY-MM-DD HH:mm:ss")
          : 
          summaryData.days > 1 ? 
            (new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.endDate} ${summaryData.submittedData.trips.trip1.pickup_schedule.startTime}`, "DD-MM-YYYY HH:mm:ss")).add(summaryData.submittedData.trips.trip1.package, 'hours').format("YYYY-MM-DD HH:mm:ss")
            :
            (new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.endDate} ${summaryData.submittedData.trips.trip1.pickup_schedule.startTime}`, "DD-MM-YYYY HH:mm:ss")).add(summaryData.submittedData.trips.trip1.package, 'hours').format("YYYY-MM-DD HH:mm:ss")
        // if(summaryData.days > 1) {
        //   for(var i = 0; i < summaryData.days; i++) {
        //     let dt = moment(startDate).add(i, 'days').format("YYYY-MM-DD HH:mm:ss")
        //     StartDateArr.push(dt);
        //   } 
        // }
        pickupLocation = [{
          "CityId": requestStock.CityId,
          "Long": from_location.coords.lng,
          "Lat": from_location.coords.lat,
          "Alamat": _.filter(from_location.options, { value: from_location.value })[0].label,
          "Time": startDate,
          "Notes": notes
        }];
        dropLocation = [{
          "CityId": requestStock.CityId,
          "Long": from_location.coords.lng,
          "Lat": from_location.coords.lat,
          "Alamat": _.filter(from_location.options, { value: from_location.value })[0].label,
          "Time": endDate,
          "Notes": notes
        }];
        CityName = summaryData.submittedData.trips.trip1.from_city;
        Duration = summaryData.submittedData.trips.trip1.package;
        break;
      case AIRPORT_TRANSFER:
        startDate = new moment(`${summaryData.submittedData.trips.trip1.arrival_schedule.date} ${summaryData.submittedData.trips.trip1.arrival_schedule.time}`, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss")
        endDate = new moment(`${summaryData.submittedData.trips.trip1.arrival_schedule.date} ${summaryData.submittedData.trips.trip1.arrival_schedule.time}`, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss")
        pickupLocation = [{
          "CityId": requestStock.CityId,
          "Long": from_airport.coords.lng,
          "Lat": from_airport.coords.lat,
          "Alamat": _.filter(from_airport.options, { value: from_airport.value })[0].label,
          "Time": startDate,
          "Notes": notes
        }];
        dropLocation = [{
          "CityId": requestStock.CityId,
          "Long": to_location.coords.lng,
          "Lat": to_location.coords.lat,
          "Alamat": _.filter(to_location.options, { value: to_location.value })[0].label,
          "Time": endDate,
          "Notes": notes
        }];
        ZoneId = this.props.zone.MsZoneId;
        TotalDistance = this.props.zone.KM;
        CityName = summaryData.submittedData.trips.trip1.to_city;
        Duration = this.props.zone.KM;
        break;
      case BUS_RENTAL:
        startDate = new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.startDate} ${summaryData.submittedData.trips.trip1.pickup_schedule.startTime}`, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss");
        endDate = new moment(`${summaryData.submittedData.trips.trip1.pickup_schedule.endDate} ${summaryData.submittedData.trips.trip1.pickup_schedule.endTime}`, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss")
        pickupLocation = [{
          "CityId": requestStock.CityId,
          "Long": from_location.coords.lng,
          "Lat": from_location.coords.lat,
          "Alamat": _.filter(from_location.options, { value: from_location.value })[0].label,
          "Time": startDate,
          "Notes": notes
        }];
        dropLocation = [{
          "CityId": requestStock.CityId,  // ambigu
          "Long": to_location.coords.lng,
          "Lat": to_location.coords.lat,
          "Alamat": _.filter(to_location.options, { value: to_location.value })[0].label,
          "Time": endDate,
          "Notes": notes
        }];
        ZoneId = this.props.zoneForBus.MsZoneId;
        TotalDistance = this.props.zoneForBus.KM;
        CityName = summaryData.submittedData.trips.trip1.from_city;
        Duration = summaryData.days;
        break;
      default:
        break;
    }
    let promo = isPromoValid ? [{
      "PromoCode": discount.code,
      "CategoryPromoId": discount.category_id,
      "CategoryPromoName": "",
      "TypeValue": discount.type_value,
      "Value": parseInt(discount.value)
    }] : []
    let ReservationDetail = _.map(summaryData.list, v => {
      let extras = _.filter(v.vehicle.extraItems, v => v.amounts !== 0);
      let extraItems = extras.length > 0 ? _.map(extras, v => {
        if(_.isBoolean(v.amounts)) {
          v.amounts = 1
        };
        return v;
      }) : [];
      let Passengers = [{
        "Name": tempPersonalDetail.name,
        "PhoneNumber": tempPersonalDetail.phone_number,
        "Email": login.Data.EmailPersonal,
        "IDCardNumber": tempPersonalDetail.ktp,
        "NPWPNumber": "",
        "LicenseNumber": login.Data.NoSIM,
        "PassportNumber": "",
        "Address": tempPersonalDetail.address,
        // "IsPIC": login.Data.IsPic === "1" ? true : false,
        "IsPIC": true,
        "ImageKTP": ImageKTP,
        "ImageSIM": "",
        "IsForeigner": login.Data.IsForeigner
      }]
      switch (this.props.selectedProduct) {
        case CAR_RENTAL:
          if(v.vehicle.extraItems.length > 0) {
            IsStay = this.props.requestStock.ProductServiceId === SELF_DRIVE ? false : _.filter(v.vehicle.extraItems, { ExtrasId: "3" })[0].amounts !== 0 ? true : false;
            OutTown = this.props.requestStock.ProductServiceId === SELF_DRIVE ? false : _.filter(v.vehicle.extraItems, { ExtrasId: "2" })[0].amounts !== 0 ? true : false;
          }
          if(this.props.requestStock.ProductServiceId === SELF_DRIVE) {
            _.map(this.props.tempDriverDetail, (v, k) => {
              var ImageKtpPass = null;
              var ImageSimPass = null;
              if(typeof v.ImageKTP === "object") {
                if(v.ImageKTP.type === "galeri") {
                  ImageKtpPass = v.ImageKTP.base64;
                } else {
                  var s = v.ImageKTP.uri.split('.')
                  ImageKtpPass = `data:image/${s[s.length - 1]};base64, ${v.ImageKTP.base64}`
                }
              } else {
                ImageKtpPass = v.ImageKTP;
              }

              if(typeof v.ImageSIM === "object") {
                if(v.ImageSIM.type === "galeri") {
                  ImageSimPass = v.ImageSIM.base64;
                } else {
                  var s = v.ImageSIM.uri.split('.')
                  ImageSimPass = `data:image/${s[s.length - 1]};base64, ${v.ImageSIM.base64}`
                }
              } else {
                ImageSimPass = v.ImageSIM;
              }
              Passengers.push({
                "Name": v.Name,
                "PhoneNumber": v.PhoneNumber,
                "Email": null,
                "IDCardNumber": v.IDCardNumber,
                "NPWPNumber": "",
                "LicenseNumber": v.LicenseNumber,
                "PassportNumber": "",
                "Address": v.Address,
                "IsPIC": false,
                "ImageKTP": ImageKtpPass,
                "ImageSIM": ImageSimPass,
                "IsForeigner": v.IsForeigner
              })
            })
          }
          break;
        case AIRPORT_TRANSFER:
          if(v.vehicle.extraItems.length > 0) {
            // IsStay = _.filter(v.vehicle.extraItems, { ExtrasId: "3" })[0].amounts !== 0 ? true : false;
            OutTown = _.filter(v.vehicle.extraItems, { ExtrasId: "2" })[0].amounts !== 0 ? true : false;
          }
          break;
        case BUS_RENTAL:
          IsStay = true;
          OutTown = true;
          _.map(this.props.tempCoordinatorDetail, (v, k) => {
            // var ImageKtpPass = v.ImageKTP.uri.split('.');
            var ImageKtpPass = null;
            if(v.ImageKTP.type === "galeri") {
              ImageKtpPass = v.ImageKTP.base64;
            } else {
              var s = v.ImageKTP.uri.split('.')
              ImageKtpPass = `data:image/${s[s.length - 1]};base64, ${v.ImageKTP.base64}`
            }
            Passengers.push({
              "Name": v.Name,
              "PhoneNumber": v.PhoneNumber,
              "Email": null,
              "IDCardNumber": v.IDCardNumber,
              "NPWPNumber": "",
              "LicenseNumber": "",
              "PassportNumber": "",
              "Address": v.Address,
              "IsPIC": false,
              "ImageKTP": ImageKtpPass,
              "ImageSIM": null,
              "IsForeigner": v.IsForeigner
            })
          })
          break;
      }
      let prm = {
        "UserId": login.Data.Id,
        "UnitTypeId": v.vehicle.vehicleTypeId,
        "UnitTypeName": v.vehicle.vehicleAlias,
        "CityId": requestStock.CityId,
        // "MsAirportCode": requestStock.MsAirportCode,
        "CityName": CityName,
        "BranchId": requestStock.BranchId,
        "StartDate": [startDate],
        "EndDate": [endDate],
        "Duration": Duration,
        "QtyUnit": 1,
        "QtyPassenger": this.props.requestStock.ProductServiceId === "PSV0001" ? 1 : v.vehicle.totalSeat,  // 1 untuk self drive / untuk chaffeur gunakan v.totalSeat
        "IsStay": IsStay,
        "OutTown": OutTown,
        // "IsStay": this.props.requestStock.ProductServiceId === "PSV0001" ? false : _.filter(v.vehicle.extraItems, { ExtrasId: "3" })[0].amounts !== 0 ? true : false,
        // "OutTown": this.props.requestStock.ProductServiceId === "PSV0001" ? false : _.filter(v.vehicle.extraItems, { ExtrasId: "2" })[0].amounts !== 0 ? true : false,
        "IsPlanned": false,
        "IsExpedition": false,
        "IsWithDriver": this.props.selectedProduct === CAR_RENTAL ? ( this.props.requestStock.ProductServiceId === "PSV0001" ? false : true ) : true,
        "Fuel": "01",
        "TollAndParking": "01",
        "DriverOrRider": "01",
        "IsPaymentUpfront": true,
        "ContractId": v.vehicle.unitContractList[0].contractId,
        "ContractItemId": v.vehicle.unitContractList[0].contractItemId,
        "ProductId": "PD-001",
        "MaterialId": v.vehicle.unitContractList[0].materialId,
        "Price": v.vehicle.rentInfo.basePrice * summaryData.days,
        "IsTransmissionManual": v.vehicle.isTransmissionManual ? true : false,
        "PriceExtras": 0,
        "MsProductId": selectedProduct,
        "MsProductServiceId": this.props.selectedProduct !== BUS_RENTAL ? requestStock.ProductServiceId : null,
        "ZoneId": ZoneId,     // kecuali air transport dan bus
        "TotalDistance": TotalDistance,  // kecuali air transport dan bus
        "PickupLocation": pickupLocation,
        "DropLocation": dropLocation,
        "Passengers": Passengers,
        "ReservationExtras": extraItems,
        "ReservationPromo": promo
      }
      if(this.props.selectedProduct === AIRPORT_TRANSFER) {
        prm.MsAirportCode = requestStock.MsAirportCode;
      }
      return prm
    });
    let params = {
      "CompanyId": requestStock.BusinessUnitId === "0104" ? "0100" : "0200",
      "CustomerName": "Customer B2C",
      "BusinessUnitId": requestStock.BusinessUnitId,
      "BranchId": requestStock.BranchId,
      "ServiceTypeId": requestStock.ServiceTypeId,
      "ServiceTypeName": `${summaryData.list[0].vehicle.unitContractList[0].contractSAPCode} - ${summaryData.list[0].vehicle.unitContractList[0].serviceTypeId}`,
      "CreatedBy": login.Data.Id,
      "PIC": login.Data.Id,
      "PICName": tempPersonalDetail.name,
      "PICPhone": tempPersonalDetail.phone_number,
      "NotesReservation": notes,
      "TotalPrice": isPromoValid ? totalPriceAfterDiscount : summaryData.totalPrice,
      "ReservationDetail": ReservationDetail
    }
    // console.log('params ==>', JSON.stringify(params));
    this.props.saveReservation({
      FormData: params,
      token: this.props.login.token
    });
    // alert("test");
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderTabItem(label, index) {
    const isActive = this.state.activetabIndex == index;

    return (
      <TouchableOpacity
        style={ [styles.tab.container, isActive ? {backgroundColor: "#0B1C4A", borderColor: appVariables.colorOrange} : {}] }
        onPress={ () => this.setState({activetabIndex: index}) }
      >
        <Text color={ appVariables.colorWhite }>
          { label }
        </Text>
      </TouchableOpacity>
    );
  }  

// ---------------------------------------------------

  _renderTabs() {
    return (
      <View style={ styles.tab.grouper }>

        {/* { this._renderTabItem("Debit/Credit Card", 0) } */}
        { this._renderTabItem("Virtual Account", 1) }        

      </View>
    );
  }

// ---------------------------------------------------

  _renderContents() {
    // if (this.state.activetabIndex === 0) {
    //   return (
    //     <CreditCardTabContent
    //       { ... this.props }
    //     />
    //   );
    // }

    // if (!this.state.isVirtualAccountSelected) {
    return (
      <VirtualAccountTabContent
        { ... this.props }
        onSelect={ paymentChannel => this.manageReservation(paymentChannel) }
        isLoading={this.props.loadingReservation || this.props.loadingPayment}
      />
    );
    // }

    // return (
    //   <VirtualAccountSelectedTabContent
    //     { ... this.props }
    //     onChange={ () => this.setState({isVirtualAccountSelected: false}) }
    //   />
    // );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const navData = this.props.navigation.getParam("navData", {});

    return (
      <ProgressedContainer
        header={{
          label: Lang.payment[this.props.lang],
          onBackPress: () => this.props.navigation.pop(),
        }}
        steps= { navData.processSteps }
        currentStepIndex={ navData.processSteps.indexOf(Lang.payment[this.props.lang]) }
        { ... this.props }
      >

        {/* {  this._renderTabs() } */}

        <KeyboardAvoidingView
          behavior={ "padding" }
          enabled={false}
          style={ styles.container }
        >
          <ScrollView
            style={ styles.innerContainer}
            contentContainerStyle={ styles.innerContentContainer }
          >
          
            { this._renderContents() }

          </ScrollView>
        </KeyboardAvoidingView>

      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ stockList, authentication, tempNotes, temporaryForm, discountPromo, productService, camera, reservationSave, payment, airportZone, imageBuffer, busZone, language, userProfile }) => {
  const { requestStock, tempStockComplete } = stockList;
  const { login } = authentication;
  const { notes } = tempNotes;
  const { tempPersonalDetail, tempForm, tempDriverDetail, tempCoordinatorDetail } = temporaryForm;
  const { totalPriceAfterDiscount, isPromoValid, discount } = discountPromo;
  const { products, selectedProduct } = productService;
  const { capture } = camera;
  const { reservation } = reservationSave;
  const { va } = payment;
  const loadingReservation = reservationSave.loading;
  const loadingPayment = payment.loading;
  const { zone } = airportZone;
  const { bufferKtp } = imageBuffer;
  const zoneForBus = busZone.zone;
  const { lang } = language;
  const { user } = userProfile;
  return { requestStock, tempStockComplete, login, notes, tempPersonalDetail, tempDriverDetail, totalPriceAfterDiscount, isPromoValid, products, selectedProduct, tempForm, discount, capture, reservation, va, loadingReservation, loadingPayment, zone, bufferKtp, tempCoordinatorDetail, zoneForBus, lang, user }
}

export default connect(mapStateToProps, {
  saveReservation,
  getVirtualAccount,
  setRequestStockList,
  getUserProfile, 
  setNewUser
})(Payment);
