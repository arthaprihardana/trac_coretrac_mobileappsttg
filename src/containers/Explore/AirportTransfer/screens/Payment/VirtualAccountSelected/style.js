import {
  appVariables,
  appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
  },

  innerContainer: {
    flex: 1,
    backgroundColor: appVariables.colorWhite,
  },

  innerContentContainer: {
    paddingBottom: 20,
  },

  tab: {
    grouper: {
      flex: -1,
      flexDirection: "row",
      height: 70,
      borderTopWidth: 1,
      borderColor: "#aaaaaa",
    },

    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      borderBottomWidth: 4,
      paddingTop: 5,
      borderColor: "#12265B",
      backgroundColor: "#12265B"
    },
  },


  detail: {
    container: {
      flex: -1,
      paddingHorizontal: 20,
      paddingTop: 20,
      paddingBottom: 30,
    },

    title: {
      paddingBottom: 10,
    },

    row: {
      paddingTop: 10,
      flex: -1,
    }
  },

  dropdown: {
    container: { 
      flex: -1,
      padding: 20,
      paddingTop: 10,
      paddingBottom: 0,
    },

    label: {
      container: { 
        flex: -1,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: appVariables.colorGray,
      },

      textContainer: {
        flex: 1,
      },

      innerContainer: {
        flex: -1,
      },
    },

    content: {
      container: {
        paddingTop: 30,
        paddingBottom: 20,
      },
    },
  },

  button: {
    grouper: {
      flex: -1,
      flexDirection: 'row',
      height: 40,
      marginTop: 30,
      paddingHorizontal: 20,
      justifyContent: "space-between",
    },

    container: {
      flex: -1,
      // width: (appMetrics.screenWidth - 60) / 2,
      width: appMetrics.screenWidth,
      borderRadius: 7,
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
}