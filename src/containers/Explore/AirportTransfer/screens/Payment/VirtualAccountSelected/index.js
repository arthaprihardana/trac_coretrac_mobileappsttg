/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-27 15:41:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-24 00:40:19
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/id";
import _ from "lodash";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";

import Text from "@airport-transfer-fragments/Text";
import Icon from '@airport-transfer-fragments/Icon';

import styles from "./style";
import { Actions } from "react-native-router-flux";
import {
  setTempStock, 
  setTempStockComplete, 
  setTempForm, 
  setTempPersonalDetail, 
  setNotes,
  checkPromoValid,
  setTempDriverDetail,
  setRequestStockList
} from "../../../../../../actions";
import Lang from '../../../../../../assets/lang';

class VirtualAccountSelected extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      activeDropIndex: 0,
      countdown: null,
      // currentTime: moment().unix()

      hour: 0,
      minutes: 0,
      seconds: 0
    };

    this.secondsRemaining;
    this.intervalHandle;
    this.startCountDown = this.startCountDown.bind(this);
    this.tick = this.tick.bind(this);
  }

  componentDidMount = () => {
    this.startCountDown()
  }
  
  componentWillUnmount = () => {
    clearInterval(this.intervalHandle);
    this.props.setRequestStockList(null);
    this.props.setTempStock(null);
    this.props.setTempStockComplete(null);
    this.props.setTempForm(null);
    this.props.setTempPersonalDetail(null);
    this.props.setTempDriverDetail(null);
    this.props.setNotes(null);
    this.props.checkPromoValid(false);
  }
  
// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  tick() {
    let diffTime = this.secondsRemaining;
    let duration = moment.duration(diffTime*1000, 'milliseconds');
    this.setState({
      hour: duration.hours(),
      minutes: duration.minutes(),
      seconds: duration.seconds()
    });
    if(duration.seconds() < 10) {
      this.setState({
        seconds: `0${duration.seconds()}`
      })
    }
    if(duration.minutes() < 10) {
      this.setState({
        minutes: `0${duration.minutes}`
      })
    }
    if(duration.hours() < 10) {
      this.setState({
        hour: `0${duration.hours()}`
      })
    }
    if(duration.seconds() === 0 && duration.minutes() === 0 && duration.hours() === 0) {
      clearInterval(this.intervalHandle);
    }

    this.secondsRemaining--;
  }

  startCountDown() {
    this.intervalHandle = setInterval(this.tick, 1000);
    let currentTime = moment().unix();
    let eventTime= moment(this.props.reservation.WaitingForPaymentTime).unix();
    this.secondsRemaining = eventTime - currentTime;
  }

  toggleDrop(index) {
    if (this.state.activeDropIndex == index) {
      return false;
    }

    this.setState({
      activeDropIndex: index,
    });
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderDetailRow(label, value, copy) {
    return (
      <View style={styles.detail.row}>
        <Text size={15} color={appVariables.colorGray} style={{paddingBottom: 2,}}>
          {label}
        </Text>

        <Text size={15}>
          {value}
        </Text>
      </View>
    )
  }

// ---------------------------------------------------

  _renderPaymentStep(label) {
    return (
      <View style={styles.content.container.paymentStepText}>
        <Text color={appVariables.colorGray}>
          {label}
        </Text>
      </View>
    )
  }

// ---------------------------------------------------

  _renderDetail() {
    let data = Object.keys(this.props.va);
    let bankName = _.filter(data, o => {
      let reg = /(?!=_)^[va]+/g;
      if(reg.test(o)) {
        return o;
      }
    });
    
    return (
      <View style={styles.detail.container}>
        <Text size={25} style={styles.detail.title}>
          { `${_.startCase(bankName[0].split("_")[1])} Virtual Account` }
        </Text>
        {this._renderDetailRow(Lang.pleaseCompletePayment[this.props.lang], `${this.state.hour} ${Lang.hour[this.props.lang]} ${this.state.minutes} ${Lang.minutes[this.props.lang]} ${this.state.seconds} ${Lang.second[this.props.lang]}`, Lang.copy[this.props.lang])}
        {this._renderDetailRow(Lang.virtualAccountNumber[this.props.lang], this.props.va[bankName[0]] )}
      </View>
    );
  }

// ---------------------------------------------------

  _renderDropdown(label, content, index) {
    const isActive = this.state.activeDropIndex == index;

    let contentRender = null;
    if (isActive) {
      contentRender = this._renderContent(content);
    }

    return (
      <View style={ styles.dropdown.container }>
        <TouchableOpacity style={ styles.dropdown.label.container }
          onPress={ () => this.toggleDrop(index) }
        >
          <View style={ styles.dropdown.label.textContainer }>
            <Text size={16}>
              {label}
            </Text>
          </View>

          <View style={ styles.dropdown.label.iconContainer }>
            <Icon size={20} name={ isActive ? "angle-up" : "angle-down" } />
          </View>
        </TouchableOpacity>

        { contentRender }

      </View>
    )
  }

// ---------------------------------------------------

  _renderContent(content) {
    return (
      <View style={ styles.dropdown.content.container }>
        <Text color={ appVariables.colorGray }>
          { content }
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderDetailButton(buttonColor, buttonLabel, textColor, borderWidth,onPress) {
    return (
      <TouchableOpacity
        style={[styles.button.container, {borderWidth: borderWidth, backgroundColor: buttonColor,}]}
        onPress={() => onPress()}
      >
        <Text color={textColor}>
          {buttonLabel}
        </Text>
      </TouchableOpacity>
    );
  }

// ---------------------------------------------------

  _renderButtons() {
    return (
      <View style={styles.button.grouper}>
        {/* {this._renderDetailButton('white',"CHANGE METHOD", '', 1, () => this.props.onChange())} */}
        {this._renderDetailButton('#F27D20',Lang.done[this.props.lang], 'white', 0, () => Actions.reset("PaymentSuccess"))}
      </View>
    )
  }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const navData = this.props.navigation.getParam("navData", {});
    let data = Object.keys(this.props.va);
    let bankName = _.filter(data, o => {
      let reg = /(?!=_)^[va]+/g;
      if(reg.test(o)) {
        return o;
      }
    });
    let atm = '';
    let ebanking = '';
    switch (bankName[0].split("_")[1]) {
      case "mandiri":
        atm = `1. Masukkan kartu ATM Mandiri, lalu masukkan PIN ATM
2. Pilih menu "Bayar/Beli"
3. Pilih "Lainnya" dan pilih "Lainnya" kembali
4. Pilih "Multi Payment"
5. Masukkan 5 digit awal dari nomor Mandiri VA (Virtual Account) yang di dapat (contoh: 89022)
6. Masukkan keseluruhan nomor VA
7. Masukkan jumlah pembayaran
8. Nomor VA, Nama dan Jumlah pembayaran akan ditampilkan di layar
9. Tekan angka 1 dan pilih "YA"
10. Konfirmasi pembayaran dan pilih "YA"
11. Transaksi selesai. Mohon simpan bukti transaksi.`;
        ebanking = `1. Install aplikasi Mandiri Online
2. Masukkan User ID dan PIN, kemudian login
3. Pilih Menu "Pembayaran"
4. Klik Buat "Pembayaran Baru"
5. Pilih "Multipayment" 
6. Pilih "DOKU Merchant" pada bagian penyedia jasa  
7. Masukkan Nomor VA  
8. Klik "Lanjut", layar akan menampilkan nomor VA
9. Klik "Konfirmasi" lalu masukan nominal transaksi
10. Klik "Lanjut" 
11. Klik "Konfirmasi"  
12. Masukkan MPIN (PIN SMS Banking)  
13. Selesai dan Simpan Bukti Pembayaran Anda`
        break;
      case "permata":
        atm = `1. Pilih TRANSAKSI LAINNYA
2. Pilih PEMBAYARAN
3. Pilih PEMBAYARAN LAINNYA
4. Pilih VIRTUAL ACCOUNT
5. Masukan nomor Virtual Account, lalu pilih BENAR
6. Masukkan nominal yang akan dibayarkan, pilih BENAR
7. Pilih rekening yang akan digunakan untuk melakukan pembayaran
8.Transaksi selesai. Simpan struk ATM sebagai bukti bayar`
        ebanking = `1. Log in ke Permata Mobile Internet
2. Pilih menu PEMBAYARAN TAGIHAN
3. Pilih VIRTUAL ACCOUNT
4. Masukan nomor Virtual Account
5. Masukkan nominal pembayaran
6. Masukkan token transaksi
7. Transaksi selesai`
        break;
      default:
        break;
    }
    return (
      <ProgressedContainer
        header={{ label: Lang.payment[this.props.lang],
          onBackPress: () => Actions.reset("MyBooking"),
        }}
        steps= { navData.processSteps }
        currentStepIndex={ navData.processSteps.indexOf(Lang.payment[this.props.lang]) }
        { ... this.props }>

        <KeyboardAvoidingView
          behavior={ "padding" }
          enabled={false}
          style={ styles.container }>
          <ScrollView
            style={ styles.innerContainer}
            contentContainerStyle={ styles.innerContentContainer }>
          
            <View>
              {this._renderDetail()}
              
              {this._renderDropdown(
                Lang.PaymentViaATM[this.props.lang],
                atm,
                0
              )}
              {this._renderDropdown(
                Lang.PaymentViaMBanking[this.props.lang],
                ebanking,
                1
              )}

              {this._renderButtons()}
            </View>

          </ScrollView>
        </KeyboardAvoidingView>
      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ reservationSave, payment, language }) => {
  const { reservation } = reservationSave;
  const { va } = payment;
  const { lang } = language;
  return { reservation, va, lang }
}

export default connect(mapStateToProps, {
  setTempStock, 
  setTempStockComplete, 
  setTempForm, 
  setTempPersonalDetail, 
  setTempDriverDetail,
  setNotes,
  checkPromoValid,
  setRequestStockList
})(VirtualAccountSelected);