import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
  },

  innerContainer: {
    flex: 1,
    backgroundColor: appVariables.colorWhite,
  },

  innerContentContainer: {
    paddingBottom: 20,
  },

  tab: {
    grouper: {
      flex: -1,
      flexDirection: "row",
      height: 70,
      borderTopWidth: 1,
      borderColor: "#aaaaaa",
    },

    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      borderBottomWidth: 4,
      paddingTop: 5,
      borderColor: "#12265B",
      backgroundColor: "#12265B"
    },
  },
}