import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  Switch,
} from "react-native";
import { appVariables, appMetrics } from "@variables";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import RedeemForm from "@airport-transfer-fragments/RedeemForm";
import MainButton from "@airport-transfer-fragments/MainButton";

import TnCModal from "@airport-transfer-modals/TermsAndCondition";

import styles from "./style";



export default class CreditCard extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        number: "",
        name: "",
        expiration: "",
        cvv: "",
        shouldSave: "",
        promoCode: "",
      },

      isTncAgreed: false,
      isTnCModalVisible: false,
    };
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  formatCreditCardNumber(value) {
    value = value + "";
    value = value.replace(/\-/g, "");

    let split = [];
    while (value.length > 0) {
      split.push(value.substring(0, 4));

      value = value.substring(4);
    }

    return split.join("-").substring(0, 19);
  }

  goToPaymentDetailCreditCard() {
    this.props.navigation.push("PaymentSuccess", {
      navData: this.props.navigation.getParam("navData", {}),
    });
  }

// ---------------------------------------------------

  formatExpiration(value) {
    value = value + "";
    value = value.replace(/\//g, "");

    let split = [];
    while (value.length > 0) {
      split.push(value.substring(0, 2));

      value = value.substring(2);
    }

    if ((split[0] * 1) > 12) {
      return split[0].substring(0, 1);
    }

    return split.join("/").substring(0, 5);
  }

// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderSaveInfo() {
    return (
      <View style={ styles.form.ccSave.container }>
        <Text>
          Save This Card Information
        </Text>

        <Switch
          trackColor={{ true: appVariables.colorOrange, }}
          onValueChange={ (value) => this.onChangeForm("shouldSave", value) }
          value={ this.state.form.shouldSave }
        />

      </View>
    );
  }

// ---------------------------------------------------

  _renderTncRadio() {
    return (
      <RoundedCheck
        style={ styles.form.agreement.container }
        onPress={ (value) => this.setState({isTncAgreed: value}) }
        value={ this.state.isTncAgreed }
      >
        <View>
          <Text>
            I have read and understood the
          </Text>

          <TouchableOpacity
            onPress={ () => this.setState({isTnCModalVisible: true}) }
          >
            <Text color={ "navy" }>
              Astra Trac Privacy Policy and Term Condition
            </Text>
          </TouchableOpacity>
        </View>
      </RoundedCheck>
    );
  }

// ---------------------------------------------------

  _renderTopForm() {
    this.formatCreditCardNumber("123421323943949")

    return (
      <View style={ styles.form.topSectionContainer }>
        <FloatingTextInput
          label={ "Credit/Debit Card Number" }
          keyboardType={ "numeric" }
          onChangeText={ (value) => this.onChangeForm("number", value) }
          value={ this.state.form.number }
          customFormat={ (value) => this.formatCreditCardNumber(value) }
        />

        <FloatingTextInput
          label={ "Name on Card" }
          onChangeText={ (value) => this.onChangeForm("name", value) }
          value={ this.state.form.name }
        />

        <View style={ styles.form.splitFormContainer }>
          <FloatingTextInput
            label={ "Exp Date (MM/YY)" }
            keyboardType={ "numeric" }
            onChangeText={ (value) => this.onChangeForm("expiration", value) }
            value={ this.state.form.expiration }
            customFormat={ (value) => this.formatExpiration(value) }
          />

          <View>
            <FloatingTextInput
              label={ "CVV" }
              keyboardType={ "numeric" }
              onChangeText={ (value) => this.onChangeForm("cvv", value) }
              value={ this.state.form.cvv }
            />

            <TouchableOpacity
              style={ styles.form.cvvButton.container }
            >
              <Text color={ "#00c0c0" } size={ 12 }>
                What is CVV?
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        { this._renderSaveInfo() }

        { this._renderTncRadio() }
      </View>
    );
  }

// ---------------------------------------------------

  _renderBottomForm() {
    return (
      <View style={ styles.form.bottomSectionContainer}>

        <RedeemForm
          onApply={ (value) => this.onChangeForm("promoCode", value) }
        />
        
        <MainButton
          label={ "MAKE PAYMENT" }
          onPress={() => this.goToPaymentDetailCreditCard()}
          rounded
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderTnCModal() {
    return (
      <TnCModal
        isVisible={ this.state.isTnCModalVisible }
        onClosePress={ () => this.setState({isTnCModalVisible: false}) }
        onAgreePress={ () => this.setState({isTncAgreed: true, isTnCModalVisible: false}) }
      >
        { "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor vitae urna et laoreet. Mauris molestie, ipsum nec aliquet pretium, nisl nibh consectetur quam, id iaculis velit tellus lobortis massa. Integer quis odio mollis, mollis risus vitae, consectetur elit. Integer non sem in enim tempor lacinia at in ligula. Cras imperdiet, lectus at venenatis sagittis, odio metus elementum leo, ac convallis dolor lacus fringilla justo. Fusce non luctus metus. Phasellus convallis mollis porta." +
          "\n\n" +
        "Aenean condimentum eleifend ex et sagittis. Vestibulum quis ullamcorper nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante massa, egestas commodo auctor eget, interdum a sem. Vestibulum mattis ipsum quam, mattis convallis nibh sollicitudin in. Vestibulum sodales sit amet leo sed pellentesque." }
      </TnCModal>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View>
        { this._renderTopForm() }

        { this._renderBottomForm() }

        { this._renderTnCModal() }
      </View>
    );
  }
}
