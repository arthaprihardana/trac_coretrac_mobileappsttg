import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  form: {
    topSectionContainer: {
      flex: -1,
      paddingHorizontal: 20,
      borderBottomWidth: 1,
      borderColor: "#cccccc",
    },

    bottomSectionContainer: {
      paddingVertical: 10,
      paddingHorizontal: 20,
    },

    splitFormContainer: {
      flex: -1,
      flexDirection: "row",
      justifyContent: "space-between",
    },

    ccSave: {
      container: {
        flex: -1,
        flexDirection: "row",
        marginTop: 40,
        paddingVertical: 20,
        justifyContent: "space-between",
      },
    },

    cvvButton: {
      container: {
        position: "absolute",
        bottom: -24,
      },
    },

    agreement: {
      container: {
        paddingVertical: 20,
      },
    },
  },
}