import React, { Component } from 'react';
import { Content } from 'native-base';
import { OMessageSection } from '../../../../../components/organisms';
import Header from "@airport-transfer-fragments/Header";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";

class Message extends Component {
  
  render() {
    return (
      <BaseContainer>
        <Header
         title={"Message"}
         onBackPress={ () => this.props.navigation.pop()}
       />
        <Content style={{backgroundColor:'white'}}>
        <OMessageSection />
        </Content>
      </BaseContainer>
    )
  }
}

export default Message;