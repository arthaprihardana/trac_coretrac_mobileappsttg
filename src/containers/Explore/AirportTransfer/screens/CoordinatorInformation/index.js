/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-11 11:24:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 16:59:44
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert
} from "react-native";
import { Textarea } from "native-base";
import ModalDropdown from 'react-native-modal-dropdown';
import MCameraSim from '../../../../../components/molecules/MCameraSim';
import MCameraKtp from '../../../../../components/molecules/MCameraKtp';
import Mask from 'react-native-mask';
import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import SvgIcon from "../../../../../utils/SvgIcon";
import RoundedCheck from '../../fragments/RoundedCheck/index';
import { phonePrefix } from '../../../../../utils/DummyData';
import styles from "./style";
import MainButton from "@airport-transfer-fragments/MainButton";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";
import { appVariables } from "@variables";
import _ from "lodash";
import { connect } from "react-redux";
import { openCameraForKtp, setTempCoordinatorDetail } from "../../../../../actions";
import Lang from '../../../../../assets/lang';

class CoordinatorInformation extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------
  constructor(props) {
    super(props);

    const navData = JSON.parse(JSON.stringify(this.props.navData));
    const forms = navData.summaryData.list.map((item, index) => {
      return {
        ImageKTP: "",
        ImageSIM: "",
        IsForeigner: 0,
        IDCardNumber: "",
        LicenseNumber: "",
        Name: "",
        Address: "",
        PhoneNumber: "",
        IsPIC: false
      };
    });
    
    this.state = {
      navData: navData,
      forms: forms,
      activeDropIndex: 0,
      activeIndex: 0,
      phoneData: phonePrefix,
      selectedPhoneNumber: phonePrefix[0].value,
      selectedPhoneImage: phonePrefix[0].image,
      readyToSubmit: false
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    // if(this.props.captureSim !== prevProps.captureSim) {
    //   return { captureSim: this.props.captureSim }
    // }
    if(this.props.captureKtp !== prevProps.captureKtp) {
      return { captureKtp: this.props.captureKtp }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      // if(snapshot.captureSim !== undefined) {
      //   let frm = this.state.forms;
      //   frm[this.state.activeIndex].ImageSIM = snapshot.captureSim;
      //   this.setState({
      //     forms: frm
      //   })
      // }
      if(snapshot.captureKtp !== undefined) {
        let frm = this.state.forms;
        frm[this.state.activeDropIndex].ImageKTP = snapshot.captureKtp;
        this.setState({
          forms: frm
        })
      }
    }
  }
  
  
// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------
  
  onChangeForm(name, index, value) {
    const forms = this.state.forms;
    forms[index][name] = value;
    let ready = _.every(this.state.forms, v => {
      return !_.isEmpty(v.ImageKTP) && !_.isEmpty(v.IDCardNumber) && !_.isEmpty(v.Name) && !_.isEmpty(v.Address) && !_.isEmpty(v.PhoneNumber)
    });
    this.setState({
      forms,
      readyToSubmit: ready
    });
  }

  onValidate = async (current, index) => {
    let except = ['IsPIC', 'IsForeigner', 'ImageSIM', 'LicenseNumber'];
    except.push(current);
    let o = _.omit(this.state.forms[index], except);
    let a = await _.findKey(o, val => {
      return _.isEmpty(val);
    });
    return a;
  }

// ---------------------------------------------------

  toggleDrop(index) {
    if (this.state.activeDropIndex == index) {
      return false;
    }

    this.setState({
      activeDropIndex: index,
    });
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderExpandableDriverInformation(label, index) {
    const isActive = this.state.activeDropIndex == index;
    let contentRender = null;
    if (isActive) {
      contentRender = this._renderDriverInformationContent(index);
    }
    return (
      <View>
        <TouchableOpacity style={styles.expandableDriverInformation.title}
          onPress={() => this.toggleDrop(index)}>
          <View style={{
            flex: -1,
            justifyContent: 'center',
          }}>
            <SvgIcon name="icoIDCard"></SvgIcon>
          </View>
          <View style={{
            flex: 1,
            paddingLeft: 20,
          }}>
            <Text
              size={18}>
              {label}
            </Text>
          </View>
        </TouchableOpacity>
        {contentRender}
      </View>
    );
  }
// ---------------------------------------------------
  
  _renderScanGuidance() {
    return (
      <View style={styles.expandableDriverInformationContent.scanGuidance}>
        <Text
          color={appVariables.colorGray}
          size={14}>
          {Lang.pleaseScanUploadKTPCoordinator[this.props.lang]}
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderCitizen(index) {
    return (
      <View style={styles.expandableDriverInformationContent.citizenLabel.container}>
        <View style={styles.expandableDriverInformationContent.citizenLabel.innerContainer}>
          {this._renderCitizenDetail(Lang.indeonsiaCitizen[this.props.lang], 0, index)}
        </View>

        <View style={styles.expandableDriverInformationContent.citizenLabel.innerContainer}>
          {this._renderCitizenDetail(Lang.foreignCitizen[this.props.lang], 1, index)}
        </View>

      </View>
    )
  }

// ---------------------------------------------------

  _renderCitizenDetail(label, value, index) {
    return (
      <View style={{ flex: -1 }}>
        <RoundedCheck
          value={this.state.forms[index].IsForeigner === value}
          onPress={() => {
            let frm = this.state.forms;
            frm[index].IsForeigner = value;
            this.setState({
              forms: frm
            });
          }}
          >
          <Text size={14}>{label}</Text>
        </RoundedCheck>
      </View>
    )
  }

// ---------------------------------------------------

  _renderIDScanner(index) {
    return (
      <View style={styles.expandableDriverInformationContent.idScanner.boxContainer}>
        <View style={styles.expandableDriverInformationContent.idScanner.boxInnerContainer}>
          <View style={styles.expandableDriverInformationContent.idScannerDetail.wrapper}>
            { this.props.captureKtp !== null ?
            <View style={styles.expandableDriverInformationContent.idScannerDetail.scanImage}>
              <Image
                style={{width: 150, height: 100}}
                resizeMode="cover"
                source={{uri: this.state.forms[index].ImageKTP.uri }}
              />
            </View> : <View style={styles.expandableDriverInformationContent.idScannerDetail.scanIcon}>
              <SvgIcon name="icoScan" ></SvgIcon>
            </View> }
            <View style={styles.expandableDriverInformationContent.idScannerDetail.buttonWrapper}>
              <MainButton
                onPress={() => {
                  Alert.alert(
                    'Info',
                    'Dari mana anda akan mengambil gambar?',
                    [
                      {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                      {
                        text: 'Galeri',
                        onPress: () => Actions.push("Galeri", { route: "CoordinatorInformation", type: "ktp" }),
                        style: 'cancel',
                      },
                      {text: 'Kamera', onPress: () => this.props.openCameraForKtp(true)},
                    ],
                    {cancelable: false},
                  );
                }}
                label={Lang.photoKTP[this.props.lang]}
                backgroundColor={appVariables.colorBlueHeader}
                rounded>
              </MainButton>
            </View>
          </View>
        </View>
        <View style={{
          flex: 0.25
        }}>
        </View>
        <View style={{ flex: 4 }}>
          {/* <View style={styles.expandableDriverInformationContent.idScannerDetail.wrapper}>
            { this.props.captureSim !== null ?
            <View style={styles.expandableDriverInformationContent.idScannerDetail.scanImage}>
              <Image
                style={{width: 150, height: 100}}
                resizeMode="cover"
                source={{uri: this.state.forms[index].ImageSIM.uri }}
              />
            </View> : <View style={styles.expandableDriverInformationContent.idScannerDetail.scanIcon}>
              <SvgIcon name="icoScan" ></SvgIcon>
            </View> }
            <View style={styles.expandableDriverInformationContent.idScannerDetail.buttonWrapper}>
              <MainButton
                onPress={() => this.props.openCameraForSim(true)}
                label={"PHOTO SIM"}
                backgroundColor={appVariables.colorBlueHeader}
                rounded>
              </MainButton>
            </View>
          </View> */}
        </View>
      </View>
    )
  }

// ---------------------------------------------------

  _renderField(index) {
    return (
      <View>
        <FloatingTextInput
          label={Lang.ktpNumber[this.props.lang]}
          keyboardType={"numeric"}
          onChangeText={(value) => {
            this.onValidate("IDCardNumber", index).then(val => {
              this.onChangeForm("IDCardNumber", index, value);
            });
          }}
          value={this.state.forms[index].IDCardNumber}
          maxLength={16}
        />
        {/* <FloatingTextInput
          label={"SIM Number"}
          keyboardType={"numeric"}
          onChangeText={(value) => this.onChangeForm("LicenseNumber", index, value)}
          value={this.state.forms[index].LicenseNumber}
          maxLength={12}
        /> */}
        <FloatingTextInput
          label={Lang.fullName[this.props.lang]}
          onChangeText={(value) => {
            this.onValidate("Name", index).then(val => {
              this.onChangeForm("Name", index, value)
            });
          }}
          value={this.state.forms[index].Name}
        />
        <FloatingTextInput
          label={Lang.residentialAddress[this.props.lang]}
          onChangeText={(value) => {
            this.onValidate("Address", index).then(val => {
              this.onChangeForm("Address", index, value)
            });
          }}
          value={this.state.forms[index].Address}
        />
      </View>
    )
  }

// ---------------------------------------------------

  _phoneNumberSelected = (idx, value) => {
    this.setState({
      selectedPhoneNumber: value.value,
      selectedPhoneImage: value.image
    });
  }

  _renderPhoneNumber = (option, index) => {
    return (
      <View key={index} style={styles.dropdownItemWrapper}>
        <Mask shape={'circle'}>
          <Image source={option.image} style={styles.dropdownImage} />
        </Mask>
        <Text style={styles.dropdownText}>{option.value}</Text>
      </View>
    );
  }

  _renderFieldPhoneNumber(index) {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'column' }}>
          <View style={{ flex: 1 }}>
            <ModalDropdown
              style={{
                marginLeft: 0,
                flex: 1
              }}
              ref="phone_number"
              options={this.state.phoneData}
              onSelect={(idx, value) => this._phoneNumberSelected(idx, value)}
              renderRow={this._renderPhoneNumber}>
              <View style={styles.sortWrapper}>
                <Mask style={styles.itemImage} shape={'circle'}>
                  <Image style={styles.image} source={this.state.selectedPhoneImage} />
                </Mask>
                <Text style={styles.labelPhoneStyle}>{this.state.selectedPhoneNumber}</Text>
                <SvgIcon name="icoSort" style={styles.iconWrapper} />
              </View>
            </ModalDropdown>
          </View>
          <View style={{
            flex: 3,
            flexDirection: 'row',
            borderBottomWidth: 1,
            marginLeft: 0,
            borderBottomColor: "#aaaaaa" 
            }} />
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          paddingLeft: 20,
          }}>
          <View style={{
            flex: 1,
            borderBottomWidth: 1,
            height: 35,
            borderBottomColor: "#aaaaaa",
            marginRight: 0,
            marginTop: 0,
            }}>
            <Textarea rowSpan={5} placeholder="" maxLength={12} style={styles.textarea} keyboardType="number-pad" value={this.state.forms[index].PhoneNumber} onChangeText={(value) => {
              this.onValidate("PhoneNumber", index).then(val => {
                this.onChangeForm("PhoneNumber", index, value)
              });
            }} />
          </View>
        </View>
      </View>
    )
  }

// ---------------------------------------------------

  _renderButton() {
    return (
      <View style={styles.expandableDriverInformationContent.submitButtonWrapper}>
        <MainButton
          label={Lang.continue[this.props.lang]}
          rounded
          onPress={ () => {
            if(this.state.readyToSubmit) {
              this.props.setTempCoordinatorDetail(this.state.forms);
              Actions.push("Payment", {
                navData: this.props.navData
              });
            }
            return true;
          }}
          disabled={!this.state.readyToSubmit}
        />
      </View>
    )
  }

// ---------------------------------------------------

  _renderGrayLine() {
    return (
      <View style={{
        flex: -1,
        borderBottomWidth: 1,
        borderBottomColor: appVariables.colorGray,
      }}>
      </View>
    )
  }

// ---------------------------------------------------

  _renderDriverInformationContent(index) {
    return (
      <View style={{
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
      }}>
        {this._renderScanGuidance()}
        {this._renderCitizen(index)}
        {this._renderIDScanner(index)}
        {this._renderField(index)}
        <View style={{
          flex: 1,
          paddingTop: 40
          }}>
          <Text
            size={12}
            color={appVariables.colorGray}>
            {Lang.phoneNumber[this.props.lang]}
          </Text>
        </View>
        {this._renderFieldPhoneNumber(index)}
        {/* {this._renderButton()} */}
      </View>
    )
  }

// ---------------------------------------------------

  _renderForms() {
    return this.state.forms.map((form, index) => {
      return (
        <View key={ index } style={styles.collapsibleDriverInformation.grayLineWrapper}>
          {this._renderExpandableDriverInformation(`${Lang.coordinatorBus[this.props.lang]} ${(index + 1)}`, index)}
        </View>
      );
    })
        {/*this._renderGrayLine()*/}
  }



// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ProgressedContainer
        header={{
          label: Lang.personalDetail[this.props.lang],
          onBackPress: () => Actions.pop(),
        }}
        steps={this.state.navData.processSteps}
        currentStepIndex={this.state.navData.processSteps.indexOf(Lang.informationCoordinator[this.props.lang])}
        {... this.props}
      >
        <View style={{
          flex: 1,
          backgroundColor:appVariables.colorWhite,
        }}>
          <ScrollView style={{flex: 1,}}>
            <View style={{flex: 1,}}>
              
              { this._renderForms() }

            </View>
            {this._renderButton()}
          </ScrollView>
        </View>
        {/* { this.props.openCameraSim && 
          <MCameraSim 
            visible={this.props.openCameraSim}
            onCapture={() => console.log('on capture')}
            onClose={() => this.props.openCameraForSim(false) }
          /> } */}

          { this.props.openCameraKtp && 
          <MCameraKtp 
            visible={this.props.openCameraKtp}
            onCapture={() => console.log('on capture')}
            onClose={() => this.props.openCameraForKtp(false) }
          /> }
      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ temporaryForm, camera, language }) => {
  const { tempPersonalDetail } = temporaryForm;
  const { openCameraSim, captureSim, openCameraKtp, captureKtp } = camera;
  const { lang } = language
  return { tempPersonalDetail, openCameraSim, captureSim, openCameraKtp, captureKtp, lang }
}

export default connect(mapStateToProps, {
  openCameraForKtp,
  setTempCoordinatorDetail
})(CoordinatorInformation)
