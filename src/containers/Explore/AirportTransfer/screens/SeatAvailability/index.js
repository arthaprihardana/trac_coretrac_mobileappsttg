import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
} from "@variables";
import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import styles from "./style";

export default class SeatAvailability extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderImage() {
    return (
      <View style={styles.image}>
        <Image
          source={require("@images/png/bus-seat-availability.png")}>
        </Image>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderText1() {
    return (
      <View style={styles.text}>
        <Text
          size={25}>
          Seat may not be enough
        </Text>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderText2() {
    return (
      <View style={styles.text}>
        <Text
          size={24}
          color={appVariables.colorGray}>
          The buses that you order have
        </Text>
        <Text
          size={24}
          color={appVariables.colorGray}>
          only 70 total seats,
        </Text>
        <Text
          size={24}
          color={appVariables.colorGray}>
          still want to continue?
          </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderButton() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
      }}>
        <View
          style={{
            flex: 1,
            paddingTop:10,
          }}>
          <TouchableOpacity
            style={{
              flex: -1,
              borderRadius: 7,
              height:47,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              backgroundColor: 'white',
            }}
            onPress={() => onPress()}>
            <Text color={appVariables.colorBlack}>
              ADD BUS
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{ flex: 0.25 }}>

        </View>
        <View
          style={{
            flex: 1,
          }}>
          <MainButton
            label={"CONTINUE"}
            rounded
          />
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------
  render() {
    return (
      <View style={styles.container}>
        {this._renderImage()}
        {this._renderText1()}
        {this._renderText2()}
        {this._renderButton()}
      </View>
    );
  }
}