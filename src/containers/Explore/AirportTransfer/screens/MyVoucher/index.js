/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-28 09:30:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 10:18:13
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Clipboard,
  ToastAndroid
} from "react-native";
import {
  appVariables,
} from "@variables";
import Lang from '../../../../../assets/lang';
import { connect } from "react-redux";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import { Actions } from "react-native-router-flux";

class MyVoucher extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  _copy = (code) => {
    Clipboard.setString(code);
    ToastAndroid.show(Lang.successfullyCopyPromo[this.props.lang], ToastAndroid.SHORT);
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderMyVoucherCard() {
    return (
      <View style={styles.myVoucher.wrapper}>

        {/* period promo */}
        <View style={styles.periodPromo.wrapper}>
          <View style={{
            flex: 1,
            flexDirection: "row"
          }}>
            <View style={{
              flex: 1.5,
              flexDirection: "column"
            }}>
              <Text size={24}>PROMO AYO MUDIK! 2019</Text>
            </View>
            <ImageBackground source={require('@images/png/voucher/periodPromo.png')} style={{ height: 40, width: 150 }}>
              <View style={styles.periodPromo.textWrapper}>
                <Text size={14} color={appVariables.colorWhite}>Periode Promo</Text>
                <Text size={14} color={appVariables.colorWhite}>-</Text>
              </View>
            </ImageBackground>
          </View>
          <Text
            size={18}
            color="#8A959E"
            style={{ paddingTop: 20 }}>
            Mau dapat voucher diskon 30% + 17% untuk sewa mobil lepas kunci?
            Yuk pesan
          </Text>
        </View>

        {/* coupon code */}
        <View style={{
          flex: 1,
          flexDirection: "row",
        }}>
          <View style={styles.couponCode.wrapper}>
            <View style={styles.couponCode.dashedLine} />
            <View style={styles.couponCode.buttonWrapper}>
              <Text size={18} color={appVariables.colorWhite} style={{ paddingTop: 20 }}>
                {Lang.couponCode[this.props.lang]}
              </Text>
              <View style={{ paddingVertical: 10}}>
                <View style={styles.couponCode.couponCodeLabelWrapper}>
                  <View style={styles.couponCode.couponCodeLabelStyle}></View>
                  <Text
                    size={14}
                    color={appVariables.colorWhite}
                    style={{ paddingVertical: 13 }}>
                    TRACKPROMO200K</Text>
                </View>
              </View>
              <View>
                <TouchableOpacity style={styles.couponCode.copyPromo} onPress={() => this._copy('TRACKPROMO200K')}>
                  <Text size={14} color={appVariables.colorWhite}>{Lang.copyPromo[this.props.lang]}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderMyVouchers() {
    var myVouchers = [];
    var myVouchersAmount = 1;
    for (let i = 0; i < myVouchersAmount; i++) {
      myVouchers.push(
        <View key={i}>
          {this._renderMyVoucherCard()}
        </View>
      )
    }
    return (
      <View>
        {myVouchers}
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={Lang.myVoucher[this.props.lang]}
          onBackPress={() => Actions.pop() } />
        <ScrollView style={{
          flex: 1,
        }}>
          {this._renderMyVoucherCard()}
          {/* {this._renderMyVouchers()} */}
        </ScrollView>
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(MyVoucher)
