/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 14:18:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 21:15:24
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ListView,
  Clipboard,
  ImageBackground,
  Alert,
  FlatList,
  RefreshControl
} from "react-native";
import { H2, p, ListItem } from 'native-base';
import {
  appVariables,
  appMetrics
} from "@variables";
import { connect } from "react-redux";
import Placeholder from 'rn-placeholder';
import Lang from '../../../../../assets/lang';

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import ImageHeader from "@airport-transfer-fragments/ImageHeader";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import NewsDetail from "../NewsDetail";
import { getContentArticle } from '../../../../../actions'

class NewsEvent extends Component {
  
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.feature = [{id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}, {id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}];
    this.guidance = [{id: '1' , value: 'guidance in January'}, {id: '1' , value: 'guidance in February'}, {id: '1' , value: 'guidance in March'}, {id: '1' , value: 'guidance in April'}, {id: '1' , value: 'guidance in May'}, {id: '1' , value: 'guidance in June'}, {id: '1' , value: 'guidance in July'}, {id: '1' , value: 'guidance in August'}, {id: '1' , value: 'guidance in September'}, {id: '1' , value: 'guidance in October'}, {id: '1' , value: 'guidance in November'}, {id: '1' , value: 'guidance in December'}, {id: '1' , value: 'guidance in January'}, {id: '1' , value: 'guidance in February'}, {id: '1' , value: 'guidance in March'}, {id: '1' , value: 'guidance in April'}, {id: '1' , value: 'guidance in May'}, {id: '1' , value: 'guidance in June'}, {id: '1' , value: 'guidance in July'}, {id: '1' , value: 'guidance in August'}, {id: '1' , value: 'guidance in September'}, {id: '1' , value: 'guidance in October'}, {id: '1' , value: 'guidance in November'}, {id: '1' , value: 'guidance in December'}];
    this.news = [{id: '1' , value: 'news in January'}, {id: '1' , value: 'news in February'}, {id: '1' , value: 'news in March'}, {id: '1' , value: 'news in April'}, {id: '1' , value: 'news in May'}, {id: '1' , value: 'news in June'}, {id: '1' , value: 'news in July'}, {id: '1' , value: 'news in August'}, {id: '1' , value: 'news in September'}, {id: '1' , value: 'news in October'}, {id: '1' , value: 'news in November'}, {id: '1' , value: 'news in December'}, {id: '1' , value: 'news in January'}, {id: '1' , value: 'news in February'}, {id: '1' , value: 'news in March'}, {id: '1' , value: 'news in April'}, {id: '1' , value: 'news in May'}, {id: '1' , value: 'news in June'}, {id: '1' , value: 'news in July'}, {id: '1' , value: 'news in August'}, {id: '1' , value: 'news in September'}, {id: '1' , value: 'news in October'}, {id: '1' , value: 'news in November'}, {id: '1' , value: 'news in December'}];
    this.event = [{id: '1' , value: 'event in January'}, {id: '1' , value: 'event in February'}, {id: '1' , value: 'event in March'}, {id: '1' , value: 'event in April'}, {id: '1' , value: 'event in May'}, {id: '1' , value: 'event in June'}, {id: '1' , value: 'event in July'}, {id: '1' , value: 'event in August'}, {id: '1' , value: 'event in September'}, {id: '1' , value: 'event in October'}, {id: '1' , value: 'event in November'}, {id: '1' , value: 'event in December'}, {id: '1' , value: 'event in January'}, {id: '1' , value: 'event in February'}, {id: '1' , value: 'event in March'}, {id: '1' , value: 'event in April'}, {id: '1' , value: 'event in May'}, {id: '1' , value: 'event in June'}, {id: '1' , value: 'event in July'}, {id: '1' , value: 'event in August'}, {id: '1' , value: 'event in September'}, {id: '1' , value: 'event in October'}, {id: '1' , value: 'event in November'}, {id: '1' , value: 'event in December'}];
    this.state = { 
      btnOne : 3,
      btnTwo : 0,
      btnThre :  0,
      btnFour :  0,
      datacontent : [],//[{id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}, {id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, {id: '1' , value: 'Feature in May'}, {id: '1' , value: 'Feature in June'}, {id: '1' , value: 'Feature in July'}, {id: '1' , value: 'Feature in August'}, {id: '1' , value: 'Feature in September'}, {id: '1' , value: 'Feature in October'}, {id: '1' , value: 'Feature in November'}, {id: '1' , value: 'Feature in December'}],
      btnobj : [
        {label:"Aturan Privasi", id:1},
        {label:"Token TRAC", id:2},
        {label:"Cookie", id:3}
      ],
      refreshing: false
    };
    this.onPress = this.onPress.bind(this);
    
  }

  componentDidMount() {
    this.props.getContentArticle();
  }
  
  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.articles !== prevProps.articles) {
      return { articles: this.props.articles }
    }
    return null; 
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(snapshot !== null) {
      if(snapshot.articles) {
        this.setState({
          datacontent: snapshot.articles.reverse(),
          refreshing: false
        })
      }
    }
  }
  
  
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  onPress = (txt) => {
    switch(txt) {
      case '1':
        this._removeActive();
        this.setState({btnOne : 3, datacontent : this.feature});
        break;
      
      case '2':
        this._removeActive();
        this.setState({btnTwo : 3, datacontent : this.guidance});
        break;
 
      case '3':
        this._removeActive();
        this.setState({btnThree : 3, datacontent : this.news});
        break;
 
      case '4':
        this._removeActive();
        this.setState({btnFour : 3, datacontent : this.event});
        break;
      }
  }

  _removeActive = ()=> {
    this.setState({
      btnOne : 0, btnTwo : 0, btnThree : 0, btnFour : 0,
    });
  }
  
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  
  // ---------------------------------------------------
  _renderContent(item, key) {
    return (          
      <View style={{ width: "50%" }} key={key}> 
        <TouchableOpacity onPress={() => Actions.push("NewsDetail", { item: item })} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}> 
          <View style={{ flex: 1, flexDirection: "column" }}>
            <Image source={{ uri: item.image }} resizeMode="cover" style={{ width: 150, height: 100 }} />
            <Text style={{fontWeight: '400'}} size={14} color={appVariables.colorGray}>{item.title}</Text>
          </View>
        </TouchableOpacity>
      </View>
     
    );
  }
 
  _renderLoadingContent(key) {
    return (
      <View style={{ width: "50%" }} key={key}> 
        <View style={{ flex: 1, marginBottom: 20 }}>
          <View style={{ flex: 1, flexDirection: "column" }}>
            <Placeholder.Media
              color="#1b1f230d"
              size={170}
              onReady={!this.props.loading}
              />
            <View style={{ marginBottom: 10 }} />
            <Placeholder.Line
              color="#1b1f230d"
              width="77%"
              textSize={14}
              onReady={!this.props.loading}
              />
          </View>
        </View>
      </View>
    )
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.loading ? [1,2,3,4,5,6] : this.state.datacontent}
          ListHeaderComponent={() => <ImageBackground source={require('@images/png/bannerContent/bannerNews.png')} style={{width: '100%'}}>
            <ImageHeader title={Lang.news[this.props.lang]} onBackPress={() => Actions.pop()} />
            <View style={{marginHorizontal: 20, marginTop: 70, marginBottom: 50}}>
              <H2 style={{color: 'white'}}>{Lang.newsSlideShow[this.props.lang]}</H2>
            </View>
            </ImageBackground>
          }
          ListFooterComponent={() => <View style={{marginVertical: 20, width: 200, height: 40, alignSelf: "center"}}>
            <TouchableOpacity style={styles.copyPromo.copyDetailButton}>
              <Text size={14}>{Lang.loadMore[this.props.lang]}</Text>
            </TouchableOpacity>
          </View>}
          numColumns={2}
          horizontal={false}
          columnWrapperStyle={{ padding: 20 }}
          onRefresh={() => {
            this.setState({refreshing: true});
            this.props.getContentArticle();
          } }
          refreshing={this.state.refreshing}
          keyExtractor={(item, index) => index}
          renderItem={({item, index}) => this.props.loading ? this._renderLoadingContent(index) : this._renderContent(item, index)}
        />
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ contentArticle, language }) => {
  const { loading, articles } = contentArticle;
  const { lang } = language;
  return { loading, articles, lang }
}

export default connect(mapStateToProps, {
  getContentArticle
})(NewsEvent);
