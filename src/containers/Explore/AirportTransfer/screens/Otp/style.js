import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  form: {
    pin: {
      container: {
        flex: -1,
        flexDirection: "row",
        height: 50,
        marginTop: 40,
        marginBottom: 20,
      },
      
      field: {
        flex: -1,
        width: 40,
        borderBottomWidth: 1,
        borderColor: "#cccccc",
        textAlign: "center",
        fontWeight: "bold",
        marginRight: 10,
      },
    },
  },
}