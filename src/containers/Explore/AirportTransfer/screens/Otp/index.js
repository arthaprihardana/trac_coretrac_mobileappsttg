/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-28 11:12:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 17:11:51
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Text from "@airport-transfer-fragments/Text";
import TextInput from "@airport-transfer-fragments/TextInput";
import MainButton from "@airport-transfer-fragments/MainButton";
import AuthContainer from "@airport-transfer-fragments/AuthContainer";

import styles from "./style";
import Lang from '../../../../../assets/lang';

class Otp extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        pin: ["", "", "", "", "", ""],
      },

      formActiveIndex: 0,
    };

    this.pinField = [null, null, null, null, null, null];
  }


// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangePinFormField(index, value) {
    const form = this.state.form;
    

    if (value == "BACKSPACE") {
      if (index > 0) {
        if (index == 5 && form.pin[index] != "") {
          form.pin[index] = "";
          newIndex = index;
        } else {
          form.pin[index - 1] = "";
          newIndex = index - 1;
        }
      }
    } else if (value != "") {
      form.pin[index] = value;
      newIndex = index + 1;
    }

    if (newIndex >= 0 && newIndex <=5) {
      this.pinField[newIndex].focus();
    }

    const formActiveIndex = newIndex;

    this.setState({
      form,
      formActiveIndex,
    });
  }

// ---------------------------------------------------

  onPinFormFieldFocus() {
    this.pinField[this.state.formActiveIndex].focus();
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// --------------------------------------------------- 

  goToOtp() {
    this.props.navigation.replace("ResetPassword", {
      navData: this.props.navigation.getParam("navData", {}),
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderPinformField(index) {
    return (
      <TextInput
        getRef={ (ref) => this.pinField[index] = ref }
        onChangeText={ (value) => this.onChangePinFormField(index, value) }
        onKeyPress={({ nativeEvent }) => {
          nativeEvent.key === 'Backspace' ? this.onChangePinFormField(index, "BACKSAPCE") : {}
        }}
        value={ this.state.form.pin[index] }
        size={ 25 }
        style={ styles.form.pin.field }
        keyboardType="numeric"
        maxLength={ 1 }
        onFocus={ () => this.onPinFormFieldFocus() }
      />
    );
  }

// ---------------------------------------------------

  _renderForm() {
    return (
      <View>
        <Text style={{
          paddingTop: 10,
        }}>
          {Lang.forgotSentVerification[this.props.lang]} {this.props.email}. 
          {Lang.forgotPasswordNote[this.props.lang]}
        </Text>

        {/* <View style={ styles.form.pin.container }>
          
          { this._renderPinformField(0) }
          { this._renderPinformField(1) }
          { this._renderPinformField(2) }
          { this._renderPinformField(3) }
          { this._renderPinformField(4) }
          { this._renderPinformField(5) }

        </View>

        <TouchableOpacity>
          <Text color="navy">
            Resend verification code?
          </Text>
        </TouchableOpacity>

        <MainButton
          label={ "SEND" }
          onPress={ () => this.goToOtp() }
          rounded
          style={{
            marginTop: 40,
          }}
        /> */}

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <AuthContainer 
        { ... this.props }
        title={ "Verification" }
      >

        { this._renderForm() }        

      </AuthContainer>
    );
  }
}

const mapStateToProps = ({ authentication, language }) => {
  const { forgotPassword } = authentication;
  const { lang } = language;
  return { forgotPassword, lang }
}

export default connect(mapStateToProps, {})(Otp);