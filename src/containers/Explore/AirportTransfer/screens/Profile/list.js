/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-28 11:54:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-29 11:26:27
 */
import Lang from '../../../../../assets/lang';
export default [
  {
    iconList: "icoAccount",
    // textList : "Account",
    textList : Lang.tracAccount,
    listTarget :'Account',
  },
  {
    iconList: "icoIdentificationInformation",
    // textList : "Identification Information",
    textList: Lang.identificationInformation,
    listTarget : 'IdentificationInformation',
  },
  // {
  //   iconList: "icoCreditCards",
  //   textList : "Credit Cards",
  //   listTarget : 'ProfileCreditCard',
  // },
  {
    iconList: "icoChangePassword",
    // textList : "Change Password",
    textList : Lang.changePassword,
    listTarget : 'ChangePassword',
  },
  {
    iconList: "icoHistoryBooking",
    // textList : "History Booking",
    textList : Lang.historyBooking,
    listTarget : 'HistoryBooking',
  },
  // {
  //   iconList: "icoMessage",
  //   textList : "Message",
  //   listTarget : 'Message',
  // },
  // {
  //   iconList: "icoMyVoucher",
  //   textList : "My Voucher",
  //   listTarget : '',
  // },
  {
    iconList: "icoLogout",
    textList : "Logout",
    listTarget : 'Home',
  },
]

            

        
              
            
