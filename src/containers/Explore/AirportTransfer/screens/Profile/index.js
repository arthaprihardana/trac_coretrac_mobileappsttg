/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-12 15:09:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-24 00:43:30
 */
import React, { Component } from 'react';
import { Content, Text,Thumbnail, List } from 'native-base';
import { connect } from "react-redux";
import SvgIcon from '../../../../../utils/SvgIcon';
import { View, TouchableOpacity, Image } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import listItems from './list';
import { Actions } from "react-native-router-flux";
import { postLogout } from "../../../../../actions"
import Lang from '../../../../../assets/lang';
import firebase from 'react-native-firebase';

class Profile extends Component {
  
  _renderListitem(iconList, textList, onPress, index) {
    return (
      <TouchableOpacity style={{
        flex: -1,
        flexDirection: "row",
        borderBottomWidth: 1,
        height: 50,
        alignItems: "center",
        borderColor: "#D8D8D8"
      }}
        onPress={() => onPress()}
        key={index}>
        <View style={{
          flex: -1
        }}>
          <SvgIcon name={iconList} />
        </View>
        <View style={{
          flex: 1,
          paddingLeft: 10
        }}>
          <Text style={{ fontSize: 12 }}>{ textList === "Logout" ? Lang.logout[this.props.lang] : textList[this.props.lang]}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { isLogin, login } = this.props;
    const nav = this.props.navigation;
    let photo = isLogin ? login.Data.ImagePath !== null ? { uri: `${login.Data.ImagePath}?random_number=${new Date().getTime()}` } : require("@images/png/avatar/profileAvatar.png") : require("@images/png/avatar/profileAvatar.png");
    return (
      <BaseContainer
        hasFooter>
      <Header
        title={Lang.myProfile[this.props.lang]}/>
        <Content style={{ backgroundColor: 'white' }}>
          <Grid>
            <Row style={{ backgroundColor: '#21419A', height: 160 }}>
              <Col style={{ justifyContent: 'center', alignItems: 'center', width: '25%', marginLeft: 10, backgroundColor: '#21419A' }}>
                {/* <Thumbnail style={{ width: 74, height: 74, borderRadius: 37 }} source={photo}/> */}
                <View style={{ width: 74, height: 74, borderRadius: 37, backgroundColor: '#cccccc' }}>
                  <Image 
                    style={{ width: 74, height: 74, borderRadius: 37 }} 
                    source={photo}
                    resizeMode="cover"
                    progressiveRenderingEnabled={true} />
                </View>
              </Col>
              <Col style={{ justifyContent: 'flex-start', alignItems: 'flex-start', width: '75%', marginLeft: 10, marginTop: 10, backgroundColor: '#21419A' }}>
                <Row>
                  <Col style={{ width: '100%', backgroundColor: '#21419A' }}>
                    <Text style={{ color: '#8A959E', fontSize: 12 }}>{Lang.ktpNumber[this.props.lang]}</Text>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{isLogin && login.Data.NoKTP !== null ? login.Data.NoKTP : "-"}</Text>
                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '100%', backgroundColor: '#21419A' }}>
                    <Text style={{ color: '#8A959E', fontSize: 12 }}>{Lang.phoneNumber[this.props.lang]}</Text>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{isLogin && login.Data.NoHandphone !== null ? login.Data.NoHandphone : "-"}</Text>
                  </Col>
                </Row>
                <Row>
                  <Col style={{ width: '100%', backgroundColor: '#21419A' }}>
                    <Text style={{ color: '#8A959E', fontSize: 12 }}>{Lang.email[this.props.lang]}</Text>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{isLogin && login.Data.EmailPersonal !== null ? login.Data.EmailPersonal : "-"}</Text>
                  </Col>
                </Row>
              </Col>
              <Col>
              </Col>
            </Row>
          </Grid>

          <List style={{ backgroundColor: 'white', paddingHorizontal: 20, }}>
            {listItems.map((item, index) => {
              return this._renderListitem(item.iconList, item.textList, () => {
                if(item.textList === "Logout") {
                  firebase.messaging().unsubscribeFromTopic(this.props.login.Data.Id);
                  this.props.postLogout();
                  Actions.reset("Home")
                } else {
                  Actions.push(item.listTarget)
                }
              },index);
            })}
          </List>
        </Content>
        </BaseContainer>
    )
  }
}

const mapStateToProps = ({ authentication, language }) => {
  const { login, isLogin } = authentication;
  const { lang } = language;
  return { login, isLogin, lang };
}

export default connect(mapStateToProps, {
  postLogout
})(Profile);
