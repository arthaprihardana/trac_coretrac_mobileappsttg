import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import MainButton from "@airport-transfer-fragments/MainButton";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import styles from "./style";
import { Actions } from "react-native-router-flux";

export default class DriverReview extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderQuestion() {
    return (
      <View style={styles.questionWrapper}>
        <Text size={18}>
          How was your ride today, Bryan?
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderDescription() {
    return (
      <View style={styles.descriptionWrapper}>
        <Text
          color={appVariables.colorGray}
          size={14}>
          Please rate your experience today
        </Text>
        <Text
          color={appVariables.colorGray}
          size={14}>
          to make us better.
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderImage() {
    return (
      <View style={styles.imageWrapper}>
        <Image
          source={require('@images/png/stars-rate.png')}>
        </Image>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderTwoButton(label1, label2) {
    return (
      <View style={styles.buttonWrapper}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <TouchableOpacity
            style={styles.grayButtonStyle}
            onPress={() => onPress()}>
            <Text size={12} color={appVariables.colorGray}>
              {label1}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{
          flex: 0.10,
          flexDirection: 'column',
        }}>
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <TouchableOpacity
            style={styles.grayButtonStyle}
            onPress={() => onPress()}>
            <Text size={12}color={appVariables.colorGray}>
              {label2}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderOneButton(label1) {
    return (
      <View style={styles.buttonWrapper}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <TouchableOpacity
            style={styles.grayButtonStyle}
            onPress={() => onPress()}>
            <Text size={12} color={appVariables.colorGray}>
              {label1}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderComment() {
    return (
      <View>
        <FloatingTextInput
          label={"add your comment"} />
      </View>
    );
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={""}
          onBackPress={() => this.props.navigation.pop()} />
        <View style={styles.mainContainer}>
          {this._renderQuestion()}
          {this._renderDescription()}
          {this._renderImage()}
          <View style={{
            flex: 1,
            flexDirection: 'row',
            paddingVertical: 5,
          }}>
            {this._renderTwoButton("FRIENDLY DRIVER", "EXTENSIVE KNOWLEDGE")}
          </View>

          <View style={styles.buttonSeparator}>
            {this._renderTwoButton("FAST AND SPEED REPLY", "GIVE MORE HELP")}
          </View>

          <View style={styles.buttonSeparator}>
            {this._renderOneButton("USEFULLY RELAXED")}
          </View>

          <View style={{
            flex: -1,
            flexDirection: 'row',
            paddingVertical: 5,
          }}>
            <View style={{
              flex: 1,
              flexDirection: 'column',
            }}>
              {this._renderComment()}
            </View>
          </View>
        </View>
        <MainButton
          onPress={() => {
            this.props.navData.onComplete();
            Actions.pop();
          }}
          label={"SUBMIT"}>
        </MainButton>
      </BaseContainer>
    );
  }
}