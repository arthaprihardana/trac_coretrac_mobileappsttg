import { appVariables,appMetrics } from "@variables";

export default {
  mainContainer:{
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 20,
    paddingVertical:appMetrics.windowHeight/15,
  },
  questionWrapper:{
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  descriptionWrapper:{
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
  },
  imageWrapper:{
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
  },
  buttonWrapper:{
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  orangeButtonStyle:{
    borderRadius: 7,
    flex: -1,
  },
  grayButtonStyle:{
    flex: -1,
    borderRadius: 7,
    height: 60,
    borderWidth: 1,
    borderColor:appVariables.colorGray,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSeparator:{
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 5,
  }
}