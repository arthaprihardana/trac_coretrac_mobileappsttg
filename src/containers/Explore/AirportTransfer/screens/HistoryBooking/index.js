/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 16:53:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-02 10:11:53
 */
import React, { Component } from 'react';
import { ActivityIndicator, FlatList, View, Image, TouchableOpacity } from 'react-native';
import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import { Content } from 'native-base';
import { OHistoryBookingSection } from '../../../../../components/organisms';
import Header from "@airport-transfer-fragments/Header";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import { connect } from "react-redux";
import { getMyHistory, getVehicleAttribute } from "../../../../../actions";
import styles from './style';
import { STATUS_RESERVATION, RENTAL_TIMEBASE, RENTAL_KMBASE, RENTAL_HOURLY, UAS_TIMEBASE, UAS_KMBASE } from '../../../../../constant';
import _ from "lodash";
import Lang from '../../../../../assets/lang';
import { appVariables } from '../../../../../assets/styles';
import moment from "moment";
import "moment/locale/id";
import { Actions } from 'react-native-router-flux';

class HistoryBooking extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: null,
      totalPage: null,
      totalData: null,
      data: [],
      vehicle: []
    };
  }
  
  componentDidMount = () => {
    this.props.getVehicleAttribute({ token: this.props.login.token });
    this.props.getMyHistory({
      PIC: this.props.login.Data.Id,
      token: this.props.login.token
    });
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.myHistory !== prevProps.myHistory) {
      return { myHistory: this.props.myHistory }
    }
    if(this.props.vehicle !== prevProps.vehicle) {
      return { vehicle: this.props.vehicle }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.myHistory !== undefined) {
        this.setState({
          data: snapshot.myHistory,
          currentPage: this.props.currentPage,
          totalPage: this.props.totalPage,
          totalData: this.props.totalData,
        })
      }
      if(snapshot.vehicle !== undefined) {
        this.setState({
          vehicle: snapshot.vehicle
        })
      }
    }
  }

  goToCarDetail(item) {
    Actions.CarDetail({
      currentPage: "My History",
      item: item
    });
  }

  _renderCarRentalTitle(item) {
    let productName = null;
    let status = _.filter(STATUS_RESERVATION, {id: item.Status});
    let x = _.filter(this.props.products, { MsProductId: item.details[0].MsProductId });
    let y = _.filter(x[0].product_service, { ProductServiceId: item.details[0].MsProductServiceId });
    let serviceName = y[0] !== undefined ? y[0].ProductServiceName : null;
    switch (item.ServiceTypeId) {
      case RENTAL_TIMEBASE:
        productName = "Car Rental";
        break;
      case RENTAL_KMBASE:
        productName = "Airport Transfer";
        break;
      case RENTAL_HOURLY:
        productName = "Airport Transfer";
        break;
      case UAS_TIMEBASE:
        productName = "Bus Rental";
        break;
      case UAS_KMBASE:
        productName = "Bus Rental";
        break;
      default:
        break;
    }
    return (
      <View style={styles.carTitle.container}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-start'
        }}>
          <Text size={20}>{productName}</Text>
          <Text size={10} style={{ color: appVariables.colorGray }}>{serviceName}</Text>
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-end'
        }}>
          <Text
            size={14}
            color={appVariables.colorOrange}>
            {_.replace(_.startCase(_.toLower(status[0].name)), /_/g, " ")}
          </Text>
        </View>
      </View>
    );
  }

  _renderCarSwiper(item) {
    let f = _.filter(this.state.vehicle, { vehicleTypeId: item.details[0].UnitTypeId });
    let image = this.state.vehicle.length > 0 ? { uri: f[0].vehicleImage } : require('../../../../../assets/images/png/cars/Avanza.png')
    return (
        <Image
          source={image}
          style={{ height: 80, width: 150, resizeMode: "contain" }} />
    );
  }

  _renderCarTotal(item) {
    return (
      <View style={{ flexDirection: 'column', marginBottom: 10 }}>
        <Text
          size={11}
          color={appVariables.colorGray}>
          {Lang.carTotal[this.props.lang]}
        </Text>

        <Text
          size={14}
          color={appVariables.colorBlack}
          style={{ paddingTop: 0 }}>
          {item.details.length} Cars
        </Text>
      </View>
    );
  }

  _renderBookingNumber(item) {
    return (
      <View style={{ flexDirection: 'column' }}>
        <Text
          size={11}
          color={appVariables.colorGray}>
          {Lang.reservationNumber[this.props.lang]}
        </Text>

        <Text
          size={14}
          color={appVariables.colorBlack}
          style={{ paddingTop: 0 }}>
          {item.ReservationId}
        </Text>
      </View>
    );
  }

  _renderDates() {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        paddingTop: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <Text
            size={11}
            color={appVariables.colorGray}>
            {Lang.dates[this.props.lang]}
          </Text>
        </View>
      </View>
    );
  }

  _renderDetails(item) {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        paddingVertical: 10,
      }}>
        <View style={{
          flex: 2,
          flexDirection: 'row',
        }}>
          <Text
            size={14}
            color={appVariables.colorBlack}>
            {(new moment(item.details[0].StartDate, "YYYY-MM-DD HH:mm:ss")).format("LL")} {item.details[0].EndDate !== null && `- ${(new moment(item.details[0].EndDate, "YYYY-MM-DD HH:mm:ss")).format("LL")}`}
            </Text>
        </View>

        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end'
          }}
          onPress={() => this.goToCarDetail(item)}>
          <View style={{
            // flex: 2,
            flexDirection: 'row',
            alignItems: 'center'
            }}>
            <Text
              size={12}
              color={appVariables.colorBlack}>
              {Lang.detail[this.props.lang]}
            </Text>
            <Icon
              name="arrow-right"
              size={12}
              color={appVariables.colorOrange}
              style={{ marginLeft: 10 }} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  
  _renderCarRentals(item, index) {
    return (
      <View style={styles.mainContainer} key={index}>
        {this._renderCarRentalTitle(item)}
        <View style={{
          flex: -1,
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: 10,
        }}>
          {this._renderCarSwiper(item)}
          <View>
            {this._renderCarTotal(item)}
            {this._renderBookingNumber(item)}
          </View>
        </View>
        {this._renderDates()}
        {this._renderDetails(item)}
      </View>
    );
  }
  
  render() {
    return (
      <BaseContainer>
        <Header
          title={Lang.historyBooking[this.props.lang]}
          onBackPress={ () => this.props.navigation.pop()}
        />
        <Content style={{backgroundColor:'white'}}>
          {/* <OHistoryBookingSection /> */}
          <FlatList 
            keyExtractor={(item, index) => index.toString()}
            data={this.state.data}
            renderItem={({item, index}) => this._renderCarRentals(item, index) }
            ListEmptyComponent={() => this.state.currentPage === null && this.props.loading ? <ActivityIndicator size="small" color="#2A2E36" style={{ marginTop: 20 }} /> : null }
            />
        </Content>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ history, authentication, vehicleAttribute, productService, language }) => {
  const { myHistory, loading } = history;
  const { login } = authentication;
  const { vehicle } = vehicleAttribute;
  const { products } = productService;
  const { lang } = language;
  return { loading, myHistory, login, vehicle, products, lang };
}

export default connect(mapStateToProps, {
  getMyHistory,
  getVehicleAttribute
})(HistoryBooking);
