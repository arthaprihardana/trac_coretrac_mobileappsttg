import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {ORateDriver} from '../../../../../../components/organisms';

class ModalRateDriver extends Component{
  render(){
    let {action} = this.props;
    return(
      <ORateDriver action={action}>
      </ORateDriver>
    )
  }
}

ModalRateDriver.propTypes={
  action: PropTypes.func
}

export default ModalRateDriver;