import {StyleSheet} from 'react-native';
import { appVariables,appMetrics } from '@variables';

export default {
  carTitle:{
    container:{
      flex: 1,
      flexDirection: 'row',
      paddingVertical: 10,
      borderBottomWidth: 1,
      borderBottomColor: "#D8D8D8",
    },
  },
  mainContainer:{
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderBottomColor: "#D8D8D8",
    borderBottomWidth: 10,
    backgroundColor:appVariables.colorWhite,
  },
}

// const styles = StyleSheet.create({
//   titleLabel:{
//     fontSize: 14,
//     color: appVariables.colorGray,
//     fontFamily: appVariables.museo500,
//     lineHeight: 20,
//     marginTop:0,
//   },
//   headerLabel:{
//     fontSize: 14,
//     color: appVariables.colorWhite,
//     fontFamily: appVariables.museo500,
//     flex: 1,
//     lineHeight: 20,
//     marginTop:0,
//   },
//   floatingLabel:{
//     marginLeft:40,
//     marginRight:40,
//   },
//   bottomButton:{
//     width: appMetrics.windowWidth, bottom: 0, position: 'absolute', height: 60,alignSelf: 'stretch',
//   },
//   closeButton:{
//     top:0,
//   },

//   carTitle:{
//     container:{
//       flex: 1,
//       flexDirection: 'row',
//       paddingVertical: 10,
//       borderBottomWidth: 1,
//       borderBottomColor: "#D8D8D8",
//     },
//   },
//   mainContainer:{
//     paddingVertical: 20,
//     paddingHorizontal: 20,
//     borderBottomColor: "#D8D8D8",
//     borderBottomWidth: 10,
//     backgroundColor:appVariables.colorWhite,
//   },
// })

// export default styles;