import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  outerContainer: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appVariables.colorWhite,
  },

  category: {
    grouper: {
      flex: 1,
      flexWrap: "wrap",
      flexDirection: "row",
      paddingLeft: 20,
      paddingTop: 20,
    },

    container: {
      flex: -1,
      width: (appMetrics.screenWidth - (40 + 10)) / 3,
      justifyContent: "center",
      alignItems: "center",
      padding: 10,
      paddingTop: 20,
      paddingBottom: 0,
      marginRight: 5,
      marginBottom: 20,
      borderColor: "#dddddd",
      backgroundColor: appVariables.colorWhite,
      shadowOffset:{  width: 0,  height: 0,  },
      shadowColor: 'black',
      shadowOpacity: .2,
      shadowRadius: 7,
      elevation:2,
    },

    image: {
      container: {
        flex: -1,
        height: 80,
        width: 80,
        marginBottom: 10,
        justifyContent: "center",
        alignItems: "center",
      },
    },

    main: {
      container: {
        flex: -1,
        height: 40,
        marginBottom: 20,
        justifyContent: "center",
        alignItems: "center",
      },

      title: {
        paddingBottom: 4,
        textAlign: "center",
      },
    },

    control: {
      container: {
        flex: -1,
        width: (appMetrics.screenWidth - (40 + 10)) / 3,
        height: 40,
        borderTopWidth: 1,
        borderColor: "#dddddd",
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

  main: {
    container: {
      flex: -1,
      width: appMetrics.screenWidth,
    },

    tab: {
      grouper: {
        flex: -1,
        height: 40,
        borderBottomWidth: 1,
        borderColor: "#dddddd",
        marginBottom: 20,
      },

      innerGrouper: {
        paddingHorizontal: 20,
      },

      container: {
        flex: -1,
        justifyContent: "center",
        paddingHorizontal: 15,
      },
    },

    menu: {
      grouper: {
        flex: -1,
      },

      container: {
        flex: -1,
        padding: 20,
        marginBottom: 20,
        borderColor: "#dddddd",
        backgroundColor: appVariables.colorWhite,
        shadowOffset:{  width: 0,  height: 0,  },
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowRadius: 7,
        elevation:2,
      },

      top: {
        container: {
          flex: -1,
          flexDirection: "row",
          paddingBottom: 20,
          borderBottomWidth: 1,
          borderColor: "#dddddd",
        },

        image: {
          container: {
            flex: -1,
            width: 80,
            height: 80,
          },
        },

        detail: {
          container: {
            flex: 1,
            paddingLeft: 20,
          },

          title: {
            paddingBottom: 5,
          },
        },
      },

      bottom: {
        container: {
          flex: -1,
          flexDirection: "row",
          justifyContent: "space-between",
          paddingTop: 20,
        },

        innerContainer: {
          flex: -1,
          flexDirection: "row",
        },

        input: {
          flex: -1,
          height: 40,
          width: 70,
          borderWidth: 1,
          borderColor: "#bbbbbb",
          borderRadius: 4,
          marginLeft: 10,
          textAlign: "center",
        },
      },
    },
  },
}