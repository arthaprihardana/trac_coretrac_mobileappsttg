/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-15 13:51:01 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-03 14:29:28
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import { 
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Switch,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import TextInput from "@airport-transfer-fragments/TextInput";

import BookedVehicleDetailContainer from "@airport-transfer-fragments/BookedVehicleDetailContainer";

import NumberHelper from "@airport-transfer-helpers/Number";

import BusExtraServiceConfigs from "@airport-transfer-configs/data/BusExtraService";

import styles from "./style";


export default class BookedBusDetail extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    const navData = JSON.parse(JSON.stringify(props.navData));
    const list = navData.list.map((item, index) => {
      return {
        ... item,
        extras: BusExtraServiceConfigs.map((extra, extIndex) => {
          return {
            ... extra,
            value: extra.type === "number" ? 0 : false,
          }
        }),
      }
    });
  
    this.state = {
      navData: navData,

      list: list,
      checkoutData: navData.checkoutData,
      priceModifier: 0,

      activeListIndex: 0,
    };

    this.mainImageScroll = null;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  switchList(index) {
    if (this.state.activeListIndex == index) {
      return false;
    }
      
    if (index > this.state.list.length - 1) {
      index = 0;
    } else if (index < 0) {
      index = this.state.list.length - 1;
    }

    this.mainImageScroll.scrollTo({x: index * appMetrics.screenWidth});

    this.setState({
      activeListIndex: index,
    });
  }

// ---------------------------------------------------

  getTotalPrice() {
    let totalPrice = 0;
    this.state.list.map((item, index) => {
      totalPrice += item.price * this.state.checkoutData.days;
    });

    totalPrice += this.state.priceModifier;

    return totalPrice;
  }

// ---------------------------------------------------

  changeExtraValue(index, value, priceValueModifier) {
    const list = this.state.list;
    let priceModifier = this.state.priceModifier;

    if (value < 0) {
      value = 0;
      priceValueModifier = 0;
    }

    list[this.state.activeListIndex].extras[index].value = value;
    priceModifier += priceValueModifier;

    this.setState({
      value,
      priceModifier,
    })
  }

// ---------------------------------------------------

  removeList(index) {
    let newIndex = this.state.activeListIndex - 1;
    if (index == 0) {
      newIndex = 0;
    }
    const list = this.state.list;
    list.splice(index, 1);

    this.switchList(newIndex);

    this.setState({
      activeListIndex: newIndex,
      list,
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderExtraItemCategory(index, value, price) {
    return (
      <TouchableOpacity
        style={ styles.category.control.container }
        onPress={ () => this.changeExtraValue(index, value + 1, price) }
      >
        <Icon name="plus" color={ "black" } size={ 10 }/>
      </TouchableOpacity>
    )
  }

// ---------------------------------------------------
  _renderExtraItemNumberControl(index, value, price) {
    return (
      <View style={ styles.numberControl.container }>

        <TouchableOpacity
          style={ styles.numberControl.button.container }
          onPress={ () => this.changeExtraValue(index, value - 1, (price * -1)) }
        >
          <Icon name="minus" color={ "black" } size={ 10 }/>
        </TouchableOpacity>

        <View style={ styles.numberControl.value }>
          <Text>
            { value }
          </Text>
        </View>

        <TouchableOpacity
          style={ [styles.numberControl.button.container, {borderWidth: 0,}] }
          onPress={ () => this.changeExtraValue(index, value + 1, price) }
        >
          <Icon name="plus" color={ "black" } size={ 10 }/>
        </TouchableOpacity>

      </View>
    )
  }

// ---------------------------------------------------

  _renderExtraItemToggleControl(index, value, price) {
    return (
      <View style={ styles.toggleControl.container }>

        <Switch
          trackColor={{ true: appVariables.colorOrange, }}
          onValueChange={ (newValue) => this.changeExtraValue(index, !value, !value ? price : (price * -1)) }
          value={ value }
        />

      </View>
    )
  }

// ---------------------------------------------------

  _renderExtraCategory(title, icon, price, unit, value, type, index) {
    // let controlRender = this._renderExtraItemNumberControl(index, value, price);
    // if (type == "toggle") {
    //   controlRender = this._renderExtraItemToggleControl(index, value, price);
    // }

    return (
      <View
        style={ styles.category.container }
        key={ index }
      >
        <View style={ styles.category.image.container }>
          <Image
            resizeMode={ "contain" }
            source={ icon }
          />
        </View>

        <View style={ styles.category.main.container }>
          <Text color={ "black" } style={ styles.category.main.title }>
            { title.toUpperCase() }
          </Text>
        </View>

        { this._renderExtraItemCategory(index, value, price) }
      </View>
    );
  }

// ---------------------------------------------------

  _renderCategories() {
    return (
      <View style={ styles.category.grouper }>

        { this.state.list[this.state.activeListIndex].extras.map((extra, index) => {
          return this._renderExtraCategory(extra.title, extra.icon, extra.price, extra.unit, extra.value, extra.type, index);
        }) }

      </View>
    );
  }

// ---------------------------------------------------

  _renderTabItem(label, index) {
    return (
      <TouchableOpacity
        key={ index }
        style={ [styles.main.tab.container, (index === 0 ? { borderBottomWidth: 3, borderColor: appVariables.colorOrange,} : {})] }
      >
        <Text size={ 16 }>
          { label }
        </Text>
      </TouchableOpacity>
    );
  }


// ---------------------------------------------------

  _renderTabs() {
    return (
      <ScrollView
        style={ styles.main.tab.grouper }
        contentContainerStyle={ styles.main.tab.innerGrouper }
        horizontal
        showsHorizontalScrollIndicator={ false }
      >
        
        { this._renderTabItem("Mc Donald's", 0) }
        { this._renderTabItem("Dapur Solo", 1) }
        { this._renderTabItem("KFC", 2) }
        { this._renderTabItem("RM Sederhana", 3) }

      </ScrollView>
    );
  }

// ---------------------------------------------------

  _renderMenuItem(label, description, imageUrl, price, unit, index) {
    return (
      <View
        key={ index }
        style={ styles.main.menu.container }
      >

        <View style={ styles.main.menu.top.container }>

          <View style={ styles.main.menu.top.image.container }>
            <Image
              style={{flex: 1,}}
              resizeMode={ "contain" }
              source={{uri: imageUrl}}
            />
          </View>

          <View style={ styles.main.menu.top.detail.container }>
            <Text size={ 16 } style={ styles.main.menu.top.detail.title }>
              { label }
            </Text>

            <Text color={ appVariables.colorGray }>
              { description }
            </Text>
           </View>

        </View>

        <View style={ styles.main.menu.bottom.container }>

          <View style={ styles.main.menu.bottom.innerContainer }>
            <Text color={ appVariables.colorOrange } size={ 18 }>
              { this.state.checkoutData.currency } { NumberHelper.format(price, ".") }
            </Text>

            <Text color={ appVariables.colorGray } style={{paddingLeft: 4,}}>
              / { unit }
            </Text>
          </View>

          <View style={ styles.main.menu.bottom.innerContainer }>
            <Text size={ 15 }>
              Total
            </Text>

            <TextInput
              style={ styles.main.menu.bottom.input }
              value={ "0" }
              keyboardType={ "numeric" }
            />
          </View>

        </View>

      </View>
    );
  }

// ---------------------------------------------------

  _renderMenus() {
    return (
      <View style={ styles.main.menu.grouper }>

        { this._renderMenuItem("Cheese Burger Alacarte", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "https://www.pngarts.com/files/3/KFC-Burger-Transparent-Background-PNG.png", 25000, "pcs", 0) }
        { this._renderMenuItem("Coca Cola", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "http://pngimg.com/uploads/cocacola/cocacola_PNG2.png", 5000, "pcs", 1) }

      </View>
    );
  }

// ---------------------------------------------------

  // _renderExtraItem(title, icon, price, unit, value, type, index, available, stockType) {
  //   let icons = require("@images/png/extras/add_hour.png");
  //   let controlRender = this._renderExtraItemNumberControl(index, value, price, available, stockType);
  //   // if (type == "toggle") {
  //   //   controlRender = this._renderExtraItemToggleControl(index, value, price);
  //   // }
  //   if (type == "boolean") {
  //     controlRender = this._renderExtraItemToggleControl(index, value, price);
  //   }

  //   if(/Add/g.test(title)) {
  //     icons = require("@images/png/extras/add_hour.png");
  //   }
  //   if(/Out/g.test(title)) {
  //     icons = require("@images/png/extras/out_of_town.png");
  //   }
  //   if(/Over/g.test(title)) {
  //     icons = require("@images/png/extras/overnight.png");
  //   }
  //   if(/Fuel/g.test(title)) {
  //     icons = require("@images/png/extras/fuel_plans.png")
  //   }

  //   return (
  //     <View
  //       style={ styles.container }
  //       key={ index }
  //     >
  //       <View style={ styles.image.container }>
  //         <Image
  //           resizeMode={ "contain" }
  //           source={ icons }
  //         />
  //       </View>

  //       <View style={ styles.main.container }>
  //         <Text color={ "black" } size={ 15 } style={ styles.main.title }>
  //           { title.toUpperCase() }
  //         </Text>
  //         <Text color={ "#aaaaaa" } size={ 12 }>
  //           { this.state.checkoutData.currency } { NumberHelper.format(parseInt(price), ".") } 
  //           {/* / { unit } */}
  //         </Text>
  //       </View>

  //       { controlRender }
  //     </View>
  //   );
  // }

  _renderExtras() {
    return (
      <View style={{
        flex: 1,
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingTop: 20,
      }}>

        {/* { this._renderCategories() }

        <View style={ styles.main.container }>
          { this._renderTabs() }

          { this._renderMenus() }
        </View> */}

        {/* { this.state.list[this.state.activeListIndex].vehicle.extraItems.length > 0 && this.state.list[this.state.activeListIndex].vehicle.extraItems.map((extra, index) => {
          return this._renderExtraItem(
            extra.name, 
            extra.icon, 
            extra.price, 
            extra.pricePer, 
            extra.amounts, 
            extra.ValueType, 
            index, 
            extra.Availability,
            extra.StockType);
        }) } */}

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BookedVehicleDetailContainer
        { ... this.props }
        setScrollRef={ sv => {this.mainImageScroll = sv;} }
        serviceType={ this.state.navData.title }
        list={ this.state.list }
        activeList={ this.state.list[this.state.activeListIndex] }
        activeListIndex={ this.state.activeListIndex }
        checkoutData={ this.state.checkoutData }
        list={ this.state.list }
        activeListIndex={ this.state.activeListIndex }
        checkoutData={ this.state.checkoutData }
        submittedData={ this.state.navData.submittedData }
        type={ this.state.navData.type }
        totalPrice={ this.getTotalPrice() }
        onSwitchList={ (index) => this.switchList(index) }
        onRemoveList={ (index) => this.removeList(index) }
        onSwitchList={ (index) => this.switchList(index) }
      >

        { this._renderExtras() }

      </BookedVehicleDetailContainer>
    );
  }
}
