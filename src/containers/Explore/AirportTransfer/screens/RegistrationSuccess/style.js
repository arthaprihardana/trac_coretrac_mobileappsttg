import { appVariables, appMetrics } from "@variables";

export default {
  container:{
    flex: 1,
    paddingVertical: appMetrics.windowHeight/4,
    paddingHorizontal: 30,
    backgroundColor: appVariables.colorWhite,
  },
  image:{
    flex: -1,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 20,
  },
  text:{
    flex: -1,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
  },
  button:{
    flex: -1,
    paddingHorizontal:150,
  }
}