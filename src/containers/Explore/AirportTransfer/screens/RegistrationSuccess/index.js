import React, { Component } from "react";
import {
  View,
  Image,
} from "react-native";
import {
  appVariables,
} from "@variables";
import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import styles from "./style";

export default class RegistrationSuccess extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderImage() {
    return (
      <View style={styles.image}>
        <Image
          source={require("@images/png/registration-success.png")}>
        </Image>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderText1() {
    return (
      <View style={styles.text}>
        <Text
          size={25}>
          Congratulation Bryan!
        </Text>
        <Text
          size={25}>
          You've become a TRAC Member!
        </Text>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderText2() {
    return (
      <View style={styles.text}>
        <Text
          size={24}
          color={appVariables.colorGray}>
          We have sent a verification Email to
        </Text>
        <Text
          size={24}
          color={appVariables.colorGray}>
          Bryan@gmail.com.
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderText3() {
    return (
      <View style={styles.text}>
        <Text
          size={24}
          color={appVariables.colorGray}>
          Please Verify your email to have an update
          </Text>
        <Text
          size={24}
          color={appVariables.colorGray}>
          of your Booking.
          </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderButton() {
    return (
      <View style={styles.button}>
        <MainButton
          label={"CONTINUE"}
          rounded
        />
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------
  render() {
    return (
      <View style={styles.container}>
        {this._renderImage()}
        {this._renderText1()}
        {this._renderText2()}
        {this._renderText3()}
        {this._renderButton()}
      </View>
    );
  }
}