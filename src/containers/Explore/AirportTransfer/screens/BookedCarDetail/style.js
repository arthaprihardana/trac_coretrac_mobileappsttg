import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  outerContainer: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appVariables.colorWhite,
  },

  grouper: {
    flex: 1,
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingTop: 20,
  },

  container: {
    flex: -1,
    width: (appMetrics.screenWidth - 65) / 2,
    // width: '45%',
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    paddingTop: 20,
    paddingBottom: 0,
    marginRight: 20,
    marginBottom: 20,
    borderColor: "#dddddd",
    backgroundColor: appVariables.colorWhite,
    shadowOffset:{  width: 0,  height: 0,  },
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowRadius: 7,
    elevation:2,
  },

  image: {
    container: {
      flex: -1,
      height: 80,
      width: 80,
      marginBottom: 10,
      justifyContent: "center",
      alignItems: "center",
    },
  },

  main: {
    container: {
      flex: -1,
      marginBottom: 40,
      justifyContent: "center",
      alignItems: "center",
    },

    title: {
      paddingBottom: 4,
    },
  },

  numberControl: {
    container: {
      flex: -1,
      flexDirection: "row",
      width: (appMetrics.screenWidth - (40 + 20)) / 2,
      height: 45,
      borderTopWidth: 1,
      borderColor: "#dddddd",
    },

    button: {
      container: {
        flex: -1,
        width: 40,
        justifyContent: "center",
        alignItems: "center",
        // borderRightWidth: 1,
        // borderColor: "#dddddd",
      },
    },

    value: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      borderLeftWidth: 1,
      borderRightWidth: 1,
      borderColor: "#dddddd",
    },
  },

  toggleControl: {
    container: {
      flex: -1,
      flexDirection: "row",
      width: (appMetrics.screenWidth - (40 + 20)) / 2,
      height: 45,
      justifyContent: "center",
    },
  },
}