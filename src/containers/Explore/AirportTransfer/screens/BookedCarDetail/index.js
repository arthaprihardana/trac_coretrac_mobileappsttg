/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-08 11:17:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-10 17:15:17
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import { 
  View,
  TouchableOpacity,
  Image,
  Switch,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import _ from "lodash";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";

import BookedVehicleDetailContainer from "@airport-transfer-fragments/BookedVehicleDetailContainer";

import NumberHelper from "@airport-transfer-helpers/Number";

import CarExtraServiceConfigs from "@airport-transfer-configs/data/CarExtraService";

import styles from "./style";


export default class BookedCarDetail extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    const navData = JSON.parse(JSON.stringify(props.navData));
    // const list = navData.list.map((item, index) => {
    //   return {
    //     ... item,
    //     extras: CarExtraServiceConfigs.map((extra, extIndex) => {
    //       return {
    //         ... extra,
    //         value: extra.type === "number" ? 0 : false,
    //       }
    //     }),
    //   }
    // });
    // console.log('list ==>', navData.list);
    
    this.state = {
      navData: navData,

      // list: list,
      list: navData.list,
      checkoutData: navData.checkoutData,
      priceModifier: 0,

      activeListIndex: 0,
    };

    this.mainImageScroll = null;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  switchList(index, newList = null) {
    if (this.state.activeListIndex == index && !newList) {
      return false;
    }

    const list = newList ? newList : this.state.list;

    if (index > list.length - 1) {
      index = 0;
    } else if (index < 0) {
      index = list.length - 1;
    }

    let imageIndex = index;
    if (newList && index == list.length - 1) {
      imageIndex  = imageIndex + 1;
    }

    this.mainImageScroll.scrollTo({x: imageIndex * appMetrics.screenWidth});

    this.setState({
      activeListIndex: index,
      list,
    });
  }

// ---------------------------------------------------

  getTotalPrice() {
    let totalPrice = 0;
    this.state.list.map((item, index) => {
      totalPrice += item.price * this.state.checkoutData.days;
    });

    totalPrice += this.state.priceModifier;

    return totalPrice;
  }

// ---------------------------------------------------

  changeExtraValue(index, value, priceValueModifier, available) {
    const list = this.state.list;
    let priceModifier = this.state.priceModifier;

    if (value < 0) {
      value = 0;
      priceValueModifier = 0;
    }

    // // list[this.state.activeListIndex].extras[index].value = value;
    list[this.state.activeListIndex].vehicle.extraItems[index].amounts = value;
    priceModifier += parseInt(priceValueModifier);
    list[this.state.activeListIndex].vehicle.extraItems[index].total = parseInt(list[this.state.activeListIndex].vehicle.extraItems[index].price) * list[this.state.activeListIndex].vehicle.extraItems[index].amounts;

    this.setState({
      value,
      priceModifier,
    })
  }

// ---------------------------------------------------

  removeList(index) {
    let newIndex = this.state.activeListIndex - 1;
    if (index == 0) {
      newIndex = 0;
    }

    const list = this.state.list;
    list.splice(index, 1);

    this.switchList(newIndex, list);
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderExtraItemNumberControl(index, value, price, available, stockType) {
    return (
      <View style={ styles.numberControl.container }>

        <TouchableOpacity
          style={ styles.numberControl.button.container }
          onPress={ () => this.changeExtraValue(index, value - 1, (price * -1)) }
        >
          <Icon name="minus" color={ "black" } size={ 10 }/>
        </TouchableOpacity>

        <View style={ styles.numberControl.value }>
          <Text>
            { value }
          </Text>
        </View>

        <TouchableOpacity
          style={ [styles.numberControl.button.container, {borderWidth: 0,}] }
          onPress={ () => {
            // if(available !== null) {
            if(stockType !== "0") {
              let reduceExtrasAmount = _.reduce(this.state.list, (sum, n) => {
                let e = n.vehicle.extraItems[index].amounts;
                return sum + e;
              },0)
              if((reduceExtrasAmount + 1) > parseInt(available)) {
                console.log('stock available habis');
              } else {
                this.changeExtraValue(index, value + 1, price, available) 
              }
            } else {
              this.changeExtraValue(index, value + 1, price, available) 
            }
          }}
        >
          <Icon name="plus" color={ "black" } size={ 10 }/>
        </TouchableOpacity>

      </View>
    )
  }

// ---------------------------------------------------

  _renderExtraItemToggleControl(index, value, price) {
    return (
      <View style={ styles.toggleControl.container }>

        <Switch
          thumbColor={ appVariables.colorOrange }
          trackColor={{ true: appVariables.colorOrange }}
          onValueChange={ (newValue) => this.changeExtraValue(index, !value, !value ? price : (price * -1)) }
          value={ value }
        />

      </View>
    )
  }

// ---------------------------------------------------

  _renderExtraItem(title, icon, price, unit, value, type, index, available, stockType) {
    let icons = require("@images/png/extras/add_hour.png");
    let controlRender = this._renderExtraItemNumberControl(index, value, price, available, stockType);
    // if (type == "toggle") {
    //   controlRender = this._renderExtraItemToggleControl(index, value, price);
    // }
    if (type == "boolean") {
      controlRender = this._renderExtraItemToggleControl(index, value, price);
    }

    if(/Add/g.test(title)) {
      icons = require("@images/png/extras/add_hour.png");
    }
    if(/Out/g.test(title)) {
      icons = require("@images/png/extras/out_of_town.png");
    }
    if(/Over/g.test(title)) {
      icons = require("@images/png/extras/overnight.png");
    }
    if(/Fuel/g.test(title)) {
      icons = require("@images/png/extras/fuel_plans.png")
    }

    return (
      <View
        style={ styles.container }
        key={ index }
      >
        <View style={ styles.image.container }>
          <Image
            resizeMode={ "contain" }
            source={ icons }
          />
        </View>

        <View style={ styles.main.container }>
          <Text color={ "black" } size={ 15 } style={ styles.main.title }>
            { title.toUpperCase() }
          </Text>
          <Text color={ "#aaaaaa" } size={ 12 }>
            { this.state.checkoutData.currency } { NumberHelper.format(parseInt(price), ".") } 
            {/* / { unit } */}
          </Text>
        </View>

        { controlRender }
      </View>
    );
  }

// ---------------------------------------------------

  _renderExtras() {
    return (
      <View style={ styles.grouper }>

        {/* { this.state.list[this.state.activeListIndex].extras.map((extra, index) => {
          return this._renderExtraItem(extra.title, extra.icon, extra.price, extra.unit, extra.value, extra.type, index);
        }) } */}
        { this.state.list[this.state.activeListIndex].vehicle.extraItems.map((extra, index) => {
          return this._renderExtraItem(
            extra.name, 
            extra.icon, 
            extra.price, 
            extra.pricePer, 
            extra.amounts, 
            extra.ValueType, 
            index, 
            extra.Availability,
            extra.StockType);
        }) }

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BookedVehicleDetailContainer
        { ... this.props }
        setScrollRef={ sv => {this.mainImageScroll = sv;} }
        serviceType={ this.state.navData.title }
        list={ this.state.list }
        activeList={ this.state.list[this.state.activeListIndex] }
        activeListIndex={ this.state.activeListIndex }
        checkoutData={ this.state.checkoutData }
        list={ this.state.list }
        activeListIndex={ this.state.activeListIndex }
        checkoutData={ this.state.checkoutData }
        submittedData={ this.state.navData.submittedData }
        type={ this.state.navData.type }
        // totalPrice={ this.getTotalPrice() }
        // onSwitchList={ (index) => this.switchList(index) }
        onRemoveList={ (index) => this.removeList(index) }
        onSwitchList={ (index) => this.switchList(index) }
      >

        { this._renderExtras() }

      </BookedVehicleDetailContainer>
    );
  }
}
