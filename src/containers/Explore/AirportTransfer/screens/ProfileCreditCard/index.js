/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 23:38:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-05 15:01:06
 */
import React, { Component } from 'react';
import { Content, Text } from 'native-base';
import { View, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import {appVariables} from '@variables';
import styles from './style';
import Header from "@airport-transfer-fragments/Header";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { getCreditCard } from "../../../../../actions";
import _ from "lodash";

class ProfileCreditCard extends Component {

  state = {
    primaryCC: []
  }

  componentDidMount = () => {
    this.props.getCreditCard({
      UserLoginId: this.props.login.Data.Id,
      token: this.props.login.token
    })
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.cc !== prevProps.cc) {
      return { cc: this.props.cc }
    }
    return null; 
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(this.props.refresh !== prevProps.refresh) {
      if(this.props.refresh) {
        this.setState({ primaryCC: [] })
        this.props.getCreditCard({
          UserLoginId: this.props.login.Data.Id,
          token: this.props.login.token
        })
      }
    }
    if(snapshot !== null) {
      if(snapshot.cc !== undefined) {
        let x = _.filter(snapshot.cc, v => v.IsPrimary === "1");
        this.setState({
          primaryCC: x
        });
      }
    }
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------
  goToDeleteCreditCardConfirmation(){
    Actions.DeleteCreditCardConfirmation();
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderOpeningWording() {
    return (
      <View style={{ 
          marginTop: 20,
          borderBottomWidth:1,
          borderBottomColor:"#D8D8D8",
        }}>
        <Text style={styles.openingWord}>
          Please enter a credit or debit card to make it easier for you to order.
          Your payment details are stored safely
      </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderLabel() {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <View>
            <Text style={styles.title}>Credit Card Number</Text>
          </View>
        </View>

        <View style={styles.subContainer}>
          <View>
            <Text style={styles.title}>Expiry Date</Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderLabelContent(cc) {
    let x = _.replace(cc[0].DecryptedCreditCard, /-/g, " ");
    let y = _.replace(x, /^\d{4}\ \d{4}/g, "**** ****");
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <View>
            {/* <Text style={styles.contentWrapper}>•••• 2222</Text> */}
            <Text style={styles.contentWrapper}>{y}</Text>
          </View>
        </View>

        <View style={styles.subContainer}>
          <View>
            <Text style={styles.contentWrapper}>{cc[0].DecryptedCardExpired}</Text>
          </View>
        </View>
      </View>

    );
  }

  // ---------------------------------------------------
  _renderVisaCard(CardImage) {
    return (
      <View style={styles.visaCardWrapper}>
        <View style={styles.subContainer}>
          <View>
            {/* <Image source={require('@images/png/payment/visaCard.png')} /> */}
            <Image source={{ uri: CardImage }} style={{ width: 40, height: 20 }} resizeMode="cover" />
          </View>
        </View>
      </View>
    );
  }

  _renderNameonCard(cc) {
    return (
      <View style={{
        paddingVertical: 20
      }}>
        <View style={styles.container}>
          <View style={styles.subContainer}>
            <View>
              <Text style={styles.title}>Name On Card</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.subContainer}>
            <View>
              <Text style={styles.contentWrapper}>Bryan Barry Borang</Text>
            </View>
          </View>
        </View>

      </View>
    );
  }

  // ---------------------------------------------------
  _renderClosingWording() {
    return (
      <TouchableOpacity 
        style={{paddingVertical:20}}
        onPress={()=> this.goToDeleteCreditCardConfirmation()}>
        <Text style={styles.closingWord}>
          Tap and hold to remove your card
          </Text>
      </TouchableOpacity>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Credit/Debit Card"}
          onBackPress={() => this.props.navigation.pop()} />

        <Content style={{ backgroundColor: '#FFFFFF' }}>

          {this._renderOpeningWording()}
          { this.props.loading && <View style={{ width: '100%', padding: 20, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="small" color="#2A2E36" /></View> }
          { this.state.primaryCC.length > 0 &&
            <View style={{
            paddingVertical:20,        
            borderBottomWidth:1,
            borderBottomColor:"#D8D8D8"
            }}>
              {this._renderVisaCard(this.state.primaryCC[0].CardImage)}
              {this._renderLabel()}
              {this._renderLabelContent(this.state.primaryCC)}
              {/* {this._renderNameonCard(this.state.primaryCC)} */}
            </View>
          }
          <View style={{paddingVertical:20}}>
            { this.state.primaryCC.length === 0 && <Text style={[{marginBottom: 20}, styles.closingWord]}>You don't have credit card here</Text> }
            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', padding: 30 }}>
              <TouchableOpacity
                style={styles.buttonWrapper}
                onPress={() => Actions.AddCreditCard()}>
                <Text color={appVariables.colorBlack}>
                  ADD CARD
                </Text>
              </TouchableOpacity>

              { this.state.primaryCC.length > 0 && <TouchableOpacity
                style={styles.buttonWrapper}
                onPress={() => Actions.ViewCreditCard()}>
                <Text color={appVariables.colorBlack}>
                  VIEW CARD
                </Text>
              </TouchableOpacity> }
            </View>
          </View>
          {/* {this._renderClosingWording()} */}
        </Content>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ creditCard, authentication }) => {
  const { cc, loading } = creditCard;
  const { login } = authentication;
  return { cc, loading, login }
}

export default connect(mapStateToProps, {
  getCreditCard
})(ProfileCreditCard);