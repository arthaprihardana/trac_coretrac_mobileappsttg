import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
  },

  innerContainer: {
    flex: 1,
    padding: 20,
    paddingTop: 10,
    backgroundColor: appVariables.colorWhite,
  },

  form: {
    extra: {
      container: {
        flex: -1,
        flexDirection: "row",
        paddingVertical: 20,
        justifyContent: "space-between",
      },
    },
  },

  imageForm: {
    container: {
      flex: -1,
      borderWidth: 1,
      borderRadius: 3,
      borderStyle: "dashed",
      borderColor: "#ccc",
      width: 220,
      height: 200,
      marginTop: 30,
      padding: 15, 
    },

    image: {
      container: {
        flex: -1,
        height: 120,
        borderStyle: "dashed",
        borderRadius: 3,
        borderWidth: .3,
        borderColor: '#777',
        marginBottom: 10,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center'
      },
      caption: { fontSize: 9, color: "#ccc" }
    },

    button: {
      container: {
        flex: 1,
        borderWidth: 1,
        backgroundColor: appVariables.colorBlueHeader,
        borderRadius: 7,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

  otherPassanger: {
    container: {
      flex: -1,
    },

    title: {
      container: {
        flex: -1,
        height: 45,
        justifyContent: "space-between",
      },
    },

    carList: {
      container: {
        flex: -1,
        marginTop: 40,
        height: 110,
        justifyContent: "space-between",
        paddingBottom: 20,
      },
    },
  }
}
