/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 10:56:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 17:00:09
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import _ from "lodash";
import MCamera from '../../../../../components/molecules/MCamera';
import { 
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import Icon from "@airport-transfer-fragments/Icon";
import Header from "@airport-transfer-fragments/Header";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import MainButton from "@airport-transfer-fragments/MainButton";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";

import styles from "./style";
import { Actions } from "react-native-router-flux";
import { setTempPersonalDetail, getImageBufferKtp, getImageBufferSim } from "../../../../../actions";
import { CAR_RENTAL, AIRPORT_TRANSFER, BUS_RENTAL } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class PassagerDetail extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        name: props.login.Data.FirstName !== null ? `${props.login.Data.FirstName} ${props.login.Data.LastName}` : "",
        phone_number: props.login.Data.NoHandphone || "",
        ktp: props.login.Data.NoKTP || "",
        address: props.login.Data.Address || "",
        email: props.login.Data.EmailPersonal || "",
        capture: null,
      },
      errorMessage: {
        name: null,
        phone_number: null,
        ktp: null,
        address: null,
        email: null,
        capture: null
      },

      activeCarIndex: -1,

      isForSomeoneElse: false,
      showModalCamera: false,
      readyToSubmit: false,
      successLoadKTP: false,
      successLoadSIM: false
    };
  }

// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  componentDidMount = () => {
    // if(this.props.login.Data.ImageKTP !== null) {
    //   this.props.getImageBufferKtp({ uri: this.props.login.Data.ImageKTP });
    // } else {
    //   this.setState({ successLoadKTP: true })
    // }
    // if(this.props.login.Data.ImageSIM !== null) {
    //   this.props.getImageBufferSim({ uri: this.props.login.Data.ImageSIM });
    // } else {
    //   this.setState({ successLoadSIM: true })
    // }
    if(this.validateAll()) {
      this.setState({ 
        readyToSubmit: true,
        capture: null
      })
    }
  }
  

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.capture !== prevProps.capture) {
      return { capture: this.props.capture }
    }
    // if(this.props.bufferKtp !== prevProps.bufferKtp) {
    //   return { bufferKtp: this.props.bufferKtp }
    // }
    // if(this.props.bufferSim !== prevProps.bufferSim) {
    //   return { bufferSim: this.props.bufferSim }
    // }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null)  {
      if(snapshot.capture) {
        this.setState({ capture: snapshot.capture.uri }, () => {
          this.onValidate("capture").then(val => {
            const form = this.state.form;
            const err = this.state.errorMessage;
            form["capture"] = snapshot.capture;
            err["capture"] = null;
            this.setState({ 
              readyToSubmit: this.state.capture !== null && val === undefined ? true : false,
              form: form,
              errorMessage: err
            })
          })
        });
      }
      // if(snapshot.bufferKtp) {
      //   this.setState({ successLoadKTP: true })
      // }
      // if(snapshot.bufferSim) {
      //   this.setState({ successLoadSIM: true })
      // }
    }
  }
  
  onChangeForm(name, value) {
    const form = this.state.form;
    const err = this.state.errorMessage;
    form[name] = value;
    err[name] = null

    this.setState({
      form: form,
      errorMessage: err
    });
  }

  onValidate = async current => {
    let except = this.props.login.Data.ImageKTP !== null ? ['capture'] : [];
    except.push(current);
    let o = _.omit(this.state.form, except);
    let a = await _.findKey(o, val => {
      if(_.isEmpty(val)) { return true }
    });
    return a;
  }

  validateAll = () => {
    let f = this.state.form;
    if(f.name.length === 0) return false
    if(f.phone_number.length === 0) return false
    if(f.ktp.length === 0) return false
    if(f.address.length === 0) return false
    if(f.email.length === 0) return false
    // if(_.isNull(f.capture)) return false
    if(_.isNull(this.props.login.Data.ImageKTP)) return false
    return true;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  goToNextStep() {
    this.props.setTempPersonalDetail(this.state.form);
    switch (this.props.selectedProduct) {
      case CAR_RENTAL:
        if(this.props.requestStock.ProductServiceId === "PSV0001") {  // self drive
          Actions.push("DriverInformation", {
            navData: this.props.navData
          });
        } else {
          Actions.push("Payment", {
            navData: this.props.navData
          });
        }
        break;
      case AIRPORT_TRANSFER:
        Actions.push("Payment", {
          navData: this.props.navData
        });
        break;
      case BUS_RENTAL:
        Actions.push("CoordinatorInformation", {
          navData: this.props.navData
        });
        break;
      default:
        Actions.push("Payment", {
          navData: this.props.navData
        });
        break;
    }
  }

  infoEmptyField() {
    let form = this.state.form;
    let err = this.state.errorMessage;
    let f = _.findKey(form, val => _.isEmpty(val));
    if(f !== undefined) {
      switch (f) {
        case "name":
          this._form.scrollTo({x: 0, y: 0, animated: true});
          err.name = "Nama harus diisi";
          break;
        case "phone_number":
          this._form.scrollTo({x: 50, y: 0, animated: true});
          err.phone_number = "No Telepon harus diisi";
          break;
        case "ktp":
          this._form.scrollTo({x: 100, y: 0, animated: true});
          err.ktp = "No KTP harus diisi";
          break;
        case "address":
          err.address = "Alamat harus diisi";
          break;
        case "capture":
          err.capture = "Foto KTP harus diupload";
          break;
        default:
          break;
      }
      this.setState({ errorMessage: err })
    }
    return null;
  }

// ---------------------------------------------------

  loginBySocialMedia(socMed) {
    alert(socMed);
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderIsForSomeoneElseToggle() {
    return (
      <View style={ styles.form.extra.container }>
        <RoundedCheck
          value={ this.state.isForSomeoneElse }
          onPress={ (value) => this.setState({isForSomeoneElse: !this.state.isForSomeoneElse}) }
        >
          <Text>
            I'm booking for someone else
          </Text>
        </RoundedCheck>
      </View>
    );
  }

// ---------------------------------------------------

  _renderForm() {
    return (
      <View>
        <FloatingTextInput
          label={Lang.fullName[this.props.lang]}
          onChangeText={ value => {
            this.onValidate("name").then(val => {
              this.onChangeForm("name", value);
              this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
            });
          }}
          value={ this.state.form.name }
          note={ Lang.asOnIDCard[this.props.lang] }
          editable={this.props.selectedProduct !== BUS_RENTAL ? (this.props.login.Data.FirstName !== null ? false : true) : true}
        />

        <FloatingTextInput
          label={ Lang.phoneNumber[this.props.lang] }
          onChangeText={ value => {
            this.onValidate("phone_number").then(val => {
              this.onChangeForm("phone_number", value);
              this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
            });
          }}
          value={ this.state.form.phone_number }
          maxLength={13}
          keyboardType="phone-pad"
          note={this.state.errorMessage.phone_number}
          editable={this.props.selectedProduct !== BUS_RENTAL ? (this.props.login.Data.NoHandphone !== null ? false : true) : true}
        />

        <FloatingTextInput
          label={ Lang.ktpNumber[this.props.lang] }
          onChangeText={ value => {
            this.onValidate("ktp").then(val => {
              this.onChangeForm("ktp", value);
              this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
            });
          }}
          value={ this.state.form.ktp }
          maxLength={16}
          keyboardType="phone-pad"
          note={this.state.errorMessage.ktp}
          editable={this.props.selectedProduct !== BUS_RENTAL ? (this.props.login.Data.NoKTP !== null ? false : true) : true}
        />

        <FloatingTextInput
          label={ Lang.residentialAddress[this.props.lang] }
          onChangeText={ value => {
            this.onValidate("address").then(val => {
              this.onChangeForm("address", value);
              this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
            });
          }}
          value={ this.state.form.address }
          note={this.state.errorMessage.address}
          editable={this.props.selectedProduct !== BUS_RENTAL ? (this.props.login.Data.Address !== null ? false : true) : true}
        />

        <View style={ [styles.imageForm.container, this.props.selectedProduct !== BUS_RENTAL ? (this.props.login.Data.ImageKTP !== null ? { height: 135 } : {} ) : {} ] }>

          <View style={ styles.imageForm.image.container }>
            {this.props.capture !== null ? 
              <Image source={{
                uri: this.props.capture.uri
              }} style={{width: 197, height: 120}} resizeMode="cover" />
            :
             this.props.login.Data.ImageKTP !== null ? 
              <Image source={{
                uri: this.props.login.Data.ImageKTP
              }} style={{width: 197, height: 120}} resizeMode="cover" />
             :
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Icon name="upload" color={"#ccc"} size={28} />
                <Text style={styles.imageForm.image.caption}>Upload yout ID Card Here</Text>
              </View>
            }
          </View>
          { this.props.selectedProduct !== BUS_RENTAL ? (this.props.login.Data.ImageKTP !== null ? false : 
            <TouchableOpacity
              style={ styles.imageForm.button.container }
              onPress={() => {
                Alert.alert(
                  'Info',
                  'Dari mana anda akan mengambil gambar?',
                  [
                    {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                    {
                      text: 'Galeri',
                      onPress: () => Actions.push("Galeri", { route: "PassangerDetail", type: "ktp" }),
                      style: 'cancel',
                    },
                    {text: 'Kamera', onPress: () => this.setState({showModalCamera:true}) },
                  ],
                  {cancelable: false},
                ); 
              }}
              >
              <Text color={ appVariables.colorWhite } bold>
                {Lang.photoKTP[this.props.lang]}
              </Text>
            </TouchableOpacity>
            ) : <TouchableOpacity
              style={ styles.imageForm.button.container }
              onPress={() => {
                Alert.alert(
                  'Info',
                  'Dari mana anda akan mengambil gambar?',
                  [
                    {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                    {
                      text: 'Galeri',
                      onPress: () => Actions.push("Galeri", { route: "PassangerDetail", type: "ktp" }),
                      style: 'cancel',
                    },
                    {text: 'Kamera', onPress: () => this.setState({showModalCamera:true}) },
                  ],
                  {cancelable: false},
                ); 
              }}
              >
              <Text color={ appVariables.colorWhite } bold>
                {Lang.photoKTP[this.props.lang]}
              </Text>
            </TouchableOpacity> }
        </View>
        {this.state.errorMessage.capture !== null && <View style={{ marginBottom: 10 }}>
          <Text color={ "#aaaaaa" } size={ 12 }>{this.state.errorMessage.capture}</Text>
        </View>}

        <MCamera 
          visible={this.state.showModalCamera}
          onCapture={() => console.log('on capture')}
          onClose={() => this.setState({showModalCamera:false})}
        />

        <MainButton
          label={ Lang.continue[this.props.lang] }
          onPress={ () => this.state.readyToSubmit ? this.goToNextStep() : this.infoEmptyField() }
          rounded
          disabled={ !this.state.readyToSubmit }
        />
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const navData = this.props.navigation.getParam("navData", {});
    
    return (
      <ProgressedContainer
        header={{
          label: Lang.personalInformation[this.props.lang],
          onBackPress: () => this.props.navigation.pop(),
        }}
        steps= { navData.processSteps }
        currentStepIndex={ navData.processSteps.indexOf(Lang.personalDetail[this.props.lang]) }
        { ... this.props }
      >
        <KeyboardAvoidingView
          behavior={ "padding" }
          enabled={false}
          style={ styles.container }
        >
          <ScrollView
            ref={ref => this._form = ref}
            style={ styles.innerContainer }
            contentContainerStyle={{
              paddingBottom: 30,
            }}
          >
            { this._renderForm() }
          </ScrollView>
        </KeyboardAvoidingView>
      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ authentication, camera, stockList, productService, imageBuffer, language }) => {
  const { login } = authentication;
  const { capture } = camera;
  const { requestStock } = stockList;
  const { selectedProduct } = productService;
  const { bufferKtp, bufferSim } = imageBuffer;
  const { lang } = language;
  return { login, capture, requestStock, selectedProduct, bufferKtp, bufferSim, lang };
}

export default connect(mapStateToProps, {
  setTempPersonalDetail,
  getImageBufferKtp,
  getImageBufferSim
})(PassagerDetail);
