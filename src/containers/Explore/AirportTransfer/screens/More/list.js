/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-28 09:14:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 14:23:07
 */
import Lang from '../../../../../assets/lang';
export default [
  // {
  //   iconList: "icoPromo",
  //   textList: Lang.promo,
  //   listTarget :'MyVoucher',
  // },
  {
    iconList: "icoNewsEvent",
    textList: Lang.news,
    listTarget : 'NewsEvent',
  },
  {
    iconList: "icoProductServices",
    textList: Lang.product_service,
    listTarget : 'ProductService',
  },
  {
    iconList: "icoPusatBantuan",
    textList: Lang.contactUs,
    listTarget : 'ContactUs',
  },
  {
    iconList: "icoContactUs",
    textList: Lang.callCenter,
    listTarget : 'CallCenter',
  },
  // {
  //   iconList: "icoLocation",
  //   textList: Lang.branchLocation,
  //   listTarget : 'OutletLocation',
  // },
  {
    iconList: "icoTNC",
    textList: Lang.termsAndConditions,
    listTarget : 'TNC',
  },
  {
    iconList: "icoFAQ",
    textList: Lang.faq,
    listTarget : 'FAQ',
  },
  {
    iconList: "icoPrivacyPolicy",
    textList: Lang.privacyPolicy,
    listTarget : 'PrivacyPolicy',
  },
]

            

        
              
            
