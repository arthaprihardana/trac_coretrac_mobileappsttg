/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 10:26:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 11:45:57
 */
import React, { Component } from 'react';
import { Content, Text,Thumbnail, List } from 'native-base';
import SvgIcon from '../../../../../utils/SvgIcon';
import { View, TouchableOpacity, Modal, SafeAreaView, TextInput, ScrollView, Image } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Icon from "@airport-transfer-fragments/Icon";
import Header from "@airport-transfer-fragments/Header";
import Mask from 'react-native-mask';
import listItems from './list';
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import Lang from '../../../../../assets/lang'
import _ from "lodash";
import {
  appVariables,
} from "@variables";
import { setLanguage } from '../../../../../actions';
const version = require('../../../../../../version.json').version;

const lang = [{'value': 'id', 'label':'Bahasa Indonesia', 'image': require('../../../../../assets/images/png/flags/62.png')}, {'value': 'en', 'label': 'English', 'image': require('../../../../../assets/images/png/flags/1.png')}];

class More extends Component {

  state = {
    modalVisible: false,
    item: lang,
    searchText: null
  }
  
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  _renderListitem(iconList, textList, onPress, index) {
    return (
      <TouchableOpacity style={{
        flex: -1,
        flexDirection: "row",
        borderBottomWidth: 1,
        height: 50,
        alignItems: "center",
        borderColor: "#D8D8D8"
      }}
        onPress={() => onPress()}
        key={index}>
        <View style={{
          flex: -1,
        }}>
          <SvgIcon name={iconList} />
        </View>
        <View style={{
          flex: 1,
          paddingLeft: 10
        }}>
          <Text style={{ fontSize: 12 }}>{textList[this.props.lang]}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  _renderLanguage() {
    return (
      <TouchableOpacity 
        style={{
          flex: -1,
          flexDirection: "row",
          justifyContent: 'space-between',
          borderBottomWidth: 1,
          height: 50,
          alignItems: "center",
          borderColor: "#D8D8D8"
        }}
        onPress={() => this.setModalVisible(!this.state.modalVisible)} >
        <View style={{ flex: -1 }}>
          <Icon
            name="language"
            size={22}
            color={"#21419A"} />
        </View>
        <View style={{ flex: 2, paddingLeft: 10 }}>
          <Text style={{ fontSize: 12 }}>{Lang.languageApps[this.props.lang]}</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'flex-end', paddingRight: 10 }}>
          <Mask shape={'circle'} >
            <Image style={{ width: 20, height: 20 }} source={ _.filter(lang, { value: this.props.lang })[0].image } />
          </Mask>
        </View>
      </TouchableOpacity>
    )
  }

  _renderVersionInfo() {
    return (
      <View 
        style={{
          flex: -1,
          flexDirection: "row",
          justifyContent: 'space-between',
          borderBottomWidth: 1,
          height: 50,
          alignItems: "center",
          borderColor: "#D8D8D8"
        }}
        onPress={() => this.setModalVisible(!this.state.modalVisible)} >
        <View style={{ flex: -1 }}>
          <Icon
            name="info-circle"
            size={22}
            color={"#21419A"} />
        </View>
        <View style={{ flex: 2, paddingLeft: 10 }}>
          <Text style={{ fontSize: 12 }}>Version</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'flex-end', paddingRight: 10 }}>
          <Text style={{ fontSize: 12 }}>{version}</Text>
        </View>
      </View>
    )
  }

  render() {
    const nav = this.props.navigation;

    return (
      <BaseContainer hasFooter>
        <Header title={Lang.more[this.props.lang]}/>
        <Content style={{ backgroundColor: 'white' }}>
          <List style={{ backgroundColor: 'white', paddingHorizontal: 20, }}>
            { this.props.isLogin ? listItems.map((item, index) => {
              return this._renderListitem(item.iconList, item.textList, () => Actions.push(item.listTarget),index);
            }) : _.filter(listItems, v => v.listTarget !== "ContactUs" ).map((item, index) => {
              return this._renderListitem(item.iconList, item.textList, () => Actions.push(item.listTarget),index);
            }) }
            { this._renderLanguage() }
            { this._renderVersionInfo() }
          </List>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => this.setModalVisible(!this.state.modalVisible) }>
            <SafeAreaView style={{flex: 1, backgroundColor: appVariables.colorBlueHeader }}>
              <Header title={Lang.changeLanguage[this.props.lang]} onPress={this._onPressButton} onBackPress={ () => this.setModalVisible(!this.state.modalVisible) }/>
              <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <View>
                  <View style={{ height: 60, backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }}>
                    <TextInput
                      style={{height: 40, width: '100%', borderColor: '#ccc', borderWidth: 1, backgroundColor: '#ffffff', paddingHorizontal: 10 }}
                      onChangeText={text => {
                        this.setState({
                          searchText: text
                        }, () => {
                          this.setState({
                            item: text.length > 0 ? _.filter(lang, v => v.value.toLowerCase().match(new RegExp(this.state.searchText, "gi"))) : lang
                          })
                        });
                      }}
                      value={this.state.searchText}
                      placeholder="Search"
                      placeholderTextColor={"#cccccc"}
                    />
                  </View>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    { _.map(this.state.item, (v, k) => (
                      <TouchableOpacity key={k} style={{ height: 50, width: '100%',  borderBottomWidth: 1, borderBottomColor: '#eee', flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10 }} onPress={() => {
                        this.props.setLanguage(v.value);
                        this.setModalVisible(!this.state.modalVisible);
                      }}>
                        <Mask shape={'circle'}>
                          <Image style={{ width: 20, height: 20 }} source={v.image} />
                        </Mask>
                        <Text style={{ marginLeft: 20 }}>{v.label}</Text>
                      </TouchableOpacity>
                    )) }
                  </ScrollView>
                </View>
              </View>
            </SafeAreaView>
          </Modal>
        </Content>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ authentication, language }) => {
  const { isLogin } = authentication;
  const { lang } = language;
  return { isLogin, lang };
}

export default connect(mapStateToProps, {
  setLanguage
})(More);
