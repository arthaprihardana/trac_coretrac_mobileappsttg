/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 16:46:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 16:08:17
 */
import React, { Component } from "react";
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Modal,
  SafeAreaView
} from "react-native";
import {
  appVariables,
} from "@variables";
import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import NumberHelper from "@airport-transfer-helpers/Number";
import Icon from "@airport-transfer-fragments/Icon";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import VehicleSliderDetail from "@airport-transfer-fragments/VehicleSliderDetail";
// import carBookingDummy from "@airport-transfer-dummies/car_booking";
import { appMetrics } from "../../../../../assets/styles";
import { connect } from "react-redux";
import _ from "lodash";
import { getRentalPackage, getVehicleAttribute, getMyBooking, getReservationDetail } from "../../../../../actions";
import moment from "moment";
import "moment/locale/id";
import { CAR_RENTAL, AIRPORT_TRANSFER, SELF_DRIVE } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class CarDetail extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      currentPageRender : this.props.currentPage,
      list: props.item.details,
      activeList: props.item.details.length > 0 ? props.item.details[0] : [],
      activeListIndex: 0,
      allVehicleList: [],
      modalPaymentGuide: false,
      activeDropIndex: 0,
      isDetail: props.item.details.length > 0 ? true : false
    };

    this.mainImageScroll = null;
  }

  componentDidMount = () => {
    this.props.getRentalPackage();
    this.setState({ allVehicleList: this.props.vehicle });
    if(this.props.ReservationId) {
      this.props.getReservationDetail({
        token: this.props.login.token,
        ReservationId: this.props.ReservationId
      });
    }
    // // if(Object.keys(this.props.item).length > 0) {
    //   this.setState({ 
    //     allVehicleList: this.props.vehicle,
    //     list: this.props.item.details,
    //     activeList: this.props.item.details[0]
    //   });
    // // } else {
    // //   this.props.getVehicleAttribute({ token: this.props.login.token });
    // //   this.props.getReservationDetail({
    // //     token: state.authentication.login.token,
    // //     ReservationId: this.props.ReservationId
    // //   });
    // // }
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.detailReservation !== prevProps.detailReservation) {
      return { detailReservation: this.props.detailReservation }
    }
    if(this.props.vehicle !== prevProps.vehicle) {
      return { vehicle: this.props.vehicle }
    }
    return null;
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    if(snapshot !== null) {
      if(snapshot.detailReservation) {
        this.setState({ 
          activeList: snapshot.detailReservation.details[0],
          list: snapshot.detailReservation.details,
          isDetail: true
        })
      }
      if(snapshot.vehicle) {
        this.setState({ 
          allVehicleList: this.props.vehicle
        })
      }
    }
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------

  goToDriverReview() {
    Actions.DriverReview({
      navData: {
        onComplete: () => this.setState({ rating: 1 })
      }
    });
  }

  // ---------------------------------------------------

  switchList(index, newList = null) {
    if (this.state.activeListIndex == index && !newList) {
      return false;
    }

    const list = newList ? newList : this.state.list;

    if (index > list.length - 1) {
      index = 0;
    } else if (index < 0) {
      index = list.length - 1;
    }

    let imageIndex = index;
    if (newList && index == list.length - 1) {
      imageIndex  = imageIndex + 1;
    }

    this.mainImageScroll.scrollTo({x: imageIndex * appMetrics.screenWidth});

    this.setState({
      activeListIndex: index,
      activeList: this.state.list[index],
      list,
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderRatingStars() {
    let rateButton = this._renderReviewDriverButton();

    if (this.state.rating > 0) {
      rateButton = this._renderStarsImage();
    }

    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        paddingVertical: 20,
      }}>
        {rateButton}
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCarDetailContent(){
  let carDetailRender = null;
   if(this.state.currentPageRender=="My Booking"){
    carDetailRender = this.state.isDetail ? this._renderMyBooking() : null
   } else {
     carDetailRender = this._renderHistoryBooking();
   }
   return(
     <View style={{
       flex :1,
       flexDirection:'row'
     }}>
       {carDetailRender}
     </View>
   )
  }
  // ---------------------------------------------------

  _renderTopContent() {
    return (
      <View style={styles.topContentWrapper}>
        <View style={styles.topContentItem}>
          <Text color={appVariables.colorGray} size={11}>
            {Lang.carTotal[this.props.lang]}
         </Text>
          <Text color={appVariables.colorBlack} size={14}>
            {this.state.list.length} {this.state.length > 1 ? `Cars` : `Car`}
          </Text>
        </View>
        <View style={styles.topContentItem}>
          <Text color={appVariables.colorGray} size={11}>
            {Lang.reservationNumber[this.props.lang]}
         </Text>
          <Text color={appVariables.colorBlack} size={14}>
            {/* {this.props.item.ReservationId} */}
            {this.state.list[0].ReservationId}
          </Text>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderModifyAndExtend() {
    return (
      <View style={styles.topContentWrapper}>
        <View style={styles.modifyAndExtendItem}>
          <Image
            source={require("@images/png/myBooking/icoModify.png")}
            style={{ height: 20, width: 20 }}
          />
          <Text color={appVariables.colorBlack} size={12} style={{ paddingHorizontal: 10 }}>
            MODIFY
        </Text>
        </View>
        <View style={styles.modifyAndExtendItem}>
          <Image
            source={require("@images/png/myBooking/icoExtend.png")}
            style={{ height: 20, width: 20 }}
          />
          <Text color={appVariables.colorBlack} size={12} style={{ paddingHorizontal: 10 }}>
            EXTEND
        </Text>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderBookingProgressBar() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          {this._renderBookingProgressBar1()}
          {this._renderBookingProgressBar2()}
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderBookingProgressBar1() {
    switch (this.state.activeList.ActivityId) {
      case "AMID-009#ASID-015":  // Car On The Way
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={styles.progressBar.circleGray} />
            <View style={ styles.progressBar.rectangleGray} />
            <View style={ styles.progressBar.circleGray} />
          </View>
        );
      case "AMID-018#ASID-015": // Car On The Way
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={styles.progressBar.circleGray} />
            <View style={ styles.progressBar.rectangleGray} />
            <View style={ styles.progressBar.circleGray} />
          </View>
        );
      case "AMID-002#ASID-017": // Car With You
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleGray} />
            <View style={ styles.progressBar.circleGray} />
          </View>
        );
      case "AMID-002#ASID-016":
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={ styles.progressBar.circleOrange} />
          </View>
        );
      case "AMID-006#ASID-017":
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={ styles.progressBar.circleOrange} />
          </View>
        );
      case "AMID-009#ASID-017":
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={styles.progressBar.circleOrange} />
            <View style={ styles.progressBar.rectangleOrange} />
            <View style={ styles.progressBar.circleOrange} />
          </View>
        );
      default:
        return (
          <View style={styles.progressBar}>
            <View style={ styles.progressBar.circleGray} />
            <View style={ styles.progressBar.rectangleGray} />
            <View style={styles.progressBar.circleGray} />
            <View style={ styles.progressBar.rectangleGray} />
            <View style={ styles.progressBar.circleGray} />
          </View>
        );
    }
  }

  // ---------------------------------------------------
  _renderBookingProgressBar2() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              {Lang.carOnTheWay[this.props.lang]}
          </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              {Lang.carWithYou[this.props.lang]}
          </Text>
          </View>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              {Lang.complete[this.props.lang]}
          </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderReviewDriverButton() {
    return (
      <TouchableOpacity
        style={styles.driverReviewButton}
        onPress={() => this.goToDriverReview()}>
        <Text color={appVariables.colorBlack}>
          REVIEW DRIVER
          </Text>
      </TouchableOpacity>
    );
  }

  // ---------------------------------------------------
  _renderCancelBookingButton() {
    return (
      <TouchableOpacity
        style={styles.driverReviewButton}
        onPress={() => Actions.push("CancelBookingList", {
          item: this.props.item
        })}>
        <Text color={appVariables.colorBlack}>
          CANCEL BOOKING
          </Text>
      </TouchableOpacity>
    );
  }

  // ---------------------------------------------------
  _renderStarsImage() {
    return (
      <Image
        source={require('@images/png/stars-rate.png')}>
      </Image>
    );
  }

  // ---------------------------------------------------
  _renderRatingDriver() {
    if(this.props.item.Status === 'BOSID-003' || this.props.item.Status === 'BOSID-007') {
      return (
        <View style={{
          flex: -1,
          paddingTop:20,
          paddingHorizontal: 20,
        }}>
            <Text
              size={18}>
              Rating Driver
          </Text>
          {this._renderRatingStars()}
        </View>
      );
    } else {
      return <View />
    }
    
  }

  // ---------------------------------------------------
  _renderDriverInformation() {
    return (
      <View style={styles.driverInformationWrapper}>
        <Text size={18}>
          { this.state.activeList.MsProductServiceId === SELF_DRIVE ? Lang.vehicleInformation[this.props.lang] : Lang.driverInformation[this.props.lang] }
        </Text>
        { this.state.activeList.MsProductServiceId !== SELF_DRIVE && 
        <Text
          size={14}
          style={{ paddingBottom: 10, paddingTop: 20 }}>
          {Lang.name[this.props.lang]}: {this.state.activeList.drivers.length > 0 ? this.state.activeList.drivers[0].DriverName : "Waiting"}
        </Text> }
        { this.state.activeList.MsProductServiceId !== SELF_DRIVE && 
        <Text
          size={14}
          style={{ paddingBottom: 10 }}>
          {Lang.phoneNumber[this.props.lang]}: {this.state.activeList.drivers.length > 0 ? this.state.activeList.drivers[0].Phone : "Waiting"}
        </Text> }
        <Text
          size={14}
          style={{ paddingBottom: 10 }}>
          {Lang.vehicleLicensePlate[this.props.lang]}: {this.state.activeList.LicensePlate !== null ? this.state.activeList.LicensePlate : "Waiting"}
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderBookingDescription(label, description1, description2) {
    return (
      <View
        style={styles.bookingDescriptionWrapper}>
        <Text
          size={14}>
          {label}
        </Text>
        <Text
          size={14}>
          {description1}
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderBookingDescriptionDate(label, description1, description2) {
    return (
      <View
        style={styles.bookingDescriptionWrapper}>
        <Text
          size={14}>
          {label}
        </Text>
        <Text
          size={14}>
          {description1}
        </Text>
        <Text
          size={14}>
          {description2}
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderPaymentDetails() {
    return (
      <View
        style={styles.paymentDetailWrapper}>
        <Text
          size={14}
          style={{ paddingVertical: 20 }}>
          {Lang.paymentDetails[this.props.lang]}
        </Text>
        { _.map(this.state.list, (v, i) => {
          var vehicle = _.filter(this.state.allVehicleList, { vehicleTypeId: v.UnitTypeId });
          if(vehicle.length > 0) {
            return this._renderCarList(vehicle[0].vehicleAlias, i, v)
          }
        }) }
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCarList(label, index, data) {
    // let totalPrice = this.state.activeList.MsProductId !== AIRPORT_TRANSFER ? parseInt(data.Price) * parseInt(data.TotalDay) : parseInt(data.Price);
    let totalPrice = parseInt(data.Price);
    let priceAfterDiscount = data.reservation_promos.length > 0 ? parseInt(data.Price) - parseInt(data.reservation_promos[0].Amount) : 0
    return (
      <View
        key={index}
        style={styles.carListWrapper}>
        <Text size={14}>
          {label}
        </Text>
        { this.state.activeList.MsProductId === CAR_RENTAL ? 
          this._renderCarListDetail(`Rp ${NumberHelper.format(parseInt(data.Price))} for ${data.TotalDay} Day(s)`, `Rp ${NumberHelper.format(totalPrice)}`, (index+2) * 6, priceAfterDiscount > 0 ? true : false ) : 
          this._renderCarListDetail(`Rp ${NumberHelper.format(parseInt(data.Price))}`, `Rp ${NumberHelper.format(totalPrice)}`, (index+2) * 6, priceAfterDiscount > 0 ? true : false ) }
        { priceAfterDiscount > 0 && this._renderCarListDetail(`Rp ${NumberHelper.format(parseInt(priceAfterDiscount))} x ${data.TotalDay} Day(s)`, `Rp ${NumberHelper.format((priceAfterDiscount * parseInt(data.TotalDay)))}`, (index+1) * 6) }
        { data.reservation_extras.length > 0 && _.map(data.reservation_extras[0].details, (v, i) => {
          return this._renderCarListDetail(`${v.MsExtrasName}: Rp ${NumberHelper.format(parseInt(v.Price))} x ${v.Qty} Item(s)`, `Rp ${NumberHelper.format( (parseInt(v.Price) * parseInt(v.Qty)) )}`, (i+1)) 
        })}

        { data.extend_durations.length > 0 && data.extend_durations.map((v, i) => {
          return this._renderCarListDetail(`Additional Hour: x ${v.AddDuration} Hour(s)`, `Rp ${NumberHelper.format(parseInt(v.AddDurationPrice) )}`, (i+1)) 
        })}
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCarListDetail(description, price, index = 0, isDiscount = false) {
    return (
      <View
        key={index}
        style={{
          flex: 1,
          flexDirection: "row",
        }}>
        <View style={{ flex: 1 }}>
          <Text style={ isDiscount ? { textDecorationLine: "line-through", textDecorationStyle: "solid", textDecorationColor: "red" } : {} }>
            {description}
          </Text>
        </View>
        <View style={{ flex: 1, alignItems: "flex-end"}}>
          <Text style={ isDiscount ? { textDecorationLine: "line-through", textDecorationStyle: "solid", textDecorationColor: "red" } : {} }>
            {price}
          </Text>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderTotalPayment(totalPayment) {
    return (
      <View
        style={styles.totalPaymentWrapper}>
        <View style={{
          flex: 1,
        }}>
          <Text>
            {Lang.totalPayment[this.props.lang]}
        </Text>
        </View>
        <View style={{
          flex: 1,
          alignItems: "flex-end",
        }}>
          <Text size={20}>
            {totalPayment}
          </Text>
        </View>
      </View>
    );
  }

  _renderModalPaymentGuide() {
    let currentTime = moment().unix();
    let eventTime= moment(this.props.item.WaitingForPaymentTime).unix();
    let diffTime = eventTime - currentTime;
    let duration = moment.duration(diffTime*1000, 'milliseconds');
    let hour = duration.hours() < 10 ? `0${duration.hours()}` : duration.hours();
    let minutes = duration.minutes() < 10 ? `0${duration.minutes()}` : duration.minutes();
    let mandiri = `Payment via ATM : 

1. Masukkan kartu ATM Mandiri, lalu masukkan PIN ATM
2. Pilih menu "Bayar/Beli"
3. Pilih "Lainnya" dan pilih "Lainnya" kembali
4. Pilih "Multi Payment"
5. Masukkan 5 digit awal dari nomor Mandiri VA (Virtual Account) yang di dapat (contoh: 89022)
6. Masukkan keseluruhan nomor VA
7. Masukkan jumlah pembayaran
8. Nomor VA, Nama dan Jumlah pembayaran akan ditampilkan di layar
9. Tekan angka 1 dan pilih "YA"
10. Konfirmasi pembayaran dan pilih "YA"
11. Transaksi selesai. Mohon simpan bukti transaksi.

Payment via E-Banking :

1. Install aplikasi Mandiri Online
2. Masukkan User ID dan PIN, kemudian login
3. Pilih Menu "Pembayaran"
4. Klik Buat "Pembayaran Baru"
5. Pilih "Multipayment" 
6. Pilih "DOKU Merchant" pada bagian penyedia jasa  
7. Masukkan Nomor VA  
8. Klik "Lanjut", layar akan menampilkan nomor VA
9. Klik "Konfirmasi" lalu masukan nominal transaksi
10. Klik "Lanjut" 
11. Klik "Konfirmasi"  
12. Masukkan MPIN (PIN SMS Banking)  
13. Selesai dan Simpan Bukti Pembayaran Anda`;
    let permata = `Payment via ATM : 

1. Pilih TRANSAKSI LAINNYA
2. Pilih PEMBAYARAN
3. Pilih PEMBAYARAN LAINNYA
4. Pilih VIRTUAL ACCOUNT
5. Masukan nomor Virtual Account, lalu pilih BENAR
6. Masukkan nominal yang akan dibayarkan, pilih BENAR
7. Pilih rekening yang akan digunakan untuk melakukan pembayaran
8.Transaksi selesai. Simpan struk ATM sebagai bukti bayar

Payment via E-Banking :

1. Log in ke Permata Mobile Internet
2. Pilih menu PEMBAYARAN TAGIHAN
3. Pilih VIRTUAL ACCOUNT
4. Masukan nomor Virtual Account
5. Masukkan nominal pembayaran
6. Masukkan token transaksi
7. Transaksi selesai`
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalPaymentGuide}
        onRequestClose={() => this.setState({ modalPaymentGuide: !this.state.modalPaymentGuide }) }>
        <SafeAreaView style={{flex: 1, backgroundColor: appVariables.colorBlueHeader }}>
            <Header title={"Payment Guide"} onBackPress={ () => this.setState({modalPaymentGuide: !this.state.modalPaymentGuide}) }/>
            <View style={{ flex: 1, backgroundColor: '#FFFFFF', paddingHorizontal: 20, paddingBottom: 20 }}>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingTop: 10, flex: 1 }}>
                  <Text size={15} color={appVariables.colorGray} style={{paddingBottom: 2}}>Please complete your payment in</Text>
                  <Text size={15}>{`${hour} Hour ${minutes} Minutes`}</Text>
                </View>
                <View style={{ paddingTop: 10, flex: 1 }}>
                  <Text size={15} color={appVariables.colorGray} style={{paddingBottom: 2}}>Virtual Account Number:</Text>
                  <Text size={15}>{this.props.item.VANumber}</Text>
                </View>

                {this._renderDropdown("Bank Permata", permata, 0)}
                {this._renderDropdown("Bank Mandiri", mandiri, 1)}
              </ScrollView>
            </View>
        </SafeAreaView>
      </Modal>
    )
  }

  _renderDropdown(label, content, index) {
    const isActive = this.state.activeDropIndex == index;

    let contentRender = null;
    if (isActive) {
      contentRender = this._renderContent(content);
    }

    return (
      <View style={{
        flex: 1,
        paddingHorizontal: 0,
        paddingTop: 10,
        paddingBottom: 0,
      }}>
        <TouchableOpacity style={{
          flex: 1,
          flexDirection: 'row',
          paddingTop: 10,
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: appVariables.colorGray
        }}
          onPress={ () => this.toggleDrop(index) }
        >
          <View style={{flex: 1}}>
            <Text size={16}>
              {label}
            </Text>
          </View>

          <View>
            <Icon size={20} name={ isActive ? "angle-up" : "angle-down" } />
          </View>
        </TouchableOpacity>

        { contentRender }

      </View>
    )
  }

  _renderContent(content) {
    return (
      <View style={{ paddingTop: 30, paddingBottom: 20 }}>
        <Text color={ appVariables.colorGray }>
          { content }
        </Text>
      </View>
    );
  }

  toggleDrop(index) {
    if (this.state.activeDropIndex == index) {
      return false;
    }

    this.setState({
      activeDropIndex: index,
    });
  }

  // ---------------------------------------------------
  _renderLineSeparator() {
    return (
      <View
        style={styles.lineSeparator}>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCarSlider() {
    return (
      <View>
        <View style={styles.carSliderStyle}>
          <Image style={styles.carSliderStyle}
            resizeMode={"cover"}
            source={require("@images/png/order-detail-header.png")}
          />
        </View>
        <VehicleSliderDetail
            type={this.props.currentPage}
            setScrollRef={ sv => this.mainImageScroll = sv }
            serviceType={ "" }
            list={ this.state.list }
            activeList={ this.state.activeList }
            activeListIndex={ this.state.activeListIndex }
            // checkoutData={ this.props.checkoutData }
            onSwitchList={ (index) => this.switchList(index) }
          />
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationDescription() {
    let x = _.filter(this.props.time, { MsProductId: this.state.activeList.MsProductId });
    let CancelationTime = x[0].configuration_adjustment_retail_prices[0].CancelationTime;
    var toMin = moment.duration(CancelationTime).asMinutes();
    var setHour = (new moment(this.state.activeList.StartDate, "YYYY-MM-DD HH:mm:ss")).subtract(toMin, "minutes");
    var now = moment().unix();
    var targetDate = setHour.unix();
    var diffTime = targetDate - now;
    var duration = moment.duration(diffTime*1000, 'milliseconds');
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 20,
        paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <Text
            color="#1A3681">
            Cancellation is Free
            </Text>
          <Text
            color={appVariables.colorBlack}
            style={{ paddingVertical: 10 }}>
            {Lang.for[this.props.lang]}: {duration.days() >= 0 ? duration.days() : 0} {Lang.days[this.props.lang]} {duration.hours() >= 0 ? duration.hours() : 0} {Lang.hour[this.props.lang]} {duration.minutes() >= 0 ? duration.minutes() : 0} {Lang.minutes[this.props.lang]}
          </Text>
          <Text
            color={appVariables.colorBlack}
            style={{ paddingVertical: 10 }}>
            {Lang.cancelQuote[this.props.lang]} { setHour.locale('en').format("LLL") } {Lang.jakartaTime[this.props.lang]}
          </Text>
          <View style={styles.cancellationContentStyle}>
            {this._renderCancellationProgressBar()}
          </View>
          { moment().isSameOrBefore(setHour) && 
          <View style={styles.cancellationContentStyle}>
            {this._renderCancelBookingButton()}
          </View> }
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          {this._renderCancellationProgressBar1()}
          {this._renderCancellationProgressBar2()}
          {this._renderCancellationProgressBar3()}
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar1() {
    let x = _.filter(this.props.time, { MsProductId: this.state.activeList.MsProductId });
    let ChargeCancelation = x[0].configuration_adjustment_retail_prices[0].ChargeCancelation;
    let CancelationFee = (parseInt(this.props.item.TotalPrice) * (parseInt(ChargeCancelation) / 100));
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          {/* <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              FREE
            </Text>
          </View> */}

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              Cancellation Fees
            </Text>
            <Text
              color={appVariables.colorGray}
              size={12}>
              Rp { NumberHelper.format(CancelationFee) }
            </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              style={{ paddingTop: 10 }}
              size={12}>
              Non-Refundable
            </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar2() {
    return (
      <View style={styles.progressBar}>
        <View style={{
          flex: 1,
          flexDirection: 'row'
        }}>
          <View style={styles.circleWrapper}>
            <View style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: "center",
            }}>
              <View style={styles.progressBar.circleOrange}>
              </View>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleOrange}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleOrange}>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleOrange}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleOrange}>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleGray}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleGray}>
            </View>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar3() {
    let x = _.filter(this.props.time, { MsProductId: this.state.activeList.MsProductId });
    let CancelationTime = x[0].configuration_adjustment_retail_prices[0].CancelationTime;
    var toMin = moment.duration(CancelationTime).asMinutes();
    var setHour = (new moment(this.state.activeList.StartDate, "YYYY-MM-DD HH:mm:ss")).subtract(toMin, "minutes");
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              { moment().isSameOrBefore(setHour.format("YYYY-MM-DD HH:mm:ss")) ? "Today": null }
            </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              {setHour.locale('en').format("lll")}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  // My Booking 
  _renderMyBooking() {
    return (
      <ScrollView style={{
        flex: 1,
        backgroundColor: appVariables.colorWhite,
        borderWidth: 1,
      }}>
        {this._renderTopContent()}

        {this._renderCarSlider()}
        {this._renderLineSeparator()}
        {this._renderBookingProgressBar()}
        {this._renderLineSeparator()}

        {this._renderDriverInformation()}
        {this._renderLineSeparator()}

        { this.state.activeList.MsProductId === CAR_RENTAL && this._renderBookingDescription(Lang.packages[this.props.lang].toUpperCase(), `${this.state.activeList.Duration} Hours`) }
        {this._renderBookingDescriptionDate(
          Lang.dates[this.props.lang].toUpperCase(), 
          moment(this.state.activeList.StartDate).format("ll") + ( this.state.activeList.EndDate !== null ? " -> " + moment(this.state.activeList.EndDate).format('ll') : "" ), 
          "Start " + moment(this.state.activeList.StartDate).format("HH:mm") + ( this.state.activeList.EndDate !== null ? " - End " + moment(this.state.activeList.EndDate).format('HH:mm') : "" ))}
        {this._renderBookingDescription(Lang.pickUpLocation[this.props.lang].toUpperCase(), this.state.activeList.pickup_locations[0].Alamat)}
        {this._renderBookingDescription(Lang.notesRequest[this.props.lang].toUpperCase(), this.props.item.NotesReservation || "-")}
        {this._renderPaymentDetails()}
        {/* {this._renderTotalPayment(`Rp ${NumberHelper.format(parseInt(this.props.item.TotalPrice))}`)} */}
        {/* {this._renderTotalPayment(`Rp. ${NumberHelper.format(parseInt(this.state.list[0].Price))}`)} */}
        {this._renderTotalPayment(`Rp. ${NumberHelper.format(parseInt(this.props.item.TotalPrice))}`)}

        { this.props.item.Status === "BOSID-010" && <View style={{ flex: 1, paddingVertical: 10, paddingHorizontal: 20, justifyContent: 'center', alignItems: 'flex-end' }}>
          <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }} onPress={() => this.setState({ modalPaymentGuide: !this.state.modalPaymentGuide }) }>
            <Icon name="question-circle" size={14} color={appVariables.colorOrange} style={{ marginRight: 10, marginTop: 2 }} />
            <Text style={{ fontSize: 12, color: appVariables.colorOrange }}>Payment Guide</Text>
          </TouchableOpacity>
        </View> }

        {this._renderCancellationDescription()}

        {this._renderModalPaymentGuide()}

      </ScrollView>
    );
  }

  // ---------------------------------------------------
  // History Booking 
  _renderHistoryBooking() {
    return (
      <ScrollView style={{
        flex: 1,
        backgroundColor: appVariables.colorWhite,
        borderWidth: 1,
      }}>
  
        {this._renderTopContent()}
        {this._renderCarSlider()}
        {this._renderLineSeparator()}
        {this._renderBookingProgressBar()}
        {this._renderLineSeparator()}

        {this._renderRatingDriver()}
        {this._renderDriverInformation()}
        {this._renderLineSeparator()}

        { this.state.activeList.MsProductId === CAR_RENTAL && this._renderBookingDescription(Lang.packages[this.props.lang].toUpperCase(), `${this.state.activeList.Duration} Hours`) }
        {this._renderBookingDescriptionDate(
          Lang.dates[this.props.lang].toUpperCase(), 
          moment(this.state.activeList.StartDate).format("ll") + ( this.state.activeList.EndDate !== null ? " -> " + moment(this.state.activeList.EndDate).format('ll') : "" ), 
          "Start " + moment(this.state.activeList.StartDate).format("HH:mm") + ( this.state.activeList.EndDate !== null ? " - End " + moment(this.state.activeList.EndDate).format('HH:mm') : "" ))}
        {this._renderBookingDescription(Lang.pickUpLocation[this.props.lang].toUpperCase(), this.state.activeList.pickup_locations[0].Alamat)}
        {this._renderBookingDescription(Lang.notesRequest[this.props.lang].toUpperCase(), this.props.item.NotesReservation || "-")}
        {this._renderPaymentDetails()}
        {this._renderTotalPayment( _.isNaN(parseInt(this.props.item.TotalPrice)) ? "Cancelled" : `Rp ${NumberHelper.format(parseInt(this.props.item.TotalPrice))}`)}

      </ScrollView>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------
  render() {
    return (
      <BaseContainer>
        <Header title={Lang.reservationDetail[this.props.lang]} onBackPress={() => this.props.navigation.pop()}/>
        {this._renderCarDetailContent()}
      </BaseContainer>
    );

  }
}

const mapStateToProps = ({ rentalPackage, vehicleAttribute, adjustmentTime, language, authentication, reservationSave }) => {
  const { rentalDuration } = rentalPackage;
  const { vehicle } = vehicleAttribute;
  const { time } = adjustmentTime;
  const { lang } = language;
  const { login } = authentication;
  const { detailReservation } = reservationSave;
  return { rentalDuration, vehicle, time, lang, login, detailReservation }
}

export default connect(mapStateToProps, {
  getRentalPackage,
  getVehicleAttribute, 
  getMyBooking,
  getReservationDetail
})(CarDetail)
