import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import MainButton from "@airport-transfer-fragments/MainButton";
import styles from "./style";
import { Actions } from "react-native-router-flux";

export default class DeleteCreditCardConfirmation extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  goToAddCreditCard() {
    Actions.AddCreditCard();
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderQuestion() {
    return (
      <View style={styles.questionWrapper}>
        <Text size={18}>
          Bryan, Are you sure you want
        </Text>
        <Text size={18}>
          to delete your credit card?
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderDescription() {
    return (
      <View style={styles.descriptionWrapper}>
        <Text
          color={appVariables.colorGray}
          size={16}>
          add card if you want to use payment with
        </Text>
        <Text
          color={appVariables.colorGray}
          size={16}>
          your credit card
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderButton() {
    return (
      <View style={styles.buttonWrapper}>
        <View style={styles.subButtonWrapper}>
          <MainButton
            label={"CONFIRM DELETE CREDIT CARD"}
            style={styles.orangeButtonStyle}
            onPress={() => this.goToAddCreditCard()}>
          </MainButton>
        </View>

        <View style={styles.subButtonWrapper}>
          <TouchableOpacity
            style={styles.whiteButtonStyle}
            onPress={() => onPress()}>
            <Text color={appVariables.colorBlack}>
              CANCEL
          </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Credit/Debit Card"}
          onBackPress={() => this.props.navigation.pop()} />

        <View style={styles.mainContainer}>
          {this._renderQuestion()}
          {this._renderDescription()}
          {this._renderButton()}
        </View>

      </BaseContainer>
    );
  }
}