import { appMetrics } from "@variables";

export default {
  mainContainer: {
    paddingVertical: appMetrics.windowHeight / 4,
    paddingHorizontal: 30,
  },
  questionWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  descriptionWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  buttonWrapper: {
    flex: -1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  subButtonWrapper: {
    flex: -1,
    flexDirection: 'row',
    paddingVertical: 10,
  },
  orangeButtonStyle: {
    borderRadius: 7,
    flex: -1,
    paddingHorizontal: 20,
  },
  whiteButtonStyle: {
    flex: -1,
    borderRadius: 7,
    height: 47,
    borderWidth: 1,
    backgroundColor: 'white',
    paddingHorizontal: 110,
    justifyContent: 'center',
    alignItems: 'center',
  },
}