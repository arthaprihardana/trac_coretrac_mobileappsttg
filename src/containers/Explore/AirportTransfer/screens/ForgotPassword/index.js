/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-04 17:57:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 17:09:18
 */
import { Container, Content } from "native-base";
import { Actions } from "react-native-router-flux";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from 'react-redux';
import { postForgotPassword } from '../../../../../actions';
import Lang from '../../../../../assets/lang'
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import MainButton from "@airport-transfer-fragments/MainButton";
import AuthContainer from "@airport-transfer-fragments/AuthContainer";

import styles from "./style";
import { REGEX_EMAIL } from "../../../../../constant";


class ForgotPassword extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        email: "",
      },
      readyToSubmit: false,
      errorEmail: null
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.forgotPassword !== prevProps.forgotPassword) {
      return { forgotPassword: this.props.forgotPassword }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.forgotPassword !== undefined) {
        // Actions.Otp({
        //   navData: this.props.navData,
        //   email: this.state.email
        // });
        Actions.push("Otp", { 
          email: this.state.form.email 
        })
      }
    }
  }
  
// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form,
      errorEmail: REGEX_EMAIL.test(value) ? null : "Email didn't valid"
    });
  }

  onValidate = async current => {
    let except = [];
    except.push(current);
    let o = _.omit(this.state.form, except);
    let a = await _.findKey(o, val => {
      if(_.isEmpty(val)) { return true }
    });
    return a;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// --------------------------------------------------- 

  goToOtp() {
    this.props.postForgotPassword({ 
      email: this.state.form.email
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderForm() {
    return (
      <View>
        <Text style={{ paddingTop: 10 }}>
          {Lang.forgotPasswordQuote[this.props.lang]}
        </Text>

        <FloatingTextInput
          label={ Lang.email[this.props.lang] }
          onChangeText={ (value) => {
            this.onValidate("email").then(val => {
              this.onChangeForm("email", value) 
              this.setState({ readyToSubmit: value !== "" && val === undefined && this.state.errorEmail === null ? true : false })
            });
          }}
          value={ this.state.form.email }
          keyboardType={ "email-address" }
          note={this.state.errorEmail}
        />

        <MainButton
          label={ Lang.sendButton[this.props.lang] }
          isLoading={this.props.loading}
          onPress={ () => this.state.readyToSubmit ? this.goToOtp() : {}}
          rounded
          style={{
            marginTop: 40,
          }}
          disabled={!this.state.readyToSubmit}
        />

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <AuthContainer 
        { ... this.props }
        title={ Lang.forgotPassword[this.props.lang] }
      >

        { this._renderForm() }        

      </AuthContainer>
    );
  }
}

const mapStateToProps = ({ authentication, language }) => {
  const { loading, forgotPassword } = authentication;
  const { lang } = language;
  return { loading, forgotPassword, lang };
}

export default connect(mapStateToProps, {
  postForgotPassword
})(ForgotPassword);
