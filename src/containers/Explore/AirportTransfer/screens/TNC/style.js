import {
	appVariables,
} from "@variables";

export default {
	searchBar: {
		blueBox: {
			wrapper: {
				flex: 1,
				flexDirection: "column",
				backgroundColor: "#1A3681",
				position: "relative",
				textWrapper: {
					flex: 1,
					flexDirection: "row",
					paddingVertical: 20,
					paddingHorizontal: 20,
					height: 120,
				},
				findTopicBoxWrapper: {
					flex: 1,
					flexDirection: "row",
					position: "absolute",
					top: 55,
				},
			},
		},
	},
	findTopicBox: {
		wrapper: {
			flex: 1,
			flexDirection: "row",
			paddingVertical: 30,
			paddingHorizontal: 20,
			findTopicTextArea: {
				flex: 4,
				flexDirection: "column",
				backgroundColor: appVariables.colorWhite,
				borderRadius: 5,
			},
			searchIconWrapper: {
				flex: 1,
				flexDirection: "column",
				backgroundColor: "#F27C21",
				borderRadius: 5,
			},
		},
	},
	listTopicButton: {
		wrapper: {
			flex: 1,
			flexDirection: "row",
			paddingTop: 60,
			paddingHorizontal: 20,
			paddingVertical: 20,
		},
		buttonSeparator: {
			flex: 1,
			flexDirection: "column",
			paddingHorizontal: 10,
		},
	},
	activeButtonStyle: {
		flex: 1,
		flexDirection: "row",
		backgroundColor: "#F27C21",
		borderRadius: 50,
		paddingVertical: 10,
		paddingHorizontal: 20,
		alignItems: "center",
		justifyContent: "center",
	},
	inactiveButtonStyle: {
		flex: 1,
		flexDirection: "row",
		backgroundColor: "#CCD1DB",
		borderRadius: 50,
		paddingVertical: 10,
		paddingHorizontal: 20,
		alignItems: "center",
		justifyContent: "center",
	},
	expandableViewTitle: {
		flex: 1,
		flexDirection: "row",
		paddingVertical: 20,
		paddingHorizontal: 20,
	},
	dropdown: {
		container: {
			flex: -1,
			padding: 20,
			paddingTop: 10,
			paddingBottom: 0,
		},
		label: {
			container: {
				flex: -1,
				flexDirection: 'row',
				paddingTop: 10,
				paddingBottom: 5,
				borderBottomWidth: 1,
				marginBottom: 20,
				borderBottomColor: "#d8d8d8",
			},
			textContainer: {
				flex: 1,
			},
			innerContainer: {
				flex: -1,
			},
		},
		content: {
			container: {
				paddingVertical: 20,
				paddingHorizontal: 20,
				marginBottom: 20,
				backgroundColor: "#e7e7e7"
			},
		},
	},
}