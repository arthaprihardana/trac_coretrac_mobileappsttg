let tnc = [
    {
        category: "SYARAT & KETENTUAN",
        question: "Pendahuluan",
        answer: `Pendahuluan

Terima kasih atas kunjungan Anda ke website Kami, www.trac.astra.co.id. Kami berharap Anda merasa nyaman dalam mengakses dan menggunakan seluruh Layanan yang tersedia di website Kami. Kami terus berupaya memperbaiki dan meningkatan mutu pelayanan Kami, dan sangat menghargai segala saran dan masukan Anda. Silakan menyampaikannya kepada Kami melalui nomor (021)87787787 atau mengirimkan email ke rco.nasional@trac.astra.co.id.

Website ini dimiliki, dioperasikan, dan diselenggarakan oleh TRAC (“Kami”), yang telah memberikan Layanan penyewaan kendaraan dan solusi transportasi sejak 1986 (“Layanan”). Layanan Kami tersedia secara online melalui website www.trac.astra.co.id (“website” atau “situs TRAC”) atau melalui berbagai akses berbasis internet via media, perangkat dan platform lainnya, baik yang sudah atau akan tersedia dikemudian hari. Selain itu, Anda juga dapat mengakses Layanan TRAC secara langsung di berbagai 46 outlet dan jaringan TRAC yang tersebar di kota-kota besar Indonesia.

Dengan mengakses dan menggunakan website dan Layanan Kami, Anda menyatakan telah membaca, memahami, dan menyetujui untuk tunduk pada Syarat dan Ketentuan dan segala perubahannya. Syarat dan Ketentuan ini mengatur ketentuan yang berlaku untuk setiap akses dan penggunaan Anda terhadap Layanan TRAC yang tersedia pada website.

Syarat dan Ketentuan dapat Kami ubah, modifikasi, tambah, hapus, atau koreksi ("perubahan") sewaktu-waktu sesuai dengan kebijakan Kami. Oleh karena itu, Kami sangat menganjurkan Anda untuk mengunjungi situs TRAC secara berkala agar dapat mengetahui adanya perubahan tersebut.

Syarat dan Ketentuan ini berlaku menggantikan semua versi sebelumnya, terhitung sejak versi ini diunggah ke situs TRAC.`
    },
    {
        category: "SYARAT & KETENTUAN",
        question: "Umum",
        answer: `Ketentuan Umum Layanan TRAC

1. Anda bisa melakukan pemesanan/reservasi secara online melalui situs TRAC untuk Layanan rental kendaraan individu (“Personal Rental”). Anda dapat mencari informasi tentang penyewaan kendaraan yang Anda inginkan, melakukan transaksi pemesanan sekaligus pembayaran secara online dengan aman melalui berbagai sistem dan fasilitas pembayaran yang tersedia di website Kami.

2.  Untuk menggunakan Layanan reservasi online tersebut, Anda harus terlebih dulu menjadi mendaftarkan diri menjadi member dengan mengisi data diri berupa nama dan alamat email. Lalu, Anda juga wajib melengkapi persyaratan tertentu untuk menggunakan Layanan TRAC.

3.  Khusus Layanan TRAC untuk perusahaan (“Corporate Lease” dan “Astra Fleet Management Solution”) dapat diakses dengan menghubungi langsung cabang TRAC terdekat di kota Anda. Situs TRAC hanya menampilkan informasi detail terkait Layanan yang tersedia untuk kebutuhan perusahaan tersebut. Anda tidak dapat melakukan transaksi reservasi via online untuk jenis-jenis Layanan ini. Silahkan klik tautan ini untuk melihat daftar cabang TRAC.

4.  Layanan Personal Rental (yang di dalamnya terdapat Daily Car Rental, Airport Transfer, dan Bus Rental) secara umum dapat diakses via online selama dua puluh empat jam sehari dan tujuh hari seminggu; kecuali dalam hal adanya perbaikan, peningkatan atau pemeliharaan pada website Kami.

5.  Meski demikian, reservasi Layanan Personal Rental melalui situs web hanya bisa diakses untuk pemakaian paling cepat satu hari kerja ke depan. Jadi, Anda harus melakukan pemesanan minimal satu hari kerja sebelum hari pemakaian.`
    },
    {
        category: "SYARAT & KETENTUAN",
        question: "Penggunaan",
        answer: `Ketentuan Penggunaan

1. Website ini dan Layanan yang tersedia di dalamnya dapat digunakan oleh Anda hanya untuk penggunaan pribadi dan atau secara non-komersial serta setiap saat tunduk pada Syarat dan Ketentuan juga Kebijakan Privasi Kami.

2.  Segala informasi (berbentuk teks, foto, video, audio, gambar, logo, ilustrasi lain, ikon, data, software, script, interfaced, dan lainnya) yang terdapat atau terkandung dalam situs TRAC dimiliki oleh Kami dan tidak boleh dipergunakan tanpa sepengetahuan dan persetujuan TRAC, kecuali untuk penggunaan yang secara tegas diijinkan dan diperbolehkan dalam Syarat dan Ketentuan juga Kebijakan Privasi Kami.

3.  Kami berhak menarik, mengubah Layanan, informasi atau tampilan situs TRAC demi memberi pelayanan yang lebih baik. Jika dibutuhkan sewaktu-waktu, Kami dapat membatasi akses Anda ke beberapa bagian dari website atau keseluruhan dari Layanan situs TRAC.

4.  Kami berhak mengolah informasi Anda yang tersimpan dalam database Kami sesuai dengan kebijakan yang tercantum dalam Kebijakan Privasi. Dengan menggunakan Layanan Kami, Anda setuju atas pemrosesan tersebut dan menjamin semua data yang diberikan adalah benar.`
    },
    {
        category: "SYARAT & KETENTUAN",
        question: "Car Rental",
        answer: `Ketentuan Layanan Personal Rental: Daily Car Rental
        
1. Daily Car Rental adalah Layanan penyewaan mobil harian yang terbagi dalam dua pilihan, yaitu sewa mobil sekaligus Layanan pengemudi (chauffeur) dan sewa mobil lepas kunci atau tanpa pengemudi (selfdrive).

2.  Anda dapat melakukan pemesanan Daily Car Rental (chauffeur maupun selfdrive) ke tujuan manapun, untuk perjalanan dalam kota maupun keluar kota.

3.  Syarat menggunakan Layanan Daily rental adalah dengan melengkapi dokumen sebagai berikut:

    -   Layanan chauffeur: KTP atau paspor (khusus WNA) dan kartu kredit yang masih valid.
    -   Layanan selfdrive: KTP atau paspor (khusus WNA), SIM A atau SIM Internasional (khusus WNA), dan kartu kredit yang masih valid.

4.  Anda dapat melakukan reservasi Daily Car Rental (chauffeur maupun selfdrive) melalui berbagai channel yang telah kami sediakan, yaitu pemesanan secara online di situs TRAC, melalui email ke rco.nasional@trac.astra.co.id, via telepon ke Reservation Call Center (021) 877 877 87, atau dengan datang langsung ke cabang dan outlet TRAC.

5.  Untuk reservasi melalui situs TRAC, Anda dapat memilih jenis pembayaran yang tersedia, yaitu dengan kartu kredit atau Virtual Account (VA).

6.  Durasi paket Layanan chauffeur yang dapat Anda pilih adalah 4 jam dan 12 jam. Harga yang tertera untuk Layanan chauffeur sudah termasuk sewa mobil dan pengemudi, asuransi, biaya bahan bakar mobil, tol, dan tiket parkir.

7.  Untuk kelebihan durasi pemakaian Layanan chauffeur, Anda akan dikenai biaya tambahan yang dihitung per jam (add hour) dengan jumlah biaya bergantung pada jenis mobil yang Anda gunakan.

8.  Jika Anda harus menginap atau perjalanan Anda melebihi 16 jam, Anda akan dikenakan Overnight Lodging Cost (OLC) atau biaya menginap. Dengan OLC ini, Anda tidak perlu memberikan fasilitas penginapan untuk pengemudi Kami.

9.  Sementara itu, durasi paket Layanan selfdrive dihitung dengan pemakaian per hari. Harga yang tertera untuk Layanan ini termasuk untuk biaya sewa mobil dan asuransi.

10. Khusus Layanan selfdrive, Anda harus mengembalikan mobil ke cabang TRAC tempat Anda memesan mobil paling lambat dua jam setelah masa sewa berakhir. Jika waktu keterlambatan melebihi batas waktu ini, Anda akan dikenakan biaya sejumlah satu hari masa sewa mobil TRAC.

11. Jika Anda tidak bisa mengantarkan mobil tersebut ke cabang TRAC, Anda dapat mengajukan permintaan penjemputan mobil. Kami akan mengambil mobil tersebut ke tempat yang telah disepakati, dengan tambahan biaya yang jumlahnya berbeda-beda tergantung jarak dari titik penjemputan ke cabang TRAC tempat Anda menyewa mobil. Kami akan menginformasikan jumlah biaya tersebut sebelum Anda memutuskan menggunakan Layanan penjemputan mobil.

12. Metode pembayaran untuk tambahan waktu sewa maupun biaya keterlambatan dapat dilakukan langsung melalui mesin EDC yang dibawa oleh pengemudi (jika menggunakan Layanan chauffeur) atau pembayaran melalui transfer ke nomor rekening TRAC. Kami akan menghubungi Anda lebih lanjut untuk informasi jumlah pembayaran tambahan ini.

13. Anda dapat melakukan reschedule maupun perubahan jenis mobil dengan cara membatalkan reservasi yang sudah diproses dan melakukan pemesanan kembali melalui channel reservasi yang telah Kami sebutkan sebelumnya`
    },
    {
        category: "SYARAT & KETENTUAN",
        question: "Airpott Transfer",
        answer: `Ketentuan Layanan Personal Rental: Airport Transfer
        
1. Airport Transfer adalah Layanan antar dan jemput (pick and drop service) dari dan ke bandara yang mencakup jaringan Layanan TRAC di Indonesia. Layanan ini bersifat one stop, artinya Anda akan dijemput dari dan ke bandara tanpa ada perhentian selama perjalanan.

2.  Syarat menggunakan Layanan ini adalah dengan melengkapi dokumen KTP atau paspor (khusus WNA).

3.  Anda dapat melakukan pemesanan Layanan ini melalui berbagai channel yang telah Kami sediakan, yaitu pemesanan secara online di situs TRAC, melalui email ke rco.nasional@trac.astra.co.id, via telepon ke Reservation Call Center (021) 877 877 87, atau dengan datang langsung ke cabang atau outlet TRAC.

4.  Untuk reservasi melalui situs TRAC, Anda dapat memilih jenis pembayaran yang tersedia, yaitu dengan kartu kredit atau VA.

5.  Anda dapat melakukan reschedule maupun perubahan jenis mobil dengan cara membatalkan reservasi yang sudah diproses dan melakukan pemesanan kembali melalui channel reservasi yang telah Kami sebutkan sebelumnya.`
    },
    {
        category: "SYARAT & KETENTUAN",
        question: "Bus Rental",
        answer: `Ketentuan Layanan Personal Rental: Bus Rental

1. Anda dapat menggunakan Layanan Bus Rental TRAC untuk berbagai macam kebutuhan dan aktivitas. Layanan ini sudah termasuk jasa pengemudi, bahan bakar, dan asuransi perjalanan. Pilihan bus yang dapat digunakan mulai dari small bus (13, 15, atau 16 kursi), medium bus (18, 23, 29, 31, atau 35 kursi), big bus (59, 48, 45, atau 40 kursi), dan luxury bus (11 kursi).

2.  Anda dapat melakukan reservasi Bus Rental melalui berbagai channel yang telah Kami sediakan, yaitu pemesanan secara online di situs TRAC, melalui email ke rco.nasional@trac.astra.co.id, via telepon ke Reservation Call Center (021) 877 877 87, atau dengan datang langsung ke cabang dan outlet TRAC.

3.  Minimal durasi pemakaian Bus Rental adalah 12 jam. Jika pemakaian melebihi paket yang telah dibayar, Anda akan dikenakan biaya tambahan dengan hitungan per jam. Biaya tambahan akan mulai dihitung setelah pukul 22.00.

4.  Anda dapat melakukan reschedule maupun perubahan jenis bus dengan cara membatalkan reservasi yang sudah diproses dan melakukan pemesanan kembali melalui channel reservasi yang telah Kami sebutkan sebelumnya.`
    },
    {
        category: "SYARAT & KETENTUAN",
        question: "Pembayaran",
        answer: `Ketentuan Pembayaran, Pembatalan, dan Refund

1. Anda berhak memilih metode pembayaran yang telah Kami sediakan, yaitu menggunakan kartu kredit atau melalui VA atas nama PT Serasi Autoraya.

2.  Anda wajib melakukan pembayaran terlebih dulu sesuai jumlah transaksi sebelum tanggal jatuh tempo. Setelah pembayaran Anda terkonfirmasi, Kami akan segera memproses reservasi Anda.

3.  Jika Anda melakukan pembatalan dalam dalam rentang waktu 24-48 jam sebelum waktu pemakaian Layanan, Anda tidak akan dikenakan biaya pembatalan. Kami akan mengembalikan uang (refund) melalui transfer bank ke rekening Anda, dalam waktu kurang lebih 14 hari kerja setelah waktu pembatalan.

4.  Namun, jika pembatalan reservasi dilakukan dalam waktu kurang dari 24 jam, Anda akan dikenai biaya pembatalan sebesar 100% dari nilai transaksi.

5.  Proses refund dapat dilakukan apabila Anda membatalkan reservasi dalam waktu yang sesuai dengan ketentuan Kami. Jika Anda membatalkan reservasi dalam kurun waktu lebih dari 48 jam sebelum waktu sewa dimulai, Anda akan menerima refund sebesar 100%. Jika Anda membatalkan dalam waktu 24-48 jam sebelum waktu sewa dimulai, Anda akan menerima refund sebesar 50%.

6.  Proses refund dimulai dari pemberian informasi nomor rekening Anda saat melakukan pembatalan. Lalu, TRAC akan mengirimkan kembali uang ke rekening Anda dalam kurun waktu lebih kurang 14 hari kerja setelah pembatalan sewa`
    }
]

export default tnc;