/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 13:42:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 11:20:44
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import Icon from '@airport-transfer-fragments/Icon';
import styles from "./style";
// import { BoxShadow } from "react-native-shadow";
// import faqList from '../../../../../utils/DummyData/faqList';
import TncList from './data';
import Lang from '../../../../../assets/lang';
import { connect } from 'react-redux';

class TNC extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      activeDropIndex: 0,
      activeButtonIndex: 0,
      activeCategory:"ALL",
      // faqList: faqList,
    };
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  toggleDrop(index) {
    this.setState({
      activeDropIndex: index,
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderFindATopic() {
    return (
      <View style={styles.searchBar.blueBox.wrapper}>
        <View style={styles.searchBar.blueBox.wrapper.textWrapper}>
          <Text
            size={18}
            color={appVariables.colorWhite}>
            What can we help you with?
          </Text>
        </View>
        <View style={styles.searchBar.blueBox.wrapper.findTopicBoxWrapper}>
          {this._renderFindTopicBox()}
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderFindTopicBox() {
    const shadowTopicBox = {
      width: 300,
      height: 70,
      color: "#000",
      border: 10,
      radius: 10,
      opacity: 0.2,
      style: {
        flex: 4,
        flexDirection: "row",
      }
    }

    return (
      <View style={styles.findTopicBox.wrapper}>
        {/* <BoxShadow setting={shadowTopicBox}> */}
          <View style={styles.findTopicBox.wrapper.findTopicTextArea}>
            <TextInput
              style={{ paddingVertical: 20, paddingHorizontal: 20, color: appVariables.colorGray, fontSize: 16 }}
              placeholder="Type here to find a topic" />
          </View>
        {/* </BoxShadow> */}
        <TouchableOpacity style={styles.findTopicBox.wrapper.searchIconWrapper}>
          <Icon
            size={20}
            color={appVariables.colorWhite}
            name={"search"}
            style={{
              paddingVertical: 25,
              paddingHorizontal: 20,
            }}>
          </Icon>
        </TouchableOpacity>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderListTopicButton() {
    let categories = [
      "ALL",
    ];

    faqList.map((faq)=>{
      if (categories.indexOf(faq.category) === -1) {
        categories.push(faq.category);
      }
    });

    return (
      <ScrollView
        horizontal
        style={styles.listTopicButton.wrapper}> 
        {
          categories.map((category, index) => {
            return(
              <View style={styles.listTopicButton.buttonSeparator}>
                {this._renderButton(category,index)}
              </View>
            );
           })
        } 
      </ScrollView>
    );
  }
  // ---------------------------------------------------
  _renderButton(label, index) {
    return (
      <TouchableOpacity
        style={[styles.inactiveButtonStyle,
        this.state.activeButtonIndex == index ? { backgroundColor: "#F27C21" } : {}]}
        onPress={() => this.setState({ activeButtonIndex: index,activeCategory:label,activeDropIndex:0} )}>
        <Text
          size={14}
          color={appVariables.colorWhite}
          style={{ textAlign: "center" }}>
          {label}
        </Text>
      </TouchableOpacity>
    );
  }
  // ---------------------------------------------------
  _renderExpandableMenuTitle(label) {
    return (
      <View style={styles.expandableViewTitle}>
        <Text size={20}>
          {label}
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderDropdown(label, content, index) {
    const isActive = this.state.activeDropIndex == index;

    let contentRender = null;
    if (isActive) {
      contentRender = this._renderContent(content);
    }

    return (
      <View style={styles.dropdown.container} key={index}>
        <TouchableOpacity style={styles.dropdown.label.container}
          onPress={() => this.toggleDrop(index)}>
          <View style={styles.dropdown.label.textContainer}>
            <Text size={16}>
              {label}
            </Text>
          </View>

          <View style={styles.dropdown.label.iconContainer}>
            <Icon size={20} name={isActive ? "caret-up" : "caret-down"} />
          </View>
        </TouchableOpacity>

        {contentRender}

      </View>
    )
  }

  // ---------------------------------------------------

  _renderContent(content) {
    return (
      <View style={styles.dropdown.content.container}>
        <Text color={appVariables.colorGray}>
          {content}
        </Text>
      </View>
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={Lang.termsAndConditions[this.props.lang]}
          onBackPress={() => this.props.navigation.pop()} />
        <ScrollView>
          {/* {this._renderFindATopic()}
          {this._renderListTopicButton()} 
          {this._renderExpandableMenuTitle(this.state.activeCategory)}*/}
          {
            TncList
              .filter((faq) => {
                return this.state.activeCategory=="ALL" || (this.state.activeCategory==faq.category);
              })
              .map((faq, index) => {
                return this._renderDropdown(faq.question, faq.answer, index);
              })
          }
        </ScrollView>
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang };
}

export default connect(mapStateToProps, {})(TNC);
