/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-12 15:51:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-06 14:25:09
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  FlatList
} from "react-native";
import { connect } from "react-redux";
import {
  appVariables,
} from "@variables";
import moment from "moment";
import "moment/locale/id";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import { getMyBooking, getVehicleAttribute } from "../../../../../actions";
import { greetings } from "../../../../../helpers";
import { RENTAL_TIMEBASE, RENTAL_KMBASE, RENTAL_HOURLY, UAS_TIMEBASE, UAS_KMBASE, STATUS_RESERVATION, BUS_RENTAL, CAR_RENTAL } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class MyBooking extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      currentPage: null,
      totalPage: null,
      totalData: null,
      data: [],
      vehicle: []
    };
  }

  componentDidMount = () => {
    this.props.getVehicleAttribute({ token: this.props.login.token });
    this.props.getMyBooking({
      PIC: this.props.login.Data.Id,
      token: this.props.login.token
    });
  }
  
  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.myBooking !== prevProps.myBooking) {
      return { myBooking: this.props.myBooking }
    }
    if(this.props.vehicle !== prevProps.vehicle) {
      return { vehicle: this.props.vehicle }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.myBooking !== undefined) {
        this.setState({
          data: snapshot.myBooking,
          currentPage: this.props.currentPage,
          totalPage: this.props.totalPage,
          totalData: this.props.totalData,
        })
      }
      if(snapshot.vehicle !== undefined) {
        this.setState({
          vehicle: snapshot.vehicle
        })
      }
    }
  }
  
  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 

  goToCarDetail(item) {
    Actions.CarDetail({
      currentPage: "My Booking",
      item: item
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderTopBarText() {
    const { login: { Data } } = this.props;
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        backgroundColor: '#21419A',
        paddingVertical: 30,
        paddingHorizontal: 20,
        borderColor: '#21419A',
        borderWidth: 1,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
          alignItems:'center',
        }}>

        <Text color={appVariables.colorWhite} size={16}>
            {Lang.hai[this.props.lang]} {greetings(this.props.lang)} {Data.FirstName !== null ? Data.FirstName : "Our Customer"} {Data.LastName !== null ? Data.LastName : ""}
        </Text>
        <Text color={appVariables.colorWhite} size={14} style={{textAlign: 'center'}}>
            {Lang.myBookingQuote[this.props.lang]}
        </Text>

        </View>
      </View>
    )
  }

  // ---------------------------------------------------

  _renderCarRentalTitle(item) {
    let productName = null;
    let status = _.filter(STATUS_RESERVATION, {id: item.Status});
    let x = _.filter(this.props.products, { MsProductId: item.details[0].MsProductId });
    let y = _.filter(x[0].product_service, { ProductServiceId: item.details[0].MsProductServiceId });
    let serviceName = y[0] !== undefined ? y[0].ProductServiceName : null;
    switch (item.ServiceTypeId) {
      case RENTAL_TIMEBASE:
        productName = "Car Rental";
        break;
      case RENTAL_KMBASE:
        productName = "Airport Transfer";
        break;
      case RENTAL_HOURLY:
        productName = "Airport Transfer";
        break;
      case UAS_TIMEBASE:
        productName = "Bus Rental";
        break;
      case UAS_KMBASE:
        productName = "Bus Rental";
        break;
      default:
        break;
    }
    return (
      <View style={styles.carTitle.container}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-start'
        }}>
          <Text size={20}>{productName}</Text>
          {this.props.selectedProduct !== BUS_RENTAL && <Text size={10} style={{ color: appVariables.colorGray }}>{serviceName}</Text> }
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-end'}}>
          <Text
            size={14}
            color={appVariables.colorOrange}>
            {_.replace(_.startCase(_.toLower(status[0].name)), /_/g, " ")}
          </Text>
        </View>
      </View>
    );
  }



  // ---------------------------------------------------

  _renderCarSwiper(item) {
    let f = _.filter(this.state.vehicle, { vehicleTypeId: item.details[0].UnitTypeId });
    let image = this.state.vehicle.length > 0 ? { uri: f[0].vehicleImage } : require('../../../../../assets/images/png/cars/Avanza.png')
    return (
      <Image
        source={image}
        style={{ height: 80, width: 150, resizeMode: "contain"}} />
    );
  }


  // ---------------------------------------------------

  _renderCarTotal(item) {
    return (
      <View style={{ flexDirection: 'column', marginBottom: 10 }}>
        <Text
          size={11}
          color={appVariables.colorGray}>
          {Lang.carTotal[this.props.lang]}
        </Text>

        <Text
          size={14}
          color={appVariables.colorBlack}
          style={{ paddingTop: 0 }}>
          {item.details.length} Cars
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderBookingNumber(item) {
    return (
      <View style={{ flexDirection: 'column' }}>
        <Text
          size={11}
          color={appVariables.colorGray}>
          {Lang.reservationNumber[this.props.lang]}
        </Text>

        <Text
          size={14}
          color={appVariables.colorBlack}
          style={{ paddingTop: 0 }}>
          {item.ReservationId}
        </Text>
      </View>
    );
  }


  // ---------------------------------------------------

  _renderDates() {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        paddingTop: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <Text
            size={11}
            color={appVariables.colorGray}>
            {Lang.dates[this.props.lang]}
          </Text>
        </View>
      </View>
    );
  }


  // ---------------------------------------------------

  _renderDetails(item) {
    return (
      <View style={{
        flex: -1,
        flexDirection: 'row',
        paddingVertical: 10,
      }}>
        <View style={{
          flex: 2,
          flexDirection: 'row',
        }}>
          <Text
            size={14}
            color={appVariables.colorBlack}>
            {(new moment(item.details[0].StartDate, "YYYY-MM-DD HH:mm:ss")).format("LL")} {item.details[0].EndDate !== null && `- ${(new moment(item.details[0].EndDate, "YYYY-MM-DD HH:mm:ss")).format("LL")}`}
            </Text>
        </View>

        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end'
          }}
          onPress={() => this.goToCarDetail(item)}>
          <View style={{
            // flex: 2,
            flexDirection: 'row',
            alignItems: 'center'
            }}>
            <Text
              size={12}
              color={appVariables.colorBlack}>
              {Lang.detail[this.props.lang]}
            </Text>
            <Icon
              name="arrow-right"
              size={12}
              color={appVariables.colorOrange}
              style={{ marginLeft: 10 }} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderCarRentals(item, index) {
    return (
        <View style={styles.mainContainer} key={index}>
          {this._renderCarRentalTitle(item)}
          <View style={{
            flex: -1,
            flexDirection: 'row',
            justifyContent: 'space-around',
            paddingVertical: 10,
          }}>
            {this._renderCarSwiper(item)}
            <View>
              {this._renderCarTotal(item)}
              {this._renderBookingNumber(item)}
            </View>
          </View>
          {this._renderDates()}
          {this._renderDetails(item)}
        </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer hasFooter>
        <Header title={Lang.myBooking[this.props.lang]} />
        { this.props.isLogin ? 
        <FlatList 
          keyExtractor={(item, index) => index.toString()}
          data={this.state.data}
          renderItem={({item, index}) => this._renderCarRentals(item, index) }
          ListHeaderComponent={() => this._renderTopBarText()}
          ListEmptyComponent={() => this.state.currentPage === null && this.props.loading ? <ActivityIndicator size="small" color="#2A2E36" style={{ marginTop: 20 }} /> : null }
          />
        : <View /> }
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ authentication, booking, vehicleAttribute, productService, language }) => {
  const { isLogin, login } = authentication;
  const { myBooking, loading, totalData, currentPage, totalPage } = booking;
  const { vehicle } = vehicleAttribute;
  const { products, selectedProduct } = productService;
  const { lang } = language;
  return { isLogin, login, myBooking, loading, totalData, currentPage, totalPage, vehicle, products, selectedProduct, lang };
}

export default connect(mapStateToProps, {
  getMyBooking,
  getVehicleAttribute
})(MyBooking);
