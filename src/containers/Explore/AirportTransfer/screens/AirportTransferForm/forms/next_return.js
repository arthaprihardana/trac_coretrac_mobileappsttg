/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-07 23:25:02 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-05-07 23:25:02 
 */
import moment from "moment";
import "moment/locale/id";

export default [
  {
    type: "from_destination", 
    label: "From (Area, Address, Building)", 
    modalTitle: "From (Area, Address, Building)", 
    placeholder: "Select Area, Address, Building", 
    backgroundColor: "#1A3681",
    hasSearchIcon: true,
    isHidden: false,
    isRequired: true,
    isDisabled: false,
    modifier: null,
    optionsModifier: null,
    onSuccessChain: null,
    onCancelChain: null,
  },
  {
    type: "to_airport", 
    label: "To (Airport)", 
    modalTitle: "To (Airport)", 
    placeholder: "Select Airport", 
    backgroundColor: "#1F419B",
    hasSearchIcon: true,
    isHidden: false,
    isRequired: true,
    isDisabled: false,
    modifier: null,
    optionsModifier: null,
    onSuccessChain: null,
    onCancelChain: null,
  },
  {
    type: "flight", 
    label: "Flight Number", 
    modalTitle: "Flight Details", 
    placeholder: "Select Flight Number", 
    backgroundColor: "#1B3373",
    hasSearchIcon: false,
    isHidden: false,
    isRequired: true,
    isDisabled: false,
    modifier: (value, formsState) => (!value.name || !value.number) ? null : (value.name + " " + value.number),
    optionsModifier: null,
    onSuccessChain: null,
    onCancelChain: null,
  },
  {
    type: "arrival_schedule", 
    label: "Date and Time", 
    modalTitle: "Date and Time", 
    placeholder: "Select Date & Time", 
    backgroundColor: "#12265B",
    hasSearchIcon: false,
    isRequired: true,
    isHidden: false,
    isDisabled: false,
    modifier: (value, formsState) => (!value.date || !value.time) ? null : (new moment(value.date + " " + value.time, "DD-MM-YYYY hh:mm")).format("ddd, DD MMM YYYY [at] hh:mm A"),
    optionsModifier: null,
    onSuccessChain: null,
    onCancelChain: null,
  },

];