/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-07 20:51:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 23:24:38
 */
import moment from "moment";
import "moment/locale/id";
import Lang from '../../../../../../assets/lang';

export default (lang) => {
  return [
    {
      type: "type", 
      label: Lang.typeOfService[lang],
      modalTitle: Lang.typeOfService[lang],
      placeholder: Lang.selectType[lang], 
      backgroundColor: "#2246A8",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value ? null : formsState.type.options.filter((option) => option.value == value)[0].label,
      optionsModifier: null,
      onSuccessChain: null, 
      onCancelChain: null,
    },
    {
      type: "from_airport", 
      label: Lang.fromAirport[lang], 
      modalTitle: Lang.fromAirport[lang], 
      placeholder: Lang.selectAirport[lang], 
      backgroundColor: "#1F419B",
      hasSearchIcon: true,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      // modifier: null,
      modifier: (value, formsState) => !value || !formsState.from_airport.value ? null : formsState.from_airport.options.filter((option) => option.value == formsState.from_airport.value)[0].label,
      optionsModifier: null,
      onSuccessChain: null,
      onCancelChain: null,
    },
    {
      type: "to_city", 
      label: Lang.toAreaAdress[lang],
      modalTitle: Lang.toAreaAdress[lang],
      placeholder: Lang.selectArea[lang], 
      backgroundColor: "#1A3681",
      hasSearchIcon: true,
      isHidden: false,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => !value || !formsState.to_location.value ? null : formsState.to_location.options.filter((option) => option.value == formsState.to_location.value)[0].label,
      optionsModifier: null,
      onSuccessChain: "to_location", 
      onCancelChain: null, 
    },
    {
      type: "to_location", 
      label: null, 
      modalTitle: Lang.toAreaAdress[lang], 
      placeholder: null, 
      backgroundColor: null,
      hasSearchIcon: false,
      isHidden: true,
      isRequired: true,
      isDisabled: false,
      modifier: (value, formsState) => {
        console.log('value ==>', value);
        console.log('formsState ==>', formsState);
      },
      optionsModifier: null,
      // optionsModifier: (options, formsState) => options.filter((option) => option.parent == formsState.from_city.value),
      onSuccessChain: null, 
      onCancelChain: "to_city", 
    },
    // {
    //   type: "to_destination", 
    //   label: "To (Area, Address, Building)", 
    //   modalTitle: "To (Area, Address, Building)", 
    //   placeholder: "Select Area, Address, Building", 
    //   backgroundColor: "#1A3681",
    //   hasSearchIcon: true,
    //   isHidden: false,
    //   isRequired: true,
    //   isDisabled: false,
    //   modifier: null,
    //   optionsModifier: null,
    //   onSuccessChain: null,
    //   onCancelChain: null,
    // },
    {
      type: "flight", 
      label: Lang.enterFlightDetails[lang], 
      modalTitle: Lang.flightDetails[lang], 
      placeholder: Lang.enterFlightDetails[lang], 
      backgroundColor: "#1B3373",
      hasSearchIcon: false,
      isHidden: false,
      isRequired: false,
      isDisabled: false,
      modifier: (value, formsState) => (!value.name || !value.number) ? null : (value.name + " " + value.number),
      optionsModifier: null,
      onSuccessChain: null,
      onCancelChain: null,
    },
    {
      type: "arrival_schedule", 
      label: Lang.dateAndTime[lang], 
      modalTitle: Lang.dateAndTime[lang], 
      placeholder: Lang.dateAndTime[lang], 
      backgroundColor: "#12265B",
      hasSearchIcon: false,
      isRequired: true,
      isHidden: false,
      isDisabled: false,
      modifier: (value, formsState) => (!value.date || !value.time) ? null : (new moment(value.date + " " + value.time, "DD-MM-YYYY hh:mm")).format("LLL"),
      optionsModifier: null,
      onSuccessChain: null,
      onCancelChain: null,
    },
  ];
}