/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-19 10:02:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 20:52:10
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import styles from "./style";
import { connect } from "react-redux";
import _ from "lodash";
import moment from "moment";
import "moment/locale/id";

import mainForm from "./forms/main";
// import nextReturnForm from "./forms/next_return";
// import nextMultiForm from "./forms/next_multi";

import areaDummies from "@airport-transfer-dummies/areas";
import airportDummies from "@airport-transfer-dummies/airports";
import flightDummies from "@airport-transfer-dummies/flights";

import MainFormContainer from "@airport-transfer-fragments/MainFormContainer";

import { getMasterAirport, getMasterCarType, getExtras, setTempForm, setRequestStockList, getAirportZone } from "../../../../../actions";
import { RENTAL_KMBASE } from "../../../../../constant";
import Lang from '../../../../../assets/lang'

const INITIAL_FORMS_STATE = {
  type: {
    value: null,
    modalType: "radio",
    isModalOpened: false,
    options: [],
  },
  from_airport: {
    value: null,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  // to_airport: {
  //   value: null,
  //   modalType: "list",
  //   isModalOpened: false,
  //   options: [],
  // },
  to_city: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  to_location: {
    value: null,
    isError: false,
    modalType: "list",
    isModalOpened: false,
    options: [],
  },
  // to_destination: {
  //   value: null,
  //   modalType: "list",
  //   isModalOpened: false,
  //   options: [],
  // },
  // from_destination: {
  //   value: null,
  //   modalType: "list",
  //   isModalOpened: false,
  //   options: [],
  // },
  flight: {
    value: {
      name: null,
      number: null,
    },
    modalType: "flight",
    isModalOpened: false,
    options: [],
  },
  arrival_schedule: {
    value: {
      date: null,
      time: null,
    },
    modalType: "singleSchedule",
    isModalOpened: false,
    options: [],
  },
};


class AirportTransfer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
    let serviceType = _.map(product[0].product_service, v => {
      return {
        label: v.ProductServiceName,
        value: v.ProductServiceId
      }
    });
    let serviceAvailableForNow = _.filter(serviceType, { value: "PSV0003" });

    this.state = {
      BusinessUnitId: product[0].BusinessUnitId,
      RentalDuration: 0,
      // forms: JSON.parse(JSON.stringify(INITIAL_FORMS_STATE)),
      forms: {
        type: {
          value: null,
          modalType: "radio",
          isModalOpened: false,
          options: serviceAvailableForNow,
        },
        from_airport: {
          value: null,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        // to_airport: {
        //   value: null,
        //   modalType: "list",
        //   isModalOpened: false,
        //   options: [],
        // },
        // to_destination: {
        //   value: null,
        //   modalType: "list",
        //   isModalOpened: false,
        //   options: [],
        // },
        // from_destination: {
        //   value: null,
        //   modalType: "list",
        //   isModalOpened: false,
        //   options: [],
        // },
        to_city: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        to_location: {
          value: null,
          isError: false,
          modalType: "list",
          isModalOpened: false,
          options: [],
        },
        flight: {
          value: {
            name: null,
            number: null,
          },
          modalType: "flight",
          isModalOpened: false,
          options: [],
        },
        arrival_schedule: {
          value: {
            date: null,
            time: null,
          },
          modalType: "singleSchedule",
          isModalOpened: false,
          options: [],
        },
      },
      missingForms: [],

      formStacks: [],
      activeStackIndex: 0,
      stackType: null,

      isAddTripButtonVisible: false,

      isInitiated: false,
    };
  }

// ---------------------------------------------------

  componentDidMount() {
    this.getOptions();
    if(this.props.airport !== null) {
      const forms = this.state.forms;
      const fromAirport = forms.from_airport;
      fromAirport.options = this.props.airport;
    } else {
      this.props.getMasterAirport();
    }
  }

// ---------------------------------------------------

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.airport !== prevProps.airport) {
      return { airport: this.props.airport }
    }
    if(this.props.airportCityCoverage !== prevProps.airportCityCoverage) {
      return { airportCityCoverage: this.props.airportCityCoverage }
    }
    if(this.props.address !== prevProps.address) {
      return { address: this.props.address }
    }
    if(this.props.distanceMatrix !== prevProps.distanceMatrix) {
      return { distanceMatrix: this.props.distanceMatrix }
    }
    if(this.props.zone !== prevProps.zone) {
      return { zone: this.props.zone }
    }
    if(this.props.geometry !== prevProps.geometry) {
      return { coordsAirport: this.props.geometry }
    }
    if(this.props.placeDetail !== prevProps.placeDetail) {
      return { coordsDestination: this.props.placeDetail }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.airport) {
        this.manageMasterAirport(snapshot.airport);
      }
      if(snapshot.airportCityCoverage) {
        this.manageAirportCoverage(snapshot.airportCityCoverage);
      }
      if(snapshot.address !== undefined) {
        this.manageAddressFromGM(snapshot.address);
      }
      if(snapshot.distanceMatrix !== undefined) {
        this.props.getAirportZone({
          BusinessUnitId: this.state.BusinessUnitId,
          MsProductId: this.props.selectedProduct,
          Distance: snapshot.distanceMatrix
        });
      }
      if(snapshot.zone !== undefined) {
        this.setState({ RentalDuration: snapshot.zone.KM });
      }
      if(snapshot.coordsAirport !== undefined) {
        const forms = this.state.forms;
        const fromAirport = forms.from_airport;
        fromAirport.coords = snapshot.coordsAirport;
        this.setState({
          forms
        })
      }
      if(snapshot.coordsDestination !== undefined) {
        const forms = this.state.forms;
        const toLocation = forms.to_location;
        toLocation.coords = snapshot.coordsDestination;
        this.setState({
          forms
        })
      }
    }
    if (!this.state.isInitiated) {
      this.getOptions();
    }
  }

  // componentDidUpdate() {
  //   if (!this.state.isInitiated) {
  //     this.getOptions();
  //   }
  // }


// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onFormsStackPress(index) {
    let forms = this.state.forms;
    let formStacks = this.state.formStacks;
    let isAddTripButtonVisible = this.state.isAddTripButtonVisible;
    let activeStackIndex = this.state.activeStackIndex;
    let stackType = this.state.stackType;

    formStacks[activeStackIndex].forms = forms;

    isAddTripButtonVisible = (stackType == "return" && index === 0) || stackType == "multi" ;
    activeStackIndex = index;
    forms = formStacks[index].forms;

    this.setState({
      forms,
      missingForms: [],
      formStacks,
      isAddTripButtonVisible,
      activeStackIndex,
    });
  }

// ---------------------------------------------------

  onFormsRemovePress(index) {
    let forms = this.state.forms;
    let formStacks = this.state.formStacks;
    let isAddTripButtonVisible = this.state.isAddTripButtonVisible;
    let activeStackIndex = this.state.activeStackIndex;
    let stackType = this.state.stackType;

    formStacks[index].isVoid = true;

    if (index === activeStackIndex) {
      let lastAvailableStackIndex = 0;
      for (i = 1; i < index; i ++) {
        if (!formStacks[i].isVoid) {
          lastAvailableStackIndex = i;
        }
      } 

      activeStackIndex = lastAvailableStackIndex;
      forms = formStacks[lastAvailableStackIndex].forms;
    }

    this.setState({
      forms,
      missingForms: [],
      formStacks,
      isAddTripButtonVisible,
      activeStackIndex,
    });
  }

// ---------------------------------------------------

  onFormsStateChange(forms) {
    let formStacks = this.state.formStacks;
    let isAddTripButtonVisible = this.state.isAddTripButtonVisible;
    let stackType = this.state.stackType;

    if (forms.type.value == "1" && stackType != "return") {
      formStacks = [];
      formStacks.push({
        title: "Return - Trip",
        isVoid: false,
        forms: {},
      });

      isAddTripButtonVisible = true;
      stackType = "return";
    } else if (forms.type.value == "2" && stackType != "multi") {
      formStacks = [];
      formStacks.push({
        title: "Multi City - Trip",
        isVoid: false,
        forms: {},
      });

      isAddTripButtonVisible = true;
      stackType = "multi";
    } else if (forms.type.value == "0" && stackType != null) {
      formStacks = [];

      isAddTripButtonVisible = false;
      stackType = null;
    }

    this.setState({
      forms,
      missingForms: [],
      formStacks,
      stackType,
      isAddTripButtonVisible,
    });
  }

// ---------------------------------------------------

  onAddTripButtonPressed(isMulti = false) {
    let forms = this.state.forms;

    const missingForms = this.getInvalidForms(this.getForm(), this.state.forms);
    if (missingForms.length > 0) {
      this.setState({
        missingForms
      });

      return;
    }

    let formStacks = this.state.formStacks;
    let isAddTripButtonVisible = this.state.isAddTripButtonVisible;
    let isInitiated = this.state.isInitiated;
    let activeStackIndex = this.state.activeStackIndex;

    if (formStacks.length > 0 && ((!isMulti && formStacks.length < 2) || isMulti)) {
      formStacks[formStacks.length - 1].forms = forms;
      formStacks.push({
        title: (!isMulti ? "Return" : "Multi City") + " - Trip",
        forms: {},
      });

      if (!isMulti) {
        isAddTripButtonVisible = false;
      }

      forms = JSON.parse(JSON.stringify(INITIAL_FORMS_STATE));
      forms.type.value = !isMulti ? "1" : "2";

      isInitiated = false;
      activeStackIndex = formStacks.length - 1;
    } else {
      this.onFormsStackPress(1);

      return;
    }

    this.setState({
      forms,
      formStacks,
      isAddTripButtonVisible,
      isInitiated,
      activeStackIndex,
    });
  }

  // ---------------------------------------------------

  onChainingModalTriggered(key, chainKey, isForwardChain = false) {
    const forms = this.state.forms;
    
    forms[chainKey].isModalOpened = true;

    if (isForwardChain) {
      forms[chainKey].value = INITIAL_FORMS_STATE[chainKey].value;
    }

    this.setState({
      forms
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN METHODS
// ---------------------------------------------------

  manageMasterAirport = airport => {
    const forms = this.state.forms;
    const fromAirport = forms.from_airport;
    fromAirport.options = airport;
    this.setState({ forms })
  }

  manageAirportCoverage = city => {
    const forms = this.state.forms;
    let airportCoverage = _.map(city, v => {
      return {
        CityId: v.CityId,
        BranchId: v.city.BranchId,
        label: _.startCase(_.lowerCase(v.CityName)),
        value: _.startCase(_.lowerCase(v.CityName))
      }
    });
    const toCity = forms.to_city;
    toCity.options = airportCoverage;
    this.setState({ forms })
  }

  manageAddressFromGM = address => {
    const forms = this.state.forms;
    const toLocation = forms.to_location;
    toLocation.options = address;
    this.setState({ forms })
  }

  getOptions() {
    const forms = this.state.forms;

    // const toDestinationState = forms.to_destination;
    // toDestinationState.options = areaDummies;

    // const fromDestinationState = forms.from_destination;
    // fromDestinationState.options = areaDummies;

    // const fromAirport = forms.from_airport;
    // fromAirport.options = airportDummies;

    // const toAirport = forms.to_airport;
    // toAirport.options = airportDummies;

    const flight = forms.flight;
    flight.options = flightDummies;
    
    this.setState({
      forms,
      isInitiated: true,
    });
  }

// ---------------------------------------------------

  getForm() {
    formConfig = mainForm(this.props.lang);

    // if (this.state.stackType == "return" && this.state.activeStackIndex > 0) {
    //   formConfig = nextReturnForm;
    // } else if (this.state.stackType == "multi" && this.state.activeStackIndex > 0) {
    //   formConfig = nextMultiForm;
    // }

    return formConfig;
  }

// ---------------------------------------------------

  getStacks() {
    let stacks = JSON.parse(JSON.stringify(this.state.formStacks));

    if (this.state.forms && this.state.forms.type.value == "1") {
      if (stacks[0].forms.from_airport && stacks[0].forms.from_airport.value) {
        stacks[0].title += " [" + stacks[0].forms.from_airport.value + "]";
      }

      if (stacks[1] && stacks[1].forms.to_airport && stacks[1].forms.to_airport.value) {
        stacks[1].title += " [" + stacks[1].forms.to_airport.value + "]";
      }
    } else if (this.state.forms && this.state.forms.type.value == "2") {
      for (x in  stacks) {
        if (stacks[x].forms.from_airport && stacks[x].forms.from_airport.value) {
          stacks[x].title += " [" + stacks[x].forms.from_airport.value + "]";
        }
      }
    }

    return stacks;
  }

// ---------------------------------------------------

  getInvalidForms(formTemplate, formData) {
    const missingForms = [];
    formTemplate.map((form) => {
      if (form.isRequired) {
        if (!formData[form.type].value) {
          missingForms.push(form.type);
        } else if (typeof formData[form.type].value == "object") {
          for (x in formData[form.type].value) {
            if (!formData[form.type].value[x]) {
              missingForms.push(form.type);

              break;
            }
          }
        }
      }
    });

    return missingForms;
  }

// ---------------------------------------------------

  submit() {
    let forms = [];
    let stacks = this.state.formStacks;
    let data = {};
    let missingForms = [];
    let navTripsData = [];

    switch (this.state.forms.type.value) {
      case "PSV0003":
        data.type = "One Way";
        data.trips = {};
        data.trips.trip1 = {};
        forms = this.getForm();
        forms.map((form, index) => {
          if (form.type != "type") {
            data.trips.trip1[form.type] = this.state.forms[form.type].value;
          }
        });

        missingForms = this.getInvalidForms(forms, this.state.forms);
        if (missingForms.length > 0) {
          break;
        }

        navTripsData = [
          {
            from: this.state.forms.from_airport.value,
            fromLabel: _.filter(this.state.forms.from_airport.options, { value: this.state.forms.from_airport.value })[0].label,
            to: this.state.forms.to_location.value,
            toLabel: _.filter(this.state.forms.to_location.options, { value: this.state.forms.to_location.value })[0].label,
            start: new moment(this.state.forms.arrival_schedule.value.date+" "+this.state.forms.arrival_schedule.value.time, "YYYY-MM-DD HH:mm:ss")
          },
        ];
        
        break;
      default:
        missingForms = this.getInvalidForms(this.getForm(), this.state.forms);
    }

    if (missingForms.length > 0) {
      this.setState({
        missingForms,
      });

      return;
    }

    // console.log(navTripsData.filter((item) => !!item));
    // console.log('submitted data ==>', data);
    // console.log('view data ==>', {
    //   type: data.type,
    //   title: "Airport Transfer - " + data.type,
    //   tripIndex: 0,
    //   addedList: [],
    //   trips: navTripsData.filter((item) => !!item),
    // });

    let branch = _.filter(this.state.forms.to_city.options, { value: this.state.forms.to_city.value });
    this.props.setTempForm(this.state.forms);
    this.props.getMasterCarType(this.state.BusinessUnitId);
    this.props.getExtras({
      BusinessUnitId: this.state.BusinessUnitId, 
      BranchId: branch[0].BranchId
    });
    let startDate = (new moment(this.state.forms.arrival_schedule.value.date+" "+this.state.forms.arrival_schedule.value.time, "DD-MM-YYYY hh:mm")).toISOString();
    let endDate = (new moment(this.state.forms.arrival_schedule.value.date+" "+this.state.forms.arrival_schedule.value.time, "DD-MM-YYYY hh:mm")).toISOString();
    let requestStock = {
      BusinessUnitId: this.state.BusinessUnitId,
      BranchId: branch[0].BranchId,
      StartDate: startDate,
      EndDate: endDate,
      IsWithDriver: 1, // 0 = self driver 1 = pakai driver
      RentalDuration: this.state.RentalDuration,
      RentalPackage: this.state.RentalDuration,
      Uom: 'km',
      ServiceTypeId: RENTAL_KMBASE,  // Rental Time Based
      ValidateAttribute: 1,
      ValidateContract: 1,
      ProductServiceId: this.state.forms.type.value,      // buat exception pada saat request
      // CityId: branch[0].CityId,                           // buat exception pada saat request
      CityId: null,
      MsAirportCode: this.state.forms.from_airport.value  // buat exception pada saat request
    }
    this.props.setRequestStockList(requestStock);
    Actions.push("CarList", {
      navData: {
        submittedData: data,
        viewData: {
          type: data.type,
          title: `${Lang.airportTransfer[this.props.lang]} - ${data.type}`,
          tripIndex: 0,
          addedList: [],
          trips: navTripsData.filter((item) => !!item),
        },
      },
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <MainFormContainer
        title={ Lang.airportTransfer[this.props.lang] }
        onBackPressed={ () => Actions.pop() }
        forms={ this.getForm() }
        missingForms={ this.state.missingForms }
        formsState={ this.state.forms }
        onSubmitPressed={ this.state.stackType == "return" && this.state.formStacks.length === 1 ? null : () => this.submit() }
        onFormsStateChange={ forms => this.onFormsStateChange(forms) }
        formStack={{
          stacks: this.getStacks(),
          activeIndex: this.state.activeStackIndex,
          onStackPress: (index) => this.onFormsStackPress(index),
          onRemovePress: this.state.stackType == "multi" ? (index) => this.onFormsRemovePress(index) : null,
          nextButton: {
            title: this.state.stackType === "return" ? "Next Trip" : "Add Trip",
            isVisible: this.state.formStacks.filter(item => !item.isVoid).length < 4 ? this.state.isAddTripButtonVisible : false,
            onPress: () => this.onAddTripButtonPressed(this.state.stackType == "multi"),
          },
        }}
        onChainingModalTriggered={ (key, chainKey, isForwardChain) => this.onChainingModalTriggered(key, chainKey, isForwardChain) }
      />
    );
  }
};

const mapStateToProps = ({ productService, airportCoverage, addressFromGoogle, airportZone, language }) => {
  const { products, selectedProduct } = productService;
  const { address, distanceMatrix, geometry, placeDetail } = addressFromGoogle;
  const { loading, airport, airportCityCoverage } = airportCoverage;
  const { zone } = airportZone;
  const { lang } = language;
  return { products, loading, airport, airportCityCoverage, address, distanceMatrix, zone, selectedProduct, geometry, placeDetail, lang };
}

export default connect(mapStateToProps, {
  getMasterAirport,
  getMasterCarType,
  getExtras,
  setTempForm,
  setRequestStockList,
  getAirportZone
})(AirportTransfer);