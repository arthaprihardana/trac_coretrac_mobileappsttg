import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import {
  View,
} from "react-native";
import MainButton from "@airport-transfer-fragments/MainButton";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import Text from "@airport-transfer-fragments/Text";
import { appVariables } from "@variables";
import styles from "./style";

export default class PickupDetail extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      form: {
        pickupLocation: "",
      },

      navData: props.navData,
    };
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------
  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderPickupDescription() {
    return (
      <View>
        <Text
          size={14}
          color={appVariables.colorGray}>
          Please enter the pickup detail information for a more approximate pickup
        </Text>
      </View>
    )
  }

  _renderPickupField() {
    return (
      <View>
        <FloatingTextInput
          label={"eg. Lobby utama Hotel Kempinski"}
          onChangeText={(value) => this.onChangeForm("pickupLocation", value)}
          value={this.state.form.pickupLocation}>
        </FloatingTextInput>
      </View>
    )
  }

  _renderButton() {
    return (
      <View style={styles.button}>
        <MainButton
          label={"CONTINUE"}
          rounded
          onPress={() => Actions.Login({navData:this.state.navData})}
        />
      </View>
    )
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------
  render() {
    return (
      <ProgressedContainer
        header={{
          label: "Pickup Details",
          onBackPress: () => Actions.pop(),
        }}
        steps={this.state.navData.processSteps}
        currentStepIndex={this.state.navData.processSteps.indexOf("Pickup")}
        {... this.props}
      >
        <View style={styles.container}>
          {this._renderPickupDescription()}
          {this._renderPickupField()}
          {this._renderButton()}
        </View>
      </ProgressedContainer>
    );
  }
}