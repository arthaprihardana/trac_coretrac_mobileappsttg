import { appVariables } from "@variables";

export default {
  container: {
    flex:1,
    paddingVertical:50,
    paddingHorizontal:20,
    backgroundColor:appVariables.colorWhite,
  },
  button:{
    flex:1,
    paddingVertical:50,
    paddingHorizontal:20,
    backgroundColor:appVariables.colorWhite,
  },
}