/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 14:24:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 13:31:42
 */
import React, { Component } from "react";
import {
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Clipboard,
    TextInput,
} from "react-native";
import { H2, p, ListItem } from 'native-base';
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import {
  appVariables,
  appMetrics
} from "@variables";
import { Actions } from "react-native-router-flux";
import _ from "lodash";
import Lang from '../../../../../assets/lang';

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import ImageHeader from "@airport-transfer-fragments/ImageHeader";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import MyWebView from '@airport-transfer-fragments/WebView';
import { connect } from "react-redux";

class PromoDetail extends Component {
    // ---------------------------------------------------
    // ---------------------------------------------------
    // CONSTRUCTOR AND LIFECYCLES
    // ---------------------------------------------------
    constructor(props) {
        super(props);
        this.state = { 
            datacontent : [{id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, ],
        };
    }
    // ---------------------------------------------------
    // ---------------------------------------------------
    // EVENT LISTENERS
    // ---------------------------------------------------

    // ---------------------------------------------------
    // ---------------------------------------------------
    // METHODS
    // --------------------------------------------------- 
    _copy = (code) => {
        Clipboard.setString(code);
    }

    _showLogin = ()=> {

    }
    // ---------------------------------------------------
    // ---------------------------------------------------
    // RENDERS
    // ---------------------------------------------------
    _renderBannerPromo() {
        return (
        <View style={styles.bannerPromo}>
            <View style={{
            flex: 1,
            flexDirection: "row"
            }}>
            <Image
                source={{ uri: this.props.item.ImageBanner }}
                style={{ height: 200, width: appMetrics.windowWidth }}
                resizeMode="cover" />
            </View>
        </View>
        );
    }
    // ---------------------------------------------------
    _renderPromoDescription() {
        const customStyle = `<style>* {max-width: 100%;} body {font-family: ${ appVariables.museo900 }; padding-right: 40px}</style>`;
        return (
        <View style={styles.promoDescription.wrapper}>
            <View style={{
                flex: 1,
                flexDirection: "row",
                paddingVertical: 20,
                }}>
                <Text size={24}>{this.props.item.Title}</Text>
            </View>

            <MyWebView originWhitelist={['*']} source={{ html: `${customStyle}${this.props.item.Content}` }} style={{ width: '100%', height: 300 }} mixedContentMode="always" />

            {/* {this.props.item.paket.map((value, index) => <View key={index}>
                <Text>{value.title}</Text>
                {value.detail.map((v, k) => <View key={k} style={{ flexDirection: "row", marginBottom: 10 }}>
                    <View key={k} style={{ width: '5%'}}>
                        <Text size={16} color={appVariables.colorGray}>{k + 1}</Text>
                    </View>
                    <View style={{ width: '95%' }}>
                        <Text size={16} color={appVariables.colorGray}>{v}</Text>
                    </View>
                </View>)}
            </View>)}
            
            <View style={{ marginBottom: 20 }} />
            
            <Text color={"grey"}>SYARAT DAN KETENTUAN</Text>
            <View style={{ marginBottom: 20 }} />
            <View>
            {this.props.item.syaratKetentuan.map((val, index) => <View key={index} style={{ flexDirection: "row"}}>
                <View style={{ width: '5%'}}>
                    <Text size={16} color={appVariables.colorGray}>{index + 1}</Text>
                </View>
                <View style={{ width: '95%' }}>
                    <Text size={16} color={appVariables.colorGray}>{val}</Text>
                </View>
            </View> )}
            </View> */}
        </View>
        );
    }
    
    // ---------------------------------------------------
    _renderOtherPromoCard() {
        return (
        <View style={styles.otherPromoCard.wrapper}>
            <View style={{
            flex: 1,
            flexDirection: "row"
            }}>
            <Image
                source={require('@images/png/messageDetail/promo1.png')}
                style={{ flex: -1 }}
                resizeMode="stretch" />
            </View>
            <View style={styles.otherPromoCard.title}>
            <Text
                size={16}>
                Promo HUT RI Dapatkan Voucher Diskon
            </Text>
            </View>
        </View>
        );
    }

    // ---------------------------------------------------
    _renderContent(item, key) {
        return (          
        <View style={{ width: "50%",}}>  
        <TouchableOpacity onPress={() => Actions.NewsDetail()}> 
            <View style={{
            flex: 1,
            flexDirection: "column",
            }}>
            <Image
                source={require('@images/png/bannerContent/contentNews.png')}              
                resizeMode="cover" />
            {/* <View style={styles.promoDescription.descriptionWrapper}> */}
                <Text style={{fontWeight: 'bold',}} size={18} color={appVariables.colorGray}>
                {item} 
                </Text>
            {/* </View> */}
            </View>
            <View style={styles.promoDescription.descriptionWrapper}>
            <Text size={12} color={appVariables.colorGray}>
            Jul 6, 2017
            </Text>
            </View>
            </TouchableOpacity>  
        </View>
        
        );
    }

    // ---------------------------------------------------
    // ---------------------------------------------------
    // MAIN RENDER
    // ---------------------------------------------------

    render() {
        return (
        <BaseContainer>
            <Header onBackPress={() => this.props.navigation.pop()} />
            <ScrollView style={{ flex: 1 }}>
            {this._renderBannerPromo()}
            {this._renderPromoDescription()}
            </ScrollView>
        </BaseContainer>
        );
    }
}

export default PromoDetail;
