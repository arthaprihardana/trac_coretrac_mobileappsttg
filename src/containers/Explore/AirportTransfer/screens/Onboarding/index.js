/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-07 23:44:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 11:46:53
 */
import { Container, H2, Text } from 'native-base';
import { Actions } from "react-native-router-flux";
import React, { Component } from 'react';
import { ImageBackground, StatusBar, TouchableOpacity, View } from 'react-native';
import Swiper from 'react-native-swiper';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { appStyles, appVariables, appMetrics } from '../../../../../assets/styles';
import { AButton } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';
import styles from './style';

class Onboarding extends Component {
  state = {
    activeSlide: 0,
    sliderData: [
      {
        title: 'Transportation services on the go with TRAC To Go',
        subtitle: 'From airport to your holiday destination, we always keep you company without hassle-free.',
        image: require("@images/png/onboarding/1.png")
      },
      {
        title: "Track your driver's location anywhere",
        subtitle: 'Wherever and whenever you are ready, our driver always accessible through your phone to get you to your next destination!',
        image: require("@images/png/onboarding/2.png")
      },
      {
        title: 'Various add ons to keep you moving on',
        subtitle: 'Customize your booking car based on your needs, we got them covered.',
        image: require("@images/png/onboarding/3.png")
      },
    ]
  }

  _renderItem = ({item, index}) => {
    let btn = null;
    if (index == 2) {
      btn = (
        <View style={{ paddingTop: 20}}>
        <AButton primary block style={styles.cta} onPress={() => Actions.Home({type: "replace"}) }>Get Started</AButton>
        <TouchableOpacity style={appStyles.mt20} onPress={() => Actions.Login({type: "replace"}) }><Text style={[styles.p, appStyles.alignCenter]}>I Already Have an Account</Text></TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={appStyles.appContainer}>
        <View style={appStyles.appImageBackground}>
          <ImageBackground style={appStyles.fullScreen} source={item.image} />
        </View>
        <View style={styles.logo}><SvgIcon name="tracLogoWhite" /></View>
        <View style={[appStyles.p20, styles.content]}>
          <H2 style={styles.h2}>{item.title}</H2>
          <Text style={styles.p}>{item.subtitle}</Text>
          { btn }
        </View>
      </View>
    )
  }

  get pagination () {
    const { sliderData, activeSlide } = this.state;
    
    return (
      <Pagination
        dotsLength={sliderData.length}
        activeDotIndex={activeSlide}
        containerStyle={{ backgroundColor: 'transparent', position: 'absolute', bottom: 0 }}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          marginHorizontal: 0,
          backgroundColor: 'rgba(255, 255, 255, 0.92)',
          alignSelf: 'center'
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.8}
      />
    );
  }

  render() {
    return (
      <Container style={appStyles.appContainer}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={appVariables.colorBlack}
        />

        <Carousel
          data={this.state.sliderData}
          renderItem={this._renderItem}
          sliderWidth={appMetrics.windowWidth}
          itemWidth={appMetrics.windowWidth}
          activeSlideAlignment='start'
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          onSnapToItem={(index) => this.setState({ activeSlide: index }) }
        />

        { this.pagination }
        
        {/* <Swiper
          loop={false}
          style={styles.image}
          activeDotColor={appVariables.colorActivePagination}
          dotColor={appVariables.colorPagination}
        >
          <View style={appStyles.appContainer}>
            <View style={appStyles.appImageBackground}>
              <ImageBackground style={appStyles.fullScreen} source={slide1} />
            </View>
            <View style={styles.logo}><SvgIcon name="tracLogoWhite" /></View>
            <View style={[appStyles.p20, styles.content]}>
              <H2 style={styles.h2}>Transportation services on the go with Trac to go</H2>
              <Text style={styles.p}>From airport to your holiday destination, we always keep you company without hassle-free.</Text>
            </View>
          </View>
          <View style={appStyles.appContainer}>
            <View style={appStyles.appImageBackground}>
              <ImageBackground style={appStyles.fullScreen} source={slide2} />
            </View>
            <View style={styles.logo}><SvgIcon name="tracLogoWhite" /></View>
            <View style={[appStyles.p20, styles.content]}>
              <H2 style={styles.h2}>Track your driver's location anywhere</H2>
              <Text style={styles.p}>Wherever and whenever you are ready, our driver always accessible through your phone to get you to your next destination!</Text>
            </View>
          </View>
          <View style={appStyles.appContainer}>
            <View style={appStyles.appImageBackground}>
              <ImageBackground style={appStyles.fullScreen} source={slide3} />
            </View>
            <View style={styles.logo}><SvgIcon name="tracLogoWhite" /></View>
            <View style={[appStyles.p20, styles.content]}>
              <H2 style={styles.h2}>Various add ons to keep you moving on</H2>
              <Text style={styles.p}>Customize your booking car based on your needs, we got them covered.</Text>
              <AButton primary block style={styles.cta} onPress={() => props.navigation.navigate('TabNavigator')}>Get Started</AButton>
              <TouchableOpacity style={appStyles.mt20} onPress={() => props.navigation.navigate('AuthRoutes')}><Text style={[styles.p, appStyles.alignCenter]}>I Already Have an Account</Text></TouchableOpacity>
            </View>
          </View>
        </Swiper> */}
        <TouchableOpacity style={styles.skip} onPress={() => Actions.Home({type: "replace"}) }><Text style={styles.small}>Skip</Text></TouchableOpacity>
      </Container>
    )
  }
}

export default Onboarding;