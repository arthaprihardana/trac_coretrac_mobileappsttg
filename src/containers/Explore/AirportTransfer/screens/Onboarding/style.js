import React from 'react';
import { StyleSheet } from 'react-native';
import { appMetrics, appVariables } from '@variables';

const styles = StyleSheet.create({
  image: {
    width: appMetrics.windowWidth, 
    height: appMetrics.windowHeight,
  },
  h2: {
    color: appVariables.colorWhite,
    fontFamily: appVariables.museo700,
    marginBottom: 10,
    lineHeight: 30,
  },
  p: {
    fontSize: 16,
    color: appVariables.colorWhite,
  },
  logo: {
    flex: 3.5,
    top: 80,
  },
  content: {
    flex: 3,
  },
  cta: {
    marginTop: 20,
    flex: 1,
    borderWidth: 1, 
    borderColor: 'red',
  },
  skip: {
    position: 'absolute',
    right: 20,
    bottom: 20,
  },
  small: {
    fontSize: 16,
    color: appVariables.colorWhite
  },
})

export default styles;