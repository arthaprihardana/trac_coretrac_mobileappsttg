/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 10:48:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 23:53:27
 */
import React, { Component } from 'react';
import { Content } from 'native-base';
import { ONotificationSection } from '../../../../../components/organisms';
import Header from "@airport-transfer-fragments/Header";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import { Actions } from 'react-native-router-flux';
import Lang from '../../../../../assets/lang';
import { connect } from "react-redux";

class Notification extends Component {
  
  render() {
    return (
      <BaseContainer>
        <Header
         title={Lang.notification[this.props.lang]}
         onBackPress={ () => Actions.pop()}
       />
        <Content style={{backgroundColor:'white'}}>
        <ONotificationSection />
        </Content>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang };
}

export default connect(mapStateToProps, {})(Notification);