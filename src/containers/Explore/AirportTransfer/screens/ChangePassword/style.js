import {Platform,StyleSheet} from 'react-native';
import { appVariables,appMetrics } from '@variables';

const styles = StyleSheet.create({
  titleLabel:{
    fontSize: 14,
    color: appVariables.colorGray,
    fontFamily: appVariables.museo500,
    lineHeight: 20,
    marginTop:0,
  },
  headerLabel:{
    fontSize: 14,
    color: appVariables.colorWhite,
    fontFamily: appVariables.museo500,
    flex: 1,
    lineHeight: 20,
    marginTop:0,
  },
  floatingLabel:{
    marginLeft:40,
    marginRight:40,
  },
  bottomButton:{
    width: appMetrics.windowWidth, 
    bottom: Platform.OS == "ios" ? 20: 0, 
    position: 'absolute', 
    height: 30,
    alignSelf: 'stretch',
  },
  closeButton:{
    top:0,
  },
})

export default styles;