/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 09:25:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-29 11:50:20
 */
import React, { Component } from 'react';
import { Item, Label, Input } from 'native-base';
import { Modal,View, Alert, Text} from 'react-native';
import styles from './style';
import ModalChangePassword from './thisModals/ModalChangePassword';
import MainButton from "@airport-transfer-fragments/MainButton";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import { putChangePassword } from "../../../../../actions";
import _ from "lodash";
import { REGEX_PASSWORD } from '../../../../../constant';
import Lang from '../../../../../assets/lang';

class ChangePassword extends Component {
  state = {
    modalChangePassword: false,
    oldPassword: null,
    newPassword: null,
    newPasswordConfirm: null,
    errorPassword: null,
    errorConfirmPassword: null
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.changePassword !== prevProps.changePassword) {
      return { changePassword: this.props.changePassword }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.changePassword !== undefined) {
        if(_.isString(snapshot.changePassword)) {
          Alert.alert(
            'Information',
            snapshot.changePassword,
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        } else {
          this.setState({modalChangePassword:true})
        }
      }
    } 
  }

  handleChangePasswordNotify = () => {
    this.setState({ modalChangePassword: false })
    Actions.pop();
  }

  onChangeForm(name, value) {
    const form = this.state;
    form[name] = value;
    this.setState({
      form
    });
    switch (name) {
      case "newPassword":
        this.setState({ errorPassword: REGEX_PASSWORD.test(value) ? null : "Minimum 8 character with Uppercase, Lowercase, Number and Special Character" });
        break;
      case "newPasswordConfirm":
        this.setState({ errorConfirmPassword: this.state.newPassword !== value ? "Confirm password didn't match with Password" : null });
        break;
      default:
        break;
    }
  }

  render() {
    const nav = this.props.navigation;

    return (
      <BaseContainer>
         <Header title={Lang.changePassword[this.props.lang]} onBackPress={ () => Actions.pop() }/>
        <View style={{
          flex: 1,
          backgroundColor: '#FFFFFF',
        }}>
          <View>
            <View style={{ marginTop: 30 }} >
              <Item floatingLabel style={styles.floatingLabel}>
                <Label style={styles.titleLabel}>{Lang.oldPassword[this.props.lang]}</Label>
                <Input secureTextEntry={true} onChangeText={text => this.onChangeForm("oldPassword", text)} />
              </Item>
            </View>
            <View style={{ marginTop: 20 }} >
              <Item floatingLabel style={styles.floatingLabel}>
                <Label style={styles.titleLabel}>{Lang.newPassword[this.props.lang]}</Label>
                <Input secureTextEntry={true} onChangeText={text => this.onChangeForm("newPassword", text)} />
              </Item>
              <View style={styles.floatingLabel}>
                <Text style={{ fontSize: 10, color: "#aaaaaa" }}>
                  { this.state.errorPassword }
                </Text>
              </View>
            </View>
            <View style={{ marginTop: 20 }} >
              <Item floatingLabel style={styles.floatingLabel}>
                <Label style={styles.titleLabel}>{Lang.confirm_password[this.props.lang]}</Label>
                <Input secureTextEntry={true} onChangeText={text => this.onChangeForm("newPasswordConfirm", text)} />
              </Item>
              <View style={styles.floatingLabel}>
                <Text style={{ fontSize: 10, color: "#aaaaaa" }}>
                  { this.state.errorConfirmPassword }
                </Text>
              </View>
            </View>
          </View>
        </View>

        <Modal animationType="none"  visible={this.state.modalChangePassword} onRequestClose={() => this.setState({ modalChangePassword: false })} >
          <ModalChangePassword action={this.handleChangePasswordNotify} />
        </Modal> 
        
        <MainButton
            isLoading={this.props.loading}
            label={Lang.changePassword[this.props.lang]}
            onPress={()=> {
              this.props.putChangePassword({
                FormData: {
                  UserProfileId: this.props.login.Data.Id,
                  password: this.state.newPassword,
                  password_confirmation: this.state.newPasswordConfirm,
                  old_password: this.state.oldPassword
                },
                token: this.props.login.token
              })
              // this.setState({modalChangePassword:true})
            }}>
        </MainButton>
      </BaseContainer>
    )
  }
}

const mapStateToProps = ({ userProfile, authentication, language }) => {
  const { login } = authentication;
  const { loading, changePassword } = userProfile;
  const { lang } = language;
  return { loading, changePassword, login, lang };
}

export default connect(mapStateToProps, {
  putChangePassword
})(ChangePassword);
