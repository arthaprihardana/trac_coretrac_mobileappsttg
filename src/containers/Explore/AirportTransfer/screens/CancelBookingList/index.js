/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 20:45:01 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-31 21:25:15
 */
import React, { Component } from "react";
import {
  View,
  Image,
  ScrollView,
} from "react-native";
import {
  appVariables,
} from "@variables";
import { connect } from "react-redux";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import NumberHelper from "@airport-transfer-helpers/Number";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import { Actions } from "react-native-router-flux";
import moment from "moment";
import "moment/locale/id";

class CancelBooking1 extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  goToCancelBookingReason() {
    Actions.push("CancelBookingReason", {
      item: this.props.item
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderCancellationDetail() {
    let x = _.filter(this.props.time, { MsProductId: this.props.item.details[0].MsProductId });
    let CancelationTime = x[0].configuration_adjustment_retail_prices[0].CancelationTime;
    var toMin = moment.duration(CancelationTime).asMinutes();
    var setHour = (new moment(this.props.item.details[0].StartDate, "YYYY-MM-DD HH:mm:ss")).subtract(toMin, "minutes");
    var now = moment().unix();
    var targetDate = setHour.unix();
    var diffTime = targetDate - now;
    var duration = moment.duration(diffTime*1000, 'milliseconds');
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 20,
        paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          <Text
            color="#1A3681"
            size={14}>
            Cancellation is Free
            </Text>
          <Text
            color={appVariables.colorBlack}
            style={{ paddingTop: 10 }}
            size={12}>
            {/* For: 10 days 7 hours 45 minutes */}
            For: {duration.days() >= 0 ? duration.days() : 0} days {duration.hours() >= 0 ? duration.hours() : 0} hours {duration.minutes() >= 0 ? duration.minutes() : 0} minutes
          </Text>
        </View>
      </View>
    )
  } 

  // ---------------------------------------------------
  _renderCancellationProgressBar() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'column',
        }}>
          {this._renderCancellationProgressBar1()}
          {this._renderCancellationProgressBar2()}
          {this._renderCancellationProgressBar3()}
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar1() {
    let x = _.filter(this.props.time, { MsProductId: this.props.item.details[0].MsProductId });
    let ChargeCancelation = x[0].configuration_adjustment_retail_prices[0].ChargeCancelation;
    let CancelationFee = (parseInt(this.props.item.TotalPrice) * (parseInt(ChargeCancelation) / 100));
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          {/* <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              FREE
            </Text>
          </View> */}

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              Cancellation Fees
            </Text>
            <Text
              color={appVariables.colorGray}
              size={12}>
              Rp { NumberHelper.format(CancelationFee) }
            </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              style={{ paddingTop: 10 }}
              size={12}>
              Non-Refundable
            </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar2() {
    return (
      <View style={styles.progressBar}>
        <View style={{
          flex: 1,
          flexDirection: 'row'}}>
          <View style={styles.circleWrapper}>
            <View style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: "center",
            }}>
              <View style={styles.progressBar.circleOrange}>
              </View>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleOrange}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleOrange}>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleOrange}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleOrange}>
            </View>
          </View>

          <View style={styles.rectangleWrapper}>
            <View style={styles.progressBar.rectangleGray}>
            </View>
          </View>

          <View style={styles.circleWrapper}>
            <View style={styles.progressBar.circleGray}>
            </View>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCancellationProgressBar3() {
    let x = _.filter(this.props.time, { MsProductId: this.props.item.details[0].MsProductId });
    let CancelationTime = x[0].configuration_adjustment_retail_prices[0].CancelationTime;
    var toMin = moment.duration(CancelationTime).asMinutes();
    var setHour = (new moment(this.props.item.details[0].StartDate, "YYYY-MM-DD HH:mm:ss")).subtract(toMin, "minutes");
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              { moment().isSameOrBefore(setHour.format("YYYY-MM-DD HH:mm:ss")) ? "Today": null }
            </Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: "center",
          }}>
            <Text
              color={appVariables.colorGray}
              size={12}>
              {setHour.locale('en').format("lll")}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCarPreference() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        paddingVertical: 20,
        paddingHorizontal: 20,
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
          }}>
            <Text
              size={14}
              color={appVariables.colorBlack}>
              List that will be canceled
          </Text>
          </View>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderCarList(vtId, carDescription, index) {
    let x = _.filter(this.props.vehicle, { vehicleTypeId: vtId });
    return (
      <View style={styles.carListWrapper} key={index}>
        <View style={{
          flex: 1,
          flexDirection: 'row'}}>
          <View style={styles.carImageWrapper}>
            <Image
              source={{ uri: x[0].vehicleImage }}
              style={{ height: 70, width: 150 }}
              resizeMode="cover"/>
          </View>
          <View style={{
            flex: 2,
            flexDirection: 'column',
            alignItems: "center",
            alignItems: "flex-start",
            justifyContent: "center",
            paddingLeft: 20,
          }}>
            <Text
              color={appVariables.colorBlack}
              size={16}>
              {x[0].vehicleAlias} #{index+1}
              </Text>
            <Text
              color={appVariables.colorGray}
              size={12}>
              {carDescription}
              </Text>
          </View>

        </View>
      </View>
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={"Cancel Booking"}
          onBackPress={() => this.props.navigation.pop()}/>
        <ScrollView style={styles.mainContainer}>
          {this._renderCancellationDetail()}
          {this._renderCancellationProgressBar()}
          <View style={styles.lineSeparator}>
          </View>
          {this._renderCarPreference()}
          {_.map(this.props.item.details, (v, i) => {
            var extras = v.reservation_extras.length > 0 ? _.map(v.reservation_extras[0].details, (vv, ii) => vv.MsExtrasName ) : [];
            return this._renderCarList(v.UnitTypeId, extras.length > 0 ? `Use additional items ${extras.join(",")}` : "", i)  
          })}
          {/* {this._renderCarList("Toyota Avanza #1","Use additional items wifi,overnight, baby car seat x 1 day")}
          {this._renderCarList("Toyota Avanza #2","Use additional items wifi,Fuel Plant")} */}
        </ScrollView>
        <MainButton
            label={"CANCEL BOOKING"}
            onPress={ () => this.goToCancelBookingReason() }
          />
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ vehicleAttribute, adjustmentTime }) => {
  const { vehicle } = vehicleAttribute;
  const { time } = adjustmentTime;
  return { vehicle, time }
}

export default connect(mapStateToProps, {})(CancelBooking1)