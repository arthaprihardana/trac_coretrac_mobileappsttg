import {
  appVariables,
  appMetrics,
} from "@variables";

export default {
  mainContainer:{
    flex: 1,
    backgroundColor: appVariables.colorWhite,
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  lineSeparator:{
    flex: 1,
    flexDirection: 'row',
    borderColor: "#D8D8D8",
    borderBottomWidth: 1,
  },
  progressBar:{
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    circleGray:{
      flex:-1,
      width:15,
      height:15,
      borderRadius:15/2,
      backgroundColor:"#D8D8D8",
    },
    circleOrange:{
      flex:-1,
      width:15,
      height:15,
      borderRadius:15/2,
      backgroundColor:"#F27C21",
    },
    rectangleOrange:{
      flex:-1,
      width:(appMetrics.screenWidth-20)/4,
      height:7,
      backgroundColor:"#F27C21",
    },
    rectangleGray:{
      flex:-1,
      width:(appMetrics.screenWidth-20)/4,
      height:7,
      backgroundColor:"#D8D8D8",
    },
  },
  circleWrapper:{
    flex: -1,
    flexDirection: 'column',
    alignItems: "center",
  },
  rectangleWrapper:{
    flex: 1,
    flexDirection: 'column',
    paddingTop: 5,
  },
  carListWrapper:{
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderColor: appVariables.colorGray,
    borderBottomWidth:1,
    borderBottomColor:appVariables.colorGray,
  },
  carImageWrapper:{
    flex: -1,
    flexDirection: 'column',
    alignItems: "center",
    justifyContent: "center",
  },
}