import {
  appVariables,
  appMetrics,
} from "@variables";

export default {
  container:{
    flexDirection: 'row',
  },
  subContainer:{
    flex:1,
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  sortWrapper: { 
    flexDirection: 'row', 
    height: 30, 
    alignItems: 'center' 
  },
  itemImage: { marginRight: 5},
  image: { width: 20, height: 20 },
  labelPhoneStyle: { 
    fontSize: 16 ,
    fontFamily: appVariables.museo500, 
    marginLeft: 10, 
    marginRight: 10, 
    marginTop: 0, },
  iconWrapper: { marginTop: 0 },
  dropdownItemWrapper: { 
    flexDirection: 'row', 
    alignItems: 'center', 
    borderBottomColor: '#C3C8D3', 
    borderBottomWidth: 2, 
    padding: 10, 
  },
  dropdownImage: { 
    width: 20, 
    height: 20
  },
  dropdownText: { 
    fontSize: 16, 
    fontFamily: appVariables.museo500, 
    color: '#242842', 
    marginLeft: 10 
  },
  textarea: { 
    paddingLeft: 0, left: 0, marginLeft: 0,
    fontSize: 16,
    color: appVariables.colorDark,
    fontFamily: appVariables.museo500,
    lineHeight: 20,
  },
  expandableDriverInformation: {
    title: {
      flex: -1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical:20,
      paddingHorizontal:10,
    },
  },
  expandableDriverInformationContent: {
    scanGuidance: {
      flex: -1,
      paddingTop: 20,
      paddingBottom: 20,
    },

    // ---------------------------------------------------

    citizenLabel: {
      container: {
        flex: -1,
        flexDirection: 'row',
      },
      innerContainer: {
        flex: 1,
        flexDirection: 'column',
      },
    },

    // ---------------------------------------------------

    idScanner: {
      boxContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 20,
      },
      boxInnerContainer: {
        flex: 4,
        borderWidth: 1,
        borderColor: '#8A959E',
        borderRadius: 5,
        borderStyle: 'dashed',
      },
    },

     // ---------------------------------------------------

    idScannerDetail: {
      wrapper: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: 10,
        paddingBottom: 10,
      },
      scanIcon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
      },
      scanImage: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      buttonWrapper: {
        flex: 1,
        paddingTop: 10,
        paddingHorizontal: 10,
      },
    },

    // ---------------------------------------------------

    submitButtonWrapper: {
      flex: 1,
      paddingTop: 20,
      paddingBottom: 20,
      paddingHorizontal: 20,
    },
  },
  
  collapsibleDriverInformation:{
    grayLineWrapper:{
      paddingVertical: 20,
      paddingHorizontal: 20,
    },
  },
}