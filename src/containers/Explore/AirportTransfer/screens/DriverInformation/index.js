/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-11 11:24:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-21 15:09:30
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert
} from "react-native";
import { Textarea } from "native-base";
import ModalDropdown from 'react-native-modal-dropdown';
import MCameraSim from '../../../../../components/molecules/MCameraSim';
import MCameraKtp from '../../../../../components/molecules/MCameraKtp';
import Mask from 'react-native-mask';
import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import SvgIcon from "../../../../../utils/SvgIcon";
import RoundedCheck from '../../fragments/RoundedCheck/index';
import { phonePrefix } from '../../../../../utils/DummyData';
import styles from "./style";
import MainButton from "@airport-transfer-fragments/MainButton";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";
import { appVariables } from "@variables";
import _ from "lodash";
import { connect } from "react-redux";
import { openCameraForSim, openCameraForKtp, setTempDriverDetail } from "../../../../../actions";
import Lang from '../../../../../assets/lang';

class DriverInformation extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------
  constructor(props) {
    super(props);

    const navData = JSON.parse(JSON.stringify(this.props.navData));
    const forms = navData.summaryData.list.map((item, index) => {
      return {
        ImDriver: false,
        ImageKTP: "",
        ImageSIM: "",
        IsForeigner: 0,
        IDCardNumber: "",
        LicenseNumber: "",
        Name: "",
        Address: "",
        PhoneNumber: "",
        IsPIC: false
      };
    });
    const errors = navData.summaryData.list.map((item, index) => {
      return {
        ImageKTP: null,
        ImageSIM: null,
        IDCardNumber: null,
        LicenseNumber: null,
        Name: null,
        Address: null,
        PhoneNumber: null,
      };
    });
    
    this.state = {
      navData: navData,
      forms: forms,
      errorMessage: errors,
      activeDropIndex: 0,
      activeIndex: 0,
      phoneData: phonePrefix,
      selectedPhoneNumber: phonePrefix[0].value,
      selectedPhoneImage: phonePrefix[0].image,
      readyToSubmit: false,
      selectedImDriver: null
    };
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.captureSim !== prevProps.captureSim) {
      return { captureSim: this.props.captureSim }
    }
    if(this.props.captureKtp !== prevProps.captureKtp) {
      return { captureKtp: this.props.captureKtp }
    }
    if(this.state.forms !== prevState.forms) {
      return { changeForms: this.state.forms }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.captureSim !== undefined) {
        let frm = this.state.forms;
        frm[this.state.activeDropIndex].ImageSIM = snapshot.captureSim;
        this.setState({
          forms: frm
        }, () => {
          this.onValidate("ImageSIM", this.state.activeDropIndex).then(val => {
            let ready = _.every(this.state.forms, v => {
              return !_.isEmpty(v.ImageKTP) && !_.isEmpty(v.ImageSIM) && !_.isEmpty(v.IDCardNumber) && !_.isEmpty(v.LicenseNumber) && !_.isEmpty(v.Name) && !_.isEmpty(v.Address) && !_.isEmpty(v.PhoneNumber)
            });
            this.setState({
              readyToSubmit: ready
            })
          })
        })
      }
      if(snapshot.captureKtp !== undefined) {
        let frm = this.state.forms;
        frm[this.state.activeDropIndex].ImageKTP = snapshot.captureKtp;
        this.setState({
          forms: frm
        }, () => {
          this.onValidate("ImageKTP", this.state.activeDropIndex).then(val => {
            let ready = _.every(this.state.forms, v => {
              return !_.isEmpty(v.ImageKTP) && !_.isEmpty(v.ImageSIM) && !_.isEmpty(v.IDCardNumber) && !_.isEmpty(v.LicenseNumber) && !_.isEmpty(v.Name) && !_.isEmpty(v.Address) && !_.isEmpty(v.PhoneNumber)
            });
            this.setState({
              readyToSubmit: ready
            })
          })
        })
      }
      if(snapshot.changeForms) {
        if(_.filter(snapshot.changeForms, {ImDriver: true}).length > 0) {

        }
      }
    }
  }
  
  
// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  onReady = () => {
    let ready = _.every(this.state.forms, v => {
      return !_.isEmpty(v.ImageKTP) && !_.isEmpty(v.ImageSIM) && !_.isEmpty(v.IDCardNumber) && !_.isEmpty(v.LicenseNumber) && !_.isEmpty(v.Name) && !_.isEmpty(v.Address) && !_.isEmpty(v.PhoneNumber)
    });
    return ready
  }
  
  onChangeForm(name, index, value) {
    const forms = this.state.forms;
    forms[index][name] = value;
    let ready = this.onReady();
    this.setState({
      forms,
      readyToSubmit: ready
    });
  }

  onValidate = async (current, index) => {
    let except = ['IsPIC', 'IsForeigner'];
    except.push(current);
    let o = _.omit(this.state.forms[index], except);
    let a = await _.findKey(o, val => {
      return _.isEmpty(val);
    });
    return a;
  }

  // infoEmptyField(index) {
  //   let except = ['ImDriver', 'IsPIC', 'IsForeigner'];
  //   let form = _.omit(this.state.forms[index], except);
  //   let err = this.state.errorMessage[index];
  //   let f = _.findKey(form, val => _.isEmpty(val));
  //   console.log('f ==>', f === "IDCardNumber");
  //   console.log('err ==>', err);
    
  //   if(f !== undefined) {
  //     switch (f) {
  //       case "ImageKTP":
  //         this._form.scrollTo({x: 80, y: 0, animated: true});
  //         err.ImageKTP = "Foto KTP harus diupload";
  //         break;
  //       case "ImageSIM":
  //         this._form.scrollTo({x: 80, y: 0, animated: true});
  //         err.ImageSIM = "Foto SIM harus diupload";
  //         break;
  //       case "LicenseNumber":
  //         this._form.scrollTo({x: 170, y: 0, animated: true});
  //         err.LicenseNumber = "No SIM harus diisi";
  //         break;
  //       case "IDCardNumber":
  //         this._form.scrollTo({x: 120, y: 0, animated: true});
  //         // err.IDCardNumber = "No KTP harus diisi";
  //         break;
  //       case "Name":
  //         this._form.scrollTo({x: 230, y: 0, animated: true});
  //         err.Name = "Nama harus diisi";
  //         break;
  //       case "Address":
  //         err.Address = "Alamat harus diisi";
  //         break;
  //       case "PhoneNumber":
  //         err.PhoneNumber = "No Telepon harus disii";
  //         break;
  //       default:
  //         break;
  //     }
  //     this.setState({ errorMessage: err })
  //   }
  //   return null;
  // }

// ---------------------------------------------------

  toggleDrop(index) {
    if (this.state.activeDropIndex == index) {
      return false;
    }

    this.setState({
      activeDropIndex: index,
    });
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderExpandableDriverInformation(label, index) {
    const isActive = this.state.activeDropIndex == index;
    let contentRender = null;
    if (isActive) {
      contentRender = this._renderDriverInformationContent(index);
    }
    return (
      <View>
        <TouchableOpacity style={styles.expandableDriverInformation.title}
          onPress={() => this.toggleDrop(index)}>
          <View style={{
            flex: -1,
            justifyContent: 'center',
          }}>
            <SvgIcon name="icoIDCard"></SvgIcon>
          </View>
          <View style={{
            flex: 1,
            paddingLeft: 20,
          }}>
            <Text
              size={18}>
              {label}
            </Text>
          </View>
        </TouchableOpacity>
        {contentRender}
      </View>
    );
  }
// ---------------------------------------------------

  _renderImDriver(label, value, index) {
    return (
      <View style={{ flex: -1 }}>
        <RoundedCheck value={this.state.forms[index].ImDriver === value} onPress={() => {
            let frm = this.state.forms;
            frm[index].ImDriver = value;
            // frm[index].ImageKTP = this.props.tempPersonalDetail.capture !== null ? this.props.tempPersonalDetail.capture : this.props.bufferKtp ;
            // frm[index].ImageSIM = this.props.login.Data.ImageSIM !== null ? this.props.bufferSim : "";
            frm[index].ImageKTP = this.props.tempPersonalDetail.capture !== null ? this.props.tempPersonalDetail.capture : this.props.login.Data.ImageKTP ;
            frm[index].ImageSIM = this.props.login.Data.ImageSIM !== null ? this.props.login.Data.ImageSIM : "";
            frm[index].IsForeigner = parseInt(this.props.login.Data.IsForeigner);
            frm[index].IDCardNumber = this.props.tempPersonalDetail.ktp;
            frm[index].LicenseNumber = this.props.login.Data.NoSIM;
            frm[index].Name = this.props.tempPersonalDetail.name;
            frm[index].Address = this.props.tempPersonalDetail.address;
            frm[index].PhoneNumber = this.props.tempPersonalDetail.phone_number.replace(/\+62|0/g, '');
            frm[index].IsPIC = true;
            this.setState({
              forms: frm
            }, () => {
              let ready = this.onReady();
              this.setState({ readyToSubmit: ready })
            });
          }}
          >
          <Text size={14}>{label}</Text>
        </RoundedCheck>
      </View>
    )
  }
  
  _renderScanGuidance() {
    return (
      <View style={styles.expandableDriverInformationContent.scanGuidance}>
        <Text
          color={appVariables.colorGray}
          size={14}>
          {Lang.pleaseScanUploadKTPDriver[this.props.lang]}
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderCitizen(index) {
    return (
      <View style={styles.expandableDriverInformationContent.citizenLabel.container}>
        <View style={styles.expandableDriverInformationContent.citizenLabel.innerContainer}>
          {this._renderCitizenDetail(Lang.indeonsiaCitizen[this.props.lang], 0, index)}
        </View>

        <View style={styles.expandableDriverInformationContent.citizenLabel.innerContainer}>
          {this._renderCitizenDetail(Lang.foreignCitizen[this.props.lang], 1, index)}
        </View>

      </View>
    )
  }

// ---------------------------------------------------

  _renderCitizenDetail(label, value, index) {
    return (
      <View style={{ flex: -1 }}>
        <RoundedCheck
          value={this.state.forms[index].IsForeigner === value}
          onPress={() => {
            let frm = this.state.forms;
            frm[index].IsForeigner = value;
            this.setState({
              forms: frm
            });
          }}
          >
          <Text size={14}>{label}</Text>
        </RoundedCheck>
      </View>
    )
  }

// ---------------------------------------------------

  _renderIDScanner(index) {
    return (
      <View style={styles.expandableDriverInformationContent.idScanner.boxContainer}>
        <View style={styles.expandableDriverInformationContent.idScanner.boxInnerContainer}>
          <View style={styles.expandableDriverInformationContent.idScannerDetail.wrapper}>
            { this.state.forms[index].ImageKTP !== "" ?
            <View style={styles.expandableDriverInformationContent.idScannerDetail.scanImage}>
              <Image
                style={{width: 150, height: 100}}
                resizeMode="cover"
                source={{uri: typeof this.state.forms[index].ImageKTP === "object" ? this.state.forms[index].ImageKTP.uri : this.state.forms[index].ImageKTP }}
              />
            </View> : <View style={styles.expandableDriverInformationContent.idScannerDetail.scanIcon}>
              <SvgIcon name="icoScan" ></SvgIcon>
            </View> }
            <View style={styles.expandableDriverInformationContent.idScannerDetail.buttonWrapper}>
              <MainButton
                onPress={() => {
                  Alert.alert(
                    'Info',
                    'Dari mana anda akan mengambil gambar?',
                    [
                      {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                      {
                        text: 'Galeri',
                        onPress: () => Actions.push("Galeri", { route: "DriverInformation", type: "ktp" }),
                        style: 'cancel',
                      },
                      {text: 'Kamera', onPress: () => this.props.openCameraForKtp(true)},
                    ],
                    {cancelable: false},
                  );
                }}
                label={Lang.photoKTP[this.props.lang]}
                backgroundColor={appVariables.colorBlueHeader}
                rounded>
              </MainButton>
            </View>
          </View>
        </View>
        <View style={{
          flex: 0.25
        }}>
        </View>
        <View style={styles.expandableDriverInformationContent.idScanner.boxInnerContainer}>
          <View style={styles.expandableDriverInformationContent.idScannerDetail.wrapper}>
            { this.state.forms[index].ImageSIM !== "" ?
            <View style={styles.expandableDriverInformationContent.idScannerDetail.scanImage}>
              <Image
                style={{width: 150, height: 100}}
                resizeMode="cover"
                source={{uri: typeof this.state.forms[index].ImageSIM === "object" ? this.state.forms[index].ImageSIM.uri : this.state.forms[index].ImageSIM }}
              />
            </View> : <View style={styles.expandableDriverInformationContent.idScannerDetail.scanIcon}>
              <SvgIcon name="icoScan" ></SvgIcon>
            </View> }
            <View style={styles.expandableDriverInformationContent.idScannerDetail.buttonWrapper}>
              <MainButton
                onPress={() => {
                  Alert.alert(
                    'Info',
                    'Dari mana anda akan mengambil gambar?',
                    [
                      {text: 'Batal', onPress: () => console.log('Ask me later pressed')},
                      {
                        text: 'Galeri',
                        onPress: () => Actions.push("Galeri", { route: "DriverInformation", type: "sim" }),
                        style: 'cancel',
                      },
                      {text: 'Kamera', onPress: () => this.props.openCameraForSim(true)},
                    ],
                    {cancelable: false},
                  );  
                }}
                label={Lang.photoSIM[this.props.lang]}
                backgroundColor={appVariables.colorBlueHeader}
                rounded>
              </MainButton>
            </View>
          </View>
        </View>
      </View>
    )
  }

// ---------------------------------------------------

  _renderField(index) {
    return (
      <View>
        <FloatingTextInput
          label={Lang.ktpNumber[this.props.lang]}
          keyboardType={"numeric"}
          onChangeText={(value) => {
            this.onValidate("IDCardNumber", index).then(val => {
              this.onChangeForm("IDCardNumber", index, value);
            });
          }}
          value={this.state.forms[index].IDCardNumber}
          maxLength={16}
          note={this.state.errorMessage[index].IDCardNumber}
        />
        <FloatingTextInput
          label={Lang.simNumber[this.props.lang]}
          keyboardType={"numeric"}
          onChangeText={(value) => {
            this.onValidate("LicenseNumber", index).then(val => {
              this.onChangeForm("LicenseNumber", index, value)
            });
          }}
          value={this.state.forms[index].LicenseNumber}
          maxLength={12}
          note={this.state.errorMessage[index].LicenseNumber}
        />
        <FloatingTextInput
          label={Lang.fullName[this.props.lang]}
          onChangeText={(value) => {
            this.onValidate("Name", index).then(val => {
              this.onChangeForm("Name", index, value)
            });
          }}
          value={this.state.forms[index].Name}
          note={this.state.errorMessage[index].Name}
        />
        <FloatingTextInput
          label={Lang.residentialAddress[this.props.lang]}
          onChangeText={(value) => {
            this.onValidate("Address", index).then(val => {
              this.onChangeForm("Address", index, value)
            });
          }}
          value={this.state.forms[index].Address}
          note={this.state.errorMessage[index].Address}
        />
      </View>
    )
  }

// ---------------------------------------------------

  _phoneNumberSelected = (idx, value) => {
    this.setState({
      selectedPhoneNumber: value.value,
      selectedPhoneImage: value.image
    });
  }

  _renderPhoneNumber = (option, index) => {
    return (
      <View key={index} style={styles.dropdownItemWrapper}>
        <Mask shape={'circle'}>
          <Image source={option.image} style={styles.dropdownImage} />
        </Mask>
        <Text style={styles.dropdownText}>{option.value}</Text>
      </View>
    );
  }

  _renderFieldPhoneNumber(index) {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'column' }}>
          <View style={{ flex: 1 }}>
            <ModalDropdown
              style={{
                marginLeft: 0,
                flex: 1
              }}
              ref="phone_number"
              options={this.state.phoneData}
              onSelect={(idx, value) => this._phoneNumberSelected(idx, value)}
              renderRow={this._renderPhoneNumber}>
              <View style={styles.sortWrapper}>
                <Mask style={styles.itemImage} shape={'circle'}>
                  <Image style={styles.image} source={this.state.selectedPhoneImage} />
                </Mask>
                <Text style={styles.labelPhoneStyle}>{this.state.selectedPhoneNumber}</Text>
                <SvgIcon name="icoSort" style={styles.iconWrapper} />
              </View>
            </ModalDropdown>
          </View>
          <View style={{
            flex: 3,
            flexDirection: 'row',
            borderBottomWidth: 1,
            marginLeft: 0,
            borderBottomColor: "#aaaaaa" 
            }} />
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          paddingLeft: 20,
          }}>
          <View style={{
            flex: 1,
            borderBottomWidth: 1,
            height: 35,
            borderBottomColor: "#aaaaaa",
            marginRight: 0,
            marginTop: 0,
            }}>
            <Textarea rowSpan={5} placeholder="" maxLength={12} style={styles.textarea} keyboardType="number-pad" value={this.state.forms[index].PhoneNumber} onChangeText={(value) => {
              this.onValidate("PhoneNumber", index).then(val => {
                this.onChangeForm("PhoneNumber", index, value)
              });
            }} />
          </View>
        </View>
      </View>
    )
  }

// ---------------------------------------------------

  _renderButton() {
    return (
      <View style={styles.expandableDriverInformationContent.submitButtonWrapper}>
        <MainButton
          label={Lang.continue[this.props.lang]}
          rounded
          onPress={ () => {
            if(this.state.readyToSubmit) {
              this.props.setTempDriverDetail(this.state.forms);
              Actions.push('Payment', {
                navData: this.props.navData
              })
            } /** else {
              this.infoEmptyField(this.state.activeDropIndex);
            } */
            return true;
          }}
          disabled={!this.state.readyToSubmit}
        />
      </View>
    )
  }

// ---------------------------------------------------

  _renderGrayLine() {
    return (
      <View style={{
        flex: -1,
        borderBottomWidth: 1,
        borderBottomColor: appVariables.colorGray,
      }}>
      </View>
    )
  }

// ---------------------------------------------------

  _renderDriverInformationContent(index) {
    return (
      <View style={{
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
      }}>
        {_.filter(this.state.forms, 'ImDriver').length === 0 ? this._renderImDriver(Lang.ImDriver[this.props.lang], true, index) : 
        (_.findIndex(this.state.forms, o => { return o.ImDriver == true; }) === index ? this._renderImDriver(Lang.ImDriver[this.props.lang], true, index) : null )}
        {this._renderScanGuidance()}
        {this._renderCitizen(index)}
        {this._renderIDScanner(index)}
        {this._renderField(index)}
        <View style={{
          flex: 1,
          paddingTop: 40
          }}>
          <Text
            size={12}
            color={appVariables.colorGray}>
            {Lang.phoneNumber[this.props.lang]}
          </Text>
        </View>
        {this._renderFieldPhoneNumber(index)}
        {/* {this._renderButton()} */}
      </View>
    )
  }

// ---------------------------------------------------

  _renderForms() {
    return this.state.forms.map((form, index) => {
      return (
        <View key={ index } style={styles.collapsibleDriverInformation.grayLineWrapper}>
          {this._renderExpandableDriverInformation(`${Lang.driverInformation[this.props.lang]} #${index + 1}`, index)}
        </View>
      );
    })
        {/*this._renderGrayLine()*/}
  }



// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ProgressedContainer
        header={{
          label: Lang.driverInformation[this.props.lang],
          onBackPress: () => Actions.pop(),
        }}
        steps={this.state.navData.processSteps}
        currentStepIndex={this.state.navData.processSteps.indexOf(Lang.driverInformation[this.props.lang])}
        {... this.props}
      >
        <View style={{
          flex: 1,
          backgroundColor:appVariables.colorWhite,
        }}>
          <ScrollView ref={ref => this._form = ref} style={{flex: 1,}}>
            <View style={{flex: 1,}}>
              
              { this._renderForms() }

            </View>
            {this._renderButton()}
          </ScrollView>
        </View>
        { this.props.openCameraSim && 
          <MCameraSim 
            visible={this.props.openCameraSim}
            onCapture={() => console.log('on capture')}
            onClose={() => this.props.openCameraForSim(false) }
          /> }

          { this.props.openCameraKtp && 
          <MCameraKtp 
            visible={this.props.openCameraKtp}
            onCapture={() => console.log('on capture')}
            onClose={() => this.props.openCameraForKtp(false) }
          /> }
      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ temporaryForm, camera, authentication, imageBuffer, language }) => {
  const { tempPersonalDetail } = temporaryForm;
  const { openCameraSim, captureSim, openCameraKtp, captureKtp } = camera;
  const { login } = authentication;
  const { bufferKtp, bufferSim } = imageBuffer;
  const { lang } = language;
  return { tempPersonalDetail, openCameraSim, captureSim, openCameraKtp, captureKtp, login, bufferKtp, bufferSim, lang }
}

export default connect(mapStateToProps, {
  openCameraForSim,
  openCameraForKtp,
  setTempDriverDetail
})(DriverInformation)
