/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 11:45:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 11:34:38
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import {
  appVariables,
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import Icon from '@airport-transfer-fragments/Icon';
import styles from "./style";
// import { BoxShadow } from "react-native-shadow";
// import faqList from '../../../../../utils/DummyData/faqList';
import privacyPolicyList from './data';
import { connect } from "react-redux";
import Lang from '../../../../../assets/lang';

class PrivacyPolicy extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      activeDropIndex: 0,
      activeButtonIndex: 0,
      activeCategory:"ALL",
      // faqList: faqList,
    };
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  toggleDrop(index) {
    this.setState({
      activeDropIndex: index,
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  

  // ---------------------------------------------------
  _renderDropdown(label, content, index) {
    const isActive = this.state.activeDropIndex == index;

    let contentRender = null;
    if (isActive) {
      contentRender = this._renderContent(content);
    }

    return (
      <View key={index} style={styles.dropdown.container}>
        <TouchableOpacity style={styles.dropdown.label.container}
          onPress={() => this.toggleDrop(index)}>
          <View style={styles.dropdown.label.textContainer}>
            <Text size={16}>
              {label}
            </Text>
          </View>

          <View style={styles.dropdown.label.iconContainer}>
            <Icon size={20} name={isActive ? "caret-up" : "caret-down"} />
          </View>
        </TouchableOpacity>

        {contentRender}

      </View>
    )
  }

  // ---------------------------------------------------

  _renderContent(content) {
    return (
      <View style={styles.dropdown.content.container}>
        <Text color={appVariables.colorGray}>
          {content}
        </Text>
      </View>
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header
          title={Lang.privacyPolicy[this.props.lang]}
          onBackPress={() => this.props.navigation.pop()} />
        <ScrollView showsVerticalScrollIndicator={false}>
          {
            privacyPolicyList
              .filter((faq) => {
                return this.state.activeCategory=="ALL" || (this.state.activeCategory==faq.category);
              })
              .map((faq, index) => {
                return this._renderDropdown(faq.question, faq.answer, index);
              })
          }
        </ScrollView>
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(PrivacyPolicy)
