let privacyPolicy = [
    {
        category: "KEBIJAKAN PRIVASI",
        question: "Aturan Privasi",
        answer: `Pengenalan 
        
Selamat datang di situs resmi TRAC yang beralamat www.trac.astra.co.id (selanjutnya disebut sebagai “Situs TRAC”). Kebijakan Privasi ini mengatur dan atau menjelaskan seluruh layanan yang sudah kami sediakan untuk Anda (atau “Pengguna”) gunakan, baik layanan yang Kami operasikan sendiri maupun yang dioperasikan melalui afiliasi dan/atau rekanan kami.

Untuk menjaga kepercayaan Anda kepada Kami, kami senantiasa akan menjaga segala kerahasiaan yang terkandung dalam data pribadi Anda, karena kami menganggap privasi Anda sangat penting bagi Kami. Dalam aturan privasi ini Kami sertakan penjelasan mengenai tata cara Kami mengumpulkan, menggunakan, mengungkapkan, memproses, dan melindungi informasi dan data pribadi Anda yang kami identifikasi (“Data Pribadi”).

Pada saat Anda membuat pemesanan dan atau akun pribadi pada website TRAC dan dengan mengklik notofikasi yang muncul pada halaman website, secara otomatis Anda juga memberikan persetujuan kepada Kami untuk mencatat dan menyimpan informasi dan Data Pribadi Anda (selanjutnya disebut sebagai “Persetujuan User”). Di mana pada prinsipnya, setiap data yang Anda berikan dari waktu ke waktu akan Kami simpan dan kami gunakan untuk kepentingan penyediaan produk dan layanan kami kepada Anda, yaitu antara lain untuk keperluan tokenisasi, akuntansi, tagihan, audit, verifikasi kredit atau pembayaran, serta keperluan keamanan, administrasi dan hukum, pengujian, pemeliharaan dan pengembangan sistem, hubungan pelanggan, promosi dan membantu kami di kemudian hari dalam memberikan pelayanan kepada Anda dan Kami dapat menggunakan data pribadi Anda dan informasi lainnya yang dikumpulkan dengan tujuan pemasaran media sosial menggunakan teknik grafik langsung dan terbuka dan untuk tujuan pemasaran digital konvensional, seperti mengirimkan Anda informasi secara otomatis melalui surat elektronik untuk memberitahukan informasi produk dan layanan terbaru, penawaran khusus, atau informasi lainnya yang menurut kami akan menarik bagi Anda (selanjutnya disebut sebagai “Tujuan Penggunaan Data Pribadi”)

Untuk tercapainya Tujuan Penggunaan Data Pribadi, Kami dapat mengungkapkan data Anda kepada grup perusahaan di mana TRAC bergabung di dalamnya, mitra penyedia produk dan layanan, perusahaan lain yang tercatat sebagai rekanan dari TRAC, perusahaan yang ditunjuk untuk melakukan proses data yang terikat kontrak dengan kami, agen perjalanan, badan pemerintah dan badan peradilan yang berwenang di jurisdiksi manapun.

Kebijakan privasi dan ketentuan lainnya dalam penggunaan website ini sangat penting untuk menjaga keamanan data pribadi Anda. Oleh karena itu, Anda dimohon untuk membaca secara seksama dan menyeluruh mengenai ketentuan dalam kebijakan privasi ini, juga ketentuan lainnya dalam website kami.

Informasi yang kami kumpulkan dan gunakan

Pada saat Persetujuan User telah diberikan, maka semua informasi dan data pribadi Anda akan kami kumpulkan dengan ketentuan sebagai berikut :

a. Kami akan mengumpulkan informasi mengenai komputer atau pun media apapun yang Anda gunakan, termasuk IP address, sistem operasi, browser yang digunakan, URL, halaman, lokasi geografis, dan waktu akses serta data lainnya terkait penggunaan komputer Anda (“Detail IP”).

b. Kami akan meminta Anda untuk mengisi data-data pribadi secara benar, jelas, lengkap, akurat, dan tidak menyesatkan, seperti nama, alamat email, nomor telepon, alamat tempat tinggal lengkap, informasi yang digunakan untuk pembayaran, informasi kartu kredit (nomor kartu kredit dan masa berlaku kartu kredit) dan data-data lain yang kami perlukan guna memproses transaksi Anda yang dilakukan melalui channel website dan layanan online lainnya yang kami sediakan. Dengan demikian, Anda dapat menikmati layanan yang Anda butuhkan. Namun, kami tidak bertanggung jawab atas segala kerugian yang mungkin terjadi karena informasi dan atau data yang tidak benar, jelas, lengkap, akurat dan menyesatkan yang Anda berikan;

c. Kami dapat menggunakan data pribadi Anda dan informasi lainnya yang dikumpulkan dengan tujuan pemasaran media sosial menggunakan teknik grafik langsung dan terbuka dan untuk tujuan pemasaran digital konvensional, seperti mengirimkan Anda informasi secara otomatis melalui surat elektronik untuk memberitahukan informasi produk dan layanan terbaru, penawaran khusus, atau informasi lainnya yang menurut kami akan menarik bagi Anda.

d. Dalam menggunakan layanan kami, informasi-informasi yang Anda informasikan dapat kami gunakan dan berikan kepada pihak ketiga yang bekerja sama dengan kami, sejauh untuk kepentingan transaksi dan atau untuk memberikan dan meningkatkan pengalaman Anda ketika mengakses dan atau menggunakan layanan kami

e. Segala informasi yang kami terima dapat kami gunakan untuk melindungi diri kami terhadap segala tuntutan dan hukum yang berlaku terkait dengan penggunaan layanan dan pelanggaran yang Anda lakukan pada website kami atas segala ketentuan sebagaimana diatur dalam persyaratan layanan TRAC dan pedoman penggunaan produk dan layanan kami, termasuk dan tidak terbatas apabila dibutuhkan atas perintah Pengadilan dalam proses hukum;

f. Anda bertanggung jawab atas kerahasiaan informasi dan data pribadi Anda, termasuk bertanggung jawab atas semua akses dan penggunaan website yang menggunakan kata sandi dan akun serta token yang Anda miliki yang digunakan oleh siapa saja, baik atas seijin maupun tidak seijin Anda. Demi keamanan data rahasia Anda, kami sangat menyarankan agar Anda menyimpan akun dan kata sandi yang Anda miliki dengan sebaik-baiknya dan atau melakukan perubahan kata sandi secara berkala. Setiap penggunaan yang tidak sah dan tanpa sepengetahuan dan izin Anda menjadi tanggung jawab Anda sendiri dan kami tidak bertanggung jawab atas segala kerugian yang ditimbulkan sebagai akibat dari kelalaian yang Anda lakukan.

g. Anda harus segera memberitahukan kepada kami mengenai adanya penggunaan sandi atau akun tanpa izin Anda atau semua bentuk pelanggaran atau ancaman pelanggaran keamanan dalam website ini. Informasi Anda sangat berguna bagi kami untuk melakukan pencegahan, perbaikan, atau penghentian segala aktivitas illegal yang merugikan Anda dan atau keamanan website TRAC.

h. Anda berhak untuk merubah dan atau menghapus data alamat yang telah Anda berikan dan telah tersimpan dalam sistem kami dengan cara menghubungi Customer Service TRAC di nomor (021) 877 877 87 atau dapat mengirimkan email ke rco.nasional@trac.astra.co.id

Penggunaan Situs TRAC

Dengan menerima Kebijakan Privasi atas Situs TRAC ini, maka Anda telah diberikan keleluasaan dan atau otoritas akses yang terbatas dan dapat dibatalkan, tidak dapat dipindah-tangankan dan lisensi non-eksklusif untuk mengakses dan menggunakan website melalui browser Anda untuk tujuan transaksi produk dan layanan yang kami sediakan.

Dalam penggunaan website, Anda akan memiliki kata sandi dan akun yang unik untuk memungkinkan Anda mengakses bagian-bagian tertentu dari website ini. Setiap kali Anda menggunakan kata sandi atau akun tersebut, Anda akan dianggap memiliki otorisasi untuk mengakses dan menggunakan website ini dengan cara yang sesuai dengan Kebijakan Privasi ini dan kami tidak memiliki kewajiban untuk menyelidiki otorisasi atau sumber dari akses tersebut atau penggunaan website. Dengan demikian, Anda setuju bahwa Anda senantiasa memperhatikan ketentuan larangan yang berlaku dalam penggunaan website ini, yaitu sebagai berikut :

Anda dilarang untuk melakukan serta tidak akan membantu orang lain untuk mereproduksi, mendistribusikan, menampilkan, menjual, menyewakan, mengirimkan, membuat karya turunan dari, menerjemahkan, memodifikasi, merekayasa balik, membongkar, menguraikan atau mengeksploitasi website ini atau sebagian dari itu.
Anda dilarang untuk menjual dan memindah-tangankan akun Anda, profil, atau item yang terkait dengan website dan layanan yang kami sediakan, kecuali secara tegas diizinkan secara tertulis oleh k
Anda dilarang untuk menggunakan setiap informasi yang diberikan pada website atau membuat penggunaan website untuk kepentingan komersial dan atau bisnis lain, kecuali secara tegas diizinkan secara tertulis oleh k
Anda dilarang untuk meniru atau menggambarkan diri Anda dengan orang lain atau badan usaha lain pada saat Anda memberikan data dan informasi Anda kepada kami dalam penggunaan website dan layanan k
Anda dilarang untuk mengirimkan iklan, materi promosi, email spam, serta merusak operasi layanan kami dengan cara menggunggah virus, worm atau kode berbahaya lainnya.
Anda dilarang untuk melakukan serta tidak akan membantu orang lain untuk mengasosiasikan, mencitrakan, mengaitkan, menghubungkan, mengafiliasikan dengan suatu website yang berisi konten-konten sebagai berikut :
Konten yang melanggar hak kekayaan intelektual dan hak kepemilikan lainnya.
Konten yang mengandung unsur pornografi atau cabul atau asusila, perjudian, penghinaan, fitnah dan atau pencemaran nama baik, pemerasan dan atau pengancaman, penipuan, pemalsuan.
Konten yang menimbulkan dan atau mendorong timbulnya atau memfasilitasi rasa kebencian, permusuhan, pengancaman, penghasutan, pelecehan dan mengobarkan suatu ketegangan antar perorangan, pihak manapun atau kelompok tertentu, baik berdasarkan suku, agama, ras dan antar golongan (SARA) ataupun gender atau orientasi seksual, asal negara, etnis, kebangsaan, keterbatasan fisik dan keyakinan.
Konten-konten terlarang lainnya berdasarkan peraturan perundang-undangan yang berlaku.
Apabila terdapat perilaku Anda selaku pelanggan yang termasuk dan atau dianggap melanggar hukum dan ketentuan yang berlaku serta merupakan pelanggaran terhadap Kebijakan Privasi ini, atau berbahaya bagi kepentingan kami, kami berhak melakukan pencabutan terhadap ijin akses yang diberikan dalam Kebijakan Privasi ini tanpa pemberitahuan terlebih dahulu kepada Anda. Kami juga berhak untuk menolak layanan, menghentikan akun, dan atau membatalkan pesanan yang Anda lakukan, termasuk dan tidak terbatas untuk mengungkapkan identitas dalam database Anda kepada pihak yang berwenang atas dasar suatu surat perintah yang sah.

Anda harus bertanggung jawab terhadap semua kerugian yang kami maupun afiliasi dan mitra atau rekanan kami alami, termasuk dan tidak terbatas terhadap klaim pihak ketiga, beserta biaya-biaya lainnya yang mungkin muncul sebagai akibat yang ditimbulkan dari perilaku Anda. Konten yang disediakan di website ini adalah semata-mata untuk tujuan informasi. Dengan Anda menyetujui ketentuan ini, maka sekaligus membebaskan kami dari tanggung jawab atas setiap klaim, sanggahan, kerusakan, atau kerugian yang diakibatkan dari penggunaan website atau layanan yang terkandung di dalamnya.

Jika Anda masih berusia di bawah 18 tahun, silahkan menggunakan layanan dari Situs TRAC dengan persetujuan dan pengawasan orang tua atau wali Anda. 

Perubahan

Kami berhak untuk melakukan perubahan, penambahan, dan atau pengurangan seluruh ataupun sebagian ketentuan dari aturan privasi ini dari waktu ke waktu tanpa pemberitahuan terlebih dahulu dan segala perubahan yang Kami lakukan akan kami beritahukan melalui website www.trac.astra.co.id. Kami harap Anda dapat secara berkala memeriksa mengenai kebijakan ini. Dengan mengakses dan menggunakan layanan kami, maka secara langsung kami menganggap Anda sudah mengerti dan menyetujui segala Kebijakan Privasi yang tertera di halaman website kami saat itu juga.

Hukum yang berlaku

Apabila Anda mengakses website ini di luar Indonesia, maka atas penggunaan website dan layanan yang kami berikan akan tunduk dan diatur oleh dan atau ditafsirkan sesuai dengan hukum yang berlaku di Indonesia, tanpa mempengaruhi adanya prinsip-prinsip konflik hukum yang berlaku.



Persetujuan

Dengan membaca seluruh ketentuan ini, mengakses dan menggunakan Situs TRAC  serta layanan kami, maka Anda dianggap menyatakan telah membaca, memahami, menyetujui dan memberikan seluruh persetujuan lain yang diperlukan, dan menyatakan tunduk pada aturan privasi ini beserta perubahan-perubahan yang mungkin kami lakukan dari waktu ke waktu. Apabila Anda tidak dapat menyetujui aturan privasi ini, baik secara keseluruhan ataupun sebagian, maka Anda tidak diperbolehkan untuk mengakses Situs TRAC ini ataupun menggunakan layanan yang kami sediakan.



Pembatasan Tanggung Jawab.

Selaku pengguna Anda setuju bahwa Anda memanfaatkan Layanan TRAC atas risiko Anda sendiri, dan Layanan TRAC diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA", oleh karena itu Anda setuju untuk tidak menuntut TRAC bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung dari:

Penggunaan atau ketidakmampuan Anda dalam menggunakan Layanan TRAC;
Kelalaian Anda untuk menjaga username dan password yang diberikan TRAC dalam penggunaan Situs TRAC;
Penyalahgunaan Token TRAC termasuk akibat penyalahgunaan kartu kredit Anda oleh pihak lain;
ketidakakuratan dan ketidakabsahan Anda dalam melakukan pengisian data-data yang diperlukan untuk menggunakan Situs TRAC.


Penghapusan Data Pribadi

Apabila menurut pendapat TRAC Anda sudah tidak aktif sebagai pengguna atau atas pertimbangan lain dari TRAC, maka TRAC dapat memblokir akses Anda terhadap Situs TRAC dan/atau menghapus Data Pribadi Anda yang disimpan oleh TRAC dengan terlebih dahulu memberikan pemberitahuan tertulis baik secara manual atau elektronik kepada Anda atas penghapusan atau pemblokiran tersebut.



Bantuan dan pertanyaan

Jika Anda memiliki pertanyaan atau membutuhkan bantuan dan atau informasi lebih lanjut mengenai reservasi Anda atau layanan kami, Anda dapat menghubungi Customer Service TRAC di nomor (021) 877 877 87 atau dapat mengirimkan email ke rco.nasional@trac.astra.co.id.`
    },
    {
        category: "KEBIJAKAN PRIVASI",
        question: "Token TRAC",
        answer: `Penggunaan Token TRAC

Token TRAC adalah suatu fitur atau fasilitas yang kami sediakan untuk Anda agar dapat melakukan proses pembayaran dengan menggunakan kartu kredit dengan aman, mudah dan cepat. Fitur dan fasilitas ini dilengkapi dengan sistem keamanan berlapis yang terintegrasi dengan payment gateway agar data dan informasi yang Anda berikan terjamin kerahasiaannya. Selain untuk menjamin kerahasiaan data dan informasi, fitur ini juga berfungsi untuk mempermudah dan mempercepat transaksi yang Anda lakukan dengan menggunakan kartu kredit.

Pada saat Anda melakukan pembayaran dengan kartu kredit, Anda akan diminta untuk mengisi data-data kartu kredit Anda secara benar, jelas, lengkap, akurat dan tidak menyesatkan, seperti data jenis kartu (Visa/MasterCard), nama pemegang kartu, nama bank penerbit kartu kredit, nomor kartu (Hanya 4 digit terakhir yang tampil), dan tanggal habis berlaku kartu (expired date).

Setiap data yang Anda berikan tersebut akan tersimpan secara otomatis ke dalam sistem database payment gateway yang kami gunakan (TRAC tidak secara langsung menyimpan database kartu kredit Anda, tapi tersimpan ke dalam sistem database payment gateway yang kami gunakan) agar memudahkan dan mempercepat proses pembayaran Anda pada transaksi selanjutnya. Setelah transaksi pertama menggunakan kartu kredit, seluruh database Anda telah tersimpan dengan lengkap, aman, dan tidak dibutuhkan lagi mengulang proses penyimpanan untuk data kartu kredit yang sama.`
    },
    {
        category: "KEBIJAKAN PRIVASI",
        question: "Cookie",
        answer: `Penggunaan cookies

Cookies adalah file teks yang mengandung sejumlah kecil informasi yang diunduh ke perangkat Anda saat Anda mengunjungi sebuah website. Kami menggunakan cookie untuk berbagai alasan, seperti untuk melacak data Anda jika dibutuhkan untuk digunakan, pelacakan transaksi Anda dengan mitra atau rekanan kami, mengingat preferensi Anda dan pada umumnya meningkatkan pengalaman Anda saat menggunakan sebuah website. Anda dapat mengatur browser Anda untuk menerima atau tidak menerima suatu cookies.

TRAC menjalin kerja sama dengan pihak ketiga untuk membantu dalam mempromosikan dan membuat iklan yang menarik untuk Anda berdasarkan penggunaan Situs TRAC yang dimana Kami simpulkan dari data-data yang Anda gunakan pada website kami. Perusahaan-perusahaan yang bekerja sama dengan kami juga mungkin menggunakan cookies untuk membantu melacak perilaku pelanggan. Perusahaan tersebut mungkin menggunakan informasi Anda untuk mengungkap tentang perilaku pengguna untuk menyediakan iklan yang disesuaikan di berbagai layanan dan produk. Dalam menyediakan produk dan layanan Masing-masing perusahaan menggunakan cookies sesuai dengan informasi privasi yang juga sejalan dengan kebijakan privasi dan keamanan masing-masing perusahaan.`
    }
]

export default privacyPolicy;