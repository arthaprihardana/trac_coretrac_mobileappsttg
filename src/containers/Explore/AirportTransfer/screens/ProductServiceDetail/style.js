import {
	appVariables,
} from "@variables";

export default {
	bannerPromoWrapper:{
		flex: 1,
		flexDirection: "column",
		paddingBottom: 20,
	},
	promoDescription:{
		wrapper:{
			flex: 1,
			flexDirection: "column",
			paddingVertical: 10,
			paddingHorizontal: 20,
		},
		wrapperbox:{
			flex: 1,
			flexDirection: "column",
			paddingVertical: 0,
			paddingHorizontal: 0,
			borderWidth: 0,
			margin: 20,
			backgroundColor: 'white',
			shadowOffset:{  width: 0,  height: 0,  },
			shadowColor: 'black',
			shadowOpacity: 0.5,
			borderRadius: 10
		},
		descriptionWrapper:{
			flex: 1,
			flexDirection: "row",
			paddingVertical: 0,
		},
	},
	copyPromo:{
		wrapper:{
			flex: 1,
			flexDirection: "column",
			paddingVertical: 20,
			paddingHorizontal: 20,
		},
		wrapperBackground:{
			flex: 1,
			flexDirection: "column",
			backgroundColor: "#2246A8",
			paddingVertical: 20,
			paddingHorizontal: 20,
		},
		trackPromoWrapper:{
			flex: 2,
			flexDirection: "column",
			justifyContent: "flex-end",
			alignItems: "center",
			marginBottom: 5,
		},
		trackPromoBackground:{
			backgroundColor: appVariables.colorWhite,
			opacity: 0.1,
			position: "absolute",
			left: 0,
			top: 0,
			width: '100%',
			height: 45,
			borderRadius: 5,
		},
		copyPromoButton:{
			flex: 1,
			flexDirection: "column",
			justifyContent: "flex-end",
			alignItems: "center",
			borderRadius: 5,
			backgroundColor: appVariables.colorOrange,
			paddingVertical: 10,
			paddingHorizontal: 10,
		},
	},
	otherPromo:{
		title:{
			flex: 1,
			flexDirection: "row",
			paddingVertical: 20,
			paddingHorizontal: 20,
		},
	},
	otherPromoCard:{
		wrapper:{
			flex: 1,
			flexDirection: "column",
			paddingVertical: 10,
			paddingHorizontal: 20,
		},
		title:{
			flex: 1,
			flexDirection: "row",
			paddingVertical: 10,
		},
	},
	lineSeparator:{
		flex: 1,
		flexDirection: "row",
		borderBottomWidth: 1,
		borderBottomColor: "#D8D8D8",
		paddingVertical: 20,
	},
}