/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-06 13:14:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 20:47:21
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Clipboard,
  ImageBackground,
} from "react-native";
import { H1, H2, p } from 'native-base';
import {
  appVariables,
  appMetrics
} from "@variables";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import ImageHeader from "@airport-transfer-fragments/ImageHeader";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import Icon from "@airport-transfer-fragments/Icon";
import { Actions} from "react-native-router-flux";

export default class ProductServiceDetail extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
    constructor(props) {
      super(props);
      this.state = {
        activeTab: 0,
        activeContent: this.props.item.detail.product.length > 0 ? this.props.item.detail.product[0].description : null
      };
    }
    
  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------
    _renderContent = (index) => {
      this.setState({
        activeContent: this.props.item.detail.product.length > 0 ? this.props.item.detail.product[index].description : null
      })
    }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  _copy = (code) => {
    Clipboard.setString(code);
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderBannerPromo() {
    return (
      <View style={styles.bannerPromo}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <Image
            source={require('@images/png/bannerContent/bannerService.png')}
            style={{ height: 250, width: appMetrics.windowWidth }}
            resizeMode="stretch" />
        </View>
      </View>
    );
  }
 
  // ---------------------------------------------------
  _renderIntro() {
    return (   
      <View style={styles.promoDescription.wrapper}>
        <Text style={{color: "grey", lineHeight: 24, fontWeight: "200", fontSize: 16 }}>
          {this.props.item.detail.description}
        </Text>
      </View>
    );
  }
  
  // ---------------------------------------------------
  _renderProduct(value, index) {
    let icon = this.state.activeTab === index ? require('@images/png/bannerContent/icoOpen.png') : require('@images/png/bannerContent/icoClose.png');
    return (      
      <View key={index}>
        <View style={styles.promoDescription.wrapperbox}>
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity
              style={{
                flex: 1,
                flexDirection: 'row',
                marginHorizontal:20,
                marginVertical: 20
              }}
              onPress={() => this.setState({ activeTab: index, activeContent: value.description })}>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: "center",
                justifyContent: "space-between"}}>
                <View  style={{flex: 1}}>
                  <Image             
                    source={value.icon}
                    style={{ width: 44, height: 20 }}
                    resizeMode="contain" />
                  </View>
                <View  style={{flex: 8}}>
                  <Text style={{marginHorizontal: 20, fontWeight: '400'}} size={20}>
                    {value.title}
                  </Text>
                </View>
                <View  style={{flex: 1}}>
                  <Image
                    //source={require('@images/png/bannerContent/icoOpen.png')}
                    source={icon}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {this.state.activeTab === index && <View style={styles.promoDescription.wrapper}>
          <Text style={{color: "grey", lineHeight: 24, fontWeight: '200', fontSize: 16 }}>
            {this.state.activeContent}
          </Text>

          {value.subProduct.length > 0 && value.subProduct.map((v, k) => <View key={k} style={{ marginTop: 10 }}>
            <Text style={{ fontWeight: '500', fontSize: 16 }}>{v.title}</Text>
            <Text style={{color: "grey", lineHeight: 24, fontWeight: '200', fontSize: 16, marginTop: 10 }}>
              {v.description}
            </Text>
          </View>)}
        </View> }
        {/* {this._renderContent(index)} */}
      </View>
    );
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <ImageBackground source={ require("@images/png/img-top-background.png") } style={{width: '100%'}}>
          <ImageHeader onBackPress={() => this.props.navigation.pop()} />
          <View style={{marginHorizontal: 20, marginTop: 70, marginBottom: 50}}>
            <Text style={{fontSize: 32, color: 'white', fontWeight: '600'}}>{this.props.item.detail.title}</Text>
          </View>
        </ImageBackground>
        <ScrollView style={{ flex: 1 }}>
          {this._renderIntro()}
          {this.props.item.detail.product.map((value, index) => this._renderProduct(value, index))}
          {/* {this._renderCarDetail()} */}
          {/* {this._renderDriverDetail()}
          {this._renderMotorDetail()}
          {this._renderBusDetail()} */}
        </ScrollView>
      </BaseContainer>
    );
  }
}
