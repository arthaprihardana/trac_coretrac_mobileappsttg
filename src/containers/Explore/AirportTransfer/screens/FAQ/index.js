/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-26 16:45:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 11:32:20
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import {
  appVariables,
} from "@variables";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import Icon from '@airport-transfer-fragments/Icon';
import styles from "./style";
// import { BoxShadow } from "react-native-shadow";
import faqList from './data';
import { connect } from "react-redux";
import Lang from '../../../../../assets/lang';

class FAQ extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      activeDropIndex: 0,
      activeButtonIndex: 0,
      activeCategory:"all",
      faqList: faqList,
      findText: '',
      search: false
    };
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  toggleDrop(index) {
    this.setState({
      activeDropIndex: index,
    });
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderFindATopic() {
    return (
      <View style={styles.searchBar.blueBox.wrapper}>
        <View style={styles.searchBar.blueBox.wrapper.textWrapper}>
          <Text
            size={18}
            color={appVariables.colorWhite}>
            {Lang.whatCanHelpYou[this.props.lang]}
          </Text>
        </View>
        <View style={styles.searchBar.blueBox.wrapper.findTopicBoxWrapper}>
          {this._renderFindTopicBox()}
        </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderFindTopicBox() {
    const shadowTopicBox = {
      width: 300,
      height: 70,
      color: "#000",
      border: 10,
      radius: 10,
      opacity: 0.2,
      style: {
        flex: 4,
        flexDirection: "row",
      }
    }

    return (
      <View style={styles.findTopicBox.wrapper}>
        {/* <BoxShadow setting={shadowTopicBox}> */}
          <View style={styles.findTopicBox.wrapper.findTopicTextArea}>
            <TextInput style={{ paddingVertical: 20, paddingHorizontal: 20, color: appVariables.colorGray, fontSize: 16 }} placeholder={Lang.findTopic[this.props.lang]} value={this.state.findText} onChangeText={text => this.setState({ findText : text, search: false })} />
          </View>
        {/* </BoxShadow> */}
        <TouchableOpacity style={styles.findTopicBox.wrapper.searchIconWrapper} onPress={() => this.setState({ search: true, activeCategory: "all", activeButtonIndex: 0, activeDropIndex: 0 }) }>
          <Icon
            size={20}
            color={appVariables.colorWhite}
            name={"search"}
            style={{
              paddingVertical: 25,
              paddingHorizontal: 25,
            }}>
          </Icon>
        </TouchableOpacity>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderListTopicButton() {
    let categories = _.keys(_.keyBy(this.state.faqList, 'categoryName'));
    let self = this;
    return (
      <ScrollView horizontal style={styles.listTopicButton.wrapper} showsHorizontalScrollIndicator={false}> 
        { categories.map((category, index) => (
            <View key={index} style={styles.listTopicButton.buttonSeparator}>
              {this._renderButton( category, self.state.faqList[index].category ,index)}
            </View>
          ))
        } 
      </ScrollView>
    );
  }
  // ---------------------------------------------------
  _renderButton(label, value, index) {
    return (
      <TouchableOpacity
        style={[styles.inactiveButtonStyle,
        this.state.activeButtonIndex == index ? { backgroundColor: "#F27C21" } : {}]}
        onPress={() => this.setState({ search: false, activeButtonIndex: index, activeCategory:value, activeDropIndex:0, findText: '' })}>
        <Text size={14} color={appVariables.colorWhite} style={{ textAlign: "center" }}>
          {label}
        </Text>
      </TouchableOpacity>
    );
  }
  // ---------------------------------------------------
  _renderExpandableMenuTitle(label) {
    return (
      <View style={styles.expandableViewTitle}>
        <Text size={20}>
          {label}
        </Text>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderDropdown(label, content, index) {
    const isActive = this.state.activeDropIndex == index;
    let contentRender = null;
    if (isActive) {
      contentRender = this._renderContent(content);
    }

    return (
      <View style={styles.dropdown.container} key={index}>
        <TouchableOpacity style={styles.dropdown.label.container}
          onPress={() => this.toggleDrop(index)}>
          <View style={styles.dropdown.label.textContainer}>
            <Text size={16}>
              {label}
            </Text>
          </View>

          <View style={styles.dropdown.label.iconContainer}>
            <Icon size={20} name={isActive ? "caret-up" : "caret-down"} />
          </View>
        </TouchableOpacity>

        {contentRender}

      </View>
    )
  }

  // ---------------------------------------------------

  _renderContent(content) {
    return (
      <View style={styles.dropdown.content.container}>
        <Text color={appVariables.colorGray}>
          {content}
        </Text>
      </View>
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    let self = this;
    return (
      <BaseContainer>
        <Header title={Lang.faq[this.props.lang]} onBackPress={() => this.props.navigation.pop()} />
        <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
          {this._renderFindATopic()}
          {this._renderListTopicButton()}
          {this._renderExpandableMenuTitle(this.state.faqList[this.state.activeButtonIndex].categoryName)}
          {
            this.state.search ? _.map(_.filter(_.flatten(_.map(this.state.faqList, v => v.content )), o => o.question.match(new RegExp(this.state.findText, 'g'))), (v,k) => this._renderDropdown(v.question, v.answer, k) ) :
              this.state.activeCategory === "all" ? _.map(_.flatten(_.map(this.state.faqList, v => v.content )), (v,k) => self._renderDropdown(v.question, v.answer, k) )
              : _.map(_.filter(this.state.faqList, d => d.category === this.state.activeCategory)[0].content, (v,k) => self._renderDropdown(v.question, v.answer, k) )
          }
        </ScrollView>
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(FAQ);
