export default [
    {
        "category": "all",
        "categoryName": "ALL",
        "content": []
    },
    {
        "category": "umum",
        "categoryName": "PERTANYAAN UMUM",
        "content": [
            {"question":"Siapa TRAC?","answer":"Kami merupakan perusahaan yang bergerak di bidang solusi jasa transportasi sejak 1986. Kami menawarkan beragam layanan transportasi yang dapat digunakan untuk kebutuhan personal ataupun korporat."},
            {"question":"Apa saja layanan yang disediakan oleh TRAC?","answer":"TRAC menyediakan layanan Rental Mobil Harian, Airport transfer, Sewa Bus, Rental Mobil Korporasi, dan TRAC Fleet Management Solution. "},
            {"question":"Apa keuntungan bagi saya jika menggunakan layanan TRAC?","answer":"Keamanan dan kenyamanan Anda terjamin dari awal hingga akhir masa penggunaan layanan. Anda bisa menikmati reservasi online yang mudah melalui website dan aplikasi TRAC, jaminan kondisi kendaraan prima, asuransi perjalanan, layanan pelanggan selama 24 jam setiap hari, kendaraan pengganti jika terjadi kendala selama masa sewa. Kami juga bermitra dengan bengkel-bengkel resmi yang memiliki jaringan nasional."},
            {"question":"Di mana saja cabang dan outlet TRAC di Indonesia?","answer":"Keamanan dan kenyamanan Anda terjamin dari awal hingga akhir masa penggunaan layanan. Anda bisa menikmati reservasi online yang mudah melalui website dan aplikasi TRAC, jaminan kondisi kendaraan prima, asuransi perjalanan, layanan pelanggan selama 24 jam setiap hari, kendaraan pengganti jika terjadi kendala selama masa sewa. Kami juga bermitra dengan bengkel-bengkel resmi yang memiliki jaringan nasional."},
            {"question":"Di mana saja saya bisa menggunakan layanan TRAC?","answer":"TRAC telah memiliki lebih dari 50 jaringan cabang, service point dan outlet yang ada di berbagai kota di Indonesia : di antaranya di Jakarta, Bandung, Semarang, Yogyakarta, Solo, Surabaya, Malang, Bali, Lombok, Banjarmasin, Balikpapan, Manado, Makassar, Pontianak, Lampung, Palembang, Medan, Padang, Pekanbaru, Batam, dan Jambi"},
            {"question":"Bagaimana cara mengakses layanan TRAC?","answer":"Anda dapat melakukan reservasi layanan Rental Mobil Harian, Airport transfer, Sewa Bus secara online via situs web dan aplikasi TRAC yang dapat diunduh di App Store dan Play Store atau via Customer Assistance Center 1500009 atau dengan datang langsung ke cabang dan outlet TRAC. Khusus layanan Rental Mobil Korporasi dan TRAC Fleet Management Solution dapat menghubungi Customer Assistance Center."},
            {"question":"Jenis kendaraan apa saja yang disewakan TRAC?","answer":"TRAC menyewakan mobil, motor, dan bus dengan beragam kapasitas dengan beragam pilihan kendaraan jenis kendaraan di antaranya city car, MPV, SUV, single/double cabin, commercial car dan beragam tipe Bus."},
            {"question":"Ke mana saya bisa menghubungi TRAC?","answer":" \n- Telp 1500009\n- WA 08111177087\n- WA 08111177087 \n- Medsos : IG (trac astra), FB (trac Astra)\n- Web : www.trac.astra.co.id \n- Walk in ke kantor cabang TRAC di 18 kota, Serta di outlet bandara TRAC di kota jogja, semarang, bali, dan Palembang."},
            {"question":"Apakah saya bisa menitipkan kendaraan saya kepada TRAC untuk disewakan?","answer":"Saat ini kami belum menerima penitipan kendaraan untuk disewakan kembali."},
            {"question":"Apakah saya bisa menyewa jasa pengemudi TRAC saja, tanpa menyewa kendaraan?","answer":"Jasa pengemudi TRAC hanya bisa digunakan bersamaan dengan layanan sewa kendaraan dari TRAC."},
            {"question":"Apakah saya bisa menyetir mobil sendiri untuk mobil yang saya sewa di TRAC?","answer":"Bisa, namun dengan mengikuti ketentuan yang berlaku di TRAC : \n - Memiliki SIM A \n - Memikili KTP \n - Informasi data diri : nama, alamat tempat tinggal, email, no HP"},
            {"question":"Saya warna negara asing, apakah saya bisa menggunakan layanan TRAC?","answer":"Bisa, namun dengan mengikuti ketentuan yang berlaku di TRAC : \n- Memberikan copy passport \n- Memiliki SIM A international (untuk sewa selfdrive), khusus untuk bali memiliki SIM negaranya tidak perlu sim international \n- Informasi data diri : nama, email, no hp"},
            {"question":"Bagaimana cara saya menghubungi TRAC jika memiliki pertanyaan, saran, atau kendala?","answer":"- Telp 1500009\n- WA 08111177087\n- Email : rco.nasional@trac.astra.coid\n- Medsos : IG (trac_astra), FB (trac Astra)"}
        ]
    },
    {
        "category": "website",
        "categoryName": "WEBSITE & AKUN TRAC",
        "content": [
            {"question":"Apa saja layanan TRAC yang bisa diakses melalui website?","answer":"Anda dapat melakukan reservasi layanan Rental Mobil Harian, Airport transfer, dan Sewa Bus, serta mencari semua informasi produk dan layanan TRAC, sekaligus informasi terbaru seputar event TRAC, tren rental mobil, juga tips, ulasan wisata, dan informasi menarik lainnya."},
            {"question":"Apa saja fitur yang terdapat di website TRAC?","answer":"1. Layanan sewa mobil dan bus\n2. Blog dan artikel\n3. Filter harga dan tipe kendaraan \n4. Detail informasi unit dan asuransi\n5. Add on\n6. Kolom kode promo\n7. Pembayaran melalui VA"},
            {"question":"Apakah saya bisa menyewa mobil via website?","answer":"Ya, Bisa"},
            {"question":"Bagaimana cara melakukan reservasi mobil melalui website?","answer":"Pada beranda website TRAC www.trac.astra.co.id, pilih layanan yang Anda inginkan (Rental Mobil, Airport transfer, atau Sewa Bus), lalu masukkan informasi sesuai kebutuhan Anda, di antaranya alamat penjemputan dan tujuan, tanggal pemakaian, dan pilihan paket rental. Setelah itu Anda akan dibawa ke laman hasil pencarian di mana Anda dapat memilih jenis mobil dan layanan tambahan sesuai kebutuhan. Selanjutnya, Anda tinggal mengikuti tahapan reservasi hingga pembayaran. Anda dapat memilih pembayaran dengan kartu kredit atau Vitrual Account. Ketika pembayaran berhasil, Anda akan mendapatkan konfirmasi reservasi melalui email yang terdaftar pada akun TRAC Anda"},
            {"question":"Apakah saya harus memiliki akun TRAC untuk melakukan reservasi mobil?","answer":"Ya, anda harus melakukan log in untuk dapat menggunakan layanan TRAC melalui website"},
            {"question":"Jika saya tidak memiliki akun TRAC, apakah saya bisa melakukan reservasi melalui channel lain?","answer":"Ya, anda dapat melakukan reservasi melalui telepon di nomor 1500-009 dan (WA) Whatsapp 08111177087"},
            {"question":"Apakah saya bisa menggunakan layanan Rental Mobil Korporasi dan TRAC Fleet Management Solution melalui website?","answer":"Saat ini layanan tersebut hanya dapat diakses secara offline dengan menghubungi Customer Assistance Center 1500009."},
            {"question":"Apakah ada biaya untuk melakukan registrasi di website TRAC?","answer":"Tidak ada"},
            {"question":"Bagaimana cara melakukan registrasi di website?","answer":"1. Setelah masuk ke web trac.astra.co.id, pilih menu log in/sign up\n2. Pilih cara pendaftaran, menggunakan google account atau manual dengan mendaftarkan email.\n3. Verifikasi email anda\n4. Berhasil Log in"},
            {"question":"Saya adalah warga negara asing, apakah bisa saya bisa melakukan registrasi di website TRAC?","answer":"Ya, bisa"},
            {"question":"Apakah saya perlu melakukan verifikasi akun setelah registrasi?","answer":"Ya, perlu memverifikasi di email yang didaftarkan"},
            {"question":"Bagaimana jika saya lupa kata sandi?","answer":"Pilih menu “lupa kata sandi” dan masukan email anda, sandi akan dikirimkan ke email anda."},
            {"question":"Apa yang harus saya lakukan jika tidak menerima email verifikasi akun?","answer":"Hubungi costumer service TRAC dan sampaikan bahwa anda tidak menerima email verifikasi"},
            {"question":"Bagaimana cara mengubah data pada akun saya?","answer":"1. Pilih menu “Pengaturan Akun”\n2. Pilih menu “Profil Saya”\n3. Ubah data sesuai keperluan"},
            {"question":"Apakah saya bisa mengubah alamat email yang terdaftar di akun saya?","answer":"Tidak bisa"}
        ]
    },
    {
        "category": "carRental",
        "categoryName": "DAILY CAR RENTAL",
        "content": [
            {"question":"Apa itu Rental Mobil Harian dan apa saja pilihan layanannya?","answer":"Daily car rental adalah layanan sewa kendaraan jangka pendek baik dengan driver maupun tanpa driver. Saat ini paket yg kami miliki adalah :\n 1. Dengan driver : sewa 4 jam, 12 jam, mingguan, dan monthly \n 2. Tanpa driver : daily (minimal 24 jam) \n\n Jika ada kebutuhan tambahan ada layanan extra namun akan dikenakan biaya tambahan : \n - Additional hours atau biaya tambahan jam jika pemakaian melebihi batas waktu \n -Trip (out of town) atau biaya keluar kota jika menggunakan layanan driver\n -OLC atau biaya inap jika menggunakan layanan driver dan perlu menginap\n -Ekspedisi atau biaya pengantaran dan pengambilan unit jika customer tidak mengambil unit di cabang/outlet TRAC"},
            {"question":"Di mana saja saya bisa menggunakan Layanan Rental Mobil Harian ini?","answer":"Layanan Rental Mobil Harian TRAC mencakup 19 kota, yaitu Jakarta, Bandung, Surabaya, Malang, Yogyakarta, Semarang, Medan, Pekanbaru, Palembang, Lampung, Jambi, Balikpapan, Sangatta, Banjarmasin, Makassar, Manado, Denpasar, Lombok, Batam."},
            {"question":"Bagaimana cara melakukan reservasi layanan Daily Car Rental ini?","answer":"Offline dengan menghubungi dibawah ini : \n - Telp 1500009 \n - WhatsApp 08111177087\n - Email : rco.nasional@trac.astra.coid\n - Medsos : IG (trac astra), FB (trac Astra)\n - Atau datang ke kantor cabang  TRAC di 18 kota\n\nOnline dengan melakukan pemesanan ke :\n - Web trac di www.astra.co.id \n - Aplikasi TRAC To Go (unduh di play store atau apple store)"},
            {"question":"Apa saja syarat untuk melakukan reservasi layanan Daily Car Rental?","answer":"Untuk layanan with driver : \n 1. KTP untuk WNI dan Passport untuk WNA \n 2. Email dan no HP \n\nUntuk layanan selfdrive :\n 1. KTP untuk WNI dan Passport untuk WNA \n 2. SIM A untuk WNI dan SIM International untuk WNA \n 3. Email dan no HP"},
            {"question":"Saya adalah warga negara asing, apakah saya bisa menggunakan layanan Daily Car Rental ini?","answer":"Bisa, namun dengan mengikuti ketentuan yang berlaku di TRAC :\n - Memberikan copy passport \n - Memiliki SIM A international (untuk sewa selfdrive), khusus untuk bali memiliki SIM negaranya tidak perlu sim international \n - Informasi data diri : nama, email, no hp"},
            {"question":"Mobil tipe apa saja yang bisa saya pilih?","answer":"Untuk Chaffeur(With Driver) \n- CITY CAR: AGYA* \n- LUXURY: ALPHARD*, ALTIS*, CAMRY*, FORTUNER* \n- MPV: AVANZA, INNOVA \n- COMMUTER: HIACE \n\nUntuk Self Drive \n- CITY CAR: AGYA* \n- LUXURY: ALTIS*, CAMRY*, FORTUNER* \n- MVP: AVANZA, INNOVA\n- COMMUTER: HIACE* \n\n Note : * hanya ada dikota tertentu dan untuk HI ace harus punya SIM B1"},
            {"question":"Apakah saya bisa mengganti tipe mobil yang telah dipilih?" , "answer" :"Bisa selama belum completed order dan melakukan pembayaran, jika mengganti unit sesudah completed order dan melakukan pembayaran maka TRAC tidak menjamin penggantian unit dapat dilakukan karena bergantung ketersediaan unit/availability unit yg diinginkan di TRAC. Untuk proses refund (jika downgrade unit) dan penambahan (jika upgrade unit) menyesuaikan kebijakan internal TRAC."},
            {"question":"Apakah saya bisa menggunakan mobil keluar kota sesuai dengan tujuan yang saya pilih?","answer":"Bisa, selama waktu pemakaian tidak melebihi paket rental yang dipilih sebelumnya. Namun,  jika Anda menggunakan Layanan Chauffeur, Anda akan dikenakan biaya tambahan trip keluar kota (dengan hitungan jarak lebih dari 100 km dari titik penjemputan) dan Overnight  Lodging Cost (jika menginap). Lebih lanjut, Anda dapat menghubungi Customer Assistance Center 1500009."},
            {"question":"Apakah saya bisa mengubah destinasi tujuan saya?","answer":"Bisa, selama waktu pemakaian tidak melebihi paket rental yang dipilih. Kelebihan durasipemakaian akibat perubahan destinasi tujuan akan dikenakan biaya tambahan per jam (add  hour). Meski demikian, Anda diminta untuk menginfokan perubahan tersebut paling lambat H-1 agar kami dapat menyiapkan kebutuhan Anda, terutama jika Anda menggunakan  Layanan Chauffeur. Kami juga akan menginfokan Anda jika destinasi baru tersebut membutuhkan biaya tambahan, seperti trip keluar kota atau Overnight Lodging Cost (jika menginap). Lebih lanjut, Anda dapat menghubungi Customer Assistance Center 1500009."},
            {"question":"Apakah saya bisa mengubah hari pemakaian sewa mobil?","answer":"Bisa, tapi Anda harus menginformasikan kepada kami paling lambat H-1. Jika ada penambahan hari dari reservasi awal, Anda akan dikenakan biaya tambahan yang harus dilunasi sebelum waktu pemakaian. Sebaliknya, pengurangan durasi sewa akan mengacu kepada ketentuan pembatalan dan refund, yaitu pembatalan pada hari H tidak bisa dilakukan refund, pembatalan 1-2 hari sebelum tanggal pemakaian refund 50%, sedangkan pembatalan lebih dari 2 hari sebelum tanggal sewa (&gt; H-2) mendapat refund 100% dari harga sewa. Lebih lanjut, Anda dapat menghubungi Customer Assistance Center 1500009."},
            {"question":"Apakah saya bisa melakukan sewa mobil pada hari yang sama dengan pemakaian ?","answer":"Bisa selama sesuai TnC TRAC yaitu 12 jam sebelum jam order. Atau jika sewa walk in ke cabang/outlet TRAC dan unit masih tersedia."},
            {"question":"Berapa jangka waktu maksimal saya dapat menyewa mobil dengan layanan Daily Car Rental di TRAC?","answer":"- Dengan driver : sewa 4 jam, 12 jam, mingguan, dan monthly \n- Tanpa driver : daily (minimal 24 jam) namun belum bisa monthly"},
            {"question":"Apakah saya bisa melakukan reservasi sewa mobil untuk untuk orang lain, misalnya keluarga atau teman?","answer":"Bisa, namun wajib menginformasikan data passenger  dan jika sewanya selfdrive wajib menginformasikan data diri pengambil unit dan pengemudinya wajib punya SIM A. Dokumen harus ditunjukan saat pengambilan unit (selfdrive)"},
            {"question":"Apakah saya bisa menyewa mobil di kota yang bukan tempat tinggal saya?","answer":"Bisa, dengan melengkapi persyaratan TRAC."},
            {"question":"Apakah mobil yang saya sewa bisa diantarkan ke tempat saya?","answer":"Bisa, namun akan dikenakan biaya ekspedisi."},
            {"question":"Dapatkah saya mengembalikan mobil sewaan ke cabang TRAC yang bukan merupakan tempat saya menyewa?","answer":"Bisa, namun akan dikenakan biaya ekspedisi."},
            {"question":"Apakah saya bisa menggunakan mobil melebihi batas waktu reservasi (over time)?","answer":"Bisa namun akan dikenanakan biaya additional hours."}
        ]
    },
    {
        "category": "aiport",
        "categoryName": "AIRPORT TRANSFER",
        "content": [
            {"question":"Apa itu layanan Airport Transfer dan perbedaan dengan Daily Car Rental?","answer":"Layanan yang sifatnya pick and drop dengan tujuan dari dan ke Bandara. Menggunakan system zona (km). layanan sudah paket all  in inc driver, bbm, tol, parkir."},
            {"question":"Bagaimana cara melakukan reservasi layanan ini?","answer":"Offline dengan menghubungi dibawah ini : \n - Telp 1500009 \n - WhatsApp 08111177087\n - Email : rco.nasional@trac.astra.coid\n - Medsos : IG (trac astra), FB (trac Astra)\n - Atau datang ke kantor cabang  TRAC di 18 kota atau di outlet bandara TRAC jogja, semarang, Bali, dan palembang\n\nOnline dengan melakukan pemesanan ke :\n - Web trac di www.astra.co.id \n - Aplikasi TRAC To Go (unduh di play store atau apple store)"},
            {"question":"Apa saja syarat untuk melakukan reservasi layanan Airport Transfer?","answer":"Memberikan persyaratan : KTP, EMAIL dan No HP"},
            {"question":"Saya adalah warga negara asing, apakah saya bisa menggunakan layanan Airport Transfer ini?","answer":"Bisa, namun dengan mengikuti ketentuan yang berlaku di TRAC : \n - Memberikan copy passport \n - Informasi data diri : nama, email, no hp"},
            {"question":"Apakah saya bisa memilih tipe mobil yang ingin saya gunakan?","answer":"Bisa, sesuai dengan ketersediaan unit di TRAC untuk layanan airport transfer"},
            {"question":"Apakah ada batas waktu untuk penggunaan layanan ini?","answer":"Untuk layanan airport transfer penggunaannya hanya sesuai titik jemput dan titik antar yg sudah dipilih diawal order karena menggunakan system zona."},
            {"question":"Apakah saya bisa mengubah destinasi tujuan saya?","answer":"Bisa, selama waktu pemakaian tidak melebihi paket rental yang dipilih. Kelebihan durasipemakaian akibat perubahan destinasi tujuan akan dikenakan biaya tambahan per jam (add  hour). Meski demikian, Anda diminta untuk menginfokan perubahan tersebut paling lambat H-1 agar kami dapat menyiapkan kebutuhan Anda, terutama jika Anda menggunakan  Layanan Chauffeur. Kami juga akan menginfokan Anda jika destinasi baru tersebut membutuhkan biaya tambahan, seperti trip keluar kota atau Overnight Lodging Cost (jika menginap). Lebih lanjut, Anda dapat menghubungi Customer Assistance Center 1500009."},
            {"question":"Apakah saya bisa mengubah hari pemakaian layanan?","answer":"Bisa, tapi Anda harus menginformasikan kepada kami paling lambat H-1. Jika ada penambahan hari dari reservasi awal, Anda akan dikenakan biaya tambahan yang harus dilunasi sebelum waktu pemakaian. Sebaliknya, pengurangan durasi sewa akan mengacu kepada ketentuan pembatalan dan refund, yaitu pembatalan pada hari H tidak bisa dilakukan refund, pembatalan 1-2 hari sebelum tanggal pemakaian refund 50%, sedangkan pembatalan lebih dari 2 hari sebelum tanggal sewa (&gt; H-2) mendapat refund 100% dari harga sewa. Lebih lanjut, Anda dapat menghubungi Customer Assistance Center 1500009."},
            {"question":"Apakah saya bisa melakukan reservasi layanan ini pada hari yang sama dengan pemakaian ?","answer":"Bisa selama sesuai TnC TRAC yaitu 12 jam sebelum jam order. Atau jika sewa walk in ke cabang/outlet TRAC dan unit masih tersedia."},
            {"question":"Bagaimana jika pengemudi terlambat menjemput saya di bandara?","answer":"Jam start order akan dihitung sejak user naik dan tidak dikenakan biaya tambahan, serta jika menyebabkan user tidak jadi naik maka tidak akan dikenakan cancelation fee."},
            {"question":"Bagaimana cara saya menghubungi pengemudi?","answer":"Setiap order yg sudah terbooking (sudah ada payment dari user) dari pihak TRAC akan selalu memberikan nama dan no HP pengemudi. Jika ada kendala user dapat menghubungi call centre 24 jam TRAC."}
        ]
    },
    {
        "category": "busRental",
        "categoryName": "BUS RENTAL",
        "content": [
            {"question":"Untuk keperluan apa saja saya bisa menggunakan layanan Sewa Bus?","answer":"Anda dapat memanfaatkan layanan ini untuk perjalanan rombongan, misalnya perjalanan bisnis bersama kolega, trip kantor atau sekolah, acara arisan, ulang tahun, reuni, dan lain-lain."},
            {"question":"Bagaimana cara melakukan reservasi online untuk layanan ini?","answer":"Untuk reservasi layanan ini Anda tinggal memilih layanan “Sewa Bus” pada halaman depan situs web TRAC www.trac.astra.co.id dan mengisikan detail informasi yang dibutuhkan seperti kota dan lokasi penjemputan, tujuan, serta tanggal dan waktu pemakaian. Setelah itu Anda akan dibawa ke laman hasil pencarian di mana Anda dapat memilih jenis bus dan layanan tambahan sesuai kebutuhan. Selanjutnya, Anda tinggal mengikuti tahapan reservasi hingga pembayaran. Anda dapat memilih pembayaran dengan kartu kredit atau Vitrual Account. Ketika pembayaran berhasil, Anda akan mendapatkan konfirmasi reservasi melalui email yang terdaftar pada akun TRAC Anda."},
            {"question":"Apa saja syarat untuk melakukan reservasi layanan Sewa Bus?","answer":"Setelah memiliki akun TRAC, Anda hanya perlu menyediakan dokumen KTP dan NPWP."},
            {"question":"Saya adalah warga negara asing, apakah saya bisa menggunakan layanan Bus Rental ini?","answer":"Bisa, Anda hanya perlu mendaftar akun TRAC secara online dan menyediakan dokumen paspor. Pembayaran dapat dilakukan melalui transfer atau kartu kredit."},
            {"question":"Saya perwakilan perusahaan, dapatkah saya menyewa bus untuk kebutuhan perusahaan saya?","answer":"Bisa, pelunasan di muka hanya membutuhkan persyaratan KTP dan NPWP, sedangkan jika Anda ingin menggunakan pembayaran dengan sistem TOP, Anda wajib melengkapi dokumen legalitas perusahaan dan membuat kontrak korporat terlebih dahulu."},
            {"question":"Apa saja tipe bus yang tersedia?","answer":"Big Bus : 59, 48, 45, 40 kursi \nMedium Bus : 18, 23, 29, 31, 35 kursi\nSmall Bus : 13, 15, 16 kursi\nLuxury Bus : 11 kursi"},
            {"question":"Apakah saya bisa mengganti tipe bus setelah reservasi?","answer":"Bisa secara offline dengan melakukan konfirmasi terlebih dulu paling lambat H-7 dari hari keberangkatan"},
            {"question":"Apakah saya bisa mengubah hari pemakaian sewa bus?","answer":"Bisa secara offline dengan melakukan konfirmasi terlebih dulu paling lambat H-7 dari hari keberangkatan"},
            {"question":"Apakah saya bisa menyewa bus tanpa pengemudi?","answer":"Anda dapat menyewa bus tanpa pengemudi untuk penyewaan small bus atau medium bus selama minimal satu tahun. Sementara itu, penyewaan bus harian sudah satu paket dengan layanan pengemudi."},
            {"question":"Apakah saya bisa menyewa bus untuk perjalanan menginap keluar kota?","answer":"Bisa, sesuai kebutuhan reservasi Anda"},
            {"question":"Berapa jangka waktu maksimal penyewaan bus? ","answer":"Tidak ada maksimum waktu penyewaan bus, Anda dapat menginformasikan terlebih dulu kebutuhan durasi penyewaan bus kepada kami"},
            {"question":"Apakah saya bisa menyewa bus di kota yang bukan tempat tinggal saya?","answer":"Bisa, titik penjemputan sewa bus adalah area Jabodetabek dan Surabaya"},
            {"question":"Apakah ada tambahan biaya jika saya memesan dari kota yang jaraknya cukup jauh dari pool bus TRAC?","answer":"Ya, tapi ada biaya pengiriman bus (ekspedisi) dari pool terdekat kami ke area penjemputan yang berlokasi di luar Jakarta atau Surabaya"},
            {"question":"Kota apa saja yang sudah terjangkau layanan Bus Rental?","answer":"Titik penjemputan sewa bus adalah area Jabodetabek dan Surabaya (termasuk Bali dan Yogyakarta)"},
            {"question":"Dapatkan saya menyewa bus untuk tujuan luar pulau Jawa?","answer":"Bisa, jangkauan layanan sewa bus TRAC adalah Pulau Sumatera, Jawa, dan Bali."}
        ]
    },
    {
        "category": "wisata",
        "categoryName": "PAKET WISATA",
        "content": [
            {"question":"Apa saja benefit yang ditawarkan dari Paket Wisata TRAC?","answer":" - Paket all in sudah termasuk unit, driver, bbm, tol, parkir, itinerary \n - Kunjungan ke lokasi2 yang menarik \n - Driver yg handal dan memahami lokasi \n - Garansi kendaraan dan layanan yang berkualitas (Astra Guarantee) \n - Tersedia paket yang sudah ada di TRAC dan dapat customized sesuai kebutuhan customer"},
            {"question":"Apakah paket wisata bersifat open trip?","answer":"Untuk saat ini kami belum menyediakan layanan  open trip"},
            {"question":"Bagaimana cara melakukan reservasi layanan ini?","answer":"Untuk saat ini apabila ada kebutuhan reservasi paket wisata baru tersedia secara offline dengan : : \n - Telp 1500009 \n - WhatsApp 08111177087\n - Email : rco.nasional@trac.astra.coid\n - Medsos : IG (trac astra), FB (trac Astra)\n - Atau datang ke kantor cabang  TRAC di 18 kota atau di outlet bandara TRAC jogja, semarang, Bali, dan palembang\n Untuk kebutuhan customized akan dibantu oleh PIC TRAC di kota terkait."},
            {"question":"Apakah saya bisa mengajukan penggantian destinasi wisata dari paket wisata yang sudah ada?","answer":"Bisa selama belum completed order dan belum melakukan pembayaran. Untuk penggantian destinasi wisata agar diinfokan setidaknya H-7 sebelum jalan order dan jika ada perubahan jadwal maka guarantee unit mengikuti availability unit yg ada di kota tersebut."},
            {"question":"Apakah saya masih bisa menggunakan layanan ini jika jumlah orang tidak sesuai dengan syarat minimal?","answer":"Bisa, namun akan kena biaya tambahan dikarenakan minimal pax tidak terpenuhi."}
        ]
    },
    {
        "category": "mobilKorporasi",
        "categoryName": "RENTAL MOBIL KORPORASI",
        "content": [
            {"question":"Apa saja layanan yang ditawarkan dalam Rental Mobil Korporasi?","answer":"TRAC menawarkan layanan khusus kebutuhan penyewaan motor, mobil, bus serta layanan pengemudi untuk perusahaan, termasuk layanan lain seperti TRAC Syariah. Selengkapnya Anda dapat membaca produk dan layanan TRAC secara lengkap di sini (hyperlink ke page produk & layanan TRAC)."},
            {"question":"Apakah perusahaan saya dapat menggunakan layanan pengemudi saja?","answer":"Tidak bisa. Jasa pengemudi TRAC hanya bisa digunakan bersamaan dengan layanan sewa kendaraan dari TRAC"},
            {"question":"Sektor binis apa saja yang dapat menggunakan layanan ini?","answer":"TRAC dapat melayani jenis bisnis apapun. Kami siap melayani berbagai kebutuhan transportasi perusahaan Anda."},
            {"question":"Apa syarat untuk menggunakan layanan ini?","answer":"Layanan ini hanya dapat digunakan oleh konsumen perusahaan untuk jangka waktu kerja sama minimal satu tahun."},
            {"question":"Saya belum tahu pilihan layanan yang cocok untuk bisnis saya, apa yang harus saya lakukan?","answer":"Anda dapat menghubungi Customer Assistance Center 1500009"},
            {"question":"Bisnis saya masih skala kecil (UKM), apakah saya bisa menggunakan layanan ini?","answer":"Bisa. TRAC dapat melayani bisnis dengan berbagai skala"},
            {"question":"Berapa lama jangka waktu minimum kerja sama untuk Rental Mobil Korporasi?","answer":"Minimal satu tahun"},
            {"question":"Apakah saya bisa menggunakan layanan Rental Mobil Korporasi dalam jangka waktu kurang dari ketentuan minimal?","answer":"Jika jangka waktu kebutuhan Anda kurang dari satu tahun, kami menyarankan Anda untuk menggunakan layanan Rental Mobil Harian untuk korporasi"},
            {"question":"Apakah saya bisa menggunakan beberapa layanan yang tersedia dalam Rental Mobil Korporasi secara bersamaan?","answer":"Bisa. Anda dapat menggabungkan beberapa layanan TRAC sesuai dengan kebutuhan perusahaan Anda"},
            {"question":"Apa yang harus saya lakukan jika membutuhkan informasi lebih lanjut mengenai layanan ini?","answer":"Anda dapat menghubungi Customer Assistance Center 1500009 atau datang langsung ke cabang TRAC terdekat."}
        ]
    },
    {
        "category": "pembayaran",
        "categoryName": "PEMBAYARAN",
        "content": [
            {"question":"Metode pembayaran apa saja yang diterima TRAC?","answer":"Transfer melalui bank Permata dan Mandiri menggunakan Virtual Account atas nama PT Serasi Autoraya."},
            {"question":"Apakah saya harus membayar uang muka?","answer":"Tidak ada pembayaran uang muka untuk layanan TRAC, semua layanan dibayar penuh terlebih dulu sebelum digunakan."},
            {"question":"Bagaimana jika saya ingin mengganti jenis kendaraan padahal saya telah melakukan pembayaran?","answer":"Apabila ingin mengganti kendaraan, Anda terlebih dahulu harus membatalkan reservasi lalu membuat reservasi baru dengan pilihan kendaraan yang diinginkan. Setelah itu, Anda dapat menghubungi Customer Assistance Center 1500009 untuk proses refund."},
            {"question":"Bagaimana jika saya sewaktu-waktu ingin menambah waktu penyewaan di luar reservasi saya?","answer":"Untuk penambahan waktu penyewaan layanan, Anda dapat menginformasikan Customer Assistance Center 1500009."},
            {"question":"Bagaimana metode pembayaran untuk biaya tambahan waktu sewa tersebut?","answer":"Metode pembayaran untuk tambahan waktu sewa yang tidak tercatat dalam reservasi awal dapat dilakukan langsung melalui mesin EDC (oleh staf TRAC) atau pembayaran melalui transfer ke nomor rekening TRAC. Kami akan menghubungi Anda lebih lanjut untuk informasi jumlah pembayaran tambahan ini."},
            {"question":"Jika saya terlambat mengembalikan mobil, apakah saya terkena biaya tambahan?","answer":"Untuk Rental Mobil Self Drive, pengembalian mobil harus dilakukan paling lambat 15 menit setelah masa sewa berakhir. Jika waktu keterlambatan pengembalian melebihi batas waktu  ini, Anda akan dikenakan biaya tambahan yang dihitung per jam (add hour) dengan jumlah biaya bergantung pada jenis mobil yang Anda gunakan."},
            {"question":"Bagaimana metode pembayaran untuk biaya tambahan waktu sewa?","answer":"Transfer melalui bank Permata atau Mandiri setelah mendapatkan konfirmasi perpanjangan sewa dari costumer service TRAC "},
            {"question":"Jika saya ingin membatalkan reservasi, seperti apa prosedurnya?","answer":"Pembatalan reservasi hanya dapat dilakukan melalui aplikasi TracToGo. Di dalam aplikasi terdapat tombol “cancel order” pada halaman “My Booking”"},
            {"question":"Jika saya telah membatalkan reservasi setelah melakukan pembayaran, apakah saya akan mendapatkan kembali uang saya (refund)?","answer":"Pembatalan maksimal H-3 uang dikembalikan 100%\nPembatalan maksimal H-1 uang dikembalikan 50%\nPembatalan kurang dari 24 Jam Uang tidak dapat dikembalikan"},
            {"question":"Seperti apa prosedur refund di TRAC?","answer":" 1. Hubungi costumer service TRAC dan berikan konfirmasi atas pembatalan sewa dengan menyebutkan nomor booking order\n 2. Costumer service akan merubah status pesanan anda pada system menjadi “cancel order”\n 3. System akan membaca transaksi atas nomor booking order anda \n 4. Dalam waktu 14 hari kerja uang anda akan kembali ke rekening yang melakukan pembayaran atas nomor booking order anda \n 5. Dalam waktu 14 hari kerja uang anda akan kembali ke rekening yang melakukan pembayaran atas nomor booking order anda"}
        ]
    },
    {
        "category": "fms",
        "categoryName": "ASTRA FMS",
        "content": [
            {"question":"Apa itu TRAC Fleet Management Solution (TRAC FMS)?","answer":"TRAC FMS adalah solusi komperhensif dalam pengelolaan kendaraan dan transportasi pertama di Indonesia yang berbasis teknologi informasi, khusus untuk konsumen perusahaan."},
            {"question":"Sektor binis apa saja yang dapat menggunakan layanan ini?","answer":"Semua sektor bisnis dapat memanfaatkan layanan ini."},
            {"question":"Apa syarat untuk menggunakan layanan ini?","answer":"Layanan ini hanya dapat digunakan oleh konsumen perusahaan untuk jangka waktu kerja sama minimal satu tahun"},
            {"question":"Saya belum tahu pilihan layanan yang cocok untuk bisnis saya, apa yang harus saya lakukan?","answer":"Anda dapat menghubungi Customer Assistance Center 1500009 atau datang langsung ke cabang TRAC terdekat"},
            {"question":"Bisnis saya masih skala kecil (UKM), apakah saya bisa menggunakan layanan ini?","answer":"Bisa. TRAC dapat melayani bisnis dengan berbagai skala."},
            {"question":"Berapa lama jangka waktu minimum kerja sama untuk TRAC FMS?","answer":"Minimal satu tahun"},
            {"question":"Apakah saya bisa menggunakan layanan TRAC FMS dalam jangka waktu kurang dari ketentuan minimal?","answer":"Saat ini kami belum bisa melayani kebutuhan layanan TRAC FMS untuk jangka waktu kurang dari satu tahun. Namun, Anda dapat terlebih dulu mengkonsultasikan kebutuhan transportasi Anda kepada kami dengan menghubungi Customer Assistance Center 1500009."},
            {"question":"Apa yang harus saya lakukan jika membutuhkan informasi lebih lanjut mengenai layanan ini?","answer":"Anda dapat menghubungi Customer Assistance Center 1500009 atau datang langsung ke cabang TRAC terdekat."}
        ]
    },
    {
        "category": "bantuanSelamaSewa",
        "categoryName": "BANTUAN SELAMA MASA SEWA",
        "content": [
            {"question":"Apa yang harus saya lakukan jika mengalami kendala mobil selama masa penyewaan?","answer":"Anda dapat menghubungi Customer Assistance Center 1500009."},
            {"question":"Jika mobil mengalami kendala berat, apakah saya akan mendapat mobil pengganti?","answer":"Kendaraan pengganti akan diberikan apabila estimasi perbaikan kendaraan memakan waktu lebih dari 4 jam."},
            {"question":"Jika mobil mengalami kerusakan karena kelalaian saya, apakah saya terkena denda?","answer":"Setiap kerusakan yang tidak dilindungi asuransi akan menjadi tanggungan pengguna jasa, dengan catatan kendaraan tersebut digunakan tanpa pengemudi TRAC."},
            {"question":"Jika barang di dalam mobil dicuri, apakah saya mendapat kompensasi dari TRAC?","answer":"Anda dapat melakukan klaim atas barang yang dicuri dengan melaporkan kejadian tersebut kepada TRAC dan memberikan semua informasi yang dibutuhkan untuk proses klaim"},
            {"question":"Jika mobil hilang/dicuri, apa yang harus saya lakukan?","answer":"Jangan panik. Segera hubungi Customer Assistance Center 1500009 sekaligus melaporkannya ke kantor kepolisian terdekat. Kami akan segera mengirimkan bantuan untuk Anda"}
        ]
    }
];
