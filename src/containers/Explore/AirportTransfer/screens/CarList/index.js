/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 14:34:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 15:34:49
 */
import React, { Component } from "react";
import {
  View,
  ActivityIndicator
} from "react-native";
import moment from "moment";
import "moment/locale/id";
import { connect } from "react-redux";
import _ from "lodash";
import Ionicons from 'react-native-vector-icons/Ionicons';

import CheckoutFooter from "@airport-transfer-fragments/CheckoutFooter";

import CarFilter from "@airport-transfer-modals/CarFilter";

import CurrencyConfigs from "@airport-transfer-configs/data/Currency";
// import CarSortingConfigs from "@airport-transfer-configs/data/CarSorting";

import VehicleListContainer from "@airport-transfer-fragments/VehicleListContainer";
import CarCard from "@airport-transfer-fragments/CarCard";
import Text from "@airport-transfer-fragments/Text";

import { setTempExtras, getStock, setTempStock, setTempStockComplete, getPrice } from "../../../../../actions";
import { SELF_DRIVE, ONE_WAY, CAR_RENTAL, CHAFFEUR } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class CarList extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    const navData = JSON.parse(JSON.stringify(props.navData));
    this.CarSortingConfigs = [
      {
        label: "Default",
        value: "",
        mode: "DEFAULT",
      },
      {
        label: Lang.highestPrice[this.props.lang],
        value: "price",
        mode: "DESC",
      },
      {
        label: Lang.lowestPrice[this.props.lang],
        value: "-price",
        mode: "ASC",
      },
    ]
  
    this.state = {
      navData: navData,

      list: [],
      listPrice: [],
      listPriceAddHour: [],

      prevAddedList: navData.viewData.addedList ? navData.viewData.addedList : [],
      addedList: [],

      selectedCurrency: CurrencyConfigs.filter((curr) => curr.value == "IDR")[0],
      selectedSort: this.CarSortingConfigs.filter((sort) => sort.value == "default")[0],

      isFilterModalVisible: false,

      filters: {
        price: {
          min: 0,
          max: 100000000,
        },
        types: [],
      },
      isEmpty: false
    };
  }

// ---------------------------------------------------

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.extras !== prevProps.extras) {
      return { extras: this.props.extras }
    }
    if(this.props.stock !== prevProps.stock) {
      return { stock: this.props.stock }
    }
    if(this.props.price !== prevProps.price) {
      return { price: this.props.price }
    }
    if(this.props.priceAddHour !== prevProps.priceAddHour) {
      return { priceAddHour: this.props.priceAddHour }
    }
    if(this.props.tempStockComplete !== prevProps.tempStockComplete) {
      return { stockComplete: this.props.tempStockComplete }
    }
    if(this.state.filters !== prevState.filters) {
      return { filters: this.state.filters }
    }
    if(this.state.selectedSort !== prevState.selectedSort) {
      return { selectedSort: this.state.selectedSort }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.extras !== undefined) {
        this.manageExtras(snapshot.extras);
      }
      if(snapshot.stock !== undefined) {
        this.setState({ isEmpty: snapshot.stock.length === 0 })
        this.manageStock(snapshot.stock);
      }
      if(snapshot.price !== undefined) {
        this.setState({
          listPrice: [ ...prevState.listPrice, snapshot.price[0]]
        }, () => {
          if(this.state.listPrice.length === this.props.stock.length) {
            this.managePrice();
          }
        })
      }
      if(snapshot.stockComplete !== undefined) {
        let filters = this.state.filters;
        let min;
        let max;
        if(snapshot.stockComplete.length > 0) {
          min = _.minBy(snapshot.stockComplete, v => v.rentInfo.basePrice);
          max = _.maxBy(snapshot.stockComplete, v => v.rentInfo.basePrice);
          filters.price.min = min.rentInfo.basePrice;
          filters.price.max = max.rentInfo.basePrice;
          filters.price.defaultMin = min.rentInfo.basePrice;
          filters.price.defaultMax = max.rentInfo.basePrice;
        }
        this.setState({
          list: snapshot.stockComplete,
          filters: filters,
          isEmpty: snapshot.stockComplete.length === 0
        })
      }
      if(snapshot.filters !== undefined) {
        this.manageFilters(snapshot.filters);
      }
      if(snapshot.selectedSort !== undefined) {
        this.manageSort(snapshot.selectedSort);
      }
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  getCarList(stock) {
    return stock.map((car, index) => {
      return car;
    });
  }

  manageExtras = data => {
    let filterExtrasBool;
    let filterExtrasCounter;
    let filterExtrasCounterWithoutAvail;
    let dataExtrasCounter;
    let dataExtrasCounterWithoutAvail;
    let dataExtrasBool;
    let dataExtras;
    filterExtrasBool = _.filter(data, v => v.extras !== null && v.extras.ValueType === "boolean" );
    filterExtrasCounter = _.filter(data, (v) => v.extras !== null && v.Availability !== null && v.extras.ValueType === "counter" && v.extras.StockType === "1");
    filterExtrasCounterWithoutAvail = _.filter(data, (v) => v.extras !== null && v.extras.ValueType === "counter" && v.extras.StockType === "0");
    dataExtrasCounter = _.map(filterExtrasCounter, (extra, key) => {
      let ex = {
          "ExtrasId": extra.ExtrasId,
          "Availability": extra.Availability,
          "name": extra.extras.Name,
          "price": extra.Price,
          "pricePer": "item",
          "amounts": 0,
          "total": 0,
          "ValueType": "counter",
          "StockType": extra.extras.StockType
      };
      return ex;
    });
    
    dataExtrasCounterWithoutAvail = _.map(filterExtrasCounterWithoutAvail, (extra, key) => {
      let ex = {
          "ExtrasId": extra.ExtrasId,
          "Availability": extra.Availability,
          "name": extra.extras.Name,
          "price": extra.Price,
          "pricePer": "item",
          "amounts": 0,
          "total": 0,
          "ValueType": "counter",
          "StockType": extra.extras.StockType
      };
      return ex;
    });

    dataExtrasBool = _.map(filterExtrasBool, (extra, key) => {
      let ex = {
          "ExtrasId": extra.ExtrasId,
          "Availability": extra.Availability,
          "name": extra.extras.Name,
          "price": extra.Price,
          "pricePer": "item",
          "amounts": 0,
          "total": 0,
          "ValueType": "boolean",
          "StockType": extra.extras.StockType
      };
      return ex;
    });

    let join = _.concat(dataExtrasCounter, dataExtrasBool, dataExtrasCounterWithoutAvail);
    dataExtras = _.map(join, (extra, key) => {
      extra.id = key + 1;
      return extra;
    });
    
    this.props.setTempExtras(dataExtras);
    let req = _.omit(this.props.requestStock, ['ProductServiceId', 'CityId', 'MsAirportCode']);
    this.props.getStock(req);
  }

  manageStock = data => {
    let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
    let stock = _.map(data, (v, k) => {
      let newData = v;
      newData.id = k + 1;
      newData.extraItems = this.props.tempExtras;
      newData.rentInfo = {
        "basePrice": 0,
        "additionalPrice": 0,
        "priceAfterDiscount": 0,
        "pricePer": "day",
        "amounts": 0,
        "total": 0
      }
      let requestPrice = {
        BusinessUnitId: this.props.requestStock.BusinessUnitId,
        VehicleTypeId: v.vehicleTypeId,
        Duration: this.props.requestStock.RentalDuration,
        MsProductId: product[0].MsProductId,
        ProductServiceId: this.props.requestStock.ProductServiceId
      }
      if(this.props.requestStock.CityId !== null) {
        requestPrice.CityId = this.props.requestStock.CityId;
      } else {
        requestPrice.MsAirportCode = this.props.requestStock.MsAirportCode;
      }
      this.props.getPrice(requestPrice);
      return newData;
    });
    this.props.setTempStock(stock);
  }

  managePrice = () => {
    const { listPrice } = this.state;
    let flattenStock;
    let originPrice = _.filter(listPrice, o => o !== undefined && o !== null && o !== "");
    let price = _.uniqBy(originPrice, 'VehicleTypeId');
    let newStock = _.map(price, p => {
      let filter = _.filter(this.props.tempStock, { vehicleTypeId: p.VehicleTypeId });
      let newFilter = _.map(filter, f => {
        let extras = _.cloneDeep(f.extraItems);
        let addHour = _.filter(extras, { ExtrasId: "1" });
        let outOfTown = _.filter(extras, { ExtrasId: "2" });
        let overNight = _.filter(extras, { ExtrasId: "3" });
        let expedisi = _.filter(extras, { ExtrasId: "7" });
        let newExtras = [];
        if(addHour.length > 0) {
          addHour[0].price = parseInt(p.configuration_price_product_retail_details[0].AdditionalPrice);
        }        
        switch (this.props.requestStock.ProductServiceId) {
          case SELF_DRIVE:
            f.extraItems = _.concat(addHour, expedisi);
            break;
          case ONE_WAY:
            f.extraItems = _.concat(newExtras, outOfTown)
            break;
          default:
            f.extraItems = _.concat(newExtras, outOfTown, overNight, addHour);
            break;
        }
        f.rentInfo = {
          "basePrice": parseInt(p.configuration_price_product_retail_details[0].BasePrice),
          "additionalPrice": parseInt(p.configuration_price_product_retail_details[0].AdditionalPrice),
          "priceAfterDiscount": 0,
          "pricePer": "day",
          "amounts": 0,
          "total": 0
        }
        return f;
      })
      return newFilter;
    });
    flattenStock = _.flatten(newStock);
    if(flattenStock.length === originPrice.length) {
      this.props.setTempStockComplete(flattenStock);
    }
  }

  manageFilters = params => {
    let filter = [];
    _.map(params.types, v => {
      let f = _.filter(this.props.tempStockComplete, { carType: v });
      filter.push(f);
    });
    filter = _.flatten(filter);
    let byPrice = _.filter(filter, v => {
      return v.rentInfo.basePrice <= params.price.max && v.rentInfo.basePrice >= params.price.min
    });
    this.setState({ list: byPrice });
  }

  manageSort = sort => {
    switch(sort.value) {
      case "stock":
        this.setState({ list: _.orderBy(this.state.list, ['availableUnit'], ['desc']) })
        break;
      case "-stock":
        this.setState({ list: _.orderBy(this.state.list, ['availableUnit'], ['asc']) })
        break;
      case "price":
        this.setState({ list: _.orderBy(this.state.list, ['rentInfo.basePrice'], ['desc']) })
        break;
      case "-price":
        this.setState({ list: _.orderBy(this.state.list, ['rentInfo.basePrice'], ['asc']) })
        break;
      default:
        this.setState({ list: this.state.list })
        break;
    }
  }

// ---------------------------------------------------

  refreshAddedList(code, addedCount, price, car) {
    const addedList = this.state.addedList.filter((item) => {
      return item.code != code;
    });

    const navParam = this.state.navData.viewData;
    const viewData = navParam.trips[navParam.tripIndex];
    
    for (x = 0; x < addedCount; x ++) {
      addedList.push({
        direction: {
          from: viewData.from,
          to: viewData.to,
        },
        tripIndex: navParam.tripIndex,
        vehicle: car,
        code,
        price,
      });
    }

    this.setState({
      addedList,
    });
  }

// ---------------------------------------------------

  getCheckoutData(currency, isMultiple = false) {
    const selecteds = ([]).concat(this.state.prevAddedList, this.state.addedList);
    const request = this.props.requestStock;
    
    const startDate = new moment(request.StartDate, "YYYY-MM-DD HH:mm");
    const endDate = new moment(request.EndDate, "YYYY-MM-DD HH:mm");

    let totalPrice = 0;
    let days = 0;

    if (selecteds.length < 1) {
      return null;
    }

    days = moment(endDate).diff(moment(startDate), 'days');
    // days += 1;
    if(this.props.selectedProduct === CAR_RENTAL) {
      if(this.props.tempForm.type.value === CHAFFEUR) {
        if(days > 0) {
          days += 0; 
        } else {
          days += 1;
        }
      } else {
        days += 1;
      }
    } else {
      days += 1;
    }
    
    selecteds.map((item, index) => {
      totalPrice += item.price * days;
    });

    let type = "car";
    const baseTitle = this.state.navData.viewData.title.split(" - ")[0].toLowerCase();
    if (baseTitle.indexOf("airport") !== -1) {
      type = "airport";
    } else if (baseTitle.indexOf("bus") !== -1) {
      type = "bus";
    }
    
    let buttonOnPress = () => {
      this.props.navigation.push("BookedCarDetail", {
        navData: {
          title: this.state.navData.viewData.title,
          submittedData: this.state.navData.submittedData,
          list: this.state.prevAddedList.concat(this.state.addedList),
          checkoutData: this.getCheckoutData(currency),
          type: type,
        },
      });
    }
    let isDisabled = this.state.addedList.length < 1;

    if (this.state.navData.viewData.tripIndex < (this.state.navData.viewData.trips.length - 1)) {
      buttonOnPress = () => {
        const navData = JSON.parse(JSON.stringify(this.state.navData));
        navData.viewData.tripIndex += 1;
        navData.viewData.addedList = ([]).concat(this.state.prevAddedList, this.state.addedList);

        Actions.CarList({
          navData,
        });
      };
    }

    return {
      currency: currency,
      totalPrice: totalPrice,
      digitDelimiter: this.state.selectedCurrency.digitDelimiter,
      days: days,
      isDisabled: isDisabled,
      listCount: selecteds.length,
      hasNext: this.state.navData.viewData.tripIndex < (this.state.navData.viewData.trips.length - 1),
      onButtonPress: () => buttonOnPress(),
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderList() {
    let cars = this.state.list;

    return cars.map((car, index) => {
      let features = [{
        type: "passanger",
        value: car.totalSeat,
      },{
        type: "baggage",
        value: car.totalLugagge,
      },{
        type: "transmission",
        value: car.isTransmissionManual === 1 ? "MT" : "AT",
      },{
        type: "airbags",
        value: car.isAirbag === 1 ? true : false,
      },{
        type: "air-conditioner",
        value: car.isAC === 1 ? true : false,
      }]
      return (
        <CarCard
          key={ index }
          code={ car.id }
          name={ car.vehicleAlias !== null ? car.vehicleAlias : car.vehicleTypeDesc }
          currency={ this.state.selectedCurrency.symbol }
          digitDelimiter={ this.state.selectedCurrency.digitDelimiter }
          originalPrice={ car.rentInfo.basePrice }
          price={ car.rentInfo.basePrice }
          stock={ car.availableUnit }
          imageUrl={ car.vehicleImage }
          features={ features }
          detail={ car.description }
          termAndCondition={ car.termAndCondition }
          isSpecialDeal={ false }
          onAddCarPress={ (code, addedCount, price) => this.refreshAddedList(code, addedCount, price, car) }
          onRemoveCarPress={ (code, addedCount, price) => this.refreshAddedList(code, addedCount, price, car) }
          existingBooking={ this.state.addedList.length }
        />
      );
    });
  }

// ---------------------------------------------------

  _renderCheckoutFooter(currency, isMultiple = false) {
    const checkoutData = this.getCheckoutData(currency, isMultiple);
    
    if (!checkoutData) {
      return null;
    }

    return (
      <CheckoutFooter
        type={ "CAR" }
        currency={ checkoutData.currency }
        totalPrice={ checkoutData.totalPrice }
        digitDelimiter={ checkoutData.digitDelimiter }
        days={ checkoutData.days }
        isDisabled={ checkoutData.isDisabled }
        hasNext={ checkoutData.hasNext }
        listCount={ checkoutData.listCount }
        onButtonPress={ () => checkoutData.onButtonPress() }
      />
    );
  }

// ---------------------------------------------------

  _renderFilterModal(priceData, typeData) {
    return (
      <CarFilter
        priceValue={ this.state.filters.price }
        typeValues={ this.state.filters.types }
        isVisible={ this.state.isFilterModalVisible }
        currency={ this.state.selectedCurrency.symbol }
        digitDelimiter={ this.state.selectedCurrency.digitDelimiter }
        onClosePress={ () => this.setState({isFilterModalVisible: false}) }
        onFilterSet={ (filterData) => this.setState({
          isFilterModalVisible: false,
          filters: {
            price: filterData.price,
            types: filterData.types,
          },
        }) }
      />
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const { loadingExtras, loadingStock, loadingPrice, loadingCarType } = this.props;
    const navParam = this.state.navData.viewData;
    const viewData = navParam.trips[navParam.tripIndex];

    return (
      <VehicleListContainer
        { ... this.props }
        title={ this.state.navData.viewData.title }
        direction={{
          from: viewData.fromLabel,
          to: viewData.toLabel,
        }}
        checkoutFooter={ this._renderCheckoutFooter(this.state.selectedCurrency.symbol) }
        sort={{
          options: this.CarSortingConfigs,
          selected: this.state.selectedSort,
          onSelect: (sort) => this.setState({selectedSort: sort,}) 
        }}
        // currency={{
        //   options: CurrencyConfigs,
        //   selected: this.state.selectedCurrency,
        //   onSelect: (curr) => this.setState({selectedCurrency: curr,}),
        // }}
        filter={{
          modal: this._renderFilterModal(),
          toggle: (isVisible) => this.setState({isFilterModalVisible: isVisible}),
        }}
      >
        { !this.state.isEmpty ? this._renderList() : 
          <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 20, paddingBottom: 20 }}>
            <Ionicons name="md-search" size={32} />
            <Text>{Lang.dataNotFound[this.props.lang]}</Text>
          </View> }

        { (loadingExtras || loadingStock || loadingPrice || loadingCarType) && <View style={{ marginTop: 20 }}>
          <ActivityIndicator size="small" color="#2A2E36" />
        </View> }
      </VehicleListContainer>
    )
  }

}

const mapStateToProps = ({ stockList, masterExtras, priceList, productService, masterCarType, temporaryForm, language }) => {
  const loadingExtras = masterExtras.loading;
  const loadingStock = stockList.loading;
  const loadingPrice = priceList.loading;
  const loadingCarType = masterCarType.loading;
  const { extras, tempExtras } = masterExtras;
  const { stock, requestStock, tempStock, tempStockComplete } = stockList;
  const { price, priceAddHour } = priceList;
  const { products, selectedProduct } = productService;
  const { tempForm } = temporaryForm;
  const { lang } = language;
  return { extras, tempExtras, requestStock, stock, tempStock, price, products, tempStockComplete, loadingExtras, loadingStock, loadingPrice, loadingCarType, selectedProduct, priceAddHour, tempForm, lang };
}

export default connect(mapStateToProps, {
  setTempExtras,
  getStock,
  setTempStock,
  setTempStockComplete,
  getPrice
})(CarList);
