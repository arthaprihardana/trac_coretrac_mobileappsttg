/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 14:24:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 10:26:55
 */
import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Clipboard,
  TextInput,
  WebView
} from "react-native";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import {
  appVariables,
  appMetrics
} from "@variables";
import { Actions } from "react-native-router-flux";
import _ from "lodash";
import Lang from '../../../../../assets/lang';

import Text from "@airport-transfer-fragments/Text";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Header from "@airport-transfer-fragments/Header";
import styles from "./style";
import MyWebView from '@airport-transfer-fragments/WebView';
import { connect } from "react-redux";

class NewsDetail extends Component {
  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------
  constructor(props)
  {
    super(props);
    this.state = { 
      datacontent : [{id: '1' , value: 'Feature in January'}, {id: '1' , value: 'Feature in February'}, {id: '1' , value: 'Feature in March'}, {id: '1' , value: 'Feature in April'}, ],
    };
  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // EVENT LISTENERS
  // ---------------------------------------------------

  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // --------------------------------------------------- 
  _copy = (code) => {
    Clipboard.setString(code);
  }

  _showLogin = ()=> {

  }
  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------
  _renderBannerPromo() {
    return (
      <View style={styles.bannerPromo}>
        <View style={{
          flex: 1,
          backgroundColor: '#cccccc'
          // flexDirection: "row"
        }}>
          <Image
            source={{ uri: this.props.item.image }}
            style={{ height: 250, width: appMetrics.windowWidth, backgroundColor: '#ccc' }}
            resizeMode="cover"
            progressiveRenderingEnabled={true} />
        </View>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderPromoDescription() {
    const customStyle = `<style>* {max-width: 100%;} body {font-family: ${ appVariables.museo900 }; padding-right: 40px}</style>`;
    return (
      <View style={styles.promoDescription.wrapper}>
        <View style={{
          flex: 1,
          flexDirection: "row",
          paddingVertical: 20,
        }}>
          <Text size={24}>{this.props.item.title}</Text>
         
        </View>
        <Text color={"grey"}>WRITTEN BY TRAC - {this.props.item.date}</Text>
        <View style={styles.promoDescription.descriptionWrapper}>
          <MyWebView originWhitelist={['*']} source={{ html: `${customStyle}${this.props.item.content}` }} style={{ width: '100%', height: 300 }} mixedContentMode="always" />
          {/* <Text size={16} color={appVariables.colorGray}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel placerat dui, a euismod quam. Sed ac lacus
            nec enim faucibus pulvinar congue id ligula. Mauris quis libero a erat hendrerit mollis ac in purus. Fusce
            ullamcorper eleifend enim, id scelerisque libero. Etiam tempus massa vel mi tincidunt, vestibulum laoreet nulla
            pulvinar. Fusce tristique ac lacus ut porta. Nulla facilisi.
         </Text> */}
        </View>
      </View>
    );
  }
  
  // ---------------------------------------------------
  _renderOtherPromoTitle() {
    return (
      <View style={styles.otherPromo.title}>
        <Text
          size={20}>
          Promo Lainnya
        </Text>
      </View>
    );
  }
  // ---------------------------------------------------
  _renderOtherPromoCard() {
    return (
      <View style={styles.otherPromoCard.wrapper}>
        <View style={{
          flex: 1,
          flexDirection: "row"
        }}>
          <Image
            source={require('@images/png/messageDetail/promo1.png')}
            style={{ flex: -1 }}
            resizeMode="stretch" />
        </View>
        <View style={styles.otherPromoCard.title}>
          <Text
            size={16}>
            Promo HUT RI Dapatkan Voucher Diskon
        </Text>
        </View>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderOtherPromos() {
    var otherPromos = [];
    var otherPromosAmount = 2;
    for (let i = 0; i < otherPromosAmount; i++) {
      otherPromos.push(
        <View key={i}>
          {this._renderOtherPromoCard()}
        </View>
      )
    }
    return (
      <View>
        {otherPromos}
        <View style={styles.otherPromoCard.wrapper}>
          <View style={{flexDirection: "row", flexWrap: "wrap"}}>
            <View style={{marginTop: 10, marginRight: 10, backgroundColor: "#1F419B", paddingVertical: 10, paddingHorizontal: 20}}><Text color={"white"}>TRAVEL</Text></View>
            <View style={{marginTop: 10, marginRight: 10, backgroundColor: "#1F419B", paddingVertical: 10, paddingHorizontal: 20}}><Text color={"white"}>ASIA</Text></View>
            <View style={{marginTop: 10, marginRight: 10, backgroundColor: "#1F419B", paddingVertical: 10, paddingHorizontal: 20}}><Text color={"white"}>LIBURAN</Text></View>
            <View style={{marginTop: 10, marginRight: 10, backgroundColor: "#1F419B", paddingVertical: 10, paddingHorizontal: 20}}><Text color={"white"}>INDONESIA</Text></View>
          </View>

          <View style={{marginTop: 20, }}>
            <View style={{ borderBottomWidth: 1, borderColor: "grey", width: 50}}>
              <Text color={"grey"}>SHARE</Text>
            </View>
            <View style={{flexDirection: "row"}}>
            <TouchableOpacity onPress={()=> this._showLogin()} style={{alignSelf: "flex-end"}}> 
              <View style={{marginTop: 20, marginRight: 10, alignSelf: "flex-end"}}>
                <Image
                  source={require('@images/png/bannerContent/icoFb.png')}
                  resizeMode="contain" />
                <Text style={{color: "grey", fontSize: 12, paddingTop: 5,}}>1200</Text>
              </View>
            </TouchableOpacity> 
            <TouchableOpacity style={{alignSelf: "flex-end"}}> 
              <View style={{marginTop: 20, marginRight: 10, alignSelf: "flex-end"}}>
                <Image
                  source={require('@images/png/bannerContent/icoTwitter.png')}
                  resizeMode="contain" />
                <Text style={{color: "grey", fontSize: 12, paddingTop: 5,}}>1200</Text>
              </View>
            </TouchableOpacity> 
            <TouchableOpacity style={{alignSelf: "flex-end"}}> 
              <View style={{marginTop: 20, marginRight: 10, alignSelf: "flex-end"}}>
                <Image
                  source={require('@images/png/bannerContent/icoShare.png')}
                  resizeMode="contain" />
                <Text style={{color: "grey", fontSize: 12, paddingTop: 5,}}>1200</Text>
              </View>
            </TouchableOpacity> 
            </View>
          </View>
        </View>
      </View>
    );
  }

   // ---------------------------------------------------
   _renderContent(item, key) {
    return (       
      <View style={{ width: "50%" }} key={key}> 
        <TouchableOpacity onPress={() => Actions.push("NewsDetail", { item: item })} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}> 
          <View style={{ flex: 1, flexDirection: "column" }}>
            <Image source={{ uri: item.image }} resizeMode="cover" style={{ width: 170, height: 100 }} />
            <Text style={{fontWeight: 'bold',}} size={18} color={appVariables.colorGray}>{item.title}</Text>
          </View>
        </TouchableOpacity>
      </View>   
      // <View style={{ width: "50%",}} key={key}>  
      //   <TouchableOpacity onPress={() => Actions.NewsDetail()}> 
      //     <View style={{
      //       flex: 1,
      //       flexDirection: "column",
      //     }}>
      //       <Image
      //           source={require('@images/png/bannerContent/contentNews.png')}              
      //           resizeMode="cover" />
      //       {/* <View style={styles.promoDescription.descriptionWrapper}> */}
      //         <Text style={{fontWeight: 'bold',}} size={18} color={appVariables.colorGray}>
      //         {item} 
      //         </Text>
      //       {/* </View> */}
      //     </View>
      //     <View style={styles.promoDescription.descriptionWrapper}>
      //       <Text size={12} color={appVariables.colorGray}>
      //       Jul 6, 2017
      //     </Text>
      //     </View>
      //   </TouchableOpacity>  
      // </View>
     
    );
  }

  // ---------------------------------------------------
  _renderAnotherArticle() {
    return (
      <View>
        <Text style={{paddingLeft: 15, paddingTop: 15, fontWeight: "bold", fontSize: 24}}>{Lang.anotherArticle[this.props.lang]}</Text>
      <View style={{
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 15,
      }}>
        { _.sampleSize(this.props.articles, 4).map((item, key) => this._renderContent(item, key)) }
        {/* {
          this.state.datacontent.map(( item, key ) =>
          (
            this._renderContent(item.value, key)
          ))          
        } */}
      </View>
      </View>
    );
  }

  // ---------------------------------------------------
  _renderComment() {
    return (
      <View style={{backgroundColor: "rgba(138, 149, 158, 0.1)"}}>
        <Text style={{paddingLeft: 15, paddingTop: 15, fontSize: 18}}>Leave a comment</Text>
        <View style={{
          margin: 15, backgroundColor: "white"}} >
          <TextInput
            style={{height: 150, padding: 10,
            justifyContent: "flex-start"}}
            underlineColorAndroid="transparent"
            placeholder="Type something"
            placeholderTextColor="grey"
            numberOfLines={10}
            multiline={true}
          />
        </View>


        <Text style={{paddingLeft: 15, paddingTop: 15, fontSize: 18}}>375 Comments</Text>
        <View style={{
          margin: 15, padding: 15, backgroundColor: "white", flexDirection: "row"}} >
          <Image
            source={require('@images/png/bannerContent/icoUser.png')}
            resizeMode="contain" />

          <View style={{marginHorizontal: 10}}>
            <Text style={{fontSize: 20}}>Bryan Barry</Text>
            <Text color={"grey"}>Aug 26, 2016 8</Text>
            <Text style={{marginTop: 20, marginRight: 30}}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorpe</Text>
          </View>

          <View style={styles.lineSeparator}></View>
        </View>

        <View style={{
          margin: 15, padding: 15, backgroundColor: "white", flexDirection: "row"}} >
          <Image
            source={require('@images/png/bannerContent/icoUser.png')}
            resizeMode="contain" />

          <View style={{marginHorizontal: 10}}>
            <Text style={{fontSize: 20}}>Bryan Barry</Text>
            <Text color={"grey"}>Aug 26, 2016 8</Text>
            <Text style={{marginTop: 20, marginRight: 30}}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorpe</Text>
          </View>

          <View style={styles.lineSeparator}></View>
        </View>

        <TouchableOpacity > 
        <View style={{
          margin: 15, padding: 15, backgroundColor: "white", alignItems: "center"}} >
          <Text>Show All Comments</Text>
        </View>
        </TouchableOpacity>
        
      </View>
    );
  }

  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <BaseContainer>
        <Header title={this.props.item.title} onBackPress={() => Actions.pop()} />
        <ScrollView style={{
          flex: 1,
        }}>
          {this._renderBannerPromo()}
          {this._renderPromoDescription()}
          {/* <View style={styles.lineSeparator}></View> */}
          {/* {this._renderOtherPromoTitle()} */}
          {/* {this._renderOtherPromos()} */}

           <View style={styles.lineSeparator}></View>

           {this._renderAnotherArticle()}
           {/* {this._renderComment()} */}
        </ScrollView>
      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ contentArticle, language }) => {
  const { articles } = contentArticle;
  const { lang } = language;
  return { articles, lang }
}

export default connect(mapStateToProps, {})(NewsDetail)
