import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  form: {
    extra: {
      container: {
        flex: -1,
        flexDirection: "row",
        paddingVertical: 20,
        justifyContent: "space-between",
      },
    },

    bottom: {
      container: {
        flex: -1,
        alignItems: "center",
        marginTop: 15,
      },

      register: {
        container: {
          flex: -1,
          flexDirection: "row",
          justifyContent: "center",
          paddingVertical: 5,
        },

        button: {
          container: {
            flex: -1,
            marginLeft: 3,
          },
        },
      },
    },
  },
}