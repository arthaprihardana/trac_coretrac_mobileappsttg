/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-04 14:09:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 14:10:43
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  Alert,
  Keyboard
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";
import _ from "lodash";
import firebase from 'react-native-firebase';

import Text from "@airport-transfer-fragments/Text";
import FloatingTextInput from "@airport-transfer-fragments/FloatingTextInput";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import MainButton from "@airport-transfer-fragments/MainButton";
import SocialMediaLogin from "@airport-transfer-fragments/SocialMediaLogin";
import AuthContainer from "@airport-transfer-fragments/AuthContainer";
import Lang from '../../../../../assets/lang';

import styles from "./style";

import { postLogin, setRememberMe, isSkipWelcomeScreen, postLoginSuccess } from "../../../../../actions";
import { Actions } from "react-native-router-flux";
import { REGEX_EMAIL } from "../../../../../constant";

class Login extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      form: {
        email: "",
        password: "",
        shouldRemember: false,
      },
      readyToSubmit: false,
      errorEmail: null
    };
  }

  componentDidMount = () => {
    if(this.props.first) this.props.isSkipWelcomeScreen(true); 
    if(this.props.isExpired) {
      Alert.alert(
        'Info',
        'Sesi login anda sudah kedaluarsa, Silahkan login kembali',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
    }
  }

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.isLogin !== prevProps.isLogin) {
      return { isLogin: this.props.isLogin };
    }
    if(this.props.login !== prevProps.login) {
      return { login: this.props.login }
    }
    return null;
  }
  
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.isLogin) {
        firebase.messaging().unsubscribeFromTopic(this.props.login.Data.Id);
        if( this.props.navData !== undefined ) {
          Actions.push("PassangerDetail", {
            navData: this.props.navData
          })
        } else {
          Actions.reset("Home");
        }
      } else {
        if(snapshot.login) {
          if(_.isString(snapshot.login)) {
            Alert.alert(
              'Info',
              Lang.loginError[this.props.lang],
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              {cancelable: false},
            );
          }
        }
      }
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeForm(name, value) {
    const form = this.state.form;
    form[name] = value;

    this.setState({
      form
    });
    switch (name) {
      case "email":
        this.setState({ errorEmail: REGEX_EMAIL.test(value) ? null : "Email didn't valid" })
        break;
      default:
        break;
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  onValidate = async current => {
    let except = ['shouldRemember'];
    except.push(current);
    let o = _.omit(this.state.form, except);
    let a = await _.findKey(o, val => {
      if(_.isEmpty(val)) { return true }
    });
    return a;
  }

  login() {
    const data = {
      EmailPersonal: this.state.form.email,
      Password: this.state.form.password,
    }
    Keyboard.dismiss();
    this.props.postLoginSuccess(null);
    this.props.postLogin(data);
  }

// ---------------------------------------------------  

  goToRegister() {
    Actions.push("Register")
  }

// ---------------------------------------------------  

  goToForgotPassword() {
    Actions.push("ForgotPassword")
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderRegisterButton() {
    return (
      <View style={ styles.form.bottom.register.container }>
        <Text>
          {Lang.dontHaveAccount[this.props.lang]}
        </Text>

        <TouchableOpacity
          style={ styles.form.bottom.register.button.container }
          onPress={ () => this.goToRegister() }
        >
          <Text color={ "navy" }>
            {Lang.registerHere[this.props.lang]}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

// ---------------------------------------------------

  _renderForm() {
    return (
      <View>
        <FloatingTextInput
          refs={ref => this._email = ref}
          label={ "Email" }
          onChangeText={ (value) => {
            this.onValidate("email").then(val => {
              this.onChangeForm("email", value) 
              this.setState({ readyToSubmit: value !== "" && val === undefined && this.state.errorEmail === null ? true : false })
            });
          }}
          value={ this.state.form.email }
          keyboardType={ "email-address" }
          returnKeyType="next"
          onSubmitEditing={() => this._password.ref.focus()}
          note={this.state.errorEmail}
        />

        <FloatingTextInput
          refs={ref => this._password = ref}
          label={ "Password" }
          onChangeText={ (value) => {
            this.onValidate("password").then(val => {
              this.onChangeForm("password", value) 
              this.setState({ readyToSubmit: value !== "" && val === undefined ? true : false })
            });
          }}
          value={ this.state.form.password }
          secureTextEntry
          returnKeyType="done"
          onSubmitEditing={ () => this.state.readyToSubmit ? this.login() : {} }
        />

        <View style={ styles.form.extra.container }>
          <RoundedCheck
            value={ this.state.form.shouldRemember }
            onPress={ (value) => {
              this.props.setRememberMe(value)
              this.onChangeForm("shouldRemember", value) 
            }}
          >
            <Text>
              {Lang.rememberMe[this.props.lang]}
            </Text>
          </RoundedCheck>

          <TouchableOpacity
            onPress={ () => this.goToForgotPassword() }
          >
            <Text>
              {Lang.forgotPassword[this.props.lang]}
            </Text>
          </TouchableOpacity>
        </View>

        <MainButton
          label={ Lang.loginButton[this.props.lang] }
          isLoading={this.props.loading}
          onPress={ () => this.state.readyToSubmit && this.state.errorEmail === null ? this.login() : {}}
          rounded
          disabled={!this.state.readyToSubmit || this.state.errorEmail !== null}
        />

        <View style={ styles.form.bottom.container }>
          <Text>
            {Lang.orContinueWith[this.props.lang]}
          </Text>

          <SocialMediaLogin/>

          { this._renderRegisterButton() }
        </View>
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <AuthContainer { ... this.props } title={ Lang.login[this.props.lang] }>

        { this._renderForm() }        

      </AuthContainer>
    );
  }
}

const mapStateToProps = ({ authentication, language }) => {
  const { loading, login, isLogin } = authentication;
  const { lang } = language;
  return { loading, login, isLogin, lang }
}

export default connect(mapStateToProps, {
  postLogin,
  setRememberMe,
  isSkipWelcomeScreen,
  postLoginSuccess
})(Login)
