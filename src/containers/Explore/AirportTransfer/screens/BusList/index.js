/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 21:50:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 16:42:41
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import {
  View,
  ScrollView,
  Image,
  ActivityIndicator,
  Alert
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import moment from "moment";
import "moment/locale/id";
import { connect } from "react-redux";
import _ from "lodash";
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from "@airport-transfer-fragments/Text";
import CheckoutFooter from "@airport-transfer-fragments/CheckoutFooter";

import BusFilter from "@airport-transfer-modals/BusFilter";

import CurrencyConfigs from "@airport-transfer-configs/data/Currency";
import BusSortingConfigs from "@airport-transfer-configs/data/BusSorting";

import styles from "./style";

import VehicleListContainer from "@airport-transfer-fragments/VehicleListContainer";
import BusCard from "@airport-transfer-fragments/BusCard";

import busListDummies from "@airport-transfer-dummies/bus_list";

import { getStock, setTempExtras, setTempStock, setTempStockComplete, getPriceForBus } from "../../../../../actions";
import Lang from '../../../../../assets/lang';

class BusList extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    const navData = JSON.parse(JSON.stringify(props.navData));
  
    this.state = {
      navData: navData,

      list: [],
      listPrice: [],

      prevAddedList: navData.viewData.addedList ? navData.viewData.addedList : [],
      addedList: [],

      selectedCurrency: CurrencyConfigs.filter((curr) => curr.value == "IDR")[0],
      selectedSort: BusSortingConfigs.filter((sort) => sort.value == "default")[0],

      isFilterModalVisible: false,

      filters: {
        price: {
          min: 0,
          max: 100000000,
        },
        types: [],

        // facilities: [],
        
        // luxury_types: [],
        // coaster_types: [],
        // big_types: [],
        // medium_types: [],
        // small_types: [],
      },
      isEmpty: false
    };
  }

// ---------------------------------------------------

  getSnapshotBeforeUpdate = (prevProps, prevState) => {
    if(this.props.extras !== prevProps.extras) {
      return { extras: this.props.extras }
    }
    if(this.props.stock !== prevProps.stock) {
      return { stock: this.props.stock }
    }
    if(this.props.priceForBus !== prevProps.priceForBus) {
      return { price: this.props.priceForBus }
    }
    if(this.props.tempStockComplete !== prevProps.tempStockComplete) {
      return { stockComplete: this.props.tempStockComplete }
    }
    return null;
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if(snapshot !== null) {
      if(snapshot.extras !== undefined) {
        this.manageExtras(snapshot.extras);
      }
      if(snapshot.stock !== undefined) {
        this.setState({ isEmpty: snapshot.stock.length === 0 })
        this.manageStock(snapshot.stock);
      }
      if(snapshot.price !== undefined) {
        this.setState({
          listPrice: [ ...this.state.listPrice, snapshot.price.Price !== null ? snapshot.price : undefined ]
        }, () => {
            if(this.state.listPrice.length === this.props.stock.length) {
              this.managePrice();
            }
        })
      }
      if(snapshot.stockComplete !== undefined) {
        // let filters = this.state.filters;
        // let min = _.minBy(snapshot.stockComplete, v => v.rentInfo.basePrice);
        // let max = _.maxBy(snapshot.stockComplete, v => v.rentInfo.basePrice);
        // filters.price.min = min.rentInfo.basePrice;
        // filters.price.max = max.rentInfo.basePrice;
        // filters.price.defaultMin = min.rentInfo.basePrice;
        // filters.price.defaultMax = max.rentInfo.basePrice;
        this.setState({
          list: snapshot.stockComplete,
          // filters: filters
          isEmpty: snapshot.stockComplete.length === 0
        })
      }
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  manageExtras = data => {
    let filterExtrasBool;
    let filterExtrasCounter;
    let filterExtrasCounterWithoutAvail;
    let dataExtrasCounter;
    let dataExtrasCounterWithoutAvail;
    let dataExtrasBool;
    let dataExtras;
    filterExtrasBool = _.filter(data, v => v.extras !== null && v.extras.ValueType === "boolean" );
    filterExtrasCounter = _.filter(data, (v) => v.extras !== null && v.Availability !== null && v.extras.ValueType === "counter" && v.extras.StockType === "1");
    filterExtrasCounterWithoutAvail = _.filter(data, (v) => v.extras !== null && v.extras.ValueType === "counter" && v.extras.StockType === "0");
    dataExtrasCounter = _.map(filterExtrasCounter, (extra, key) => {
      let ex = {
          "ExtrasId": extra.ExtrasId,
          "Availability": extra.Availability,
          "name": extra.extras.Name,
          "price": extra.Price,
          "pricePer": "item",
          "amounts": 0,
          "total": 0,
          "ValueType": "counter"
      };
      return ex;
    });
    
    dataExtrasCounterWithoutAvail = _.map(filterExtrasCounterWithoutAvail, (extra, key) => {
      let ex = {
          "ExtrasId": extra.ExtrasId,
          "Availability": extra.Availability,
          "name": extra.extras.Name,
          "price": extra.Price,
          "pricePer": "item",
          "amounts": 0,
          "total": 0,
          "ValueType": "counter"
      };
      return ex;
    });

    dataExtrasBool = _.map(filterExtrasBool, (extra, key) => {
      let ex = {
          "ExtrasId": extra.ExtrasId,
          "Availability": extra.Availability,
          "name": extra.extras.Name,
          "price": extra.Price,
          "pricePer": "item",
          "amounts": 0,
          "total": 0,
          "ValueType": "boolean"
      };
      return ex;
    });

    let join = _.concat(dataExtrasCounter, dataExtrasBool, dataExtrasCounterWithoutAvail);
    dataExtras = _.map(join, (extra, key) => {
      extra.id = key + 1;
      return extra;
    });
    
    this.props.setTempExtras(dataExtras);
    let req = _.omit(this.props.requestStock, ['ProductServiceId', 'CityId']);
    this.props.getStock(req);
  }

  manageStock = data => {
    let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
    let stock = _.map(data, (v, k) => {
      let newData = v;
      newData.id = k + 1;
      newData.extraItems = this.props.tempExtras;
      newData.rentInfo = {
        "basePrice": 0,
        "additionalPrice": 0,
        "priceAfterDiscount": 0,
        "pricePer": "day",
        "amounts": 0,
        "total": 0
      }
      let requestPrice = {
        MsProductId: product[0].MsProductId,
        BusinessUnitId: this.props.requestStock.BusinessUnitId,
        BranchId: this.props.actualBranch,
        VehicleTypeId: v.vehicleTypeId,
        IsOutTown: this.props.zone.IsOutTown,
        Distance: this.props.zone.KM,
        Date: this.props.requestStock.StartDate,
        ZoneId: this.props.zone.MsZoneId,
        TotalDays: this.props.requestStock.RentalDuration
      }
      this.props.getPriceForBus(requestPrice);
      return newData;
    });
    this.props.setTempStock(stock);
  }

  managePrice = () => {
    const { listPrice } = this.state;
    let flattenStock;
    let originPrice = _.filter(listPrice, o => o !== undefined && o !== null && o !== "");
    let price = _.uniqBy(originPrice, 'VehicleTypeId');
    let newStock = _.map(price, p => {
      let filter = _.filter(this.props.tempStock, { vehicleTypeId: p.VehicleTypeId })
      let newFilter = _.map(filter, f => {
        f.rentInfo = {
          "basePrice": parseInt(p.Price),
          "additionalPrice": parseInt(p.Price),
          "priceAfterDiscount": 0,
          "pricePer": "day",
          "amounts": 0,
          "total": 0
        }
        return f;
      })
      return newFilter;
    });
    flattenStock = _.flatten(newStock);
    if(flattenStock.length === originPrice.length) {
      this.props.setTempStockComplete(flattenStock);
    }
  }

// ---------------------------------------------------

  refreshAddedList(code, addedCount, price, bus) {
    const addedList = _.filter(this.state.addedList, item => item.code != code);
    
    const navParam = this.state.navData.viewData;
    const viewData = navParam.trips[navParam.tripIndex];

    for (x = 0; x < addedCount; x ++) {
      addedList.push({
        direction: {
          from: viewData.from,
          to: viewData.to,
        },
        tripIndex: navParam.tripIndex,
        vehicle: bus,
        code,
        price,
      });
    }
    
    this.setState({
      addedList: addedList
    });
  }

// ---------------------------------------------------

  getCheckoutData(currency, isMultiple = false) {
    const selecteds = ([]).concat(this.state.prevAddedList, this.state.addedList);
    const navParam = this.state.navData.viewData;
    const viewData = navParam.trips[navParam.tripIndex];
    const startDate = new moment(viewData.start, "DD-MM-YYYY");
    const endDate = new moment(viewData.end, "DD-MM-YYYY");
    let days = 0;

    if (selecteds.length < 1) {
      return null;
    }

    days = moment.duration(endDate.diff(startDate)).asDays();
    days += 1;

    let totalPrice = 0;
    let limitCount = 0;
    let images = [];
    
    selecteds.map((item, index) => {
      totalPrice += item.price;
      limitCount += item.vehicle.totalSeat;
      images.push(item.vehicle.vehicleImage);
      // totalPrice += item.price * days;
      // limitCount += item.vehicle.features.filter(feature => feature.type == "passanger")[0].value;
      // images.push(item.vehicle.image_url);
    });

    let type = "bus";
    // const baseTitle = this.state.navData.viewData.title.split(" - ")[0].toLowerCase();
    // if (baseTitle.indexOf("airport") !== -1) {
    //   type = "airport";
    // } else if (baseTitle.indexOf("bus") !== -1) {
    //   type = "bus";
    // }

    let buttonOnPress = () => Actions.BookedBusDetail({
      navData: {
        title: this.state.navData.viewData.title,
        submittedData: this.state.navData.submittedData,
        list: this.state.prevAddedList.concat(this.state.addedList),
        checkoutData: this.getCheckoutData(currency),
        type: type,
      },
    });
    let isDisabled = this.state.addedList.length < 1;

    if (this.state.navData.viewData.tripIndex < (this.state.navData.viewData.trips.length - 1)) {
      buttonOnPress = () => {
        const navData = JSON.parse(JSON.stringify(this.state.navData));
        navData.viewData.tripIndex += 1;
        navData.viewData.addedList = ([]).concat(this.state.prevAddedList, this.state.addedList);

        Actions.BusList({
          navData,
        });
      };
    }

    return {
      currency: currency,
      totalPrice: totalPrice,
      digitDelimiter: this.state.selectedCurrency.digitDelimiter,
      days: days,
      isDisabled: isDisabled,
      listCount: selecteds.length,
      hasNext: this.state.navData.viewData.tripIndex < (this.state.navData.viewData.trips.length - 1),
      passangerCount: this.state.navData.submittedData.trips.trip1.passanger_count,
      passangerLimit: limitCount,
      images: images,
      onButtonPress: () => buttonOnPress(),
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderList() {
    let buses = this.state.list;

    // buses = buses.filter(bus => bus.price >= this.state.filters.price.min && bus.price <= this.state.filters.price.max);
    // buses = buses.filter(bus => this.state.filters.types.length == 0 || this.state.filters.types.indexOf(bus.type) !== -1);
    // return <View />
    return buses.map((bus, index) => {
      let features = [{
	      type: "passanger",
	      value: bus.totalSeat,
	    },{
	      type: "karaoke",
	      value: bus.isKaraoke === 1 ? true : false,
	    },{
	      type: "wifi",
	      value: bus.isWiFi === 1 ? true : false,
	    },{
	      type: "power_socket",
	      value: bus.isCharger === 1 ? true : false,
	    }]
      return (
        <BusCard
          key={ index }
          code={ bus.vehicleTypeId }
          name={ bus.vehicleAlias !== null ? bus.vehicleAlias : bus.vehicleTypeDesc }
          currency={ this.state.selectedCurrency.symbol }
          digitDelimiter={ this.state.selectedCurrency.digitDelimiter }
          originalPrice={ bus.rentInfo.basePrice }
          price={ bus.rentInfo.basePrice }
          stock={ bus.availableUnit }
          imageUrl={ bus.vehicleImage }
          features={ features }
          seatingArrangement={ bus.seatArrangement.split("-").map(v => parseInt(v)) }
          galleries={ bus.detailImage }
          detail={ bus.description }
          isSpecialDeal={ false }
          onAddCarPress={ (code, addedCount, price) => this.refreshAddedList(code, addedCount, price, bus) }
          onRemoveCarPress={ (code, addedCount, price) => this.refreshAddedList(code, addedCount, price, bus) }
        />
      );
    });
  }

// ---------------------------------------------------

  _renderCheckoutFooter(currency, isMultiple = false) {
    const checkoutData = this.getCheckoutData(currency, isMultiple);

    if (!checkoutData) {
      return null;
    }

    return (
      <View>
        <View style={{
          flex: -1,
          flexDirection: "row",
          height: 80,
          shadowOpacity: 1,
          shadowRadius: 2,
          shadowOffset: {
            height: 1,
            width: 1
          },
        }}>
          <View style={{
            flex: -1,
            padding: 20,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: appVariables.colorBlueHeader,
          }}>
            <Text color={ appVariables.colorWhite } size={ 18 }>
              {Lang.passengers[this.props.lang]}
            </Text>
            <Text color={ appVariables.colorWhite } size={ 18 }>
              { checkoutData.passangerCount } / { checkoutData.passangerLimit }
            </Text>
          </View>

          <ScrollView style={{
            flex: 1,
            backgroundColor: appVariables.colorWhite,
          }}
            contentContainerStyle={{
              paddingLeft: 10,
              paddingVertical: 10,
            }}
            horizontal
            showsHorizontalScrollIndicator={ false }
          >

            { checkoutData.images.map((imageUrl, index) => {
              return (
                <View
                  key={ index }
                  style={{
                    flex: -1,
                    width: 100,
                    marginRight: 10,
                  }}
                >
                  <Image
                    style={{flex: 1}}
                    resizeMode={ "contain" }
                    source={{uri: imageUrl}}
                  />
                </View>
              );
            }) }

          </ScrollView>
        </View>

        <CheckoutFooter
          type={ "BUS" }
          currency={ checkoutData.currency }
          totalPrice={ checkoutData.totalPrice }
          digitDelimiter={ checkoutData.digitDelimiter }
          days={ checkoutData.days }
          isDisabled={ checkoutData.isDisabled }
          hasNext={ checkoutData.hasNext }
          listCount={ checkoutData.listCount }
          onButtonPress={ () => {
            if(checkoutData.passangerLimit < checkoutData.passangerCount) {
              Alert.alert(
                'Info',
                'Anda harus menambah Bus lagi untuk menampung jumlah penumpang sebanyak '+checkoutData.passangerCount+' Orang.', 
                [
                  {text: 'Tambah Bus', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},  
              )
            } else {
              checkoutData.onButtonPress()
            }
          }}
          noShadow
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderFilterModal(priceData, typeData) {
    return (
      <BusFilter
        values={ this.state.filters.types }
        isVisible={ this.state.isFilterModalVisible }
        currency={ this.state.selectedCurrency.symbol }
        digitDelimiter={ this.state.selectedCurrency.digitDelimiter }
        onClosePress={ () => this.setState({isFilterModalVisible: false}) }
        onFilterSet={ (filterData) => this.setState({
          isFilterModalVisible: false,
          filters: {
            price: filterData.price,
            types: filterData.types,
          },
        }) }
      />
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const { loadingExtras, loadingStock, loadingPrice, loadingCarType } = this.props;
    const navParam = this.state.navData.viewData;
    const viewData = navParam.trips[navParam.tripIndex];

    return (
      <VehicleListContainer
        { ... this.props }
        title={ this.state.navData.viewData.title }
        direction={{
          from: viewData.fromLabel,
          to: viewData.toLabel,
        }}
        checkoutFooter={ this._renderCheckoutFooter(this.state.selectedCurrency.symbol) }
        sort={{
          options: BusSortingConfigs,
          selected: this.state.selectedSort,
          onSelect: (sort) => this.setState({selectedSort: sort,}) 
        }}
        // currency={{
        //   options: CurrencyConfigs,
        //   selected: this.state.selectedCurrency,
        //   onSelect: (curr) => this.setState({selectedCurrency: curr,}),
        // }}
        filter={{
          modal: this._renderFilterModal(),
          toggle: (isVisible) => this.setState({isFilterModalVisible: isVisible}),
        }}
      >
       
        { !this.state.isEmpty ? this._renderList() :
        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 20, paddingBottom: 20 }}>
          <Ionicons name="md-search" size={32} />
          <Text>{Lang.dataNotFound[this.props.lang]}</Text>
        </View>}

        { (loadingExtras || loadingStock || loadingPrice || loadingCarType) && <View style={{ marginTop: 20 }}>
          <ActivityIndicator size="small" color="#2A2E36" />
        </View> }
      
      </VehicleListContainer>
    )
  }

}

const mapStateToProps = ({ masterCarType, masterExtras, stockList, priceList, productService, masterBranch, busZone, language }) => {
  const loadingExtras = masterExtras.loading;
  const loadingStock = stockList.loading;
  const loadingPrice = priceList.loading;
  const loadingCarType = masterCarType.loading;
  const { extras, tempExtras } = masterExtras;
  const { stock, requestStock, tempStock, tempStockComplete } = stockList;
  const { priceForBus } = priceList;
  const { products, selectedProduct } = productService;
  const { lang } = language;
  const { actualBranch } = masterBranch;
  const { zone } = busZone;
  return { extras, tempExtras, requestStock, stock, tempStock, priceForBus, products, tempStockComplete, loadingExtras, loadingStock, loadingPrice, loadingCarType, selectedProduct, actualBranch, zone, lang };
}

export default connect(mapStateToProps, {
  setTempExtras,
  getStock,
  setTempStock,
  setTempStockComplete,
  getPriceForBus
})(BusList);