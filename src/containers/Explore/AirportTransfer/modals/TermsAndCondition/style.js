import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingBottom: 20,
  },

  title: {
    marginBottom: 20,
  },

  main: {
    contentContainer: {
      padding: 10,
    },
  },
}