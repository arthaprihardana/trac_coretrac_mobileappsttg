/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 14:59:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 15:01:34
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import ModalContainer from "@airport-transfer-fragments/ModalContainer";

import styles from "./style";

import CarFilterData from "@airport-transfer-configs/data/CarFilter";

import NumberHelper from "@airport-transfer-helpers/Number";
import Lang from '../../../../../assets/lang'


class TermsAndCondition extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ModalContainer
        isVisible={ this.props.isVisible }
        onClosePress={ () => this.props.onClosePress() }
      >
        <View style={ styles.container }>

          <Text size={ 23 } style={ styles.title }>
            {Lang.termsAndConditions[this.props.lang]}
          </Text>

          <ScrollView contentContainerStyle={ styles.main.contentContainer} showsVerticalScrollIndicator={false}>
            <Text color={ appVariables.colorGray }>
              { this.props.children }
            </Text>
          </ScrollView>

          <MainButton
            label={ Lang.iAgree[this.props.lang] }
            rounded
            onPress={ () => this.props.onAgreePress() }
          />
        </View>

      </ModalContainer>
    );
  }

}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(TermsAndCondition)