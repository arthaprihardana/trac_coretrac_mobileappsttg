/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 15:58:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 11:57:23
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";
import _ from "lodash";

import FilterModalContainer from "@airport-transfer-fragments/FilterModalContainer";
import FilterCheckList from "@airport-transfer-fragments/FilterCheckList";
import FilterSlider from "@airport-transfer-fragments/FilterSlider";

import styles from "./style";

import CarFilterData from "@airport-transfer-configs/data/CarFilter";

import NumberHelper from "@airport-transfer-helpers/Number";
import Lang from "../../../../../assets/lang";

class CarFilter extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
    
    this.state = {
      price: {
        // min: props.priceValue.min,
        // max: props.priceValue.max,
        // defaultMin: props.priceValue.defaultMin,
        // defaultMax: props.priceValue.defaultMax,
        min: props.priceValue.min < props.priceValue.defaultMin || props.priceValue.min >= props.priceValue.defaultMax ? props.priceValue.defaultMin : props.priceValue.min,
        max: props.priceValue.max > props.priceValue.defaultMax || props.priceValue.max <= props.priceValue.defaultMin ? props.priceValue.defaultMax : props.priceValue.min,
        defaultMin: props.priceValue.defaultMin,
        defaultMax: props.priceValue.defaultMax,
        step: 10000,
        // min: props.priceValue.min < CarFilterData.price.min || props.priceValue.min >= CarFilterData.price.max ? CarFilterData.price.min : props.priceValue.min,
        // max: props.priceValue.max > CarFilterData.price.max || props.priceValue.max <= CarFilterData.price.min ? CarFilterData.price.max : props.priceValue.min,
      },

      // types: CarFilterData.types.map(item => {
      //   if (props.typeValues.length == 0 || props.typeValues.indexOf(item.value) !== -1) {
      //     return item.value;
      //   };
      // }),
      types: props.carType !== null ? props.carType.map(item => {
        if (props.typeValues.length == 0 || props.typeValues.indexOf(item.value) !== -1) {
          return item.value;
        };
      }) : [],
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      if (!this.props.isVisible && nextProps.isVisible) {
        this.setState({
          price: {
            min: nextProps.priceValue.min < this.props.priceValue.defaultMin || nextProps.priceValue.min >= this.props.priceValue.defaultMax ? this.props.priceValue.defaultMin : nextProps.priceValue.min,
            max: nextProps.priceValue.max > this.props.priceValue.defaultMax || nextProps.priceValue.max <= this.props.priceValue.defaultMin ? this.props.priceValue.defaultMax : nextProps.priceValue.max,
            defaultMin: this.props.priceValue.defaultMin,
            defaultMax: this.props.priceValue.defaultMax,
            // min: nextProps.priceValue.min < CarFilterData.price.min || nextProps.priceValue.min >= CarFilterData.price.max ? CarFilterData.price.min : nextProps.priceValue.min,
            // max: nextProps.priceValue.max > CarFilterData.price.max || nextProps.priceValue.max <= CarFilterData.price.min ? CarFilterData.price.max : nextProps.priceValue.max,
          },

          // types: CarFilterData.types.map(item => {
          //   if (nextProps.typeValues.length == 0 || nextProps.typeValues.indexOf(item.value) !== -1) {
          //     return item.value;
          //   };
          // }),
          types: this.props.carType !== null ? this.props.carType.map(item => {
            if (nextProps.typeValues.length == 0 || nextProps.typeValues.indexOf(item.value) !== -1) {
              return item.value;
            };
          }) : [],
        });
      }

      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  setType(valueKey, value) {
    const types = this.state.types.map(type => {
      if (type != valueKey) {
        return type;
      }
    });

    if (value) {
      types.push(valueKey);
    }

    this.setState({
      types: _.filter(types, v => v !== undefined)
    })
  }

// ---------------------------------------------------

  setPrice(values) {
    const price = {
      min: values[0],
      max: values[1],
      defaultMin: this.props.priceValue.defaultMin,
      defaultMax: this.props.priceValue.defaultMax
    };

    this.setState({price})
  }

// ---------------------------------------------------

  resetFilter() {
    this.setState({
      price: {
        // min: CarFilterData.price.min,
        // max: CarFilterData.price.max,
        min: this.props.priceValue.defaultMin,
        max: this.props.priceValue.defaultMax,
      },
      // types: CarFilterData.types.map(item => item.value),
      types: this.props.carType.map(item => item.value),
    })
  }

// ---------------------------------------------------

  applyFilter() {
    this.props.onFilterSet({
      price: this.state.price,
      types: this.state.types,
    })
  }

// ---------------------------------------------------

  handleClearCarType() {
    this.setState({
      types: _.map(this.props.carType, v => v.value)
    });
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderPrice() {
    return (
      <FilterSlider
        title={ Lang.price[this.props.lang] }
        options={ this.state.price }
        value={ this.state.price }
        step={ this.state.price.step }
        onClear={ () => this.setState({
          price: {
            min: this.props.priceValue.defaultMin, 
            max: this.props.priceValue.defaultMax,
            defaultMin: this.props.priceValue.defaultMin, 
            defaultMax: this.props.priceValue.defaultMax,
          }
        }) }
        onValuesChange={ (values) => this.setPrice(values) }
        valueModifier={ (value) => this.props.currency + " " + NumberHelper.format(value, this.props.digitDelimiter) }
      />
    );
  }

// ---------------------------------------------------

  _renderType() {
    // let carType = _.map(this.props.carType, v => {
    //   return {
    //     label: v.Name,
    //     value: v.CarTypeId,
    //   }
    // })
    return (
      <FilterCheckList
        title={ Lang.carType[this.props.lang] }
        // options={ CarFilterData.types }
        options={ this.props.carType }
        values={ this.state.types }
        onClear={ () => this.handleClearCarType() }
        onValueToggle={ (valueKey, value) => this.setType(valueKey, value) }
      />
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <FilterModalContainer
        onReset={ () => this.resetFilter() }
        onDone={ () => this.applyFilter() }
        isVisible={ this.props.isVisible }
        onClosePress={ () => this.props.onClosePress() }
      >

        <View style={ styles.segmentContainer }>
          { this._renderPrice() }
        </View>

        { this._renderType() }
        
      </FilterModalContainer>
    );
  }

}

const mapStateToProps = ({ masterCarType, language }) => {
  const { carType } = masterCarType;
  const { lang } = language;
  return { carType, lang }
}

export default connect(mapStateToProps, {})(CarFilter);