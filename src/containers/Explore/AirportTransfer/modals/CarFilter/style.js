import {
	appVariables,
  appMetrics,
} from "@variables";

export default {	
	segmentContainer: {
		borderBottomWidth: 1,
		borderColor: "#cccccc",
		marginBottom: 40
	},
}