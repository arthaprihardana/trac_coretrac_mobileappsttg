export default [
    {
        "id": 1,
        "title": "Asuransi",
        "content": [
            "Semua kendaraan kami telah diasuransikan kepada perusahaan asuransi (selanjutnya disebut sebagai “Pihak Asuransi”) yang ditunjuk oleh TRAC dengan Asuransi Comprehensive sesuai dengan ketentuan yang tercantum dalam polis asuransi",
            "Asuransi ini berlaku untuk semua layanan penyewaan kendaraan TRAC, baik sewa kendaraan dengan pengemudi maupun sewa kendaraan saja, yang cakupan, nominal penggantian, dan lain sebagainya diatur dalam Syarat dan Ketentuan ini",
            "Asuransi ini bersifat wajib dan melekat bersama layanan kami. Dengan menggunakan layanan TRAC, Anda dianggap telah menyetujui ketentuan asuransi ini. Oleh karena itu, kami menyarankan Anda untuk membaca secara detail Syarat & Ketentuan Asuransi ini sebelum melakukan pemesanan layanan",
            "Proteksi asuransi dimulai ketika Anda sebagai penyewa mulai menggunakan layanan serta kendaraan TRAC, dan otomatis berakhir mengikuti akhir masa sewa Anda",
            "Asuransi ini mencakup Third Party Liabilities (Pertanggungan Pihak Ketiga), Passenger Legal Liability (Pertanggungan Atas Harta Benda), Personal Accident (Kecelakaan Diri Perorangan), dan Total Lost Risk (Kehilangan Total)"
        ]
    },
    {
        "id": 2,
        "title": "Ketentuan Penggunaan",
        "content": [
            "Website ini dan Layanan yang tersedia di dalamnya dapat digunakan oleh Anda hanya untuk penggunaan pribadi dan atau secara non-komersial serta setiap saat tunduk pada Syarat & Ketentuan juga Kebijakan Privasi Kami",
            "Segala informasi (berbentuk teks, foto, video, audio, gambar, logo, ilustrasi lain, ikon, data, software, script, interfaced, dan lainnya) yang terdapat atau terkandung dalam situs web TRAC dimiliki oleh Kami dan tidak boleh dipergunakan tanpa sepengetahuan dan persetujuan TRAC, kecuali untuk penggunaan yang secara tegas diijinkan dan diperbolehkan dalam Syarat & Ketentuan juga Kebijakan Privasi Kami",
            "Kami berhak menarik, mengubah Layanan, informasi atau tampilan situs web TRAC demi memberi pelayanan yang lebih baik. Jika dibutuhkan sewaktu-waktu, Kami dapat membatasi akses Anda ke beberapa bagian dari website atau keseluruhan dari Layanan situs web TRAC",
            "Kami berhak mengolah informasi Anda yang tersimpan dalam database Kami sesuai dengan kebijakan yang tercantum dalam Kebijakan Privasi. Dengan menggunakan Layanan Kami, Anda setuju atas pemrosesan tersebut dan menjamin semua data yang diberikan adalah benar"
        ]
    },
    {
        "id": 3,
        "title": "Ketentuan Umum Layanan TRAC",
        "content": [
            "Dalam website TRAC ini, Anda dapat mencari informasi terkait produk dan layanan TRAC serta melakukan pemesanan/reservasi secara online untuk layanan Rental Mobil Harian, Airport Transfer, dan Sewa Bus. Anda juga dapat melakukan transaksi pembayaran secara online atas reservasi tersebut dengan aman melalui berbagai sistem dan fasilitas pembayaran yang tersedia di website Kami",
            "Untuk  melakukan reservasi online layanan Rental Mobil, Airport Transfer, dan Sewa Bus, Anda harus terlebih dulu mendaftarkan diri menjadi member TRAC melalui website TRAC dengan mengisi data diri berupa nama dan alamat email. Lalu, Anda juga wajib melengkapi persyaratan tertentu untuk menggunakan Layanan TRAC",
            "Layanan-layanan di atas secara umum dapat diakses via online setiap hari selama dua puluh empat jam; kecuali dalam hal adanya perbaikan, peningkatan atau pemeliharaan pada website Kami.",
            "Meski demikian, reservasi Layanan tersebut melalui website hanya bisa diakses untuk pemakaian paling cepat satu hari kerja ke depan. Jadi, Anda harus melakukan pemesanan minimal satu hari kerja sebelum hari pemakaian Layanan"
        ]
    },
    {
        "id": 4,
        "title": "Ketentuan Layanan Rental Mobil Harian Online",
        "content": [
            "Rental Mobil Harian TRAC adalah layanan sewa mobil berbasis pemakaian harian yang dapat diakses oleh masyarakat luas. Pilihan layanan Rental Mobil Harian TRAC terbagi menjadi dua, yaitu rental mobil dengan pengemudi (Chauffeur) dan rental mobil lepas kunci atau tanpa pengemudi (Self Drive).",
            "Anda dapat melakukan reservasi layanan Rental Mobil Harian TRAC melalui berbagai channel yang telah kami sediakan, yaitu secara online via website dan aplikasi TRAC (dapat diunduh di App Store dan Play Store) maupun secara offline via email ke rco.nasional@trac.astra.co.id, telpon ke Customer Assistance Center 1500009, atau dengan datang langsung ke cabang dan outlet TRAC",
            "Anda dapat melakukan pemesanan Rental Mobil Harian (Chauffeur dan Self Drive) ke tujuan manapun, untuk perjalanan dalam kota maupun luar kota",
            "Syarat menggunakan layanan tersebut adalah dengan melengkapi dokumen sebagai berikut: Layanan Chauffeur: KTP atau paspor (khusus WNA) Layanan Self Drive: KTP dan SIM A; atau paspor dan SIM Internasional (khusus WNA)",
            "Anda juga dapat melakukan pemesanan Rental Mobil Harian untuk orang lain, seperti keluarga atau teman Anda, dengan syarat Anda wajib menginformasikan data penumpang (khusus Chauffeur). Jika menggunakan Layanan Self Drive untuk orang lain, Anda wajib menginformasikan data diri pengguna mobil dan pengendara mobil wajib memiliki SIM A. Dokumen ini juga harus ditunjukan saat pengambilan mobil",
            "Cara melakukan pemesanan/reservasi online untuk layanan Rental Mobil Harian TRAC adalah dengan mengakses laman reservasi “Rental Mobil”, pilih jenis layanan (Chauffeur atau Self Drive), pilih kota dan alamat penjemputan, pilih paket rental, lalu pilih tanggal dan waktu penjemputan, kemudian klik “Cari”. Anda akan diarahkan ke laman hasil pencarian yang berisi sejumlah pilihan mobil beserta detail harga dan keterangan lainnya",
            "Paket rental Chauffeur yang bisa Anda pilih adalah 4 jam atau 12 jam. Harga yang tertera untuk Layanan ini sudah termasuk pajak, sewa mobil dan pengemudi serta asuransi. Sementara itu, pilihan paket rental Self Drive adalah minimal satu hari dengan harga yang tertera termasuk pajak, sewa mobil, dan asuransi",
            "Dalam laman hasil pencarian, Anda dapat melakukan penyesuaian pada “Filter” berdasarkan rentang harga maupun jenis mobil yang Anda inginkan. Anda juga bisa melakukan sortir hasil pencarian untuk memudahkan Anda mencari mobil yang Anda butuhkan.",
            "Setelah memilih mobil, Anda akan masuk ke proses “Order Mobil” di mana Anda dapat memilih beberapa mobil maupun menambah layanan lain yang tersedia, misalnya tambahan jam sewa, trip luar kota, atau pilihan lain yang tertera pada laman ini. Anda juga dapat melihat total pembayaran dari pemesanan Anda",
            "Setelah Anda memilih “Order Mobil”, Anda akan memasuki laman “Data Pribadi” yang berisi informasi seperti nama lengkap, nomor telepon, nomor KTP, dan alamat domisili. Selanjutnya Anda wajib mengunggah foto KTP dan SIM (khusus Self Drive). Jika semua “Data Pribadi” sudah terisi dengan benar, Anda lanjut ke halaman “Pembayaran”. Anda dapat memilih jenis pembayaran yang tersedia, yaitu dengan kartu kredit atau Virtual Account (VA). Kemudian Anda harus mengisi semua informasi yang dibutuhkan sesuai dengan metode pembayaran yang Anda pilih",
            "Cek kembali semua detail pemesanan Anda sebelum melakukan pembayaran. Jika pemesanan telah sesuai, Anda akan kami minta untuk membaca Syarat & Ketentuan ini. Proses pembayaran dapat dilanjutkan setelah Anda menyetujui Syarat & Ketentuan ini",
            "Jika Anda memilih metode pembayaran Virtual Account (VA), Anda akan mendapatkan nomor VA untuk menyelesaikan pembayaran. Anda wajib melunasi pembayaran sebelum jangka waktu yang tertera dengan mentransfer dana melalui mobile banking, internet banking, ATM, maupun teller. Setelah pembayaran berhasil, Anda akan mendapatkan nomor reservasi, detail pemesanan, dan verifikasi melalui email yang terdaftar di akun TRAC Anda",
            "Jika selama masa sewa Layanan Chauffeur maupun Self Drive terdapat kelebihan durasi pemakaian, Anda akan dikenai biaya tambahan yang dihitung per jam (add hour) dengan jumlah biaya bergantung pada jenis mobil yang Anda gunakan",
            "Jika Anda harus menginap atau perjalanan Anda melebihi 16 jam (hanya saat menggunakan Layanan Chauffeur), Anda akan dikenakan Overnight Lodging Cost (OLC) atau biaya menginap. Dengan OLC ini, Anda tidak perlu memberikan fasilitas penginapan untuk pengemudi Kami",
            "Perjalanan trip keluar kota (out of town trip) menggunakan Layanan Chauffeur akan dikenakan biaya tambahan. Trip luar kota adalah perjalanan yang berjarak lebih dari atau sama dengan 100 km dari titik penjemputan. Sedangkan trip keluar kota untuk Layanan Self Drive tidak dikenakan biaya tambahan, selama durasi pemakaian mobil tidak melebihi paket rental yang dipilih saat reservasi",
            "Khusus Layanan Self Drive, Anda harus mengambil dan mengembalikan mobil ke cabang TRAC tempat Anda memesan. Untuk pengembalian mobil harus dilakukan paling lambat 15 menit setelah masa sewa berakhir. Jika waktu keterlambatan pengembalian melebihi batas waktu ini, Anda akan dikenakan biaya tambahan yang dihitung per jam (add hour) dengan jumlah biaya bergantung pada jenis mobil yang Anda gunakan",
            "Meski demikian, Anda dapat mengajukan permintaan pengantaran maupun pengembalian mobil (ekspedisi) oleh TRAC saat melakukan reservasi Layanan Self Drive. Kami akan mengantar mobil ke lokasi Anda. Setelah masa sewa berakhir, kami juga bisa mengambil mobil dari lokasi Anda sehingga Anda tidak perlu mengembalikan mobil ke cabang TRAC. Layanan ini dikenakan biaya ekspedisi sejumlah Rp 50.000 (harga berlaku pada jam kerja: 08.00-17.00, Senin-Jumat) untuk satu kali pengantaran atau pengembalian mobil dengan jarak maksimal 25 km. Jika Anda ingin menggunakan layanan pengantaran sekaligus pengembalian mobil, biaya ekspedisi menjadi 2 x Rp 50.000",
            "Metode pembayaran untuk tambahan waktu sewa maupun biaya-biaya di atas, yang tidak tercatat dalam reservasi awal, dapat dilakukan langsung melalui mesin EDC (oleh staf TRAC) atau pembayaran melalui transfer ke nomor rekening TRAC. Kami akan menghubungi Anda lebih lanjut untuk informasi jumlah pembayaran tambahan ini",
            "Anda dapat melakukan reschedule maupun perubahan jenis mobil dengan membatalkan terlebih dulu reservasi yang sudah diproses. Pembatalan secara online hanya dapat dilakukan melalui aplikasi TRAC. Namun Anda juga bisa mengajukan pembatalan dengan menelepon Customer Assistance Center 1500009. Setelah pembatalan dilakukan, Anda dapat melakukan pemesanan kembali melalui channel reservasi yang telah Kami sebutkan sebelumnya"
        ]
    }
]