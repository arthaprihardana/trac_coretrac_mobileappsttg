/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-13 14:59:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 14:21:49
 */
import React, { Component } from 'react';
import styles from './style';
import {
  appVariables,
} from "@variables";
import { 
  ScrollView,
  TouchableOpacity,
  View,
  Image,
  KeyboardAvoidingView,
  Platform
} from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import moment from "moment";
import "moment/locale/id";

import ModalContainer from "@airport-transfer-fragments/ModalContainer";
import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import TextInput from '@airport-transfer-fragments/TextInput';

import OrderReviewTncData from '@airport-transfer-configs/data/OrderReviewTnc';

import NumberHelper from "@airport-transfer-helpers/Number";
import { setNotes, setTotalPriceAfterDiscount } from "../../../../../actions";
import { CAR_RENTAL, AIRPORT_TRANSFER, BUS_RENTAL } from '../../../../../constant';
import Lang from '../../../../../assets/lang';
import TNC from './tnc';

class OrderReview extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      note: props.notes || "",
      isNoteEditabe: false,

      activeTncIndex: -1,
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      return true;
    }
    
    return false;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderCarRow(vehicle, index) {
    let serviceName = null;
    switch (this.props.selectedProduct) {
      case AIRPORT_TRANSFER:
        var x = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
        var y = _.filter(x[0].product_service, { ProductServiceId: this.props.tempForm.type.value });
        serviceName = y[0].ProductServiceName;
        break;
      case BUS_RENTAL:
        serviceName = "Bus Rental";
        break;
      default:
        var x = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
        var y = _.filter(x[0].product_service, { ProductServiceId: this.props.tempForm.type.value });
        serviceName = y[0].ProductServiceName;
        break;
    }
    return (
      <View
        key={ index }
        style={ styles.vehicleList.container }
      >
        <View style={ styles.vehicleList.image.container }>
            <Image
              resizeMode={ "contain" }
              source={{ uri: vehicle.vehicleImage }}
              style={ styles.vehicleList.image.image }
            />
        </View>

        <View style={ styles.vehicleList.detail.container }>
          <Text color={ "#999999" }>
            Car #{ index + 1 }
          </Text>

          <Text size={ 18 }>
            { vehicle.vehicleAlias !== null ? vehicle.vehicleAlias : vehicle.vehicleTypeDesc }
          </Text>

          <Text>{serviceName}</Text>
        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderCars() {
    if (!this.props.isReviewOnly) {
      return null;
    }

    return (
      <View
        style={ styles.vehicleList.grouper }
      >

        { this.props.list.map((item, index) => {
          return this._renderCarRow(item.vehicle, index);
        }) }
        
      </View>
    );
  }

// ---------------------------------------------------

  _renderPaymentDetailRow(title, values = [], index) {
    
    return (
      <View
        key={ index }
        style={ styles.paymentDetail.container }
      >
        <Text>
          { title }
        </Text>

        <View style={ styles.paymentDetail.value.grouper }>

          { values.map((value, valIndex) => {
            
            let durationMultiplier = "";
            if (!value.isBoolean) {
              durationMultiplier = this.props.selectedProduct === CAR_RENTAL ? " x " + value.duration + " " + value.unit + "(s)" : "";
            }

            return (
              <View key={ valIndex } style={ styles.paymentDetail.value.container }>
                <View style={ styles.paymentDetail.value.label.container }>
                  <Text style={ value.priceAfterDiscount !== undefined ? { textDecorationLine: "line-through", textDecorationStyle: "solid", textDecorationColor: "red" } : {} }>
                    { value.type !== "main" ? `${value.label} : ` : `` }
                    { `${this.props.currency} ${NumberHelper.format(value.price)}` }
                    { value.type === "main" ? ` @${value.uom} ${durationMultiplier}`: (!value.isBoolean) ? ` x ${value.amounts} item` : `` }
                  </Text>
                  { value.priceAfterDiscount !== undefined && 
                  <Text>
                    { value.type !== "main" ? `${value.label} : ` : `` }
                    { `${this.props.currency} ${NumberHelper.format(value.priceAfterDiscount)}` }
                    { value.type === "main" ? ` @${value.uom} ${durationMultiplier}`: (!value.isBoolean) ? ` x ${value.amounts} item` : `` }
                  </Text> }
                </View>

                <Text>
                  { value.priceAfterDiscount !== undefined ? 
                    (this.props.selectedProduct === CAR_RENTAL ? this.props.currency + " " + NumberHelper.format((value.priceAfterDiscount * value.duration), this.props.digitDelimiter) : this.props.currency + " " + NumberHelper.format((value.priceAfterDiscount), this.props.digitDelimiter) ) :
                    value.amounts !== undefined ? 
                      this.props.currency + " " + NumberHelper.format((value.price * value.amounts), this.props.digitDelimiter) :
                      (this.props.selectedProduct === CAR_RENTAL ? this.props.currency + " " + NumberHelper.format((value.price * value.duration), this.props.digitDelimiter) : this.props.currency + " " + NumberHelper.format((value.price), this.props.digitDelimiter)) }
                </Text>
              </View>
            );
          }) }

        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderMainDetailRow(title, values = [], index) {
    return (
      <View
        key={ index }
        style={ styles.mainDetail.container }
      >
        <Text>
          { title }
        </Text>

        <View style={ styles.mainDetail.value.grouper }>

          { values.map((value, valIndex) => {
            
            return (
              <Text key={ valIndex } style={ styles.mainDetail.value.text }>
                { value }
              </Text>
            );
          }) }

        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderMainDetails() {
    let from_location = null;
    let to_location = null;
    let pkg = null;
    let pickup_schedule = null;
    
    switch (this.props.selectedProduct) {
      case AIRPORT_TRANSFER:
        from_location = _.filter(this.props.tempForm.from_airport.options, { value: this.props.tempForm.from_airport.value });
        to_location = _.filter(this.props.tempForm.to_location.options, { value: this.props.tempForm.to_location.value });
        pkg = _.filter(this.props.tempForm.type.options, { value: this.props.tempForm.type.value });
        pickup_schedule = (new moment(`${this.props.tempForm.arrival_schedule.value.date} ${this.props.tempForm.arrival_schedule.value.time}`, "DD-MM-YYYY HH:mm:ss")).format('LLL')
        break;
      case CAR_RENTAL:
        from_location = _.filter(this.props.tempForm.from_location.options, { value: this.props.tempForm.from_location.value });
        pkg = _.filter(this.props.tempForm.package.options, { value: this.props.tempForm.package.value });
        pickup_schedule = pkg[0].value <= 12 ? 
          (new moment(`${this.props.tempForm.pickup_schedule.value.date} ${this.props.tempForm.pickup_schedule.value.time}`, "DD-MM-YYYY HH:mm:ss")).format('LLL') : 
          (new moment(`${this.props.tempForm.pickup_schedule.value.startDate} ${this.props.tempForm.pickup_schedule.value.startTime}`, "DD-MM-YYYY HH:mm:ss")).format('LLL');
        break;
      case BUS_RENTAL:
        from_location = _.filter(this.props.tempForm.from_location.options, { value: this.props.tempForm.from_location.value });
        to_location = _.filter(this.props.tempForm.to_location.options, { value: this.props.tempForm.to_location.value });
        pickup_schedule = (new moment(`${this.props.tempForm.pickup_schedule.value.startDate} ${this.props.tempForm.pickup_schedule.value.startTime}`, "DD-MM-YYYY HH:mm:ss")).format('LLL');
        break;
      default:
        break;
    }

    return (
      <View style={ styles.mainDetail.grouper }>
        { this.props.selectedProduct === CAR_RENTAL && 
        <View
          style={ styles.mainDetail.container }>
          <Text>
            {Lang.fromCity[this.props.lang].toUpperCase()}
          </Text>

          <View style={ styles.mainDetail.value.grouper }>
            <Text style={ styles.mainDetail.value.text }>
              { this.props.tempForm.from_city.value }
            </Text>
          </View>
        </View> }

        <View
          style={ styles.mainDetail.container }>
          <Text>
            {Lang.fromLocation[this.props.lang].toUpperCase()}
          </Text>

          <View style={ styles.mainDetail.value.grouper }>
            <Text style={ styles.mainDetail.value.text }>
              { from_location[0].label }
            </Text>
          </View>
        </View>

        { this.props.selectedProduct !== CAR_RENTAL && 
        <View
          style={ styles.mainDetail.container }>
          <Text>
            {Lang.toLocation[this.props.lang].toUpperCase()}
          </Text>

          <View style={ styles.mainDetail.value.grouper }>
            <Text style={ styles.mainDetail.value.text }>
              { to_location[0].label }
            </Text>
          </View>
        </View> }
        { this.props.selectedProduct !== BUS_RENTAL && 
        <View
          style={ styles.mainDetail.container }>
          <Text>
            {Lang.packages[this.props.lang].toUpperCase()}
          </Text>

          <View style={ styles.mainDetail.value.grouper }>
            <Text style={ styles.mainDetail.value.text }>
              { pkg[0].label }
            </Text>
          </View>
        </View> }

        <View
          style={ styles.mainDetail.container }>
          <Text>
            {Lang.pickupSchedule[this.props.lang]}
          </Text>

          <View style={ styles.mainDetail.value.grouper }>
            <Text style={ styles.mainDetail.value.text }>
              { pickup_schedule }
            </Text>
          </View>
        </View>

      </View>
    );
  }

// ---------------------------------------------------

  _renderNote() {
    let mainRender = <View style={ styles.note.form.main }>
      <Text>
        { this.state.note }
      </Text>
    </View>;
    let iconRender = <Icon name="pencil" color={ "#aaaaaa" } size={ 20 }/>;

    if (this.state.isNoteEditabe) {
      mainRender = <TextInput
        style={ styles.note.form.main }
        value={ this.state.note }
        onChangeText={ (value) => this.setState({note: value})}
      />;
      iconRender = <Icon name="check" color={ "#aaaaaa" } size={ 20 }/>;
    }

    return (
      <View style={ styles.note.container }>
        <Text>
          {Lang.notesRequest[this.props.lang].toUpperCase()}
        </Text>
        
        <View style={ [styles.note.form.container, this.state.isNoteEditabe && Platform.OS === "android" ? { height: 60 } : {} ] }>
          
          { mainRender }

          {/* <TouchableOpacity
            style={ styles.note.form.icon.container }
            onPress={ () => {
              if(!this.props.isReviewOnly) {
                if(this.state.isNoteEditabe) {
                  this.props.setNotes(this.state.note);
                }
                this.setState({isNoteEditabe: !this.state.isNoteEditabe}) 
              }
            }}> */}
          { !this.props.isReviewOnly &&  <TouchableOpacity
            style={ styles.note.form.icon.container }
            onPress={ () => {
              if(this.state.isNoteEditabe) {
                this.props.setNotes(this.state.note);
              }
              this.setState({isNoteEditabe: !this.state.isNoteEditabe}) 
            }}>
            { iconRender }
          </TouchableOpacity> }
        </View>

        <Text color={ "#aaaaaa" } size={ 12 }>{Lang.notesNote[this.props.lang]}</Text>

      </View>
    );
  }

// ---------------------------------------------------

  _renderPaymentDetail() {
    let totalPrice = 0;
    const vehicleRenders = this.props.list.map((item, index) => {
      const data = [];
      let disc = {};
      let dt = {
        label: "",
        type: "main",
        duration: this.props.days,
        unit: "Day",
        uom: this.props.selectedProduct === AIRPORT_TRANSFER ? "Trip" : "Package",
        price: item.vehicle.rentInfo.basePrice,
        isBoolean: false,
      }

      if(this.props.isPromoValid) {
        disc = this.props.discount;
        if(disc.is_all_vehicle === 1) { // all vehicle
          dt.priceAfterDiscount = this.handlePriceDetail(item, disc);
        } else {  // selected vehicle
          let vehicle = disc.vehicle_id;
          let filterVehicle = _.filter(vehicle, v => v.vehicle_id === item.vehicle.vehicleTypeId);
          if(filterVehicle.length > 0) {
            _.map(filterVehicle, v => {
              if(v.vehicle_id === item.vehicle.vehicleTypeId) {
                dt.priceAfterDiscount = this.handlePriceDetail(item, disc);
              }
            })
          }
        }
        data.push(dt)
      } else {
        data.push(dt)
      }
      
      if(data[0].priceAfterDiscount !== undefined) {
        var priceAfterDiscount = this.handlePriceDetail(item, disc);
        totalPrice += this.props.selectedProduct !== BUS_RENTAL ? priceAfterDiscount * this.props.days : priceAfterDiscount;
      } else {
        totalPrice += this.props.selectedProduct !== BUS_RENTAL ? item.vehicle.rentInfo.basePrice * this.props.days : item.vehicle.rentInfo.basePrice;
      }
      
      item.vehicle.extraItems
        .filter((ext) => (ext.ValueType == "counter" && ext.amounts > 0) || (ext.ValueType == "boolean" && !!ext.amounts))
        .map((ext, extIndex) => {
          data.push({
            amounts: ext.amounts,
            label: ext.name,
            type: "extra",
            duration: 1,
            unit: "item",
            price: parseInt(ext.price),
            isBoolean: ext.ValueType == "boolean",
          });
          totalPrice += parseInt(ext.price) * (ext.ValueType == "boolean" ? 1 : ext.amounts);
        })
      return this._renderPaymentDetailRow(item.vehicle.vehicleAlias || item.vehicle.vehicleTypeDesc, data, index);
    });
    
    return (
      <View>
        <Text>
          {Lang.paymentDetails[this.props.lang].toUpperCase()}
        </Text>
        
        <View style={ styles.paymentDetail.grouper }>

          { vehicleRenders }

          <View style={ styles.paymentDetail.total.container }>
              <Text>
                {Lang.totalPayment[this.props.lang]}
              </Text>
              <Text size={ 18 }>
                { this.props.currency } { NumberHelper.format(totalPrice, this.props.digitDelimiter) }
              </Text>
          </View>

        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderTermsAndConditionsRow(label, content, index) {
    let contentRender = null;
    if (this.state.activeTncIndex === index) {
      contentRender = <View style={ styles.tnc.content.container }>
        {/* <Text color={ "#999999" }>
          { content }
        </Text> */}
        { content.map((val, index) => <View key={index} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ width: '10%' }}>
            <Text key={index} color={ "#999999" }>{index+1}</Text>
          </View>
          <View style={{ width: '90%' }}>
            <Text key={index} color={ "#999999" }>{val}</Text>
          </View>
        </View> ) }
      </View>;
    }

    return (
      <View key={ index }>
        <TouchableOpacity
          style={ styles.tnc.container }
          onPress={ () => this.setState({activeTncIndex: this.state.activeTncIndex === index ? -1 : index}) }
        >
          <Text color={ appVariables.colorWhite } bold>
            { label }
          </Text>

          <Icon name="caret-right" color={ appVariables.colorWhite } size={ 18 }/>
        </TouchableOpacity>

        { contentRender }
      </View>
    );
  }

// ---------------------------------------------------

  _renderTermsAndConditions() {
    if (this.props.isReviewOnly) {
      return null;
    }

    return (
      <View style={ styles.tnc.grouper }>

        { TNC.map((item, index) => {
          return this._renderTermsAndConditionsRow(item.title.toUpperCase(), item.content, index);
        })}

        {/* { OrderReviewTncData.map((item, index) => {
          return this._renderTermsAndConditionsRow("TERMS AND CONDITION " + (index + 1), item.content, index);
        }) } */}

      </View>
    );
  }

// ---------------------------------------------------

  _renderTitle() {
    return (
      <Text size={ 25 } style={ styles.title.basic.text }>
        {Lang.orderReview[this.props.lang]}
      </Text>
    );
  }

// ---------------------------------------------------

  _renderFooter() {
    if (this.props.isReviewOnly) {
      return null;
    }

    let totalPrice = 0;
    this.props.list.map((item, index) => {
      totalPrice += this.props.selectedProduct !== BUS_RENTAL ? item.vehicle.rentInfo.basePrice * this.props.days : item.vehicle.rentInfo.basePrice;

      item.vehicle.extraItems
      .filter((ext) => (ext.ValueType == "counter" && ext.amounts > 0) || (ext.ValueType == "boolean" && !!ext.amounts))
        .map((ext, extIndex) => {
          totalPrice += parseInt(ext.price) * (ext.ValueType == "boolean" ? 1 : ext.amounts);
        });
    });

    return (
      <View style={ styles.footer.container }>
        <MainButton
          label={ Lang.continue[this.props.lang].toUpperCase() }
          onPress={ () => this.props.onConfirm({
            submittedData: this.props.submittedData,
            list: this.props.list,
            days: this.props.days,
            currency: this.props.currency,
            digitDelimiter: this.props.digitDelimiter,
            totalPrice: totalPrice,
            note: this.state.note,
          }) }
        />
      </View>
    );
  }

  handlePriceDetail = (item, disc) => {
    let price = 0;
    switch (disc.type_value) {
      case "nominal":
        price = item.vehicle.rentInfo.basePrice - parseInt(disc.value);
        break;
      default:  // persentasi
        price = item.vehicle.rentInfo.basePrice - (item.vehicle.rentInfo.basePrice * (parseInt(disc.value)/100));
        break;
    }
    return price;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ModalContainer
        isVisible={ this.props.isVisible }
        onClosePress={ () => this.props.onClosePress() }
      >
        { this._renderTitle() }

        <ScrollView style={ styles.outerContainer }>
          <View style={ styles.contentContainer }>
            { this._renderCars() }

            { this._renderMainDetails() }

            <View style={ styles.innerContainer }>
              { this._renderNote() }

              { this._renderPaymentDetail() }
            </View>

            { this._renderTermsAndConditions() }
          </View>
            
          { this._renderFooter() }
        </ScrollView>
      </ModalContainer>
    );
  }
}

const mapStateToProps = ({ temporaryForm, tempNotes, productService, discountPromo, language }) => {
  const { tempForm } = temporaryForm;
  const { notes } = tempNotes;
  const { selectedProduct, products } = productService;
  const { discount, isPromoValid } = discountPromo;
  const { lang } = language;
  return { tempForm, notes, selectedProduct, discount, isPromoValid, lang, products }
}

export default connect(mapStateToProps, {
  setNotes,
  setTotalPriceAfterDiscount
})(OrderReview);