import {
  appVariables,
  appMetrics,
} from '@variables';

export default {
  outerContainer: {
    flex: 1,
  },

  contentContainer: {
    flex: -1,
    minHeight: appMetrics.screenHeight - 150,
  },

  innerContainer: {
    paddingHorizontal: 20,
  },
  
  title: {
    basic: {
      text: {
        paddingHorizontal: 20,
        paddingTop: 10,
        paddingBottom: 30,
      },
    },
  },

  footer: {
    container: {
      flex: 1,
      paddingTop: 5,
    },
  },

  mainDetail: {
    grouper: {
      flex: -1,
    },

    container: {
      flex: -1,
      paddingVertical: 15,
      paddingHorizontal: 20,
      borderBottomWidth: 1,
      borderColor: "#aaaaaa",
    },

    value: {
      grouper: {
        flex: -1,
        paddingTop: 2,
      },

      text: {
        paddingTop: 5,
      },
    },  
  },

  note: {
    container: {
      paddingVertical: 20,
    },

    form: {
      container: {
        flex: -1,
        flexDirection: "row",
        paddingTop: 10,
        height: 40,
        borderBottomWidth: 1,
        borderColor: "#aaaaaa",
        marginBottom: 5,
      },

      main: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: "center",
      },

      icon: {
        container: {
          paddingLeft: 20,
        },
      },
    },
  },

  paymentDetail: {
    grouper: {
      flex: -1,
    },

    container: {
      flex: -1,
      paddingVertical: 15,
      borderBottomWidth: 1,
      borderColor: "#aaaaaa",
    },

    value: {
      grouper: {
        flex: -1,
        paddingTop: 2,
      },

      container: {
        flex: -1,
        flexDirection: "row",
        paddingTop: 5,
      },

      label: {
        container: {
          flex: 1,
        },
      },
    },

    total: {
      container: {
        flex: -1,
        paddingVertical: 15,
        flexDirection: "row",
        justifyContent: "space-between",
      },
    }
  },

  tnc: {
    grouper: {
      flex: -1,
    },

    container: {
      flex: -1,
      height: 50,
      flexDirection: "row",
      paddingVertical: 15,
      backgroundColor: "#0038A9",
      paddingHorizontal: 20,
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: 5,
    },

    content: {
      container: {
        padding: 20,
      },
    },
  },

  vehicleList: {
    grouper: {
      flex: -1,
      paddingHorizontal: 20,
      paddingVertical: 10,
    },

    container: {
      flex: -1,
      flexDirection: "row",
      height: 100,
      paddingVertical: 10,
    },

    image: {
      container: {
        flex: 1,
        backgroundColor: "#eeeeee",
      },

      image: {
        flex: 1,
      },
    },

    detail: {
      container: {
        flex: 1,
        justifyContent: "center",
        paddingLeft: 10,
      },
    },
  },
};



