import {
	appVariables,
} from '@variables';

export default {
  radio: {
    container: {
      flex: 1,
      fontSize: 12,
      flexDirection: 'row',
      paddingVertical: 17,
      // alignItems: 'center',
    },

    button: {
      outer: {
        borderWidth: 1,
        borderColor: appVariables.colorWhite,
        borderRadius: 50,
        width: 19,
        height: 19,
        marginRight: 12,
        justifyContent: 'center',
        alignItems: 'center',
        top: 3,
      },
      
      inner: {
        backgroundColor: appVariables.colorWhite,
        borderRadius: 50,
        width: 9,
        height: 9,
      },
    },
  },
}