import { Content } from 'native-base';
import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
} from 'react-native';
import {
  appVariables,
} from '@variables';

import styles from './style';

import ModalContainer from '@airport-transfer-fragments/MainFormModalContainer';
import Text from '@airport-transfer-fragments/Text';

export default class ModalRadio extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value != nextProps.value) {
      return true;
    }

    if (this.props.options != nextProps.options) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderRadio(isSelected = false) {
    return (
      <View style={ styles.radio.button.outer }>
        { isSelected ? <View style={ styles.radio.button.inner }/> : null }
      </View>
    );
  }

// ---------------------------------------------------

  _renderOption(label, value, note, index) {
    let noteRender = null;
    if (note) {
      noteRender = <Text color={ "#aaaaaa" }>
        { note }
      </Text>;
    }

    return (
      <TouchableOpacity 
        key={ index }
        onPress={ () => this.props.onSelectOptions(value) }
        style={ styles.radio.container }
      >

        { this._renderRadio(this.props.value == value) }

        <View>
          <Text color={ appVariables.colorWhite } size={ 20 }>
            { label }
          </Text>
          
          { noteRender }
        </View>
      </TouchableOpacity>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ModalContainer
        backgroundColor={ this.props.backgroundColor }
        onClosePress={ () => this.props.onClose() }
        onBackPress={ this.props.onBack ? () => this.props.onBack() : null }
        title={{
          type: "basic",
          text: this.props.title,
        }}
      >
        <Content style={{ flex: 1 }}>
          <View style={ styles.content }>
            {
              this.props.options.map((option, index) => {
                return this._renderOption(option.label, option.value, option.note, index);
              })
            }
          </View>
        </Content>
      </ModalContainer>
    );
  }
}