import {
	appVariables,
} from '@variables';

export default {
  contentContainer: {
    flex: 1,
  },

  form: {
    grouper: {
      flexDirection: "row",
      marginTop: 20,
    },

    container: {
      flex: 1,
    },

    input: {
      height: 60,
      borderBottomWidth: 1,
      borderColor: appVariables.colorWhite,
      color: appVariables.colorWhite,
      fontSize: 25,
    },
  },

  selectedFlight: {
    marginTop: 25,
  },
}