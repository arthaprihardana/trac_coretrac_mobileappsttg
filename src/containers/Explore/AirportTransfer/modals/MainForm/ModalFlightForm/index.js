import { Content } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { appVariables } from '@variables';
import { ARentTitle } from '@atoms';

import styles from './style';

import ModalContainer from '@airport-transfer-fragments/MainFormModalContainer';
import Text from '@airport-transfer-fragments/Text';
import TextInput from '@airport-transfer-fragments/TextInput';

export default class ModalFlightForm extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      name: props.value.name ? props.value.name : "",
      number: props.value.number ? props.value.number : "",
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value != nextProps.value) {
      return true;
    }

    if (this.props.options != nextProps.options) {
      return true;
    }

    if (this.state != nextState) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderForm(name, keyboardType, label, placeholder, value, isRight = false) {
    const containerStyle = [styles.form.container];

    if (isRight) {
      containerStyle.push({paddingLeft: 10});
    } else {
      containerStyle.push({paddingRight: 10});
    }

    return (
      <View style={ containerStyle }>
        <Text color={ appVariables.colorWhite }>
          { label }
        </Text>

        <TextInput
          style={ styles.form.input }
          placeholder={ placeholder }
          placeholderTextColor={ appVariables.colorTabInactive }
          value={ this.state[name] }
          autoCapitalize={ "characters" }
          keyboardType={ keyboardType }
          onChangeText={ (value) => this.setState({[name]: value}) }
        />
      </View>

    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let inputtedFlight = "";

    const filteredFlights = this.props.options
      .filter((option) => {
        return option.value == this.state.name;
      });

    if (filteredFlights.length > 0) {
      inputtedFlight = filteredFlights[0].label + " (" + filteredFlights[0].value + ")";
    }

    return (
      <ModalContainer
        backgroundColor={ this.props.backgroundColor }
        onClosePress={ () => this.props.onClose() }
        title={{
          type: "basic",
          text: this.props.title,
        }}
        footerButton={{
          label: "DONE",
          onPress: () => this.props.onSelectOptions({
            name: this.state.name,
            number: this.state.number,
          }),
        }}
      >
        <Content style={{ flex: 1 }}>          
          <View style={ styles.contentContainer }>

            <View
              style={ styles.form.grouper }
            >
              { this._renderForm("name", "default", "Name", "Name", null, false) }
              { this._renderForm("number", "numeric", "Number", "Number", null, true) }
            </View>

            <Text style={ styles.selectedFlight } color={ appVariables.colorTabInactive } size={ 14 }>
              { inputtedFlight }
            </Text>
          
          </View>
        </Content>
      </ModalContainer>
    );
  }
}