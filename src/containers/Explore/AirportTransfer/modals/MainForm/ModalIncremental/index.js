import { Content, Text } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity, View, TextInput } from 'react-native';
import { appVariables } from '@variables';
import { ARentTitle } from '@atoms';
import SvgIcon from '@utils/SvgIcon';
import AFooterButton from '@atoms/AFooterButton';

import styles from './style';

import ModalContainer from '@airport-transfer-fragments/MainFormModalContainer';

export default class ModalIncremental extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      value: props.value ? props.value : props.default,
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value != nextProps.value) {
      return true;
    }

    if (this.props.options != nextProps.options) {
      return true;
    }

    if (this.state != nextState) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderRadio(isSelected = false) {
    return (
      <View style={ styles.radio.button.outer }>
        { isSelected ? <View style={ styles.radio.button.inner }/> : null }
      </View>
    );
  }

// ---------------------------------------------------

  _renderButton(iconText, onPress) {
    return (
      <TouchableOpacity
        style={ styles.button.container }
        onPress={ () => onPress() }
      >
        <Text style={ styles.button.text }>{ iconText }</Text>
      </TouchableOpacity>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const min = this.props.min ? this.props.min : 0;
    const max = this.props.max ? this.props.max : 10000;

    return (
      <ModalContainer
        backgroundColor={ this.props.backgroundColor }
        onClosePress={ () => this.props.onClose() }
      >
        <Content style={{ flex: -1 }}>
          <ARentTitle title={ this.props.title } bgColor='transparent' />

          <View style={ styles.form.container }>
            <TextInput
              style={ styles.form.input }
              onChangeText={(text) => this.setState({passengerNumber: text})}
              value={ this.state.value + "" }
              keyboardType={ "numeric" }
              onChangeText={ (value) => this.setState({value: (value ? value : "0")}) }
            />

            <View style={ styles.button.grouper }>
              { this._renderButton("-", () => this.setState({value: (this.state.value > min ? ((this.state.value * 1) - 1) : min)})) }
              { this._renderButton("+", () => this.setState({value: (this.state.value < max ? ((this.state.value * 1) + 1) : max)})) }
            </View>

          </View>

          <AFooterButton onPress={ () => this.props.onSelectOptions(this.state.value + "") }>
            DONE      
          </AFooterButton>
        </Content>
      </ModalContainer>
    );
  }
}