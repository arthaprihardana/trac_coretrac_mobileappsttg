import {
	appVariables,
} from '@variables';

export default {
  form: {
    container: {
      marginHorizontal: 20,
    },

    input: {
      fontSize: 60,
      fontFamily: appVariables.museo700,
      alignSelf: 'center',
      color: appVariables.colorWhite,
      marginBottom: 0,
      width: '100%',
      textAlign: 'center',
    },
  },

  button: {
    grouper: {
      flex: 1,
      flexDirection: 'row',
      marginVertical: 20,
    },
    
    container: {
      flex: 1,
      borderWidth: 1,
      borderColor: appVariables.colorBorderBottom,
      padding: 20,
      textAlign: 'center',
    },

    text: {
      fontSize: 30,
      fontFamily: appVariables.museo700,
      color: appVariables.colorWhite,
      textAlign: 'center',
      alignSelf: 'center',
    },
  },
}