import {
  appVariables,
} from "@variables";

export default {
  time: {
    container: {
      flex: 1,
      flexDirection: "row",
      borderTopWidth: .5,
      borderBottomWidth: .5,
      borderTopColor: appVariables.colorTabInactive,
      borderBottomColor: appVariables.colorTabInactive,
      paddingTop: 20,
      paddingBottom: 20,
      marginTop: 10,
    },

    title: {
      container: {
        flex: -1,
      },

      text: {
        fontSize: 16,
        color: appVariables.colorWhite
      },
    },

    picker: {
      container: {
        flex: 1,
      },

      nativePicker: { 
        borderWidth: 0,
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
      },
    },
  },

  summary: {
    container: {
      flex: 1,
      paddingTop: 20,
      paddingBottom: 20,
      marginTop: 10,
    },

    innerContainer: {
      marginBottom: 30,
    },

    label: {
      marginBottom: 10,
    },

  },
}