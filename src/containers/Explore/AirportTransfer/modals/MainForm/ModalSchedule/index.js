/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 17:07:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-10 13:54:18
 */
import { Content } from "native-base";
import React, { Component } from "react";
import {
  View,
  Alert
} from "react-native";
import { 
  appVariables,
  appMetrics
} from "@variables";
import { FormatTwoDigit } from "@utils/Helpers";
import CalendarPicker from "react-native-calendar-picker";
import DatePicker from 'react-native-datepicker';
import moment from "moment";
import "moment/locale/id";
import { connect } from "react-redux";
import _ from "lodash";

import styles from "./style";

import ModalContainer from "@airport-transfer-fragments/MainFormModalContainer";
import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";
import { BUS_RENTAL } from "../../../../../../constant";
import Lang from '../../../../../../assets/lang'

class ModalSchedule extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      minDate: new moment(),
      minTime: new moment(),
      startDate: props.value.isRange ? 
        (props.value.startDate ? new moment(props.value.startDate, "DD-MM-YYYY") : new moment()) :
        (props.value.date ? new moment(props.value.date, "DD-MM-YYYY") : new moment()),
      endDate: props.value.isRange ? 
        (props.value.endDate ? new moment(props.value.endDate, "DD-MM-YYYY") : new moment()) :
        (new moment()).add("1", "day"),
      startTime: props.value.isRange ? 
        (props.value.startTime ? props.value.startTime : (new moment).format("HH:mm")) :
        (props.value.time ? props.value.time : (new moment).format("HH:mm")),
      endTime: props.value.isRange ? 
        (props.value.endTime ? props.value.endTime : (new moment).format("HH:mm")) :
        (new moment).format("HH:mm"),
    };
  }

// ---------------------------------------------------

  componentDidMount = () => {
    let f = _.filter(this.props.time, { MsProductId: this.props.selectedProduct });
    var toMin = moment.duration(f[0].MaxOrderTime).asMinutes();
    var setHour = moment().add(toMin, "minutes").format('HH:mm');
    this.setState({
      minDate: moment().add(toMin, "minutes"),
      minTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1]),
      startDate: moment().add(toMin, "minutes"),
      startTime: moment().hours(setHour.split(":")[0]).minutes(setHour.split(":")[1]).format("HH:mm")
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value != nextProps.value) {
      return true;
    }

    if (this.state != nextState) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onDateChange(date, type) {
    if (this.props.isRange && type === "END_DATE") {
      this.setState({
        endDate: date,
      });
    } else {
      this.setState({
        startDate: date,
        endDate: "",
      });
    }
  }

// ---------------------------------------------------

  onTimeChange(time, type) {
    if(this.props.selectedProduct === BUS_RENTAL) {
      if( moment(this.state.startDate.format('YYYY-MM-DD')+" "+time).isBefore(this.state.startDate.format('YYYY-MM-DD')+" 05:00:00") ) {
        Alert.alert(
          'Information',
          `Minimum Order Time is 05:00`,
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
      }
      if( moment(this.state.startDate.format('YYYY-MM-DD')+" "+time).isAfter(this.state.startDate.format('YYYY-MM-DD')+" 21:00:00") ) {
        Alert.alert(
          'Information',
          `Maximum Order Time is 21:00`,
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
      }
    }
    if( moment(this.state.startDate.format('YYYY-MM-DD')+" "+time).isBefore(this.state.minDate.format("YYYY-MM-DD HH:mm")) ) {
      Alert.alert(
        'Information',
        `Minimum Order Date is ${this.state.startDate.format("LLL")}`,
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
    } else {
      if (this.props.isRange) {
        if (type === "END_TIME") {
          this.setState({
            endTime: time,
          });
        } else {
          this.setState({
            startTime: time,
          });
        }
      } else {
        this.setState({
          startTime: time,
          endTime: "",
        });
      }
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderDate() {
    return (
      <CalendarPicker
        width={ appMetrics.screenWidth - appVariables.padding }
        onDateChange={ (date, type) => this.onDateChange(date, type) }
        textStyle={{ fontSize: 14, color: appVariables.colorWhite }}
        allowRangeSelection={ this.props.isRange }
        selectedRangeStyle={{ backgroundColor: "#465B95" }}
        selectedDayStyle={{ backgroundColor: "#465B95" }}
        selectedDayTextColor={ appVariables.colorWhite }
        todayTextStyle={{ color: appVariables.colorDark }}
        defaultTextStyles={{ fontFamily: appVariables.museo500 }}
        minDate={ this.state.minDate }
        selectedStartDate={ this.state.startDate ? this.state.startDate : null }
        selectedEndDate={ this.state.endDate ? this.state.endDate : null }
        previousTitle={ "<" }
        nextTitle={ ">" }
      />
    );
  }

// ---------------------------------------------------

  _renderTime(title, value, onChange) {
    return (
      <View style={ styles.time.container }>
        <View style={ styles.time.title.container }>
          <Text color={ appVariables.colorWhite } size={ 17 }>{ title }</Text>
        </View>
        <View style={ styles.time.picker.container }>
          <Text color={ appVariables.colorWhite } size={ 17 } bold alignRight>{ value }</Text>

          <DatePicker
            style={ styles.time.picker.nativePicker }
            date={ new moment(value, "HH:mm") }
            mode="time"
            showIcon={ false }
            hideText={ true }
            format="HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={ (time) => onChange(time) }
          />
        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderTimeSlots() {
    // let dropHourRender = null;
    // if (this.props.isRange) {
    //   dropHourRender = this._renderTime("Drop Hour", this.state.endTime, (time) => this.onTimeChange(time, "END_TIME"));
    // }

    return (
      <View>

        { this._renderTime(Lang.pickupHour[this.props.lang], this.state.startTime, (time) => this.onTimeChange(time, "START_TIME")) }

        {/* { dropHourRender } */}

      </View>
    );
  }

// ---------------------------------------------------

  _renderResult(title, date, time) {
    let formatted = "-";
    
    if (date) {
      formatted = (new moment(date + " " + time, "DD-MM-YYYY hh:mm")).format("LLL");
    }

    return (
      <View style={ styles.summary.innerContainer }>
        <Text style={ styles.summary.label } color={ appVariables.colorTabInactive } size={ 16 }>
          { title }
        </Text>

        <Text color={ appVariables.colorWhite } size={ 16 }>
          { formatted }
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderSummaries(startDate, endDate, startTime, endTime) {
    // let endRender = null;
    // if (this.props.isRange) {
    //   // endRender = this._renderResult("Drop Date and Time", endDate, endTime);
    //   endRender = this._renderResult("Drop Date", endDate);
    // }

    return (
      <View style={ styles.summary.container }>
        { this._renderResult(Lang.dateTime[this.props.lang], startDate, startTime) }
        
        {/* { endRender } */}
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const startDate = this.state.startDate ? this.state.startDate.format("DD-MM-YYYY") : "";
    const endDate = this.state.endDate ? this.state.endDate.format("DD-MM-YYYY") : startDate;
    const startTime = this.state.startTime;
    const endTime = this.state.endTime ? this.state.endTime : startTime;

    let submitFunction = () => {};
    if (this.props.isRange) {
      submitFunction = () => {
        switch (this.props.selectedProduct) {
          case BUS_RENTAL:
            if(this.props.zone !== null) {
              let duration = _.isEmpty(this.state.endDate) ? 0 : moment.duration(this.state.endDate.diff(this.state.startDate)).asDays();
              let range = duration + 1;
              if(range < parseInt(this.props.zone.MinimumDays)) {
                Alert.alert(
                  'Information',
                  `Minimum days is ${this.props.zone.MinimumDays} Days`,
                  [
                    {text: 'Choose Date?', onPress: () => console.log('OK Pressed')},
                  ],
                  {cancelable: false},
                );
              } else {
                this.props.onSelectOptions({
                  startDate: startDate,
                  endDate: endDate,
                  startTime: startTime,
                  endTime: endTime,
                });
              }
            }
            break;
          default:
            this.props.onSelectOptions({
              startDate: startDate,
              endDate: endDate,
              startTime: startTime,
              endTime: endTime,
            });
            break;
        }
      }
    } else {
      submitFunction = () => this.props.onSelectOptions({
        date: startDate,
        time: startTime,
      });
    }

    return (
      <ModalContainer
        backgroundColor={ this.props.backgroundColor }
        onClosePress={ () => this.props.onClose() }
        title={{
          type: "basic",
          text: this.props.title,
        }}
        footerButton={{
          label: Lang.done[this.props.lang],
          onPress: submitFunction,
        }}
      >
        <Content style={{ flex: 1 }}>          

          { this._renderDate() }

          { this._renderTimeSlots() }

          { this._renderSummaries(startDate, endDate, startTime, endTime) }
          
        </Content>
      </ModalContainer>
    );
  }
}

const mapStateToProps = ({ busZone, productService, adjustmentTime, language }) => {
  const { zone } = busZone;
  const { products, selectedProduct } = productService;
  const { time } = adjustmentTime;
  const { lang } = language;
  return { zone, products, selectedProduct, time, lang };
}

export default connect(mapStateToProps, {})(ModalSchedule);
