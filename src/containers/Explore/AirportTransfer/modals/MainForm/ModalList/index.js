/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 22:29:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-15 15:37:02
 */
import { Content } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity, View, FlatList, ActivityIndicator } from 'react-native';
import { appVariables } from '@variables';
import { AInputWithIcon, ASearchList } from '@atoms';
import { connect } from "react-redux";
import { getAddressFromGoogle } from "../../../../../../actions";
import _ from "lodash";

import styles from './style';

import ModalContainer from '@airport-transfer-fragments/MainFormModalContainer';
import Text from '@airport-transfer-fragments/Text';
import { AIRPORT_TRANSFER } from '../../../../../../constant';

class ModalList extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      searchText: ""
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value != nextProps.value) {
      return true;
    }

    if (this.props.options != nextProps.options) {
      return true;
    }

    if (this.state.searchText != nextState.searchText) {
      return true;
    }

    if(this.props.loading !== nextProps.loading) {
      return true;
    }

    return false;
  }

  componentDidUpdate = (prevProps, prevState) => {
    setTimeout(() => {
      if((this.state.searchText.length - 1) === prevState.searchText.length) {
        switch (this.props.formKey) {
          case "from_location":
            this.props.getAddressFromGoogle(`${this.state.searchText}+${_.replace(this.props.searchKey, /\s/g, "")}`)
            break;
          case "to_location":
            if(this.props.selectedProduct === AIRPORT_TRANSFER) {  // airport transfer
              this.props.getAddressFromGoogle(`${this.state.searchText}+${_.replace(this.props.searchKey, /\s/g, "")}`)
            } else {  // bus rental
              this.props.getAddressFromGoogle(`+${_.replace(this.state.searchText, /\s/g, "")}`)
            }
            break;
          default:
            break;
        }
      }
    }, 1000);
  }

// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  toggleSelected(value) {
    const _this = this;
    setTimeout(function() {
      _this.props.onClose()
    }, 200);
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderItem(item, index, type) {
    let fontSize = 25;
    let color = appVariables.colorTabInactive;
    
    if (type == 'small') {
      fontSize = 17;
    }

    if (item.value == this.props.value) {
      color = appVariables.colorWhite;
    }

    return (
      <TouchableOpacity
        style={{
          paddingVertical: 16,
        }}
        onPress={ () => this.props.onSelectOptions(item.value) }
        key={ index }
      >
        <Text color={ color } size={ fontSize }>
          { item.label }
        </Text>
      </TouchableOpacity>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let data = [];
    if(this.props.formKey === "from_location" || this.props.formKey === "to_location") {
      data = this.props.loading ? [] : this.props.options;
    } else {
      data = this.props.options.filter(option => option.label.toLowerCase().includes(this.state.searchText.trim().toLowerCase()))
    }
    
    return (
      <ModalContainer
        backgroundColor={ this.props.backgroundColor }
        onClosePress={ () => this.props.onClose() }
        onBackPress={ this.props.onBack ? () => this.props.onBack() : null }
        title={{
          type: "search",
          text: this.props.title,
          value: this.state.searchText,
          onChangeText: searchText => this.setState({ searchText })
        }}
      >
        <Content style={{ flex: -1 }}>
          <FlatList 
            data={ data }
            keyExtractor={ (item, index) => index.toString() }
            renderItem={ ({item, index}) => this._renderItem(item, index, this.props.size) }
            ListEmptyComponent={ () => this.props.loadingGoogle || this.props.loadingCities ? <ActivityIndicator size="small" color="#ffffff" style={{ marginTop: 20 }} /> : <View /> }
          />
        </Content>
      </ModalContainer>
    );
  }
}

const mapStateToProps = ({ addressFromGoogle, productService, cities }) => {
  const { address, searchKey } = addressFromGoogle;
  const { selectedProduct } = productService;
  const loadingCities = cities.loading;
  const loadingGoogle = addressFromGoogle.loading;
  return { address, searchKey, selectedProduct, loadingGoogle, loadingCities }
}

export default connect(mapStateToProps, {
  getAddressFromGoogle
})(ModalList);