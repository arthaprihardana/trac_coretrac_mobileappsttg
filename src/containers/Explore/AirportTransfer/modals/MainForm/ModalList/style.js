import {
	appVariables,
} from '@variables';

export default {
  container: { 
    flex: 1,
  },

  list: {
    label: {
      paddingTop: 20,
      paddingBottom: 20,
      flex: 1,
    },
  },
}