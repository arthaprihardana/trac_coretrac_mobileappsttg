/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 15:33:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 23:24:26
 */
import React, { Component } from "react";
import {
  Modal,
} from "react-native";
import { appVariables } from "@variables";
import styles from "./style";
import moment from "moment";
import "moment/locale/id";

import ModalRadio from "./ModalRadio";
import ModalList from "./ModalList";
import ModalFlightForm from "./ModalFlightForm";
import ModalAmount from "./ModalAmount";
import ModalSchedule from "./ModalSchedule";
import ModalIncremental from "./ModalIncremental";


export default class MainForm extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

    shouldComponentUpdate(nextProps, nextState) {
      if (nextProps != this.props) {
        return true;
      }

      return false;
    }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderModalContent(thisForm, forms, modalProperties) {
    let modal = null;
    switch (thisForm.modalType) {
      case "radio":
        modal = <ModalRadio { ...modalProperties } options={ !this.props.optionsModifier ? thisForm.options : this.props.optionsModifier(thisForm.options, forms)  }/>;
        break;

      case "list":
        modal = <ModalList { ...modalProperties } options={ !this.props.optionsModifier ? thisForm.options : this.props.optionsModifier(thisForm.options, forms)  } size={ "small" }/>;
        break;

      case "flight":
        modal = <ModalFlightForm { ...modalProperties } options={ !this.props.optionsModifier ? thisForm.options : this.props.optionsModifier(thisForm.options, forms)  }/>;
        break;

      case "amount":
        modal = <ModalAmount { ...modalProperties } options={ !this.props.optionsModifier ? thisForm.options : this.props.optionsModifier(thisForm.options, forms)  } default={ 100 } />;
        break;

      case "singleSchedule":
        modal = <ModalSchedule { ...modalProperties } options={ !this.props.optionsModifier ? thisForm.options : this.props.optionsModifier(thisForm.options, forms)  } />;
        break;

      case "rangeSchedule":
        modal = <ModalSchedule { ...modalProperties } options={ !this.props.optionsModifier ? thisForm.options : this.props.optionsModifier(thisForm.options, forms)  } isRange/>;
        break;

      case "incremental":
        modal = <ModalIncremental { ...modalProperties } default={ 100 }/>;
        break;
    }

    return modal;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const forms = this.props.formsState;
    const thisForm = forms[this.props.formKey];
    const modalProperties = {
      formKey: this.props.formKey,
      title: this.props.title,
      backgroundColor: this.props.backgroundColor,
      value: thisForm.value,
      onClose: () => this.props.toggleModal(false),
      onBack: this.props.onCancelChain ? () => {
        this.props.toggleModal(false);
        this.props.onChainingModalTriggered(this.props.onCancelChain, false)
      } : null,
      onSelectOptions: (value) => {
        this.props.onModalSelected(value);
        if (this.props.onSuccessChain) {
          this.props.onChainingModalTriggered(this.props.onSuccessChain, true);
        }
      },
    };

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={ thisForm.isModalOpened }
        onRequestClose={() => {}}
      >
        { this._renderModalContent(thisForm, forms, modalProperties) }
      </Modal>
    );
  }
};