/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-15 10:59:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 21:04:50
 */
import { Content } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { appVariables } from '@variables';
import { ARentTitle } from '@atoms';

import styles from './style';

import ModalContainer from '@airport-transfer-fragments/MainFormModalContainer';
import Text from '@airport-transfer-fragments/Text';
import Icon from '@airport-transfer-fragments/Icon';
import TextInput from '@airport-transfer-fragments/TextInput';
import Lang from '../../../../../../assets/lang';
import { connect } from "react-redux";

class ModalAmount extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      amount: props.value ? props.value : (props.default ? props.default : 0) + "",
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value != nextProps.value) {
      return true;
    }

    if (this.state != nextState) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderForm(name, label, placeholder, value, isRight = false) {
    const containerStyle = [styles.form.container];

    if (isRight) {
      containerStyle.push({paddingLeft: 10});
    } else {
      containerStyle.push({paddingRight: 10});
    }

    return (
      <View style={ containerStyle }>
        <Text color={ appVariables.colorWhite }>
          { label }
        </Text>

        <TextInput
          style={ styles.form.input }
          placeholder={ placeholder }
          placeholderTextColor={ appVariables.colorTabInactive }
          value={ this.state.amount }
          keyboardType={ "numeric" }
          onChangeText={ (value) => this.setState({[name]: value}) }
        />
      </View>

    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let inputtedFlight = "";

    const filteredFlights = this.props.options
      .filter((option) => {
        return option.value == this.state.name;
      });

    if (filteredFlights.length > 0) {
      inputtedFlight = filteredFlights[0].label + " (" + filteredFlights[0].value + ")";
    }

    return (
      <ModalContainer
        backgroundColor={ this.props.backgroundColor }
        onClosePress={ () => this.props.onClose() }
        footerButton={{
          label: Lang.done[this.props.lang],
          onPress: () => this.props.onSelectOptions(this.state.amount),
        }}
      >
        <Content style={{ flex: 1 }}>          
          <View style={ styles.contentContainer }>

            <View style={ styles.title.container }>           
              <Text color={ appVariables.colorGray }>
                { this.props.title }
              </Text>
   
              <View style={ styles.amount.container }>
                {/* <Text color={ appVariables.colorWhite } size={ 50 }>
                  { this.state.amount }
                </Text> */}
                <TextInput
                  style={{ fontSize: 50, color: appVariables.colorWhite }}
                  // placeholder={ placeholder }
                  // placeholderTextColor={ appVariables.colorTabInactive }
                  value={ this.state.amount }
                  keyboardType={ "numeric" }
                  onChangeText={ text => this.setState({ amount: text }) }
                  maxLength={4}
                />
              </View>

              <View style={ styles.button.grouper }>
                <TouchableOpacity
                  style={ styles.button.container }
                  onPress={ () => this.setState({amount: this.state.amount > 0 ? ((this.state.amount * 1) - 1).toString() : 0}) }
                >
                  <Icon name="minus" color={ appVariables.colorWhite } size={ 30 }/>
                </TouchableOpacity>

                <TouchableOpacity
                  style={ [styles.button.container, {borderLeftWidth: 0,}] }
                  onPress={ () => this.setState({amount: ((this.state.amount * 1) + 1).toString() }) }
                >
                  <Icon name="plus" color={ appVariables.colorWhite } size={ 30 }/>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Content>
      </ModalContainer>
    );
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(ModalAmount)