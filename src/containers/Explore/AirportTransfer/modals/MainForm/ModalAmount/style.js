import {
	appVariables,
} from '@variables';

export default {
  contentContainer: {
    flex: 1,
  },

  title: {
    container: {
      flex: -1,
      paddingVertical: 40,
      paddingHorizontal: 40,
      justifyContent: "center",
    },
  },

  amount: {
    container: {
      alignItems: "center",
      borderBottomWidth: 1,
      borderColor: appVariables.colorGray,
      paddingVertical: 20,
    },
  },

  button: {
    grouper: {
      alignItems: "center",
      flexDirection: "row",
      paddingVertical: 20,
    },

    container: {
      flex: 1,
      height: 80,
      borderWidth: 1,
      borderColor: appVariables.colorGray,
      justifyContent: "center",
      alignItems: "center",
    },
  },
}