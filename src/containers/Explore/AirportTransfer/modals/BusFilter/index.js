/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 22:27:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-21 23:02:06
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";
import _ from "lodash";

import Text from "@airport-transfer-fragments/Text";

import FilterModalContainer from "@airport-transfer-fragments/FilterModalContainer";
import FilterCheckList from "@airport-transfer-fragments/FilterCheckList";
import FilterSlider from "@airport-transfer-fragments/FilterSlider";

import styles from "./style";

import BusFilterData from "@airport-transfer-configs/data/BusFilter";

import NumberHelper from "@airport-transfer-helpers/Number";

class BusFilter extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      types: props.carType !== null ? props.carType.map(item => {
        if (props.values.length == 0 || props.values.indexOf(item.value) !== -1) {
          return item.value;
        };
      }) : [],
    };

    // for (x in BusFilterData) {
    //   this.state[x] = this.getFilterValues(props, x);
    // }
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      if (!this.props.isVisible && nextProps.isVisible) {
        // const newState = {};

        // for (x in BusFilterData) {
        //   newState[x] = this.getFilterValues(nextProps, x);
        // }

        // this.setState(newState);
        this.setState({
          types: this.props.carType !== null ? this.props.carType.map(item => {
            if (nextProps.values.length == 0 || nextProps.values.indexOf(item.value) !== -1) {
              return item.value;
            };
          }) : [],
        })
      }

      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  setFilter(type, valueKey, value) {
    // const data = this.state[type].map(key => {
    //   if (key != valueKey) {
    //     return key;
    //   }
    // });

    // if (value) {
    //   data.push(valueKey);
    // }

    // this.setState({[type]: data});
    const types = this.state.types.map(type => {
      if (type != valueKey) {
        return type;
      }
    });

    if (value) {
      types.push(valueKey);
    }

    this.setState({
      types: _.filter(types, v => v !== undefined)
    })
  }

// ---------------------------------------------------

  resetFilter() {
    // const newState = {};

    // for (x in BusFilterData) {
    //   newState[x] = [];
    // }

    // this.setState(newState);
    this.setState({
      types: this.props.carType.map(item => item.value)
    })
  }

// ---------------------------------------------------

  applyFilter() {
    this.props.onFilterSet({
      price: this.state.price,
      types: this.state.types
      // facilities: this.state.facilities,
    })
  }

// ---------------------------------------------------

  // getFilterValues(data, type) {
  //   return BusFilterData[type].map(item => {
  //     if (data.values[type].length == 0 || data.values[type].indexOf(item.value) !== -1) {
  //       return item.value;
  //     };
  //   })
  // }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderList(title, type, hasClear = false) {
    return (
      <FilterCheckList
        title={ title }
        options={ this.props.carType }
        values={ this.state.types }
        onClear={ hasClear ? () => this.setState({ types: _.map(this.props.carType, v => v.value) }) : null }
        onValueToggle={ (valueKey, value) => this.setFilter(type, valueKey, value) }
      />
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <FilterModalContainer
        onReset={ () => this.resetFilter() }
        onDone={ () => this.applyFilter() }
        isVisible={ this.props.isVisible }
        onClosePress={ () => this.props.onClosePress() }
      >

        <View style={ styles.segmentContainer }>
          { this._renderList("BUS TYPES", "facilities", true) }
        </View>

        {/* <Text style={ styles.title }>
          TYPES
        </Text> */}

        {/* <View style={ styles.rowContainer }>

          { this._renderList("LUXURY BUS", "luxury_types", true) }
          { this._renderList("COASTER BUS", "coaster_types") }

        </View> */}

        {/* <View style={ styles.rowContainer }>

          { this._renderList("BIG BUS", "big_types") }
          { this._renderList("SMALL BUS", "small_types") }

        </View>

        <View style={ styles.rowContainer }>

          { this._renderList("MEDIUM BUS", "medium_types") }

        </View> */}
        
      </FilterModalContainer>
    );
  }

}

const mapStateToProps = ({ masterCarType }) => {
  const { carType } = masterCarType;
  return { carType }
}

export default connect(mapStateToProps, {})(BusFilter);