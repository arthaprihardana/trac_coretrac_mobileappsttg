import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
	segmentContainer: {
		borderBottomWidth: 1,
		borderColor: "#cccccc",
		marginBottom: 40
	},

	title: {
    paddingHorizontal: 40,
    paddingBottom: 20,
  },

	rowContainer: {
    flex: -1,
    flexDirection: "row",
  },
}