export default {
	format: (number, digitDelimiter = ".") => {
		if (!number) {
		  return number;
		}

		number = number + "";

		let numberLength = number.length;
		const numberSplit = [];
		if (number.length > 3) {
		  while (numberLength > 3) {
		    numberSplit.push(number.slice(-3));
		    number = number.slice(0, -3)
		    numberLength = number.length;
		  }

		  numberSplit.push(number);
		  numberSplit.reverse();

		  number = numberSplit.join(digitDelimiter);
		}

		return number;
	},
}