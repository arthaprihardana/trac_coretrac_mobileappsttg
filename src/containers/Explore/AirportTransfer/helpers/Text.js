export default {

	shorten: (text, maxLength = null) => {
    let textSplits = text.split(",");

    if (textSplits.length > 1) {
      textSplits = textSplits[0].trim();

      if (maxLength != null && textSplits.length > maxLength) {
        return textSplits.substring(0, maxLength) + "...";
      }

      return textSplits + ", ...";
    }

    text = text.trim();

    if (maxLength != null && text.length > maxLength) {
      return text.substring(0, maxLength) + "...";
    }

    return text;
  }

}