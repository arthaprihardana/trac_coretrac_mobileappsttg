export default {
	getBlueShadeBySteps: (stepLength, currentStepIndex) => {
		const baseRed = 34;
		const baseGreen = 70;
		const baseBlue = 168;

		const targetRed = 18;
		const targetGreen = 38;
		const targetBlue = 91;

		const getEndColor = (baseColor, targetColor) => baseColor + (Math.round((targetColor - baseColor) / (stepLength), 0) * currentStepIndex);

		const newRed = getEndColor(baseRed, targetRed);
		const newGreen = getEndColor(baseGreen, targetGreen);
		const newBlue = getEndColor(baseBlue, targetBlue);

		return `rgba(${newRed}, ${newGreen}, ${newBlue}, 1)`;
	},
}