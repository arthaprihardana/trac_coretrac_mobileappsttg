import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
  },

  footer: {
    button: {
      grouper: {
        flex: -1,
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 20,
        justifyContent: "space-between",
      },

      container: {
        flex: -1,
        width: (appMetrics.screenWidth - 60) / 2,
        paddingVertical: 13,
        borderRadius: 7,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

}