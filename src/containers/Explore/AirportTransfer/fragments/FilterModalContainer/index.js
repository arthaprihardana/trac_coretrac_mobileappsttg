import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";

import { connect } from "react-redux";
import Text from "@airport-transfer-fragments/Text";
import ModalContainer from "@airport-transfer-fragments/ModalContainer";

import styles from "./style";
import Lang from '../../../../../assets/lang'

class FilterModalContainer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderButtons() {
    return (
      <View style={ styles.footer.button.grouper }>

        <TouchableOpacity
          style={ [styles.footer.button.container, {borderWidth: 1,}] }
          onPress={ () => this.props.onReset() }
        >
          <Text>
            {Lang.reset[this.props.lang]}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={ [styles.footer.button.container, {backgroundColor: appVariables.colorOrange,}] }
          onPress={ () => this.props.onDone() }
        >
          <Text color={ appVariables.colorWhite }>
            {Lang.done[this.props.lang]}
          </Text>
        </TouchableOpacity>

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <ModalContainer
        isVisible={ this.props.isVisible }
        onClosePress={ () => this.props.onClosePress() }
      >
        <ScrollView>
          <View style={ [styles.container] }>
            
            { this.props.children }
            
          </View>

         { this._renderButtons() }
        </ScrollView>

      </ModalContainer>
    );
  }

}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(FilterModalContainer)