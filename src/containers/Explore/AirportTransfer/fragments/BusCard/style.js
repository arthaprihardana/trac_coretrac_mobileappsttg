import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
	container: {
    flex: -1,
    backgroundColor: appVariables.colorWhite,
  },

  description: {
  	container: {
      flex: -1,
      paddingTop: 10,
      paddingBottom: 40,
      backgroundColor: "#eeeeee",
    },

    innerContainer: {
      paddingHorizontal: 20,
    },

    image: {
      container: {
        flex: -1,
        height: 80,
        width: 140,
        marginRight: 10,
      },

      content: {
        flex: 1,
      },
    },
  },
}