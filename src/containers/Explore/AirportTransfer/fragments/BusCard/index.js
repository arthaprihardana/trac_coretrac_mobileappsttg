/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-22 00:32:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-01 16:38:42
 */
import React, { Component } from "react";
import {
  View,
  Image,
  ScrollView,
  Alert
} from "react-native";
import { appVariables } from "@variables";

import Text from "@airport-transfer-fragments/Text";
import VehicleCardMain from "@airport-transfer-fragments/VehicleCardMain";
import VehicleCardControl from "@airport-transfer-fragments/VehicleCardControl";
import MyWebView from '@airport-transfer-fragments/WebView';
// import MyWebView from 'react-native-webview-autoheight';
// import MyWebView from 'react-native-autoheight-webview';

import styles from "./style";


export default class BusCard extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      addedCount: 0,
      isDetailVisible: false,
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  increaseList() {
    const addedCount = this.state.addedCount + 1;

    if (addedCount > this.props.stock) {
      Alert.alert(
        'Message',
        'Stock for this car is already empty',
        [
          {text: 'I Understand', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
      return false;
    }

    this.setState({
      addedCount,
    });

    if (this.props.onAddCarPress) {
      this.props.onAddCarPress(this.props.code, addedCount, this.props.price);
    }
  }

// ---------------------------------------------------

  decreaseList() {
    const addedCount = (this.state.addedCount > 0 ? this.state.addedCount - 1 : 0);

    this.setState({
      addedCount,
    });

    if (this.props.onAddCarPress) {
      this.props.onRemoveCarPress(this.props.code, addedCount, this.props.price);
    }
  }

// ---------------------------------------------------

  toggleDetailVisibility() {
    this.setState({
      isDetailVisible: !this.state.isDetailVisible,
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderDescriptionImage(galleryUrl, index) {
    return (
      <View 
        key={ index }
        style={ [styles.description.image.container, (index == this.props.galleries.length - 1 ? {marginRight: 0} : {})] }
      >
        <Image
          style={ styles.description.image.content }
          resizeMode={ "cover" }
          source={{uri: galleryUrl}}
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderDescription() {
    if (!this.state.isDetailVisible || !this.props.detail) {
      return null;
    }
    const customStyle = `<style>* {max-width: 100%;} body {font-family: ${ appVariables.museo900 }; background-color: #eeeeee}</style>`;
    const htmlContent = this.props.detail;

    return (
      <View style={ styles.description.container }>
        <View style={ styles.description.innerContainer }>
          <Text>
            Interior
          </Text>
        </View>

        <ScrollView
          style={{
            marginVertical: 20,
          }}
          contentContainerStyle={{
            paddingHorizontal: 20,
          }}
          horizontal
          showsHorizontalScrollIndicator={ false }
        >
          
          { this.props.galleries.map((url, index) => {
            return this._renderDescriptionImage(url, index);
          }) }

        </ScrollView>

        {/* <View style={ styles.description.innerContainer }>
          <Text>
            { this.props.detail }
          </Text>
        </View> */}
        <MyWebView source={{html: customStyle + htmlContent}} />
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.container }>

        <VehicleCardMain
          isBus={true}
          isSpecialDeal={ this.props.isSpecialDeal }
          imageUrl={ this.props.imageUrl }
          originalPrice={ this.props.originalPrice }
          stock={ this.props.stock }
          name={ this.props.name }
          currency={ this.props.currency }
          price={ this.props.price }
          digitDelimiter={ this.props.digitDelimiter }
          features={ this.props.features }
          seatingArrangement={ this.props.seatingArrangement }
        />

        <VehicleCardControl
          stock={ this.props.stock }
          countValue={ this.state.addedCount }
          onDecreaseCount={ () => this.decreaseList() }
          onIncreaseCount={ () => this.increaseList() }
          onDetailToggle={ () => this.toggleDetailVisibility() }
          isDetailVisible={ this.state.isDetailVisible }
        >

          { this._renderDescription() }
        
        </VehicleCardControl>
      
      </View>
    );
  }
};