/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-25 11:03:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-25 11:34:21
 */
import React, { Component } from "react";

import FontAwesomeIcon5 from "react-native-vector-icons/FontAwesome5";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FontAwesomePro from "react-native-vector-icons/FontAwesome5Pro";
import FeatherIcon from "react-native-vector-icons/Feather";
import { createIconSetFromFontello } from "react-native-vector-icons";
import CarFeaturesFontConfig from "@icons/trac_icons.json";
const CarFeatureIcon = createIconSetFromFontello(CarFeaturesFontConfig);

import IconConfig from "@airport-transfer-configs/mapper/Icon";

import styles from "./style";

export default class Icon extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const props = JSON.parse(JSON.stringify({
      color: "black",
      size: 14,
      name: "question",
      ... this.props,
    }));

    switch (this.props.type) {
  
      case "trac":
        props.name = IconConfig[props.name].icon;
        props.size = IconConfig[props.name].sizeMultiplier * props.size;

        return <CarFeatureIcon
          { ... props }
        />;

      // case "fontAwesome":
      default:
        return <FontAwesomeIcon
          { ... props }
        />;
    }

  }
};
