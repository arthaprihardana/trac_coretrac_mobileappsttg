import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
	grouper: {
    flex: -1,
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: 15,
  },

  container: {
    flex: -1,
    width: 65,
    height: 65,
    borderRadius: 50,
    // borderWidth: 1,
    marginHorizontal: 5,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },

  image: {
    flex: -1,
    // width: 90,
    // height: 90,
    top: 1,
    left: -1,
    // borderWidth: 1,
  },
}