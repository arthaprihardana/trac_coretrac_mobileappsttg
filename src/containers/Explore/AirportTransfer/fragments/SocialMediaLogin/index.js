/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 13:57:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-24 18:17:02
 */
import React, { Component } from "react";
// import Auth0 from 'react-native-auth0';

import { 
  View,
  TouchableOpacity,
  Image,
  Platform
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import * as simpleAuthProviders from 'react-native-simple-auth';

import Icon from "@airport-transfer-fragments/Icon";

import styles from "./style";
import { SOCIAL_MEDIA_AUTH } from "../../../../../constant";
import { connect } from "react-redux";
import { postLoginSosmed } from "../../../../../actions";

// const auth0 = new Auth0({ 
//   domain: 'ttg-development.au.auth0.com', 
//   clientId: 'TcEbg70crat42o5VDHfsN8zKSCxmwnVI' 
// });

class SocialMediaLogin extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFE CYCLES
  // ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------

  loginBySocialMedia(socMed) {
    let auth = Platform.OS === "ios" ? SOCIAL_MEDIA_AUTH.google.ios : SOCIAL_MEDIA_AUTH.google.android;
    simpleAuthProviders['google'](auth).then(info => {
      console.log('info ==>', info);
      this.props.postLoginSosmed({
        "accessToken": "",
        "idToken": "",
        "idTokenPayload": {
          "given_name": info.user.given_name,
          "family_name": info.user.family_name,
          "nickname": "",
          "name": "",
          "picture": info.user.picture,
          "gender": 1,
          "locale": "",
          "updated_at": "",
          "email": info.user.email,
          "email_verified": info.user.verified_email,
          "iss": "",
          "sub": "",
          "aud": "",
          "iat": "",
          "exp": "",
          "at_hash": "",
          "nonce": ""
        },
        "appState": "",
        "refreshToken": "",
        "state": "",
        "expiresIn": "",
        "tokenType": "",
        "scope": ""
      })
    }).catch(err => {
      console.log('error', err);
    })
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.grouper }>
        {/* <TouchableOpacity
          style={ styles.container }
          onPress={ () => this.loginBySocialMedia("facebook") }
        >
          <Image
            resizeMode={ "contain" }
            source={ require("@images/png/socmeds/facebook.png") }
            style={ styles.image }
          />
        </TouchableOpacity> */}

        <TouchableOpacity
          style={ styles.container }
          onPress={ () => this.loginBySocialMedia("gmail") }
        >
          <Image
            resizeMode={ "contain" }
            source={ require("@images/png/socmeds/google.png") }
            style={ styles.image }
          />
        </TouchableOpacity>
      </View>
    );
  }
};

const mapStateToProps = ({ authentication }) => {
  const { loading, login, isLogin } = authentication;
  return { loading, login, isLogin }
}

export default connect(mapStateToProps, {
  postLoginSosmed
})(SocialMediaLogin);