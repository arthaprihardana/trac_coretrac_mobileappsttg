import {
  appMetrics,
  appVariables,
} from "@variables";

export default {
  container: {
    paddingHorizontal: 20,
    justifyContent: "center",
    height: 120,
  },

  label: {
    container: {
      flex: -1,
      flexDirection: "row",
    },

    text: {
      paddingBottom: 7,
      paddingRight: 10,
    },
  },
}