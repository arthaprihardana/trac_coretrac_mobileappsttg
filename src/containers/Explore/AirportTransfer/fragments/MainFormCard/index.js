/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 15:12:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-09 17:25:10
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import { appVariables } from "@variables";
import styles from "./style";

import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";


export default class MainFormCard extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

    shouldComponentUpdate(nextProps, nextState) {
      if (nextProps != this.props) {
        return true;
      }

      return false;
    }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const forms = this.props.formsState;
    const thisForm = forms[this.props.formKey];
    let rawValue = thisForm.value;

    if (this.props.modifier != null && this.props.modifier != undefined) {
      rawValue = this.props.modifier(rawValue, this.props.formsState);
    }

    const value = rawValue ? rawValue : this.props.placeholder;

    let errorMarkRender = null;
    if (this.props.isError) {
      errorMarkRender = <Icon name="exclamation-circle" color={ appVariables.colorOrange } size={ 18 }/>;
    }

    return (
      <TouchableOpacity
        style={ [styles.container, {backgroundColor: this.props.backgroundColor,}] }
        onPress={ !this.props.isDisabled ? () => this.props.onPress(this.props.formKey, true) : () => {}}
        // isError={ thisForm.isError }
      >
        <View style={ styles.label.container }>
          <Text color={ "#aaaaaa" } size={ 12 } style={ styles.label.text }>
            { this.props.label }
          </Text>

          { errorMarkRender }
        </View>

        <View>
          <Text leftIcon={ this.props.hasSearchIcon ? "search" : null } color={ appVariables.colorWhite } size={ 18 }>
            { value }
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
};