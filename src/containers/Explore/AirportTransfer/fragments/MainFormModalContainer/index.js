/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 10:02:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-25 11:28:37
 */
import React, { Component } from 'react';
import styles from './style';
import {
  appVariables,
} from "@variables";
import { 
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
} from 'react-native';

import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import TextInput from '@airport-transfer-fragments/TextInput';
import MainButton from '@airport-transfer-fragments/MainButton';


export default class MainFormModalContainer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      searchText: "",
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderHeader() {
    let backButtonRender = null;
    if (this.props.onBackPress) {
      backButtonRender = <TouchableOpacity
        style={{
          paddingRight: 10,
          paddingBottom: 7,
        }}
        onPress={ () => this.props.onBackPress() }
      >
        <Icon name="arrow-left" color={ appVariables.colorWhite } size={ 20 }/>
      </TouchableOpacity>;
    }

    return (
      <View style={ [styles.header.container, backButtonRender ? {justifyContent: "space-between",} : {}] }>
        { backButtonRender }

        <TouchableOpacity
          style={{
            paddingLeft: 10,
            paddingBottom: 7,
          }}
          onPress={ () => this.props.onClosePress() }
        >
          <Icon name="times" color={ appVariables.colorWhite } size={ 20 }/>
        </TouchableOpacity>
      </View>
    );
  }


// ---------------------------------------------------

  _renderTitle() {
    if (!this.props.title) {
      return null;
    }

    if (this.props.title.type == "basic") {
      return (
        <Text color={ appVariables.colorWhite } size={ 25 } style={ styles.title.basic.text }>
          { this.props.title.text }
        </Text>
      );
    } else if (this.props.title.type == "search") {
      return (
        <View style={ styles.title.search.container }>
          <Text color={ "#aaaaaa" }>
            { this.props.title.text }
          </Text>

          <View style={ styles.title.search.form.container }>
            <Icon name="search" color={ appVariables.colorWhite } size={ 23 }/>
            
            <TextInput
              style={ styles.title.search.form.input }
              autoCorrect={ false }
              underlineColorAndroid={ "transparent" }
              value={ this.props.title.value }
              onChangeText={ (value) => this.props.title.onChangeText(value) }
            />

          </View>
        </View>
      );
    }
  }

// ---------------------------------------------------

  _renderFooter() {
    if (!this.props.footerButton) {
      return null;
    }

    return (
      <View style={ styles.footer.container }>
        <MainButton
          label={ this.props.footerButton.label }
          onPress={ () => this.props.footerButton.onPress() }
          rounded
        />
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BaseContainer backgroundColor={ this.props.backgroundColor }>
        <KeyboardAvoidingView
          behavior={ "padding" }
          enabled={false}
          style={ [styles.container, {backgroundColor: this.props.backgroundColor}] }
        >
          { this._renderHeader() }

          { this._renderTitle() }

          <View style={ styles.contentContainer }>
            { this.props.children }
          </View>

          { this._renderFooter() }

        </KeyboardAvoidingView>
      </BaseContainer>
    );
  }
}
