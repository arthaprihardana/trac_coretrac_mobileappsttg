import {
	appVariables
} from '@variables';

export default {
  container: { 
    flex: 1,
  },

  contentContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },

  header: {
  	container: {
      flex: -1,
      flexDirection: "row",
      paddingHorizontal: 20,
      paddingTop: 15,
      justifyContent: "flex-end",
    },
  },

  title: {
  	basic: {
  		text: {
        paddingHorizontal: 20,
        paddingTop: 10,
        paddingBottom: 30,
      },
  	},

  	search: {
			container: {
	      flex: -1,
	      paddingHorizontal: 20,
	      paddingBottom: 10,
	    },

	    form: {
	    	container: {
          flex: -1,
          flexDirection: "row",
          height: 60,
          borderBottomWidth: 1,
          borderColor: appVariables.colorTabInactive,
          alignItems: "center",
        },

        input: {
          flex: 1,
          height: 60,
          fontSize: 20,
          color: appVariables.colorWhite,
          paddingLeft: 13,
        },
	    },
	  },
  },

  footer: {
    container: {
      padding: 20,
      paddingTop: 0,
    },

    button: {
      container: {
        backgroundColor: appVariables.colorOrange,
        height: 50,
        borderRadius: 7,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },
};