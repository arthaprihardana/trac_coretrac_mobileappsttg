import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  listRoute: {
    container: {
      flex: -1,
      flexDirection: "row",
      marginTop: 5,
      paddingTop: 5,
      borderTopWidth: 1,
      borderColor: appVariables.colorOrange,
    },

    text: {
      fontSize: 12,
    },
  },

  detail: {
    container: {
      flex: -1,
      height: 370,
    },

    bottomBackground: {
      flex: -1,
      height: 60,
      width: appMetrics.screenWidth,
      position: "absolute",
      bottom: 0,
      backgroundColor: appVariables.colorWhite,
    },

    innerContainer: {
      flex: 1,
    },

    title: {
      container: {
        flex: -1,
        alignItems: "center",
        paddingVertical: 15,
      },

      text: {
        paddingBottom: 7,
      },
    },

    featureWrapper: {
      flex: -1,
      height: 50,
      paddingHorizontal: 30,
      justifyContent: "center",
    },

    image: {
      container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      },

      innerContainer: {
        flex: -1,
        flexDirection: "row",
        justifyContent: "center",
        height: 190,
      },

      main: {
        container: {
          flex: -1,
          width: appMetrics.screenWidth - 100,
        },

        image: {
          flex: 1,
        },
      },

      control: {
        grouper: {
          flex: -1,
          flexDirection: "row",
          height: 190,
          width: appMetrics.screenWidth,
          justifyContent: "space-between",
          position: "absolute",
        },

        container: {
          flex: -1,
          paddingHorizontal: 20,
          justifyContent: "center",
        },
      },
    },
  },

  price: {
    container: {
      flex: -1,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      height: 60,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: "#dddddd",
      backgroundColor: appVariables.colorWhite,
    },

    striked: {
      container: {
        flex: -1,
        paddingRight: 8,
      },

      line: {
        flex: -1,
        borderTopWidth: 2,
        top: -8,
        borderColor: "red",
      },
    },

    main: {
      container: {
        flex: -1,
        flexDirection: "row",
      },

      text: {
        paddingRight: 3,
      },
    },
  },
}