/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-08 11:34:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 19:14:50
 */
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Switch,
  Modal,
} from "react-native";
import { appVariables, appMetrics } from "@variables";
import { connect } from "react-redux";
import _ from "lodash";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import FeatureList from "@airport-transfer-fragments/FeatureList";

import NumberHelper from "@airport-transfer-helpers/Number";
import TextHelper from "@airport-transfer-helpers/Text";

import styles from "./style";
import { CAR_RENTAL, AIRPORT_TRANSFER, BUS_RENTAL } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class VehicleSliderDetail extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  state = {
    direction: {
      from: null,
      to: null
    },
    allVehicleList: []
  }

  componentDidMount = () => {
    switch (this.props.type) {
      case "My Booking":
        var d = this.state.direction;
        d.from = this.props.activeList.pickup_locations[0].Alamat;
        if(this.props.activeList.MsProductId !== CAR_RENTAL) {
          d.to = this.props.activeList.drop_locations[0].Alamat;
        }
        this.setState({ allVehicleList: this.props.vehicleList, direction: d });
        break;
      case "My History":
        var d = this.state.direction;
        d.from = this.props.activeList.pickup_locations[0].Alamat;
        if(this.props.activeList.MsProductId !== CAR_RENTAL) {
          d.to = this.props.activeList.drop_locations[0].Alamat;
        }
        this.setState({ allVehicleList: this.props.vehicleList });
        break;
      default:
        this.handleDirection();
        break;
    }
    
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) return true;
    if (this.state !== nextState) return true;

    return false;
  }

  handleDirection = () => {
    let dir = this.state.direction;
    let temp = this.props.tempForm;
    switch (this.props.selectedProduct) {
      case AIRPORT_TRANSFER:
        var from = _.filter(temp.from_airport.options, { value: this.props.activeList.direction.from });
        dir.from = from[0].label;
        this.setState({
          direction: dir
        });
        break;
      default:
        var from = _.filter(temp.from_location.options, { value: this.props.activeList.direction.from });
        dir.from = from[0].label;
        this.setState({
          direction: dir
        });
        break;
    }
  }

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderDirection() {
    let arrowRender = null;
    let toRender = null;
    let direction = this.state.direction;
    
    if (direction.to) {
      arrowRender = <Icon name="arrow-right" size={ 17 } color={ appVariables.colorOrange } />;

      toRender = <Text color={ appVariables.colorWhite } style={{paddingLeft: 7}}>
        {Lang.ke[this.props.lang]}: { TextHelper.shorten(direction.to, 10) }
      </Text>;
    }

    return (
      <View style={ styles.listRoute.container }>
        <Text color={ appVariables.colorWhite } style={{paddingRight: 7}}>
          {Lang.from[this.props.lang]}: { direction.from ? TextHelper.shorten(direction.from, !!toRender ? 10 : 20) : null }
        </Text>
        
        { arrowRender }

        { toRender }
      </View>
    );
  }

// ---------------------------------------------------

  _renderTitle() {
    let vehicleName = null;
    let serviceType = null;
    switch (this.props.type) {
      case "My Booking":
        var x = _.filter(this.props.products, { MsProductId: this.props.activeList.MsProductId });
        var y = _.filter(x[0].product_service, { ProductServiceId: this.props.activeList.MsProductServiceId });
        vehicleName = this.props.activeList.UnitTypeName;
        serviceType = x[0].MsProductId !== BUS_RENTAL ? `${x[0].MsProductName} - ${y[0].ProductServiceName}` : x[0].MsProductName;
        break;
      case "My History":
        var x = _.filter(this.props.products, { MsProductId: this.props.activeList.MsProductId });
        var y = _.filter(x[0].product_service, { ProductServiceId: this.props.activeList.MsProductServiceId });
        vehicleName = this.props.activeList.UnitTypeName;
        serviceType = x[0].MsProductId !== BUS_RENTAL ? `${x[0].MsProductName} - ${y[0].ProductServiceName}` : x[0].MsProductName;
        break;
      default:
        vehicleName = this.props.activeList.vehicle.vehicleAlias !== null ? this.props.activeList.vehicle.vehicleAlias : this.props.activeList.vehicle.vehicleTypeDesc;
        serviceType = this.props.serviceType;
        break;
    }
    return (
      <View style={ styles.detail.title.container }>
        <Text color={ appVariables.colorWhite } size={ 20 } style={ styles.detail.title.text }>
          { vehicleName }
        </Text>

        <Text color={ appVariables.colorWhite } size={ 17 }>
          { serviceType }
        </Text>

        { this._renderDirection() }
      </View>
    );
  }

// ---------------------------------------------------

  _renderImage(imageUrl, index) {
    return (
      <View
        style={ [styles.detail.image.main.container, {
          flex: -1,
          flexDirection: "row",
          width: appMetrics.screenWidth,
          justifyContent: "center",
        }] }
        key={ index }
      >
        <View
          style={ [styles.detail.image.main.container, {

          }] }
        >
          <Image
            resizeMode={ "contain" }
            source={{ uri: imageUrl }}
            style={ styles.detail.image.main.image }
          />
        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderImageList() {
    let component = null;
    switch (this.props.type) {
      case "My Booking":
        var v = _.map(this.props.list, v => {
          let x = _.filter(this.state.allVehicleList, { vehicleTypeId: v.UnitTypeId });
          return x[0];
        });
        if(_.filter(v, o => o !== undefined).length > 0) {
          component = <ScrollView
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            ref={ (sf) => this.props.setScrollRef(sf) }
            horizontal>
            { v.map((item, index) => {
              return this._renderImage(item.vehicleImage, index);
            }) }
          </ScrollView>
        }
        break;
      case "My History":
        var v = _.map(this.props.list, v => {
          let x = _.filter(this.state.allVehicleList, { vehicleTypeId: v.UnitTypeId });
          return x[0];
        });
        if(_.filter(v, o => o !== undefined).length > 0) {
          component = <ScrollView
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            ref={ (sf) => this.props.setScrollRef(sf) }
            horizontal>
            { v.map((item, index) => {
              return this._renderImage(item.vehicleImage, index);
            }) }
          </ScrollView>
        }
        break;
      default:
        component = <ScrollView
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          ref={ (sf) => this.props.setScrollRef(sf) }
          horizontal>
          { this.props.list.map((item, index) => {
            return this._renderImage(item.vehicle.vehicleImage, index);
          }) }
        </ScrollView>
        break;
    }
    return (
      <View style={ styles.detail.image.container }>

        <View style={ styles.detail.image.innerContainer }>
          { component }
          {/* <ScrollView
            ref={ (sf) => this.props.setScrollRef(sf) }
            horizontal
          >
            
            { this.props.list.map((item, index) => {
              return this._renderImage(item.vehicle.vehicleImage, index);
            }) }

          </ScrollView> */}

          <View style={ styles.detail.image.control.grouper }>
            <TouchableOpacity
              style={ styles.detail.image.control.container }
              onPress={ () => this.props.onSwitchList(this.props.activeListIndex - 1) }
            >
              <Icon name="angle-left" color={ appVariables.colorWhite } size={ 35 }/>
            </TouchableOpacity>

            <TouchableOpacity
              style={ styles.detail.image.control.container }
              onPress={ () => this.props.onSwitchList(this.props.activeListIndex + 1) }
            >
              <Icon name="angle-right" color={ appVariables.colorWhite } size={ 35 }/>
            </TouchableOpacity>
          </View>
        </View>

      </View>
    );
  }

// ---------------------------------------------------
  
  _renderDetail() {
    let features = [];
    let vehicle = _.filter(this.state.allVehicleList, { vehicleTypeId: this.props.activeList.UnitTypeId });
    switch (this.props.type) {
      case "My Booking":
        features = [{
          type: "passanger",
          value: vehicle.length > 0 ? vehicle[0].totalSeat : null,
        },{
          type: "baggage",
          value: vehicle.length > 0 ? vehicle[0].totalLugagge : null,
        },{
          type: "transmission",
          value: parseInt(this.props.activeList.IsTransmissionManual) ? "MT" : "AT",
        },{
          type: "airbags",
          value: vehicle.length > 0 ? vehicle[0].isAirbag === 1 ? true : false : null,
        },{
          type: "air-conditioner",
          value: vehicle.length > 0 ? vehicle[0].isAC === 1 ? true : false : null,
        }]
        break;
      case "My History":
        features = [{
          type: "passanger",
          value: vehicle.length > 0 ? vehicle[0].totalSeat : null,
        },{
          type: "baggage",
          value: vehicle.length > 0 ? vehicle[0].totalLugagge : null,
        },{
          type: "transmission",
          value: parseInt(this.props.activeList.IsTransmissionManual) ? "MT" : "AT",
        },{
          type: "airbags",
          value: vehicle.length > 0 ? vehicle[0].isAirbag === 1 ? true : false : null,
        },{
          type: "air-conditioner",
          value: vehicle.length > 0 ? vehicle[0].isAC === 1 ? true : false : null,
        }]
        break;
      default:
        features = [{
          type: "passanger",
          value: this.props.activeList.vehicle.totalSeat,
        },{
          type: "baggage",
          value: this.props.activeList.vehicle.totalLugagge,
        },{
          type: "transmission",
          value: parseInt(this.props.activeList.vehicle.isTransmissionManual) ? "MT" : "AT",
        },{
          type: "airbags",
          value: this.props.activeList.vehicle.isAirbag === 1 ? true : false,
        },{
          type: "air-conditioner",
          value: this.props.activeList.vehicle.isAC === 1 ? true : false,
        }]
        break;
    }
    return (
      <View style={ styles.detail.container }>
        <View style={ styles.detail.bottomBackground }/>

        <View style={ styles.detail.innerContainer }>

          { this._renderTitle() }

          <View style={ styles.detail.featureWrapper }>
            <FeatureList
              features={ features }
              valueColor={ appVariables.colorWhite }
              sizeMultiplier={ 1.2 }
            />
          </View>

          { this._renderImageList() }

        </View>

      </View>
    );
  }

// ---------------------------------------------------

  _renderPrice() {
    let price = null;
    switch (this.props.type) {
      case "My Booking":
        price = parseInt(this.props.activeList.Price);
        // if(this.props.activeList.reservation_promos.length > 0) {
        //   price -= parseInt(this.props.activeList.reservation_promos[0].Amount);
        // }
        price = `Rp ${NumberHelper.format(price)}`;
        break;
      case "My History":
        price = parseInt(this.props.activeList.Price);
        // if(this.props.activeList.reservation_promos.length > 0) {
        //   price -= parseInt(this.props.activeList.reservation_promos[0].Amount);
        // }
        price = `Rp ${NumberHelper.format(price)}`;
        break;
      default:
        price = `${this.props.checkoutData.currency} ${NumberHelper.format(this.props.activeList.vehicle.rentInfo.basePrice, this.props.checkoutData.digitDelimiter)}`
        break;
    }
    return (
      <View style={ styles.price.container }>
        {/* { originalPriceRender } */}

        <View style={ styles.price.main.container }>
          <Text color={ appVariables.colorOrange } size={ 20 } bold style={ styles.price.main.text }>
            { price }
          </Text>

          <Text color={ "#aaaaaa" } size={ 13 }>
            / Package
          </Text>
        </View>
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View>
        { this._renderDetail() }

        { this._renderPrice() }
      </View>          
    );
  }
}

const mapStateToProps = ({ temporaryForm, productService, vehicleAttribute, language }) => {
  const { tempForm } = temporaryForm;
  const { selectedProduct, products } = productService;
  const vehicleList = vehicleAttribute.vehicle;
  const { lang } = language;
  return { tempForm, selectedProduct, vehicleList, products, lang }
}

export default connect(mapStateToProps, {

})(VehicleSliderDetail)
