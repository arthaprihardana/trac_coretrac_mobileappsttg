import {
  appVariables,
  appMetrics,
} from "@variables";

export default {
  progress: {
    container: {
      flex: -1,
      paddingHorizontal: 20,
      paddingVertical: 10,
      backgroundColor: "#12265B",
    },

    bar: {
      grouper: {
        flex: -1,
        flexDirection: "row",
        height: 30,
        paddingHorizontal: 20,
        alignItems: "center",
      },

      circle: {
        flex: -1,
        width: 20,
        height: 20,
        borderRadius: 50,
        backgroundColor: "#14348B",
      },

      line: {
        flex: 1,
        height: 5,
        backgroundColor: "#14348B",
      },
    },

    label: {
      grouper: {
        flex: -1,
        flexDirection: "row",
        height: 20,
        alignItems: "center",
        justifyContent: "space-between",
      },
    },
  },

}