import { Container, Content } from "native-base";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  ScrollView,
  Image,
} from "react-native";
import { appVariables, appMetrics } from "@variables";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import TextInput from "@airport-transfer-fragments/TextInput";
import Header from "@airport-transfer-fragments/Header";
import CheckoutFooter from "@airport-transfer-fragments/CheckoutFooter";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";

import styles from "./style";




export default class ProgressedContainer extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderProgress(steps, currentStepIndex) {
    const stepCount = steps.length;

    const progressRender = [];
    const progressLabelRender = [];
    for (x = 0; x < stepCount - 1; x ++) {
      progressRender.push(
        <View key={ "c" + x } style={ [styles.progress.bar.circle, x < currentStepIndex + 1 ? {backgroundColor: appVariables.colorOrange} : {}] }/>
      );

      progressRender.push(
        <View key={ "b" + x } style={ [styles.progress.bar.line, x < currentStepIndex ? {backgroundColor: appVariables.colorOrange} : {}] }/>
      );

      progressLabelRender.push(
        <Text key={ "l" + x } color={ appVariables.colorWhite } size={ 12 }>
          { steps[x] }
        </Text>
      );
    }

    progressRender.push(
      <View key={ "c" + x } style={ [styles.progress.bar.circle, x < currentStepIndex + 1 ? {backgroundColor: appVariables.colorOrange} : {}] }/>
    );    
    progressLabelRender.push(
      <Text key={ "l" + x } color={ appVariables.colorWhite } size={ 12 }>
        { steps[x] }
      </Text>
    );

    return (
      <View style={ styles.progress.container }>
        <View style={ styles.progress.bar.grouper }>
          
          { progressRender }

        </View>

        <View style={ styles.progress.label.grouper }>
          
          { progressLabelRender }

        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderSummary() {
    const summaryData = this.props.navigation.getParam("navData", {}).summaryData;
    
    return (
      <CheckoutFooter
        summaryData={ summaryData }
        currency={ summaryData.currency }
        totalPrice={ summaryData.totalPrice }
        digitDelimiter={ summaryData.digitDelimiter }
        days={ summaryData.days }
        isDisabled={ false }
        hasNext={ false }
        isDetailMode={ true }
        listCount={ 3 }
      />
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BaseContainer backgroundColor={ appVariables.colorBlueHeader }>

        <Header
          title={ this.props.header.label }
          onBackPress={ () => this.props.header.onBackPress() }
        />

        { this._renderProgress(this.props.steps, this.props.currentStepIndex) }

        { this.props.children }

        { this._renderSummary() }

      </BaseContainer>
    );
  }
}
