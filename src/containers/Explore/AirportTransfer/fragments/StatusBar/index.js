import React, { Component } from "react";
import { StatusBar as RStatusBar } from "react-native";
import { appVariables } from "@variables";

import styles from "./style";

export default class StatusBar extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <RStatusBar
        barStyle={ !this.props.inverseColor ? "light-content" : "default" }
        backgroundColor={ this.props.backgroundColor ? this.props.backgroundColor : appVariables.colorBlueHeader }
      />
    );
  }
};