/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 11:36:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 14:52:19
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  TextInput,
  ActivityIndicator
} from "react-native";
import {
  appVariables,
  appMetrics
} from "@variables";
import { connect } from "react-redux";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";

import styles from "./style";
import Lang from '../../../../../assets/lang';

class RedeemForm extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      value: "",
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.container }>
        <Text>
          {Lang.doYouHavePromo[this.props.lang]}
        </Text>

        <View style={ styles.form.container }>
          <TextInput
            placeholder={ Lang.EnterPromoCode[this.props.lang] }
            placeholderTextColor={ "#aaaaaa" }
            onChangeText={ (value) => this.setState({value: value.toUpperCase()}) }
            value={ this.state.value }
            size={ 16 }
            style={ styles.form.input }
            editable={ !this.props.isPromoValid }
          />

          {
            this.props.isLoading ? 
            <View style={ styles.form.button.container }>
              <ActivityIndicator size="small" color="#F27D20" />
            </View> :
            this.props.isPromoValid ? 
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                onPress={ () => {
                  this.setState({ value: "" });
                  this.props.onRemove();
                }}
                style={ styles.form.button.cancel }>
                <Icon name="trash" color={ "#666666" } />
              </TouchableOpacity>
              <View style={ styles.form.button.container }>
                <Icon name="check" color={ "#666666" } />
              </View>
            </View>
            :
            <TouchableOpacity
              onPress={ () => this.props.onApply(this.state.value) }
              style={ styles.form.button.container }>
              <Text color={ "#666666" }>
                {Lang.apply[this.props.lang]}
              </Text>
            </TouchableOpacity>
          }
        </View>
      </View>
    );
  }

}

const mapStateToProps = ({ discountPromo, language }) => {
  const {isPromoValid} = discountPromo;
  const { lang } = language;
  return {isPromoValid, lang}
}

export default connect(mapStateToProps, {})(RedeemForm)