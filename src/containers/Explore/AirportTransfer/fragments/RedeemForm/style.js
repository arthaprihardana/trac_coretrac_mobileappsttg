import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  container: {
    flex: -1,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#cccccc",
    backgroundColor: "#efefef",
    marginVertical: 10,
    padding: 20,
  },

  form: {
    container: {
      flex: -1,
      flexDirection: "row",
    },

    input: {
      flex: 1,
      paddingVertical: 5,
      borderBottomWidth: 1,
      borderColor: "#aaaaaa",
      fontSize: 16,
      marginTop: 20,
    },

    button: {
      container: {
        flex: -1,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#666666",
        height: 30,
        paddingHorizontal: 30,
        marginLeft: 10,
        top: 20,
        justifyContent: "center",
        alignItems: "center",
      },
      cancel:  {
        flex: -1,
        height: 30,
        paddingHorizontal: 10,
        marginLeft: 10,
        top: 20,
        justifyContent: "center",
        alignItems: "center",
      }
    },
  },
}