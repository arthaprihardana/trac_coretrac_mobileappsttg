import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  container: {
    flex: -1,
    flexDirection: "row",
    minWidth: (appMetrics.screenWidth - 60) / 2,
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderColor: "#aaaaaa",
    fontSize: 16,
    marginTop: 50,
  },

  timePicker: {
    flex: -1,
    minWidth: (appMetrics.screenWidth - 60) / 2,
    position: "absolute",
    height: 30,
  },

  label: {
    flex: -1,
    position: "absolute",
  },

  input: {
    flex: 1,
  },

  eyeButton: {
    container: {
      flex: -1,
      width: 20,
      alignItems: "flex-end",
    },
  },

  note: {
    container: {
      position: "absolute",
      bottom: -20,
    },
  },
}