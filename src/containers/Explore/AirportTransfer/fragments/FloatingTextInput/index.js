/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 23:30:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 23:24:21
 */
import React, { Component } from "react";
import {
  View,
  Animated,
  TouchableOpacity,
  Platform,
} from "react-native";
import DatePicker from "react-native-datepicker";
import moment from "moment";
import "moment/locale/id";

import {
  appVariables,
  appMetrics,
} from "@variables";

import TextInput from "@airport-transfer-fragments/TextInput";
import Text from "@airport-transfer-fragments/Text";
import Icon from "@airport-transfer-fragments/Icon";

import styles from "./style";

const labelHeight = Platform.OS == "ios" ? 4 : 19;

export default class FloatingTextInput extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      topPosition: new Animated.Value(this.props.value ? -16 : labelHeight),
      value: this.props.value ? this.props.value : (this.props.timePicker ? "00:00" : ""),
      isLabelUp: false,
      isPasswordVisible: false,
    };
  }
  
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      if (this.props.value != nextProps.value) {
        this.setState({
          value: nextProps.value,
        });

        // this.slide(nextProps.value.length > 0 ? "up" : "down");
      }

      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onChangeText(value) {
    value = value;

    if (this.props.customFormat) {
      value = this.props.customFormat(value.trim());
    }

    this.setState({
      value: value,
    });

    if (this.props.onChangeText) {
      this.props.onChangeText(value);
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  slide(direction) {
    if (direction == "up" && !this.state.isLabelUp) {
      Animated.spring(this.state.topPosition, {
        toValue: -16,
      }).start();

      this.setState({
        isLabelUp: true
      });
    } else if (direction == "down" && this.state.isLabelUp) {
      Animated.spring(this.state.topPosition, {
        toValue: labelHeight,
      }).start();

      this.setState({
        isLabelUp: false
      });
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderTimePicker() {
    if (!this.props.timePicker) {
      return null;
    }

    return ( 
      <DatePicker
        style={[styles.timePicker, this.props.half ? {width: (appMetrics.screenWidth - 60) / 2} : {}]}
        date={ new moment(this.state.value, "HH:mm") }
        mode="time"
        showIcon={ false }
        hideText={ true }
        format="HH:mm"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        onDateChange={ (value) => this.onChangeText(value) }
      />
    );
  }

// ---------------------------------------------------

  _renderIcon() {
    if (this.props.secureTextEntry) {
      return (
        <TouchableOpacity
          style={ styles.eyeButton.container }
          onPress={ () => this.setState({ isPasswordVisible: !this.state.isPasswordVisible }) }
        >
          <Icon name={ this.state.isPasswordVisible ? "eye" : "eye-slash" } color={ "#aaaaaa" } size={ 16 }/>
        </TouchableOpacity>
      );
    }

    if (this.props.timePicker) {
      return (
        <View
          style={ styles.eyeButton.container }
        >
          <Icon name={ "clock" } size={ 18 }/>
        </View>
      );
    }

    return null;
  }

// ---------------------------------------------------

  _renderNote() {
    let noteRender = null;
    if (!this.props.note) {
      return null;
    }

    return (
      <View style={ styles.note.container }>
        <Text color={ "#aaaaaa" } size={ 12 }>
          { this.props.note }
        </Text>
      </View>
    );    
  }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    

    return (
      <View style={ [styles.container, this.props.half ? {width: (appMetrics.screenWidth - 60) / 2} : {}] }>
        <Animated.View style={ [styles.label, (Platform.OS == "ios" ? {} : {left: 3,}), { top: this.state.topPosition, }] }>
          <Text color={ "#aaaaaa" } size={ 16 }>
            { this.props.label }
          </Text>
        </Animated.View>
        
        <TextInput
          ref={ this.props.refs }
          size={ 16 }
          style={ styles.input }
          { ... this.props }
          onChangeText={ (value) => this.onChangeText(value) }
          onFocus={ () => this.slide("up") }
          onBlur={ () => this.slide(this.state.value.length < 1 ? "down" : "up") }
          secureTextEntry={ this.props.secureTextEntry && !this.state.isPasswordVisible }
        />

        { this._renderTimePicker() }

        { this._renderIcon() }

        { this._renderNote() }
      </View>
    )
  }
};