/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 16:42:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 20:39:01
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  Alert
} from "react-native";
import { appVariables } from "@variables";
import styles from "./style";
import { connect } from "react-redux";
import _ from "lodash";
import { Actions } from "react-native-router-flux";

import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import MainButton from "@airport-transfer-fragments/MainButton";
import MainFormCard from "@airport-transfer-fragments/MainFormCard";
import MainFormStackCard from "@airport-transfer-fragments/MainFormStackCard";

import MainFormModal from "@airport-transfer-modals/MainForm";

import ColorHelper from "@airport-transfer-helpers/Color";

import { getCityCoverage, getAddressFromGoogle, searchKeyToGoogle, getAirportCoverage, getDistanceMatrixFromGoogle, getPlaceDetailFromGoogle, getFindGeometryFromGoogle, getPlaceDetailForToLocationBus, setActualBranchForBus } from "../../../../../actions";
import { AIRPORT_TRANSFER, BUS_RENTAL, CAR_RENTAL } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class MainFormContainer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  state = {
    cityCoverage: []
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return  true;
    }

    return false;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// EVENT LISTENERS
// ---------------------------------------------------

  onModalSelected(formKey, value) {
    const forms = this.props.formsState;
    const thisForm = forms[formKey];

    thisForm.value = value;
    forms[formKey] = thisForm;
    
    switch (formKey) {
      case "type":
        // if(this.props.title === "Car Rental") {
        if(this.props.selectedProduct === CAR_RENTAL) {
          let product = _.filter(this.props.products, { MsProductId: this.props.selectedProduct });
          this.props.getCityCoverage({ 
            BusinessUnitId: product[0].BusinessUnitId,
            MsProductId: product[0].MsProductId
          });
          // if(forms[formKey].value === "PSV0001") {
          //   Alert.alert(
          //     'Info',
          //     this.props.isLogin ? 
          //       (this.props.login.Data.HasCreditCard ?
          //       'Layanan Self Drive memerlukan Credit Card untuk validasi.' :
          //       'Jika Anda memilih layanan Self Drive, maka anda diwajibkan untuk melengkapi Credit Card') : 
          //     'Jika Anda memilih layanan Self Drive, maka anda diwajibkan untuk login terlebih dahulu dan melengkapi data Credit Card',
          //     [
          //       this.props.isLogin && !this.props.login.Data.HasCreditCard ? {text: 'Atur Nanti', onPress: () => this.toggleModal(formKey, false) } : null,
          //       this.props.isLogin && this.props.login.Data.HasCreditCard ? { text: 'Saya Mengerti', onPress: () => this.toggleModal(formKey, false), style: 'cancel' } : { text: 'Batal', onPress: () => this.toggleModal(formKey, false), style: 'cancel' },
          //       this.props.isLogin ? 
          //         (this.props.login.Data.HasCreditCard ?
          //           null :
          //           { text: "Atur Sekarang", onPress: () => {
          //             this.toggleModal(formKey, false)
          //             Actions.push("ProfileCreditCard") 
          //           }}) :
          //         { text: "Login Sekarang", onPress: () => {
          //           this.toggleModal(formKey, false)
          //           Actions.push("Login") 
          //         }}
          //     ],
          //     {cancelable: false},
          //   );
          // }
        }
        break;
      case "from_city":
        this.props.searchKeyToGoogle(thisForm.value);
        this.props.getAddressFromGoogle(`+${_.replace(thisForm.value, /\s/g, "")}`)
        break;
      case "from_airport":
        // if(this.props.title === "Airport Transfer") {
        if(this.props.selectedProduct === AIRPORT_TRANSFER) {
          let airport = _.filter(thisForm.options, { value: thisForm.value });
          this.props.getAirportCoverage({ MsAirportCode: thisForm.value });
          this.props.getFindGeometryFromGoogle(airport[0].label);
        }
        break;
      case "to_city":
        this.props.searchKeyToGoogle(thisForm.value);
        this.props.getAddressFromGoogle(`+${_.replace(thisForm.value, /\s/g, "")}`)
        break;
      case "to_location":
        if(this.props.selectedProduct === AIRPORT_TRANSFER) {  // Airport Transfer
          this.props.getPlaceDetailFromGoogle(forms.to_location.value);
          this.props.getDistanceMatrixFromGoogle({
            origins: _.filter(forms.from_airport.options, { value: forms.from_airport.value })[0].label,
            destinations: _.filter(forms.to_location.options, { value: forms.to_location.value })[0].label
          })
        } else if(this.props.selectedProduct === BUS_RENTAL) { // Bus Rental
          // this.props.getAddressFromGoogle(`+Indonesia`);
          this.props.getPlaceDetailForToLocationBus(true);
          this.props.getPlaceDetailFromGoogle(forms.to_location.value);
          this.props.getDistanceMatrixFromGoogle({
            origins: _.filter(forms.from_location.options, { value: forms.from_location.value })[0].label,
            destinations: _.filter(forms.to_location.options, { value: forms.to_location.value })[0].label
          });
        }
        break;
      case "from_location":
        this.props.getPlaceDetailFromGoogle(forms.from_location.value);
        if(this.props.selectedProduct === BUS_RENTAL) {
          // this.props.getPlaceDetailForToLocationBus(false);
        }
        break;
      default:
        break;
    }

    this.props.onFormsStateChange(forms);

    const _this = this;
    // setTimeout(function() {
      _this.toggleModal(formKey, false);
    // }, 100);
  }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN METHODS
// ---------------------------------------------------

  toggleModal(formKey, isOpened) {
    const forms = this.props.formsState;
    const thisForm = forms[formKey];

    thisForm.isModalOpened = isOpened;
    forms[formKey] = thisForm;
    
    this.props.onFormsStateChange(forms);
  }

// ---------------------------------------------------

  getBackgroundColor(form, index) {
    const forms = this.props.forms;

    return ColorHelper.getBlueShadeBySteps(forms.length, index);
  }


// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderForms() {
    let chaining = 0;
    return this.props.forms.map((form, index) => {
      if (form.onCancelChain) {
        chaining ++;
      }

      if (!form.isHidden) {
        return (
          <MainFormCard
            key={ index }
            formKey={ form.type }
            label={ form.label }
            placeholder={ form.placeholder } 
            backgroundColor={ this.getBackgroundColor(form, index - chaining) }
            isDisabled={ form.isDisabled }
            hasSearchIcon={ form.hasSearchIcon }
            modifier={ form.modifier }
            formsState={ this.props.formsState }
            onPress={ (formKey, isOpened) => this.toggleModal(formKey, isOpened) }
            isError={ this.props.missingForms.indexOf(form.type) !== -1 || this.props.missingForms.indexOf(form.onSuccessChain) !== -1 }
          />
        );
      }
    });
  }

// ---------------------------------------------------

  _renderStacks() {
    if (!this.props.formStack) {
      return null;
    }

    return(
      <View>
        { this.props.formStack.stacks.map((item, index) => {
          const isRemovable = this.props.formStack.onRemovePress && index > 0;

          if (!item.isVoid) {
            return (
                <MainFormStackCard
                key={ index }
                label={ item.title }
                onPress={ () => this.props.formStack.onStackPress(index) }
                onRemovePress={ isRemovable ? () => this.props.formStack.onRemovePress(index) : null }
                isActive={ this.props.formStack.activeIndex === index }
                isRemovable={ isRemovable }
              />
            );
          }
        }) }
      </View>
    );
  }

// ---------------------------------------------------

  _renderModals() {
    let chaining = 0;
    return this.props.forms.map((form, index) => {
      if (form.onCancelChain) {
        chaining ++;
      }

      return (
        <MainFormModal
          key={ index }
          formsState={ this.props.formsState }
          formKey={ form.type }
          title={ form.modalTitle }
          backgroundColor={ this.getBackgroundColor(form, index- chaining) }
          onSuccessChain={ form.onSuccessChain }
          onCancelChain={ form.onCancelChain }
          optionsModifier={ form.optionsModifier }
          onModalSelected={ (value) => this.onModalSelected(form.type, value) }
          toggleModal={ (isOpened) => this.toggleModal(form.type, isOpened) }
          onChainingModalTriggered={ (chainKey, shouldChain) => this.props.onChainingModalTriggered(form.type, chainKey, shouldChain) }
        />
      );
    });
  }

// ---------------------------------------------------

  _renderHeader() {
    return (
      <View style={ styles.header.container }>
        <View style={ styles.header.control.grouper }>
          <TouchableOpacity
            style={ styles.header.control.container }
            onPress={ () => this.props.onBackPressed() }
          >
            <Icon name="times" color={ appVariables.colorWhite } size={ 20 }/>
          </TouchableOpacity>
        </View>

        <Text color={ appVariables.colorWhite } size={ 25 }>
          { this.props.title }
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderExtraFooterButton(label, onPress = null) {
    if (!onPress) {
      return null;
    }

    return (
      <MainButton
        label={ label }
        onPress={ () => onPress() }
        backgroundColor={ "#091A46" }
      />
    )
  }

// ---------------------------------------------------

  _renderFooterButton(onPress = null) {
    if (!onPress) {
      return null;
    }

    return (
      <MainButton
        label={ Lang.search[this.props.lang] }
        onPress={ () => onPress() }
        icon={ "search" }
      />
    )
  }


// ---------------------------------------------------

  _renderFooter() {
    let nextTripButtonRender = null;

    if (this.props.formStack && this.props.formStack.nextButton.isVisible) {
      nextTripButtonRender = this._renderExtraFooterButton(this.props.formStack.nextButton.title, () => this.props.formStack.nextButton.onPress())
    }

    return (
      <View style={ styles.footer.container }>
        { nextTripButtonRender }

        { this._renderFooterButton(this.props.onSubmitPressed ? () => this.props.onSubmitPressed() : null) }
      </View>
    )
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BaseContainer backgroundColor={ "#12265B" }>

        { this._renderHeader() }

        <ScrollView style={ styles.contentContainer } keyboardShouldPersistTaps="always" >
          
          { this._renderStacks() }

          <View>
            { this._renderForms() }
          </View>

        </ScrollView>

        { this._renderFooter() }

        { this._renderModals() }

      </BaseContainer>
    )
  }
};

const mapStateToProps = ({ productService, cities, addressFromGoogle, authentication, language, masterBranch }) => {
  const { products, selectedProduct } = productService;
  const { cityCoverage } = cities;
  const { address, distanceMatrixForBus } = addressFromGoogle;
  const { isLogin, login } = authentication;
  const { lang } = language;
  const { branch } = masterBranch;
  return { products, selectedProduct, cityCoverage, address, isLogin, login, lang, branch, distanceMatrixForBus };
}

export default connect(mapStateToProps, {
  getCityCoverage,
  getAddressFromGoogle,
  searchKeyToGoogle,
  getAirportCoverage,
  getDistanceMatrixFromGoogle,
  getPlaceDetailFromGoogle,
  getFindGeometryFromGoogle,
  getPlaceDetailForToLocationBus,
  setActualBranchForBus
})(MainFormContainer);
