import {
  appMetrics,
  appVariables,
} from "@variables";

export default {
  contentContainer: {
    flex: 1,
    backgroundColor: "#12265B",
  },

  header: {
    container: {
      backgroundColor: appVariables.colorBlueHeader,
      flex: -1,
      paddingHorizontal: 20,
      paddingTop: 15,
      paddingBottom: 25,
    },

    control: {
      grouper: {
        flex: -1,
      },

      container: {
        alignSelf: "flex-end",
        paddingLeft: 20,
        paddingBottom: 5,
      },
    },
  },

  footer: {
    container: {
      flex: -1,
      padding: 0,
    },
  },
}
