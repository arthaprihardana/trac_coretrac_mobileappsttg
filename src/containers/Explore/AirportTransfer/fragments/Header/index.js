import { Container, Content } from "native-base";
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics
} from "@variables";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";

import styles from "./style";


export default class Header extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let backButtonRender = null;
    if (this.props.onBackPress) {
      backButtonRender = <Icon name="arrow-left" size={ 16 } color={ appVariables.colorWhite } />;
    }

    return (
      <View style={ styles.container }>
        <TouchableOpacity
          style={ [styles.button.container, {paddingLeft: 20,}] }
          onPress={ this.props.onBackPress ? () => this.props.onBackPress() : () => {}}
        >
          { backButtonRender }
        </TouchableOpacity>

        <Text style={ styles.title }>
          { this.props.title }
        </Text>

        <View
          style={ [styles.button.container, {paddingRight: 20,}] }
        />

      </View>
    );
  }

}