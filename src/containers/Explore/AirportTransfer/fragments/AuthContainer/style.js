import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  container: {
    flex: 1,
  },

  innerContainer: {
    flex: 1,
    padding: 20,
    paddingTop: 10,
    backgroundColor: appVariables.colorWhite,
  },

  innerContentContainer: {
    paddingBottom: 40,
  },
}