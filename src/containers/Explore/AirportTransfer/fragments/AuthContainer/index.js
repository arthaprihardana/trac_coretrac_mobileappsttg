/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-28 11:05:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 13:35:36
 */
import { Container, Content } from "native-base";
import { Actions } from "react-native-router-flux";
import React, { Component } from "react";
import { 
  View,
  ScrollView,
  KeyboardAvoidingView,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Header from "@airport-transfer-fragments/Header";
import ProgressedContainer from "@airport-transfer-fragments/ProgressedContainer";
import BaseContainer from "@airport-transfer-fragments/BaseContainer";

import styles from "./style";
import Lang from '../../../../../assets/lang';

class AuthContainer extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------
  
  _renderMain() {
    return (
      <KeyboardAvoidingView
        behavior={ "padding" }
        enabled={false}
        style={ styles.container }
      >
        <ScrollView
          style={ styles.innerContainer }
          contentContainerStyle={ styles.innerContentContainer }
          keyboardShouldPersistTap="always"
        >
          
          { this.props.children }

        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const navData = this.props.navData;

    if (!navData) {
      return (
        <BaseContainer backgroundColor={ appVariables.colorBlueHeader }>
          <Header
            title={ this.props.title }
            onBackPress={ () => Actions.reset('Home') }
          />

          { this._renderMain() }        

        </BaseContainer>
      );
    }

    return (
      <ProgressedContainer
        header={{
          label: this.props.title,
          onBackPress: () => Actions.pop(),
        }}
        steps= { navData.processSteps }
        currentStepIndex={ navData.processSteps.indexOf(Lang.personalDetail[this.props.lang]) }
        { ... this.props }
      >

        { this._renderMain() }

      </ProgressedContainer>
    );
  }
}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(AuthContainer)