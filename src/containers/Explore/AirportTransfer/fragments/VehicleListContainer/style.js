import {
	appVariables,
	appMetrics,
} from "@variables";

export default {

	subHeader: {
		container: {
      flex: -1,
      flexDirection: "row",
      height: 40,
      backgroundColor: appVariables.colorBlueHeader,
      alignItems: "center",
      paddingHorizontal: 20,
    },

    button: {
    	container: {
        flex: -1,
        height: 40,
        width: 60,
        justifyContent: "center",
        alignItems: "flex-end",
      },
    },

    icon: {
      flex: -1,
      color: appVariables.colorWhite,
      fontSize: 15,
      textAlign: "center",
      paddingRight: 10,
    },

    label: {
      flex: 1,
      color: appVariables.colorWhite,
      fontSize: 12,
      paddingLeft: 7,
    },
	},

	listControl: {
		container: {
      flex: -1,
      flexDirection: "row",
      height: 40,
      backgroundColor: "#eeeeeeee",
      paddingHorizontal: 20,
      borderBottomWidth: 1,
      borderColor: "#cccccccc",
    },

		leftButton: {
			grouper: {
        flex: -1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
      },

      container: {
        flex: -1,
        height: 40,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 15,
        borderRightWidth: 1,
        borderColor: "#cccccccc",
      },

      label: {
        flex: -1,
        fontSize: 12,
        paddingRight: 13,
      },
		},

		rightButton: {
			grouper: {
        flex: 1,
      },

      container: {
        flex: -1,
        height: 40,
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 15,
      },

      label: {
        flex: 1,
        fontSize: 12,
        paddingRight: 7,
      },
		},
	},

	listRoute: {
		container: {
      flex: -1,
      flexDirection: "row",
      height: 48,
      borderBottomWidth: 1,
      borderColor: "#cccccccc",
      paddingHorizontal: 20,
      alignItems: "center",
      backgroundColor: appVariables.colorWhite,
    },
	},

	modal: {
		fallBackContainer: {
      flex: -1,
      position: "absolute",
      width: appMetrics.windowWidth,
      height: appMetrics.windowHeight,
      // top: 0,
      top: -40,
      // left: 0,
      left: 3
    },

		currency: {
			container: {
	      flex: -1,
	      position: "absolute",
	      top: 140,
	      right: 20,
	      backgroundColor: appVariables.colorWhite,
	      paddingHorizontal: 10,
	      borderWidth: 1,
	      borderColor: "#cccccc",
	      borderRadius:4,
	    },

	    item: {
	    	container: {
	        flex: -1,
	        flexDirection: "row",
	        paddingVertical: 10,
	        borderBottomWidth: 1,
	        borderColor: "#cccccc",
	        alignItems: "center",
	      },

	      image: {
	        width: 20,
	      },

	      label: {
	        paddingLeft: 10,
	        fontSize: 14,
	      },
	    },
		},

		sort: {
			container: {
	      flex: -1,
	      position: "absolute",
	      top: 140,
	      left: 102,
	      backgroundColor: appVariables.colorWhite,
	      paddingHorizontal: 10,
	      borderWidth: 1,
	      borderColor: "#cccccc",
	      borderRadius:4,
	    },

	    item: {
	    	container: {
	        flex: -1,
	        flexDirection: "row",
	        paddingVertical: 10,
	        borderBottomWidth: 1,
	        borderColor: "#cccccc",
	        alignItems: "center",
	      },

	      label: {
	        paddingLeft: 10,
	        fontSize: 14,
	      },
	    },
		},
	},
  
}










