/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 14:36:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 14:24:16
 */
import { Content } from "native-base";
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import CheckoutFooter from "@airport-transfer-fragments/CheckoutFooter";
import Header from "@airport-transfer-fragments/Header";

import TextHelper from "@airport-transfer-helpers/Text";

import styles from "./style";

import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Lang from '../../../../../assets/lang';

class VehicleListContainer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isCurrencyModalVisible: false,
      isSortModalVisible: false,
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  sort(sort) {
    this.setState({
      isSortModalVisible: false
    });

    this.props.sort.onSelect(sort);
  }

// ---------------------------------------------------

  // selectCurrency(curr) {
  //   this.setState({
  //     isCurrencyModalVisible: false
  //   });

  //   this.props.currency.onSelect(curr);
  // }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderSubHeader() {
    return (
      <View style={ styles.subHeader.container }>
        <Icon name="file-alt" size={ 16 } color={ appVariables.colorWhite } />

        <Text style={ styles.subHeader.label }>
          MODIFY BOOKING
        </Text>

        <TouchableOpacity
          style={ styles.subHeader.button.container }
        >
          <Icon name="arrow-right" size={ 16 } color={ appVariables.colorWhite } />
        </TouchableOpacity>
      </View>
    );
  }

// ---------------------------------------------------

  _renderListControl() {
    return (
      <View style={ styles.listControl.container }>
        <View style={ styles.listControl.leftButton.grouper }>

          <TouchableOpacity
            style={ [styles.listControl.leftButton.container, {paddingHorizontal: 0, paddingRight: 20,}] }
            onPress={ () => this.props.filter.toggle(true) }
          >
            <Text style={ styles.listControl.leftButton.label }>
              {Lang.filter[this.props.lang]}
            </Text>
            
            <Icon name="filter" size={ 14 } />
          </TouchableOpacity>

          <TouchableOpacity
            style={ styles.listControl.leftButton.container }
            onPress={ () => this.setState({isSortModalVisible: true}) }
          >
            <Text style={ styles.listControl.leftButton.label }>
              {Lang.sort[this.props.lang]}
            </Text>
            
            <Icon name="angle-down" size={ 16 } />
          </TouchableOpacity>

        </View>

        <View style={ styles.listControl.rightButton.grouper }>

        {/* <TouchableOpacity
          style={ styles.listControl.rightButton.container }
          onPress={ () => this.setState({isCurrencyModalVisible: true}) }
        >
          <Text style={ styles.listControl.rightButton.label }>
            CURRENCY: { this.props.currency.selected.code }
          </Text>

          <Icon name="angle-down" size={ 16 } />
        </TouchableOpacity> */}

        </View>
      </View>
    );
  }

// ---------------------------------------------------

  _renderListRoute() {
    let arrowRender = null;
    let toRender = null;
    if (this.props.direction.to) {
      arrowRender = <Icon name="arrow-right" size={ 17 } color={ appVariables.colorOrange } />;

      toRender = <Text style={ [styles.listRoute.text, {paddingLeft: 7}] }>
        {Lang.ke[this.props.lang]}: { TextHelper.shorten(this.props.direction.to, 10) }
      </Text>;
    }

    return (
      <View style={ styles.listRoute.container }>
        <Text style={ [styles.listRoute.text, {paddingRight: 7}] }>
          {Lang.from[this.props.lang]}: { TextHelper.shorten(this.props.direction.from, !!toRender ? 10 : 20) }
        </Text>
        
        { arrowRender }

        { toRender }
      </View>
    );
  }

// ---------------------------------------------------

  // _renderCurrencyModal() {
  //   if (!this.state.isCurrencyModalVisible) {
  //     return null;
  //   }

  //   return (
  //     <TouchableOpacity
  //       style={ styles.modal.fallBackContainer }
  //       onPress={ () => this.setState({isCurrencyModalVisible: false}) }
  //     >
  //       <View style={ styles.modal.currency.container }>

  //         { this.props.currency.options.map((curr, index) => {
  //           return (
  //             <TouchableOpacity
  //               key={ index }
  //               style={ [styles.modal.currency.item.container, (index == this.props.currency.options.length - 1 ? {borderBottomWidth: 0} : {})] }
  //               onPress={ () => this.selectCurrency(curr) }
  //             >
  //               <Image
  //                 style={ styles.modal.currency.item.image }
  //                 resizeMode={ "contain" }
  //                 source={ curr.image }
  //               />

  //               <Text style={ styles.modal.currency.item.label }>
  //                 { curr.name }
  //               </Text>
  //             </TouchableOpacity>
  //           );
  //         }) }

  //       </View>
  //     </TouchableOpacity>
  //   );
  // }

// ---------------------------------------------------

  _renderSortModal() {
    if (!this.state.isSortModalVisible) {
      return null;
    }

    return (
      <TouchableOpacity
        style={ styles.modal.fallBackContainer }
        onPress={ () => this.setState({isSortModalVisible: false}) }
      >
        <View style={ styles.modal.sort.container }>

          { this.props.sort.options.map((sort, index) => {
            return (
              <TouchableOpacity
                key={ index }
                style={ [styles.modal.sort.item.container, (index == this.props.sort.options.length - 1 ? {borderBottomWidth: 0} : {})] }
                onPress={ () => this.sort(sort) }
              >

                <Icon
                  name={ sort.mode == "DESC" ? "sort-down" : (sort.mode == "ASC" ? "sort-up" : "sort") }
                  style={ (sort.mode == "DESC" ? {top: -4,} : (sort.mode == "ASC" ? {top: 4,} : {})) }
                  size={ 16 }
                />

                <Text style={ styles.modal.sort.item.label }>
                  { sort.label }
                </Text>
              </TouchableOpacity>
            );
          }) }

        </View>
      </TouchableOpacity>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <BaseContainer backgroundColor={ appVariables.colorBlueHeader }>

        <Header
          title={ this.props.title }
          onBackPress={ () => this.props.navigation.pop() }
        />

        {/* { this._renderSubHeader() } */}

        { this._renderListControl() }

        { this._renderListRoute() }

        <Content style={{ backgroundColor: "#eeeeee" }}>

          { this.props.children }

        </Content>

        { this.props.checkoutFooter }

        { this._renderSortModal() }

        {/* { this._renderCurrencyModal() } */}

        { this.props.filter.modal }
        
      </BaseContainer>
    );
  }

}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(VehicleListContainer)