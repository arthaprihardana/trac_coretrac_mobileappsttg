import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../../../assets/styles';
import SvgIcon from '../../../../../utils/SvgIcon';

class Sort extends Component {
    render() {
        let { label, value, noborder, onPress } = this.props;
        let { sortWrapper, borderRight, labelStyle, iconWrapper } = styles;
        return (
            <TouchableOpacity onPress={onPress}>
                <View style={[sortWrapper, noborder ? null : borderRight]}>
                    <Text style={labelStyle}>{label}: {value}</Text>
                    <View style={iconWrapper}>
                        <SvgIcon name="icoSort" />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    sortWrapper: { flexDirection: 'row', alignItems: 'center', padding: 20 },
    borderRight: { borderRightColor: '#D8D8D8', borderRightWidth: 1 },
    labelStyle: { fontSize: 12, fontFamily: appVariables.museo700 },
    iconWrapper: { marginLeft: 13 }
})

Sort.propTypes = {
    label: PropTypes.string, 
    value: PropTypes.any, 
    noborder: PropTypes.bool, 
    onPress: PropTypes.func
}

export default Sort;