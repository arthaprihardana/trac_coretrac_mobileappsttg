import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import { AButton, ACar, APricePackage, AText, AFiturIcon } from '../../../../../components/atoms';
import { OCounter } from '../../../../../components/organisms';

class CarItem extends Component {
    render() {
        let { seeDetail, carName, price, pessenger, baggage, transmition, airbag, ac, imgCar, statusNote, statusCar, onCounterChange, notifyMe } = this.props;
        return (
            <View style={{ backgroundColor: 'white' }}>
                <ACar carUri={imgCar} statusNote={statusNote} statusCar={statusCar} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', marginHorizontal: 20 }}>
                    <AText carName={carName} />
                    <APricePackage price={price} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopColor: '#D8D8D8', borderTopWidth: 1, marginHorizontal: 20, paddingTop: 11, marginTop: 12, paddingBottom: 10 }}>
                    <AFiturIcon label={pessenger} iconName="icoPeople"/>
                    <AFiturIcon label={baggage} iconName="icoBaggage" />
                    <AFiturIcon label={transmition} iconName="icoManual" />
                    <AFiturIcon label={airbag} iconName="icoAirbag" check={true} />
                    <AFiturIcon label={ac} iconName="icoAC" check={true} />
                </View>
                <View style={{ backgroundColor: '#F3F4F5', flexDirection: 'row', paddingVertical: 8, justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center' }}>
                    <View>
                        <AButton arrow title="SEE DETAIL" onPress={seeDetail} />
                    </View>
                    <View>
                        {
                            statusCar === 3 ? <AButton bordered info onPress={notifyMe}>{`Notify Me`.toUpperCase()}</AButton> : <OCounter onChange={onCounterChange} />
                        }
                        
                    </View>
                </View>
            </View>
        )
    }
}

CarItem.propTypes = {
    seeDetail: PropTypes.func, 
    carName: PropTypes.string, 
    price: PropTypes.number, 
    passanger: PropTypes.number, 
    suitcase: PropTypes.number, 
    transmition: PropTypes.string, 
    imgCar: PropTypes.any, 
    statusNote: PropTypes.string, 
    statusCar: PropTypes.number, 
    onCounterChange: PropTypes.func, 
    notifyMe: PropTypes.func
}

export default CarItem;