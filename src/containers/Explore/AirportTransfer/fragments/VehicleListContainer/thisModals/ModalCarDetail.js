import { Container, Content } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { ACar, APricePackage, AText, ATextFitur } from '../../../../../components/atoms';
import { MList } from '../../../../../components/molecules';
import SvgIcon from '../../../../../utils/SvgIcon';
import AFiturIcon from '../../../../../components/atoms/AFiturIcon';

class ModalCarDetail extends Component {
    render() {
        let { onClose, carDetail } = this.props;
        return (
            <Container>
                <View style={{ width: '100%', justifyContent: 'flex-end', flexDirection: 'row', padding: 20 }}>
                    <TouchableOpacity onPress={onClose}>
                        <SvgIcon name="icoCloseDark" />
                    </TouchableOpacity>
                </View>
                <Content>
                    <View style={{ paddingBottom: 40 }}>
                        <View style={{alignItems: 'center'}}>
                            <AText carName={carDetail.name} />
                            <APricePackage price={carDetail.price} />
                        </View>
                        <ACar carUri={carDetail.img} label="CAR ON BOOKING" onBooking />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopColor: '#D8D8D8', borderTopWidth: 1, borderBottomColor: '#D8D8D8', borderBottomWidth: 1, marginHorizontal: 20, paddingTop: 11, marginTop: 12, paddingBottom: 10 }}>
                            <AFiturIcon label={'6'} iconName="icoPeople"/>
                            <AFiturIcon label={'3'} iconName="icoBaggage" />
                            <AFiturIcon label={'MT'} iconName="icoManual" />
                            <AFiturIcon iconName="icoAirbag" check={true} />
                            <AFiturIcon iconName="icoAC" check={true} />
                            {/* <ATextFitur value={carDetail.passanger} label="Passanger" />
                            <ATextFitur value={carDetail.suitcase} label="Suitecase" />
                            <ATextFitur value={carDetail.transmition} label="Transmission" /> */}
                        </View>
                        {
                            carDetail.moreDetail.map((detail, i) => {
                                return <MList key={i} label={detail.labelDetail} listItem={detail.contents} />
                            })
                        }
                    </View>
                </Content>
            </Container>
        )
    }
}

ModalCarDetail.propTypes = {
    onClose: PropTypes.func, 
    carDetail: PropTypes.object
}

export default ModalCarDetail;