import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
	container: {
    flex: -1,
    flexDirection: "row",
    height: 60,
    backgroundColor: 'transparent',
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: appVariables.colorTabInactive,
  },

  button: {
  	container: {
      flex: -1,
      height: 60,
      width: 60,
      justifyContent: "center",
    },
  },

  title: {
    flex: 1,
    color: appVariables.colorWhite,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
}