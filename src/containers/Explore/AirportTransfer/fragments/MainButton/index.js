/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-12 15:28:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-15 13:51:40
 */
import { Container, Content } from "native-base";
import React, { Component } from "react";
import {
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  appVariables,
  appMetrics
} from "@variables";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";

import styles from "./style";


export default class MainButton extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let baseStyle = styles.main;
    if (this.props.rounded) {
      baseStyle = styles.rounded;
    }

    return (
      <TouchableOpacity
        style={ [this.props.disabled ? baseStyle.disabled : baseStyle.container, this.props.style, this.props.backgroundColor ? {backgroundColor: this.props.backgroundColor} : {}] }
        onPress={ () => this.props.onPress() }
      >
        { this.props.isLoading ? 
          <ActivityIndicator color={'#FFFFFF'} size="small" />
        : 
        <Text leftIcon={ this.props.icon } color={ appVariables.colorWhite } size={ 16 } bold>
          { this.props.label }
        </Text>}
      </TouchableOpacity>
    );
  }

}