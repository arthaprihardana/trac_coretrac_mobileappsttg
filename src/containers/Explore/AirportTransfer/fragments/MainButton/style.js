import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  main: {
    container: {
      flex: -1,
      height: 60,
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row",
      backgroundColor: appVariables.colorOrange,
    },
    disabled: {
      flex: -1,
      height: 60,
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row",
      backgroundColor: appVariables.colorGray,
    }
  },

	rounded: {
    container: {
      flex: -1,
      borderRadius: 7,
      backgroundColor: appVariables.colorOrange,
      alignItems: "center",
      paddingVertical: 13,
      marginTop: 10,
    },
    disabled: {
      flex: -1,
      borderRadius: 7,
      backgroundColor: appVariables.colorGray,
      alignItems: "center",
      paddingVertical: 13,
      marginTop: 10,
    }
  },
}