import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import { appVariables } from "@variables";
import styles from "./style";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";


export default class MainFormStackCard extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

    shouldComponentUpdate(nextProps, nextState) {
      if (nextProps != this.props) {
        return true;
      }

      return false;
    }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let removeButton = null;
    if (this.props.isRemovable) {
      removeButton = <TouchableOpacity
        style={ styles.removeButton.container }
        onPress={ () => this.props.onRemovePress() }
      >
        <Icon name="times" color={ appVariables.colorWhite } size={ 20 }/>
      </TouchableOpacity>;
    }

    return (
      <View
        style={ styles.container }
      >
        <TouchableOpacity
          style={ styles.label.container }
          onPress={ () => this.props.onPress() }
        >
          <Text style={{
            color: this.props.isActive ? appVariables.colorWhite : appVariables.colorTabInactive,
          }}>
            { this.props.label }
          </Text>
        </TouchableOpacity>

        { removeButton }
      </View>
    );
  }
};