import {
  appMetrics,
  appVariables,
} from "@variables";

export default {
  container: {
    flex: -1,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderColor: "#979797",
    flexDirection: "row",
  },

  label: {
    container: {
      flex: 1,
      height: 60,
      justifyContent: "center",
    },
  },

  removeButton: {
    container: {
      flex: -1,
      width: 32,
      height: 32,
      borderWidth: 1,
      borderRadius: 32,
      justifyContent: "center",
      alignItems: "center",
      borderColor: appVariables.colorWhite,
      alignSelf: "center",
      overflow: "hidden",
    },
  },
}