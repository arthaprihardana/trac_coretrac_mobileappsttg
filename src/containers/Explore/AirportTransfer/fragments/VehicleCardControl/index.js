/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-22 14:38:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 11:55:09
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
} from "@variables";
import { connect } from 'react-redux';

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import MyWebView from '@airport-transfer-fragments/WebView';
import Lang from '../../../../../assets/lang';
import styles from "./style";


class VehicleCardControl extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderNotifyButton() {
    return (
      <TouchableOpacity
        style={ styles.noStock.container }
        onPress={ () => alert("Notify Me") }
      >
        <Text style={ styles.noStock.text }>
          Notify Me
        </Text>
      </TouchableOpacity>
    );
  }

// ---------------------------------------------------

  _renderCountControl() {
    return (
      <View style={ styles.amountControl.container }>
        <TouchableOpacity
          onPress={ () => this.props.onDecreaseCount() }
          style={ styles.amountControl.button.container }
        >
          <Icon name="minus" size={ 10 }/>
        </TouchableOpacity>

        <View style={ styles.amountControl.value.container }>
          <Text style={ styles.amountControl.value.text }>
            { this.props.countValue }
          </Text>
        </View>

        <TouchableOpacity
          onPress={ () => this.props.onIncreaseCount() }
          style={ styles.amountControl.button.container }
        >
          <Icon name="plus" size={ 10 }/>
        </TouchableOpacity>
      </View>
    );
  }

// ---------------------------------------------------

  _renderAmountControl() {
    if (this.props.stock < 1) {
      return this._renderNotifyButton();
    }

    return this._renderCountControl();
  }

// ---------------------------------------------------

  _renderDescriptionButton() {
    return (
      <TouchableOpacity
        style={ styles.detailButton.container }
        onPress={ () => this.props.onDetailToggle() }
      >
        <Text style={ styles.detailButton.label }>
          { this.props.isDetailVisible ? Lang.hideDetail[this.props.lang] : Lang.showDetail[this.props.lang] }
        </Text>

        <Icon name="arrow-right" size={ 14 } color={ appVariables.colorOrange } />
      </TouchableOpacity>
    );
  }

// ---------------------------------------------------

  _renderDescription() {
    // if (!this.props.isDetailVisible || !this.props.children) {
    //   return null;
    // }

    // return this.props.children;
    return this.props.isDetailVisible ? this.props.children : null;
  }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View>
        <View style={ styles.container }>
          
          { this._renderDescriptionButton() }

          { this._renderAmountControl() }

        </View>

        { this._renderDescription() }
      </View>
    );
  }
};

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang };
}

export default connect(mapStateToProps, {

})(VehicleCardControl)