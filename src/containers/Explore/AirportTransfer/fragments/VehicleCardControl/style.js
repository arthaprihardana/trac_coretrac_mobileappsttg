import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
	container: {
    flex: -1,
    flexDirection: "row",
    height: 60,
    backgroundColor: "#eeeeee",
    paddingHorizontal: 20,
    alignItems: "center",
  },

  detailButton: {
  	container: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
    },

    label: {
      paddingRight: 10,
      fontSize: 14,
    },
  },

  amountControl: {
  	container: {
      flex: -1,
      flexDirection: "row",
      width: 120,
      height: 40,
      borderRadius: 20,
      backgroundColor: appVariables.colorWhite,
    },

    button: {
    	container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
    },

    value: {
    	container: {
        flex: -1,
        width: 40,
        justifyContent: "center",
        alignItems: "center",
      },

      text: {
        fontSize: 18,
      },
    },
  },

  noStock: {
    container: {
      borderWidth: 1,
      borderColor: "#dddddd",
      borderRadius: 7,
      paddingVertical: 10,
      paddingHorizontal: 16,
      backgroundColor: appVariables.colorWhite,
    },

    text: {
      color: "#888888",
      fontSize: 15,
      fontWeight: "bold",
    },
  },
}