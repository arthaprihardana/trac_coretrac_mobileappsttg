import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
  grouper: {
    flex: -1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 10,
  },

	container: {
    flex: -1,
    flexDirection: "row",
    alignItems: "center",
  },

  value: {
    flex: -1,
    paddingLeft: 5,
    fontSize: 13,
  },
}