/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 14:40:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-25 11:28:23
 */
import React, { Component } from "react";
import { View } from "react-native";
import { appVariables } from "@variables";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";

import styles from "./style";



export default class FetureList extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderFeature(type, value, index) {
    const sizeMultiplier = this.props.sizeMultiplier ? this.props.sizeMultiplier : 1;

    if (typeof value === "boolean") {
      value = value === true ? <Icon name="check" size={ 10 * sizeMultiplier } color={ this.props.valueColor }/> : <Icon name="times" size={ 10 * sizeMultiplier } color={ this.props.valueColor }/>;
    }

    return (
      <View
        key={ index }
        style={ styles.container }
      >
        <Icon type={ "trac" } name={ type } size={ 14 * sizeMultiplier } color={ "#999999" }/>
        
        <Text style={ styles.value } color={ this.props.valueColor } sie={ 14 * sizeMultiplier }>
          { value }
        </Text>
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.grouper }>

        { this.props.features.map((feature, index) => {
          return this._renderFeature(feature.type, feature.value, index);
        }) }

      </View>
    );
  }
};
