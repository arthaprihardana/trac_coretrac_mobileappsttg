import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
	container: {
	  flex: -1,
	  flexDirection: "row",
	  alignItems: "center",
	},

	radio: {
	  flex: -1,
	  height: 20,
	  width: 20,
	  borderWidth: 1,
	  borderRadius: 50,
	  marginRight: 7,
	},

	activeRadio: {
    marginRight: 7,
  },
}