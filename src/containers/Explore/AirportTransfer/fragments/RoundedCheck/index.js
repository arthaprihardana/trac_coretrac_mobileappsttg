import React, { Component } from "react";

import { 
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";

import Icon from "@airport-transfer-fragments/Icon";

import styles from "./style";

export default class RoundedCheck extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let radioRender = <View style={ styles.radio }/>;
    if (this.props.value) {
      radioRender = <Icon name="check-circle" solid size={ 20 } color={ appVariables.colorBlueHeader } style={ styles.activeRadio }/>;
    }

    return (
      <TouchableOpacity
        style={ [styles.container, this.props.style] }
        onPress={ () => this.props.onPress(!this.props.value) }
      >
        { radioRender }

        { this.props.children }
      </TouchableOpacity>
    );
  }
};