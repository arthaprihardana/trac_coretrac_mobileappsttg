export default {
	container: {
    flex: -1,
    flexDirection: "row",
    alignItems: "center",
  },
  
	base: {
		color: "black",
		fontSize: 14,
	}
}