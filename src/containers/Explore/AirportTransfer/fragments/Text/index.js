import React, { Component } from "react";
import { Text as RText, View } from "react-native";
import { appVariables } from "@variables";

import Icon from "@airport-transfer-fragments/Icon";

import styles from "./style";

export default class Text extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    const thisStyles = JSON.parse(JSON.stringify(styles.base));

    if (this.props.color) {
      thisStyles.color = this.props.color;
    }

    if (this.props.bold) {
      thisStyles.fontWeight = "bold";
    }

    if (this.props.alignRight) {
      thisStyles.textAlign = "right";
      thisStyles.flex = 1;
    }

    if (this.props.size) {
      thisStyles.fontSize = this.props.size;
    }

    let leftIcon = null;
    if (this.props.leftIcon) {
      leftIcon = <Icon
        name={ this.props.leftIcon }
        size={ thisStyles.fontSize }
        color={ thisStyles.color }
        style={{ paddingRight: thisStyles.fontSize / 2.5 }}
      />;
    }


    return (
      <View style={ styles.container }>
        { leftIcon }

        <RText
          style={ [thisStyles, this.props.style ? this.props.style : { }] }
        >
          { this.props.children }
        </RText>
      </View>
    );
  }
};