/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 23:30:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-27 22:53:56
 */
import React, { Component } from "react";
import {
  View,
  Animated,
  TouchableOpacity,
  Platform,
  Picker,
  PickerIOS,
  Modal,
  Text,
  SafeAreaView,
  ScrollView,
  TextInput
} from "react-native";
import Icon from "@airport-transfer-fragments/Icon";
import Header from "@airport-transfer-fragments/Header";

import {
  appVariables,
  appMetrics,
} from "@variables";

import _ from "lodash";

import styles from "./style";

export default class PickerInput extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      value: this.props.value,
      modalVisible: false,
      searchText: null,
      item: this.props.item
    };
  }
  
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      if (this.props.value != nextProps.value) {
        this.setState({
          value: nextProps.value,
        });
      }

      return true;
    }

    return false;
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <TouchableOpacity style={ [styles.container, this.props.half ? {width: (appMetrics.screenWidth - 60) / 2} : {}] } onPress={() => this.setModalVisible(!this.state.modalVisible)} >
        <View style={{ width: '80%' }}>
          <Text style={{ color: this.props.value !== null ? '#000' : '#ccc' }}>{ this.props.value || this.props.placeholder }</Text>
        </View>
        <View style={{ width: '20%', justifyContent: 'center', alignItems: 'flex-end', paddingRight: 10 }}>
          <Icon
            name="caret-down"
            size={12}
            color={appVariables.colorGray} />
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this.setModalVisible(!this.state.modalVisible) }>
          <SafeAreaView style={{flex: 1, backgroundColor: appVariables.colorBlueHeader }}>
            <Header title={this.props.placeholder} onPress={this._onPressButton} onBackPress={ () => this.setModalVisible(!this.state.modalVisible) }/>
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
              <View>
                <View style={{ height: 60, backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }}>
                  <TextInput
                    style={{height: 40, width: '100%', borderColor: '#ccc', borderWidth: 1, backgroundColor: '#ffffff', paddingHorizontal: 10 }}
                    onChangeText={text => {
                      this.setState({
                        item: text.length > 0 ? _.filter(this.props.item, v => v.value.match(new RegExp(text, "gi"))) : this.props.item
                      });
                    }}
                    value={this.state.searchText}
                    placeholder="Search"
                    placeholderTextColor={"#cccccc"}
                  />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                  { _.map(this.state.item, (v, k) => (
                    <TouchableOpacity key={k} style={{ height: 50, width: '100%', justifyContent: 'center', paddingHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#eee' }} onPress={() => {
                      this.props.onValueChange(v.value, v.label);
                      this.setModalVisible(!this.state.modalVisible);
                    }}>
                      <Text>{v.label}</Text>
                    </TouchableOpacity>
                  )) }
                </ScrollView>
              </View>
            </View>
          </SafeAreaView>
        </Modal>
        {/* { Platform.OS === "ios" ? 
        <PickerIOS style={{height: 50, width: '100%' }}
            { ...this.props } >
            { _.map(this.props.item, (v, k) => (
                <Picker.Item key={k} label={v.label} value={v.value} />
            )) }
        </PickerIOS>
        :
        <Picker style={{height: 50, width: '100%' }}
            { ...this.props } >
            { _.map(this.props.item, (v, k) => (
                <Picker.Item key={k} label={v.label} value={v.value} />
            )) }
        </Picker>
        } */}
      </TouchableOpacity>
    )
  }
};
