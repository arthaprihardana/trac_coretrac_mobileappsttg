import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  background: {
    container: {
      flex: -1,
      height: 370,
      width: appMetrics.screenWidth,
      position: "absolute",
    },
  },
  
  backButton: {
    container: {
      flex: -1,
      height: 60,
      width: 60,
      paddingTop: 20,
      paddingLeft: 20,
      position: "absolute",
    },
  },

  extra: {
    container: {
      flex: 1,
      paddingTop: 20,
      paddingBottom: 10,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: appVariables.colorWhite,
    },
  },
}