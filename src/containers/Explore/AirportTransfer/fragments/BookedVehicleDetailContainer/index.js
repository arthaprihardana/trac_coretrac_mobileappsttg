/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-08 11:32:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 14:05:49
 */
import { Container, Content } from "native-base";
import { Actions } from "react-native-router-flux";
import React, { Component } from "react";
import { 
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import _ from "lodash";
import { connect } from "react-redux";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";

import VehicleSliderDetail from "@airport-transfer-fragments/VehicleSliderDetail";
import ListSliderFooter from "@airport-transfer-fragments/ListSliderFooter";

import styles from "./style";

import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import { BUS_RENTAL } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class BookedVehicleDetailContainer extends Component {
  
// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderBackgroundImage() {
    return (
      <View style={ styles.background.container }>
        <Image style={ styles.background.container }
          resizeMode={ "cover" }
          source={ require("@images/png/order-detail-header.png") }
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderExtra() {
    return (
      <View style={ styles.extra.container }>

        <Text>{Lang.wouldYouExtras[this.props.lang]}</Text>

        { this.props.children }

      </View>
    );
  }

// ---------------------------------------------------

  _renderBackButton() {
    return (
      <TouchableOpacity
        style={ styles.backButton.container }
        onPress={ () => Actions.pop() }
      >
        <Icon name="arrow-left" color={ appVariables.colorWhite } size={ 18 }/>
      </TouchableOpacity>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    let totalPrice = _.reduce(this.props.list, (sum, n) => sum + n.price , 0);
    let totalExtrasItems = _.map(this.props.list, (v, k) => {
      let e = _.reduce(v.vehicle.extraItems, (sum, n) => sum + n.total ,0);
      return e;
    });
    let totalExtras = _.reduce(totalExtrasItems, (sum, n) => sum + n, 0);
    totalPrice = this.props.selectedProduct !== BUS_RENTAL ? (totalPrice * this.props.checkoutData.days) + totalExtras : totalPrice + totalExtras;
    
    return (
      <BaseContainer backgroundColor={ appVariables.colorBlueHeader }>
        { this._renderBackgroundImage() }

        <Content style={{ flex: 1, }}>
        
          <VehicleSliderDetail
            setScrollRef={ (sf) => this.props.setScrollRef(sf) }
            serviceType={ this.props.serviceType }
            list={ this.props.list }
            activeList={ this.props.activeList }
            activeListIndex={ this.props.activeListIndex }
            checkoutData={ this.props.checkoutData }
            onSwitchList={ (index) => this.props.onSwitchList(index) }
          />

          { this.props.selectedProduct !== BUS_RENTAL && this._renderExtra() }
        </Content>

        { this._renderBackButton() }

        <ListSliderFooter
          { ... this.props }
          list={ this.props.list }
          activeListIndex={ this.props.activeListIndex }
          checkoutData={ this.props.checkoutData }
          submittedData={ this.props.submittedData }
          type={ this.props.type }
          totalPrice={ totalPrice }
          onRemoveList={ (index) => this.props.onRemoveList(index) }
          onSwitchList={ (index) => this.props.onSwitchList(index) }
        />

      </BaseContainer>
    );
  }
}

const mapStateToProps = ({ productService, language }) => {
  const { selectedProduct } = productService;
  const { lang } = language;
  return { selectedProduct, lang };
}

export default connect(mapStateToProps, {})(BookedVehicleDetailContainer);