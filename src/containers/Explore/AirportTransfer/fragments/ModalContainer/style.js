export default {

	container: {
		flex: 1,
	},

	header: {
	  container: {
	    flex: -1,
	    flexDirection: "row",
	    paddingHorizontal: 20,
	    paddingTop: 15,
	    justifyContent: "flex-end",
	  },

	  button: {
	  	container: {
        paddingLeft: 10,
        paddingBottom: 7,
      },
	  },
	},

}