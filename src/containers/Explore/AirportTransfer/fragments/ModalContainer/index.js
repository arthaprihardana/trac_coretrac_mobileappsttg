import { Container } from "native-base";
import React, { Component } from "react";
import { appVariables } from "@variables";
import { 
  Modal,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
} from 'react-native';

import styles from "./style";

import BaseContainer from "@airport-transfer-fragments/BaseContainer";
import Icon from "@airport-transfer-fragments/Icon";

export default class ModalContainer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderHeader() {
    return (
      <View style={ styles.header.container }>
        <TouchableOpacity
          style={ styles.header.button.container }
          onPress={ () => this.props.onClosePress() }
        >
          <Icon name="times" size={ 20 }/>
        </TouchableOpacity>
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    if (!this.props.isVisible) {
      return null;
    }
    
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={ this.props.isVisible }
        onRequestClose={() => this.props.onClosePress()}>
        <BaseContainer backgroundColor={ appVariables.colorWhite } inverseColor>
          <KeyboardAvoidingView
            behavior={ "padding" }
            style={ styles.container }
            enabled={false}
          >

            { this._renderHeader() }

            { this.props.children }

          </KeyboardAvoidingView>
        </BaseContainer>
      </Modal>
    );
  }
};