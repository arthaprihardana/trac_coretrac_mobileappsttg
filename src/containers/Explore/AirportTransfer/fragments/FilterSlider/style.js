import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
  segment: {
    container: {
      flex: -1,
      paddingHorizontal: 40,
      paddingBottom: 40,
    },

    header: {
      container: {
        flex: -1,
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
      },
    },
  },

  slider: {
    value: {
      grouper: {
        flex: -1,
        flexDirection: "row",
        marginTop: 30,
      },

      container: {
        flex: 1,
        height: 40,
        justifyContent: "space-between",
        alignItems: "center",
      },
    },
  },

}