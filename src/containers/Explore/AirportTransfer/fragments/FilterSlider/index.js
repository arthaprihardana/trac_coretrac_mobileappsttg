/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 23:49:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 12:00:36
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Text from "@airport-transfer-fragments/Text";
import Lang from '../../../../../assets/lang';

import styles from "./style";


class FilterSlider extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderValue(label, value, isLast = false) {
    return (
      <View style={ [styles.slider.value.container, !isLast ? {borderRightWidth: 1, borderColor: "#cccccc",} : {}] }>
        <Text color={ appVariables.colorOrange }>
          { label }
        </Text>
        <Text>
          { this.props.valueModifier(value) }
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderSegmentHeader(title, onClearPress) {
    return (
      <View style={ styles.segment.header.container }>
        <Text>
          { title }
        </Text>

        <TouchableOpacity
          onPress={ () => onClearPress() }
        >
          <Text color={ "navy" }>
            {Lang.clear[this.props.lang]}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.segment.container }>

        { this._renderSegmentHeader(this.props.title, () => this.props.onClear()) }

        <MultiSlider
          isMarkersSeparated={ true }
          values={ [this.props.value.min, this.props.value.max] }
          min={ this.props.options.defaultMin }
          max={ this.props.options.defaultMax }
          step={ this.props.step }
          enableTwo
          containerStyle={{ left: 20, }}
          trackStyle={{ height: 4, }}
          selectedStyle={{ backgroundColor: "navy", }}
          unselectedStyle={{ backgroundColor: "#cccccc", }}
          sliderLength={ appMetrics.screenWidth - 120 }
          onValuesChange={ (values) => this.props.onValuesChange(values) }
        />

        <View style={ styles.slider.value.grouper }>

          { this._renderValue("min", this.props.value.min) }
          { this._renderValue("max", this.props.value.max, true) }
        
        </View>

      </View>
    );
  }

}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(FilterSlider);