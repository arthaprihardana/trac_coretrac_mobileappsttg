import React, { Component } from "react";
import {
  TextInput as RTextInput,
  View,
} from "react-native";
import { appVariables } from "@variables";

import Icon from "@airport-transfer-fragments/Icon";

import styles from "./style";

export default class TextInput extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }

// ---------------------------------------------------

  componentDidMount() {
    if (this.props.getRef) {
      this.props.getRef(this.ref);
    }
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <RTextInput
        placeholderTextColor={ appVariables.colorTabInactive }
        autoCorrect={ false }
        underlineColorAndroid={ "transparent" }
        { ... this.props }
        style={ [styles.base, this.props.style, this.props.size ? {fontSize: this.props.size} : {}] }
        autoCapitalize={ this.props.autoCapitalize ? this.props.autoCapitalize : (this.props.keyboardType != "email-address" ? this.props.autoCapitalize : "none") }
        ref={(r) => this.ref = r}
      />
    )
  }
};