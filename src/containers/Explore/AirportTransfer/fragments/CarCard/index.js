/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 01:13:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 16:28:44
 */
import React, { Component } from "react";
import { 
  View,
  Alert,
  Dimensions,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  ScrollView
} from "react-native";
import {
  appVariables,
} from "@variables";
// import MyWebView from 'react-native-webview-autoheight';
// import MyWebView from 'react-native-autoheight-webview';

import Text from "@airport-transfer-fragments/Text";
import TextHelper from "@airport-transfer-helpers/Text";
import VehicleCardMain from "@airport-transfer-fragments/VehicleCardMain";
import VehicleCardControl from "@airport-transfer-fragments/VehicleCardControl";
import MyWebView from '@airport-transfer-fragments/WebView';
import Header from "@airport-transfer-fragments/Header";

import styles from "./style";


export default class CarCard extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  constructor(props) {
    super(props);
  
    this.state = {
      addedCount: 0,
      isDetailVisible: false,
      modaltnc: false
    };
  }

// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// METHODS
// ---------------------------------------------------

  increaseList() {
    if( this.props.existingBooking < 5 ) {
      const addedCount = this.state.addedCount + 1;

      if (addedCount > this.props.stock) {
        Alert.alert(
          'Message',
          'Stock for this car is already empty',
          [
            {text: 'I Understand', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
        return false;
      }

      this.setState({
        addedCount,
      });
  
      if (this.props.onAddCarPress) {
        this.props.onAddCarPress(this.props.code, addedCount, this.props.price);
      }

    } else {
      Alert.alert(
        'Message',
        'Maximum select car is 5',
        [
          {text: 'I Understand', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
    }
  }

// ---------------------------------------------------

  decreaseList() {
    const addedCount = (this.state.addedCount > 0 ? this.state.addedCount - 1 : 0);

    this.setState({
      addedCount,
    });

    if (this.props.onAddCarPress) {
      this.props.onRemoveCarPress(this.props.code, addedCount, this.props.price);
    }
  }

// ---------------------------------------------------

  toggleDetailVisibility() {
    this.setState({
      isDetailVisible: !this.state.isDetailVisible,
    });
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderDescription() {
    const customStyle = `<style>* {max-width: 100%;} body {font-family: ${ appVariables.museo900 }; font-size: 10px; background-color: #eeeeee}</style>`;
    const htmlContent = this.props.detail;
    // return (
    //   <WebView source={{ html: htmlContent }} />
    // )
    return (
      <View>
        <MyWebView source={{html: customStyle + htmlContent}} />
        <View style={{ borderTopWidth: 0.5, borderTopColor: '#ccc', backgroundColor: '#eeeeee', padding: 10, flexDirection: 'row' }}>
          <TouchableOpacity onPress={() => this.setState({ modaltnc: !this.state.modaltnc })}><Text style={{ fontSize: 10, textDecorationLine: "underline", color: "#0000cd" }}>Syarat dan Ketentuan</Text></TouchableOpacity>
          <Text style={{ fontSize: 10 }}> Asuransi</Text>
        </View>
      </View>
    )
    // return (
    //   <View style={ styles.description.container }>
    //     <Text style={ styles.description.text }>
    //       {/* { this.props.detail } */}
    //       Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    //     </Text>
    //   </View>
    // );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.container }>

        <VehicleCardMain
          isSpecialDeal={ this.props.isSpecialDeal }
          imageUrl={ this.props.imageUrl }
          originalPrice={ this.props.originalPrice }
          stock={ this.props.stock }
          name={ this.props.name.length > 15 ? TextHelper.shorten(this.props.name, 15) : this.props.name }
          currency={ this.props.currency }
          price={ this.props.price }
          digitDelimiter={ this.props.digitDelimiter }
          features={ this.props.features }
        />

        <VehicleCardControl
          stock={ this.props.stock }
          countValue={ this.state.addedCount }
          onDecreaseCount={ () => this.decreaseList() }
          onIncreaseCount={ () => this.increaseList() }
          onDetailToggle={ () => this.toggleDetailVisibility() }
          isDetailVisible={ this.state.isDetailVisible }
        >

          { this._renderDescription() }
        
        </VehicleCardControl>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modaltnc}
          onRequestClose={() => this.setState({ modaltnc: !this.state.modaltnc }) }>
          <SafeAreaView style={{flex: 1, backgroundColor: appVariables.colorBlueHeader }}>
              <Header title={"Term And Condition"} onBackPress={ () => this.setState({modaltnc: !this.state.modaltnc}) }/>
              <View style={{ flex: 1, backgroundColor: '#FFFFFF', paddingHorizontal: 20 }}>
                <MyWebView source={{html: `<style>* {max-width: 100%;} body {font-family: ${ appVariables.museo900 }; font-size: 12px; padding-right: 40px}</style><body>${this.props.termAndCondition}</body>` }} />
              </View>
          </SafeAreaView>
        </Modal>
      
      </View>
    );
  }
};