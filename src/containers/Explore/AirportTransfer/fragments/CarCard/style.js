import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
	container: {
    flex: -1,
    backgroundColor: appVariables.colorWhite,
  },

  description: {
  	container: {
      flex: -1,
      paddingHorizontal: 20,
      paddingTop: 10,
      paddingBottom: 40,
      backgroundColor: "#eeeeee",
    },

    text: {
      fontSize: 14,
    },
  },
}