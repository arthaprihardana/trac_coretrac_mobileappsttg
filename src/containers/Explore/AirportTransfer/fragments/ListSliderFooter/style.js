import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
  outerContainer: {
    flex: -1,
    height: 80,
    backgroundColor: appVariables.colorWhite,
    shadowOffset:{  width: 0,  height: 0,  },
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowRadius: 5,
    elevation:5,
  },

  innerGrouper: {
    paddingHorizontal: 10,
  },

  container: {
    flex: -1,
    width: 140,
    padding: 10,
  },

  image: {
    container: {
      flex: 1,
      borderRadius: 5,
    },

    image: {
      flex: 1,
    },

    deleteButton: {
      container: {
        flex: -1,
        height: 24,
        width: 24,
        position: "absolute",
        right: 0,
        top: 0,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: appVariables.colorWhite,
        borderRadius: 50, 
      },
    },
  },
}