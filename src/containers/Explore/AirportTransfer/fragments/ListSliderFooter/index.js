/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-13 11:31:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 14:17:17
 */
import React, { Component } from "react";
import { Actions } from "react-native-router-flux";
import {
  View,
  TouchableOpacity,
  ScrollView,
  Image,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";

import Icon from "@airport-transfer-fragments/Icon";
import CheckoutFooter from "@airport-transfer-fragments/CheckoutFooter";

import OrderReviewModal from "@airport-transfer-modals/OrderReview";

import styles from "./style";
import Lang from '../../../../../assets/lang';

class ListSliderFooter extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFECYCLES
  // ---------------------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isDetailModalOpened: false,
    };
  }

  // ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // METHODS
  // ---------------------------------------------------

  onBookingConfirmed(summaryData) {
    this.setState({
      isDetailModalOpened: false,
    });
    let processSteps = [];
    if(this.props.type === "bus") {
      processSteps = [
        Lang.busOrder[this.props.lang],
        Lang.personalDetail[this.props.lang],
        Lang.informationCoordinator[this.props.lang],
        Lang.payment[this.props.lang],
      ]
    } else {
      if(this.props.requestStock.ProductServiceId === 'PSV0001') {
        processSteps = [
          Lang.carOrder[this.props.lang],
          Lang.personalDetail[this.props.lang],
          Lang.driverInformation[this.props.lang],
          Lang.payment[this.props.lang],
        ]
      } else {
        processSteps = [
          Lang.carOrder[this.props.lang],
          Lang.personalDetail[this.props.lang],
          Lang.payment[this.props.lang],
        ]
      }
    }
    if(this.props.isLogin) {
      setTimeout(() => {
        Actions.push("PassangerDetail", {
          navData: {
            summaryData: summaryData,
            processSteps: processSteps
          }
        })
      }, 500);
    } else {
      setTimeout(() => {
        Actions.Login({
          navData: {
            summaryData: summaryData,
            processSteps: processSteps
          },
        });
      }, 500);
    }
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderListItem(imageUrl, isActive, tripIndex, index) {
    let deleteButtonRender = null;

    if (isActive && this.props.list.filter(item => item.tripIndex === tripIndex).length > 1) {
      deleteButtonRender = <TouchableOpacity
        style={styles.image.deleteButton.container}
        onPress={() => this.props.onRemoveList(index)}
      >
        <Icon name="times-circle" solid color={"red"} size={24} />
      </TouchableOpacity>;
    }

    return (
      <TouchableOpacity
        key={index}
        style={styles.container}
        onPress={() => this.props.onSwitchList(index)}
      >
        <View style={[styles.image.container, (isActive ? { backgroundColor: "#dedede" } : {})]}>
          <Image
            resizeMode={"contain"}
            source={{ uri: imageUrl }}
            style={styles.image.image}
          />
        </View>

        {deleteButtonRender}
      </TouchableOpacity>
    );
  }

  // ---------------------------------------------------

  _renderList() {
    return (
      <View style={styles.outerContainer}>
        <ScrollView
          horizontal
          contentContainerStyle={styles.innerGrouper}
          showsHorizontalScrollIndicator={ false }
        >

          {this.props.list.map((item, index) => {
            return this._renderListItem(item.vehicle.vehicleImage, this.props.activeListIndex == index, item.tripIndex, index);
          })}

        </ScrollView>
      </View>
    );
  }

  // ---------------------------------------------------

  _renderSummary() {
    return (
      <CheckoutFooter
        type={ this.props.type == "bus" ? "BUS" : "CAR" }
        currency={this.props.checkoutData.currency}
        totalPrice={this.props.totalPrice}
        digitDelimiter={this.props.checkoutData.digitDelimiter}
        days={this.props.checkoutData.days}
        isDisabled={this.props.checkoutData.isDisabled}
        hasNext={false}
        listCount={this.props.list.length}
        onButtonPress={ () => this.setState({ isDetailModalOpened: true }) }
        isShadowDisabled
      />
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    return (
      <View>
        {this._renderList()}

        {this._renderSummary()}

        <OrderReviewModal
          submittedData={this.props.submittedData}
          list={this.props.list}
          days={this.props.checkoutData.days}
          currency={this.props.checkoutData.currency}
          digitDelimiter={this.props.checkoutData.digitDelimiter}
          onClosePress={() => this.setState({ isDetailModalOpened: false, })}
          onConfirm={(summaryData) => this.onBookingConfirmed(summaryData)}
          isVisible={this.state.isDetailModalOpened}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ authentication, stockList, language }) => {
  const { isLogin } = authentication;
  const { requestStock } = stockList;
  const { lang } = language;
  return { isLogin, requestStock, lang }
}

export default connect(mapStateToProps, {})(ListSliderFooter)