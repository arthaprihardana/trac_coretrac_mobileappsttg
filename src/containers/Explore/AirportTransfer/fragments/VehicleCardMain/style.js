import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
	container: {
    flex: -1,
    backgroundColor: appVariables.colorWhite,
  },

  image: {
  	container: {
      flex: -1,
      paddingTop: 10,
      alignItems: "center",
    },

    content:  {
      flex: -1,
      width: appMetrics.screenWidth - 40,
      height: 150,
    },
  },

  specialDealFlag: {
  	container: {
      flex: 0,
      width: 80,
      height: 80,
      position: "absolute",
    },

    content: {
      flex: -1,
      width: 80,
      top: -2,
      left: -2,
    },
  },

  detail: {
  	container: {
      flex: -1,
      paddingHorizontal: 20,
      position: "absolute",
      width: appMetrics.screenWidth,
      bottom: 0,
    },

    top: {
      container: {
        flex: -1,
        flexDirection: "row",
        borderBottomWidth: 1,
        borderColor: "#cccccc",
        alignItems: "flex-end",
        paddingBottom: 10,
      },

      left: {
        container: {
          flex: 1,
        },

        mark: {
          container: {
            flex: 1,
            flexDirection: "row",
            marginBottom: 5,
          },

          innerContainer: {
            flex: -1,
            paddingVertical: 5,
            paddingHorizontal: 13,
            borderRadius: 20, 
          },

          text: {
            flex: -1,
            color: appVariables.colorWhite,
            fontSize: 10,
          },
        },

        name: {
          flex: -1,
          fontSize: 17,
        },
      },

      right: {
        grouper: {
          flex: -1,
        },

        container: {
          flex: -1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-end",
        },

        originalValue: {
          top: 2,
          fontSize: 14,
        },

        strikeLine: {
          top: -7,
          borderTopWidth: 2,
          borderColor: "red",
        },

        value: {
          fontSize: 17,
          color: appVariables.colorOrange,
        },

        unit: {
          fontSize: 12,
          color: "#bbbbbb",
          marginLeft: 4,
        },
      },
    },
  },

  seatingArrangement: {
    container: {
      flex: -1,
      flexDirection: "row",
      alignItems: "center",
      height: 40,
    },

    seat: {
      grouper: {
        flex: 1,
        flexDirection: "row",
        paddingLeft: 10,
      },

      container: {
        flex: -1,
        height: 25,
        width: 25,
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },
}