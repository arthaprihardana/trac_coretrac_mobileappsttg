/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 14:40:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 16:44:26
 */
import React, { Component } from "react";
import {
  View,
  Image,
} from "react-native";
import { appVariables } from "@variables";
import { connect } from "react-redux";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import FeatureList from "@airport-transfer-fragments/FeatureList";

import NumberHelper from "@airport-transfer-helpers/Number";

import styles from "./style";
import { AIRPORT_TRANSFER } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class VehicleCardMain extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderDealFlag() {
    if (!this.props.isSpecialDeal) {
      return null;
    }

    return (
      <View style={ styles.specialDealFlag.container }>
        <Image
          style={ styles.specialDealFlag.content }
          resizeMode={ "contain" }
          source={ require("@images/png/flags/special-deal.png") }
        />
      </View>
    );
  }

// ---------------------------------------------------

  _renderImage() {
    return (
      <View style={ [styles.image.container, {height: (!this.props.seatingArrangement ? 250 : 290),}] }>
        <Image
          style={ styles.image.content }
          resizeMode={ "contain" }
          source={{uri: this.props.imageUrl }}
        />

      </View>
    );
  }

// ---------------------------------------------------

  _renderMark(text, backgroundColor) {
    return (
      <View style={ [styles.detail.top.left.mark.innerContainer, {backgroundColor: backgroundColor}] }>
        <Text style={ styles.detail.top.left.mark.text }>
          { text }
        </Text>
      </View>
    );
  }

// ---------------------------------------------------

  _renderSeatingArrangementSeat(isIsle, index) {
    let icon = null;
    if (!isIsle) {
      icon = <Icon type={ "trac" } name="seat" size={ 12 } color={ "#999999" }/>
    }

    return (
      <View 
        key={ index }
        style={[styles.seatingArrangement.seat.container, isIsle ? {width: 10,} : {}]}
      >
        { icon }
      </View>
    );
  }

// ---------------------------------------------------

  _renderSeatingArrangement() {
    if (!this.props.seatingArrangement) {
      return null;
    }

    const seatRenders = [];
    for (x = 0; x < (this.props.seatingArrangement[0] + this.props.seatingArrangement[1] + 1); x ++) {
      seatRenders.push(this._renderSeatingArrangementSeat(x === this.props.seatingArrangement[0], x));
    }

    return (
      <View style={ styles.seatingArrangement.container }>
        <Text>
          {Lang.SeatingArrangement[this.props.lang]}
        </Text>

        <View style={ styles.seatingArrangement.seat.grouper }>

          { seatRenders }

        </View>

      </View>
    );
  }

// ---------------------------------------------------

  _renderDetails() {
    let uom = null;
    let strikedOriginalPriceRender = null;
    if (this.props.originalPrice && this.props.originalPrice > this.props.price) {
      strikedOriginalPriceRender = <View style={ styles.detail.top.right.container }>
        <View>
          <Text style={ styles.detail.top.right.originalValue }>
            { this.props.currency } { NumberHelper.format(this.props.originalPrice, this.props.digitDelimiter) }
          </Text>

          <View style={ styles.detail.top.right.strikeLine }/>
        </View>

        <Text style={ styles.detail.top.right.unit }>
          / Package
        </Text>
      </View>;
    }

    let markRender = null;
    if (this.props.stock < 1) {
      markRender = this._renderMark("CAR ON BOOKING", "#9C1F3C");
    } else if (this.props.stock < 6) {
      // markRender = this._renderMark("HURRY! " + this.props.stock + " CAR" + (this.props.stock > 1 ? "S" : "") + " LEFT", "#2246A8");
      markRender = this._renderMark(`${Lang.hurry[this.props.lang]} ${this.props.stock} ${this.props.stock > 1 ? Lang.cars[this.props.lang] : Lang.car[this.props.lang]} ${Lang.left[this.props.lang].toUpperCase()}`, "#2246A8");
    }

    switch (this.props.selectedProduct) {
      case AIRPORT_TRANSFER:
        uom = "Trip";
        break;
      default:
        uom = "Package";
        break;
    }

    return (
      <View style={ styles.detail.container }>
        <View style={ styles.detail.top.container }>
          <View style={ styles.detail.top.left.container }>
            <View style={ styles.detail.top.left.mark.container }>
              { markRender }
            </View>

            <Text style={ styles.detail.top.left.name }>
              { this.props.name }
            </Text>
          </View>

          <View style={ styles.detail.top.right.grouper }>

            { strikedOriginalPriceRender }

            <View style={ styles.detail.top.right.container }>
              <Text style={ styles.detail.top.right.value }>
                { this.props.currency } { NumberHelper.format(this.props.price, this.props.digitDelimiter) }
              </Text>
              {/* { !this.props.isBus &&  */}
              <Text style={ styles.detail.top.right.unit }>
                / {uom}
              </Text> 
              {/* } */}
            </View>

          </View>
        </View>

        <FeatureList
          features={ this.props.features }
        />
        
        { this._renderSeatingArrangement() }
        
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View>
        { this._renderImage() }

        { this._renderDealFlag() }
      
        { this._renderDetails() }
      </View>
    );
  }
};

const mapStateToProps = ({ productService, language }) => {
  const { selectedProduct } = productService;
  const { lang } = language;
  return { selectedProduct, lang }
}
export default connect(mapStateToProps, {})(VehicleCardMain);