/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-19 22:19:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-28 09:05:43
 */
import { Container } from "native-base";
import { Actions } from "react-native-router-flux";
import { 
  View,
  TouchableOpacity,
} from "react-native";
import React, { Component } from "react";
import SafeAreaView from "react-native-safe-area-view";
import { appVariables } from "@variables";

import styles from "./style";

import StatusBar from "@airport-transfer-fragments/StatusBar";
import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import Lang from '../../../../../assets/lang';

import { connect } from "react-redux";

class BaseContainer extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderFooterItem(label, icon, key) {
    return (
      <TouchableOpacity style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 2,
      }}
        onPress={ () => {
          switch (key) {
            case "MyBooking":
              if(this.props.isLogin) {
                Actions.replace(key);
              } else {
                Actions.push("Login");
              }
              break;
            case "Profile":
              if(this.props.isLogin) {
                Actions.replace(key);
              } else {
                Actions.push("Login");
              }
              break;
            default:
              Actions.replace(key);
              break;
          }
        }}
      >
        <Icon type={ "trac" } name={ icon } color={ appVariables.colorWhite } size={ 14 }/>

        <Text color={ appVariables.colorWhite } size={ 9 } style={{marginTop: 5,}}>{ label }</Text>
      </TouchableOpacity>
    );
  }

// ---------------------------------------------------

  _renderFooter() {
    if (!this.props.hasFooter) {
      return null;
    }

    return (
      <View style={{
        flex: -1,
        flexDirection: "row",
        height: 60,
        backgroundColor: "#1C242F",
      }}>
        
        { this._renderFooterItem(Lang.explore[this.props.lang], "explore", "Home") }
        { this._renderFooterItem(Lang.myBooking[this.props.lang], "my-bookings", "MyBooking") }
        { this._renderFooterItem(Lang.myProfile[this.props.lang], "profile", "Profile") }
        { this._renderFooterItem(Lang.more[this.props.lang], "more", "More") }

      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <SafeAreaView
        forceInset={{ bottom: "never" }}
        style={[styles.container, { backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : appVariables.colorBlueHeader }] }
      >
        <Container>
          <StatusBar inverseColor={ this.props.inverseColor }/>

          { this.props.children }


        </Container>
        
        { this._renderFooter() }

      </SafeAreaView>
    );
  }
};

const mapStateToProps = ({ authentication, language }) => {
  const { isLogin } = authentication;
  const { lang } = language;
  return { isLogin, lang }
}

export default connect(mapStateToProps, {})(BaseContainer);
