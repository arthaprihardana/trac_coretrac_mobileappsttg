import {
	appVariables,
  appMetrics,
} from "@variables";

export default {
  segment: {
    container: {
      flex: -1,
      paddingHorizontal: 40,
      paddingBottom: 40,
    },

    header: {
      container: {
        flex: -1,
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 20,
      },
    },
  },

  list: {
    value: {
      grouper: {
        flex: -1,
      },
    },

  },
}