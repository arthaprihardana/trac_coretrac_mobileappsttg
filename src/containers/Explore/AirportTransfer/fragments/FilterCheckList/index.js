/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 15:59:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 12:02:11
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import {
  appVariables,
  appMetrics,
} from "@variables";
import { connect } from "react-redux";
import Text from "@airport-transfer-fragments/Text";
import RoundedCheck from "@airport-transfer-fragments/RoundedCheck";
import Lang from '../../../../../assets/lang';

import styles from "./style";


class FilterCheckList extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFECYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// RENDERS
// ---------------------------------------------------

  _renderOptions(label, valueKey, isSet, index, isLast = false) {
    return (
      <RoundedCheck
        key={ index }
        value={ isSet }
        onPress={ (value) => this.props.onValueToggle(valueKey, value) }
        style={[!isLast ? {marginBottom: 15} : {}]}
      >
        <Text>
          { label }
        </Text>
      </RoundedCheck>
    );
  }

// ---------------------------------------------------

  _renderSegmentHeader() {
    let clearButtonRender = null;
    if (this.props.onClear) {
      clearButtonRender = <TouchableOpacity
        onPress={ () => this.props.onClear() }
      >
        <Text color={ "navy" }>
          {Lang.clear[this.props.lang]}
        </Text>
      </TouchableOpacity>;
    }

    return (
      <View style={ styles.segment.header.container }>
        <Text>
          { this.props.title }
        </Text>

        { clearButtonRender }
      </View>
    );
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <View style={ styles.segment.container }>

        { this._renderSegmentHeader() }

        <View style={ styles.list.value.grouper }>

          { this.props.options.map((option, index) => {
            return this._renderOptions(option.label, option.value, this.props.values.indexOf(option.value) !== -1, index, index === (this.props.options.length - 1));
          }) }
        
        </View>
      </View>
    );
  }

}

const mapStateToProps = ({ language }) => {
  const { lang } = language;
  return { lang }
}

export default connect(mapStateToProps, {})(FilterCheckList);