import {
	appVariables,
	appMetrics,
} from "@variables";

export default {
	container: {
    flex: -1,
    flexDirection: "row",
    height: 80,
    borderTopWidth: 0,
    borderColor: "#cccccc",
    paddingHorizontal: 20,
    backgroundColor: appVariables.colorWhite,
  },

  left: {
  	container: {
      flex: 1,
      justifyContent: "center",
    },

    label: {
      color: "#999999",
      fontSize: 14,
    },

    value: {
    	container: {
        flex: -1,
        flexDirection: "row",
      },

      value: {
        fontSize: 18,
      },

      unit: {
        color: "#999999",
        fontSize: 14,
        paddingLeft: 5, 
      },
    },
  },

  right: {
  	container:  {
      flex: -1,
      justifyContent: "center",
      paddingLeft: 10,
    },

    button: {
    	container: {
        flex: -1,
        backgroundColor: appVariables.colorOrange,
        padding: 10,
        borderRadius: 7,
      },

      label: {
        color: appVariables.colorWhite,
        fontSize: 14,
      },
    },

    detailButton: {
      container: {
        flex: -1,
        height: 30,
        paddingLeft: 20,
        flexDirection: "row",
        alignItems: "flex-end",
      },

      icon: {
        marginLeft: 10,
        bottom: -2,
      },
    },
  },
}