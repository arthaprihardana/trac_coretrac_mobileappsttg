/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-08 10:44:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 16:09:25
 */
import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";

import Icon from "@airport-transfer-fragments/Icon";
import Text from "@airport-transfer-fragments/Text";
import NumberHelper from "@airport-transfer-helpers/Number";

import OrderReview from "@airport-transfer-modals/OrderReview"

import styles from "./style";
import { color } from 'color';
import appVariables from '../../../../../assets/styles/appVariables';
import { AIRPORT_TRANSFER } from "../../../../../constant";
import Lang from '../../../../../assets/lang';

class CheckoutFooter extends Component {

  // ---------------------------------------------------
  // ---------------------------------------------------
  // CONSTRUCTOR AND LIFE CYCLES
  // ---------------------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isDetailModalOpened: false,
    };
  }

  // ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state != nextState) {
      return true;
    }

    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // RENDERS
  // ---------------------------------------------------

  _renderButton() {
    if (this.props.isDetailMode) {
      return (
        <TouchableOpacity
          style={styles.right.detailButton.container}
          onPress={() => this.setState({ isDetailModalOpened: true })}>
          <Text>Detail</Text>
          <Icon name="caret-up" size={20} style={styles.right.detailButton.icon} />
        </TouchableOpacity>
      );
    }

    const type = this.props.type === "CAR" ? [Lang.car[this.props.lang], Lang.cars[this.props.lang]] : [Lang.bus[this.props.lang], Lang.buses[this.props.lang]]

    let buttonLabel = `${Lang.select[this.props.lang]} ${this.props.listCount} ${(this.props.listCount > 1 ? type[1] : type[0])}`;

    if (this.props.hasNext) {
      buttonLabel = "NEXT";
    }

    return (
      <TouchableOpacity
        style={[styles.right.button.container, (this.props.isDisabled ? { backgroundColor: "#dddddd" } : {})]}
        onPress={!this.props.isDisabled ? () => this.props.onButtonPress() : () => { }}
        activeOpacity={this.props.isDisabled ? 1 : .2}>
        <Text style={styles.right.button.label}>
          {buttonLabel}
        </Text>
      </TouchableOpacity>
    );
  }

  // ---------------------------------------------------

  _renderDetailModal() {
    if (!this.state.isDetailModalOpened) {
      return null;
    }

    return (
      <OrderReview
        { ... this.props.summaryData }
        isReviewOnly={ true }
        onClosePress={ () => this.setState({isDetailModalOpened: false}) }
        isVisible={ this.state.isDetailModalOpened }
      />
    );
  }


  // ---------------------------------------------------
  // ---------------------------------------------------
  // MAIN RENDER
  // ---------------------------------------------------

  render() {
    let borderStyle = {
      borderTopWidth: 1,
      borderColor: "#dddddd"
    };
    let uom = null

    if (!this.props.isShadowDisabled) {
      borderStyle = {
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 1
        }
      }
    }

    switch (this.props.selectedProduct) {
      case AIRPORT_TRANSFER:
        uom = "Trip";
        break;
      default:
        uom = "Package";
        break;
    }

    return (
      <View style={[styles.container, borderStyle]}>
        <View style={styles.left.container}>

          <Text style={styles.left.label}>
            {Lang.totalPayment[this.props.lang]} {this.props.days > 1 && `(${Lang.for[this.props.lang]} ${this.props.days} ${Lang.days[this.props.lang]})`}
          </Text>

          <View style={styles.left.value.container}>
            <Text style={styles.left.value.value}>
              {this.props.currency} {NumberHelper.format(this.props.isPromoValid ? this.props.totalPriceAfterDiscount : this.props.totalPrice, this.props.digitDelimiter)}
            </Text>

            <Text style={styles.left.value.unit}>
              / {uom}
            </Text>
          </View>

        </View>

        <View style={styles.right.container}>

          {this._renderButton()}

        </View>

        { this._renderDetailModal() }
      </View>
    );
  }

};

const mapStateToProps = ({ productService, discountPromo, language }) => {
  const { selectedProduct } = productService;
  const { discount, isPromoValid, totalPriceAfterDiscount } = discountPromo;
  const { lang } = language;
  return { selectedProduct, discount, isPromoValid, totalPriceAfterDiscount, lang }
}

export default connect(mapStateToProps, {})(CheckoutFooter);