export default {
  price: {
    min: 50000,
    max: 1000000,
	  step: 10000,
  },
  types: [
    {
      label: "MVP",
      value: "mvp",
    },
    {
      label: "Luxury Car",
      value: "luxury",
    },
    {
      label: "City Car",
      value: "city",
    },
  ],
};