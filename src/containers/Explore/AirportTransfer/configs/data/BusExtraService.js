export default [
	{
		key: "meal",
		title: "Food and Beverage",
		price: 150000,
		unit: "hour",
		icon: require("@images/png/extras/meal.png"),
		type: "number",
		default: 0,
	},
	{
		key: "necessity",
		title: "Travel Necessities",
		price: 50000,
		unit: "trip",
		icon: require("@images/png/extras/necessity.png"),
		type: "number",
		default: 0,
	},
	{
		key: "partner",
		title: "Our Partner",
		price: 150000,
		unit: "hour",
		icon: require("@images/png/extras/partner.png"),
		type: "number",
		default: 0,
	},
]