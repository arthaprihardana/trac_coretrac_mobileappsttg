export default [
	{
		label: "Default",
		value: "",
		mode: "DEFAULT",
	},
	// {
	// 	label: "Highest Availability",
	// 	value: "stock",
	// 	mode: "DESC"
	// },
	// {
	// 	label: "Lowest Availability",
	// 	value: "-stock",
	// 	mode: "ASC",
	// },
	{
		label: "Highest Price",
		value: "price",
		mode: "DESC",
	},
	{
		label: "Lowest Price",
		value: "-price",
		mode: "ASC",
	},
]