export default [
	{
		name: "Indonesia Rupiah",
		code: "IDR",
		value: "IDR",
		symbol: "Rp",
		digitDelimiter: ".",
		image: require("@images/png/currency-flags/IDR.png"),
	},
	{
		name: "US Dollars",
		code: "USD",
		value: "USD",
		symbol: "$",
		digitDelimiter: ",",
		image: require("@images/png/currency-flags/USD.png"),
	},
	{
		name: "EURO",
		code: "EUR",
		value: "EUR",
		symbol: "€",
		digitDelimiter: ",",
		image: require("@images/png/currency-flags/EUR.png"),
	},
	{
		name: "Singapore Dollars",
		code: "SGD",
		value: "SGD",
		symbol: "S$",
		digitDelimiter: ",",
		image: require("@images/png/currency-flags/SGD.png"),
	},
];