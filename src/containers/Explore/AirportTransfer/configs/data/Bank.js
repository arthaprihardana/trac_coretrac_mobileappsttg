export default [
	{
		name: "BCA",
		code: "bca",
		image: require("@images/png/banks/bca.png"),
	},
	{
		name: "Mandiri",
		code: "mandiri",
		image: require("@images/png/banks/mandiri.png"),
	},
	{
		name: "Permata",
		code: "permata",
		image: require("@images/png/banks/permata.png"),
	},
	{
		name: "CIMB Niaga",
		code: "cimb",
		image: require("@images/png/banks/cimb.png"),
	},
];