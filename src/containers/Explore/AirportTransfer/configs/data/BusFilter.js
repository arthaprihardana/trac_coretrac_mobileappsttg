export default {
  facilities: [
    {
      label: "Karaoke",
      value: "karaoke",
    },
    {
      label: "WC",
      value: "wc",
    },
  ],
  luxury_types: [
    {
      label: "Type A",
      value: "type_a",
    },
    {
      label: "Type B",
      value: "type_b",
    },
  ],
  big_types: [
    {
      label: "59 Seats",
      value: "59_seat",
    },
    {
      label: "48 Seats",
      value: "48_seat",
    },
    {
      label: "40 Seats",
      value: "40_seat",
    },
  ],
  medium_types: [
    {
      label: "35 Seats",
      value: "35_seat",
    },
    {
      label: "30 Seats",
      value: "30_seat",
    },
    {
      label: "29 Seats",
      value: "29_seat",
    },
  ],
  small_types: [
    {
      label: "15 Seats",
      value: "15_seat",
    },
    {
      label: "13 Seats",
      value: "13_seat",
    },
    {
      label: "11 Seats",
      value: "11_seat",
    },
  ],
  coaster_types: [
    {
      label: "23 Seats",
      value: "23_seat",
    },
    {
      label: "18 Seats",
      value: "18_seat",
    },
  ],
};