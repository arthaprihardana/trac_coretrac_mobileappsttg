export default {
	passanger: {
		icon: "passanger",
		sizeMultiplier: 1.05,
	},

	baggage: {
		icon: "baggage",
		sizeMultiplier: 1.4,
	},

	transmission: {
		icon: "transmission",
		sizeMultiplier: 1.1,
	},

	airbags: {
		icon: "airbags",
		sizeMultiplier: 1.4,
	},

	"air-conditioner": {
		icon: "air-conditioner",
		sizeMultiplier: .95,
	},

	"karaoke": {
		icon: "karaoke",
		sizeMultiplier: 1.1,
	},

	"wifi": {
		icon: "wifi",
		sizeMultiplier: .95,
	},

	"power_socket": {
		icon: "power_socket",
		sizeMultiplier: 1.4,
	},

	"seat": {
		icon: "seat",
		sizeMultiplier: 1.4,
	},

	"explore": {
		icon: "explore",
		sizeMultiplier: 1.2,
	},

	"my-bookings": {
		icon: "my-bookings",
		sizeMultiplier: 1.2,
	},

	"profile": {
		icon: "profile",
		sizeMultiplier: 1.2,
	},

	"more": {
		icon: "more",
		sizeMultiplier: .9,
	},
}