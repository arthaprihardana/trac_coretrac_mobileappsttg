import { Container, Content } from 'native-base';
import React, { Component } from 'react';
import { Modal, StatusBar, StyleSheet, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ATextWithIcon } from '../../../../components/atoms';
import { OCurrency } from '../../../../components/organisms';
import { carList, currencyList } from '../../../../utils/DummyData';
import { CarItem, Sort, ThisFooter, ThisHeader } from './thisComponents';
import { ModalCarDetail, ModalFilter } from './thisModals';
import ModalNotifyMe from './thisModals/ModalNotifyMe';

class FindCar extends Component {
  state = {
    modalCarDetail: false,
    modalFilter: false,
    modalNotify: false,
    currencyListShow: false,
    currencyList: currencyList,
    idCurrency: 'IDR',
    cars: carList,
    totalBook: 0,
    totalPrice: 0,
    selectedCar: {}
  }
  handleSubmitNotify = () => {
    this.setState({ modalNotify: false })
  }
  currencyChange = (newCurrencyList) => {
    let IDCurrency = newCurrencyList.map(cty => {
      if (cty.selected) return cty.id
    })
    this.setState({
      currencyListShow: false,
      currencyList: newCurrencyList,
      idCurrency: IDCurrency
    })
  }
  counterChange = (newValue, changedCar) => {
    let newCarsList = [...this.state.cars];
    let totalBook = 0;
    let totalPrice = 0;
    newCarsList.map(car => {
      if (car.id === changedCar.id) {
        car.numBook = newValue
      }
      totalBook = totalBook + car.numBook;
      totalPrice = totalPrice + (car.numBook * car.price);
    })

    this.setState({
      cars: newCarsList,
      totalBook: totalBook,
      totalPrice: totalPrice
    })
  }

  seeDetail = (selectedCar) => {
    this.setState({
      selectedCar: selectedCar
    }, () => {
      this.setState({ modalCarDetail: true })
    })
  }
  render() {
    let { filterWrapper, col } = styles;
    let carsList = this.state.cars.map((car, i) => {
      return <CarItem key={i} imgCar={car.img} carName={car.name} price={car.price} pessenger={car.pessenger} baggage={car.baggage} transmition={car.transmition} statusCar={car.status} statusNote={car.statusNote} onCounterChange={(value) => this.counterChange(value, car)} seeDetail={() => this.seeDetail(car)} notifyMe={() => this.setState({ modalNotify: true })} />
    })
    let footerShow = null;
    if (this.state.totalBook > 0) footerShow = <ThisFooter totalCars={this.state.totalBook} totalPrice={this.state.totalPrice} totalDays={3} selectAction={() => this.props.navigation.navigate('OrderDetail')} />
    return (
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <ThisHeader backAction={() => this.props.navigation.navigate('ExploreLanding')} title="Car Rental - Chauffer Services" />
        <View style={filterWrapper}>
          <View style={col}>
            <ATextWithIcon disabled label="Order Information" icon="icoOrderInformation" bgColor="#1A3681" align="left" />
          </View>
          <View style={col}>
            <ATextWithIcon label="Filter" icon="icoFilter" bgColor="#1A3681" align="right" onPress={() => this.setState({ modalFilter: true })} />
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', borderWidth: 1, borderColor: '#D8D8D8', position: 'relative' }}>
          <Sort label="SORT BY" value="LOWEST PRICE" />
          <Sort label="CURRENCY" value={this.state.idCurrency} noborder onPress={() => this.setState({ currencyListShow: !this.state.currencyListShow })} />
        </View>
        <Content>
          <View style={{ paddingBottom: 19 }}>
            {carsList}
          </View>
        </Content>
        {
          this.state.currencyListShow ?
            <OCurrency currencyList={this.state.currencyList} onChange={(idCurrency) => this.currencyChange(idCurrency)} />
            : null
        }
        <Modal animationType="slide" transparent={false} visible={this.state.modalCarDetail} onRequestClose={() => { }} >
          <ModalCarDetail carDetail={this.state.selectedCar} onClose={() => this.setState({ modalCarDetail: false })} />
        </Modal>
        <Modal animationType="slide" transparent={false} visible={this.state.modalFilter} onRequestClose={() => { }} >
          <ModalFilter onClose={() => this.setState({ modalFilter: false })} />
        </Modal>
        <Modal animationType="fade" transparent={true} visible={this.state.modalNotify} onRequestClose={() => { }} >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%' }}>
          </View>
        </Modal>
        <Modal animationType="slide" transparent={true} visible={this.state.modalNotify} onRequestClose={() => { }} >
          <ModalNotifyMe action={this.handleSubmitNotify} />
        </Modal>
        {footerShow}
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  filterWrapper: {
    flexDirection: 'row',
    height: 42,
    borderWidth: 1,
    borderTopColor: appVariables.colorTabInactive,
    backgroundColor: '#1A3681'
  },
  col: {
    flex: 1
  }
})

export default withNavigation(FindCar);