import { Content, Right, Textarea } from 'native-base';
import React, { Component } from 'react';
import { Image, Modal, Text, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../../assets/styles';
import { AHeader, AList } from '../../../../components/atoms';
import SvgIcon from '../../../../utils/SvgIcon';
import styles from './style';

class ModalOrderDetail extends Component {
  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={() => {}}
      >   
        <View style={[styles.modalContainer]}>
          <AHeader bgColor={appVariables.colorWhite}>
            <Right>
              <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
                <SvgIcon name="icoCloseDark" />
              </TouchableOpacity>
            </Right>
          </AHeader>
          
          <Content>
            <AList>
              <Text style={styles.mainTitle}>Order Review</Text>
            </AList>
            
            <View>
              <AList>
                <View style={styles.carList}>
                  <View style={styles.carImageContainer}>
                    <Image source={require('../../../../assets/images/png/cars/Avanza.png')} resizeMode='contain' style={styles.image} />
                  </View>
                  <View style={styles.carDescriptions}>
                    <Text style={styles.carTitle}>Toyota Avanza</Text>
                    <Text style={[styles.text, styles.serviceType]}>Chauffeur Service</Text>
                    <View style={styles.carDetails}>
                      <View style={styles.carDetailList}>
                        <View style={styles.carDetailText}><Text style={[styles.text, styles.bold, styles.first]}>4</Text><Text style={[styles.text, styles.last]}>Passenger</Text></View>
                        <View style={styles.carDetailText}><Text style={[styles.text, styles.bold, styles.first]}>M</Text><Text style={[styles.text, styles.last]}>Transmission</Text></View>
                      </View>
                      <View style={styles.carDetailList}>
                        <View style={styles.carDetailText}><Text style={[styles.text, styles.bold, styles.first]}>2</Text><Text style={[styles.text, styles.last]}>Suitcase</Text></View>
                      </View>
                    </View>
                  </View>
                </View>
              </AList>
              <AList>
                <View style={styles.carList}>
                  <View style={styles.carImageContainer}>
                    <Image source={require('../../../../assets/images/png/cars/toyota-alphard.png')} resizeMode='contain' style={styles.image} />
                  </View>
                  <View style={styles.carDescriptions}>
                    <Text style={styles.carTitle}>Toyota Alphard</Text>
                    <Text style={[styles.text, styles.serviceType]}>Chauffeur Service</Text>
                    <View style={styles.carDetails}>
                      <View style={styles.carDetailList}>
                        <View style={styles.carDetailText}><Text style={[styles.text, styles.bold, styles.first]}>4</Text><Text style={[styles.text, styles.last]}>Passenger</Text></View>
                        <View style={styles.carDetailText}><Text style={[styles.text, styles.bold, styles.first]}>M</Text><Text style={[styles.text, styles.last]}>Transmission</Text></View>
                      </View>
                      <View style={styles.carDetailList}>
                        <View style={styles.carDetailText}><Text style={[styles.text, styles.bold, styles.first]}>2</Text><Text style={[styles.text, styles.last]}>Suitcase</Text></View>
                      </View>
                    </View>
                  </View>
                </View>
              </AList>
            </View>

            <AList>
              <Text style={styles.title}>PACKAGES</Text>
              <Text style={styles.text}>12 Hours / Day</Text>
            </AList>
            
            <AList>
              <Text style={styles.title}>DATES</Text>
              <Text style={styles.text}>12 Sept 2018 -> 12 Sept 2018</Text>
              <Text style={styles.text}>Start 10 AM - End 10 PM</Text>
            </AList>

            <AList>
              <Text style={styles.title}>PICK UP LOCATION</Text>
              <Text style={styles.text}>Bali Jl. Pulau Alor No. 17, Dauh Puri, Denpasar, Bali 12345</Text>
            </AList>

            <AList>
              <Text style={styles.title}>NOTES/REQUEST</Text>
              <View style={styles.notesContainer}>
                <View style={styles.notes}>
                  <Textarea rowSpan={2} placeholder="" style={styles.textarea}/>
                </View>
                <View style={styles.notesIcon}>
                  <SvgIcon name="icoEdit" />
                </View>
              </View>
              <View>
                <Text style={styles.catatan}>*Lorem ipsum dolor sit amet sid ut parnum sadum par et um</Text>
              </View>
            </AList>

            <AList>
              <Text style={styles.title}>PAYMENT DETAILS</Text>

              <View style={styles.listCar}>
                <View><Text style={styles.text}>TOYOTA AVANZA</Text></View>
                <View style={styles.priceList}>
                  <View style={styles.priceContainer}>
                    <View style={styles.priceDesc}><Text style={styles.text}>Rp 325,000 x 3 Day(s)</Text></View>
                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 975,000</Text></View>
                  </View>
                  <View style={styles.priceContainer}>
                    <View style={styles.priceDesc}><Text style={styles.text}>Baby Car Seat: Rp 50,000 x 3 Day(s)</Text></View>
                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 150,000</Text></View>
                  </View>
                </View>
              </View>

              <View style={styles.listCar}>
                <View><Text style={styles.text}>TOYOTA INNOVA</Text></View>
                <View style={styles.priceList}>
                  <View style={styles.priceContainer}>
                    <View style={styles.priceDesc}><Text style={styles.text}>Rp 325,000 x 3 Day(s)</Text></View>
                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 975,000</Text></View>
                  </View>
                  <View style={styles.priceContainer}>
                    <View style={styles.priceDesc}><Text style={styles.text}>Baby Car Seat: Rp 50,000 x 3 Day(s)</Text></View>
                    <View style={styles.price}><Text style={[styles.text, { fontFamily: appVariables.museo700 }]}>Rp 150,000</Text></View>
                  </View>
                </View>
              </View>

              <View style={styles.totalPayment}>
                <View style={styles.tpDesc}><Text style={styles.text}>Total Payment</Text></View>
                <View style={styles.tpNum}><Text style={styles.totalPaymentNumber}>Rp. 1,025,000</Text></View>
              </View>
            </AList>
          </Content>
        </View>
      </Modal>
    )
  }
}

export default ModalOrderDetail;