import { Button, Container, Content, Text } from 'native-base';
import React, { Component } from 'react';
import { Image, StatusBar, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ASeparator } from '../../../../components/atoms';
import styles from './style';

class PaymentFinish extends Component {
  render = () => {
    return(
      <Container>
        <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
        <Content>
          <View style={styles.bigHeader}>
            <Image source={require('../../../../assets/images/png/orderFinish.png')} />
            <Text style={[styles.paddingLeftRight, styles.title]}>Congratulation Bryan Barry!</Text>
            <Text style={[styles.paddingLeftRight, styles.subTitle]}>You have succesfully rent a car with TRAC To Go</Text>
            
            <View style={styles.headerSeparator}></View>

            <View>
              <Text style={[styles.paddingLeftRight, styles.bookingNumberLabel]}>Your booking Number is</Text>
              <Text style={[styles.paddingLeftRight, styles.bookingNumber]}>A09E82093U</Text>
            </View>
          </View>

          <View style={{ padding: appVariables.padding }}>
            <Text style={styles.text}>Detail of your booking and an Email Verification is sent to</Text>
            <Text style={styles.email}>barry@gmail.com</Text>
            <Text style={styles.text}>Please verify your email to get more benefit from TRAC</Text>

            <ASeparator />
            <Text style={[styles.text, styles.dark]}>Referral Code: </Text>
            <Text style={styles.text}>Share this code to your friend. When they use is, you'll get 25% off for your next purchase.</Text>

            <ASeparator />
            <TouchableOpacity>
              <View style={styles.shareBox}>
                <Text>SHARE205</Text>
              </View>
            </TouchableOpacity>
            <ASeparator />

            <Text style={styles.text}>Copy Code</Text>

            <ASeparator />
            <Button primary block onPress={() => this.props.navigation.navigate('ExploreLanding')}><Text>CONTINUE</Text></Button>
            <ASeparator />
          </View>
        </Content>
      </Container>
    )
  }
}

export default withNavigation(PaymentFinish);