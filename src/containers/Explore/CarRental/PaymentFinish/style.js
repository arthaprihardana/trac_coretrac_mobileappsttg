import { StyleSheet } from 'react-native';
import { appVariables } from '../../../../assets/styles';

const styles = StyleSheet.create({
  paddingLeftRight: {
    paddingLeft: appVariables.padding,
    paddingRight: appVariables.padding
  },
  title: {
    fontSize: 30,
    fontFamily: appVariables.museo700,
    marginBottom: 10,
    color: appVariables.colorWhite,
    marginTop: 20,
  },
  subTitle: {
    color: appVariables.colorWhite,
    fontSize: 14,
    lineHeight: 22,
  },
  bigHeader: {
    backgroundColor: appVariables.colorBlueHeader,
    paddingTop: 30,
    paddingBottom: 30
  },
  headerSeparator: {
    borderTopWidth: .4,
    borderTopColor: appVariables.colorWhite,
    marginTop: 30,
    marginBottom: 30,
    marginLeft: appVariables.padding,
    marginRight: appVariables.padding
  },
  bookingNumberLabel: {
    color: 'rgba(255,255,255,0.4)',
    flex: 1,
    fontFamily: appVariables.museo500,
    marginBottom: 10,
  },
  bookingNumber: {
    color: appVariables.colorWhite,
    fontFamily: appVariables.museo700,
    flex: 1,
    fontSize: 18,
  },
  text: {
    color: appVariables.colorGray,
    fontSize: 14,
    fontFamily: appVariables.museo500,
    lineHeight: 20,
    marginBottom: 10,
  },
  email: {
    fontSize: 18,
    fontFamily: appVariables.museo700,
    color: appVariables.colorDark,
    marginBottom: 10
  },
  dark: {
    color: appVariables.colorDark
  },
  shareBox: {
    backgroundColor: '#F0F0F3',
    color: appVariables.colorDark,
    padding: 20,
    borderWidth: 1,
    borderColor: '#8A959E',
    alignSelf: 'flex-start',
    borderRadius: 5,
    borderStyle: 'dashed'
  },
  shareText: {
    color: appVariables.colorDark,
    fontSize: 18,
    fontFamily: appVariables.museo700
  }
});

export default styles;