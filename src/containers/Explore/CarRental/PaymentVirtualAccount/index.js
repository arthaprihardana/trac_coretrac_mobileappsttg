import { Body, Button, Card, Container, Content, Form, Header, Left, Text } from 'native-base';
import React, { Component } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ASeparator } from '../../../../components/atoms';
import { MApplyPromo, MBlueRadio, MFooterTotalPayment, MPaymentHeader } from '../../../../components/molecules';
import SvgIcon from '../../../../utils/SvgIcon';
import ModalOrderDetail from '../modalOrderDetail';
import styles from './style';

class PaymentVirtualAccount extends Component {
  state = {
    saveCardInfo: false,
    agree: false,
    selectedBank: null,
    showModalOrderDetail: false,
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <Header>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body></Body>
        </Header>

        <Content>
          <MPaymentHeader title="Virtual Account">
            <Text style={styles.headerTitle}>You can transfer from any banking channel (m-banking, SMS, or ATM)</Text>
          </MPaymentHeader>

          <Form>
            <View style={{ padding: appVariables.padding }}>
              <View style={{ flexDirection: 'column', }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <TouchableOpacity onPress={() => this.setState({ selectedBank: 'bca' })}>
                    <Card style={[styles.bankCard, { marginRight: 7, }]}>
                      <View>
                        <MBlueRadio 
                          active={this.state.selectedBank == 'bca'}
                        />
                      </View>
                      <View style={{ width: 110, justifyContent:'center', marginTop: 3, marginLeft: 5 }}>
                        <Image source={require('../../../../assets/images/png/bank/bca.png')} />
                      </View>
                    </Card>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.setState({ selectedBank: 'permata' })}>
                    <Card style={[styles.bankCard, { marginLeft: 7, }]}>
                      <View>
                        <MBlueRadio 
                          active={this.state.selectedBank == 'permata'}
                        />
                      </View>
                      <View style={{ width: 110, justifyContent:'center', marginLeft: 7 }}>
                        <Image source={require('../../../../assets/images/png/bank/permata.png')} />
                      </View>
                    </Card>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'row',}}>
                  <TouchableOpacity onPress={() => this.setState({ selectedBank: 'cimb' })}>
                    <Card style={[styles.bankCard, { marginRight: 7, }]}>
                      <View>
                        <MBlueRadio 
                          active={this.state.selectedBank == 'cimb'}
                        />
                      </View>
                      <View style={{ width: 110, justifyContent:'center', marginTop: 5, marginLeft: 5 }}>
                        <Image source={require('../../../../assets/images/png/bank/cimb.png')} />
                      </View>
                    </Card>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.setState({ selectedBank: 'mandiri' })}>
                    <Card style={[styles.bankCard, { marginLeft: 7, }]}>
                      <View>
                        <MBlueRadio 
                          active={this.state.selectedBank == 'mandiri'}
                        />
                      </View>
                      <View style={{ width: 110, justifyContent:'center', marginLeft: 5 }}>
                        <Image source={require('../../../../assets/images/png/bank/mandiri.png')} />
                      </View>
                    </Card>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={{ padding: appVariables.padding, }}>
              <MApplyPromo />

              <ASeparator />
              <MBlueRadio
                active={this.state.agree}
                onPress={() => this.setState({agree: !this.state.agree})}
                label={<Text style={styles.label} onPress={() => this.setState({agree: !this.state.agree})}>I understand the Terms and Conditions</Text>}
              />

              <ASeparator />
              <ASeparator />

              <Button primary block onPress={() => this.props.navigation.navigate('PaymentInstruction')}><Text>MAKE PAYMENT</Text></Button>
            </View>

          </Form>
        </Content>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

        <ModalOrderDetail 
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({showModalOrderDetail: false})}
        />
      </Container>
    )
  }
}

export default withNavigation(PaymentVirtualAccount);