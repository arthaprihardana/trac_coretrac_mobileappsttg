import { StyleSheet } from 'react-native';
import { appVariables } from '../../../../assets/styles';

const styles = StyleSheet.create({
  label: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
    flex: 1,
    lineHeight: 24,
  },
  headerTitle: {
    color: appVariables.colorWhite,
    fontFamily: appVariables.museo500,
    marginTop: 0,
    fontSize: 12
  },
  bankCard: { flexDirection: 'row', flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center', marginBottom: 10},
})

export default styles;