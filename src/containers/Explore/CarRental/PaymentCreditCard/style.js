import { StyleSheet } from 'react-native';
import {appVariables} from '../../../../assets/styles';

const styles = StyleSheet.create({
  label: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
    flex: 1,
    lineHeight: 24,
  }
})

export default styles;