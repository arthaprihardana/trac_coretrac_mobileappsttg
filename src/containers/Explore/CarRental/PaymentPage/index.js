import { Body, Button, Container, Content, Form, Left, Text, Right } from 'native-base';
import React, { Component } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ASeparator, AToggle } from '../../../../components/atoms';
import { MApplyPromo, MBlueRadio, MFloatingInput, MFooterTotalPayment, MPaymentHeader } from '../../../../components/molecules';
import SvgIcon from '../../../../utils/SvgIcon';
import ModalOrderDetail from '../modalOrderDetail';
import styles from './style';
import AHeader from '../../../../components/atoms/AHeader';

class PaymentPage extends Component {
  state = {
    saveCardInfo: false,
    agree: false,
    showModalOrderDetail: false,
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <AHeader bgColor={appVariables.colorBlueHeader}>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body><Text style={{color: 'white'}}>Payment</Text></Body>
          <Right></Right>
        </AHeader>

        <Content>
          <MPaymentHeader title="Credit/Debit Card">
            <Image source={require('../../../../assets/images/png/secure-credit-card.png')} />
          </MPaymentHeader>

          <View style={{ padding: 20, }}>
            <Form>
              <MFloatingInput 
                label={'Credit/Debit Card Number'}
                secure={true}
              />

              <MFloatingInput 
                label={'Name on Card'}
              />

              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flex: 1, marginRight: 5}}>
                  <MFloatingInput 
                    label={'Exp Date (MM/YY)'}
                  />
                </View>

                <View style={{flex: 1, marginLeft: 5}}>
                  <MFloatingInput 
                    label={'CVV'}
                    secure={true}
                  />
                  <Text style={{color: '#00C0F2', fontSize: 12, fontFamily: appVariables.museo500, marginTop: -15, paddingTop: 0}}>What is CVV?</Text>
                </View>
              </View>

              <ASeparator />
              <ASeparator />
              <View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <Text style={{marginTop: 5, fontSize: 16}}>Save this card information</Text>
                  <AToggle isOn={this.state.saveCardInfo} style={{alignSelf: 'flex-end'}} onPress={() => this.setState({saveCardInfo: !this.state.saveCardInfo})} />
                </View>
              </View>
              <ASeparator />
              <ASeparator />

              <MBlueRadio
                active={this.state.agree}
                onPress={() => this.setState({agree: !this.state.agree})}
                label={<Text style={styles.label} onPress={() => this.setState({agree: !this.state.agree})}>I have read and understood the Astra Trac Privacy Policy and Term Condition</Text>}
              />
              <ASeparator />

              <MApplyPromo />

              <ASeparator />
              <ASeparator />

              <Button primary block onPress={() => this.props.navigation.navigate('PaymentInstruction')}><Text>MAKE PAYMENT</Text></Button>

            </Form>
          </View>
        </Content>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

        <ModalOrderDetail 
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({showModalOrderDetail: false})}
        />

      </Container>
    )
  }
}

export default withNavigation(PaymentPage);