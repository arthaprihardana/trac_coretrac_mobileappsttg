import { StyleSheet } from 'react-native';
import appMetrics from '../../../../assets/styles/appMetrics';

const styles = StyleSheet.create({
  ButtonWrapper: { 
    flex: 1, 
    flexDirection: 'column', 
  },
  bottomButton: { width: appMetrics.windowWidth, bottom: 0, position: 'absolute', alignSelf: 'stretch', height: 60 }
});

export default styles;