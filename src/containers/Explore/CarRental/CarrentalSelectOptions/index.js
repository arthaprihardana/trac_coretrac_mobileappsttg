import { Container, Content, Footer, Left, Button, Text } from 'native-base';
import React, { Component } from 'react';
import { Modal, StatusBar, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { ABackButtonCircle, ABookButton, AHeader, ARentTitle } from '../../../../components/atoms';
import { GetShortMonth } from '../../../../utils/Helpers';
import { ModalAddress, ModalAddressDetail, ModalDateTime, ModalPackage, ModalService } from './modals';
import { connect } from 'react-redux';
import { resetCarRentalData } from '../../../../redux/ducks/CarRental';
import styles from './style';
import SvgIcon from '../../../../utils/SvgIcon';

class CarRental extends Component {
  state = {
    modalService: false,
    modalAddress: false,
    modalAddressDetail: false,
    modalPackage: false,
    modalDateTime: false,

    selectedServiceError: false,
    selectedCityError: false,
    selectedAddressError: false,
    selectedPackageError: false,
    selectedDateError: false,

    selectedServiceType: null,
    selectedCity: null,
    selectedAddress: null,
    selectedPackage: null,
    startDate: null,
    endDate: null,
    pickupTime: null
  };

  componentDidMount = () => {
    this.props.resetCarRentalData();
  }

  changeService = (value) => {
    this.setState({selectedServiceType: value});
    this.setState({selectedServiceError: false});
  }

  getMonth = (month) => {
    return GetShortMonth(month);
  }

  formatDateOrder = () => {
    const sDate = this.state.startDate;
    const eDate = this.state.endDate;

    if (!sDate || !eDate) {
      return null;
    }

    const sSDay = sDate.getDate();
    const mSDay = sDate.getMonth() + 1;
    const sEDay = eDate.getDate();
    const mEDay = eDate.getMonth() + 1;
    const sSYear = sDate.getFullYear();

    let sCombine = '';

    // if start date === end date, it means the user only order for 1 day
    if ((sDate.getDate() + sDate.getMonth() + sDate.getFullYear()) == (eDate.getDate() + eDate.getMonth() + eDate.getFullYear())) {
      // only a day
      sCombine = `${sSDay} ${this.getMonth(mSDay)}, ${sSYear} at ${this.state.pickupTime}`;
    } else {
      // multipe days
      sCombine = `${sSDay} ${this.getMonth(mSDay)} - ${sEDay} ${this.getMonth(mEDay)}, ${sSYear} at ${this.state.pickupTime}`;
    }

    return sCombine;
  }

  changePackage = (value) => {
    this.setState({
      selectedPackage: value
    })

    this.setState({selectedPackageError: false});
  }

  changeCity = (value) => {
    this.setState({
      selectedCity: value
    })

    this.setState({selectedCityError: false});
  }

  changeAddress = (value) => {
    this.setState({
      selectedAddress: value
    })

    this.setState({selectedAddressError: false});
  }

  changeDate = (startDate, endDate, pickupTime) => {
    this.setState({
      startDate,
      endDate,
      pickupTime
    });

    this.setState({selectedDateError: false});
  }

  findCarClicked = () => {
    let error = 0;
    alert(JSON.stringify(this.props.selectOptions));

    if (!this.state.selectedServiceType) { this.setState({selectedServiceError: true}); error = 1; }
    if (!this.state.selectedCity) { this.setState({selectedCityError: true}); error = 1; }
    // if (!this.state.selectedAddress) { this.setState({selectedAddressError: true}); error = 1; }
    if (!this.state.selectedPackage) { this.setState({selectedPackageError: true}); error = 1; }
    if (!this.state.startDate) { this.setState({selectedDateError: true}); error = 1; }

    if (error == 0) {
      this.props.navigation.navigate('FindCar');
    }
  }

  render() {
    const nav = this.props.navigation;

    return (
      <Container style={{backgroundColor: '#12265B'}}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={appVariables.colorBlueHeader}
        />

        <AHeader bgColor={appVariables.colorBlueHeader}>
          <Left>
            <ABackButtonCircle transparent onPress={() => nav.navigate('ExploreLanding')} />
          </Left>
        </AHeader>

        <Content>
          <ARentTitle title="Car Rental" bgColor={appVariables.colorBlueHeader} />
          <View style={styles.ButtonWrapper}>
            <ABookButton label="Type of Service" textValue={this.props.selectOptions.selectedServiceType ? String(this.props.selectOptions.selectedServiceType) : 'Select Type'} bgColor="#2246A8" onPress={() => this.setState({modalService: true})} isError={this.state.selectedServiceError} />
            <ABookButton label="City and Address" searchIcon={true} textValue={this.props.selectOptions.selectedAddress ? String(this.props.selectOptions.selectedAddress) : 'Search Your City'} bgColor="#1F419B" onPress={() => this.setState({modalAddress: true})} isError={this.state.selectedCityError} />
            <ABookButton label="Rental Package" textValue={this.props.selectOptions.selectedPackage ? String(this.props.selectOptions.selectedPackage) : 'Select Package'} bgColor="#1A3681" onPress={() => this.setState({modalPackage: true})} isError={this.state.selectedPackageError} />
            <ABookButton label="Date and Time" textValue={this.formatDateOrder() ? this.formatDateOrder() : 'Select Date & Time'} bgColor="#12265B" onPress={() => this.setState({modalDateTime: true})} isError={this.state.selectedDateError} />
          </View>
        </Content>

        <Footer style={{backgroundColor: '#12265B'}}>
          <Button iconLeft full primary style={styles.bottomButton} onPress={() => this.findCarClicked()}>
            <SvgIcon name='icoSearch' />
            <Text>Find a Car</Text>
          </Button>
        </Footer>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalService}
          onRequestClose={() => {}}
        >
          <ModalService onClose={() => this.setState({ modalService: false })} onSelectOptions={this.changeService} bgColor="#2246A8" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalAddress}
          onRequestClose={() => {}}
        >
          <ModalAddress onClose={() => this.setState({ modalAddress: false, modalAddressDetail: true })} onSelectOptions={this.changeCity} bgColor="#1F419B" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalAddressDetail}
          onRequestClose={() => {}}
        >
          <ModalAddressDetail onClose={() => this.setState({ modalAddressDetail: false })} onSelectOptions={this.changeAddress} bgColor="#1F419B" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalPackage}
          onRequestClose={() => {}}
        >
          <ModalPackage onClose={() => this.setState({ modalPackage: false })} onSelectOptions={this.changePackage} bgColor="#1A3681" />
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalDateTime}
          onRequestClose={() => {}}
        >
          <ModalDateTime onClose={() => this.setState({ modalDateTime: false })} onSelectOptions={this.changeDate} bgColor='#12265B' />
        </Modal>
      </Container>
    )
  }
};

const mapStateToProps = state => {
  return {
    selectOptions: state.CarRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    resetCarRentalData: () => {
      dispatch(resetCarRentalData());
    }
  }
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(CarRental));