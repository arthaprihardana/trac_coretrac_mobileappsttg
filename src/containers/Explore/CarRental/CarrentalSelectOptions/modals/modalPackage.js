import { Content, Radio, Right, Text } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AHeader, ARentTitle } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';
import { updateSelectedPackage } from '../../../../../redux/ducks/CarRental';
import { connect } from 'react-redux';

class ModalPackage extends Component {
  state = {
    selectedPackage: '4 Hours / Day',
  }

  toggleSelected = (value) => {
    const _this = this;
    setTimeout(function() {
      _this.props.onClose()
    }, 200);
  }

  render = () => {
    return (
      <View style={[styles.modalContainer, {backgroundColor: this.props.bgColor}]}>
        <AHeader bgColor={this.props.bgColor}>
          <Right>
            <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
              <SvgIcon name="icoClose" />
            </TouchableOpacity>
          </Right>
        </AHeader>
        <Content>
          <ARentTitle title="Rental Package" bgColor='transparent' />
          <View style={styles.content}>
            <View style={styles.inlineWrapper}>
              <Radio color={appVariables.colorWhite} selectedColor={appVariables.colorWhite} selected={this.props.selectOptions.selectedPackage == '4 Hours / Day'} onPress={() => {this.props.onSelectOptions('4 Hours / Day'); this.props.updateSelectedPackage({selectedPackage: '4 Hours / Day'}); this.toggleSelected()}} />
              <TouchableOpacity 
                onPress={() => {this.toggleSelected('4 Hours / Day'); this.props.onSelectOptions('4 Hours / Day'); this.props.updateSelectedPackage({selectedPackage: '4 Hours / Day'});}} 
                style={styles.buttonRadio}><Text style={styles.radioLabel}>4 Hours / Day</Text></TouchableOpacity>
            </View>

            <View style={styles.inlineWrapper}>
              <Radio color={appVariables.colorWhite} selectedColor={appVariables.colorWhite} selected={this.props.selectOptions.selectedPackage == '12 Hours / Day'} onPress={() => {this.props.onSelectOptions('12 Hours / Day'); this.props.updateSelectedPackage({selectedPackage: '12 Hours / Day'}); this.toggleSelected()}} />
              <TouchableOpacity 
                onPress={() => {this.toggleSelected('12 Hours / Day'); this.props.onSelectOptions('12 Hours / Day'); this.props.updateSelectedPackage({selectedPackage: '12 Hours / Day'});}}
                style={styles.buttonRadio}><Text style={styles.radioLabel}>12 Hours / Day</Text></TouchableOpacity>
            </View>
          </View>
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1, 
    margin: 0, 
    padding: 0,
    width: appMetrics.windowWidth, 
    height: appMetrics.windowHeight 
  },
  content: {
    padding: appVariables.padding,
  },
  closeButton: {
    top: 0
  },
  inlineWrapper: {
    flex: 1, 
    flexDirection: 'row',
    marginBottom: 30
  },
  buttonRadio: {
    marginLeft: 8, 
    marginTop: 5, 
    fontSize: 12,
  },
  radioLabel: {
    color: appVariables.colorWhite,
    fontSize: 20
  }
});

const mapStateToProps = state => {
  return {
    selectOptions: state.CarRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateSelectedPackage: (payload) => {
      dispatch(updateSelectedPackage(payload))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalPackage);