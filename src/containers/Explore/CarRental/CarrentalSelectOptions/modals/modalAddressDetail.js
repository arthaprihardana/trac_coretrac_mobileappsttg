import { Content, Right } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AHeader, AInputWithIcon, ASearchList } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';
import { updateSelectedAddress } from '../../../../../redux/ducks/CarRental';
import { connect } from 'react-redux';

class ModalAddressDetail extends Component {
  state = {
    addressData: [
      {city: 'Bandung Indah Plaza, Citarum, Kota Bandung, Jawa Barat'},
      {city: 'Istana BEC, Babakan Ciamis, Kota Bandung, Jawa Barat'},
      {city: 'GOR Pajajaran, Jalan Pajajaran, Pasir Kaliki, Kota Bandung, Jawa Barat'},
      {city: 'Istana BEC, Babakan Ciamis, Kota Bandung, Jawa Barat'},
    ],
    searchData: [],
    addressValue: '',
    dataType: '',
    dataAddress: '',
    dataPackage: '',
    dataDatetime: ''
  }

  componentWillMount() {
    this.setState({
      searchData: this.state.addressData
    })
  }

  searchAddress = (text) => {
    let addressData = this.state.addressData;

    let result = addressData.filter((item) => {
      if ((String(item.city).toLowerCase()).search(text) > -1) {
        return item;
      }
    });

    this.setState({
      searchData: result
    });
  }

  setAddress = (address) => {
    this.props.onClose();
    this.props.onSelectOptions(address);
    this.props.updateSelectedAddress({selectedAddress: address});
    this.setState({
      dataAddress: address
    })
  }

  render() {
    return (
      <View style={[styles.modalContainer, {backgroundColor: this.props.bgColor}]}>
        <AHeader bgColor={this.props.bgColor}>
          <Right>
            <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
              <SvgIcon name="icoClose" />
            </TouchableOpacity>
          </Right>
        </AHeader>
        <View>
          <View style={styles.content}>
            <AInputWithIcon 
              label="Search Your City"
              iconName="icoSearchBig"
              onChangeText={this.searchAddress}
              value={this.state.addressValue}
            />
          </View>
        </View>
        <Content>
          <ASearchList 
            data={this.state.searchData}
            type={'small'}
            onPress={this.setAddress}
          />
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1,
    margin: 0,
    padding: 0,
    width: appMetrics.windowWidth,
    height: appMetrics.windowHeight
  },
  listContainer: {
    paddingBottom: 40,
  },
  content: {
    padding: appVariables.padding,
  },
  closeButton: {
    top: 0
  }
});

const mapStateToProps = state => {
  return {
    selectOptions: state.CarRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateSelectedAddress: (payload) => {
      dispatch(updateSelectedAddress(payload))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalAddressDetail);