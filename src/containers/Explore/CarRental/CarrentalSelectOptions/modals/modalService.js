import { Content, Radio, Right, Text } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AHeader, ARentTitle } from '../../../../../components/atoms';
import SvgIcon from '../../../../../utils/SvgIcon';
import { updateSelectedServiceType } from '../../../../../redux/ducks/CarRental';
import { connect } from 'react-redux';

class ModalService extends Component {
  toggleSelected = (value) => {
    const _this = this;
    setTimeout(function() {
      _this.props.onClose()
    }, 200);
  }

  render = () => {
    return (
      <View style={[styles.modalContainer, {backgroundColor: this.props.bgColor}]}>
        <AHeader bgColor={this.props.bgColor}>
          <Right>
            <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
              <SvgIcon name="icoClose" />
            </TouchableOpacity>
          </Right>
        </AHeader>
        <Content>
          <ARentTitle title="Type of Service" bgColor='transparent' />
          
          <View style={styles.content}>
            <View style={styles.inlineWrapper}>
              <Radio color={appVariables.colorWhite} selectedColor={appVariables.colorWhite} selected={this.props.selectOptions.selectedServiceType == 'Chauffeur Service'} onPress={() => {this.props.onSelectOptions('Chauffeur Service'); this.props.updateSelectedServiceType({selectedServiceType: 'Chauffeur Service'}); this.toggleSelected();}} />
              <TouchableOpacity 
                onPress={() => {this.toggleSelected(); this.props.onSelectOptions('Chauffeur Service'); this.props.updateSelectedServiceType({selectedServiceType: 'Chauffeur Service'});}}
                style={styles.buttonRadio}><Text style={styles.radioLabel}>Chauffeur Services</Text></TouchableOpacity>
            </View>

            <View style={styles.inlineWrapper}>
              <Radio color={appVariables.colorWhite} selectedColor={appVariables.colorWhite} selected={this.props.selectOptions.selectedServiceType == 'Self Drive'} onPress={() => {this.props.onSelectOptions('Self Drive'); this.props.updateSelectedServiceType({selectedServiceType: 'Self Drive'}); this.toggleSelected()}} />
              <TouchableOpacity 
                onPress={() => {this.toggleSelected(); this.props.onSelectOptions('Self Drive'); this.props.updateSelectedServiceType({selectedServiceType: 'Self Drive'});}}
                style={styles.buttonRadio}><Text style={styles.radioLabel}>Self Drive</Text></TouchableOpacity>
            </View>
          </View>
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1, 
    margin: 0, 
    padding: 0,
    width: appMetrics.windowWidth, 
    height: appMetrics.windowHeight,
  },
  content: {
    padding: appVariables.padding,
  },
  closeButton: {
    top: 0
  },
  inlineWrapper: {
    flex: 1, 
    flexDirection: 'row',
    marginBottom: 30
  },
  buttonRadio: {
    marginLeft: 8, 
    marginTop: 5, 
    fontSize: 12,
  },
  radioLabel: {
    color: appVariables.colorWhite,
    fontSize: 20
  }
});

const mapStateToProps = state => {
  return {
    selectOptions: state.CarRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateSelectedServiceType: (payload) => {
      dispatch(updateSelectedServiceType(payload))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalService);