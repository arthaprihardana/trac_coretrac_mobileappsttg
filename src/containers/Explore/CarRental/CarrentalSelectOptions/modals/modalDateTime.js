import { Button, Content, Right, Text, Footer } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { AHeader, ARentTitle } from '../../../../../components/atoms';
import { MHourPicker } from '../../../../../components/molecules';
import { FormatTwoDigit } from '../../../../../utils/Helpers';
import SvgIcon from '../../../../../utils/SvgIcon';
import { updateSelectedPickupDateTime } from '../../../../../redux/ducks/CarRental';
import { connect } from 'react-redux';
import AFooterButton from '../../../../../components/atoms/AFooterButton';

class ModalDateTime extends Component {
  state = {
    serviceOne: true,
    serviceTwo: false,
    selectedTime: null,
    selectedStartDate: null,
    selectedEndDate: null,
  }

  onTimeChange = (time) => {
    this.setState({selectedTime: time});
  }

  componentWillMount() {
    const now = new Date();
    const hour = now.getHours();
    const minute = now.getMinutes();

    this.setState({
      selectedTime: `${hour}:${minute}`
    })
  }

  selectDateHandler = () => {
    if (this.state.selectedStartDate && this.state.selectedEndDate) {
      this.props.updateSelectedPickupDateTime({
        selectedStartDate: this.state.selectedStartDate, 
        selectedEndDate: this.state.selectedEndDate, 
        selectedTime: this.state.selectedTime
      });

      this.props.onSelectOptions(
        new Date(this.state.selectedStartDate), 
        new Date(this.state.selectedEndDate), 
        this.state.selectedTime
      );

      this.props.onClose();
    }
  }

  onDateChange = (date, type) => {
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  }

  render = () => {
    const minDate = new Date();

    return (
      <View style={[styles.modalContainer, {backgroundColor: this.props.bgColor}]}>
        <AHeader bgColor={this.props.bgColor}>
          <Right>
            <TouchableOpacity onPress={() => this.props.onClose()} style={styles.closeButton}>
              <SvgIcon name="icoClose" />
            </TouchableOpacity>
          </Right>
        </AHeader>
        
        <Content>
          <ARentTitle title="Book date and time" bgColor='transparent' />

          <View style={styles.content}>
            <CalendarPicker
              onDateChange={this.onDateChange}
              textStyle={{fontSize: 12, color: appVariables.colorWhite}}
              allowRangeSelection={true}
              selectedRangeStyle={{backgroundColor: '#465B95'}}
              selectedDayTextColor={appVariables.colorWhite}
              todayTextStyle={{color: appVariables.colorDark}}
              defaultTextStyles={{fontFamily: appVariables.museo500}}
              minDate={minDate}
            />

            <View style={styles.pickupTime}>
              <View style={styles.flex}>
                <Text style={{ fontSize: 16, color: appVariables.colorWhite }}>Pickup Hour</Text>
              </View>
              <View style={styles.flex}>
                <MHourPicker 
                  hourValue={FormatTwoDigit(this.state.hour)}
                  minuteValue={FormatTwoDigit(this.state.minute)}
                  onTimeChange={this.onTimeChange}
                  selectedTime={this.state.selectedTime}
                />
              </View>
            </View>

            {/* <Text style={{color: appVariables.colorWhite}}>{String(this.state.selectedStartDate)}</Text>
            <Text style={{color: appVariables.colorWhite}}>{String(this.state.selectedEndDate)}</Text> */}

            <View style={styles.holidayList}>
              <View style={styles.list}>
                <Text style={styles.holidayDate}>15 SEP 2018</Text>
                <Text style={styles.holidayDescription}>Lorem ipsum day</Text>
              </View>
              <View style={styles.list}>
                <Text style={styles.holidayDate}>16 SEP 2018</Text>
                <Text style={styles.holidayDescription}>Lorem ipsum day</Text>
              </View>
            </View>
          </View>
        </Content>
        
        <AFooterButton onPress={() => this.selectDateHandler()}>SELECT</AFooterButton>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: { 
    flex: 1, 
    margin: 0, 
    padding: 0,
    width: appMetrics.windowWidth, 
    height: appMetrics.windowHeight 
  },
  content: {
    padding: appVariables.padding,
  },
  closeButton: {
    top: 0
  },
  holidayList: {
    flexDirection: 'column',
    marginTop: 20,
  },
  list: {
    marginBottom: 14,
    flexDirection: 'column',
    flex: 1,
  },
  holidayDate: {
    fontSize: 12,
    color: appVariables.colorOrange,
    flex: 1,
  },
  holidayDescription: {
    fontSize: 14,
    color: appVariables.colorWhite,
    flex: 1
  },
  pickupTime: {
    borderTopWidth: .5,
    borderBottomWidth: .5,
    borderTopColor: appVariables.colorTabInactive,
    borderBottomColor: appVariables.colorTabInactive,
    paddingTop: 20,
    paddingBottom: 20,
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  flex: {
    flex: 1,
  }
});


const mapStateToProps = state => {
  return {
    selectOptions: state.CarRental,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    updateSelectedPickupDateTime: (payload) => {
      dispatch(updateSelectedPickupDateTime(payload))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalDateTime);