import { Body, Button, Container, Content, Footer, Left, Right } from 'native-base';
import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { AHeader } from '../../../../components/atoms';
import SvgIcon from '../../../../utils/SvgIcon';

class OrderReviewPayment extends Component {
    state = {
        paymentOption: false,
    }

    render = () => {
        let { flex } = styles;
        const { goBack } = this.props.navigation;
        
        return(
            <Container style={{backgroundColor: appVariables.colorWhite}}>
                <StatusBar barStyle="dark-content" backgroundColor={appVariables.colorWhite} />
                <AHeader backgroundColor={appVariables.colorWhite}>
                  <Left>
                    <Button transparent>
                      <TouchableOpacity onPress={() => goBack()}>
                        <SvgIcon name="backArrowBlack" />
                      </TouchableOpacity>
                    </Button>
                  </Left>
                  <Body></Body>
                  <Right></Right>
                </AHeader>
                <Content>
                    <View>
                        <View style={{ marginHorizontal: 20, marginVertical: 20, flexDirection: 'column', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 14, fontWeight: '700' }}>TOTAL PAYMENT</Text>
                            <Text style={{ fontSize: 40, fontWeight: '700', color: '#F47D00' }}>Rp 1,025,000</Text>
                        </View>
                        <View style={{ marginHorizontal: 20, borderTopColor: '#C8C8C8', borderTopWidth: 1}}>
                            <Text style={[styles.text,{ color: '#2A2E36' }, { marginVertical: 20 }]}>PAYMENT DETAIL</Text>
                            
                            <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={[styles.text, { color: '#2A2E36' }]}>TOYOTA AVANZA</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.text}>Rp 325,000 x 3 Day(s)</Text>
                                    <Text style={[styles.text, { color: '#2A2E36' }]}>Rp 975,000</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.text}>Baby Car Seat: Rp 50,000 x 3 Day(s)</Text>
                                    <Text style={[styles.text, {color: '#2A2E36' }]}>Rp 150,000</Text>
                                </View>
                                <View style={{ marginVertical: 20, borderTopColor: '#C8C8C8', borderTopWidth: 1}}/> 
                            </View>
                            <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={[styles.text, { color: '#2A2E36' }]}>TOYOTA INNOVA</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.text}>Rp 325,000 x 3 Day(s)</Text>
                                    <Text style={[styles.text, {color: '#2A2E36' }]}>Rp 975,000</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.text}>Baby Car Seat: Rp 50,000 x 3 Day(s)</Text>
                                    <Text style={[styles.text, { color: '#2A2E36' }]}>Rp 150,000</Text>
                                </View>
                                <View style={{ marginVertical: 20, borderTopColor: '#C8C8C8', borderTopWidth: 1}}/> 
                            </View>
                            <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={styles.text}>PACKAGES</Text>
                                <Text style={styles.text}>12 Hours / Day</Text>
                                <View style={{ marginVertical: 20, borderTopColor: '#C8C8C8', borderTopWidth: 1}}/> 
                            </View>
                            <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={styles.text}>DATES</Text>
                                <Text style={styles.text}>12 Sept 2018 -> 12 Sept 2018</Text>
                                <Text style={styles.text}>Start 10 AM - End 10 PM</Text>
                                <View style={{ marginHorizontal: -20, marginVertical: 20, borderTopColor: '#C8C8C8', borderTopWidth: 1}}/> 
                            </View>
                            <View style={{flexDirection: 'column', justifyContent: 'space-between'}}> 
                                <Text style={styles.text}>PICK UP LOCATION</Text>
                                <Text style={styles.text}>Bali Jl. Pulau Alor No.17, Dauh Puri,</Text>
                                <Text style={styles.text}>Denpasar, Bali 12345</Text>
                            </View>
                            <View style={{marginVertical: 20}}/>
                        </View>
                    </View>
                </Content>

                
                 {
                   (this.state.paymentOption == true) ? (
                    <View>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('PaymentCreditCard')}>
                        <View style={{height: 90, padding: 20, backgroundColor: '#1F419B', flexDirection: 'row', alignItems: 'center'}}>
                          <View style={{ width: 40, marginRight: 10, }}>
                            <SvgIcon name="icoDebit" />
                          </View>
                          <Text style={{ fontSize: 16, color: appVariables.colorWhite }}>Use credit card or debit card</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('PaymentVirtualAccount')}>
                        <View style={{height: 90, padding: 20, backgroundColor: '#1A3681', flexDirection: 'row', alignItems: 'center'}}>
                          <View style={{ width: 40, marginRight: 10, }}>
                            <SvgIcon name="icoVirtualAccount" />
                          </View>
                          <Text style={{ fontSize: 16, color: appVariables.colorWhite }}>Pay with Virtual Account</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                   ) : null
                 }

                <Footer style={styles.footer}>
                  <Button full onPress={() => this.setState({ paymentOption: !this.state.paymentOption })} style={styles.footerButton}>
                    <Text style={styles.footerButtonText}>PAYMENT OPTION</Text>
                    <SvgIcon name={ (this.state.paymentOption == true ) ? 'icoDownArrowWhite' : 'icoUpArrowWhite'} />
                  </Button>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
  footer: {height: 'auto', backgroundColor: appVariables.colorBlueHeader, justifyContent: 'flex-start', alignItems: 'center', },
  footerButtonText: { fontSize: 12, fontWeight: '700', color: '#FFFFFF', marginRight: 10, },
  text: { fontFamily: appVariables.museo500, fontSize: 14, lineHeight: 20},
  footerButton: {flex: 1, justifyContent: 'flex-start', backgroundColor: appVariables.colorBlueHeader, paddingTop: 5, paddingLeft: 20, paddingRight: 20,},
})

export default withNavigation(OrderReviewPayment);