import { Body, Button, Container, Content, Footer, Header, Left, Right, Title } from 'native-base';
import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { appVariables } from '../../../../assets/styles';
import { MBlueRadio, MFloatingInput } from '../../../../components/molecules';
import SvgIcon from '../../../../utils/SvgIcon';



const dataArray = [
    { title: "Driver #1 Information", content: "Lorem ipsum dolor sit amet" },
    { title: "Driver #2 Information", content: "Lorem ipsum dolor sit amet" },
    { title: "Driver #3 Information", content: "Lorem ipsum dolor sit amet" }
];

class PersonalDriverInfo extends Component {
    state = {
        
    }
    
    render() {
        let {modalWrapper, contentWrapper, contentWrapperA, contentWrapperB} = styles;
        let { footerContainer, left, right, totalCarsStyle, totalPriceStyle, totalDaysStyle } = styles;
    
        return(
            <Container>
                <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlue} />
                <Header style={{backgroundColor: '#21419A'}}>
                    <Left>
                        <Button transparent>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('FindCar')}>
                                <SvgIcon name="backArrow" />
                            </TouchableOpacity>
                        </Button>
                    </Left>
                    <Body>
                        <Title>
                            <Text style={{color: 'white', fontSize: 12, fontWeight: '700'}}>Personal Information</Text>
                        </Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    {/* <Accordion
                        dataArray={dataArray}
                        icon="add"
                        expandedIcon="remove"
                        iconStyle={{ color: "green" }}
                        expandedIconStyle={{ color: "red" }}
                    /> */}

                    <View style={{ marginHorizontal: 20, marginVertical: 20}}>
                        <Text style={{ fontSize: 14, fontWeight: '500', color: '#2A2E36' }}>Scan or Upload driver KTP for faster process</Text>

                         <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginVertical: 20}}>
                            <View style={{width: '50%'}}>
                                <MBlueRadio  
                                    label={<Text style={styles.label} onPress={() => this.togglePassenger()}>Indonesian Citizen</Text>}
                                    active={this.state.togglePassenger}
                                    onPress={() => this.togglePassenger()}
                                />
                                <View style={{ marginVertical:20, marginRight: 20, padding:20, borderWidth: 1, borderColor: 'black', borderStyle: 'dashed'}}>
                                    <SvgIcon name="icoScan" style={{marginLeft: 30}}/>
                                    <Button style={{marginTop:20}} primary block onPress={() => this.props.navigation.navigate('PaymentCreditCard')}>
                                        <Text style={{color: 'white'}}>PHOTO KTP</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={{width: '50%'}}>
                                <MBlueRadio  
                                    label={<Text style={styles.label} onPress={() => this.togglePassenger()}>Foreign Citizen</Text>}
                                    active={this.state.togglePassenger}
                                    onPress={() => this.togglePassenger()}
                                />
                                <View style={{ marginVertical:20, marginRight: 20, padding:20, borderWidth: 1, borderColor: 'black', borderStyle: 'dashed'}}>
                                    <SvgIcon name="icoScan" style={{marginLeft: 30}}/>
                                    <Button style={{marginTop:20}} primary block onPress={() => this.props.navigation.navigate('PaymentCreditCard')}>
                                        <Text style={{color: 'white'}}>PHOTO SIM</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>

                        <MFloatingInput 
                            label={'KTP Number'}
                        />

                        <MFloatingInput 
                            label={'Driver License Number (SIM A)'}
                        />

                        <MFloatingInput 
                            label={'Full Name (First Name, Last Name)'}
                        />

                        <MFloatingInput 
                            label={'Residential Address'}
                        />

                        <Button primary block onPress={() => this.props.navigation.navigate('PaymentCreditCard')}>
                            <Text style={{color: 'white'}}>SUBMIT</Text>
                        </Button>

                    </View>
                </Content>
                
                <Footer style={{backgroundColor: 'white', justifyContent: 'space-between' }}>
                    <View style={footerContainer}>
                        <View style={left}>
                            <Text style={totalCarsStyle}>Total price</Text>
                            <Text style={totalPriceStyle}>Rp 1,025,000</Text>
                        </View>
                        <View style={right}>
                            <View selectAction={() => this.setState({ modalReview: true })} style={{ marginHorizontal: 20, marginVertical: 20, flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={{ fontSize: 12, fontWeight: '700', color: '#2A2E36' }}>PAYMENT OPTION</Text>
                                <SvgIcon name="icoUpArrowBlack" style={{marginHorizontal: 20}}/>
                            </View>
                        </View>
                    </View>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    footerContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', width: '100%', paddingHorizontal: 20, paddingVertical: 12 },
    left: { flex: 1 },
    right: { flex: 1, paddingLeft: 12 },
    totalCarsStyle: { fontSize: 14, fontFamily: appVariables.museo500, color: '#8A959E' },
    totalPriceStyle: { fontSize: 18, fontFamily: appVariables.museo700, color: '#2A2E36' },
    totalDaysStyle: { fontSize: 12, color: '#8A959E' }
})

export default PersonalDriverInfo;