import React, { Component } from 'react';
import { Container, Left, Button, Body, Title, Content, Header, Form, Text } from 'native-base';
import { View, TouchableOpacity, Image } from 'react-native';
import { appVariables } from '../../../../assets/styles';
import MFloatingInput from '../../../../components/molecules/MFloatingInput';
import MFooterTotalPayment from '../../../../components/molecules/MFooterTotalPayment';
import ModalOrderDetail from '../modalOrderDetail';
import SvgIcon from '../../../../utils/SvgIcon';
import AFormTitle from '../../../../components/atoms/AFormTitle';
import ASeparator from '../../../../components/atoms/ASeparator';
import MBlueRadio from '../../../../components/molecules/MBlueRadio';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
// import { RNCamera } from 'react-native-camera';
import styles from './style';
import { withNavigation } from 'react-navigation';
import MCamera from '../../../../components/molecules/MCamera';
import Mask from 'react-native-mask';

class DriverInfo extends Component {
  state = {
    showModalOrderDetail: false,
    citizen: 'indonesian',
    accorActive: 1,
    showModalCamera: false,
    cameraFor: "photoKTP1",
    photoKTP1: null,
    photoSIM1: null,
    photoKTP2: null,
    photoSIM2: null,
    photoKTP3: null,
    photoSIM3: null,
  }

  openCamera = (cameraFor) => {
    this.setState({cameraFor});
    this.setState({showModalCamera: true});
  }

  closeCamera = () => {
    this.setState({showModalCamera: false});
  }

  takePicture = (pic) => {
    const b64 = JSON.parse(pic);
    let theImage = b64.base64;
    this.setState({ [ this.state.cameraFor ]: `data:image/png;base64,${theImage}`})
  }

  render = () => {
    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <Header>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body>
            <Title>Personal Information</Title>
          </Body>
        </Header>

        <Content>
          <Form>
            <Collapse 
              isCollapsed={this.state.accorActive == 1}
              onToggle={() => this.setState({accorActive: 1})}
            >
              <CollapseHeader>
                <View>
                  <View style={styles.accordionHeader} >
                    <SvgIcon name="icoIDCard" style={{ paddingRight: 18 }} />
                    <Text style={styles.accordionText}>Driver #1 Information</Text>
                  </View>
                </View>
              </CollapseHeader>
              <CollapseBody>
                <View style={styles.accordionContent}>
                  <Text style={styles.contentSubtitle}>Scan or Upload driver KTP for faster process</Text>

                  <View style={styles.contentRadioWrapper}>
                    <View style={{flex: 1}}>
                      <MBlueRadio touchText={true} 
                        label="Indonesian Citizen"
                        active={this.state.citizen === 'indonesian'}
                        onPress={() => this.setState({citizen: 'indonesian'})}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <MBlueRadio touchText={true} 
                        label="Foreign Citizen"
                        active={this.state.citizen === 'foreign'}
                        onPress={() => this.setState({citizen: 'foreign'})}
                      />
                    </View>
                  </View>

                  <View style={styles.scanWrapper}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.openCamera("photoKTP1")}>
                      <View style={styles.scanSquare}>
                        <SvgIcon name="icoScan" style={styles.scanIcon} />
                        <Button block info><Text style={styles.buttonPhotoSim}>PHOTO KTP</Text></Button>
                        <View style={styles.photoFrame}>
                          <Mask shape="rounded">
                            <Image source={{ uri: this.state.photoKTP1 }} style={styles.imageKTPSIM} resizeMode="cover" />
                          </Mask>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.openCamera("photoSIM1")}>
                      <View style={styles.scanSquare}>
                        <SvgIcon name="icoScan" style={styles.scanIcon} />
                        <Button block info><Text style={styles.buttonPhotoSim}>PHOTO SIM</Text></Button>
                        <View style={styles.photoFrame}>
                          <Mask shape="rounded">
                            <Image source={{ uri: this.state.photoSIM1 }} style={styles.imageKTPSIM} resizeMode="cover" />
                          </Mask>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <MFloatingInput 
                    label={'KTP Number'}
                  />

                  <MFloatingInput 
                    label={'Driver License Number (SIM A)'}
                  />

                  <MFloatingInput 
                    label={'Full Name (First Name, Last Name)'}
                  />

                  <MFloatingInput 
                    label={'Residential Address'}
                  />

                  <ASeparator />

                  <View>
                    <Button disabled block onPress={() => {}}><Text>SUBMIT</Text></Button>
                  </View>

                  <ASeparator />
                  <ASeparator />
                </View>
              </CollapseBody>
            </Collapse>




            <Collapse 
              isCollapsed={this.state.accorActive == 2}
              onToggle={() => this.setState({accorActive: 2})}
            >
              <CollapseHeader>
                <View>
                  <View style={styles.accordionHeader} >
                    <SvgIcon name="icoIDCard" style={{ paddingRight: 18 }} />
                    <Text style={styles.accordionText}>Driver #2 Information</Text>
                  </View>
                </View>
              </CollapseHeader>
              <CollapseBody>
                <View style={styles.accordionContent}>
                  <Text style={styles.contentSubtitle}>Scan or Upload driver KTP for faster process</Text>

                  <View style={styles.contentRadioWrapper}>
                    <View style={{flex: 1}}>
                      <MBlueRadio touchText={true} 
                        label="Indonesian Citizen"
                        active={this.state.citizen === 'indonesian'}
                        onPress={() => this.setState({citizen: 'indonesian'})}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <MBlueRadio touchText={true} 
                        label="Foreign Citizen"
                        active={this.state.citizen === 'foreign'}
                        onPress={() => this.setState({citizen: 'foreign'})}
                      />
                    </View>
                  </View>

                  <View style={styles.scanWrapper}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.openCamera("photoKTP2")}>
                      <View style={styles.scanSquare}>
                        <SvgIcon name="icoScan" style={styles.scanIcon} />
                        <Button block info><Text style={styles.buttonPhotoSim}>PHOTO KTP</Text></Button>
                        <View style={styles.photoFrame}>
                          <Mask shape="rounded">
                            <Image source={{ uri: this.state.photoKTP2 }} style={styles.imageKTPSIM} resizeMode="cover" />
                          </Mask>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.openCamera("photoSIM2")}>
                      <View style={styles.scanSquare}>
                        <SvgIcon name="icoScan" style={styles.scanIcon} />
                        <Button block info><Text style={styles.buttonPhotoSim}>PHOTO SIM</Text></Button>
                        <View style={styles.photoFrame}>
                          <Mask shape="rounded">
                            <Image source={{ uri: this.state.photoSIM2 }} style={styles.imageKTPSIM} resizeMode="cover" />
                          </Mask>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <MFloatingInput 
                    label={'KTP Number'}
                  />

                  <MFloatingInput 
                    label={'Driver License Number (SIM A)'}
                  />

                  <MFloatingInput 
                    label={'Full Name (First Name, Last Name)'}
                  />

                  <MFloatingInput 
                    label={'Residential Address'}
                  />

                  <ASeparator />

                  <View>
                    <Button disabled block onPress={() => {}}><Text>SUBMIT</Text></Button>
                  </View>

                  <ASeparator />
                  <ASeparator />
                </View>
              </CollapseBody>
            </Collapse>




            <Collapse 
              isCollapsed={this.state.accorActive == 3}
              onToggle={() => this.setState({accorActive: 3})}
            >
              <CollapseHeader>
                <View>
                  <View style={styles.accordionHeader} >
                    <SvgIcon name="icoIDCard" style={{ paddingRight: 18 }} />
                    <Text style={styles.accordionText}>Driver #3 Information</Text>
                  </View>
                </View>
              </CollapseHeader>
              <CollapseBody>
                <View style={styles.accordionContent}>
                  <Text style={styles.contentSubtitle}>Scan or Upload driver KTP for faster process</Text>

                  <View style={styles.contentRadioWrapper}>
                    <View style={{flex: 1}}>
                      <MBlueRadio touchText={true} 
                        label="Indonesian Citizen"
                        active={this.state.citizen === 'indonesian'}
                        onPress={() => this.setState({citizen: 'indonesian'})}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <MBlueRadio touchText={true} 
                        label="Foreign Citizen"
                        active={this.state.citizen === 'foreign'}
                        onPress={() => this.setState({citizen: 'foreign'})}
                      />
                    </View>
                  </View>

                  <View style={styles.scanWrapper}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.openCamera("photoKTP3")}>
                      <View style={styles.scanSquare}>
                        <SvgIcon name="icoScan" style={styles.scanIcon} />
                        <Button block info><Text style={styles.buttonPhotoSim}>PHOTO KTP</Text></Button>
                        <View style={styles.photoFrame}>
                          <Mask shape="rounded">
                            <Image source={{ uri: this.state.photoKTP3 }} style={styles.imageKTPSIM} resizeMode="cover" />
                          </Mask>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.openCamera("photoSIM3")}>
                      <View style={styles.scanSquare}>
                        <SvgIcon name="icoScan" style={styles.scanIcon} />
                        <Button block info><Text style={styles.buttonPhotoSim}>PHOTO SIM</Text></Button>
                        <View style={styles.photoFrame}>
                          <Mask shape="rounded">
                            <Image source={{ uri: this.state.photoSIM3 }} style={styles.imageKTPSIM} resizeMode="cover" />
                          </Mask>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <MFloatingInput 
                    label={'KTP Number'}
                  />

                  <MFloatingInput 
                    label={'Driver License Number (SIM A)'}
                  />

                  <MFloatingInput 
                    label={'Full Name (First Name, Last Name)'}
                  />

                  <MFloatingInput 
                    label={'Residential Address'}
                  />

                  <ASeparator />

                  <View>
                    <Button disabled block onPress={() => {}}><Text>SUBMIT</Text></Button>
                  </View>

                  <ASeparator />
                  <ASeparator />
                </View>
              </CollapseBody>
            </Collapse>


            
          </Form>
        </Content>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

        <ModalOrderDetail 
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({showModalOrderDetail: false})}
        />

        <MCamera 
          visible={this.state.showModalCamera}
          onCapture={this.takePicture}
          onClose={this.closeCamera}
        />

      </Container>
    )
  }
}

export default withNavigation(DriverInfo);