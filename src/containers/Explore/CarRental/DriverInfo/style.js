import React from 'react';
import { StyleSheet } from 'react-native';
import appVariables from '../../../../assets/styles/appVariables';

const styles = StyleSheet.create({
  accordionHeader: {flex: 1, flexDirection: 'row', width: '100%', padding: appVariables.padding, borderBottomWidth: 2, borderBottomColor: appVariables.colorBorderColor, borderTopWidth: 3, borderTopColor: appVariables.colorBorderColor},
  accordionText: {marginTop: 3},
  accordionContent: { padding: appVariables.padding },
  contentSubtitle: {fontSize: 14, marginBottom: 20 },
  contentRadioWrapper: {flexDirection: 'row', marginBottom: 20},
  scanWrapper: { flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between' },
  scanSquare: { flex: 1, alignSelf: 'stretch', borderWidth: 1, marginRight: 0, justifyContent: 'center', alignItems: 'center', borderColor: appVariables.colorBlueHeader, borderStyle: 'dashed', borderRadius: 10, padding: 20, width: 150, height: 150 },
  scanIcon: { marginBottom: 24, marginLeft: -20},
  imageKTPSIM: {  zIndex: 10, transform: [{ rotate: '90deg'}], resizeMode: 'cover', width: 150, height: 150,  },
  buttonPhotoSim: {fontSize: 13},
  photoFrame: {position:'absolute', top: 0, left: 0}
})

export default styles;