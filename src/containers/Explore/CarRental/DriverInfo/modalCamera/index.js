import React, { Component } from 'react';
import { Container } from 'native-base';
import { View, TouchableOpacity, Modal, Text } from 'react-native';
// import { appVariables } from '../../../../assets/styles';
import SvgIcon from '../../../../../utils/SvgIcon';
import { RNCamera } from 'react-native-camera';
import {appMetrics} from '../../../../../assets/styles';

class ModalCamera extends Component {
  takePicture = async function(camera) {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await camera.takePictureAsync(options);
    }
  };

  render = () => {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={() => {}}
      >   
        {/* <Container style={{backgroundColor: appVariables.colorWhite}}> */}
          <RNCamera
            type={RNCamera.Constants.Type.back}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            style={{ width: appMetrics.windowWidth, height: appMetrics.windowHeight }}
          >
            {({ camera, status }) => {
              return(
                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.takePicture(camera)}>
                  <Text>SCAN</Text>
                  {/* <View style={styles.scanSquare}>
                    <SvgIcon name="icoScan" style={styles.scanIcon} />
                    <Button block info><Text style={{fontSize: 14}}>PHOTO KTP</Text></Button>
                  </View> */}
                </TouchableOpacity>
              );
            }}
          </RNCamera>
        {/* </Container> */}
      </Modal>
    )
  }
}

export default ModalCamera;