import { Container, Content, Footer } from 'native-base';
import React, { Component } from 'react';
import { Image, Modal, ScrollView, StatusBar, StyleSheet, Text, View, ImageBackground } from 'react-native';
import { appVariables, appMetrics } from '../../../../assets/styles';
import { APricePackage } from '../../../../components/atoms';
import { MFooterSummary, MList } from '../../../../components/molecules';
import { OCarShowDetail } from '../../../../components/organisms';
import { bookedCarList } from '../../../../utils/DummyData';
import { ThisCarMainFitur, ThisExtraItems, ThisHeader, ThisSort } from './thisComponent';
import { ModalOrderReview } from './thisModals';
import AFiturIcon from '../../../../components/atoms/AFiturIcon';

class OrderDetail extends Component {
    state = {
        modalReview: false,
        cars: bookedCarList,
        activeCar: {},
        totalPrice: 0,
        showDetailCar: false
    }
    selectCarActive = () => {
        let carList = [...this.state.cars];
        let indexCarActive = carList.findIndex(car => car.isActive);
        this.setState({
            activeCar: carList[indexCarActive]
        })
    }
    handleCarSelect = (newCarsList) => {
        this.setState({
            cars: newCarsList
        }, () => this.selectCarActive())
    }
    handleExtraChange = (value, id) => {
        let carActive = this.state.activeCar;
        let carList = [...this.state.cars];
        let carActiveIndex = carList.findIndex(car => car.id === carActive.id);

        let extras = carList[carActiveIndex].extras;
        let extrasIndex = extras.findIndex(extra => extra.id === id);
        extras[extrasIndex].value = value;

        this.setState({
            cars: carList
        }, () => {
            this.calculateTotalPrice()
        })
    }
    calculateTotalPrice = () => {
        let totalPrice = 0;
        this.state.cars.map(car => {
            totalPrice = totalPrice + car.price;
            car.extras.map(extra => {
                if (typeof extra.value == 'number') {
                    totalPrice = totalPrice + (extra.value * extra.priceFitur);
                }
                if (typeof extra.value == 'boolean') {
                    if (extra.value) {
                        totalPrice = totalPrice + extra.priceFitur
                    }
                }
            })
        })
        this.setState({
            totalPrice: totalPrice
        })
    }
    componentDidMount() {
        this.selectCarActive();
        this.calculateTotalPrice();
    }
    render() {
        let { activeCar, showDetailCar } = this.state;
        let { lineStyle, carNameStyle, providerStyle, mainFiturWrapper, mainDescWrapper, sectionCarWrapper, carImgWrapper, carImgStyle, footerWrapper, extraItemsWrapper, extraItemsTitleStyle, extraItemContent, priceContainer, priceDetails, promoPriceContainer, promoPrice, activePrice} = styles;
        let extrasShow = null;
        if (activeCar.extras) {
            extrasShow = activeCar.extras.map((extra, i) => {
                let icon = 'icoBaby';
                let toggle = false;
                switch (extra.id) {
                    case 'ex1':
                        icon = 'icoAddHour';
                        break;
                    case 'ex2':
                        icon = 'icoOutTown';
                        break;
                    case 'ex3':
                        icon = 'icoOvernight';
                        break;
                    case 'ex4':
                        icon = 'icoFuelPlan';
                        toggle = true;
                        break;
                    default:
                        icon = 'icoAddHour';
                }
                if (toggle) return <ThisExtraItems key={i} iconName={icon} title={extra.label} description={extra.desc} chooseDefaultValue={extra.value} onToggleChange={(value) => this.handleExtraChange(value, extra.id)} />
                return <ThisExtraItems key={i} iconName={icon} title={extra.label} description={extra.desc} chooseDefaultValue={extra.value} onCounterChange={(value) => this.handleExtraChange(value, extra.id)} />
            })
        }

        let listDetailCar = null;
        if (activeCar.moreDetail && showDetailCar) {
            listDetailCar = activeCar.moreDetail.map((detail, i) => {
                return (
                    <View key={i} style={{marginTop: 20}}>
                        <MList label={detail.labelDetail} listItem={detail.contents} />
                    </View>
                )
            })
        }

        return (
            <Container>
                <StatusBar barStyle="light-content" backgroundColor={appVariables.colorBlueHeader} />
                <ThisHeader backAction={() => this.props.navigation.navigate('FindCar')} removeAction={() => this.props.navigation.navigate('FindCar')} />
                <Content>
                    <View>
                        <View style={mainDescWrapper}>
                          <Text style={carNameStyle}>{activeCar.name}</Text>
                          <Text style={providerStyle}>Rental Car - Chauffeur Service</Text>
                          <View style={mainFiturWrapper}>
                            <AFiturIcon label={activeCar.passanger} white iconName="icoPeople"/>
                            <AFiturIcon label={activeCar.suitcase} white iconName="icoBaggage" />
                            <AFiturIcon label={activeCar.transmition} white iconName="icoManual" />
                            <AFiturIcon white iconName="icoAirbag" check={true} />
                            <AFiturIcon white iconName="icoAC" check={true} />

                              {/* <ThisCarMainFitur value={activeCar.passanger} label="Passanger" />
                              <View style={lineStyle}></View>
                              <ThisCarMainFitur value={activeCar.suitcase} label="Suitcase" />
                              <View style={lineStyle}></View>
                              <ThisCarMainFitur value={activeCar.transmition} label="Transmission" /> */}
                          </View>
                        </View>
                        <View style={sectionCarWrapper}>
                            <View style={carImgWrapper}>
                                <Image source={activeCar.img} style={carImgStyle} />
                            </View>
                        </View>
                        <View style={priceContainer}>
                            <View style={priceDetails}>
                                {/* <View style={{ flex: 1 }}>
                                    <ThisSort label="Show Car Detail" noborder isShow={showDetailCar} onPress={()=> this.setState({showDetailCar: !showDetailCar})} />
                                </View> */}
                                <View style={promoPriceContainer}>
                                  <Text style={promoPrice}>Rp. {activeCar.price}</Text>
                                </View>

                                <View style={activePrice}>
                                  <APricePackage price={activeCar.price} />
                                </View>
                            </View>
                            {/* <View style={{ marginHorizontal: 20, marginTop: 0, marginLeft: 0 }}>
                                {listDetailCar}
                            </View> */}
                        </View>
                        <View style={extraItemsWrapper}>
                            <Text style={extraItemsTitleStyle}>WOULD YOU LIKE ANY EXTRAS ITEMS?</Text>
                            <View style={extraItemContent}>
                                {extrasShow}
                            </View>
                        </View>
                    </View>
                </Content>
                <Modal animationType="slide" transparent={false} visible={this.state.modalReview} onRequestClose={() => { }} >
                    <ModalOrderReview onPress={() => this.setState({ modalReview: false })} confirmAction={() => this.setState({modalReview: false}, ()=> this.props.navigation.navigate('OrderLogin'))}/>
                </Modal>
                <Footer style={footerWrapper}>
                    <View style={{ flex: 1 }}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                            <OCarShowDetail listCar={this.state.cars} onChange={(value) => this.handleCarSelect(value)} />
                        </ScrollView>
                        <MFooterSummary totalCars={3} totalPrice={this.state.totalPrice} totalDays={3} selectAction={() => this.setState({ modalReview: true })} />
                    </View>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    lineStyle: { width: 0.4, backgroundColor: 'white', marginHorizontal: 39 },
    carNameStyle: { fontSize: 18, fontFamily: appVariables.museo900, color: 'white', textAlign: 'center', marginBottom: 6 },
    providerStyle: { fontSize: 14, fontFamily: appVariables.museo500, color: 'white', textAlign: 'center' },
    mainFiturWrapper: { marginTop: 26, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
    mainDescWrapper: { backgroundColor: appVariables.colorBlueHeader, paddingBottom: 111 },
    sectionCarWrapper: { position: 'relative', backgroundColor: 'white', height: 76 },
    carImgWrapper: { width: 285, height: 130, position: 'absolute', bottom: 25, width: '100%' },
    carImgStyle: { flex: 1, width: undefined, height: undefined, resizeMode: 'contain' },
    footerWrapper: { backgroundColor: 'white', height: 'auto', shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 10, height: 'auto' },
    extraItemsWrapper: { backgroundColor: 'white', padding: 20 },
    extraItemsTitleStyle: { fontSize: 12, textAlign: 'center', flex: 1, fontFamily: appVariables.museo700, color: '#2A2E36' },
    extraItemContent: { marginTop: 20, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' },
    priceContainer: { justifyContent: 'center', alignItems: 'center', borderTopColor: appVariables.colorBorderColor, borderTopWidth: 1, borderBottomColor: appVariables.colorBorderColor, borderBottomWidth: 1, backgroundColor: 'white', paddingVertical: 25 },
    priceDetails: { marginHorizontal: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    promoPriceContainer: {alignItems: 'flex-end', marginRight: 10, color: appVariables.colorDark},
    promoPrice: {textDecorationLine: 'line-through', fontSize: 14,},
    activePrice: { alignItems: 'flex-start' }
})

export default OrderDetail;