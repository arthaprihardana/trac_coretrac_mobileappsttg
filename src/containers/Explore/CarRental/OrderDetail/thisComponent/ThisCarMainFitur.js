import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { appVariables } from '../../../../../assets/styles';


const ThisCarMainFitur = ({ value, label }) => {
    let {valueStyle, labelStyle} = styles;
    return (
        <View>
            <Text style={valueStyle}>{value}</Text>
            <Text style={labelStyle}>{label}</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    valueStyle: { fontSize: 20, fontFamily: appVariables.museo700, textAlign: 'center', color: 'white' },
    labelStyle: { fontSize: 12, fontFamily: appVariables.museo500, textAlign: 'center', color: 'white' }
})

ThisCarMainFitur.propTypes = {
    value: PropTypes.any, 
    label: PropTypes.string
}

export default ThisCarMainFitur;
