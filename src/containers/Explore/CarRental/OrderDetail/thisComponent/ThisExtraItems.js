import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { appMetrics, appVariables } from '../../../../../assets/styles';
import { OCounter, OToggle } from '../../../../../components/organisms';
import SvgIcon from '../../../../../utils/SvgIcon';

class ThisExtraItems extends Component {
    render() {
        let { iconName, title, description, onToggleChange, onCounterChange, chooseDefaultValue } = this.props;
        return (
            <View style={{ width: appMetrics.windowWidth / 2 - 30, shadowColor: "rgba(49,74,98,0.2)", shadowOffset: { width: 0, height: 6, }, shadowOpacity: 0.2, shadowRadius: 5, elevation: 12, backgroundColor: 'white', marginBottom: 20 }}>
                <View style={{ alignItems: 'center', paddingVertical: 30 }}>
                    <View style={{ width: 47, height: 47, marginBottom: 23 }}>
                        <SvgIcon name={iconName} />
                    </View>
                    <Text style={{ fontSize: 12, fontFamily: appVariables.museo700, color: '#2A2E36', textAlign: 'center' }}>{title.toUpperCase()}</Text>
                    <Text style={{ fontSize: 12, fontFamily: appVariables.museo500, color: '#8A959E', textAlign: 'center', marginTop: 8 }}>{description}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    {
                        onToggleChange ? 
                        <View style={{marginBottom: 16}}>
                            <OToggle defaultValue={chooseDefaultValue} onChange={onToggleChange}/>
                        </View>:
                        <OCounter positionFooter defaultValue={chooseDefaultValue} onChange={onCounterChange} />
                    }
                </View>
            </View>
        )
    }
}

ThisExtraItems.propTypes = {
    iconName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired, 
    description: PropTypes.string.isRequired,
    onToggleChange: PropTypes.func,
    onCounterChange: PropTypes.func,
    chooseDefaultValue: PropTypes.any
}

export default ThisExtraItems;