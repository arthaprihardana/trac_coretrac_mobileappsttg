import { Button, Header, Left, Right } from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import SvgIcon from '../../../../../utils/SvgIcon';

const ThisHeader = ({ backAction, removeAction }) => {
    return (
        <Header>
            <Left>
                <Button transparent>
                    <TouchableOpacity onPress={backAction}>
                        <SvgIcon name="backArrow" />
                    </TouchableOpacity>
                </Button>
            </Left>
            <Right>
                <Button transparent>
                    <TouchableOpacity onPress={removeAction}>
                        <SvgIcon name="icoRemove" />
                    </TouchableOpacity>
                </Button>
            </Right>
        </Header>
    )
}

ThisHeader.propTypes = {
    backAction: PropTypes.func,
    removeAction: PropTypes.func
}

export default ThisHeader;