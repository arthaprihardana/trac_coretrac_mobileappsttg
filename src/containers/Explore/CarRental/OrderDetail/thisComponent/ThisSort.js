import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import SvgIcon from '../../../../../utils/SvgIcon';
import { appVariables } from '../../../../../assets/styles';

class ThisSort extends Component {
    render() {
        let { label, noborder, onPress, isShow } = this.props;
        let { sortWrapper, borderRight, labelStyle, iconWrapper, showStyle } = styles;
        return (
            <TouchableOpacity onPress={onPress}>
                <View style={[sortWrapper, noborder ? null : borderRight]}>
                    <Text style={labelStyle}>{label.toUpperCase()}</Text>
                    <View style={iconWrapper}>
                        <View style={isShow ? showStyle : null}>
                            <SvgIcon name="icoSort" />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    sortWrapper: { flexDirection: 'row', alignItems: 'center'},
    borderRight: { borderRightColor: '#D8D8D8', borderRightWidth: 1 },
    labelStyle: { fontSize: 12, fontFamily: appVariables.museo700 },
    iconWrapper: { marginLeft: 13 },
    showStyle: {transform: [{ rotate: '180deg'}]}
})

ThisSort.propTypes = {
    label: PropTypes.string, 
    isShow: PropTypes.bool, 
    noborder: PropTypes.bool, 
    onPress: PropTypes.func
}

export default ThisSort;