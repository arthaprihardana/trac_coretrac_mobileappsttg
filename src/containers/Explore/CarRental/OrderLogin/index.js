import { Body, Button, Container, Content, Header, Left, Text, Title } from 'native-base';
import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import appVariables from '../../../../assets/styles/appVariables';
import { MFooterTotalPayment } from '../../../../components/molecules';
import { OLogin } from '../../../../components/organisms';
import SvgIcon from '../../../../utils/SvgIcon';
import ModalOrderDetail from '../modalOrderDetail';

class OrderLogin extends Component {
  state = {
    showModalOrderDetail: false,
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{ backgroundColor: appVariables.colorWhite }}>
        <Header>
          <Left style={{flex: 1}}>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrow" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body style={{flex: 7}}>
            <Title>Personal Detail</Title>
          </Body>
        </Header>

        <Content padder>
          <Text style={{textAlign: 'center', fontSize: 24, fontFamily: appVariables.museo700, color: '#2A2E36', marginBottom: 10}}>Login</Text>
          <Text style={{textAlign: 'center', fontSize: 14, fontFamily: appVariables.museo500, color: '#8A959E', marginBottom: 20}}>Book Faster and easier by Login or Register</Text>
          <OLogin onLogin={()=> this.props.navigation.navigate('PaymentInfo')} />
        </Content>

        <MFooterTotalPayment
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({ showModalOrderDetail: true })}
        />

        <ModalOrderDetail
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({ showModalOrderDetail: false })}
        />

      </Container>
    )
  }
}

export default OrderLogin;