import { StyleSheet } from 'react-native';
import { appVariables } from '../../../../assets/styles';

const styles = StyleSheet.create({
  inlineWrapper: {
    padding: 0,
    flexDirection: 'row',
    flex: 1,
    marginTop: 20,
    marginBottom: 0
  },
  agreeText: {
    fontSize: 12,
    marginLeft: 10,
    marginTop: 8
  },
  footerWrapper: { 
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 20, 
    paddingVertical: 12,
  },
  footerLabelTop: { 
    fontSize: 14, 
    fontFamily: appVariables.museo500, 
    color: '#8A959E', 
    marginBottom: 4 
  },
  footerLabelPrice: { fontSize: 18, fontFamily: appVariables.museo500, color: '#2A2E36' },
  footerLabelDay: { fontSize: 12, fontFamily: appVariables.museo500, color: '#8A959E' },
  footerLabelDetail: { flex: 1, justifyContent: 'center', alignItems: 'flex-end'},
  footerIcoSort: {marginTop: 6, marginLeft: 6}
})

export default styles;