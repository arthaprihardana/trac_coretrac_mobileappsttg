import { StyleSheet } from 'react-native';
import appVariables from '../../../../assets/styles/appVariables';

const styles = StyleSheet.create({
  title: { marginBottom: 10, color: appVariables.colorDark, fontFamily: appVariables.museo700, fontSize: 18, flex: 1},
  subTitle: { fontFamily: appVariables.museo500, color: appVariables.colorGray },
  borderSeparator: { borderTopColor: appVariables.colorGray, flex: 1, borderTopWidth: .5, marginTop: 30, marginBottom: 30, },
  text: { fontSize: 14, marginBottom: 5, color: appVariables.colorDark, flex: 1},
  footer: { backgroundColor: appVariables.colorWhite, borderWidth: 0, borderColor: 'transparent'}
})

export default styles;