import { Body, Button, Container, Content, Footer, Left, Right, Text } from 'native-base';
import React, { Component } from 'react';
import { Image, StatusBar, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { appVariables } from '../../../../assets/styles';
import { AHeader, ASeparator } from '../../../../components/atoms';
import { MFooterTotalPayment } from '../../../../components/molecules';
import SvgIcon from '../../../../utils/SvgIcon';
import ModalOrderDetail from '../modalOrderDetail';
import styles from './style';

class PaymentInstruction extends Component {
  state = {
    showModalOrderDetail: false,
  }

  render = () => {
    const { goBack } = this.props.navigation;

    return (
      <Container style={{backgroundColor: appVariables.colorWhite}}>
        <StatusBar barStyle="dark-content" backgroundColor={appVariables.colorWhite} />
        <AHeader backgroundColor={appVariables.colorWhite}>
          <Left>
            <Button transparent>
              <TouchableOpacity onPress={() => goBack()}>
                <SvgIcon name="backArrowBlack" />
              </TouchableOpacity>
            </Button>
          </Left>
          <Body></Body>
          <Right></Right>
        </AHeader>

        <Content padder>
          <Text style={styles.title}>Payment Instruction</Text>
          <Text style={styles.subTitle}>Please complete your payment before 18:00 (2 hours 58 minutes)</Text>

          <View style={styles.borderSeparator}></View>

          <Image source={require('../../../../assets/images/png/bank/bcaBig.png')} style={{ marginBottom: 40 }} />

          <Text style={styles.title}>Bank Central Asia (BCA)</Text>
          <Text style={styles.text}>Account Number: 5830485830</Text>
          <Text style={styles.text}>Account Holder Name : Trac Astra Rental</Text>

          <ASeparator />
          <ASeparator />
        </Content>

        <Footer style={styles.footer}>
          <Button primary block onPress={() => this.props.navigation.navigate('BusRentalPaymentFinish')}><Text>UPLOAD PAYMENT PROOF</Text></Button>
        </Footer>

        <MFooterTotalPayment 
          totalPrice={1025000}
          totalDay={3}
          onPress={() => this.setState({showModalOrderDetail: true})}
        />

        <ModalOrderDetail 
          visible={this.state.showModalOrderDetail}
          onClose={() => this.setState({showModalOrderDetail: false})}
        />
      </Container>
    )
  }
}

export default withNavigation(PaymentInstruction);