/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 22:31:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-20 22:32:34
 */
import { POST_CONTACT_US, POST_CONTACT_US_SUCCESS, POST_CONTACT_US_FAILURE } from "./Types";

export const postContactUs = params => ({
    type: POST_CONTACT_US,
    payload: params
});

export const postContactUsSuccess = response => ({
    type: POST_CONTACT_US_SUCCESS,
    payload: response
});

export const postContactUsFailure = error => ({
    type: POST_CONTACT_US_FAILURE,
    payload: error
});