/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-08 11:46:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-15 15:02:40
 */
import { TEMP_FORM, TEMP_PERSONAL_DETAIL, TEMP_DRIVER_DETAIL, TEMP_COORDINATOR_DETAIL } from "./Types";

export const setTempForm = values => ({
    type: TEMP_FORM,
    payload: values
});

export const setTempPersonalDetail = values => ({
    type: TEMP_PERSONAL_DETAIL,
    payload: values
});

export const setTempDriverDetail = values => ({
    type: TEMP_DRIVER_DETAIL,
    payload: values
});

export const setTempCoordinatorDetail = values => ({
    type: TEMP_COORDINATOR_DETAIL,
    payload: values
});