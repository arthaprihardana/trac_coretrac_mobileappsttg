/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 23:21:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-05 08:45:54
 */
import { GET_CREDIT_CARD, GET_CREDIT_CARD_SUCCESS, GET_CREDIT_CARD_FAILURE, SAVE_CREDIT_CARD, SAVE_CREDIT_CARD_SUCCESS, SAVE_CREDIT_CARD_FAILURE, UPDATE_CREDIT_CARD, UPDATE_CREDIT_CARD_SUCCESS, UPDATE_CREDIT_CARD_FAILURE, DELETE_CREDIT_CARD, DELETE_CREDIT_CARD_SUCCESS, DELETE_CREDIT_CARD_FAILURE, SET_PRIMARY_CREDIT_CARD, SET_PRIMARY_CREDIT_CARD_SUCCESS, SET_PRIMARY_CREDIT_CARD_FAILURE } from "./Types";

export const getCreditCard = params => ({
    type: GET_CREDIT_CARD,
    payload: params
});

export const getCreditCardSuccess = response => ({
    type: GET_CREDIT_CARD_SUCCESS,
    payload: response
});

export const getCreditCardFailure = error => ({
    type: GET_CREDIT_CARD_FAILURE,
    payload: error
});

export const saveCreditCard = params => ({
    type: SAVE_CREDIT_CARD,
    payload: params
});

export const saveCreditCardSuccess = response => ({
    type: SAVE_CREDIT_CARD_SUCCESS,
    payload: response
});

export const saveCreditCardFailure = error => ({
    type: SAVE_CREDIT_CARD_FAILURE,
    payload: error
});

export const updateCreditCard = params => ({
    type: UPDATE_CREDIT_CARD,
    payload: params
});

export const updateCreditCardSuccess = response => ({
    type: UPDATE_CREDIT_CARD_SUCCESS,
    payload: response
});

export const updateCreditCardFailure = error => ({
    type: UPDATE_CREDIT_CARD_FAILURE,
    payload: error
});

export const deleteCreditCard = params => ({
    type: DELETE_CREDIT_CARD,
    payload: params
});

export const deleteCreditCardSuccess = response => ({
    type: DELETE_CREDIT_CARD_SUCCESS,
    payload: response
});

export const deleteCrediCardFailure = error => ({
    type: DELETE_CREDIT_CARD_FAILURE,
    payload: error
});

export const setPrimaryCreditCard = params => ({
    type: SET_PRIMARY_CREDIT_CARD,
    payload: params
});

export const setPrimaryCreditCardSuccess = response => ({
    type: SET_PRIMARY_CREDIT_CARD_SUCCESS,
    payload: response
});

export const setPrimaryCreditCardFailure = error => ({
    type: SET_PRIMARY_CREDIT_CARD_FAILURE,
    payload: error
});