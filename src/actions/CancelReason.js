/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-01 09:23:44 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-04-01 09:23:44 
 */
import { GET_CANCEL_REASON, GET_CANCEL_REASON_SUCCESS, GET_CANCEL_REASON_FAILURE } from "./Types";

export const getCancelReason = params => ({
    type: GET_CANCEL_REASON,
    payload: params
});

export const getCancelReasonSuccess = response => ({
    type: GET_CANCEL_REASON_SUCCESS,
    payload: response
});

export const getCancelReasonFailure = error => ({
    type: GET_CANCEL_REASON_FAILURE,
    payload: error
});