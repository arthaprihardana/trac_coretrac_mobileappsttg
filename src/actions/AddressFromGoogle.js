/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 17:14:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-03 11:11:17
 */
import { GET_ADDRESS_GM, GET_ADDRESS_GM_SUCCESS, GET_ADDRESS_GM_FAILURE, SEARCH_KEY_GM, GET_DISTANCE_MATRIX_GM, GET_DISTANCE_MATRIX_GM_SUCCESS, GET_DISTANCE_MATRIX_GM_FAILURE, GET_PLACE_DETAIL_GM, GET_PLACE_DETAIL_GM_SUCCESS, GET_PLACE_DETAIL_GM_FAILURE, GET_FIND_GEOMETRY_GM, GET_FIND_GEOMETRY_GM_SUCCESS, GET_FIND_GEOMETRY_GM_FAILURE, GET_PLACE_DETAIL_GM_FOR_TO_LOCATION_BUS, GET_DISTANCE_MATRIX_GM_FOR_BUS, GET_DISTANCE_MATRIX_GM_FOR_BUS_SUCCESS, GET_DISTANCE_MATRIX_GM_FOR_BUS_FAILURE } from "./Types";

export const searchKeyToGoogle = params => ({
    type: SEARCH_KEY_GM,
    payload: params
});

export const getAddressFromGoogle = params => ({
    type: GET_ADDRESS_GM,
    payload: params
});

export const getAddressFromGoogleSuccess = response => ({
    type: GET_ADDRESS_GM_SUCCESS,
    payload: response
});

export const getAddressFromGoogleFailure = error => ({
    type: GET_ADDRESS_GM_FAILURE,
    payload: error
});

export const getDistanceMatrixFromGoogle = params => ({
    type: GET_DISTANCE_MATRIX_GM,
    payload: params
});

export const getDistanceMatrixFromGoogleSuccess = response => ({
    type: GET_DISTANCE_MATRIX_GM_SUCCESS,
    payload: response
});

export const getDistanceMatrixFromGoogleFailure = error => ({
    type: GET_DISTANCE_MATRIX_GM_FAILURE,
    payload: error
});

export const getDistanceMatrixFromGoogleForBus = params => ({
    type: GET_DISTANCE_MATRIX_GM_FOR_BUS,
    payload: params
});

export const getDistanceMatrixFromGoogleForBusSuccess = response => ({
    type: GET_DISTANCE_MATRIX_GM_FOR_BUS_SUCCESS,
    payload: response
});

export const getDistanceMatrixFromGoogleForBusFailure = error => ({
    type: GET_DISTANCE_MATRIX_GM_FOR_BUS_FAILURE,
    payload: error
});

export const getPlaceDetailFromGoogle = params => ({
    type: GET_PLACE_DETAIL_GM,
    payload: params
});

export const getPlaceDetailFromGoogleSuccess = response => ({
    type: GET_PLACE_DETAIL_GM_SUCCESS,
    payload: response
});

export const getPlaceDetailFromGoogleFailure = error => ({
    type: GET_PLACE_DETAIL_GM_FAILURE,
    payload: error
});

export const getPlaceDetailForToLocationBus = value => ({
    type: GET_PLACE_DETAIL_GM_FOR_TO_LOCATION_BUS,
    payload: value
});

export const getFindGeometryFromGoogle = params => ({
    type: GET_FIND_GEOMETRY_GM,
    payload: params
});

export const getFindGeometryFromGoogleSuccess = response => ({
    type: GET_FIND_GEOMETRY_GM_SUCCESS,
    payload: response
});

export const getFindGeometryFromGoogleFailure = error => ({
    type: GET_FIND_GEOMETRY_GM_FAILURE,
    payload: error
});