/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 02:12:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-26 21:32:03
 */
import { GET_PRICE, GET_PRICE_SUCCESS, GET_PRICE_FAILURE, GET_PRICE_FOR_BUS, GET_PRICE_FOR_BUS_SUCCESS, GET_PRICE_FOR_BUS_FAILURE } from "./Types";

export const getPrice = params => ({
    type: GET_PRICE,
    payload: params
});

export const getPriceSuccess = response => ({
    type: GET_PRICE_SUCCESS,
    payload: response
});

export const getPriceFailure = error => ({
    type: GET_PRICE_FAILURE,
    payload: error
});

export const getPriceForBus = params => ({
    type: GET_PRICE_FOR_BUS,
    payload: params
});

export const getPriceForBusSuccess = response => ({
    type: GET_PRICE_FOR_BUS_SUCCESS,
    payload: response
});

export const getPriceForBusFailure = error => ({
    type: GET_PRICE_FOR_BUS_FAILURE,
    payload: error
});