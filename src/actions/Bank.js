/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 15:07:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-01 23:11:39
 */
import { GET_MASTER_BANK, GET_MASTER_BANK_SUCCESS, GET_MASTER_BANK_FAILURE } from "./Types";

export const getBank = params => ({
    type: GET_MASTER_BANK,
    payload: params
});

export const getBankSuccess = response => ({
    type: GET_MASTER_BANK_SUCCESS,
    payload: response
});

export const getBankFailure = error => ({
    type: GET_MASTER_BANK_FAILURE,
    payload: error
});

// export const getAllBank = () => ({
//     type: GET_ALL_BANK
// });

// export const getAllBankSuccess = response => ({
//     type: GET_ALL_BANK_SUCCESS,
//     payload: response
// });

// export const getAllBankFailure = error => ({
//     type: GET_ALL_BANK_FAILURE,
//     payload: error
// });