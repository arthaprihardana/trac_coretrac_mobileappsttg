/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 14:42:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-13 14:07:39
 */
import { GET_EXTRAS, GET_EXTRAS_SUCCESS, GET_EXTRAS_FAILURE, TEMP_EXTRAS } from "./Types";

export const getExtras = params => ({
    type: GET_EXTRAS,
    payload: params
});

export const getExtrasSuccess = response => ({
    type: GET_EXTRAS_SUCCESS,
    payload: response
});

export const getExtrasFailure = error => ({
    type: GET_EXTRAS_FAILURE,
    payload: error
});

export const setTempExtras = value => ({
    type: TEMP_EXTRAS,
    payload: value
});