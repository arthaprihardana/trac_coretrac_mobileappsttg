/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 16:16:12 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-31 16:16:12 
 */
import { GET_MY_HISTORY, GET_MY_HISTORY_SUCCESS, GET_MY_HISTORY_FAILURE } from "./Types";

export const getMyHistory = params => ({
    type: GET_MY_HISTORY,
    payload: params
});

export const getMyHistorySuccess = response => ({
    type: GET_MY_HISTORY_SUCCESS,
    payload: response
});

export const getMyHistoryFailure = error => ({
    type: GET_MY_HISTORY_FAILURE,
    payload: error
});