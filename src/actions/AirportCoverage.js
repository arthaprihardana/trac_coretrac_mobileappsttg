/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-19 10:16:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-19 10:27:17
 */
import { GET_AIRPORT_COVERAGE, GET_AIRPORT_COVERAGE_SUCCESS, GET_AIRPORT_COVERAGE_FAILURE, GET_MASTER_AIRPORT, GET_MASTER_AIRPORT_SUCCESS, GET_MASTER_AIRPORT_FAILURE } from "./Types";

export const getMasterAirport = () => ({
    type: GET_MASTER_AIRPORT
});

export const getMasterAirportSuccess = response => ({
    type: GET_MASTER_AIRPORT_SUCCESS,
    payload: response
});

export const getMasterAirportFailure = error => ({
    type: GET_MASTER_AIRPORT_FAILURE,
    payload: error
});

export const getAirportCoverage = params => ({
    type: GET_AIRPORT_COVERAGE,
    payload: params
});

export const getAirportCoverageSuccess = response => ({
    type: GET_AIRPORT_COVERAGE_SUCCESS,
    payload: response
});

export const getAirportCoverageFailure = error => ({
    type: GET_AIRPORT_COVERAGE_FAILURE,
    payload: error
});