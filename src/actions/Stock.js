/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 15:18:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-07 14:27:51
 */
import { GET_STOCK_LIST, GET_STOCK_LIST_SUCCESS, GET_STOCK_LIST_FAILURE, SET_REQUEST_STOCK_LIST, TEMP_STOCK, TEMP_STOCK_COMPLETE } from "./Types";

export const getStock = params => ({
    type: GET_STOCK_LIST,
    payload: params
});

export const getStockSuccess = response => ({
    type: GET_STOCK_LIST_SUCCESS,
    payload: response
});

export const getStockFailure = error => ({
    type: GET_STOCK_LIST_FAILURE,
    payload: error
});

export const setRequestStockList = value => ({
    type: SET_REQUEST_STOCK_LIST,
    payload: value
});

export const setTempStock = value => ({
    type: TEMP_STOCK,
    payload: value
});

export const setTempStockComplete = value => ({
    type: TEMP_STOCK_COMPLETE,
    payload: value
});