/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 07:44:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-20 14:47:23
 */
import { GET_PRODUCT_SERVICES, GET_PRODUCT_SERVICES_SUCCESS, GET_PRODUCT_SERVICES_FAILURE, SET_SELECTED_PRODUCT } from "./Types";

export const setSelectedProductService = value => ({
    type: SET_SELECTED_PRODUCT,
    payload: value
});

export const getProductService = () => ({
    type: GET_PRODUCT_SERVICES
});

export const getProductServiceSuccess = response => ({
    type: GET_PRODUCT_SERVICES_SUCCESS,
    payload: response
});

export const getProductServiceFailure = error => ({
    type: GET_PRODUCT_SERVICES_FAILURE,
    payload: error
});