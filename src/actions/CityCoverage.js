/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 14:41:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-05 14:42:16
 */
import { GET_CITY_COVERAGE, GET_CITY_COVERAGE_SUCCESS, GET_CITY_COVERAGE_FAILURE } from "./Types";

export const getCityCoverage = params => ({
    type: GET_CITY_COVERAGE,
    payload: params
});

export const getCityCoverageSuccess = response => ({
    type: GET_CITY_COVERAGE_SUCCESS,
    payload: response
});

export const getCityCoverageFailure = error => ({
    type: GET_CITY_COVERAGE_FAILURE,
    payload: error
});