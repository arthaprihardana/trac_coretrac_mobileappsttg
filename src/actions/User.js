/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-01 07:24:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-04 23:45:05
 */
import { PUT_USER_PROFILE, PUT_USER_PROFILE_SUCCESS, PUT_USER_PROFILE_FAILURE, GET_USER_PROFILE, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE, PUT_CHANGE_PASSWORD, PUT_CHANGE_PASSWORD_SUCCESS, PUT_CHANGE_PASSWORD_FAILURE} from "./Types";

export const getUserProfile = params => ({
    type: GET_USER_PROFILE,
    payload: params
});

export const getUserProfileSuccess = response => ({
    type: GET_USER_PROFILE_SUCCESS,
    payload: response
});

export const getUserProfileFailure = error =>  ({
    type: GET_USER_PROFILE_FAILURE,
    payload: error
});

export const updateUserProfile = params => ({
    type: PUT_USER_PROFILE,
    payload: params
});

export const updateUserProfileSuccess = response => ({
    type: PUT_USER_PROFILE_SUCCESS,
    payload: response
});

export const updateUserProfileFailure = error => ({
    type: PUT_USER_PROFILE_FAILURE,
    payload: error
});

export const putChangePassword = params => ({
    type: PUT_CHANGE_PASSWORD,
    payload: params
});

export const putChangePasswordSuccess = response => ({
    type: PUT_CHANGE_PASSWORD_SUCCESS,
    payload: response
});

export const putChangePasswordFailure = error => ({
    type: PUT_CHANGE_PASSWORD_FAILURE,
    payload: error
});