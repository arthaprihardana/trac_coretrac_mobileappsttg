/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 14:30:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-22 14:36:20
 */
import { GET_CONTENT_ARTICLE, GET_CONTENT_ARTICLE_SUCCESS, GET_CONTENT_ARTICLE_FAILURE } from "./Types";

export const getContentArticle = () => ({
    type: GET_CONTENT_ARTICLE
});

export const getContentArticleSuccess = response => ({
    type: GET_CONTENT_ARTICLE_SUCCESS,
    payload: response
});

export const getContentArticleFailure = error => ({
    type: GET_CONTENT_ARTICLE_FAILURE,
    payload: error
});