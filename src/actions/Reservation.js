/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 14:35:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 14:29:52
 */
import { POST_RESERVATION_SAVE, POST_RESERVATION_SAVE_SUCCESS, POST_RESERVATION_SAVE_FAILURE, SET_SUMMARY, GET_RESERVATION_DETAIL, GET_RESERVATION_DETAIL_SUCCESS, GET_RESERVATION_DETAIL_FAILURE } from "./Types";

export const setSummary = value => ({
    type: SET_SUMMARY,
    payload: value
})

export const saveReservation = params => ({
    type: POST_RESERVATION_SAVE,
    payload: params
});

export const saveReservationSuccess = response => ({
    type: POST_RESERVATION_SAVE_SUCCESS,
    payload: response
});

export const saveReservationFailure = error => ({
    type: POST_RESERVATION_SAVE_FAILURE,
    payload: error
});

export const getReservationDetail = params => ({
    type: GET_RESERVATION_DETAIL,
    payload: params
});

export const getReservationDetailSuccess = response => ({
    type: GET_RESERVATION_DETAIL_SUCCESS,
    payload: response
});

export const getReservationDetailFailure = error => ({
    type: GET_RESERVATION_DETAIL_FAILURE,
    payload: error
});