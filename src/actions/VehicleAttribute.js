/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 15:22:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-29 15:28:57
 */
import { GET_VEHICLE_ATTRIBUTE, GET_VEHICLE_ATTRIBUTE_SUCCESS, GET_VEHICLE_ATTRIBUTE_FAILURE } from "./Types";

export const getVehicleAttribute = params => ({
    type: GET_VEHICLE_ATTRIBUTE,
    payload: params
});

export const getVehicleAttributeSuccess = response => ({
    type: GET_VEHICLE_ATTRIBUTE_SUCCESS,
    payload: response
});

export const getVehicleAttributeFailure = error => ({
    type: GET_VEHICLE_ATTRIBUTE_FAILURE,
    payload: error
});