/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 10:11:07 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-14 10:11:07 
 */
import { SET_NOTES } from "./Types";

export const setNotes = value => ({
    type: SET_NOTES,
    payload: value
});