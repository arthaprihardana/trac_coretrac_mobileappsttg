/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 13:36:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-06 13:37:40
 */
import { GET_MASTER_CAR_TYPE, GET_MASTER_CAR_TYPE_SUCCESS, GET_MASTER_CAR_TYPE_FAILURE } from "./Types";

export const getMasterCarType = value => ({
    type: GET_MASTER_CAR_TYPE,
    payload: value
});

export const getMasterCarTypeSuccess = response => ({
    type: GET_MASTER_CAR_TYPE_SUCCESS,
    payload: response
});

export const getMasterCarTypeFailure = error => ({
    type: GET_MASTER_CAR_TYPE_FAILURE,
    payload: error
});