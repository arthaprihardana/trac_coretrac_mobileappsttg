/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-28 10:05:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-23 15:16:59
 */
import { POST_LOGIN, POST_LOGIN_SUCCESS, POST_LOGIN_FAILURE, POST_REGISTER, POST_REGISTER_SUCCESS, POST_REGISTER_FAILURE, POST_FORGOT_PASSWORD, POST_FORGOT_PASSWORD_SUCCESS, POST_FORGOT_PASSWORD_FAILURE, POST_LOGOUT, GET_REFRESH_TOKEN, GET_REFRESH_TOKEN_SUCCESS, GET_REFRESH_TOKEN_FAILURE, SET_NEW_TOKEN, SET_REMEMBER_ME, SET_NEW_USER, POST_LOGIN_SOSMED, POST_LOGIN_SOSMED_SUCCESS, POST_LOGIN_SOSMED_FAILURE } from "./Types";

// LOGIN
export const postLogin = params => ({
    type: POST_LOGIN,
    payload: params
});

export const postLoginSuccess = response => ({
    type: POST_LOGIN_SUCCESS,
    payload: response
});

export const postLoginFailure = error => ({
    type: POST_LOGIN_FAILURE,
    payload: error
});
// LOGIN

// LOGIN SOSIAL MEDIA
export const postLoginSosmed = params => ({
    type: POST_LOGIN_SOSMED,
    payload: params
});

export const postLoginSosmedSuccess = response => ({
    type: POST_LOGIN_SOSMED_SUCCESS,
    payload: response
});

export const postLoginSosmedFailure = error => ({
    type: POST_LOGIN_SOSMED_FAILURE,
    payload: error
});
// LOGIN SOSIAL MEDIA

// REGISTER
export const postRegister = params => ({
    type: POST_REGISTER,
    payload: params
});

export const postRegisterSuccess = response => ({
    type: POST_REGISTER_SUCCESS,
    payload: response
});

export const postRegisterFailure = error => ({
    type: POST_REGISTER_FAILURE,
    payload: error
});
// REGISTER

// FORGOT PASSWORD
export const postForgotPassword = params => ({
    type: POST_FORGOT_PASSWORD,
    payload: params
});

export const postForgotPasswordSuccess = response => ({
    type: POST_FORGOT_PASSWORD_SUCCESS,
    payload: response
});

export const postForgotPasswordFailure = error => ({
    type: POST_FORGOT_PASSWORD_FAILURE,
    payload: error
});
// FORGOT PASSWORD

// LOGOUT
export const postLogout = () => ({
    type: POST_LOGOUT
});
// LOGOUT

// REFRESH TOKEN
export const getRefreshToken = value => ({
    type: GET_REFRESH_TOKEN,
    payload: value
});

export const getRefreshTokenSuccess = response => ({
    type: GET_REFRESH_TOKEN_SUCCESS,
    payload: response
});

export const getRefreshTokenFailure = error => ({
    type: GET_REFRESH_TOKEN_FAILURE,
    payload: error
});
// REFRESH TOKEN

export const setNewToken = value => ({
    type: SET_NEW_TOKEN,
    payload: value
});

export const setRememberMe = value => ({
    type: SET_REMEMBER_ME,
    payload: value
});

export const setNewUser = params => ({
    type: SET_NEW_USER,
    payload: params
});