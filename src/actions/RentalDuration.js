/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 10:30:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-06 10:31:36
 */
import { GET_RENTAL_PACKAGE, GET_RENTAL_PACKAGE_SUCCESS, GET_RENTAL_PACKAGE_FAILURE } from "./Types";

export const getRentalPackage = () => ({
    type: GET_RENTAL_PACKAGE
});

export const getRentalPackageSuccess = response => ({
    type: GET_RENTAL_PACKAGE_SUCCESS,
    payload: response
});

export const getRentalPackageFailure = error => ({
    type: GET_RENTAL_PACKAGE_FAILURE,
    payload: error
});