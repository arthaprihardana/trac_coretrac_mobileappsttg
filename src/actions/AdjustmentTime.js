/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-25 13:46:12 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-25 13:46:12 
 */
import { GET_ADJUSTMENT_TIME, GET_ADJUSTMENT_TIME_SUCCESS, GET_ADJUSTMENT_TIME_FAILURE } from "./Types";

export const getAdjustmentTime = () => ({
    type: GET_ADJUSTMENT_TIME
});

export const getAdjustmentTimeSuccess = response => ({
    type: GET_ADJUSTMENT_TIME_SUCCESS,
    payload: response
});

export const getAdjustmentTimeFailure = error => ({
    type: GET_ADJUSTMENT_TIME_FAILURE,
    payload: error
});