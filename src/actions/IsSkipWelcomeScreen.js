/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 13:15:18 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-06 13:15:18 
 */
import { IS_SKIP } from './Types';

export const isSkipWelcomeScreen = value => ({
    type: IS_SKIP,
    payload: value
});
