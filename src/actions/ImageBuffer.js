/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-08 07:07:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-08 07:08:40
 */
import { GET_BUFFER_IMAGE_KTP, GET_BUFFER_IMAGE_KTP_SUCCESS, GET_BUFFER_IMAGE_KTP_FAILURE, GET_BUFFER_IMAGE_SIM, GET_BUFFER_IMAGE_SIM_SUCCESS, GET_BUFFER_IMAGE_SIM_FAILURE } from "./Types";

export const getImageBufferKtp = params => ({
    type: GET_BUFFER_IMAGE_KTP,
    payload: params
});

export const getImageBufferKtpSuccess = response => ({
    type: GET_BUFFER_IMAGE_KTP_SUCCESS,
    payload: response
});

export const getImageBufferKtpFailure = error => ({
    type: GET_BUFFER_IMAGE_KTP_FAILURE,
    payload: error
});

export const getImageBufferSim = params => ({
    type: GET_BUFFER_IMAGE_SIM,
    payload: params
});

export const getImageBufferSimSuccess = response => ({
    type: GET_BUFFER_IMAGE_SIM_SUCCESS,
    payload: response
});

export const getImageBufferSimFailure = error => ({
    type: GET_BUFFER_IMAGE_SIM_FAILURE,
    payload: error
});