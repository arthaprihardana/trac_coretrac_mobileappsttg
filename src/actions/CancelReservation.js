/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-31 21:54:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-31 23:10:18
 */
import { POST_CANCEL_RESERVATION, POST_CANCEL_RESERVATION_SUCCESS, POST_CANCEL_RESERVATION_FAILURE, POST_CANCEL_RESERVATION_REFUND, POST_CANCEL_RESERVATION_REFUND_SUCCESS, POST_CANCEL_RESERVATION_REFUND_FAILURE } from "./Types";

export const postCancelReservation = params => ({
    type: POST_CANCEL_RESERVATION,
    payload: params
});

export const postCancelReservationSuccess = response => ({
    type: POST_CANCEL_RESERVATION_SUCCESS,
    payload: response
});

export const postCancelReservationFailure = error => ({
    type: POST_CANCEL_RESERVATION_FAILURE,
    payload: error
});

export const postCancelReservationRefund = params => ({
    type: POST_CANCEL_RESERVATION_REFUND,
    payload: params
});

export const postCancelReservationRefundSuccess = response => ({
    type: POST_CANCEL_RESERVATION_REFUND_SUCCESS,
    payload: response
});

export const postCancelReservationRefundFailure = error => ({
    type: POST_CANCEL_RESERVATION_REFUND_FAILURE,
    payload: error
});