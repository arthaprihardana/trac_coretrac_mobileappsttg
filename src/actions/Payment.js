/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-27 14:16:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-27 14:17:13
 */
import { GET_VIRTUAL_ACCOUNT, GET_VIRTUAL_ACCOUNT_SUCCESS, GET_VIRTUAL_ACCOUNT_FAILURE } from "./Types";

export const getVirtualAccount = params => ({
    type: GET_VIRTUAL_ACCOUNT,
    payload: params
});

export const getVirtualAccountSuccess = response => ({
    type: GET_VIRTUAL_ACCOUNT_SUCCESS,
    payload: response
});

export const getVirtualAccountFailure = error => ({
    type: GET_VIRTUAL_ACCOUNT_FAILURE,
    payload: error
});