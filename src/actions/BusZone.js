/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 14:23:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-21 14:24:00
 */
import { GET_BUS_ZONE, GET_BUS_ZONE_SUCCESS, GET_BUS_ZONE_FAILURE } from "./Types";

export const getBusZone = params => ({
    type: GET_BUS_ZONE,
    payload: params
});

export const getBusZoneSuccess = response => ({
    type: GET_BUS_ZONE_SUCCESS,
    payload: response
});

export const getBusZoneFailure = error => ({
    type: GET_BUS_ZONE_FAILURE,
    payload: error
});