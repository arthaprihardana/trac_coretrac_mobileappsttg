/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 11:26:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 12:56:36
 */
import { GET_VALIDATE_PROMO, GET_VALIDATE_PROMO_SUCCESS, GET_VALIDATE_PROMO_FAILURE, IS_PROMO_VALID, SET_TOTAL_PRICE_AFTER_DISCOUNT, GET_PROMO, GET_PROMO_SUCCESS, GET_PROMO_FAILURE } from "./Types";

export const getValidatePromo = params => ({
    type: GET_VALIDATE_PROMO,
    payload: params
});

export const getValidatePromoSuccess = response => ({
    type: GET_VALIDATE_PROMO_SUCCESS,
    payload: response
});

export const getValidatePromoFailure = error => ({
    type: GET_VALIDATE_PROMO_FAILURE,
    payload: error
});

export const checkPromoValid = value => ({
    type: IS_PROMO_VALID,
    payload: value
});

export const setTotalPriceAfterDiscount = value => ({
    type: SET_TOTAL_PRICE_AFTER_DISCOUNT,
    payload: value
});

export const getPromo = () => ({
    type: GET_PROMO
});

export const getPromoSuccess = response => ({
    type: GET_PROMO_SUCCESS,
    payload: response
});

export const getPromoFailure = error => ({
    type: GET_PROMO_FAILURE,
    payload: error
});