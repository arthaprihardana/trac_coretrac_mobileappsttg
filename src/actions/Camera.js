/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-27 10:55:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-04 09:02:06
 */
import { SET_CAPTURE_CAMERA, OPEN_CAMERA_FOR_KTP, OPEN_CAMERA_FOR_SIM, OPEN_CAMERA_FOR_PROFILE, CAPTURE_FOR_KTP, CAPTURE_FOR_SIM, CAPTURE_FOR_PROFILE, CLEAR_CAPTURE_FOR_ALL } from "./Types";

export const openCameraForKtp = value => ({
    type: OPEN_CAMERA_FOR_KTP,
    payload: value
});

export const openCameraForSim = value => ({
    type: OPEN_CAMERA_FOR_SIM,
    payload: value
});

export const openCameraForProfile = value => ({
    type: OPEN_CAMERA_FOR_PROFILE,
    payload: value
});

export const setCaptureForKtp = value => ({
    type: CAPTURE_FOR_KTP,
    payload: value
});

export const setCaptureForSim = value => ({
    type: CAPTURE_FOR_SIM,
    payload: value
});

export const setCaptureForProfile = value => ({
    type: CAPTURE_FOR_PROFILE,
    payload: value
});

export const setCaptureCamera = value => ({
    type: SET_CAPTURE_CAMERA,
    payload: value
});

export const clearCaptureForAll = () => ({
    type: CLEAR_CAPTURE_FOR_ALL
});