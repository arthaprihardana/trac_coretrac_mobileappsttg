/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 11:31:58 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-29 11:31:58 
 */
import { GET_MY_BOOKING, GET_MY_BOOKING_SUCCESS, GET_MY_BOOKING_FAILURE } from "./Types";

export const getMyBooking = params => ({
    type: GET_MY_BOOKING,
    payload: params
});

export const getMyBookingSuccess = response => ({
    type: GET_MY_BOOKING_SUCCESS,
    payload: response
});

export const getMyBookingFailure = error => ({
    type: GET_MY_BOOKING_FAILURE,
    payload: error
});