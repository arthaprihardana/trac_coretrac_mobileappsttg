/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 11:34:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-21 12:38:35
 */
import { GET_MASTER_BRANCH_FOR_BUS, GET_MASTER_BRANCH_FOR_BUS_SUCCESS, GET_MASTER_BRANCH_FOR_BUS_FAILURE, SET_ACTUAL_BRANCH_FOR_BUS } from "./Types";

export const getMasterBranch = params => ({
    type: GET_MASTER_BRANCH_FOR_BUS,
    payload: params
});

export const getMasterBranchSuccess = response => ({
    type: GET_MASTER_BRANCH_FOR_BUS_SUCCESS,
    payload: response
});

export const getMasterBranchFailure = error => ({
    type: GET_MASTER_BRANCH_FOR_BUS_FAILURE,
    payload: error
});

export const setActualBranchForBus = value => ({
    type: SET_ACTUAL_BRANCH_FOR_BUS,
    payload: value
});