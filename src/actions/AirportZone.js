/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 14:23:07 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-03-20 14:23:07 
 */
import { GET_AIRPORT_ZONE, GET_AIRPORT_ZONE_SUCCESS, GET_AIRPORT_ZONE_FAILURE } from "./Types";

export const getAirportZone = params => ({
    type: GET_AIRPORT_ZONE,
    payload: params
});

export const getAirportZoneSuccess = response => ({
    type: GET_AIRPORT_ZONE_SUCCESS,
    payload: response
});

export const getAirportZoneFailure = error => ({
    type: GET_AIRPORT_ZONE_FAILURE,
    payload: error
});