/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-04 23:34:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-22 17:09:03
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { getCreditCardFailure, getCreditCardSuccess, saveCreditCardSuccess, saveCreditCardFailure, setPrimaryCreditCardSuccess, setPrimaryCreditCardFailure, deleteCreditCardSuccess, deleteCreditCardFailure } from "../actions";
import { GET_CREDIT_CARD, SAVE_CREDIT_CARD, SET_PRIMARY_CREDIT_CARD, DELETE_CREDIT_CARD } from "../actions/Types";
import { CREDIT_CARD } from "../constant";

const getCreditCardRequest = async params => {
    return await Axios.get(`${CREDIT_CARD}/get-by-user/${params.UserLoginId}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCreditCardFromServer({ payload }) {
    try {
        let response = yield call(getCreditCardRequest, payload);
        yield put(getCreditCardSuccess(response.data.Data));
    } catch (error) {
        yield put(getCreditCardFailure(error));
    }
}

export function* getCreditCardSaga() {
    yield takeEvery(GET_CREDIT_CARD, getCreditCardFromServer);
}

const postCreditCardRequest = async params => {
    console.log(`${CREDIT_CARD}/save`);
    
    return await Axios.post(`${CREDIT_CARD}/save`, params.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
};

function* postCreditCardToServer({ payload }) {
    try {
        const response = yield call(postCreditCardRequest, payload);
        console.log('response save cc ==>', response);
        yield put(saveCreditCardSuccess(response.data.Data));
        // if(response.data.Status === 201 || response.data.Status === 200) {
        //     yield put(saveCreditCardSuccess(response.data.Data));
        // } else {
        //     yield put(saveCreditCardSuccess(response.data));
        // }
    } catch (error) {
        yield put(saveCreditCardFailure('Oops! something went wrong!'));
    }
}

export function* postCreditCardSagas() {
    yield takeEvery(SAVE_CREDIT_CARD, postCreditCardToServer);
}

const putPrimaryCCRequest = async params => {
    return await Axios.put(`${CREDIT_CARD}/set-primary/${params.UserLoginId}`, params.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
};

function* putPrimaryCCToServer({ payload }) {
    // yield put(setPrimaryCreditCardSuccess(0));
    try {
        const create = yield call(putPrimaryCCRequest, payload);
        if(create.data.Status === 201 || create.data.Status === 200) {
            yield put(setPrimaryCreditCardSuccess(create.data.Data));
        } else {
            yield put(setPrimaryCreditCardFailure(create.data.ErrorMessage));
        }
    } catch (error) {
        yield put(setPrimaryCreditCardFailure('Oops! something went wrong!'));
    }
}

export function* updatePrimaryCCSagas() {
    yield takeEvery(SET_PRIMARY_CREDIT_CARD, putPrimaryCCToServer);
}

const deleteCreditCardRequest = async params => {
    return await Axios.delete(`${CREDIT_CARD}/delete/${params.Id}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
};
// //
function* deleteCreditCardToServer({ payload }) {
    yield put(deleteCreditCardSuccess(-1));
    try {
        const response = yield call(deleteCreditCardRequest, payload);
        if(response.data.Status === 201 || response.data.Status === 200) {
            yield put(deleteCreditCardSuccess(response.data.Data));
        } else {
            yield put(deleteCreditCardFailure(response.data.ErrorMessage));
        }
    } catch (error) {
        yield put(deleteCreditCardFailure('Oops! something went wrong!'));
    }
}
//
export function* deleteCreditCardSagas() {
    yield takeEvery(DELETE_CREDIT_CARD, deleteCreditCardToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCreditCardSaga),
        fork(postCreditCardSagas),
        fork(updatePrimaryCCSagas),
        fork(deleteCreditCardSagas)
    ]);
}