/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-07 02:15:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-03 13:58:18
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { PRICE_PRODUCT_CAR_RENTAL, PRICE_PRODUCT_BUS } from '../constant';
import { getPriceSuccess, getPriceFailure, getPriceForBusSuccess, getPriceForBusFailure } from '../actions';
import { GET_PRICE, GET_PRICE_FOR_BUS, GET_PRICE_ADD_HOURS } from '../actions/Types';
import { objectToQueryString } from '../helpers';

const getPriceRequest = async params => {
    return await Axios.get(`${PRICE_PRODUCT_CAR_RENTAL}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPriceFromServer({ payload }) {
    try {
        let response = yield call(getPriceRequest, payload);
        yield put(getPriceSuccess(response.data.Data));
    } catch (error) {
        yield put(getPriceFailure(error));
    }
}

export function* getPriceSaga() {
    yield takeEvery(GET_PRICE, getPriceFromServer);
}

const getPriceBusRentalRequest = async params => {
    return await Axios.get(`${PRICE_PRODUCT_BUS}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPriceBusRentalFromServer({ payload }) {
    try {
        let response = yield call(getPriceBusRentalRequest, payload);
        if(response.data.Data !== "") {
            yield put(getPriceForBusSuccess(response.data.Data));
        } else {
            yield put(getPriceForBusSuccess({
                Price: null,
                VehicleTypeId: payload.VehicleTypeId
            }));
        }
    } catch (error) {
        yield put(getPriceForBusFailure(error));
    }
}

export function* getPriceBusRentalSaga() {
    yield takeEvery(GET_PRICE_FOR_BUS, getPriceBusRentalFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getPriceSaga),
        fork(getPriceBusRentalSaga)
    ]);
}