/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-08 07:12:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-05 20:50:14
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import RNFetchBlob from 'rn-fetch-blob';
import { getImageBufferKtpSuccess, getImageBufferKtpFailure, getImageBufferSimSuccess, getImageBufferSimFailure } from '../actions';
import { GET_BUFFER_IMAGE_KTP, GET_BUFFER_IMAGE_SIM } from '../actions/Types';
// import base64 from '../helpers/base64'

const getImageBufferKtpRequest = async params => {
    return await RNFetchBlob.fetch('GET', params.uri, {})
        .then(response => response)
        .catch(error => error);
}

function* getImageBufferKtpFromServer({ payload }) {
    try {
        let response = yield call(getImageBufferKtpRequest, payload);
        yield put(getImageBufferKtpSuccess({
            uri: payload.uri, 
            base64: response.base64()
        }));
    } catch (error) {
        console.log('error buffer ktp', error)
        yield put(getImageBufferKtpFailure(error));
    }
}

export function* getImageBufferKtpSaga() {
    yield takeEvery(GET_BUFFER_IMAGE_KTP, getImageBufferKtpFromServer);
}

const getImageBufferSimRequest = async params => {
    return await RNFetchBlob.fetch('GET', params.uri, {})
        .then(response => response)
        .catch(error => error);
}

function* getImageBufferSimFromServer({ payload }) {
    try {
        let response = yield call(getImageBufferSimRequest, payload);
        yield put(getImageBufferSimSuccess({
            uri: payload.uri, 
            base64: response.base64()
        }));
    } catch (error) {
        yield put(getImageBufferSimFailure(error));
    }
}

export function* getImageBufferSimSaga() {
    yield takeEvery(GET_BUFFER_IMAGE_SIM, getImageBufferSimFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getImageBufferKtpSaga),
        fork(getImageBufferSimSaga)
    ]);
}