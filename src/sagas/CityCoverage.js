/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 14:45:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-05 16:20:17
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { CITY_COVERAGE } from "../constant";
import { getCityCoverageSuccess, getCityCoverageFailure } from "../actions";
import { GET_CITY_COVERAGE } from "../actions/Types";

const getCityCoverageRequest = async params => {
    return await Axios.get(`${CITY_COVERAGE(['CID-999999', params.BusinessUnitId, params.MsProductId])}`, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCityCoverageFromServer({ payload }) {
    try {
        const response = yield call(getCityCoverageRequest, payload);
        yield put(getCityCoverageSuccess(response.data.Data));
    } catch (error) {
        yield put(getCityCoverageFailure('Oops! something went wrong!'));
    }
}

export function* getCityCoverageSaga() {
    yield takeEvery(GET_CITY_COVERAGE, getCityCoverageFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCityCoverageSaga)
    ])
}