/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 11:40:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-29 14:18:43
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { MY_BOOKING } from "../constant";
import { getMyBookingSuccess, getMyBookingFailure } from "../actions";
import { GET_MY_BOOKING } from "../actions/Types";
import { objectToQueryString } from "../helpers";
import _ from "lodash";

const getMyBookingRequest = async params => {
    let prm = _.omit(params, ['token']);
    return await Axios.get(`${MY_BOOKING}?${objectToQueryString(prm)}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMyBookingFromServer({ payload }) {
    try {
        let response = yield call(getMyBookingRequest, payload);
        yield put(getMyBookingSuccess(response.data.Data));
    } catch (error) {
        yield put(getMyBookingFailure(error));
    }
}

export function* getMyBookingSaga() {
    yield takeEvery(GET_MY_BOOKING, getMyBookingFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMyBookingSaga)
    ]);
}