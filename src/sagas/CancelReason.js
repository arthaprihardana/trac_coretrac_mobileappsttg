/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-01 09:33:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-01 09:59:49
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { CANCEL_REASON } from "../constant";
import { objectToQueryString } from "../helpers";
import { getCancelReasonSuccess, getCancelReasonFailure } from "../actions";
import { GET_CANCEL_REASON } from "../actions/Types";
import _ from "lodash";

const getCancelReasonRequest = async params => {
    let prm = _.omit(params, ['token']);
    return await Axios.get(`${CANCEL_REASON}?${objectToQueryString(prm)}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCancelReasonFromServer({ payload }) {
    try {
        const response = yield call(getCancelReasonRequest, payload);
        yield put(getCancelReasonSuccess(response.data.Data));
    } catch (error) {
        yield put(getCancelReasonFailure('Oops! something went wrong!'));
    }
}

export function* getCancelReasonSaga() {
    yield takeEvery(GET_CANCEL_REASON, getCancelReasonFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCancelReasonSaga)
    ])
}