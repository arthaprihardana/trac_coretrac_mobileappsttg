/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 14:39:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 21:30:10
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { CANCEL_RESERVATION, CANCEL_RESERVATION_REFUND } from "../constant";
import { postCancelReservationSuccess, postCancelReservationFailure, postCancelReservationRefundSuccess, postCancelReservationRefundFailure } from "../actions";
import { POST_CANCEL_RESERVATION, POST_CANCEL_RESERVATION_REFUND } from "../actions/Types";

const postCancelReservationRequest = async payload => {
    return await Axios.put(CANCEL_RESERVATION, payload.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${payload.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postCancelReservationToServer({ payload }) {
    try {
        let response = yield call(postCancelReservationRequest, payload);
        console.log('response ==>', response);
        
        yield put(postCancelReservationSuccess(response.data.Data));
    } catch (error) {
        yield put(postCancelReservationFailure(error));
    }
}

export function* postCancelReservationSaga() {
    yield takeEvery(POST_CANCEL_RESERVATION, postCancelReservationToServer);
}

const postCancelReservationRefundRequest = async payload => {
    return await Axios.post(CANCEL_RESERVATION_REFUND, payload.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${payload.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postCancelReservationRefundToServer({ payload }) {
    try {
        let response = yield call(postCancelReservationRefundRequest, payload);
        yield put(postCancelReservationRefundSuccess(response.data.Data));
    } catch (error) {
        yield put(postCancelReservationRefundFailure(error));
    }
}

export function* postCancelReservationRefundSaga() {
    yield takeEvery(POST_CANCEL_RESERVATION_REFUND, postCancelReservationRefundToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postCancelReservationSaga),
        fork(postCancelReservationRefundSaga)
    ]);
}