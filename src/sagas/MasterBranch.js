/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-21 11:42:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-21 11:44:16
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { BRANCH } from "../constant";
import { getMasterBranchSuccess, getMasterBranchFailure } from "../actions";
import { GET_MASTER_BRANCH_FOR_BUS } from "../actions/Types";

const getBranchRequest = async params => {
    return await Axios.get(`${BRANCH}/${params}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getBranchFromServer({ payload }) {
    try {
        let response = yield call(getBranchRequest, payload);
        yield put(getMasterBranchSuccess(response.data.Data))
    } catch (error) {
        yield put(getMasterBranchFailure(error));
    }
}

export function* getBranchSaga() {
    yield takeEvery(GET_MASTER_BRANCH_FOR_BUS, getBranchFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getBranchSaga)
    ])
}