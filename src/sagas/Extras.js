/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 14:47:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-06 15:12:31
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { EXTRAS } from '../constant';
import { objectToQueryString } from '../helpers';
import { GET_EXTRAS } from '../actions/Types';
import { getExtrasSuccess, getExtrasFailure } from '../actions';

const getExtrasCarRentalRequest = async (params) => {
    return await Axios.get(`${EXTRAS}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getExtrasCarRentalFromServer({ payload }) {
    try {
        let response = yield call(getExtrasCarRentalRequest, payload);
        yield put(getExtrasSuccess(response.data.Data));
    } catch (error) {
        yield put(getExtrasFailure(error));
    }
}

export function* getExtrasCarRentalSaga() {
    yield takeEvery(GET_EXTRAS, getExtrasCarRentalFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getExtrasCarRentalSaga)
    ]);
}