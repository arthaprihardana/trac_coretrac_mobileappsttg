/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 11:29:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-16 13:09:10
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import _ from 'lodash';
import { VALIDATE_PROMO, PROMO } from "../constant";
import { objectToQueryString } from "../helpers";
import { getValidatePromoSuccess, getValidatePromoFailure, getPromoSuccess, getPromoFailure } from "../actions";
import { GET_VALIDATE_PROMO, GET_PROMO } from "../actions/Types";

const getValidatePromoRequest = async params => {
    let prm = _.omit(params, ['PromoCode']);
    return await Axios.get(`${VALIDATE_PROMO(params.PromoCode)}?${objectToQueryString(prm)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getValidatePromoFromServer({ payload }) {
    try {
        let response = yield call(getValidatePromoRequest, payload);
        yield put(getValidatePromoSuccess(response.data.data));
    } catch (error) {
        yield put(getValidatePromoFailure(error));
    }
}

export function* getValidatePromoSaga() {
    yield takeEvery(GET_VALIDATE_PROMO, getValidatePromoFromServer);
}

const getPromoRequest = async () => {
    return await Axios.get(`${PROMO}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPromoFromServer() {
    try {
        let response = yield call(getPromoRequest);
        let data = _.filter(response.data.Data, { Status: "1" });
        yield put(getPromoSuccess(data));
    } catch (error) {
        yield put(getPromoFailure(error));
    }
}

export function* getPromoSaga() {
    yield takeEvery(GET_PROMO, getPromoFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getValidatePromoSaga),
        fork(getPromoSaga)
    ]);
}