/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-14 15:13:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-01 23:11:22
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { BANK, ALL_BANK } from "../constant";
import { objectToQueryString } from "../helpers";
import { getBankSuccess, getBankFailure } from "../actions";
import { GET_MASTER_BANK } from "../actions/Types";

const getMasterBankRequest = async params => {
    return await Axios.get(`${BANK}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMasterBankFromServer({ payload }) {
    try {
        let response = yield call(getMasterBankRequest, payload);
        yield put(getBankSuccess(response.data.Data));
    } catch (error) {
        yield put(getBankFailure(error));
    }
}

export function* getMasterBankSaga() {
    yield takeEvery(GET_MASTER_BANK, getMasterBankFromServer);
}

// const getAllBankRequest = async params => {
//     return await Axios.get(ALL_BANK, {
//         headers: {
//             'content-type': 'application/json'
//         }
//     })
//     .then(response => response)
//     .catch(error => error);
// }

// function* getAllBankFromServer({ payload }) {
//     try {
//         let response = yield call(getAllBankRequest, payload);
//         yield put(getAllBankSuccess(response.data.Data));
//     } catch (error) {
//         yield put(getAllBankFailure(error));
//     }
// }

// export function* getAllBankSaga() {
//     yield takeEvery(GET_ALL_BANK, getAllBankFromServer);
// }

export default function* rootSaga() {
    yield all([
        fork(getMasterBankSaga),
        // fork(getAllBankSaga)
    ]);
}