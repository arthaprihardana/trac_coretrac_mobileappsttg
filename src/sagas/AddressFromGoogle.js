/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 17:19:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-03 11:15:35
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { GOOGLE_PLACE_AUTOCOMPLETE, GOOGLE_GET_DISTANCE_MATRIX, GOOGLE_PLACE_DETAIL, GOOGLE_FIND_PLACE } from '../constant';
import { GET_ADDRESS_GM, GET_DISTANCE_MATRIX_GM, GET_PLACE_DETAIL_GM, GET_FIND_GEOMETRY_GM, GET_DISTANCE_MATRIX_GM_FOR_BUS } from '../actions/Types';
import { getAddressFromGoogleSuccess, getAddressFromGoogleFailure, getDistanceMatrixFromGoogleSuccess, getDistanceMatrixFromGoogleFailure, getPlaceDetailFromGoogleSuccess, getPlaceDetailFromGoogleFailure, getFindGeometryFromGoogleSuccess, getFindGeometryFromGoogleFailure, getDistanceMatrixFromGoogleForBusSuccess, getDistanceMatrixFromGoogleForBusFailure } from '../actions';
import { objectToQueryString } from "../helpers";

// AUTOCOMPLETE START HERE
const getAddressRequest = async input => {
    return await Axios.get(`${GOOGLE_PLACE_AUTOCOMPLETE}?input=${input}`, {
        headers: {
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            crossdomain: true
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getAddressFromGoogle({ payload }) {
    try {
        let response = yield call(getAddressRequest, payload);
        yield put(getAddressFromGoogleSuccess(response.data.data.predictions))
    } catch (error) {
        yield put(getAddressFromGoogleFailure(error));
    }
}

export function* getAddressSaga() {
    yield takeEvery(GET_ADDRESS_GM, getAddressFromGoogle)
}
// AUTOCOMPLETE END HERE

// GET DISTANCE START HERE
const getDistanceRequest = async params => {
    return await Axios.get(`${GOOGLE_GET_DISTANCE_MATRIX}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            crossdomain: true
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDistanceFromGoogle({ payload }) {
    try {
        let response = yield call(getDistanceRequest, payload);
        if(response.data.data.status === "OK") {
            yield put(getDistanceMatrixFromGoogleSuccess(response.data.data.rows));
        } else {
            yield put(getDistanceMatrixFromGoogleSuccess(null));
        }
    } catch (error) {
        yield put(getDistanceMatrixFromGoogleFailure(error));
    }
}

export function* getDistanceSaga() {
    yield takeEvery(GET_DISTANCE_MATRIX_GM, getDistanceFromGoogle)
}
// GET DISTANCE END HERE

// GET DISTANCE FOR BUS START HERE
const getDistanceForBusRequest = async params => {
    return await Axios.get(`${GOOGLE_GET_DISTANCE_MATRIX}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            crossdomain: true
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDistanceFromGoogleForBus({ payload }) {
    try {
        let response = yield call(getDistanceForBusRequest, payload);
        if(response.data.data.status === "OK") {
            yield put(getDistanceMatrixFromGoogleForBusSuccess(response.data.data.rows));
        } else {
            yield put(getDistanceMatrixFromGoogleForBusSuccess(null));
        }
    } catch (error) {
        yield put(getDistanceMatrixFromGoogleForBusFailure(error));
    }
}

export function* getDistanceForBusSaga() {
    yield takeEvery(GET_DISTANCE_MATRIX_GM_FOR_BUS, getDistanceFromGoogleForBus)
}
// GET DISTANCE FOR BUS END HERE

// PLACE DETAIL START HERE
const getPlaceDetailRequest = async input => {
    return await Axios.get(`${GOOGLE_PLACE_DETAIL}?placeid=${input}`, {
        headers: {
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            crossdomain: true
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getPlaceDetailFromGoogle({ payload }) {
    try {
        let response = yield call(getPlaceDetailRequest, payload);
        if(response.data.data.status === "OK") {
            yield put(getPlaceDetailFromGoogleSuccess(response.data.data.result.geometry.location))
        } else {
            yield put(getPlaceDetailFromGoogleSuccess(null))
        }
    } catch (error) {
        yield put(getPlaceDetailFromGoogleFailure(error));
    }
}

export function* getPlaceDetailSaga() {
    yield takeEvery(GET_PLACE_DETAIL_GM, getPlaceDetailFromGoogle)
}
// PLACE DETAIL END HERE

// FIND GEOMETRY START HERE
const getFindGeometryRequest = async input => {
    return await Axios.get(`${GOOGLE_FIND_PLACE}?input=${input}`, {
        headers: {
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            crossdomain: true
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getFindGeometryFromGoogle({ payload }) {
    try {
        let response = yield call(getFindGeometryRequest, payload);
        if(response.data.data.status === "OK") {
            yield put(getFindGeometryFromGoogleSuccess(response.data.data.candidates[0].geometry.location));
        } else {
            yield put(getFindGeometryFromGoogleSuccess(null));
        }
    } catch (error) {
        yield put(getFindGeometryFromGoogleFailure(error));
    }
}

export function* getFindGeometrySaga() {
    yield takeEvery(GET_FIND_GEOMETRY_GM, getFindGeometryFromGoogle)
}
// FIND GEOMETRY END HERE

export default function* rootSaga() {
    yield all([
        fork(getAddressSaga),
        fork(getDistanceSaga),
        fork(getDistanceForBusSaga),
        fork(getPlaceDetailSaga),
        fork(getFindGeometrySaga)
    ])
}