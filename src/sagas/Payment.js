/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-27 14:19:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-27 15:26:44
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { CREATE_INVOICE } from "../constant";
import { getVirtualAccountSuccess, getVirtualAccountFailure } from "../actions";
import { GET_VIRTUAL_ACCOUNT } from "../actions/Types";

const postCreateInvoiceRequest = async payload => {
    return await Axios.post(CREATE_INVOICE, payload.FormData, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${payload.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postCreateInvoiceFromServer({ payload }) {
    try {
        let response = yield call(postCreateInvoiceRequest, payload);
        yield put(getVirtualAccountSuccess(response.data.Data));
    } catch (error) {
        yield put(getVirtualAccountFailure(error));
    }
}

export function* postCreateInvoiceSaga() {
    yield takeEvery(GET_VIRTUAL_ACCOUNT, postCreateInvoiceFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(postCreateInvoiceSaga)
    ]);
}