/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 14:28:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-15 14:30:26
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { ZONE } from "../constant";
import { getBusZoneSuccess, getBusZoneFailure } from "../actions";
import { GET_BUS_ZONE } from "../actions/Types";

// GET DURATION AIRPORT TRANSFER START HERE
const getBusZoneRequest = async params => {
    return await Axios.get(`${ZONE([params.BusinessUnitId, params.MsProductId, params.Distance])}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getBusZoneFromServer({ payload }) {
    try {
        let response = yield call(getBusZoneRequest, payload);
        yield put(getBusZoneSuccess(response.data.Data));
    } catch (error) {
        yield put(getBusZoneFailure(error));
    }
}

export function* getBusZoneSaga() {
    yield takeEvery(GET_BUS_ZONE, getBusZoneFromServer);
}
// GET DURATION AIRPORT TRANSFER END HERE

export default function* rootSaga() {
    yield all([
        fork(getBusZoneSaga)
    ]);
}