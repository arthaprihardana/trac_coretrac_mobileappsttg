/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 15:26:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-29 15:29:48
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { VEHICLE_ATTRIBUTE } from "../constant";
import { getVehicleAttributeSuccess, getVehicleAttributeFailure } from "../actions";
import { GET_VEHICLE_ATTRIBUTE } from "../actions/Types";

const getVehcileAttributeRequest = async params => {
    return await Axios.get(`${VEHICLE_ATTRIBUTE}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getVehicleAttributeFromServer({ payload }) {
    try {
        let response = yield call(getVehcileAttributeRequest, payload);
        yield put(getVehicleAttributeSuccess(response.data.Data));
    } catch (error) {
        yield put(getVehicleAttributeFailure(error));
    }
}

export function* getVehicleAttributeSagas() {
    yield takeEvery(GET_VEHICLE_ATTRIBUTE, getVehicleAttributeFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getVehicleAttributeSagas)
    ]);
}