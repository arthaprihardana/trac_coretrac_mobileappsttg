/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-19 10:19:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-19 10:30:03
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import { AIRPORT_COVERAGE, MASTER_AIRPORT } from "../constant";
import { objectToQueryString } from "../helpers";
import Axios from 'axios';
import { getAirportCoverageSuccess, getAirportCoverageFailure, getMasterAirportSuccess, getMasterAirportFailure } from "../actions";
import { GET_AIRPORT_COVERAGE, GET_MASTER_AIRPORT } from "../actions/Types";

const getMasterAirport = async () => {
    return await Axios.get(MASTER_AIRPORT, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMasterAirportFromServer({ payload }) {
    try {
        let response = yield call(getMasterAirport, payload);
        yield put(getMasterAirportSuccess(response.data.Data));
    } catch (error) {
        yield put(getMasterAirportFailure(error));
    }
}

export function* getMasterAirportSaga() {
    yield takeEvery(GET_MASTER_AIRPORT, getMasterAirportFromServer);
}

const getAirportCoverage = async params => {
    return await Axios.get(`${AIRPORT_COVERAGE}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getAirportCoverageFromServer({ payload }) {
    try {
        let response = yield call(getAirportCoverage, payload);
        yield put(getAirportCoverageSuccess(response.data.Data));
    } catch (error) {
        yield put(getAirportCoverageFailure(error));
    }
}

export function* getAirportCoverageSaga() {
    yield takeEvery(GET_AIRPORT_COVERAGE, getAirportCoverageFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMasterAirportSaga),
        fork(getAirportCoverageSaga)
    ]);
}