/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-01 07:27:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-07 15:15:08
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { USER_PROFILE_UPDATE, USER, CHANGE_PASSWORD, CREDIT_CARD } from "../constant";
import { updateUserProfileSuccess, updateUserProfileFailure, getUserProfileSuccess, getUserProfileFailure, putChangePasswordSuccess, putChangePasswordFailure, getCreditCardSuccess, getCreditCardFailure } from "../actions";
import { PUT_USER_PROFILE, GET_USER_PROFILE, PUT_CHANGE_PASSWORD, GET_CREDIT_CARD } from "../actions/Types";
import _ from "lodash";

const getUserProfileRequest = async payload => {
    return await Axios.get(`${USER}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${payload.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getUserProfileToServer({ payload }) {
    try {
        let response = yield call(getUserProfileRequest, payload);
        yield put(getUserProfileSuccess(response.data.Data));
    } catch (error) {
        yield put(getUserProfileFailure(error));
    }
}

export function* getUserProfileSaga() {
    yield takeEvery(GET_USER_PROFILE, getUserProfileToServer);
}

const putUserProfileRequest = async payload => {
    return await Axios.put(`${USER_PROFILE_UPDATE}/${payload.UserId}`, payload.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${payload.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* putUserProfileToServer({ payload }) {
    try {
        let response = yield call(putUserProfileRequest, payload);
        yield put(updateUserProfileSuccess(response.data.Data));
    } catch (error) {
        yield put(updateUserProfileFailure(error));
    }
}

export function* putUserProfileSaga() {
    yield takeEvery(PUT_USER_PROFILE, putUserProfileToServer);
}

const putChangePasswordRequest = async params => {
    return await Axios.put(CHANGE_PASSWORD, params.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
};

function* putChangePasswordToServer({ payload }) {
    try {
        const response = yield call(putChangePasswordRequest, payload);
        if((response.data.Status === 201 || response.data.Status === 200) && response.data.Data!=='') {
            yield put(putChangePasswordSuccess(response.data.Data));
        } else {
            yield put(putChangePasswordSuccess(response.data.ErrorMessage));
        }
    } catch (error) {
        yield put(putChangePasswordFailure('Oops! something went wrong!'));
    }
}

export function* putChangePasswordSaga() {
    yield takeEvery(PUT_CHANGE_PASSWORD, putChangePasswordToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getUserProfileSaga),
        fork(putUserProfileSaga),
        fork(putChangePasswordSaga)
    ]);
}