/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 10:35:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 18:56:30
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { GET_RENTAL_PACKAGE } from "../actions/Types";
import { getRentalPackageSuccess, getRentalPackageFailure } from "../actions";
import { RENTAL_DURATION_CAR_RENTAL } from "../constant";

const getRentalPackageRequest = async () => {
    return await Axios.get(RENTAL_DURATION_CAR_RENTAL, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getRentalPackageFromServer() {
    try {
        let response = yield call(getRentalPackageRequest);
        yield put(getRentalPackageSuccess(response.data.Data));
    } catch (error) {
        yield put(getRentalPackageFailure(error));
    }
}

export function* getRentalPackageSaga() {
    yield takeEvery(GET_RENTAL_PACKAGE, getRentalPackageFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getRentalPackageSaga)
    ]);
}