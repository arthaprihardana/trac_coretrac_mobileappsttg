/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-05 07:50:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-08 10:40:31
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { LIST_PRODUCT } from "../constant";
import { getProductServiceSuccess, getProductServiceFailure } from "../actions";
import { GET_PRODUCT_SERVICES } from "../actions/Types";
import Axios from 'axios';

const getProductServiceRequest = async () => {
    return await Axios.get(LIST_PRODUCT, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getProductServiceFromServer() {
    try {
        const response = yield call(getProductServiceRequest);
        yield put(getProductServiceSuccess(response.data.Data));
    } catch (error) {
        yield put(getProductServiceFailure('Oops! something went wrong!'));
    }
}

export function* getProductServiceSaga() {
    yield takeEvery(GET_PRODUCT_SERVICES, getProductServiceFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getProductServiceSaga)
    ])
}