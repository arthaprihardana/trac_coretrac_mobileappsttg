/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-20 14:28:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-21 14:32:43
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { ZONE } from "../constant";
import { getAirportZoneSuccess, getAirportZoneFailure } from "../actions";
import { GET_AIRPORT_ZONE } from "../actions/Types";

// GET DURATION AIRPORT TRANSFER START HERE
const getAirportZoneRequest = async params => {
    return await Axios.get(`${ZONE([params.BusinessUnitId, params.MsProductId, params.Distance])}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getAirportZoneFromServer({ payload }) {
    try {
        let response = yield call(getAirportZoneRequest, payload);
        yield put(getAirportZoneSuccess(response.data.Data));
    } catch (error) {
        yield put(getAirportZoneFailure(error));
    }
}

export function* getAirportZoneSaga() {
    yield takeEvery(GET_AIRPORT_ZONE, getAirportZoneFromServer);
}
// GET DURATION AIRPORT TRANSFER END HERE

export default function* rootSaga() {
    yield all([
        fork(getAirportZoneSaga)
    ]);
}