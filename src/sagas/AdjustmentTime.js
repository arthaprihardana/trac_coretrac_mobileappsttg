/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-25 14:13:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-25 14:29:19
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { getAdjustmentTimeSuccess, getAdjustmentTimeFailure } from "../actions";
import { GET_ADJUSTMENT_TIME } from "../actions/Types";
import { ADJUSTMENT_TIME } from "../constant";

const getAdjustmentRetailRequest = async params => {
    return await Axios.get(ADJUSTMENT_TIME, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getAdjustmentRetailFromServer({ payload }) {
    try {
        let response = yield call(getAdjustmentRetailRequest, payload);
        yield put(getAdjustmentTimeSuccess(response.data.Data));
    } catch (error) {
        yield put(getAdjustmentTimeFailure(error));
    }
}

export function* getAdjustmentRetailSaga() {
    yield takeEvery(GET_ADJUSTMENT_TIME, getAdjustmentRetailFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getAdjustmentRetailSaga)
    ]);
}