/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-22 14:34:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-22 14:36:54
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { CONTENT_ARTICLE } from "../constant";
import { getContentArticleSuccess, getContentArticleFailure } from "../actions";
import { GET_CONTENT_ARTICLE } from "../actions/Types";

const getContentArticleRequest = async params => {
    return await Axios.get(CONTENT_ARTICLE, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getContentArticleFromServer({ payload }) {
    try {
        const response = yield call(getContentArticleRequest, payload);
        yield put(getContentArticleSuccess(response.data.Data));
    } catch (error) {
        yield put(getContentArticleFailure('Oops! something went wrong!'));
    }
}

export function* getContentArticleSaga() {
    yield takeEvery(GET_CONTENT_ARTICLE, getContentArticleFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getContentArticleSaga)
    ])
}