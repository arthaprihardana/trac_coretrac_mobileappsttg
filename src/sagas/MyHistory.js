/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-29 11:40:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-29 11:43:13
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { MY_HISTORY } from "../constant";
import { getMyHistorySuccess, getMyHistoryFailure } from "../actions";
import { GET_MY_HISTORY } from "../actions/Types";
import { objectToQueryString } from "../helpers";
import _ from "lodash";

const getMyHistoryRequest = async params => {
    let prm = _.omit(params, ['token']);
    return await Axios.get(`${MY_HISTORY}?${objectToQueryString(prm)}`, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMyHistoryFromServer({ payload }) {
    try {
        let response = yield call(getMyHistoryRequest, payload);
        yield put(getMyHistorySuccess(response.data.Data));
    } catch (error) {
        yield put(getMyHistoryFailure(error));
    }
}

export function* getMyHistorySaga() {
    yield takeEvery(GET_MY_HISTORY, getMyHistoryFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMyHistorySaga)
    ]);
}
