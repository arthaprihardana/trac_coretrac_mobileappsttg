/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-28 10:23:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-08 22:53:09
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { postLoginSuccess, postLoginFailure, postRegisterSuccess, postRegisterFailure, postForgotPasswordSuccess, postForgotPasswordFailure, getRefreshTokenSuccess, getRefreshTokenFailure, postLoginSosmedSuccess, postLoginSosmedFailure } from "../actions/Authentication";
import { POST_LOGIN, POST_REGISTER, POST_FORGOT_PASSWORD, GET_REFRESH_TOKEN, POST_LOGIN_SOSMED } from "../actions/Types";
import { LOGIN, REGISTER, FORGOT_PASSWORD, REFRESH_TOKEN, LOGIN_SOSMED, CHECK_TOKEN } from "../constant";

// LOGIN
const postLoginRequest = async FormData => {
    return await Axios.post(LOGIN, FormData, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postLoginToServer({ payload }) {
    try {
        const login = yield call(postLoginRequest, payload);
        if(login.data.Data !== "") {
            yield put(postLoginSuccess({
                login_data: {
                    Data: login.data.Data,
                    token: login.data.token
                }
            }));
        } else {
            yield put(postLoginFailure(login.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postLoginFailure('Oops! something went wrong!'));
    }
}

export function* postLoginSaga() {
    yield takeEvery(POST_LOGIN, postLoginToServer);
}
// LOGIN

// LOGIN SOSMED
const postLoginSosmedRequest = async FormData => {
    return await Axios.post(LOGIN_SOSMED, FormData, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postLoginSosmedToServer({ payload }) {
    try {
        const login = yield call(postLoginSosmedRequest, payload);
        if(login.data.Data !== "") {
            yield put(postLoginSosmedSuccess({
                login_data: {
                    Data: login.data.Data,
                    token: login.data.token
                }
            }));
        } else {
            yield put(postLoginSosmedFailure(login.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postLoginSosmedFailure('Oops! something went wrong!'));
    }
}

export function* postLoginSosmedSaga() {
    yield takeEvery(POST_LOGIN_SOSMED, postLoginSosmedToServer);
}
// LOGIN SOSMED

// REGISTER
const postRegisterRequest = async FormData => {
    return await Axios.post(REGISTER, FormData, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postRegisterToServer({ payload }) {
    try {
        const register = yield call(postRegisterRequest, payload);
        if(register.data.Status === 201) {
            yield put(postRegisterSuccess(register.data.Data));
        } else {
            yield put(postRegisterFailure(register.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postRegisterFailure('Oops! something went wrong!'));
    }
}

export function* postRegisterSaga() {
    yield takeEvery(POST_REGISTER, postRegisterToServer);
}
// REGISTER

// FORGOT PASSWORD
const postForgotPasswordRequest = async FormData => {
    return await Axios.post(FORGOT_PASSWORD, FormData, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postForgotPasswordToServer({ payload }) {
    try {
        const register = yield call(postForgotPasswordRequest, payload);
        if(register.data.Status === 200) {
            yield put(postForgotPasswordSuccess(register.data.Data));
        } else {
            yield put(postForgotPasswordFailure(register.data.ErrorMessage));
        }
    } catch (error) {
        yield put(postForgotPasswordFailure('Oops! something went wrong!'));
    }
}

export function* postForgotPasswordSaga() {
    yield takeEvery(POST_FORGOT_PASSWORD, postForgotPasswordToServer);
}
// FORGOT PASSWORD

// REFRESH TOKEN
const getRefreshTokenRequest = async params => {
    return await Axios.get(CHECK_TOKEN, {
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${params}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getRefreshTokenFromServer({ payload }) {
    try {
        const response = yield call(getRefreshTokenRequest, payload);
        console.log('res ==>', response);
        
        yield put(getRefreshTokenSuccess(response.data.Data));
    } catch (error) {
        yield put(getRefreshTokenFailure(error));
    }
}

export function* getRefreshTokenSaga() {
    yield takeEvery(GET_REFRESH_TOKEN, getRefreshTokenFromServer);
}
// REFRESH TOKEN

export default function* rootSaga() {
    yield all([
        fork(postLoginSaga),
        fork(postRegisterSaga),
        fork(postForgotPasswordSaga),
        fork(getRefreshTokenSaga),
        fork(postLoginSosmedSaga)
    ])
}