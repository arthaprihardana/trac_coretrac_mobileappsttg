/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-15 14:39:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-07 17:31:36
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from "axios";
import { RESERVATION, DETAIL_MY_RESERVATION } from "../constant";
import { saveReservationSuccess, saveReservationFailure, getReservationDetailSuccess, getReservationDetailFailure } from "../actions";
import { POST_RESERVATION_SAVE, GET_RESERVATION_DETAIL } from "../actions/Types";

const postReservationRequest = async payload => {
    return await Axios.post(RESERVATION, payload.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${payload.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postReservationToServer({ payload }) {
    try {
        let response = yield call(postReservationRequest, payload);
        console.log('response reservation ==>', response);
        yield put(saveReservationSuccess(response.data.Data));
    } catch (error) {
        yield put(saveReservationFailure(error));
    }
}

export function* postReservationSaga() {
    yield takeEvery(POST_RESERVATION_SAVE, postReservationToServer);
}

const getReservationDetailRequest = async params => {
    return await Axios.get(`${DETAIL_MY_RESERVATION}/${params.ReservationId}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReservationDetailFromServer({ payload }) {
    try {
        let response = yield call(getReservationDetailRequest, payload);
        yield put(getReservationDetailSuccess(response.data.Data));
    } catch (error) {
        yield put(getReservationDetailFailure(error));
    }
}

export function* getReservationDetailSaga() {
    yield takeEvery(GET_RESERVATION_DETAIL, getReservationDetailFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(postReservationSaga),
        fork(getReservationDetailSaga)
    ]);
}