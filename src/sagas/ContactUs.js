/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-20 23:48:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-22 11:38:44
 */
import { all, call, put, fork, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { CONTACT_US } from "../constant";
import { postContactUsSuccess, postContactUsFailure } from "../actions";
import { POST_CONTACT_US } from "../actions/Types";

const postContactUsRequest = async params => {
    return await Axios.post(CONTACT_US, params.FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${params.token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* postContactUsToServer({ payload }) {
    try {
        const response = yield call(postContactUsRequest, payload);
        yield put(postContactUsSuccess(response.data.Data));
    } catch (error) {
        yield put(postContactUsFailure('Oops! something went wrong!'));
    }
}

export function* postContactUsSaga() {
    yield takeEvery(POST_CONTACT_US, postContactUsToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postContactUsSaga)
    ])
}