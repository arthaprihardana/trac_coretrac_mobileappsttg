/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 15:24:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-03 14:19:27
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { SM_APP_KEY, LIST_STOCK } from '../constant';
import { objectToQueryString } from '../helpers';
import { getStockSuccess, getStockFailure } from '../actions';
import { GET_STOCK_LIST } from '../actions/Types';

const getStockRequest = async (params) => {
    // ?BusinessUnitId=0102&BranchId=SL44&StartDate=2019-01-16T07:00:00.000Z&EndDate=2019-01-16T12:00:00.000Z&IsWithDriver =1
    return await Axios.get(`${LIST_STOCK}?${objectToQueryString(params)}`, {
        headers: {
            'content-type': 'application/json',
            'App-Key': SM_APP_KEY
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getStockFromServer({ payload }) {
    try {
        let response = yield call(getStockRequest, payload);
        console.log('response stok ==>', response);
        
        yield put(getStockSuccess(response.data.data));
    } catch (error) {
        yield put(getStockFailure(error));
    }
}

export function* getStockCarRentalSaga() {
    yield takeEvery(GET_STOCK_LIST, getStockFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getStockCarRentalSaga)
    ]);
}