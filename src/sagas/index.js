/**
 * @author: Artha Prihardana 
 * @Date: 2019-02-28 10:37:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-04-22 14:37:19
 */
import { all } from 'redux-saga/effects';

import AuthenticationSaga from './Authentication';
import ProductServiceSaga from './ProductService';
import CityCoverageSaga from './CityCoverage';
import AddressFromGoogleSaga from './AddressFromGoogle';
import RentalDurationSaga from './RentalDuration';
import CarTypeSaga from './MasterCarType';
import ExtrasSaga from './Extras';
import StockSaga from './Stock';
import PriceSaga from './Price';
import BankSaga from './MasterBank';
import PromoSaga from './Promo';
import ReservationSaga from './Reservation';
import AirportCoverageSaga from './AirportCoverage';
import AirportZoneSaga from './AirportZone';
import MasterBranchSaga from './MasterBranch';
import BusZoneSaga from './BusZone';
import AdjustmentRetailSaga from './AdjustmentTime';
import PaymentSaga from './Payment';
import MyBookingSaga from './MyBooking';
import VehicleAttributeSaga from './VehicleAttribute';
import MyHistorySaga from './MyHistory';
import CancelReservationSaga from './CancelReservation';
import UserProfileSaga from './User';
import CancelReasonSaga from './CancelReason';
import CreditCardSaga from './CreditCard';
import BufferImageSaga from './ImageBuffer';
import ContactUsSaga from './ContactUs';
import ContentArticleSaga from './ContentArticle';

export default function* rootSaga() {
    yield all([
        AuthenticationSaga(),
        ProductServiceSaga(),
        CityCoverageSaga(),
        AddressFromGoogleSaga(),
        RentalDurationSaga(),
        CarTypeSaga(),
        ExtrasSaga(),
        StockSaga(),
        PriceSaga(),
        BankSaga(),
        PromoSaga(),
        ReservationSaga(),
        AirportCoverageSaga(),
        AirportZoneSaga(),
        MasterBranchSaga(),
        BusZoneSaga(),
        AdjustmentRetailSaga(),
        PaymentSaga(),
        MyBookingSaga(),
        VehicleAttributeSaga(),
        MyHistorySaga(),
        CancelReservationSaga(),
        UserProfileSaga(),
        CancelReasonSaga(),
        CreditCardSaga(),
        BufferImageSaga(),
        ContactUsSaga(),
        ContentArticleSaga()
    ])
}