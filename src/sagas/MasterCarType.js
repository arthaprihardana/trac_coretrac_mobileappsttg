/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-06 13:41:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-06 13:43:00
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { LIST_CAR_TYPE } from '../constant';
import { getMasterCarTypeSuccess, getMasterCarTypeFailure } from '../actions';
import { GET_MASTER_CAR_TYPE } from '../actions/Types';

const getCarTypeRequest = async param => {
    return await Axios.get(`${LIST_CAR_TYPE}/${param}`, {
        headers: {
            'content-type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarTypeFromServer({ payload }) {
    try {
        let response = yield call(getCarTypeRequest, payload);
        yield put(getMasterCarTypeSuccess(response.data.Data));
    } catch (error) {
        yield put(getMasterCarTypeFailure(error));
    }
}

export function* getCarTypeSaga() {
    yield takeEvery(GET_MASTER_CAR_TYPE, getCarTypeFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCarTypeSaga)
    ]);
}