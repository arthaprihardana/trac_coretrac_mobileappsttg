/**
 * @author: Artha Prihardana 
 * @Date: 2019-03-04 13:16:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-03-06 13:30:23
 */
import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from "redux-saga";
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import RootReducers from '../reducers/index';
import RootSaga from '../sagas/index';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore() {
    const store = createStore(
        RootReducers, 
        composeEnhancer(applyMiddleware(...middlewares))
    );

    let persistor = persistStore(store);

    sagaMiddleware.run(RootSaga);
    
    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('../reducers/index').default;
            store.replaceReducer(nextRootReducer);
        })
    }
    
    // return store;
    return { store, persistor };
}