export default {
    // "default":"id",
    "goodMorning": {
      "id": "Selamat Pagi,",
      "en": "Good Morning,"
    },
    "goodDay": {
      "id": "Selamat Siang,",
      "en": "Good Day,"
    },
    "goodAfternoon": {
      "id": "Selamat Sore,",
      "en": "Good Afternoon,"
    },
    "goodNight": {
      "id": "Selamat Malam,",
      "en": "Good Night,"
    },
    "guest": {
      "id": "Tamu",
      "en": "Guest"
    },
    "explore": {
      "id": "Jelajahi",
      "en": "Explore"
    },
    "myProfile": {
      "id": "Profil Saya",
      "en": "My Profile"
    },
    "more": {
      "id": "Lain",
      "en": "More"
    },
    "seeAll": {
      "id": "Selengkapnya",
      "en": "See All"
    },
    "orContinueWith": {
      "id": "Atau Masuk Dengan",
      "en": "Or Continue With"
    },
    "languageApps": {
      "id": "Bahasa",
      "en": "Language"
    },
    "forgotPasswordQuote": {
      "id": "Masukkan alamat email Anda dan kami akan mengirimkan Anda instruksi",
      "en": "Enter your email address and we'll send you the instruction"
    },
    "myVoucher": {
      "id": "Kupon Saya",
      "en": "My Voucher"
    },
    "couponCode": {
      "id": "Kode Kupon",
      "en": "Coupon Code"
    },
    "copy": {
      "id": "Salin",
      "en": "Copy"
    },
    "copyPromo": {
      "id": "Salin Kode",
      "en": "Copy Code"
    },
    "successfullyCopyPromo": {
      "id": "Kode telah disalin",
      "en": "Code has been copied"
    },
    "loadMore": {
      "id": "Muat Lebih",
      "en": "Load More"
    },
    "anotherArticle": {
      "id": "Artikel Lainnya",
      "en": "Another Article"
    },
    "whatCanHelpYou": {
      "id": "Apa yang bisa kita bantu?",
      "en": "What can we help you with?"
    },
    "findTopic": {
      "id": "Ketik di sini untuk menemukan topik",
      "en": "Type here to find a topic"
    },
    "changeLanguage": {
      "id": "Ganti Bahasa",
      "en": "Change Language"
    },
    "category": {
      "id": "Kategori",
      "en": "Category"
    },
    "subCategory": {
      "id": "Sub Kategori",
      "en": "Sub Category"
    },
    "message": {
      "id": "Pesan",
      "en": "Message"
    },
    "oldPassword": {
      "id": "Old Password",
      "en": "New Password"
    },
    "identificationInformationQuote": {
      "id": "Silakan masukkan nomor ID Anda dan nomor SIM untuk memudahkan Anda memesan",
      "en": "Please enter your ID number and Driver license number to make it easier for you to order"
    },
    "wni": {
      "id": "Warga Negara Indonesia",
      "en": "Indonesia Citizen"
    },
    "wna": {
      "id": "Warga Negara Asing",
      "en": "Foreign Citizen"
    },
    "imageKtp": {
      "id": "Foto KTP",
      "en": "KTP Image"
    },
    "imageSim": {
      "id": "Foto SIM",
      "en": "SIM Image"
    },
    "add": {
      "id": "Tambah",
      "en": "Add"
    },
    "update": {
      "id": "Ubah",
      "en": "Update"
    },
    "reservationDetail": {
      "id": "Detail Reservasi",
      "en": "Reservation Detail"
    },
    "detail": {
      "id": "Selengkapnya",
      "en": "Details"
    },
    "vehicleInformation": {
      "id": "Informasi Kendaraan",
      "en": "Vehicle Information"
    },
    "for": {
      "id": "Untuk",
      "en": "For"
    },
    "hour": {
      "id": "Jam",
      "en": "Hour"
    },
    "minutes": {
      "id": "Menit",
      "en": "Minutes"
    },
    "second": {
      "id": "Detik",
      "en": "Second"
    },
    "cancelQuote": {
      "id": "Untuk membatalkan secara gratis, Anda harus membatalkan sebelum",
      "en": "To cancel for free, you need to cancel before"
    },
    "jakartaTime": {
      "id": "Waktu Jakarta",
      "en": "Jakarta time"
    },
    "notification": {
      "id": "Notifikasi",
      "en": "Notification"
    },
    "dataNotFound": {
      "id": "Kendaraan belum tersedia",
      "en": "Vehicle not available"
    },
    "callCenter": {
      "id": "Pusat Bantuan",
      "en": "Call Center"
    },
    "filter": {
      "id": "MENYARING",
      "en": "FILTER"
    },
    "sort": {
      "id": "URUTKAN",
      "en": "SORT"
    },
    "reset": {
      "id": "ATUR ULANG",
      "en": "RESET"
    },
    "fromCity": {
      "id": "Dari Kota",
      "en": "From City"
    },
    "fromLocation": {
      "id": "Dari Lokasi",
      "en": "From Location"
    },
    "toLocation": {
      "id": "Ke Lokasi",
      "en": "To Location"
    },
    "IUnderstandThe": {
      "id": "Saya mengerti dengan",
      "en": "I understand the"
    },
    "EnterPromoCode": {
      "id": "Masukan Kode Promo",
      "en": "Enter Promo Code"
    },
    "ImDriver": {
      "id": "Saya Supir",
      "en": "I'm Driver"
    },
    "SeatingArrangement": {
      "id": "Pengaturan tempat duduk",
      "en": "Seating Arrangement"
    },

    "login":{
      "id":"Login",
      "en":"Log in"
    },
    "dontHaveAccount" : {
      "id":"Belum punya akun?",
      "en":"Don't have an account?"
    },
    "dreamExploreDiscover" : {
      "id":"Temukan dan Jelajahi Destinasi Baru Bersama TRAC To Go",
      "en":"Dream, Explore, Discover Making Memories with TRAC To Go"
    },
    "registerHere" : {
      "id":"Daftar Sekarang!",
      "en":"Register Here!"
    },
    "loginEmptyEmail" : {
      "id":"Bagian ini harus diisi",
      "en":"Email is required!"
    },
    "loginEmptyPassword" : {
      "id":"Bagian ini harus diisi",
      "en":"Password is required!"
    },
    "rememberMe" : {
      "id":"Saya ingin tetap login",
      "en":"Remember Me"
    },
    "forgotPassword" : {
      "id":"Lupa Password",
      "en":"Forgot Password?"
    },
    "loginButton" : {
      "id":"Log in",
      "en":"LOGIN"
    },
    "loginSocial" : {
      "id":"Gunakan akun media sosial Anda",
      "en":"Use your social accounts"
    },
    "corporateRental" : {
      "id":"Akun Korporat",
      "en":"Corporate Rental Account"
    },
    "corporateRentalText" : {
      "id":"Bagi pemilik akun Corporate Silahkan masuk melalui portal berikut",
      "en":"Please login here"
    },
    "loginError" : {
      "id":"Email atau password salah. Coba kembali.",
      "en":"The username and password you entered did not match"
    },
    "congratulationRegister" : {
      "id":"Hai ",
      "en":"Congratulation "
    },
    "registerCongrats" : {
      "id":" Anda telah terdaftar sebagai member TRAC!",
      "en":" You've become a TRAC Member!"
    },
    "continueButton" : {
      "id":"Lanjutkan",
      "en":"Continue"
    },
    "register" : {
      "id":"Daftar",
      "en":"Register"
    },
    "alreadyHaveAccount" : {
      "id":"Sudah Memiliki Akun?",
      "en":"Already have an account?"
    },
    "loginHere" : {
      "id":"Log in Disini",
      "en":"Login Here!"
    },
    "email" : {
      "id":"Email",
      "en":"Email"
    },
    "passwordPlaceholder" : {
      "id":"Password (minimum 8 characters)",
      "en":"Password (minimum 8 characters)"
    },
    "confirm_password" : {
      "id":"Konfirmasi Password",
      "en":"Confirm Password"
    },
    "registerErrorEmail" : {
      "id":"Email Sudah Terdaftar",
      "en":"Email has been registered"
    },
    "passwordNotMatch" : {
      "id":"Password dan konfirmasi password tidak sama",
      "en":"The password does not match the conditions"
    },
    "sendButton" : {
      "id":"Kirim",
      "en":"SEND"
    },
    "forgotPasswordFailed" : {
      "id":"Lupa Password Gagal!",
      "en":"Forgot Password Failed!"
    },
    "forgotPasswordErrorMail" : {
      "id":"Email tidak terdaftar",
      "en":"Unregistered email"
    },
    "forgotVerification" : {
      "id":"Verifikasi",
      "en":"Verification"
    },
    "forgotSentVerification" : {
      "id":"Kami sudah mengirimkan kode verifikasi ke ",
      "en":"We have sent the verification code to "
    },
    "forgotPasswordNote": {
      "id": "Jika anda menggunakan google account maka email verifikasi tidak akan terkirim",
      "en": "If you use Google account, the verification email will not be sent"
    },
    "forgotEnterCode" : {
      "id":" Masukan kode untuk verifiksi.",
      "en":" enter the code to verify"
    },
    "forgotReSend" : {
      "id":"Kirim ulang kode verifikasi?",
      "en":"Resend verification code?"
    },
    "newPassword" : {
      "id":"Password Baru",
      "en":"New Password"
    },
    "newPasswordItTime" : {
      "id":" silahkan untuk reset password sekarang",
      "en":" it's time to make your new TRAC password"
    },
    "promo" : {
      "id":"Promo",
      "en":"Promo"
    },
    "product_service" : {
      "id":"Produk & Layanan",
      "en":"Product and services"
    },
    "blog" : {
      "id":"Blog",
      "en":"Inspirasi, Tips & Event"
    },
    "about" : {
      "id":"Tentang Kami",
      "en":"About"
    },
    "support" : {
      "id":"Pusat Bantuan",
      "en":"Support"
    },
    "contactUs" : {
      "id":"Hubungi Kami",
      "en":"Contact Us"
    },
    "branchLocation" : {
      "id":"Lokasi Cabang/Outlet",
      "en":"CarRentalFormInput"
    },
    "faq" : {
      "id":"FAQ",
      "en":"FAQ"
    },
    "company" : {
      "id":"Tentang TRAC",
      "en":"Company"
    },
    "aboutUs" : {
      "id":"Tentang Kami",
      "en":"About Us"
    },
    "career" : {
      "id":"Karir",
      "en":"Career"
    },
    "corporateCarService" : {
      "id":"Corporate Car Services",
      "en":"Corporate Car Services"
    },
    "driverServie" : {
      "id":"Driver Services",
      "en":"Driver Services"
    },
    "corporateBikeService" : {
      "id":"Corporate Bike Services",
      "en":"Corporate Bike Services"
    },
    "busService" : {
      "id":"Bus Services",
      "en":"Bus Services"
    },
    "tracToGo" : {
      "id":"Trac to Go",
      "en":"Trac to Go"
    },
    "subcribeToNews" : {
      "id":"Berlangganan newsletter",
      "en":"Subscribe to our newsletter"
    },
    "insertYourEmail" : {
      "id":"Berlangganan newsletter",
      "en":"Insert your email here"
    },
    "termsAndConditions" : {
      "id":"Syarat & Ketentuan",
      "en":"Terms and Conditions"
    },
    "privacyPolicy" : {
      "id":"Kebijakan Privasi",
      "en":"Privacy Policy"
    },
    "followUs" : {
      "id":"Follow Kami",
      "en":"Follow us on social media"
    },
    "facebookLink" : {
      "id":"https://web.facebook.com/TRACAstraRentACar/?ref=br_rs",
      "en":"https://web.facebook.com/TRACAstraRentACar/?ref=br_rs"
    },
    "twitterLink" : {
      "id":"https://twitter.com/tracastra?lang=id",
      "en":"https://twitter.com/tracastra?lang=en"
    },
    "instagramLink" : {
      "id":"https://www.instagram.com/trac_astra/",
      "en":"https://www.instagram.com/trac_astra/"
    },
    "youtubeLink" : {
      "id":"https://www.youtube.com/channel/UC-9jLt-k0R30YzImXxnn6PQ",
      "en":"https://www.youtube.com/channel/UC-9jLt-k0R30YzImXxnn6PQ"
    },
    "scrollToExplore" : {
      "id":"Mulai penjelajahan",
      "en":"Scroll to Explore"
    },
    "carRental" : {
      "id":"Rental Mobil",
      "en":"Car Rental"
    },
    "airportTransfer" : {
      "id":"Antar-Jemput Bandara",
      "en":"Airport Transfer"
    },
    "busRental" : {
      "id":"Sewa Bus",
      "en":"Bus Rental"
    },
    "typeOfService" : {
      "id":"Jenis Layanan",
      "en":"Type of Service"
    },
    "selectType" : {
      "id":"Pilih",
      "en":"Select Type"
    },
    "chauffeur" : {
      "id":"Dengan Pengemudi",
      "en":"Chauffeur"
    },
    "pickupCityAdress" : {
      "id":"Lokasi Penjemputan",
      "en":"Pickup City & Address"
    },
    "pickupCarRental" : {
      "id":"Lokasi Anda",
      "en":"Pickup City & Address"
    },
    "selectYourCity" : {
      "id":"Cari Kota",
      "en":"Select Your City"
    },
    "selectCity" : {
      "id":"Pilih Kota",
      "en":"Select City"
    },
    "sorryAreaOutCoverage" : {
      "id":"Kota yang dipilih tidak tercakup dalam area layanan kami",
      "en":"We're sorry, the area you search is out of coverage"
    },
    "searchCarRental" : {
      "id":"Ganti Kota Lainnya",
      "en":"Search Car Rental"
    },
    "findYourAddress" : {
      "id":"Cari Alamat",
      "en":"Find Your Address"
    },
    "rentalPackage" : {
      "id":"Paket Sewa",
      "en":"Rental Package"
    },
    "selectPackage" : {
      "id":"Pilih Paket",
      "en":"Select Package"
    },
    "4hours" : {
      "id":"4 jam",
      "en":"4 hours/day"
    },
    "12hours" : {
      "id":"12 jam",
      "en":"12 hours/day"
    },
    "24hours" : {
      "id":"24 jam",
      "en":"24 hours/day"
    },
    "dateTime" : {
      "id":"Tanggal & Waktu Jemput",
      "en":"Date and Time"
    },
    "selectDateTime" : {
      "id":"Pilih Tanggal dan Jam",
      "en":"Select Date and Time"
    },
    "calendar" : {
      "id":"Calendar",
      "en":"Kalender"
    },
    "pickupHour" : {
      "id":"Waktu",
      "en":"Pickup Hour"
    },
    "resetDate" : {
      "id":"Ganti Waktu",
      "en":"Reset Date"
    },
    "typeOfPickup" : {
      "id":"Pengambilan/Pengantaran mobil",
      "en":"Type of Pickup"
    },
    "pickupMethod" : {
      "id":"Cara Pengambilan/Pengantaran mobil",
      "en":"Pickup Method"
    },
    "howTheCarDeliver" : {
      "id":"Pilih Cara Pengambilan/Pengantaran mobil",
      "en":"How the car deliver to you"
    },
    "howTheCarReturn" : {
      "id":"Bagaimana anda mengembalikan kendaraan?",
      "en":"How you will return the car?"
    },
    "deliverToLocation" : {
      "id":"Diantarkan ke Kamu",
      "en":"Deliver to Location"
    },
    "pickupAtpool" : {
      "id":"Jemput di Cabang Terdekat",
      "en":"Pickup at pool"
    },
    "deliverToPool" : {
      "id":"diantar ke Cabang Terdekat",
      "en":"Deliver to pool"
    },
    "logout": {
      "id": "Keluar",
      "en": "Logout"
    },
    "hai": {
      "id": "Hai",
      "en": "Hi"
    },
    "myBookingQuote": {
      "id": "ini pesanan Anda, cari informasi tentang pemesanan Anda",
      "en": "this is your order, find information about your booking"
    },
    "orderReview": {
      "id": "Ringkasan Pesanan",
      "en": "Review Order"
    },
    "pickupSchedule": {
      "id": "Jadwal Penjemputan",
      "en": "Pickup Schedule"
    },
    "PaymentViaATM": {
      "id": "Pembayaran Melalui ATM",
      "en": "Payment via ATM"
    },
    "PaymentViaMBanking": {
      "id": "Pembayaran Melalui M-Banking",
      "en": "Payment via M-Banking"
    },
    "Congratulations": {
      "id": "Selamat",
      "en": "Congratulations"
    },

    "dateAndTime" : {
      "id":"Tanggal dan Jam",
      "en":"Date and Time"
    },
    "search" : {
      "id":"Cari",
      "en":"Search"
    },
    "getTheBestDeal" : {
      "id":"Promo Rental Mobil & Sewa Bus Terbaru",
      "en":"Get the best deal for you"
    },
    "seeAllDeals" : {
      "id":"Lihat Semua Promo",
      "en":"SEE ALL DEALS"
    },
    "whyWeDifferent" : {
      "id":"Mengapa TRAC?",
      "en":"Why We Are Different"
    },
    "premiumQualityRental" : {
      "id":"Layanan Pelanggan 24/7",
      "en":"Premium Quality Car"
    },
    "premiumQualityRentalDetail" : {
      "id":"Kami siap membantu Anda setiap hari selama 24 jam. Hubungi Customer Assiatance Center di nomor 1500 009 untuk pusat bantuan, informasi, maupun pemesanan.",
      "en":"TRAC always strive to provide the best quality of services that assures customer satisfaction through premium and various car unit drivers and excellent services proven over 30 years experiences."
    },
    "fastBookingProcess" : {
      "id":"Akses Mudah & Jaringan Luas",
      "en":"Fast Booking Process"
    },
    "fastBookingProcessDetail" : {
      "id":"Reservasi semakin mudah melalui website dan aplikasi TRAC. Dengan dukungan jaringan luas yang tersebar di lebi dari 50 kota di Indonesia, kami siap melayani kebutuhan transportasi Anda.",
      "en":"We ensure to deliver the best mobility for your better lifestyle with our high level of responsiveness by putting our customer first through fast booking process, car subtitution and additional request. "
    },
    "insuranceCoverageSafety" : {
      "id":"Jaminan Proteksi & Layanan Prima",
      "en":"Insurance Coverage & Safety"
    },
    "insuranceCoverageSafetyDetail" : {
      "id":"Demi ketenangan dan rasa aman selama perjalanan, kami melindungi Anda dengan proteksi asuransi dan jaminan kondisi armada yang selalu prima.",
      "en":"TRAC provides insurance coverage for its as required as a commitment in minimizing possible risks related to safety and environment."
    },
    "wideRangeNetworking" : {
      "id":"Andal dengan Teknologi Teruji",
      "en":"Wide Range Networking"
    },
    "wideRangeNetworkingDetail" : {
      "id":"Sistem kami terhubung dengan TRAC Fleet Management Solution sehingga kami mampu memastikan keselamatan serta kenyamanan perjalanan Anda secara akurat dan real-time.",
      "en":"TRAC as Indonesia's largest car rental company with an integrated network in more than 20 big cities in Indonesia"
    },
    "pickYourStyle" : {
      "id":"Varian Kendaraan Kami",
      "en":"Pick Your Style"
    },
    "empoweringMobility" : {
      "id":"Kami memiliki beragam varian kendaraan untuk berbagai kebutuhan transportasi Anda",
      "en":"Empowering mobility through our fleets"
    },
    "luxuryCar" : {
      "id":"Kami memiliki beragam varian kendaraan untuk berbagai kebutuhan transportasi Anda",
      "en":"Luxury Car"
    },
    "mvp" : {
      "id":"MVP",
      "en":"MVP"
    },
    "cityCar" : {
      "id":"City Car",
      "en":"City Car"
    },
    "viewAllVehicle" : {
      "id":"Lihat Semua Mobil",
      "en":"VIEW ALL VEHICLES"
    },
    "seeAllBlog" : {
      "id":"Selengkapnya",
      "en":"SEE ALL BLOGS"
    },
    "news" : {
      "id":"Berita",
      "en":"News"
    },
    "readMore" : {
      "id":"Selengkapnya",
      "en":"READ MORE"
    },
    "hearFromCostumer" : {
      "id":"Kata pelanggan kami",
      "en":"Hear From Our Customer"
    },
    "checkOutAllProduct" : {
      "id":"Kenali produk & layanan kami",
      "en":"Check Out All Of Our Products"
    },
    "checkOutAllProductDesc" : {
      "id":"Ketahui keunggulan beragam produk serta layanan kami dan temukan solusi transportasi yang sesuai dengan kebutuhan Anda. ",
      "en":"Ketahui keunggulan beragam produk serta layanan kami dan temukan solusi transportasi yang sesuai dengan kebutuhan Anda. "
    },
    "productService" : {
      "id":"Produk & Layanan",
      "en":"Product & Services"
    },
    "startYourAdventure" : {
      "id":"Download TRAC  To Go  App Sekarang!",
      "en":"Start Your Adventure With TRAC To Go App"
    },
    "bookOnlineYourCar" : {
      "id":"Reservasi mobil, bus, dan airport transfer  mudah & aman",
      "en":"Book Online Your Car or Airport Transfer"
    },
    "trackYourDriver" : {
      "id":"Update  posisi pengemudi ",
      "en":"Track Your Driver"
    },
    "flexibilityChange" : {
      "id":"Special Promo khusus transaksi via aplikasi",
      "en":"Flexibility To Change And Cancel Reservation Anytime"
    },
    "fromAirport" :{
      "en":"From (Airport)",
      "id":"Dari (Bandara)"
    },
    "selectAirport" :{
      "en":"Select Airport",
      "id":"Pilih Bandara"
    },
    "selectArea" :{
      "en":"Select Area",
      "id":"Pilih Area"
    },
    "toAreaAdress" :{
      "en":"To (Area, Address, Building)",
      "id":"Ke (area, alamat, gedung)"
    },
    "destinationBus" :{
      "en":"Destination (Area, Address, Building)",
      "id":"Tujuan (area, alamat, gedung)"
    },
    "flightDetails" :{
      "en":"Flight Details",
      "id":"Informasi Penerbangan"
    },
    "enterFlightDetails" :{
      "en":"Enter Flight Details",
      "id":"Isi Informasi Penerbangan"
    },
    "airlineNameNumber" :{
      "en":"Airline Name & Number",
      "id":"Maskapai & Nomor Penerbangan"
    },
    "pickupTime" :{
      "en":"Pickup Time",
      "id":"Jam"
    },
    "dateSelect" :{
      "en":"Date Select",
      "id":"Pilih Tanggal"
    },
    "carType" :{
      "en":"car type",
      "id":"jenis mobil"
    },
    "clear" :{
      "en":"clear",
      "id":"hapus"
    },
    "lowestPrice" :{
      "en":"Lowest Price",
      "id":"Harga Terendah"
    },
    "highestPrice" :{
      "en":"Highest Price",
      "id":"Harga Tertinggi"
    },
    "from" :{
      "en":"From",
      "id":"Dari"
    },
    "ke" :{
      "en":"To",
      "id":"Ke"
    },
    "moreInfo" :{
      "en":"MORE INFO",
      "id":"INFO LENGKAP"
    },
    "TaxesFeesIncluded" :{
      "en":"Taxes and Fees Included",
      "id":"Termasuk pajak, biaya pengemudi, BBM & parkir"
    },
    "showDetail" :{
      "en":"SHOW DETAIL",
      "id":"BUKA DETAIL"
    },
    "hideDetail" :{
      "en":"HIDE DETAIL",
      "id":"TUTUP DETAIL"
    },
    "totalPayment" :{
      "en":"Total Payment",
      "id":"Total Pembayaran"
    },
    "day" :{
      "en":"day",
      "id":"hari"
    },
    "select" :{
      "en":"SELECT",
      "id":"PILIH"
    },
    "car" :{
      "en":"CAR",
      "id":"MOBIL"
    },
    "cars" :{
      "en":"CARS",
      "id":"MOBIL"
    },
    "bus": {
      "en":"BUS",
      "id":"BIS"
    },
    "buses": {
      "en":"BUSES",
      "id":"BIS"
    },
    "carOrder" :{
      "en":"Car Order",
      "id":"Order Mobil"
    },
    "busOrder" :{
      "en":"Bus Order",
      "id":"Order Bis"
    },
    "personalDetail" :{
      "en":"Personal Detail",
      "id":"Data Pribadi"
    },
    "personalInformation" :{
      "en":"Personal Information",
      "id":"Data Pribadi"
    },
    "payment" :{
      "en":"Payment",
      "id":"Pembayaran"
    },
    "previousCar" :{
      "en":"PREVIOUS CAR",
      "id":"SEBELUMNYA"
    },
    "nextCar" :{
      "en":"NEXT CAR",
      "id":"SELANJUTNYA"
    },
    "selectedCars" :{
      "en":"Selected Cars",
      "id":"Mobil Terpilih"
    },
    "wouldYouExtras" :{
      "en":"Would you like any extra items?",
      "id":"Apakah Anda butuh produk tambahan?"
    },
    "bookingDetail" :{
      "en":"Booking Detail",
      "id":"Detail Pemesanan"
    },
    "packages" :{
      "en":"Packages",
      "id":"Jumlah Durasi"
    },
    "dates" :{
      "en":"Dates",
      "id":"Tanggal"
    },
    "pickUpTime" :{
      "en":"Pick Up Time",
      "id":"Waktu Penjemputan"
    },
    "pickUpLocation" :{
      "en":"Pick Up Location",
      "id":"Lokasi Penjemputan"
    },
    "doYouHavePromo" :{
      "en":"Do You Have Promo/Referal Code?",
      "id":"Punya Promo/Kode Referal?"
    },
    "enterPromoReferral" :{
      "en":"Enter Promo/Referral Code",
      "id":"Masukkan Promo/Kode Referal"
    },
    "apply" :{
      "en":"APPLY",
      "id":"GUNAKAN"
    },
    "notesRequest" :{
      "en":"Notes/Request",
      "id":"Catatan/Permintaan Khusus"
    },
    "notesNote" :{
      "en":"Add your detail location or another request detail here",
      "id":"*Tambahkan informasi lokasi Anda serinci mungkin atau permintaan khusus lainnya."
    },
    "paymentDetails" :{
      "en":"Payment Details",
      "id":"Detail Pembayaran"
    },
    "pleaseEnterLocation" :{
      "en":"Please enter location detail or other request",
      "id":"Silahkan masukkan detail lokasi atau permintaan khusus"
    },
    "order" :{
      "en":"ORDER",
      "id":"ORDER"
    },
    "fullName" :{
      "en":"Full Name",
      "id":"Nama Lengkap"
    },
    "asOnIDCard" :{
      "en":"As on ID Card/Passport/driving license",
      "id":"Sesuai KTP/Pasport/SIM"
    },
    "phoneNumber" :{
      "en":"Phone Number",
      "id":"Nomor Telepon"
    },
    "ktpNumber" :{
      "en":"KTP Number",
      "id":"Nomor KTP"
    },
    "simNumber" :{
      "en":"SIM Number",
      "id":"Nomor SIM"
    },
    "residentialAddress" :{
      "en":"Residential Address",
      "id":"Alamat Domisili"
    },
    "indeonsiaCitizen" :{
      "en":"Indonesia Citizen",
      "id":"WNI"
    },
    "foreignCitizen" :{
      "en":"Foreign Citizen",
      "id":"WNA"
    },
    "photoKTP" :{
      "en":"PHOTO KTP",
      "id":"FOTO KTP"
    },
    "photoSIM" :{
      "en":"PHOTO SIM",
      "id":"FOTO SIM"
    },
    "continue" :{
      "en":"CONTINUE",
      "id":"LANJUTKAN"
    },
    "congrats" :{
      "en":"Congratulation!",
      "id":"Selamat!"
    },
    "yourReservationNumber": {
      "en":"Your reservation number is",
      "id":"Nomor reservasi anda"
    },
    "yoreReservationCreated" :{
      "en":"Your Reservation has been created",
      "id":"Anda berhasil melakukan reservasi"
    },
    "pleaseContinueToPayment" :{
      "en":"Please continue to payment process of your Booking.",
      "id":"Silahkan lanjutkan proses pembayaran untuk pemesanan Anda"
    },
    "virtualAccount" :{
      "en":"Virtual Account",
      "id":"Virtual Account"
    },
    "youCanTransfer" :{
      "en":"Anda dapat melakukan transfer dana melalui beragam channel (via mobile dan internet banking, ATM, atau teller)",
      "id":"Anda dapat melakukan transfer dana melalui beragam channel (via mobile dan internet banking, ATM, atau teller)"
    },
    "iUnderstandTerms" :{
      "en":"I undertand the Terms and Conditions",
      "id":"Saya menyetujui Syarat & Ketentuan"
    },
    "iAgree" :{
      "en":"I AGREE",
      "id":"SETUJU"
    },
    "decline" :{
      "en":"DECLINE",
      "id":"TIDAK SETUJU"
    },
    "makePayment" :{
      "en":"MAKE PAYMENT",
      "id":"BAYAR SEKARANG"
    },
    "pleaseCompletePayment" :{
      "en":"Please complete your payment in ",
      "id":"Mohon segera lakukan pembayaran dalam waktu "
    },
    "virtualAccountNumber" :{
      "en":"Virtual Account Number",
      "id":"Nomor Virtual Account"
    },
    "pembayaranViaATMMandiri" :{
      "en":"Pembayaran via ATM Mandiri ",
      "id":"Pembayaran via ATM Mandiri "
    },
    "pembayaranViaATMMandiriItem" :{
      "en":"Masukkan kartu ATM dan PIN#Pilih menu \"Bayar/Beli\"#Pilih menu \"Lainnya\", hingga menemukan menu \"Multipayment\"#Masukkan Kode Biller nama perusahaan  (kode), lalu pilih Benar#Masukkan \"Nomor Virtual Account\" , lalu pilih tombol Benar#Masukkan Angka \"1\" untuk memilih tagihan, lalu pilih tombol Ya#Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya#Simpan struk sebagai bukti pembayaran Anda ",
      "id":"Masukkan kartu ATM dan PIN#Pilih menu \"Bayar/Beli\"#Pilih menu \"Lainnya\", hingga menemukan menu \"Multipayment\"#Masukkan Kode Biller nama perusahaan  (kode), lalu pilih Benar#Masukkan \"Nomor Virtual Account\" , lalu pilih tombol Benar#Masukkan Angka \"1\" untuk memilih tagihan, lalu pilih tombol Ya#Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya#Simpan struk sebagai bukti pembayaran Anda "
    },
    "pembayaranViaMandiriOnline" :{
      "en":"Mandiri Internet Banking / Mandiri Online",
      "id":"Mandiri Internet Banking / Mandiri Online"
    },
    "pembayaranViaMandiriOnlineItem" :{
      "en":"Login Mandiri Online dengan memasukkan Username dan Password#Pilih menu \"Pembayaran\"#Pilih menu \"Multipayment\"# Pilih penyedia jasa \"DOKU\"#Masukkan \"Nomor Virtual Account\" dan \"Nominal\" yang akan dibayarkan, lalu pilih Lanjut#Setelah muncul tagihan, pilih Konfirmasi#Masukkan PIN / Challenge Code Token#Transaksi selesai, simpan bukti bayar Anda",
      "id":"Login Mandiri Online dengan memasukkan Username dan Password#Pilih menu \"Pembayaran\"#Pilih menu \"Multipayment\"# Pilih penyedia jasa \"DOKU\"#Masukkan \"Nomor Virtual Account\" dan \"Nominal\" yang akan dibayarkan, lalu pilih Lanjut#Setelah muncul tagihan, pilih Konfirmasi#Masukkan PIN / Challenge Code Token#Transaksi selesai, simpan bukti bayar Anda"
    },
    "pembayaranViaATMPermata" :{
      "en":"Pembayaran via ATM Permata",
      "id":"Pembayaran via ATM Permata"
    },
    "pembayaranViaATMPermataItem" :{
      "en":"Masukkan kartu ATM dan PIN#Pilih menu transaksi Lainnya#Pilih menu rekening nasabah lain Permata#Masukkan nomor rekening dengan nomor Virtual Account dan pilih Benar#Masukkan jumlah nominal transaksi sesuai dengan invoice yang ditagihkan. Setelah itu pilih Benar#Pilih rekening anda. Tunggu sebentar hingga muncul konfirmasi pembayaran. Kemudian pilih Ya",
      "id":"Masukkan kartu ATM dan PIN#Pilih menu transaksi Lainnya#Pilih menu rekening nasabah lain Permata#Masukkan nomor rekening dengan nomor Virtual Account dan pilih Benar#Masukkan jumlah nominal transaksi sesuai dengan invoice yang ditagihkan. Setelah itu pilih Benar#Pilih rekening anda. Tunggu sebentar hingga muncul konfirmasi pembayaran. Kemudian pilih Ya"
    },
    "pembayaranViaMobilePermata" :{
      "en":"Pembayaran via Internet Banking/Mobile Banking  Permata",
      "id":"Pembayaran via Internet Banking/Mobile Banking  Permata"
    },
    "pembayaranViaMobilePermataItem" :{
      "en":"Login#Klik menu pembayaran tagihan dan pilih menu virtual account#Pilih tagihan anda dan pilih daftar tagihan baru#Masukkan nomor rekening dengan nomor Virtual Account.  Apabila selesai silahkan klik Konfirmasi#Masukkan jumlah nominal tagihan sesuai dengan invoice. Setelah itu klik Konfirmasi#Masukkan Response Code dan klik Konfirmasi ",
      "id":"Masukkan kartu ATM dan PIN#Pilih menu transaksi Lainnya#Pilih menu rekening nasabah lain Permata#Masukkan nomor rekening dengan nomor Virtual Account dan pilih Benar#Masukkan jumlah nominal transaksi sesuai dengan invoice yang ditagihkan. Setelah itu pilih Benar#Pilih rekening anda. Tunggu sebentar hingga muncul konfirmasi pembayaran. Kemudian pilih Ya"
    },
    "passengers" :{
      "en":"Passengers",
      "id":"Penumpang"
    },
    "price" :{
      "en":"PRICE",
      "id":"HARGA"
    },
    "days" :{
      "en":"days",
      "id":"hari"
    },
    "trip" :{
      "en":"trip",
      "id":"perjalanan"
    },
    "done" :{
      "en":"Done",
      "id":"Selesai"
    },
    "insurance" :{
      "en":"Insurance",
      "id":"Asuransi"
    },
    "carTypeBus" :{
      "en":"CAR TYPE",
      "id":"JENIS BUS"
    },
    "hurry" :{
      "en":"HURRY!",
      "id":"PESAN SEGERA!"
    },
    "carLeft" :{
      "en":"CAR LEFT",
      "id":"MOBIL"
    },
    "carLeftBus" :{
      "en":"CAR LEFT",
      "id":"BUS"
    },
    "left": {
      "id": "Tersisa",
      "en": "Left"
    },
    "coordinatorBus" :{
      "en":"Coordinator Bus #",
      "id":"Informasi Pemesan Bus #"
    },
    "informationCoordinator" :{
      "en":"Bus Coordinator",
      "id":"Koordinator Bis"
    },
    "pleaseScanUploadKTPCoordinator" :{
      "en":"Please Scan or Upload the KTP of the person coordinator the bus faster process",
      "id":"Mohon unggah foto KTP pemesan bus untuk mempercepat proses"
    },
    "pleaseScanUploadKTPDriver" :{
      "en":"Please Scan or Upload the KTP and SIM of the person driving the car faster process",
      "id":"Mohon unggah foto KTP dan SIM dari supir untuk mempercepat proses"
    },
    "youCanDragAndDrop" :{
      "en":"You can drag and drop images to upload",
      "id":"Drag dan drop foto untuk mengunggah"
    },
    "youHaveSuccessfullyRent" :{
      "en":"You have succesfully rent a car",
      "id":"Anda berhasil melakukan pemesanan kendaraan"
    },
    "withTracToGo" :{
      "en":"with TRAC To Go",
      "id":"bersama TRAC To Go"
    },
    "yourBookingNumber" :{
      "en":"Your booking Number is",
      "id":"Nomor reservasi Anda adalah "
    },
    "detailOfYourBookingSent" :{
      "en":"Detail of your booking and an Email Verification is sent to",
      "id":"Detail pemesanan dan Verifikasi Email telah kami kirim ke"
    },
    "pleaseVerifyYourEmail" :{
      "en":"Please Verify your email to get more benefit from TRAC",
      "id":"Mohon verifikasi email Anda untuk mendapat lebih banyak keuntungan dari TRAC"
    },
    "showMyBooking" :{
      "en":"SHOW MY BOOKING",
      "id":"LIHAT RESERVASI SAYA"
    },
    "backToHome" :{
      "en":"BACK TO HOME",
      "id":"KEMBALI KE BERANDA"
    },
    "myBooking" :{
      "en":"My Booking",
      "id":"Reservasi Saya"
    },
    "historyBooking" :{
      "en":"History Booking",
      "id":"Riwayat Reservasi"
    },
    "accountSettings" :{
      "en":"Account Settings",
      "id":"Pengaturan Akun"
    },
    "b2cRental" :{
      "en":"B2C Rental",
      "id":"Rental Personal"
    },
    "carTotal" :{
      "en":"Car Total",
      "id":"Total Mobil"
    },
    "reservationNumber" :{
      "en":"Reservation Number",
      "id":"Nomor Reservasi"
    },
    "paymentGuide" :{
      "en":"PAYMENT GUIDE",
      "id":"PANDUAN PEMBAYARAN"
    },
    "carOnTheWay" :{
      "en":"Car on the way",
      "id":"Mobil dalam perjalanan"
    },
    "carWithYou" :{
      "en":"Car with you",
      "id":"Mobil sampai"
    },
    "complete" :{
      "en":"Complete",
      "id":"Selesai"
    },
    "driverInformation" :{
      "en":"Driver Information",
      "id":"Informasi Pengemudi"
    },
    "name" :{
      "en":"Name",
      "id":"Nama"
    },
    "mobileNumber" :{
      "en":"Mobile Number",
      "id":"Nomor HP"
    },
    "vehicleLicensePlate" :{
      "en":"Vehicle License Plate",
      "id":"Plat Nomor Mobil"
    },
    "newsSlideShow" :{
      "en":"Apart from Vacationing, Car Rental Can Make Everyday Needs",
      "id":"Selain Berlibur, Sewa Mobil Bisa Buat Keperluan Sehari-Hari"
    },
    "read" :{
      "en":"READ",
      "id":"BACA"
    },
    "feature" :{
      "en":"Feature",
      "id":"Tips"
    },
    "guidance" :{
      "en":"Guidance",
      "id":"Travel"
    },
    "event" :{
      "en":"Event",
      "id":"Event"
    },
    "loginSliderTitle" :{
      "en":"Find Your Next Adventure with TRAC",
      "id":"Rental Mobil Untuk Semua Keperluan Perjalanan"
    },
    "loginSliderDesc" :{
      "en":"Apapun tujuan perjalanan Anda bersama TRAC berpergian akan lebih nyaman dan aman!",
      "id":"Apapun tujuan perjalanan Anda bersama TRAC berpergian akan lebih nyaman dan aman!"
    },
    "tracAccount" :{
      "en":"TRAC Account",
      "id":"Akun TRAC"
    },
    "inputAmount" :{
      "en":"Input Amount",
      "id":"Jumlah Penumpang"
    },
    "changePhoto" :{
      "en":"Change Photo",
      "id":"Ganti foto"
    },
    "displayName" :{
      "en":"Display Name",
      "id":"Nama"
    },
    "changePassword" :{
      "en":"Change Password",
      "id":"Ubah Password"
    },
    "identificationInformation" :{
      "en":"Identification Information",
      "id":"Data Diri"
    },
    "weWillAutoFill" :{
      "en":"We'll autofill your details and make it easier for you to rent services through TRAC. Your payment details are stored securely",
      "id":"Kami akan melengkapi informasi Anda secara otomatis untuk memudahkan Anda menggunakan layanan TRAC. Kami memastikan detail pembayaran Anda tersimpan dengan aman."
    },
    "licenseNumber" :{
      "en":"License Number",
      "id":"Nomor SIM"
    },
    "address" :{
      "en":"Address",
      "id":"Alamat"
    },
    "creditCardNumber" :{
      "en":"Credit Card Number",
      "id":"Nomor Kartu Kredit"
    },
    "expiryDate" :{
      "en":"Expiry Date",
      "id":"Masa Berlaku"
    },
    "addCard" :{
      "en":"Add Card",
      "id":"Tambah Kartu"
    },
    "setPrimary" :{
      "en":"Add Card",
      "id":"Kartu Utama"
    },
    "removeCard" :{
      "en":"Remove Card",
      "id":"Hapus Kartu"
    },
    "uploadPhoto" :{
      "en":"Upload Photo",
      "id":"Unggah Foto"
    }
  }