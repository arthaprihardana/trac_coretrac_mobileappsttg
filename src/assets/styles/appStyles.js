import { StyleSheet } from 'react-native';
import appVariables from './appVariables';
import appMetrics from './appMetrics';

const appStyles = StyleSheet.create({
  appContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appImageBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
  },
  fullScreen: {
    width: appMetrics.windowWidth, 
    height: appMetrics.windowHeight,
  },
  appText: {
    fontFamily: appVariables.museo500,
    fontSize: 16,
  },
  appButton: {
    fontFamily: appVariables.museo500,
    color: appVariables.colorWhite,
    fontSize: 18,
  },
  

  /* Global Styling */
  left: {
    alignSelf: 'flex-start',
  },
  right: {
    alignSelf: 'flex-end',
  },
  applyFont: {
    fontFamily: appVariables.museo500,
  },
  bold: {
    fontFamily: appVariables.museo700,
  },
  p10: {
    padding: 10
  },
  p20: {
    padding: 20
  },
  m10: {
    margin: 10
  },
  m20: {
    margin: 20
  },
  mb10: {
    marginBottom: 10,
  },
  mb20: {
    marginBottom: 20,
  },
  mt10: {
    marginTop: 10,
  },
  mt20: {
    marginTop: 20
  },
  alignCenter: {
    textAlign: 'center'
  }
})

export default appStyles;