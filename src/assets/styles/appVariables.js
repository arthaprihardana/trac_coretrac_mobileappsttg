const appVariables = {
  /* Color */
  colorBlack: '#000000',
  colorOrange: '#F27D20',
  colorWhite: 'white',
  colorDark: '#1C242F',
  colorGray: '#8A959E',
  colorLink: '#204496',
  colorDarkBlue: '#2A2E36',
  colorBlueHeader: '#21419A',
  colorBorderColor: '#D9D5DC',
  colorLabelActive: '#808080',

  brandPrimary:"#F27D20",
  brandInfo:"rgba(0,56,169,1)",
  brandSuccess:"#5cb85c",
  brandDanger:"#d9534f",
  brandWarning:"#f0ad4e",

  /* Tab */
  colorTabInactive: 'rgba(255, 255, 255, 0.5)',
  colorBorderBottom: 'rgba(255, 255, 255, 0.3)',
  colorBorder: 'rgba(255, 255, 255, 0.6)',
  tabFont: 9,

  /* Swiper */
  colorActivePagination: '#FFFFFF',
  colorPagination: 'rgba(255, 255, 255, 0.5)',

  /* Fonts */
  museo500: "MuseoSans-500",
  museo700: "MuseoSans-700",
  museo900: "MuseoSans-900",

  /* Font Size */
  fontNormal: 16,
  fontSmall: 11,

  padding: 20,
}

export default appVariables;