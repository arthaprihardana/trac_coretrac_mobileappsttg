# TRAC To Go Mobile Apps (Android / iOS)

Aplikasi tersedia di : 

[![PlayStore](https://play.google.com/intl/en_us/badges/images/badge_new.png)](https://play.google.com/store/apps/details?id=com.trac.tractogo&hl=in) 
![AppStore](https://developer.apple.com/app-store/marketing/guidelines/images/badge-download-on-the-app-store.svg)

## Description

TRAC To Go adalah sebuah platform yang menyediakan jasa Rental Mobil dan Bus yang berkualitas.

Platform ini dibuat menggunakan React Native.

## Usage Dependency

### Dependency
-   @ptomasroos/react-native-multi-slider
-   @react-native-community/cameraroll
-   accordion-collapse-react-native
-   axios
-   lodash
-   moment
-   native-base
-   react
-   react-native
-   react-native-calendar-picker
-   react-native-camera
-   react-native-confirmation-code-input
-   react-native-datepicker
-   react-native-firebase
-   react-native-fs
-   react-native-maps
-   react-native-mask
-   react-native-modal-dropdown
-   react-native-paper
-   react-native-picker-select
-   react-native-router-flux
-   react-native-safe-area-view
-   react-native-sentry
-   react-native-simple-auth
-   react-native-slider
-   react-native-snap-carousel
-   react-native-splash-screen
-   react-native-svg
-   react-native-swiper
-   react-native-vector-icons
-   react-native-webview
-   react-navigation
-   react-navigation-material-bottom-tabs
-   react-navigation-redux-helpers
-   react-redux
-   redux
-   redux-persist
-   redux-saga
-   rn-fetch-blob
-   rn-placeholder
-   shelljs
-   svgo

### Dev Dependency

-   babel-core
-   babel-jest
-   babel-plugin-transform-remove-console
-   jest
-   metro-react-native-babel-preset
-   plist
-   react-native-debugger-open
-   react-test-renderer
-   semver

## How to Run The Project

### Install Dependencies

```
npm install or yarn install
```

### (Debug) Run On Device / Emulator (Android Only)

```
npm run android
or
yarn android
or
react-native run-android
```

### (Release) How to release The Application

1.  Ubah version di dalam file package.json.
2.  Jalankan perintah di dalam file ./scripts/version.js untuk merubah version ke semua platform (android/iOS)

```
npm run version
```

## Screenshot

### Welcome Screen
![onboarding-1](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559095094.png)
### Home Screen
![home](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559095609.png)
### Form Rental Mobil Screen
![car-rental](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559095613.png)
### Form Rental Mobile (Airport)
![airport-transfer](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559095616.png)
### Form Rental Bus Screen
![bus-rental](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559095620.png)
### List Car Screen
![car-list](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559096077.png)
### Detail Car Screen
![car-detail](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559096088.png)
### My Reservation Screen
![reservasi](https://bitbucket.org/arthaprihardana/trac_coretrac_mobileappsttg/raw/7fd5d413de7b7e7efe32e9d2ff0dcdec794f7bd4/screenshots/Screenshot_1559096160.png)